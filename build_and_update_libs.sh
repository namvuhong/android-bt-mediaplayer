#HUBBLE_APP_HOME=/home/hoang/Project/workspace/hubble-connected-app
HUBBLE_APP_HOME=/E/1.Git/1.bt/android-app
echo "Building native libs"
#/home/hoang/android-ndk-r8e/ndk-build
ndk-build
echo "Build native libs done"
echo ">>>>> Update arm binary files"
#cp -v libs/armeabi/libffmpeg_jni.so ../cvision-hubble/libs/armeabi/libffmpeg_jni.so
#cp -v libs/armeabi/libmediaplayer.so ../cvision-hubble/libs/armeabi/libmediaplayer.so
#cp -v libs/armeabi/librmc.so ../cvision-hubble/libs/armeabi/librmc.so
#cp -v libs/armeabi/libyuv.so ../cvision-hubble/libs/armeabi/libyuv.so

cp -v libs/armeabi/libffmpeg_jni.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/libffmpeg_jni.so
cp -v libs/armeabi/libmediaplayer.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/libmediaplayer.so
cp -v libs/armeabi/librmc.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/librmc.so
cp -v libs/armeabi/librmc_client.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/librmc_client.so
cp -v libs/armeabi/libyuv.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/libyuv.so
cp -v libs/armeabi/libffmpeg.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi/libffmpeg.so

echo ">>>>> Update arm-v7a binary files"
cp -v libs/armeabi/libffmpeg_jni.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/libffmpeg_jni.so
cp -v libs/armeabi/libmediaplayer.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/libmediaplayer.so
cp -v libs/armeabi/librmc.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/librmc.so
cp -v libs/armeabi/librmc_client.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/librmc_client.so
cp -v libs/armeabi/libyuv.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/libyuv.so
cp -v libs/armeabi/libffmpeg.so $HUBBLE_APP_HOME/app/libs/native-libs/armeabi-v7a/libffmpeg.so

echo ">>>>> Update x86 binary files"
#cp -v libs/x86/libffmpeg_jni.so ../cvision-hubble/libs/x86/libffmpeg_jni.so
#cp -v libs/x86/libmediaplayer.so ../cvision-hubble/libs/x86/libmediaplayer.so
#cp -v libs/x86/librmc.so ../cvision-hubble/libs/x86/librmc.so
#cp -v libs/x86/libyuv.so ../cvision-hubble/libs/x86/libyuv.so

cp -v libs/x86/libffmpeg_jni.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/libffmpeg_jni.so
cp -v libs/x86/libmediaplayer.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/libmediaplayer.so
cp -v libs/x86/librmc.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/librmc.so
cp -v libs/x86/librmc_client.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/librmc_client.so
cp -v libs/x86/libyuv.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/libyuv.so
cp -v libs/x86/libffmpeg.so $HUBBLE_APP_HOME/app/libs/native-libs/x86/libffmpeg.so
echo ">>>>>>>>> Update native libs done <<<<<<<<<<<"

