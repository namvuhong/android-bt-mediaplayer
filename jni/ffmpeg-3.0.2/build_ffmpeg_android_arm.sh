#!/bin/bash


######################################################
# Usage:
#   put this script in top of FFmpeg source tree
#   ./build_android
#
# It generates binary for following architectures:
#     ARMv6 
#     ARMv6+VFP 
#     ARMv7+VFPv3-d16 (Tegra2) 
#     ARMv7+Neon (Cortex-A8)
#
# Customizing:
# 1. Feel free to change ./configure parameters for more features
# 2. To adapt other ARM variants
#       set $CPU and $OPTIMIZE_CFLAGS 
#       call build_one
######################################################


if ! [ -f ../set_env_arm.sh ]; then
echo "file ../set_env_arm.sh is not found, create one"
exit 1
fi
. ../set_env_arm.sh

function dirty_build
{
make  -j4 

$PREBUILT/bin/$CROSS_COMPILE_PREFIX-ar d libavcodec/libavcodec.a inverse.o

make install

#Link all library to 1 libffmpeg.so
$PREBUILT/bin/$CROSS_COMPILE_PREFIX-ld -rpath-link=$PLATFORM/usr/lib \
    -L$PLATFORM/usr/lib  -L$PREFIX/lib \
    -soname libffmpeg.so \
    -shared -nostdlib  $NOEXECSTACK_FLAG  \
    --whole-archive --no-undefined -o $PREFIX/libffmpeg.so \
    --dynamic-linker=/system/bin/linker \
    $PREBUILT/lib/gcc/$CROSS_COMPILE_PREFIX/$ARM_GCC_VERSION/libgcc.a \
    -lc -lm -lz -ldl -llog \
    -Bstatic \
    -lavcodec -lavformat \
    -lavutil -lswscale \
    -lswresample -lavfilter \
    -lavdevice

}

function build_one
{
FFMPEG_DIR_NAME=ffmpeg-3.0.2
OPENSSL_DIR_NAME=openssl
echo "Building ffmpeg library for ARM..."
echo "Building openssl for ARM..."
cd ../$OPENSSL_DIR_NAME
echo "Entering openssl folder" $OPENSSL_DIR_NAME
pwd
./build_openssl_android_arm.sh
echo "Building openssl for ARM DONE"
echo "Start to build ffmpeg"
cd ../$FFMPEG_DIR_NAME

./configure --target-os=linux \
    --prefix=$PREFIX \
    --enable-cross-compile \
    --enable-memalign-hack \
    --enable-version3 \
    --enable-asm \
    --extra-libs="-lgcc" \
    --arch=$ARCH \
    --cc=$PREBUILT/bin/$CROSS_COMPILE_PREFIX-gcc \
    --cross-prefix=$PREBUILT/bin/$CROSS_COMPILE_PREFIX- \
    --nm=$PREBUILT/bin/$CROSS_COMPILE_PREFIX-nm \
    --sysroot=$PLATFORM \
    --extra-cflags=" -O3 -fpic -DANDROID -DHAVE_SYS_UIO_H=1 -Dipv6mr_interface=ipv6mr_ifindex -fasm -Wno-psabi -fno-short-enums  -fno-strict-aliasing -finline-limit=300 -I$OPENSSL_PREFIX/include $OPTIMIZE_CFLAGS -DRTCP_THRESHOLD=8 -DCVISION_RTP_WITH_STUN -DCVISION_GET_NAME_INFO -DCVISION_FILTER_CORRUPT_FRAMES -DCVISION_H264_ENCRYPTION" \
    --disable-shared \
    --enable-static \
    --enable-runtime-cpudetect \
    --extra-ldflags="-Wl,-rpath-link=$PLATFORM/usr/lib -L$PLATFORM/usr/lib -L$OPENSSL_PREFIX/lib -nostdlib -lc -lm -ldl -llog" \
    --disable-everything \
    --disable-doc \
    --enable-demuxer=h264 \
    --enable-demuxer=flv \
    --enable-demuxer=mpegps \
    --enable-demuxer=mpegts \
    --enable-demuxer=mpegvideo \
    --enable-demuxer=mpegtsraw \
    --enable-demuxer=pcm_s16le \
    --enable-muxer=flv \
    --enable-muxer=pcm_s16le \
    --enable-protocol=file \
    --enable-protocol=http \
    --enable-protocol=https \
    --enable-protocol=rtmp \
    --enable-protocol=rtmpt \
    --enable-protocol=rtmpe \
    --enable-protocol=rtmpte \
    --enable-protocol=rtmpts \
    --enable-protocol=rtmps \
    --enable-protocol=rtp \
    --enable-protocol=tcp \
    --enable-protocol=udp \
    --enable-protocol=tls_openssl \
    --enable-avformat \
    --enable-avcodec \
    --enable-decoder=imc \
    --enable-decoder=aac \
    --enable-decoder=mpeg4 \
    --enable-decoder=h264 \
    --enable-encoder=flv \
    --enable-decoder=flv \
    --enable-decoder=adpcm_swf \
    --enable-decoder=adpcm_ms \
    --enable-decoder=adpcm_ima_wav \
    --enable-decoder=adpcm_ima_qt \
    --enable-decoder=adpcm_yamaha \
    --enable-encoder=adpcm_swf \
    --enable-encoder=adpcm_ms \
    --enable-encoder=adpcm_ima_wav \
    --enable-encoder=adpcm_ima_qt \
    --enable-encoder=adpcm_yamaha \
    --enable-encoder=pcm_s16le \
    --enable-decoder=pcm_s16le \
    --enable-decoder=pcm_s16le_planar \
    --enable-parser=h264 \
    --enable-parser=mpeg4video \
    --enable-parser=mpegaudio \
    --enable-parser=mpegvideo \
    --enable-muxer=ffm --enable-demuxer=ffm --enable-muxer=rtp --enable-demuxer=rtp \
    --enable-muxer=rtsp --enable-demuxer=rtsp \
    --enable-decoder=amrnb \
    --enable-decoder=amrwb \
    --enable-decoder=adpcm_g722 \
    --enable-encoder=adpcm_g722 \
    --enable-muxer=g722 --enable-demuxer=g722 \
    --enable-encoder=pcm_mulaw \
    --enable-decoder=pcm_mulaw \
    --enable-encoder=pcm_alaw \
    --enable-decoder=pcm_alaw \
    --enable-muxer=pcm_mulaw \
    --enable-demuxer=pcm_mulaw \
    --enable-muxer=pcm_alaw \
    --enable-demuxer=pcm_alaw \
    --enable-filter=aresample \
    --enable-filter=aformat \
    --enable-openssl \
    $ADDITIONAL_CONFIGURE_FLAG


make clean
make -j4

$PREBUILT/bin/$CROSS_COMPILE_PREFIX-ar d libavcodec/libavcodec.a inverse.o

make install


#Link all library to 1 libffmpeg.so
$PREBUILT/bin/$CROSS_COMPILE_PREFIX-ld -rpath-link=$PLATFORM/usr/lib \
    -L$PLATFORM/usr/lib -L$PREFIX/lib -L$OPENSSL_PREFIX/lib \
    -soname libffmpeg.so \
    -shared -nostdlib  $NOEXECSTACK_FLAG \
    --whole-archive --no-undefined -o $PREFIX/libffmpeg.so \
    --dynamic-linker=/system/bin/linker \
    $PREBUILT/lib/gcc/$CROSS_COMPILE_PREFIX/$ARM_GCC_VERSION/libgcc.a \
    -lc -lm -lz -ldl -llog \
    -Bstatic \
    -lavcodec -lavformat \
    -lavutil -lswscale \
    -lswresample -lavfilter \
    -lavdevice -lssl -lcrypto
}


function configureARMv7a 
{
echo ">>>>>>>>>> Config env for Armv7-a With NEON support <<<<<<<<<<<<<<"
#arm v7n
CPU=armv7-a
OPTIMIZE_CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=$CPU -mthumb -D__thumb__ "
PREFIX=./android/armeabi-v7a 
ADDITIONAL_CONFIGURE_FLAG=--enable-neon
X264_PREFIX=../x264/android/$CPU
OPENSSL_PREFIX=../openssl/android/$CPU
#build_one

}

function configureARM
{
echo ">>>>>>>>>> Config env for Arm-generic Without  NEON support <<<<<<<<<<<<<<"
#arm 
CPU=armeabi
OPTIMIZE_CFLAGS=""
PREFIX=./android/$CPU 
X264_PREFIX=../x264/android/$CPU
OPENSSL_PREFIX=../openssl/android/$CPU
ADDITIONAL_CONFIGURE_FLAG=
}

#arm v6
#CPU=armv6
#OPTIMIZE_CFLAGS="-marm -march=$CPU"
#PREFIX=./android/$CPU 
#ADDITIONAL_CONFIGURE_FLAG=
#build_one

#arm v7vfpv3
#CPU=armv7-a
#OPTIMIZE_CFLAGS="-mfloat-abi=softfp -mfpu=vfpv3-d16 -marm -march=$CPU "
#PREFIX=./android/$CPU 
#ADDITIONAL_CONFIGURE_FLAG=
#build_one

#arm v7vfp
#CPU=armv7-a
#OPTIMIZE_CFLAGS="-mfloat-abi=softfp -mfpu=vfp -marm -march=$CPU "
#PREFIX=./android/$CPU-vfp
#ADDITIONAL_CONFIGURE_FLAG=
#build_one


#arm v6+vfp
#CPU=armv6
#OPTIMIZE_CFLAGS="-DCMP_HAVE_VFP -mfloat-abi=softfp -mfpu=vfp -marm -march=$CPU"
#PREFIX=./android/${CPU}_vfp 
#ADDITIONAL_CONFIGURE_FLAG=
#build_one




if [ $# -lt 1 ] ; then 
#full rebuild

#configureARMv7a 
#
#build_one || exit 1

configureARM

build_one || exit 1


elif [ $1 -eq 0 ] ; then 
#- Dont reconfigure - just dirty build
echo "**************** WARNING: this will just rebuild the last configuration (arm or armv7) not both "

configureARM
dirty_build
fi



