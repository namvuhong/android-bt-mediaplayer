#!/bin/bash
######################################################
# Usage:
#   put this script in top of FFmpeg source tree
#   ./build_ffmpeg_android_all.sh
#
# It generates binary for following architectures:
#     ARMv6 
#     ARMv6+VFP 
#     ARMv7+VFPv3-d16 (Tegra2) 
#     ARMv7+Neon (Cortex-A8)
######################################################

#Build ffmpeg for x86
echo "Building ffmpeg library for X86..."
./build_ffmpeg_android_x86.sh
echo "Building ffmpeg library for X86 DONE"

#Build ffmpeg for arm
echo "Building ffmpeg library for ARM..."
./build_ffmpeg_android_arm.sh
echo "Building ffmpeg library for ARM DONE"
