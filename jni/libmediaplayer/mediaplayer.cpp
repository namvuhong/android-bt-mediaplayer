/*
 * mediaplayer.cpp
 */


#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

struct AVBuffer;
struct AVBufferPool;
struct AVCodecDefault;
struct AVFilterFormats;
struct AVFilterGraphInternal;
struct AVFilterInternal;

extern "C" {

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/log.h"
#include "libavutil/time.h"
#include "libavutil/error.h"
#include "libavfilter/avfilter.h"
#include "libyuv/convert_from.h"
// Copy rtsp.h from ffmpeg
#include  "rtsp_.h"
} // end of extern C

#include "mediaplayer.h"
#include "output.h"

#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#endif

#define FPS_DEBUGGING true
#define VIDEO_THROUGHPUT_DEBUGGING false
#define AUDIO_THROUGHPUT_DEBUGGING false
#define BIT_RATE_DEBUGGING true

#define DEBUG_RECORDING_MODE 0
#define DEBUG_BUFFERING 0
#define DEBUG_RECORDING 0
#define DEBUG_READ_VIDEO_PKT 0
#define DEBUG_READ_AUDIO_PKT 0

#define DEBUG_UPDATE_VIDEO_CLK 0
#define DEBUG_UPDATE_AUDIO_CLK 0

#define TAG "FFMpegMediaPlayer-native"
#define LOG_TAG TAG

#define LOG_LEVEL 1000

#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else

extern void print( int level, char * format, ...);

#define log_info(...) printf(__VA_ARGS__)
#define LOGI  print

#endif


#ifndef INT64_MIN
#define INT64_MIN       (-0x7fffffffffffffffLL-1)
#endif

#ifndef INT64_MAX
#define INT64_MAX INT64_C(9223372036854775807)
#endif

// Define all timeout values here
#define TIMEOUT_WHILE_STREAMING 20
#define TIMEOUT_OPEN_INPUT 20
#define TIMEOUT_FIND_STREAM_INFO 20
#define TIMEOUT_SEEK_FILE 20

#define KEEP_ALIVE_INTERVAL 5*60*1000000
#define PROBE_SIZE_DEFAULT 200*1024

static int64_t sws_flags = SWS_BICUBIC;
static MediaPlayer* sPlayer;
static long data_in_bytes;
static long avg_in_10sec;
static int64_t t0;

static int should_abort_cb(void *opaque)
{
	int ret_value = 0;
	time_t start_time, current_time;
	int ret;
	MediaPlayer *sPlayer_cb = (MediaPlayer *)opaque;
	if (sPlayer_cb->getExternalInterruptFlag() == true)
	{
		sPlayer_cb->setFFmpegInterrupt(true);
		ret_value = 1;
	}
	else if (sPlayer_cb->getInterruptCallbackId() == INTERRUPT_CALLBACK_FIND_STREAM_INFO)
	{
		start_time = sPlayer_cb->getStartTime();
		if (start_time != -1)
		{
			ret = time(&current_time);
			if (ret == -1)
			{
				ret_value = 0;
			}
			else if ((current_time - start_time) > TIMEOUT_FIND_STREAM_INFO)
			{
				sPlayer_cb->setFFmpegInterrupt(true);
				ret_value = 1;
			}
		}
	}
	else if (sPlayer_cb->getInterruptCallbackId() == INTERRUPT_CALLBACK_OPEN_INPUT)
	{
		start_time = sPlayer_cb->getStartTime();
		if (start_time != -1)
		{
			ret = time(&current_time);
			if (ret == -1)
			{
				ret_value = 0;
			}
			else if ((current_time - start_time) > TIMEOUT_OPEN_INPUT)
			{
				sPlayer_cb->setFFmpegInterrupt(true);
				ret_value = 1;
			}
		}
	}
	else if (sPlayer_cb->getInterruptCallbackId() == INTERRUPT_CALLBACK_AV_READ_FRAME)
	{
		start_time = sPlayer_cb->getStartTime() ;
		if (start_time != -1)
		{
			ret = time(&current_time);
			if (ret == -1)
			{
				ret_value = 0;
			}
			else {
			  if (sPlayer_cb->getIsSdcardStreaming() == true)
			  {
			    int64_t remainingDuration = sPlayer_cb->getRemainingDuration();
			    if (current_time - start_time > TIMEOUT_WHILE_STREAMING ||
			        current_time - start_time > remainingDuration/1000000 + 2)
			    {
			      log_info("sdcard streaming, av_read_frame timeout...\n");
			      sPlayer_cb->setFFmpegInterrupt(true);
			      ret_value = 1;
			    }
			  }
			  else if ((current_time - start_time) > TIMEOUT_WHILE_STREAMING)
			  {
			    //cause av_read_frame() to fail
			    log_info("av_read_frame timeout...\n");
			    sPlayer_cb->setFFmpegInterrupt(true);
			    ret_value = 1;
			  }
			}
		}
	}
	else if (sPlayer_cb->getInterruptCallbackId() == INTERRUPT_CALLBACK_SEEK_FILE)
	{
		start_time = sPlayer_cb->getStartTime() ;
		if (start_time != -1)
		{
			ret = time(&current_time);
			if (ret == -1)
			{
				ret_value = 0;
			}
			else if ( (current_time - start_time) > TIMEOUT_SEEK_FILE)
			{
				//cause av_read_frame() to fail
				sPlayer_cb->setFFmpegInterrupt(true);
				ret_value = 1;
			}
		}
	}

	return ret_value;
}


MediaPlayer::MediaPlayer(bool forPlayback, bool forSharedCam)
{
	mListener = NULL;
	mFrame = NULL;
	mCurrentFrame = NULL;
	mMovieFile = NULL;
	mConvertCtx = NULL;
	mCookie = NULL;
	mDuration = 0;
	//mStreamType = MUSIC;
	mStartPosition = -1;
	mCurrentPosition = 0;
	mFirstClipStartTime = -1;
	mSeekPosition = -1;
	mCurrentState = MEDIA_PLAYER_IDLE;
	mDecoderAudio = NULL;
	mDecoderAudio1 = NULL;
	mDecoderVideo = NULL;
	mDecoderVideo1 = NULL;

	mPrepareSync = false;
	mPrepareStatus = NO_ERROR;
	pthread_mutex_init(&mLock, NULL);
	mVideoWidth = mVideoHeight = 0;
	sPlayer = this;
	ffmpegEngineInitialized = false;
	interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
	startTime = -1;
	shouldInterrupt = false;
	externalInterruptSignal = false;
	url = NULL;
	mPlayBackOnly = forPlayback;
	mForSharedCam = forSharedCam;
	mRtspWithTcp = false;
	firstKeyFrameDisplayed = false;

	//use for video timer
	mVideoTimerRunning = false;
	mReceiveDecodedFrame = false;
	isLoadingDialogShowing = false;

	//use for video bitrate task
	mVideoBirateRunning = true;
	mVideoBitrateThread = NULL;

	mRecorderAudio = NULL;
	mRecorderVideo = NULL;

	mRecordingThread = NULL;
	record_url = NULL;
	mStopRecording = true;
	snapBuf = NULL;
	isEncryptionEnable = false;
	encryption_key = NULL;
	encryption_iv = NULL;
	isAudioEnabled = true;
	ffmpeg_buffer_size_in_kb = -1;
	ffmpeg_buffer_size = 0;
	max_video_buf_size = 0;
	max_audio_buf_size = 0;
	mIsInPTZMode = false;
	decoded_frames = 0;
	shouldShowDuration = true;
	shouldUpdateDuration = true;
	shouldUseLibYUV = true;

	/* For RUDPClient. */
	mRmcClient = NULL;
	videoCtx = NULL;
	audioCtx = NULL;
	isInLocal = false;
	p2pSessionCount = 0;
	mRmcChannelInfo = NULL;

	/* For TUTK Client */
	//	mAVAPIsClient = NULL;
	mNumId = 0;

	mAccumulateDuration = 0;
	mPrevDuration = 0;
	mCurrDuration = 0;
	mIsSdcardStreaming = false;
	mCurrTimestamp = 0;

	isRecordMode = false;
	mIsP2pPlayByTimestampEnabled = false;
	mIsBackgroundModeEnabled = false;
	mProbeSize = PROBE_SIZE_DEFAULT;
}

MediaPlayer::~MediaPlayer()
{
	cancelVideoBitrateTask();
	if (mVideoTimerRunning == true)
	{
		cancelVideoTimer();
	}
	sPlayer = NULL;
	pthread_mutex_destroy(&mLock);

	if(mListener != NULL) {
		delete(mListener);
		log_info("MediaPlayer Listener destroyed...\n");
		mListener = NULL;
	}

	if (mDecoderVideo != NULL)
	{
		delete(mDecoderVideo);
		mDecoderVideo = NULL;
	}
	if (mDecoderVideo1 != NULL)
	{
		delete(mDecoderVideo1);
		mDecoderVideo1 = NULL;
	}
	if (mRmcClient != NULL)
	{
		delete(mRmcClient);
		mRmcClient = NULL;
	}
	//	if (mAVAPIsClient != NULL)
	//	{
	//		delete mAVAPIsClient;
	//		mAVAPIsClient = NULL;
	//	}
	if (mDecoderAudio != NULL)
	{
		delete(mDecoderAudio);
		mDecoderAudio = NULL;
	}
	if (mDecoderAudio1 != NULL)
	{
		delete(mDecoderAudio1);
		mDecoderAudio1 = NULL;
	}

	if (mFrame)
	{
		av_frame_free(&mFrame);
	}

	//free snapshot variable
	if (snapBuf != NULL)
	{
		av_free(snapBuf);
		snapBuf = NULL;
	}
	if (mCurrentFrame != NULL)
	{
		av_frame_free(&mCurrentFrame);
	}

	if (mMovieFile != NULL)
	{
		avformat_close_input(&mMovieFile);
		mMovieFile = NULL;
	}

	if (encryption_key != NULL)
	{
		free(encryption_key);
		encryption_key = NULL;
	}
	if (encryption_iv != NULL)
	{
		free(encryption_iv);
		encryption_iv = NULL;
	}

	// free p2p variable
	if (videoCtx != NULL)
	{
		av_free(videoCtx);
		videoCtx = NULL;
	}
	if (audioCtx != NULL)
	{
		av_free(audioCtx);
		audioCtx = NULL;
	}

	if (mRmcChannelInfo != NULL) {
//	  free(mRmcChannelInfo);
	  mRmcChannelInfo = NULL;
	}
	if (p2pSessionCount > 0)
	{
//		for (int i=0; i<p2pSessionCount; i++)
//		{
//			if (stunConnInfo[i] != NULL)
//			{
//				if (stunConnInfo[i]->src_ip != NULL)
//				{
//					free(stunConnInfo[i]->src_ip);
//					stunConnInfo[i]->src_ip = NULL;
//				}
//				free(stunConnInfo[i]);
//				stunConnInfo[i] = NULL;
//			}
//		}
		p2pSessionCount = 0;
	}

	if (mNumId > 0)
	{
		for (int i=0; i<mNumId; i++)
		{
			free(UID[i]);
		}
	}

	sws_freeContext(mConvertCtx);
	log_info("MediaPlayer destroyed...\n");
}

int MediaPlayer::getCurrentState()
{
	return mCurrentState;
}

time_t MediaPlayer::getStartTime()
{
	return startTime;
}

void MediaPlayer::setStartTime(time_t start_time)
{
	startTime = start_time;
}

interrupt_callback_id MediaPlayer::getInterruptCallbackId()
{
	return interrupt_cb_id;
}

void MediaPlayer::setInterruptCallbackId(interrupt_callback_id interrupt_cb_id)
{
	this->interrupt_cb_id = interrupt_cb_id;
}

void MediaPlayer::setFFmpegInterrupt(bool interrupt)
{
	this->shouldInterrupt = interrupt;
}


void* MediaPlayer::videoBitrateTask(void* ptr)
{
	if(attachVmToThread () != JNI_OK)
	{
		log_info("Attach VM to thread VideoBitrate failed");
		return NULL;
	}

	int currentBitrate = 0;
	int currentRmcBandwidth = 0;
	while (sPlayer->mVideoBirateRunning)
	{
		if (sPlayer->mRmcClient != NULL)
		{
			currentBitrate = sPlayer->mRmcClient->getCurrentVideoBitrate();
			currentRmcBandwidth = sPlayer->mRmcClient->getCurrentRmcBandwidth();
//			log_info("Current video bitrate: %d, rmc bandwidth %d\n", currentBitrate*8/1000, currentRmcBandwidth*8/1000);
		}
		//		if (sPlayer->mAVAPIsClient != NULL)
		//		{
		//			currentBitrate = sPlayer->mAVAPIsClient->getCurrentVideoBitrate();
		//		}

		sPlayer->notify(MEDIA_INFO_BITRATE_BPS, currentBitrate, currentRmcBandwidth);
		/*
		 * Need to split sleep time into smaller sleep
		 */
		int count = 10;
		while (count-- > 0 && sPlayer->mVideoBirateRunning)
		{
		  usleep(100000);
		}
	}

	dettachVmFromThread();
	log_info("Dettach thread VideoBirate");

	return NULL;
}

void MediaPlayer::cancelVideoBitrateTask()
{
	log_info("Cancel video bitrate task...\n");
	mVideoBirateRunning = false;
	pthread_join(mVideoBitrateThread, NULL);
}

void* MediaPlayer::videoTimerTask(void* ptr)
{
	if(attachVmToThread () != JNI_OK)
	{
		log_info("Attach VM to thread VideoTimer Failed");
	}

	while (sPlayer->mVideoTimerRunning)
	{
		timeval pTime;
		static double t1 = -1;
		static double t2 = -1;

		//only start timer when first key frame is displayed
		if (sPlayer->firstKeyFrameDisplayed == true)
		{
			gettimeofday(&pTime, NULL);
			if (sPlayer->mReceiveDecodedFrame == true)
			{
				//received video frame --> refresh timer, dismiss progress dialog
				t1 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
				t2 = -1;
				if (sPlayer->isLoadingDialogShowing == true)
				{
				  long delta_delay = av_gettime() - sPlayer->mVideoTimeoutStart;
				  if (sPlayer->mDecoderVideo != NULL) {
				    //log_info("Increase accumulate video delay: %ld\n", delta_delay);
				    sPlayer->mDecoderVideo->increaseAccumulateTimeout(delta_delay);
				  }
					sPlayer->notify(MEDIA_INFO_RECEIVED_VIDEO_FRAME, -1, -1);
					sPlayer->isLoadingDialogShowing = false;
				}
				sPlayer->mReceiveDecodedFrame = false;
			}
			else
			{
				t2 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
				if (t1 != -1 && t2 > t1 + CORRUPT_FRAME_TIMEOUT)
				{
					if (sPlayer->isLoadingDialogShowing == false)
					{
					  sPlayer->mVideoTimeoutStart = av_gettime();
						sPlayer->notify(MEDIA_INFO_CORRUPT_FRAME_TIMEOUT, -1, -1);
						sPlayer->isLoadingDialogShowing = true;
					}
				}

			}
		} //if (sPlayer->firstKeyFrameDisplayed == true)

		/*
		 * Need to split sleep time into smaller sleep
		 */
		int count = 10;
		while (count-- > 0 && sPlayer->mVideoTimerRunning)
		{
		  usleep(100000);
		}
	}

	dettachVmFromThread();
	log_info("Dettach thread VideoTimer");

	return NULL;
}

void MediaPlayer::cancelVideoTimer()
{
	log_info("Cancel video timer...\n");
	mVideoTimerRunning = false;
	pthread_join(mVideoTimerThread, NULL);
}

status_t MediaPlayer::initFFmpegEngine()
{
	//FFMPEG INIT code

	avcodec_register_all();
	avfilter_register_all();
	av_register_all();
	avformat_network_init();

	ffmpegEngineInitialized = true;

	return NO_ERROR;
}

status_t MediaPlayer::prepareAudio(JNIEnv *env, jobject thiz)
{
	log_info("prepareAudio\n");
	mAudioStreamIndex = -1;

	if (isAudioEnabled == false)
	{
		log_info("Audio is disabled, don't need to prepare audio context.\n");
		return NO_ERROR;
	}

	for (int i = 0; i < mMovieFile->nb_streams; i++) {
		if (mMovieFile->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
			mAudioStreamIndex = i;
			break;
		}
	}

	if (externalInterruptSignal == true)
	{
		return INVALID_OPERATION;
	}

	if (mAudioStreamIndex == -1) {

		log_info("prepareAudio mAudioStreamIndex==-1 mute audio \n");
		//mAudioStreamIndex -- could be -1 .. but prepare OK
		//simply the stream does not have audio -- return OK here

		//return INVALID_OPERATION;
		return  NO_ERROR;
	}

	AVStream* stream = mMovieFile->streams[mAudioStreamIndex];
	// Get a pointer to the codec context for the video stream
	AVCodecContext* codec_ctx = stream->codec;
	AVCodec* codec = avcodec_find_decoder(codec_ctx->codec_id);
	if (codec == NULL) {
		log_info("prepareAudio Could not find audio codec. Maybe the stream has no audio\n");
		//HACK: disable audio if cannot find decoder
		mAudioStreamIndex = -1;
		return NO_ERROR;
	}

	if (av_get_channel_layout_nb_channels(codec_ctx->channel_layout) == 0)
	{
		codec_ctx->channel_layout =
				av_get_default_channel_layout(codec_ctx->channels);
	}

	if (p2pSessionCount > 0) {
		log_info("Set default audio context parameters for P2P\n");
		codec_ctx->sample_rate = 8000;
		codec_ctx->sample_fmt = AV_SAMPLE_FMT_S16;
		codec_ctx->channels = 1;
		codec_ctx->channel_layout = AV_CH_LAYOUT_MONO;
		codec_ctx->time_base = (AVRational) {1, codec_ctx->sample_rate};
	}

	char codec_opt[20];
	AVDictionary * opts = NULL;
	//AVDictionary * opts1; --> Caused segmentation Fault // Same line in prepareVideo() is fine
	//av_dict_set(&opts1, "threads", "auto", 0);
	// Open codec
	//if (avcodec_open(codec_ctx, codec) < 0) { //avcodec_open is no longer supported in ffmpeg 2.0
	if (avcodec_open2(codec_ctx, codec, &opts)) {
		//if (avcodec_open2(codec_ctx, codec, &opts1)< 0) {
		log_info("prepareAudio 03\n");
		return INVALID_OPERATION;
	}

	log_info("prepareAudio DONE codecID: %s\n", avcodec_get_name(codec_ctx->codec_id));
	log_info("prepareAudio DONE st_tb: %f, codec_tb %f\n", av_q2d(stream->time_base), av_q2d(stream->codec->time_base));

	int sampleRate = 8000;
	//int sampleRate = 44100;
	//  public static final int CHANNEL_CONFIGURATION_MONO = 2;
	//  public static final int CHANNEL_CONFIGURATION_STEREO = 3;
	int num_channels = 4; //MONO
	//int num_channels = 12; //STEREO

	//Start java pcm player here
#if ANDROID_BUILD

	jclass clazz;
	clazz = env->FindClass("com/media/ffmpeg/FFMpegPlayer");

	jmethodID native_startPcmPlayer = env->GetMethodID(clazz, "native_startPCMPlayer", "(II)V");
	if (native_startPcmPlayer == 0)
	{
		log_info("Cant' find java void native_startPCMPlayer(int sampleRate, int channels)");
		return -1;
	}

	env->CallVoidMethod(thiz, native_startPcmPlayer, sampleRate, num_channels);
#endif

	return NO_ERROR;
}

status_t MediaPlayer::prepareVideo()
{
	log_info("prepareVideo\n");
	// Find the first video stream
	mVideoStreamIndex = -1;
	for (int i = 0; i < mMovieFile->nb_streams; i++) {
		if (mMovieFile->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			mVideoStreamIndex = i;
			break;
		}
	}

	if (externalInterruptSignal == true)
	{
		return INVALID_OPERATION;
	}

	if (mVideoStreamIndex == -1)
	{
		log_info("prepareVideo mVideoStreamIndex==-1 no video \n");
		//return INVALID_OPERATION;
		return NO_ERROR;
	}

	AVStream* stream = mMovieFile->streams[mVideoStreamIndex];
	// Get a pointer to the codec context for the video stream
	AVCodecContext* codec_ctx = stream->codec;
	AVCodec* codec = avcodec_find_decoder(codec_ctx->codec_id);
	if (codec == NULL) {
		return INVALID_OPERATION;
	}

	if (p2pSessionCount > 0) {
		// For P2P, use default video context parameters
		log_info("Set default video context parameters for P2P\n");
		codec_ctx->width = 1280;
		codec_ctx->height = 720;
		codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
		codec_ctx->time_base = (AVRational) {1, 15};
	}

	char codec_opt[20];
	AVDictionary * opts = NULL;
	av_dict_set(&opts, "threads", "auto", 0);
	//av_dict_set(&opts, "threads", "1", 0);
	//av_dict_set(&opts, "thread_type", "slice", 0);
	//av_dict_set(&opts, "thread_type", "frame", 0);

	//set error recognition flag
	/* Use err_detect level too high could cause the video context could be never reinitialized.
	 * see FCL-120.
	 * Solution: use default error detect level. */
	//		int err_detect_flag = AV_EF_EXPLODE;
	//		sprintf(codec_opt, "%d", err_detect_flag);
	//		av_dict_set(&opts, "err_detect", codec_opt, 0);

	//set debug flags: all
	int debug_flag = FF_DEBUG_ER;
	sprintf(codec_opt, "%d", debug_flag);
	av_dict_set(&opts, "debug", codec_opt, 0);

	// Open codec
	//if (avcodec_open(codec_ctx, codec) < 0) {
	if (avcodec_open2(codec_ctx, codec, &opts) < 0) {
		log_info("avcodec_open2: failed\n");
		return INVALID_OPERATION;
	}

	log_info("prepareVideo DONE st_tb: %f, codec_tb %f\n", av_q2d(stream->time_base), av_q2d(stream->codec->time_base));

#if 0 //DEBUG
	log_info("##### error concealment: %d\n",
			codec_ctx->error_concealment);
	log_info("##### error recognition: %d\n",
			codec_ctx->err_recognition);
	log_info("##### debug flag: %d\n", codec_ctx->debug);
	log_info("##### thread type: %d, thread count: %d\n",
			codec_ctx->thread_type, codec_ctx->thread_count);
#endif

	//20130423: SDP/RTP stream: hack: if pix_fmt is NULL here (for some reason it could not be determined)
	//        assign it to yuv420p
	if (stream->codec->pix_fmt == AV_PIX_FMT_NONE)
	{
		log_info("prepareVideo HACK: fixed pix format to yuv420p\n");
		stream->codec->pix_fmt = AV_PIX_FMT_YUV420P;

	}

#if 1
	if (stream->codec->height == 0 || stream->codec->width == 0 )
	{
		//return INVALID_OPERATION;

		log_info("prepareVideo HACK: failed to read codec width & height hack for now \n");

		if (stream->codec->width == 0 )
		{
			stream->codec->width =  160; // HACK FOR NOW
		}

		if (stream->codec->height == 0 )
		{
			stream->codec->height = 120 ; // HaCK For now
		}

	}
#endif

	mVideoWidth = codec_ctx->width;
	mVideoHeight = codec_ctx->height;
	notify(MEDIA_INFO_VIDEO_SIZE, mVideoWidth, mVideoHeight);

	log_info("src width: %d, src height: %d, src pix_fmt: %s\n",
			stream->codec->width, stream->codec->height, av_get_pix_fmt_name(stream->codec->pix_fmt));
	log_info("dst width: %d, dst height: %d, dst pix_fmt: %s\n",
			stream->codec->width, stream->codec->height, av_get_pix_fmt_name(AV_PIX_FMT_ARGB));

	mConvertCtx = sws_getContext(
			stream->codec->width,
			stream->codec->height,
			stream->codec->pix_fmt,
			stream->codec->width,
			stream->codec->height,
			AV_PIX_FMT_ARGB,
			sws_flags,
			NULL,
			NULL,
			NULL);

	if (mConvertCtx == NULL) {
		return INVALID_OPERATION;
	}

	void*		pixels;
	int         size;
#if 1

///// &pixels is pointed to a Native bitmap.
	if (Output::VideoDriver_getPixels(
			stream->codec->width,
			stream->codec->height,
			&pixels) != 0) {
		return INVALID_OPERATION;
	}


#else

	/* create temporary picture */
	size = avpicture_get_size(PIX_FMT_ARGB, stream->codec->width, stream->codec->height);
	pixels  = av_malloc(size);
	if (!pixels)
		return INVALID_OPERATION;

#endif

	//prepare for snapshot
	int snapSize = avpicture_get_size(AV_PIX_FMT_ARGB, stream->codec->width, stream->codec->height);
	snapBuf  = (uint8_t*) av_malloc(snapSize);
	mCurrentFrame = av_frame_alloc();
	avpicture_fill((AVPicture *) mCurrentFrame,
			(uint8_t *) snapBuf,
			AV_PIX_FMT_ARGB,
			stream->codec->width,
			stream->codec->height);

	mFrame = av_frame_alloc();
	if (mFrame == NULL) {
		return INVALID_OPERATION;
	}
	// Assign appropriate parts of buffer to image planes in pFrameRGB
	// Note that pFrameRGB is an AVFrame, but AVFrame is a superclass
	// of AVPicture
	avpicture_fill((AVPicture *) mFrame,
			(uint8_t *) pixels,
			AV_PIX_FMT_ARGB,
			stream->codec->width,
			stream->codec->height);

	return NO_ERROR;
}

status_t MediaPlayer::prepare(JNIEnv *env, jobject thiz)
{
	status_t ret;
	mCurrentState = MEDIA_PLAYER_PREPARING;

	//20130412: dont overwrite ffmpeg default log callback, we have put our root in there
	//av_log_set_callback(ffmpegNotify);

	if ((ret = prepareVideo()) != NO_ERROR)
	{
		mCurrentState = MEDIA_PLAYER_STATE_ERROR;
		if (mVideoStreamIndex != -1)
		{
			avcodec_close(mMovieFile->streams[mVideoStreamIndex]->codec);
		}
		avformat_close_input(&mMovieFile);
		mMovieFile = NULL;
		return ret;
	}

	if ((ret = prepareAudio(env, thiz)) != NO_ERROR)
	{
		mCurrentState = MEDIA_PLAYER_STATE_ERROR;
		if (mVideoStreamIndex != -1)
		{
			avcodec_close(mMovieFile->streams[mVideoStreamIndex]->codec);
		}
		if (mAudioStreamIndex != -1)
		{
			avcodec_close(mMovieFile->streams[mAudioStreamIndex]->codec);
		}
		avformat_close_input(&mMovieFile);
		mMovieFile = NULL;
		return ret;
	}

	mRecorderAudio = NULL;
	mRecorderVideo = NULL;

	mCurrentState = MEDIA_PLAYER_PREPARED;
	return NO_ERROR;
}

status_t MediaPlayer::setListener(MediaPlayerListener* listener)
{
	log_info("setListener");
	mListener = listener;
	return NO_ERROR;
}

MediaPlayerListener* MediaPlayer::getListener()
{
	log_info("getListener");
	return this->mListener;
}


status_t MediaPlayer::setDataSource(const char *url)
{
	log_info("setDataSource(%s) with avformat_open_input",  url);
	status_t err = BAD_VALUE;
	interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
	startTime = -1;
	shouldInterrupt = false;

	//* try to init the ffMpeg engine if not done
	if (ffmpegEngineInitialized == false)
	{
		initFFmpegEngine();
	}

	av_log_set_level(AV_LOG_INFO);

	log_info("setDataSource: p2pSessionCount %d\n", p2pSessionCount);
	if (p2pSessionCount <= 0)
	{
		log_info("avformat alloc context ");
		//20121015:phung: need to allocate the AVFormatContext first
		mMovieFile = avformat_alloc_context();
		if (!mMovieFile)
		{
			log_error("memory error ");
			return INVALID_OPERATION;

		}



		//20130328: set probesize to 100K - otherwise for single stream (i.e. Video Only stream)
		//           ffmpeg will take forever to decide the stream info
		//mMovieFile->probesize = 100*1024;

		log_info("av_open input ");
		// Open video file
		mMovieFile->interrupt_callback.callback = should_abort_cb;
		mMovieFile->interrupt_callback.opaque = sPlayer;
		interrupt_cb_id = INTERRUPT_CALLBACK_OPEN_INPUT;
		time(&startTime);

		char avfmt_opt[20];

		AVDictionary* options = NULL;
		if (mPlayBackOnly == false)
		{
			// Force TCP for rtsp transport
			if ( ( mRtspWithTcp == true) &&
					( strncmp(url, "rtsp", 4) ==  0 ) )
			{
				av_dict_set(&options,"rtsp_transport","2", 0);
			}
		}

		int err_detect = AV_EF_EXPLODE;
		sprintf(avfmt_opt, "%d", err_detect);
		av_dict_set(&options, "err_detect", avfmt_opt, 0);

		if (mProbeSize > 0) {
			log_info("Set probe size %ld\n", mProbeSize);
			sprintf(avfmt_opt, "%d", mProbeSize);
			av_dict_set(&options, "probesize", avfmt_opt, 0);
		}
		//av_dict_set(&options, "analyzeduration", "3000000", 0);

		int avfmt_flag;
		if (mPlayBackOnly == true)
		{
			avfmt_flag = AVFMT_FLAG_DISCARD_CORRUPT;
		}
		else
		{
			if (strncmp(url, "rtmp", 4) == 0) {
				avfmt_flag = AVFMT_FLAG_DISCARD_CORRUPT;
			} else {
				avfmt_flag = AVFMT_FLAG_DISCARD_CORRUPT | AVFMT_FLAG_NOBUFFER;
			}
		}
		sprintf(avfmt_opt, "%d", avfmt_flag);
		av_dict_set(&options, "fflags", avfmt_opt, 0);

		//av_dict_set(&options, "fdebug", "ts", 0);

		int ret;
		AVInputFormat* iFormat = NULL;
		if (mPlayBackOnly == true)
		{
			if (strstr(url, "h264") != NULL)
			{
				iFormat = av_find_input_format("h264");
			}
			else
			{
				iFormat = av_find_input_format("flv");
			}

			if (strncmp(url, "rtmp", 4) == 0) {
			  log_info("Sdcard streaming via rtmp detected!\n");
			  mIsSdcardStreaming = true;
			}
		}
		else if (strncmp(url, "rtmp", 4) == 0)
		{
			iFormat = av_find_input_format("flv");
			av_dict_set(&options, "rtmp_buffer", "0", 0);
		}
		else if (strncmp(url, "rtsp", 4) == 0)
		{
			iFormat = av_find_input_format("rtsp");
		}
		else
		{
			iFormat = av_find_input_format("h264");
		}

		if (iFormat != NULL)
		{
			log_info("input format set to: %s\n", iFormat->name);
		}

		// mMovieFile->avio_flags |= AVIO_FLAG_NONBLOCK;
		ret = avformat_open_input(&mMovieFile, url, iFormat, &options);
		if (ret != 0 || shouldInterrupt) {
			log_error("av_open_input error ret %d\n", ret);
			avformat_free_context(mMovieFile);
			mMovieFile = NULL;
			if (ret == AVERROR_STREAM_NOT_FOUND) {
			  return NAME_NOT_FOUND;
			} else {
			  return INVALID_OPERATION;
			}
		}
#if 0 //DEBUG
		log_info("@@@@@ avformat_context error_recognition: %d\n",
				mMovieFile->error_recognition);
		log_info("@@@@@ avformat_context flags: %d\n",
				mMovieFile->flags);
		log_info("@@@@@ avformat_context debug: %d\n",
				mMovieFile->debug);
#endif
		interrupt_cb_id = INTERRUPT_CALLBACK_OFF;

		log_info("av_find_stream_info ");
		// Retrieve stream information
		interrupt_cb_id = INTERRUPT_CALLBACK_FIND_STREAM_INFO;
		time(&startTime);

		AVDictionary **opts = NULL;
		opts = setup_find_stream_info_opts(mMovieFile);
		ret = avformat_find_stream_info(mMovieFile, opts);
		if (ret < 0 || shouldInterrupt)
		{
			log_error("av_find_stream_info error");
			avformat_close_input(&mMovieFile);
			mMovieFile = NULL;
			return INVALID_OPERATION;
		}

		/*
		 * 20170311 HOANG
		 * Fix crash due to "audio stream discovered after head already parsed".
		 * mMovieFile->nb_streams at before and after avformat_find_stream_info DONE.
		 * e.g. after avformat_open_input DONE -> nb_streams is 1
		 * 		after avformat_find_stream_info DONE -> nb_streams is 2
		 */
		if (opts != NULL)
		{
			int optLength = sizeof(opts) / sizeof(opts[0]);
			log_info("avformat_find_stream_info opt length: %d\n", optLength);
			for (int i=0; i < optLength; i++)
			{
				av_dict_free(&opts[i]);
			}
			av_freep(&opts);
		}

		interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
		if (shouldUpdateDuration)
		{
			mDuration =  mMovieFile->duration;
		}
		log_info("duration: %ld\n", mDuration / 1000);
		av_dump_format(mMovieFile, 0, url, 0);

		log_info("input format name: %s, playback only? %d\n",
				mMovieFile->iformat->name, mPlayBackOnly?1:0);
		if (mPlayBackOnly == true)
		{
			// Store curr clip duration
			mPrevDuration = mCurrDuration;
			mCurrDuration = mMovieFile->duration;
			notify(MEDIA_INFO_CLIP_URL, -1, -1);
		}

		if (strcmp(mMovieFile->iformat->name, "rtsp") == 0 )
		{
			RTSPState *rt = (RTSPState *) mMovieFile->priv_data;
			log_info("rt lower transport UDP : %d",
					(rt->lower_transport == RTSP_LOWER_TRANSPORT_UDP) );
			log_info("rt lower transport  TCP : %d",
					(rt->lower_transport == RTSP_LOWER_TRANSPORT_TCP) );

		}

	} // if (p2pSessionCount <= 0)
	else {
		// HOANG Create video & audio stream for P2P
		mMovieFile = avformat_alloc_context();
		if (!mMovieFile)
		{
			log_error("Could not allocate P2P input context");
			return INVALID_OPERATION;
		}

		const AVCodec *vcodec = avcodec_find_decoder(AV_CODEC_ID_H264);
		AVStream *vStream = avformat_new_stream(mMovieFile, vcodec);
		if (!vStream) {
			log_error("Could not allocate video stream\n");
			avformat_free_context(mMovieFile);
			mMovieFile = NULL;
			return INVALID_OPERATION;
		}
		vStream->time_base = (AVRational) {1, 1000};

		const AVCodec *acodec = avcodec_find_decoder(AV_CODEC_ID_PCM_ALAW);
		AVStream *aStream = avformat_new_stream(mMovieFile, acodec);
		if (!aStream) {
			log_error("Could not allocate audio stream\n");
			avformat_free_context(mMovieFile);
			mMovieFile = NULL;
			return INVALID_OPERATION;
		}
		aStream->time_base = (AVRational) {1, 1000};

//		log_info("Create input context for P2P DONE, video index %d, audio index %d\n",
//				mVideoStreamIndex, mAudioStreamIndex);

		// For p2p streaming via sdcard
		if (mPlayBackOnly == true)
		{
			log_info("Sdcard file streaming via p2p detected!\n");
			mIsSdcardStreaming = true;
		}
	}

	mCurrentState = MEDIA_PLAYER_INITIALIZED;
	return NO_ERROR;
}

AVDictionary** MediaPlayer::setup_find_stream_info_opts(AVFormatContext *s)
{
	AVDictionary **ret_opts;
	AVStream *st;
	AVCodecContext *avctx;

	if (s->nb_streams <= 0) {
		log_error("Setup find stream info opts, no stream found!\n");
		return NULL;
	}

	ret_opts = (AVDictionary **) av_mallocz_array(s->nb_streams, sizeof(*ret_opts));
	for (int i=0; i<s->nb_streams; i++)
	{
		st = s->streams[i];
		avctx = st->codec;
		switch (avctx->codec_type) {
		case AVMEDIA_TYPE_VIDEO:
			av_dict_set(&ret_opts[i], "vcodec", "h264", 0);
			av_dict_set(&ret_opts[i], "pixel_format", "yuv420p", 0);
			if (mForSharedCam == true)
			{
				av_dict_set(&ret_opts[i], "video_size", "320x240", 0);
			}
			else
			{
				av_dict_set(&ret_opts[i], "video_size", "1280x720", 0);
			}
			//			st->r_frame_rate.num = 15;
			//			st->r_frame_rate.den = 1;
			break;
		case AVMEDIA_TYPE_AUDIO:
			if (mPlayBackOnly == false)
			{
				if (mForSharedCam == false)
				{
					av_dict_set(&ret_opts[i], "acodec", "pcm_alaw", 0);
					av_dict_set(&ret_opts[i], "ar", "8000", 0);
				}
				else
				{
					if (strncmp(s->filename, "rtmp", 4) == 0)
					{
						av_dict_set(&ret_opts[i], "acodec", "adpcm_swf", 0);
						av_dict_set(&ret_opts[i], "ar", "11025", 0);
					}
					else
					{
						av_dict_set(&ret_opts[i], "acodec", "pcm_mulaw", 0);
						av_dict_set(&ret_opts[i], "ar", "8000", 0);
					}
				}
			} //if (mPlayBackOnly == false)
			av_dict_set(&ret_opts[i], "ac", "1", 0);
			av_dict_set(&ret_opts[i], "sample_fmt", "s16", 0);
			//av_dict_set(&opts[i], "channel_layout", "mono", 0);

			if (isEncryptionEnable == true)
			{
				//enable h264 encryption
				log_info(
						"Stream is encrypted, set find stream info audio key.\n");
				av_dict_set(&ret_opts[i], "stream_is_encrypted", "1", 0);
				av_dict_set(&ret_opts[i], "adecryptionkey", encryption_key, 0);
			}

			break;
		default:
			break;
		}
	}

	return ret_opts;
}

bool MediaPlayer::getExternalInterruptFlag()
{
	return externalInterruptSignal;
}

status_t MediaPlayer::suspend() {
	log_info("suspending...");
	externalInterruptSignal = true;
	/*
     - player is already starting :  current code is fine
     - player is prepared but not started yet : set to STOPPED -> in the decodeMovie()
     it will just come out.
	 */
	log_error("suspended");

	return NO_ERROR;
}

status_t MediaPlayer::resume()
{
	//pthread_mutex_lock(&mLock);
	int64_t timeDeltaPause = 0;
	if (timeStartPause != 0)
	{
		timeDeltaPause = (av_gettime() - timeStartPause);
		timeTotalPause += timeDeltaPause;
	}
	resumeDecoders();
	mCurrentState = MEDIA_PLAYER_STARTED;
	//pthread_mutex_unlock(&mLock);
	return NO_ERROR;
}

status_t MediaPlayer::setVideoSurface(JNIEnv* env, jobject mediaPlayerClass, jobject jsurface)
{
	if(Output::VideoDriver_register(env, mediaPlayerClass, jsurface) != 0) {
		return INVALID_OPERATION;
	}
	return NO_ERROR;
}

status_t MediaPlayer::resetVideoSurface(JNIEnv* env, jobject mediaPlayerClass, jobject jsurface)
{
  if(Output::VideoDriver_register(env, mediaPlayerClass, jsurface) != 0) {
    return INVALID_OPERATION;
  }

  firstKeyFrameDisplayed = false;
  reconfigureVideo();

  return NO_ERROR;
}

status_t MediaPlayer::setAudioBufferCallBack(JNIEnv* env, jobject mediaPlayerClass)
{
	if(Output::AudioDriver_register(env, mediaPlayerClass) != 0) {
		return INVALID_OPERATION;
	}
	return NO_ERROR;
}

bool MediaPlayer::shouldCancel(PacketQueue* queue)
{
	return (mCurrentState == MEDIA_PLAYER_STATE_ERROR || mCurrentState == MEDIA_PLAYER_STOPPED ||
			((mCurrentState == MEDIA_PLAYER_DECODED || mCurrentState == MEDIA_PLAYER_STARTED)
					&& queue->size() == 0));
}

status_t MediaPlayer::reconfigureVideo()
{
	if (mMovieFile != NULL)
	{
		/* Not P2P */
		AVStream* stream = mMovieFile->streams[mVideoStreamIndex];
		// Get a pointer to the codec context for the video stream
		AVCodecContext* codec_ctx = stream->codec;

		mVideoWidth = codec_ctx->width;
		mVideoHeight = codec_ctx->height;

		log_info("reconfigureVideo\n");
		log_info("src width: %d, src height: %d, src pix_fmt: %s\n",
				stream->codec->width, stream->codec->height, av_get_pix_fmt_name(stream->codec->pix_fmt));
		log_info("dst width: %d, dst height: %d, dst pix_fmt: %s\n",
				stream->codec->width, stream->codec->height, av_get_pix_fmt_name(AV_PIX_FMT_ARGB));
		mConvertCtx = sws_getCachedContext(
				mConvertCtx,
				stream->codec->width,
				stream->codec->height,
				stream->codec->pix_fmt,
				stream->codec->width,
				stream->codec->height,
				AV_PIX_FMT_ARGB,
				sws_flags,
				NULL,
				NULL,
				NULL);

		if (mConvertCtx == NULL)
		{
			return INVALID_OPERATION;
		}

		//prepare for snapshot
		int snapSize = avpicture_get_size(AV_PIX_FMT_ARGB, stream->codec->width, stream->codec->height);
		snapBuf  = (uint8_t*) av_realloc(snapBuf, snapSize);
		avpicture_fill((AVPicture *) mCurrentFrame,
				(uint8_t *) snapBuf,
				AV_PIX_FMT_ARGB,
				stream->codec->width,
				stream->codec->height);

		void*		pixels;

		if (Output::VideoDriver_getPixels(
				stream->codec->width,
				stream->codec->height,
				&pixels) != 0) {
			return INVALID_OPERATION;
		}

		avpicture_fill((AVPicture *) mFrame,
				(uint8_t *) pixels,
				AV_PIX_FMT_ARGB,
				stream->codec->width,
				stream->codec->height);
	}
//	else
//	{
//		/* For P2P context. */
//		if (videoCtx == NULL)
//		{
//			log_info("video codec ctx is null\n");
//			return INVALID_OPERATION;
//		}
//		mVideoWidth = videoCtx->width;
//		mVideoHeight = videoCtx->height;
//		log_info("reconfigureVideo\n");
//		log_info("src width: %d, src height: %d, src pix_fmt: %s\n",
//				videoCtx->width, videoCtx->height, av_get_pix_fmt_name(videoCtx->pix_fmt));
//		log_info("dst width: %d, dst height: %d, dst pix_fmt: %s\n",
//				videoCtx->width, videoCtx->height, av_get_pix_fmt_name(AV_PIX_FMT_ARGB));
//		notify(MEDIA_INFO_VIDEO_SIZE, mVideoWidth, mVideoHeight);
//
//		mConvertCtx = sws_getContext(
//				videoCtx->width, videoCtx->height,videoCtx->pix_fmt,
//				videoCtx->width,videoCtx->height, AV_PIX_FMT_ARGB,
//				sws_flags,
//				NULL,
//				NULL,
//				NULL);
//
//		void*		pixels;
//		///// &pixels is pointed to a Native bitmap.
//		if (Output::VideoDriver_getPixels(
//				videoCtx->width,
//				videoCtx->height,
//				&pixels) != 0) {
//			log_info("reconfigureVideo getPixels failed\n");
//		}
//
//		avpicture_fill((AVPicture *) mFrame,
//				(uint8_t *) pixels,
//				AV_PIX_FMT_ARGB,
//				videoCtx->width,
//				videoCtx->height);
//
//		//prepare for snapshot
//		int snapSize = avpicture_get_size(AV_PIX_FMT_ARGB, videoCtx->width, videoCtx->height);
//		snapBuf  = (uint8_t*) av_realloc(snapBuf, snapSize);
//		avpicture_fill((AVPicture *) mCurrentFrame,
//				(uint8_t *) snapBuf,
//				AV_PIX_FMT_ARGB,
//				videoCtx->width,
//				videoCtx->height);
//	}

	return 0;
}

bool MediaPlayer::isPlaybackMode()
{
	return mPlayBackOnly;
}

void MediaPlayer::decode(AVFrame* frame, double pts)
{
	if (sPlayer == NULL || sPlayer->externalInterruptSignal == true)
	{
		return;
	}

	int64_t pkt_pts = (int64_t) (pts * AV_TIME_BASE);

	if (sPlayer->mPlayBackOnly == true)
	{
		/*
		 * Remember that first packet pts does not always start from 0.
		 * So we can't compare it to 0. Need a more complicated algorithm to detect timestamp reset.
		 * First timestamp is usually in 0..1
		 */
		//if (pkt_pts == 0)
		if (pts == 0 || pts < 1)
		{
			if (sPlayer->shouldUpdateDuration) {
				/* Timestamp reset detected, update accumulate duration */
				if (sPlayer->mPrevDuration != 0)
				{
					sPlayer->mAccumulateDuration += sPlayer->mPrevDuration;
					log_info(
							"MediaPlayer update accumulate duration %d, prev %d\n",
							sPlayer->mAccumulateDuration, sPlayer->mPrevDuration);
					sPlayer->mPrevDuration = 0;
				}
			}
		}

		if (sPlayer->mIsSdcardStreaming == true)
		{
		  sPlayer->mCurrTimestamp = pts * 1000000; // in microseconds
		}
	}

	bool isDecoderVideoRunning = (sPlayer->mDecoderVideo != NULL && !sPlayer->mDecoderVideo->isPaused()) ||
			(sPlayer->mDecoderVideo1 != NULL && !sPlayer->mDecoderVideo1->isPaused());
	if (sPlayer->mCurrentState != MEDIA_PLAYER_SEEKING && isDecoderVideoRunning)
	{
//		sPlayer->mCurrentPosition = (int) ((pkt_pts - sPlayer->mFirstClipStartTime) / 1000);
		if (sPlayer->mRmcClient != NULL) {
			/*
			 * 20160928: HOANG: calculate progress for P2P file streaming
			 */
			int64_t firstVideoTs = sPlayer->mRmcClient->getFirstVideoTs();
//			log_info("Update currentPosition: pts %0.2f, first ts %ld\n", pts, firstVideoTs);
			if (firstVideoTs > -1) {
				sPlayer->mCurrentPosition = (int) (pts * 1000 - firstVideoTs);
			} else {
				sPlayer->mCurrentPosition = 0;
			}
		} else {
			sPlayer->mCurrentPosition = (int) ((sPlayer->mAccumulateDuration + pkt_pts - sPlayer->mFirstClipStartTime) / 1000);
		}
	}
//	log_info("mCurrentPosition: %d, video clock: %lld, mFirstClipStartTime: %lld",
//			sPlayer->mCurrentPosition, pkt_pts, sPlayer->mFirstClipStartTime);
//	log_error("mCurrentPosition: %d, video clock: %f, mFirstClipStartTime: %lld",
//			sPlayer->mCurrentPosition, pts, sPlayer->mFirstClipStartTime);
	sPlayer->decoded_frames++;
	if (sPlayer->decoded_frames > FFMPEG_PLAYER_MAX_QUEUE_SIZE)
	{
		sPlayer->decoded_frames = 0;
		sPlayer->mIsInPTZMode = false;
	}

	if (sPlayer->firstKeyFrameDisplayed == false && frame->key_frame == 1)
	{
		//detect first key frame
		sPlayer->firstKeyFrameDisplayed = true;
		sPlayer->notify(MEDIA_VIDEO_STREAM_HAS_STARTED, 0, 0);

		/* In player mode: pause decoders after receive first key frame.
		 * In playback mode: We only pause & resume decoders when buffering. */
		if (sPlayer->isPlaybackMode() == false &&
				sPlayer->mDecoderVideo != NULL &&
				!sPlayer->mDecoderVideo->isPaused() &&
				sPlayer->isPlaying() &&
				!sPlayer->externalInterruptSignal)
		{
			if (sPlayer->ffmpeg_buffer_size > 0)
			{
				if (sPlayer->mDecoderAudio != NULL)
				{
					sPlayer->mDecoderAudio->pause();
				}
				sPlayer->mDecoderVideo->pause();
			}
		}
	}

	if (sPlayer->firstKeyFrameDisplayed == true)
	{
		sPlayer->mReceiveDecodedFrame = true;
		/*
		 * 20130917: hoang: hack for support change resolution on the fly
		 */
		//reconfigure video: SwsContext, pictureRGB - pictureBuffer, mFrame
		if (sPlayer->mVideoWidth != frame->width ||
				sPlayer->mVideoHeight != frame->height)
		{
			//notify video size change
			sPlayer->notify(MEDIA_INFO_VIDEO_SIZE, frame->width, frame->height);
			//resolution has changed --> reconfigure video
			sPlayer->reconfigureVideo();
		}

		if(FPS_DEBUGGING) {
			timeval pTime;
			static int frames = 0;
			static double t1 = -1;
			static double t2 = -1;

			gettimeofday(&pTime, NULL);
			t2 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
			if (t1 == -1 || t2 > t1 + 1) {
				sPlayer->notify(MEDIA_INFO_FRAMERATE_VIDEO, frames, -1);
				t1 = t2;
				frames = 0;
			}
			frames++;
		}

#if ANDROID_BUILD

		/* Store key frame for get snapshot feature. */
		if (sPlayer->mPlayBackOnly == false)
		{
			if (frame->key_frame)
			{
				if (sPlayer->shouldUseLibYUV == true)
				{
					libyuv::I420ToARGB(frame->data[0], frame->linesize[0], frame->data[2],
							frame->linesize[2], frame->data[1], frame->linesize[1],
							sPlayer->mCurrentFrame->data[0], sPlayer->mCurrentFrame->linesize[0], frame->width,
							frame->height);
				}
				else
				{
					sws_scale(sPlayer->mConvertCtx,
							frame->data,
							frame->linesize,
							0,
							frame->height,
							sPlayer->mCurrentFrame->data,
							sPlayer->mCurrentFrame->linesize);
				}
				sPlayer->mCurrentFrame->width = frame->width;
				sPlayer->mCurrentFrame->height = frame->height;
			}
		}

		// Convert the image from its native format to RGB
		int64_t start = av_gettime();
		int end;
		if (sPlayer->shouldUseLibYUV == true)
		{
			libyuv::I420ToARGB(frame->data[0], frame->linesize[0], frame->data[2],
					frame->linesize[2], frame->data[1], frame->linesize[1],
					sPlayer->mFrame->data[0], sPlayer->mFrame->linesize[0], frame->width,
					frame->height);
			end = av_gettime() - start;
			//			log_info("LibYUV, color space conversion cost %d.",(end/1000));
		}
		else
		{
			sws_scale(sPlayer->mConvertCtx,
					frame->data,
					frame->linesize,
					0,
					frame->height,
					sPlayer->mFrame->data,
					sPlayer->mFrame->linesize);
			end = av_gettime() - start;
			log_info("sws_scale, color space conversion cost %d.",(end/1000));
		}

		// now sPlayer->mFrame has the picture data --
		//   start processing it ..
		// (AVPicture *) mFrame

#endif

		if (sPlayer->isRecordMode == false) {
		  if (sPlayer->mIsBackgroundModeEnabled) {
		    //log_info("Offline mode is enabled, don't need to render frame");
		  } else {
		    /* Render the decoded frame to screen. */
		    Output::VideoDriver_updateSurface();
		  }
		} else {
#if DEBUG_RECORDING_MODE
		  log_info("Record mode is enabled, don't need to render frame");
#endif
		}

#if DEBUG_UPDATE_VIDEO_CLK
		log_info(
				"MediaPlayer update video clk %f\n", pts);
#endif
	}

}

//void MediaPlayer::decode(int16_t* buffer, int buffer_size)
void MediaPlayer::decode(uint8_t* buffer, int buffer_size, double pts)
{
	if(FPS_DEBUGGING) {
		timeval pTime;
		static int frames = 0;
		static double t1 = -1;
		static double t2 = -1;

		gettimeofday(&pTime, NULL);
		t2 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
		if (t1 == -1 || t2 > t1 + 1) {
			sPlayer->notify(MEDIA_INFO_FRAMERATE_AUDIO, frames, -1);
			t1 = t2;
			frames = 0;
		}
		frames++;
	}

	/* CAREFUL: this may affect to audio & video sync logic.
	 * For player: write audio data to PCMPlayer only if we have displayed
	 * the first key frame.
	 * For playback: don't need to wait first key frame displayed. */
	//	if (sPlayer->firstKeyFrameDisplayed == true ||
	//			sPlayer->mPlayBackOnly == true)
	int byte_written = Output::AudioDriver_write(buffer, buffer_size, pts);
	//	if(byte_written <= 0) {
	//		log_error("Couldn't write samples to audio track");
	//	}

}

status_t MediaPlayer::resetDataSource(const char *new_url)
{
	shouldInterrupt = false;
	log_info("Reset data source called\n");
	mMovieFile = avformat_alloc_context();
	if (!mMovieFile)
	{
		log_info("memory error ");
		return INVALID_OPERATION;
	}
	//20130328: set probesize to 100K - otherwise for single stream (i.e. Video Only stream)
	//           ffmpeg will take forever to decide the stream info
	mMovieFile->probesize =100*1024 ;

	// Open video file
	mMovieFile->interrupt_callback.callback = should_abort_cb;
	mMovieFile->interrupt_callback.opaque = sPlayer;
	interrupt_cb_id = INTERRUPT_CALLBACK_OPEN_INPUT;
	time(&startTime);
	log_info("avformat_open_input");

	AVInputFormat* iFormat = NULL;
	iFormat = av_find_input_format("flv");
	mMovieFile->avio_flags |= AVIO_FLAG_NONBLOCK;
	int ret = avformat_open_input(&mMovieFile, new_url, iFormat, NULL);
	if (ret != 0 || shouldInterrupt) {
		log_error("av_open error ");
		avformat_free_context(mMovieFile);
		mMovieFile = NULL;
		return INVALID_OPERATION;
	}
	shouldInterrupt = false;
	interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
	log_info("avformat_find_stream_info");
	// Retrieve stream information
	interrupt_cb_id = INTERRUPT_CALLBACK_FIND_STREAM_INFO;
	time(&startTime);
	AVDictionary **opts = NULL;
	opts = setup_find_stream_info_opts(mMovieFile);
	ret = avformat_find_stream_info(mMovieFile, opts);
	log_info("avformat_find_stream_info return: %d", ret);
	if (ret < 0 || shouldInterrupt)
	{
		log_error("avformat_find_stream_info error");
		//avformat_close_input(&mMovieFile);
		avformat_free_context(mMovieFile);
		mMovieFile = NULL;
		return INVALID_OPERATION;
	}

	/*
	 * 20170311 HOANG
	 * Fix crash due to "audio stream discovered after head already parsed".
	 * mMovieFile->nb_streams at before and after avformat_find_stream_info DONE.
	 * e.g. after avformat_open_input DONE -> nb_streams is 1
	 * 		after avformat_find_stream_info DONE -> nb_streams is 2
	 */
	if (opts != NULL)
	{
		int optLength = sizeof(opts) / sizeof(opts[0]);
		log_info("avformat_find_stream_info opt length: %d\n", optLength);
		for (int i=0; i < optLength; i++)
		{
			av_dict_free(&opts[i]);
		}
		av_freep(&opts);
	}

	shouldInterrupt = false;
	interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
	log_info("should update duration: %d\n", shouldUpdateDuration?1:0);
	if (shouldUpdateDuration)
	{
		mDuration += mMovieFile->duration;
	}
	log_info("clip duration: %d\n", mDuration);
	av_dump_format(mMovieFile, 0, new_url, 0);

	// Update curr clip duration
	mPrevDuration = mCurrDuration;
	mCurrDuration = mMovieFile->duration;
	log_info("prev duration: %d, curr duration %d\n",
			mPrevDuration, mCurrDuration);
	notify(MEDIA_INFO_CLIP_URL, -1, -1);

	return NO_ERROR;
}


//Recording functions ...
AVStream *  MediaPlayer::add_stream(AVFormatContext *oc, AVCodec **codec,
		enum AVCodecID codec_id)
{
	AVCodecContext *c;
	AVStream *st;

	/* find the encoder */
	*codec = avcodec_find_encoder(codec_id);
	if (!(*codec)) {
		fprintf(stderr, "Could not find encoder for '%s'\n",
				avcodec_get_name(codec_id));
		exit(1);
	}

	st = avformat_new_stream(oc, *codec);
	if (!st) {
		fprintf(stderr, "Could not allocate stream\n");
		exit(1);
	}
	st->id = oc->nb_streams-1;
	c = st->codec;

	switch ((*codec)->type) {
	case AVMEDIA_TYPE_AUDIO:
		c->sample_fmt  = AV_SAMPLE_FMT_S16;
		c->sample_rate = 11025;
		c->channels    = 1;
		c->channel_layout = av_get_default_channel_layout(c->channels);
		break;

	case AVMEDIA_TYPE_VIDEO:
		c->codec_id = codec_id;

		c->bit_rate = 400000;
		/* Resolution must be a multiple of two. */
		c->width    = 352;
		c->height   = 288;
		/* timebase: This is the fundamental unit of time (in seconds) in terms
		 * of which frame timestamps are represented. For fixed-fps content,
		 * timebase should be 1/framerate and timestamp increments should be
		 * identical to 1. */
		c->time_base.den = 15; // STREAM_FRAME_RATE;
		c->time_base.num = 1;
		c->gop_size      = 12; /* emit one intra frame every twelve frames at most */
		c->pix_fmt       = AV_PIX_FMT_YUV420P;
		if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
			/* just for testing, we also add B frames */
			c->max_b_frames = 2;
		}
		if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
			/* Needed to avoid using macroblocks in which some coeffs overflow.
			 * This does not happen with normal video, it just happens here as
			 * the motion of the chroma plane does not match the luma plane. */
			c->mb_decision = 2;
		}
		break;

	default:
		break;
	}

	/* Some formats want stream headers to be separate. */
	if (oc->oformat->flags & AVFMT_GLOBALHEADER)
		c->flags |= CODEC_FLAG_GLOBAL_HEADER;

	return st;
}

int  MediaPlayer::recordMovie(void* ptr)
{
	int status;
	AVPacket pPacket;
	const char *filename;
	AVOutputFormat *fmt;
	AVFormatContext *oc;
	AVStream  *video_st = NULL,*audio_st = NULL;

	double audio_time, video_time;

	filename = (const char *)ptr;

	log_info("Recorded file_path: %s", filename);


#ifdef ANDROID_BUILD
	// Attach to JAVA VM
	if(attachVmToThread () != JNI_OK)
	{
		log_info("Attach VM to thread Record  Failed");
	}
#endif

	/* allocate the output media context */
	avformat_alloc_output_context2(&oc, NULL, NULL, filename);
	if (!oc) {
		log_error("Could not deduce output format from file extension: using MPEG.\n");
		avformat_alloc_output_context2(&oc, NULL, "mpeg", filename);
	}
	if (!oc) {
		return -1;
	}
	fmt = oc->oformat;


	//Set up 2 queues here..
	mRecorderAudio = NULL;
	mRecorderVideo = NULL;
	if (mStopRecording == false) {
		if (mAudioStreamIndex != -1)
		{
			AVStream* src_audio_stream = mMovieFile->streams[mAudioStreamIndex];
			mRecorderAudio = new RecorderAudio(src_audio_stream);
			// Get a pointer to the codec context for the audio stream
			// also add an audio stream to @oc
			mRecorderAudio->prepare(oc, src_audio_stream);
		} else {
			LOGI(10, "Prepare audio recorder failed, no audio stream found\n");
		}

		if (mVideoStreamIndex != -1) {
			AVStream* src_video_stream = mMovieFile->streams[mVideoStreamIndex];
			mRecorderVideo = new RecorderVideo(src_video_stream);
			mRecorderVideo->prepare(oc, src_video_stream);
		} else {
			LOGI(10, "Prepare video recorder failed, no video stream found\n");
		}
	}

	av_dump_format(oc, 0, filename, 1);

	int ret = -1;


	/* open the output file, if needed */
	if (!(fmt->flags & AVFMT_NOFILE))
	{
		ret = avio_open(&oc->pb, filename, AVIO_FLAG_WRITE);
		if (ret < 0) {
			LOGI(10, "Could not open '%s': %s\n", filename,
					av_err2str(ret));
			return 1;
		}
	}


	LOGI(10, "Write Header \n");
	/* Write the stream header, if any. */
	ret = avformat_write_header(oc, NULL);
	if (ret < 0) {
		LOGI(10, "Error occurred when opening output file: %s\n",
				av_err2str(ret));
		return 1;
	}

	initRecorders();

	if (mRecorderAudio != NULL)
	{
		audio_st = mRecorderAudio->getAudioStream();
	}
	video_st = mRecorderVideo->getVideoStream();

	//Connect this queue to decodeMovie Thread - TODO

	LOGI(10, "Start record loop\n");

	if (mStopRecording == false)
	{
	  while (mStopRecording == false)
	  {
	    /* Compute current audio and video time. */
#ifdef USE_FFMPEG_3_0
	    audio_time = audio_st ? av_stream_get_end_pts(audio_st) * av_q2d(audio_st->time_base) : 0.0;
	    video_time = video_st ? av_stream_get_end_pts(video_st) * av_q2d(video_st->time_base) : 0.0;
#else
	    audio_time = audio_st ? audio_st->pts.val * av_q2d(audio_st->time_base) : 0.0;
	    video_time = video_st ? video_st->pts.val * av_q2d(video_st->time_base) : 0.0;
#endif

	    //update recording time every 1s
	    timeval pTime;
	    static double t1 = -1;
	    static double t2 = -1;
	    static int last_time = -1;

	    gettimeofday(&pTime, NULL);
	    t2 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
	    //if (t1 == -1 || t2 > t1 + 1)
	    if (audio_st != NULL)
	    {
	      if (last_time == -1 || last_time != (int) audio_time)
	      {
#if DEBUG_RECORDING
	    	  LOGI(10, "Update recording time: video_time %f, audio_time %f\n", audio_time, video_time);
#endif
	    	  last_time = (int) audio_time;
	    	  //sPlayer->notify(MEDIA_INFO_RECORDING_TIME, record_time, -1);
	    	  sPlayer->notify(MEDIA_INFO_RECORDING_TIME,(int) audio_time, -1);
	    	  t1 = t2;
	      }
	    }
	    else
	    {
	      /* No audio, update recording time based on video time. */
	      if (last_time == -1 || last_time != (int) video_time)
	      {
#if DEBUG_RECORDING
	    	  LOGI(10, "Update recording time: video_time %f\n", video_time);
#endif
	        last_time = (int) video_time;
	        //sPlayer->notify(MEDIA_INFO_RECORDING_TIME, record_time, -1);
	        sPlayer->notify(MEDIA_INFO_RECORDING_TIME,(int) video_time, -1);
	        t1 = t2;
	      }
	    }


	    /* write interleaved audio and video frames */

	    /* at the start we give priority to video frame as we can wait for the Key frame to start
           recording.

           During this time, the audio may start to queue up. Need some ways to clear up excess data.
	     */
//	    LOGI(10, "Update recording time: video_time %f, audio_time %f\n", audio_time, video_time);
	    if (audio_st != NULL)
	    {
	      if (!video_st || (video_st && audio_st && audio_time < video_time))
	      {
	        mRecorderAudio->write_audio_frame4(oc);
	      }
	      else
	      {
	        mRecorderVideo->write_video_frame(oc);
	      }

//	      if (video_time == 0.00)
//	      {
//	        //Flush all audio buffer
//	        mRecorderAudio->flush_all_audio();
//	      }
	    }
	    else
	    {
	      mRecorderVideo->write_video_frame(oc);
	    }

	  }

#if DEBUG_RECORDING
	  LOGI(10, "Write all unencoded packets\n");
#endif
	  // need to take some time to handle extra data in buffer..
	  while( mRecorderVideo->hasUnencodedFrames() &&
	      (mRecorderAudio == NULL || mRecorderAudio->hasUnencodedFrames()) )
	  {
	    /* Compute current audio and video time. */
#ifdef USE_FFMPEG_3_0
	    audio_time = audio_st ? av_stream_get_end_pts(audio_st) * av_q2d(audio_st->time_base) : 0.0;
	    video_time = video_st ? av_stream_get_end_pts(video_st) * av_q2d(video_st->time_base) : 0.0;
#else
	    audio_time = audio_st ? audio_st->pts.val * av_q2d(audio_st->time_base) : 0.0;
	    video_time = video_st ? video_st->pts.val * av_q2d(video_st->time_base) : 0.0;
#endif

#if DEBUG_RECORDING
	    LOGI(10, "video_time: %f  audio_time:%f\n", video_time, audio_time);
#endif

	    //LOGI(10, "mStopRecording: %d  more audioframe:%d\n", mStopRecording,mRecorderAudio->hasUnencodedFrames());

	    if (audio_st != NULL)
	    {
	      /* write interleaved audio and video frames */
	      if (!video_st || (video_st && audio_st && audio_time < video_time))
	      {
	        mRecorderAudio->write_audio_frame4(oc) ;
	      }
	      else
	      {
	        mRecorderVideo->write_video_frame(oc);
	      }
	    }
	    else
	    {
	      /* No audio, write video frames only. */
	      mRecorderVideo->write_video_frame(oc);
	    }

	  }
	}

	/* Write the trailer, if any. The trailer must be written before you
	 * close the CodecContexts open when you wrote the header; otherwise
	 * av_write_trailer() may try to use memory that was freed on
	 * av_codec_close(). */
	LOGI(10, "closing input file (if any)\n ");
	if (oc != NULL) {
	  av_write_trailer(oc);
	  if (!fmt && !(fmt->flags & AVFMT_NOFILE))
	      /* Close the output file. */
	      avio_close(oc->pb);
	}


	if (mRecorderVideo != NULL)
	{
	  mRecorderVideo->cleanUp();
	  delete mRecorderVideo;
	  mRecorderVideo = NULL;
	}

	if (mRecorderAudio != NULL)
	{
		mRecorderAudio->cleanUp();
		delete mRecorderAudio;
		mRecorderAudio = NULL;
	}

	//May need to join thread for synchronization
	destroyRecorders();

	/* free the stream */
	if (oc != NULL) {
	  avformat_free_context(oc);
	}
#ifdef ANDROID_BUILD
	//Dettach from Java vm
	dettachVmFromThread() ;
	LOGI(10, "Dettached recording thread from VM\n");
#endif

	return 0;
}

//////// Recording functions ends /////////////////

void MediaPlayer::decodeMovie(void* ptr)
{
	if (p2pSessionCount > 0)
	{
		AVStream * audio_st = NULL;
		if (mAudioStreamIndex != -1) // Has audio
		{
			audio_st = mMovieFile->streams[mAudioStreamIndex];
			mDecoderAudio1 = new DecoderAudio1(audio_st);
			mDecoderAudio1->setPlaybackMode(mPlayBackOnly);
			mDecoderAudio1->onDecode = decode;
			mDecoderAudio1->setLog(mp_log);
//			mDecoderAudio1->external_clock_time = clock_start_time;//external clock to sync
			mDecoderAudio1->last_updated = (double) av_gettime() / 1000000.0;
			mDecoderAudio1->startAsync();
			log_info("decodeMovie audio codecid: %s\n",
					avcodec_get_name(audio_st->codec->codec->id ));
		}

//		mDecoderAudio1 = new DecoderAudio1(audioCtx);
//		mDecoderAudio1->setPlaybackMode(mPlayBackOnly);
//		mDecoderAudio1->onDecode = decode;
//		mDecoderAudio1->last_updated = (double) av_gettime() / 1000000.0;
//		mDecoderAudio1->setLog(mp_log);
//		//mDecoderAudio1->pause();
//		mDecoderAudio1->startAsync();
//		log_info("decodeMovie audio codecid: %s\n",
//				avcodec_get_name(audioCtx->codec->id));

		AVStream *video_st = NULL;
		if (mVideoStreamIndex != -1) {
			video_st = mMovieFile->streams[mVideoStreamIndex];
			mDecoderVideo1 = new DecoderVideo1(video_st);
			mDecoderVideo1->setPlaybackMode(mPlayBackOnly);
			if (mPlayBackOnly) {
				mDecoderVideo1->setP2pPlayByTimestampEnabled(true);
			} else {
				mDecoderVideo1->setP2pPlayByTimestampEnabled(mIsP2pPlayByTimestampEnabled);
			}
			mDecoderVideo1->setRecordingModeOnly(isRecordMode);
			mDecoderVideo1->onDecode = decode;
			mDecoderVideo1->audioDecoder = mDecoderAudio1;
			mDecoderVideo1->last_updated = (double) av_gettime() / 1000000.0;
			mDecoderVideo1->setLog(mp_log);
			//mDecoderVideo1->pause();
			mDecoderVideo1->startAsync();
			log_info("decodeMovie video codecid: %s\n",
					avcodec_get_name(video_st->codec->codec->id));
		}

		/* P2P client. */
		log_info("starting RMC client thread\n");
		mRmcClient = new RmcClient();
		mRmcClient->setDecoderVideo(mDecoderVideo1);
		mRmcClient->setDecoderAudio(mDecoderAudio1);
		mRmcClient->setRmcChannelInfo(mRmcChannelInfo);
		int rmcWorkingMode = 0;
		if (isRecordMode) {
			rmcWorkingMode = WORKING_MODE_RECORDING_ONLY;
		} else if (mPlayBackOnly) {
			rmcWorkingMode = WORKING_MODE_PLAYBACK;
		}
		mRmcClient->setWorkingMode(rmcWorkingMode);
		mRmcClient->setEventListener((void *)this);
		mRmcClient->start();

		/* TUTK client. */
		//	log_info("starting AVAPIs client thread\n");
		//	mAVAPIsClient = new AVAPIsClient();
		//	mAVAPIsClient->setDecoderVideo(mDecoderVideo1);
		//	mAVAPIsClient->setDecoderAudio(mDecoderAudio1);
		//	mAVAPIsClient->setUIDs((char **)UID, mNumId);
		//	mAVAPIsClient->init();
		//	mAVAPIsClient->start();


		//	while (!externalInterruptSignal &&
		//			(mDecoderVideo1->packets() == 0 || mDecoderAudio1->packets() == 0))
		//	{
		//		usleep(100);
		//	}
		//	log_info("Already has video and audio --> resume decoders\n");
		//	mDecoderAudio1->resume();
		//	mDecoderVideo1->resume();

		mCurrentState = MEDIA_PLAYER_STARTED;
	}
	else
	{
		time_t time_now;
		int status;
		AVPacket pPacket;

#if ANDROID_BUILD
		if(attachVmToThread () != JNI_OK)
		{
			log_info("Attach VM to thread MOVIE  Failed");
		}
#endif

		/* Calculate buffer size. */
		if (mPlayBackOnly == false)
		{
			//		if (ffmpeg_buffer_size_in_kb != -1)
			//		{
			//			// buffer size has been set from outside
			//			log_info(
			//					"Buffer size %d KB --> recalculate parameters.\n", ffmpeg_buffer_size_in_kb);
			//			int ffmpeg_audio_buff_size_kb = (int) ((double) ffmpeg_buffer_size_in_kb / 7);
			//			int ffmpeg_video_buff_size_kb = ffmpeg_buffer_size_in_kb - ffmpeg_audio_buff_size_kb;
			//			ffmpeg_video_buff_size = calculateVideoBufferSize(ffmpeg_video_buff_size_kb);
			//			ffmpeg_audio_buff_size = calculateAudioBufferSize(ffmpeg_audio_buff_size_kb);
			//			ffmpeg_buffer_size = ffmpeg_video_buff_size + ffmpeg_audio_buff_size;
			//			log_info("Video buff %d, audio buff %d, total %d\n",
			//					ffmpeg_video_buff_size, ffmpeg_audio_buff_size, ffmpeg_buffer_size);
			//		}
			//		else
			{
				if (strcmp(mMovieFile->iformat->name, "rtsp") == 0 )
				{
					ffmpeg_video_buff_size = FFMPEG_PLAYER_LOCAL_MIN_VIDEO_QUEUE_SIZE;
					ffmpeg_audio_buff_size = FFMPEG_PLAYER_LOCAL_MIN_AUDIO_QUEUE_SIZE;
					max_video_buf_size = FFMPEG_PLAYER_LOCAL_MAX_VIDEO_QUEUE_SIZE;
					max_audio_buf_size = FFMPEG_PLAYER_LOCAL_MAX_AUDIO_QUEUE_SIZE;
					mIsLocal = true;
				}
				else
				{
					ffmpeg_video_buff_size = FFMPEG_PLAYER_REMOTE_MIN_VIDEO_QUEUE_SIZE;
					ffmpeg_audio_buff_size = FFMPEG_PLAYER_REMOTE_MIN_AUDIO_QUEUE_SIZE;
					max_video_buf_size = FFMPEG_PLAYER_REMOTE_MAX_VIDEO_QUEUE_SIZE;
					max_audio_buf_size = FFMPEG_PLAYER_REMOTE_MAX_AUDIO_QUEUE_SIZE;
					mIsLocal = false;
				}
				ffmpeg_buffer_size = ffmpeg_video_buff_size + ffmpeg_audio_buff_size;
			}
		}
		else
		{
			ffmpeg_video_buff_size = FFMPEG_PLAYBACK_MIN_VIDEO_QUEUE_SIZE;
			ffmpeg_audio_buff_size = FFMPEG_PLAYBACK_MIN_AUDIO_QUEUE_SIZE;
			ffmpeg_buffer_size = ffmpeg_video_buff_size + ffmpeg_audio_buff_size;
		}
		log_info("Max video buff size %d, max audio buff size %d\n", max_video_buf_size, max_audio_buf_size);
		ffmpeg_buffer_size = 0;

		int64_t clock_start_time = av_gettime();
		/* 20140728: hoang: check interrupt flag before start decoders.
		 * Start decoders if player is interrupted could cause hang app. */
		mDecoderAudio = NULL;
		AVStream * stream_audio = NULL;
		if (externalInterruptSignal == true)
		{
			log_info(
					"Player is interrupted, don't start audio decoder.\n");
		}
		else
		{
			if (mAudioStreamIndex != -1) // Has audio
			{
				stream_audio = mMovieFile->streams[mAudioStreamIndex];
				mDecoderAudio = new DecoderAudio(stream_audio);
				mDecoderAudio->setPlaybackMode(mPlayBackOnly);
				mDecoderAudio->onDecode = decode;
				mDecoderAudio->setLog(mp_log);
				mDecoderAudio->external_clock_time = clock_start_time;//external clock to sync
				mDecoderAudio->last_updated = (double) clock_start_time / 1000000.0;
				mDecoderAudio->startAsync();
				log_info("decodeMovie audio codecid: %s\n",
						avcodec_get_name(stream_audio->codec->codec->id ));
			}
		}

		mDecoderVideo = NULL;
		AVStream* stream_video = NULL;
		if (externalInterruptSignal == true)
		{
			log_info(
					"Player is interrupted, don't start video decoder.\n");
		}
		else
		{
			if (mVideoStreamIndex != -1) //Has video
			{
				stream_video =     mMovieFile->streams[mVideoStreamIndex];
				mDecoderVideo = new DecoderVideo(stream_video);
				mDecoderVideo->setPlaybackMode(mPlayBackOnly);
				mDecoderVideo->setRecordingModeOnly(isRecordMode);
				mDecoderVideo->onDecode = decode;
				mDecoderVideo->setLog(mp_log);
				mDecoderVideo->external_clock_time = clock_start_time; //external clock to sync
				mDecoderVideo->last_updated = (double) clock_start_time / 1000000.0;
				mDecoderVideo->audioDecoder = mDecoderAudio;
				if (mAudioStreamIndex == -1)
				{
					/* Dont need to sync video. */
					log_info(
							"Audio muted --> dont need to sync video.");
					mDecoderVideo->setSyncMaster(AV_NO_SYNC);
				} else {
					mDecoderVideo->setSyncMaster(AV_SYNC_AUDIO_MASTER);
				}
				//mDecoderVideo->pause(); //will pause after show first key frame
				mDecoderVideo->startAsync();
				log_info("decodeMovie video codecid: %s\n",
						avcodec_get_name(stream_video->codec->codec->id ));
			}
		}

		int ret;
		if (externalInterruptSignal == true)
		{
			log_info(
					"Player is interrupted, don't start main player thread.\n");
		}
		else
		{
			ret = MEDIA_PLAYBACK_STATUS_STARTED;
			// STREAMER  MODE
			if (mPlayBackOnly == false)
			{
				bool first_pkt = true;
				mCurrentState = MEDIA_PLAYER_STARTED;

				// set flag to indicate that we are reading frame now..
				interrupt_cb_id = INTERRUPT_CALLBACK_AV_READ_FRAME;
				// reset startTime to -1
				startTime = -1;
				int64_t currentKAL, lastKAL = av_gettime();
				while ( mCurrentState != MEDIA_PLAYER_DECODED   &&
						mCurrentState != MEDIA_PLAYER_STOPPED   &&
						mCurrentState != MEDIA_PLAYER_STATE_ERROR  )
				{
					/*
					 * 20140521: hoang: check stream is valid
					 * if audio or video clock is not updated after 30s,
					 * assume stream is not alive --> force user to reconnect.
					 */
					if (checkStreamValid() == false)
					{
						log_info("There is invalid stream --> force reconnect...\n");
						mCurrentState = MEDIA_PLAYER_DECODED;
						if (externalInterruptSignal == false) {
						    notify(MEDIA_ERROR_SERVER_DIED, 0, 0);
						} else {
							log_info("Media player interrupted, don't notify\n");
						}
						continue;
					}

					/* Check to send keep-alive every 5 minutes in remote. */
					if (mIsLocal == false)
					{
						currentKAL = av_gettime();
						if (currentKAL - lastKAL >= KEEP_ALIVE_INTERVAL)
						{
							notify(MEDIA_SEND_KEEP_ALIVE, 0, 0);
							lastKAL = currentKAL;
						}
					}

					/* Checking to flush buffer every 5 seconds. */
					timeval pTime2;
					static double time0 = -1;
					static double time1 = -1;
					gettimeofday(&pTime2, NULL);
					time1 = pTime2.tv_sec + (pTime2.tv_usec / 1000000.0);
					if (time0 == -1 || time1 > time0 + BUFFER_CHECKING_INTERVAL_IN_S)
					{
						if (time0 != -1)
						{
							if (mDecoderVideo != NULL)
							{
								int videoPkts = mDecoderVideo->packets();
								log_info(
										"*******************Checking buffer size************** %d", videoPkts);
								if (videoPkts > max_video_buf_size ||
										(mDecoderAudio != NULL && mDecoderAudio->packets() > max_audio_buf_size) )
								{
									log_info(
											"Buffers are too large now --> flush old packets to reduce latency.\n");
									flushAllBuffers();
									/* Show loading icon to indicate the flush. */
//									mReceiveDecodedFrame = false;
//									isLoadingDialogShowing = true;
//									notify(MEDIA_INFO_CORRUPT_FRAME_TIMEOUT, -1, -1);
								}
							}
						}
						time0 = time1;
					}
//					log_info("video buffer size: %d\n", mDecoderVideo->packets());

					time(&startTime);
					status = av_read_frame(mMovieFile, &pPacket);

					if(status < 0 || shouldInterrupt) /* some error : May need to classified it. */
					{
						if (mCurrentState != MEDIA_PLAYER_STOPPED)
						{
							log_info("av_read_frame error, status: %d\n", status);

							//FIXME: change this to sth more meaningful
							mCurrentState = MEDIA_PLAYER_DECODED;

							/* status < 0 If :
                       1) server died (server become disconnected /connection refused on host)
                       2) timeout: rcv 1 packet timeout, (triggerd by callback)
                       3)
                       .. How to differentiate ?
							 */
							if (externalInterruptSignal == false) {
							    notify(MEDIA_ERROR_SERVER_DIED, status, 0);
							} else {
								log_info("read frame error, media player interrupted, don't notify\n");
							}
							//Whatever it is .. try to read the next frame
							// we will be stopped by either TIMEOUT or USER STOP
						}
						continue;
					}

					if (BIT_RATE_DEBUGGING )
					{
						timeval pTime;
						static double t1 = -1;
						static double t2 = -1;
						static double t3 = -1;
						static double t4 = -1;

						gettimeofday(&pTime, NULL);
						t2 = pTime.tv_sec + (pTime.tv_usec / 1000000.0);
						if (t1 == -1 || t2 > t1 + 1) {
							double duration = (av_gettime() - t0) / 1000000.0 - 2;
							sPlayer->notify(MEDIA_INFO_BITRATE_BPS, (int) (data_in_bytes / (double) duration), -1);
							t1 = t2;
						}

						if ((av_gettime() - t0)/1000000.0 >= 2)
						{
							data_in_bytes += pPacket.size;
						}


						//log_info("data_in_bytes %ld\n", data_in_bytes);
					}

					/* Player mode: don't buffer when we still don't receive the first key frame yet. */
					if (ffmpeg_buffer_size > 0 && sPlayer->firstKeyFrameDisplayed == true)
					{
						if (mIsInPTZMode == true)
						{
							/* In PTZ. */
							//						log_info(
							//								"IN PTZ bypass the buffer\n");
							if ( (mDecoderVideo != NULL && mDecoderVideo->isPaused()) ||
									(mDecoderAudio != NULL && mDecoderAudio->isPaused()) )
							{
								notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
								resumeDecoders();
							}
						}
						//buffering audio & video
						else if (mDecoderAudio != NULL && mDecoderVideo != NULL)
						{
							if (mDecoderAudio->packets() + mDecoderVideo->packets() == 0)
							{
								if (!mDecoderAudio->isPaused() || !mDecoderVideo->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("stop decoders, buffering...\n");
#endif
									notify(MEDIA_INFO_START_BUFFERING, -1, -1);
									pauseDecoders();
								}
							}
							else if (mDecoderAudio->packets() + mDecoderVideo->packets() > ffmpeg_buffer_size
									&& mDecoderAudio->packets() >= ffmpeg_audio_buff_size)
							{
								if (mDecoderAudio->isPaused() || mDecoderVideo->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("start decoders, stop buffering...\n");
#endif
									notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
									resumeDecoders();
								}
								else
								{
#if DEBUG_BUFFERING
									log_info(
											"audio buffer size: %d, video buffer size: %d\n",
											mDecoderAudio->packets(), mDecoderVideo->packets());
#endif
								}
							}
							else
							{
#if DEBUG_BUFFERING
								log_info(
										"audio buffer size: %d, video buffer size: %d\n",
										mDecoderAudio->packets(), mDecoderVideo->packets());
#endif
							}
						}
						else if (mDecoderVideo != NULL) //no audio
						{
							if (mDecoderVideo->packets() == 0)
							{
								if (!mDecoderVideo->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("stop video decoder, buffering...\n");
#endif
									notify(MEDIA_INFO_START_BUFFERING, -1, -1);
									pauseDecoders();
								}
							}
							else if (mDecoderVideo->packets() > ffmpeg_video_buff_size)
							{
								if (mDecoderVideo->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("start video decoder, stop buffering...\n");
#endif
									notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
									resumeDecoders();
								}
							}
							else
							{
#if DEBUG_BUFFERING
								log_info(
										"video buffer size: %d\n", mDecoderVideo->packets());
#endif
							}
						}
						else //no video
						{
							if (mDecoderAudio->packets() == 0)
							{
								if (!mDecoderAudio->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("stop audio decoder, buffering...\n");
#endif
									notify(MEDIA_INFO_START_BUFFERING, -1, -1);
									pauseDecoders();
								}
							}
							else if (mDecoderAudio->packets() > ffmpeg_audio_buff_size)
							{
								if (mDecoderAudio->isPaused())
								{
#if DEBUG_BUFFERING
									log_info("start audio decoder, stop buffering...\n");
#endif
									notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
									resumeDecoders();
								}
							}
							else
							{
#if DEBUG_BUFFERING
								log_info(
										"audio buffer size: %d\n", mDecoderAudio->packets());
#endif
							}
						}
					} //if (ffmpeg_buffer_size != 0)

					// Is this a packet from the video stream?
					if (pPacket.stream_index == mVideoStreamIndex)
					{
						if (mRecorderVideo != NULL)
						{
							AVPacket rPacket;

							av_new_packet(&rPacket, pPacket.size );
							rPacket.pts = pPacket.pts;
							rPacket.dts = pPacket.dts;
							rPacket.pos = pPacket.pos;
							rPacket.duration = pPacket.duration;
							rPacket.convergence_duration = pPacket.convergence_duration;
							rPacket.stream_index = pPacket.stream_index;
							rPacket.flags = pPacket.flags;

							memcpy(rPacket.data, pPacket.data, pPacket.size);

							// log_info( "video buffer size: %d\n", mRecorderVideo->packets());

							mRecorderVideo->enqueue(&rPacket);
						}

						if (VIDEO_THROUGHPUT_DEBUGGING) {
						    timeval pVideoTime;
						    static int videoFrames = 0;
						    static double videoT1 = -1;
						    static double videoT2 = -1;

						    gettimeofday(&pVideoTime, NULL);
						    videoT2 = pVideoTime.tv_sec + (pVideoTime.tv_usec / 1000000.0);
						    if (videoT1 == -1 || videoT2 > videoT1 + 1) {
						        log_info("Video input fps: %d\n", videoFrames);
						        videoT1 = videoT2;
						        videoFrames = 0;
						    }
						    videoFrames++;
						}

						if (mDecoderVideo->packets() >= FFMPEG_PLAYER_MAX_QUEUE_SIZE)
						{
							/* We drop to next key pkt to avoid video frame corrupted. */
							LOGI(10, "Video buffer is full, drop to next key frame.\n",
									mDecoderVideo->packets());
							mDecoderVideo->dropToNextKeyFrame();
						}
						mDecoderVideo->enqueue(&pPacket);

#if DEBUG_READ_VIDEO_PKT
						log_info(
								"Video pkt: pts: %lld VVVVV pts_double: %f, timebase: %f, pkt_size: %d, pkt_duration %lld, isKey? %d\n",
								pPacket.pts, pPacket.pts*av_q2d(mMovieFile->streams[mVideoStreamIndex]->time_base), av_q2d(mMovieFile->streams[mVideoStreamIndex]->time_base),
								pPacket.size, pPacket.duration, pPacket.flags & AV_PKT_FLAG_KEY);
#endif
						//					log_info(
						//							"av_gettime: %lld, start_time_realtime: %lld, curr_pos: %lld\n",
						//							av_gettime(), mMovieFile->start_time_realtime, mMovieFile->pb->pos);
					}
					else if (pPacket.stream_index == mAudioStreamIndex)
					{
						if (first_pkt == true)
						{
							double first_audio_pts = pPacket.pts * av_q2d(mMovieFile->streams[mAudioStreamIndex]->time_base);
							mDecoderAudio->external_clock_time = av_gettime() + first_audio_pts * 1000000;
							first_pkt = false;
						}

						/* BE CAREFUL: mRecorderAudio can be NULL here. */
//						if(mStopRecording == false && mRecorderAudio != NULL)
//						{
//							mDecoderAudio->setRecorder((void *)mRecorderAudio);
//						}

						if (mDecoderAudio->packets() >= FFMPEG_PLAYER_MAX_QUEUE_SIZE)
						{
							/* For audio: we just need to drop oldest pkt before add new one. */
							LOGI(10, "Audio buffer is full, dequeue old packet.\n");
							AVPacket packet;
							if (mDecoderAudio->dequeue(&packet, true) > 0)
							{
#ifdef USE_FFMPEG_3_0
								av_packet_unref(&packet);
#else
								// Free the packet that was allocated by av_read_frame
								av_free_packet(&packet);
#endif
							}
						}

						if (AUDIO_THROUGHPUT_DEBUGGING) {
						    timeval pAudioTime;
						    static int audioData = 0;
						    static double audioT1 = -1;
						    static double audioT2 = -1;

						    gettimeofday(&pAudioTime, NULL);
						    audioT2 = pAudioTime.tv_sec + (pAudioTime.tv_usec / 1000000.0);
						    if (audioT1 == -1 || audioT2 > audioT1 + 1) {
						        log_info("Audio throughput: %d\n", audioData);
						        audioT1 = audioT2;
						        audioData = 0;
						    }
						    audioData += pPacket.size;
						}

						mDecoderAudio->enqueue(&pPacket);

#if DEBUG_READ_AUDIO_PKT
						log_info(
								"Audio pkt: pts: %lld AAAAA pts_double: %f, timebase: %f, pkt_size: %d, duration %lld\n",
								pPacket.pts, pPacket.pts*av_q2d(mMovieFile->streams[mAudioStreamIndex]->time_base), av_q2d(mMovieFile->streams[mAudioStreamIndex]->time_base),
								pPacket.size, pPacket.duration);
#endif
					}
					else
					{
						//LOGI(10, "Free unknown packet\n");
#ifdef USE_FFMPEG_3_0
						av_packet_unref(&pPacket);
#else
						// Free the packet that was allocated by av_read_frame
						av_free_packet(&pPacket);
#endif
					}

				}

			}
			else // PLAYBACK MODE
			{
				//used to keep track of duration..
				mStartPosition =  av_gettime();
				mSeekPosition = mMovieFile->start_time;
				mFirstClipStartTime = mMovieFile->start_time;
				log_info("First start time %lld\n", mFirstClipStartTime);
				timeTotalPause = 0;
				timeDeltaSeek = 0;

				mCurrentState = MEDIA_PLAYER_STARTED;

				// set flag to indicate that we are reading frame now..
				while (ret != MEDIA_PLAYBACK_STATUS_COMPLETE)
				{
					if (url != NULL)
					{
						int res = resetDataSource(url);
						if (res == INVALID_OPERATION)
						{
							free(url);
							url = NULL;
							continue;
						}
					}


					if (ret == MEDIA_PLAYBACK_STATUS_STARTED && mMovieFile != NULL)
					{
						//mStartPosition =  av_gettime();
						while (mCurrentState != MEDIA_PLAYER_STOPPED &&
								mCurrentState != MEDIA_PLAYER_STATE_ERROR)
						{
							if (mCurrentState == MEDIA_PLAYER_SEEKING)
							{
								// seeking
								int64_t seek_target = mSeekPosition;//(int64_t)mSeekPosition/1000.0*AV_TIME_BASE + mMovieFile->start_time;
								int64_t seek_min    = INT64_MIN;//INT64_MIN;//is->seek_rel > 0 ? seek_target - is->seek_rel + 2: INT64_MIN;
								int64_t seek_max    = INT64_MAX;//is->seek_rel < 0 ? seek_target - is->seek_rel - 2: INT64_MAX;

								interrupt_cb_id = INTERRUPT_CALLBACK_SEEK_FILE;
								time(&startTime);
								log_info("media player seeking...\n");
								int ret = avformat_seek_file(mMovieFile, -1, seek_min, seek_target, seek_max, AVSEEK_FLAG_ANY);
								interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
								if (ret < 0 || shouldInterrupt)
								{
									av_log(NULL, AV_LOG_ERROR,
											"%s: ERROR while seeking -ret: %d", __FUNCTION__, ret);
								}
								else
								{
									av_log(NULL, AV_LOG_ERROR, "%s SUCCEEDED - ret: %d, duration: %lld, start_time: %lld, seek_target: %lld, newPos: %lld",
											__FUNCTION__, ret, mMovieFile->duration, mMovieFile->start_time, seek_target, mMovieFile->pb->pos);
									/* flush audio & video buffer */
									flushAllBuffers();

									//update start time
									mStartPosition = av_gettime();
									//reset timeTotalPause
									timeStartPause = 0;
									timeTotalPause = 0;
								}

								//usleep(200);
								notify(MEDIA_SEEK_COMPLETE, 0, 0);
								//continue;
								mCurrentState = MEDIA_PLAYER_STARTED;
								resumeDecoders();
							}

							/*
							 * If pause, pause & continue loop, after that (resume) update total time pause & proccess streaming.
							 */
							if (mCurrentState == MEDIA_PLAYER_PAUSED)
							{
								usleep(200);
								continue;
							}

							if ( (mDecoderVideo == NULL ||
									mDecoderVideo->packets() > FFMPEG_PLAYBACK_MAX_QUEUE_SIZE ) &&
									(mDecoderAudio == NULL ||
											mDecoderAudio->packets() > FFMPEG_PLAYBACK_MAX_QUEUE_SIZE ) )
							{
								if (mCurrentState != MEDIA_PLAYER_PAUSED && mCurrentState != MEDIA_PLAYER_SEEKING)
								{
									resumeDecoders();
								}

								usleep(100000);
								continue;
							}

							interrupt_cb_id = INTERRUPT_CALLBACK_AV_READ_FRAME;
							time(&startTime);
							status = av_read_frame(mMovieFile, &pPacket);
							interrupt_cb_id = INTERRUPT_CALLBACK_OFF;
							if (status < 0 || shouldInterrupt)
							{
								log_info("av_read_frame error --> exit\n");
								/* If playback is still buffering, force decoders resume
								 * to avoid hang decoders when exit app. */
								resumeDecoders();
								break;
							}

							if (ffmpeg_buffer_size > 0)
							{
								//buffering audio & video
								if (mDecoderAudio != NULL && mDecoderVideo != NULL)
								{
									if (mDecoderAudio->packets() + mDecoderVideo->packets() == 0)
									{
#if DEBUG_BUFFERING
										log_info("stop decoders, buffering...\n");
#endif
										if (!mDecoderAudio->isPaused() || !mDecoderVideo->isPaused())
										{
											notify(MEDIA_INFO_START_BUFFERING, -1, -1);
											pauseDecoders();
										}
									}
									else if (mDecoderAudio->packets() + mDecoderVideo->packets() > ffmpeg_buffer_size
											&& mDecoderAudio->packets() >= ffmpeg_audio_buff_size)
									{
										if (mDecoderAudio->isPaused() || mDecoderVideo->isPaused())
										{
											notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
											if (mCurrentState != MEDIA_PLAYER_PAUSED && mCurrentState != MEDIA_PLAYER_SEEKING)
											{
												resumeDecoders();
											}
										}
									}
									else
									{
#if DEBUG_BUFFERING
										log_info(
												"audio buffer size: %d, video buffer size: %d\n",
												mDecoderAudio->packets(), mDecoderVideo->packets());
#endif
									}
								}
								else if (mDecoderVideo != NULL) //no audio
								{
									if (mDecoderVideo->packets() == 0)
									{
#if DEBUG_BUFFERING
										log_info("stop video decoder, buffering...\n");
#endif
										notify(MEDIA_INFO_START_BUFFERING, -1, -1);
										pauseDecoders();
									}
									else if (mDecoderVideo->packets() > ffmpeg_video_buff_size)
									{
										if (mDecoderVideo->isPaused())
										{
											notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
											if (mCurrentState != MEDIA_PLAYER_PAUSED && mCurrentState != MEDIA_PLAYER_SEEKING)
											{
												resumeDecoders();
											}
										}
									}
									else
									{
#if DEBUG_BUFFERING
										log_info(
												"video buffer size: %d\n", mDecoderVideo->packets());
#endif
									}
								}
								else //no video
								{
									if (mDecoderAudio->packets() == 0)
									{
#if DEBUG_BUFFERING
										log_info("stop audio decoder, buffering...\n");
#endif
										notify(MEDIA_INFO_START_BUFFERING, -1, -1);
										pauseDecoders();
									}
									else if (mDecoderAudio->packets() > ffmpeg_audio_buff_size)
									{
										if (mDecoderAudio->isPaused())
										{
											notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
											if (mCurrentState != MEDIA_PLAYER_PAUSED && mCurrentState != MEDIA_PLAYER_SEEKING)
											{
												resumeDecoders();
											}
										}
									}
									else
									{
#if DEBUG_BUFFERING
										log_info(
												"audio buffer size: %d\n", mDecoderAudio->packets());
#endif
									}
								}
							}

							// Is this a packet from the video stream?
							if (pPacket.stream_index == mVideoStreamIndex)
							{
							  if (mRecorderVideo != NULL)
							  {
							    AVPacket rPacket;

							    av_new_packet(&rPacket, pPacket.size );
							    rPacket.pts = pPacket.pts;
							    rPacket.dts = pPacket.dts;
							    rPacket.pos = pPacket.pos;
							    rPacket.duration = pPacket.duration;
							    rPacket.convergence_duration = pPacket.convergence_duration;
							    rPacket.stream_index = pPacket.stream_index;
							    rPacket.flags = pPacket.flags;

							    memcpy(rPacket.data, pPacket.data, pPacket.size);

							    // log_info( "video buffer size: %d\n", mRecorderVideo->packets());

							    mRecorderVideo->enqueue(&rPacket);
							  }
								mDecoderVideo->enqueue(&pPacket);
#if DEBUG_READ_VIDEO_PKT
								log_info(
										"Video pkt: pts: %lld VVVVV pts_double: %f, timebase: %f, pkt_size: %d, duration %lld, isKey? %d\n",
										pPacket.pts, pPacket.pts*av_q2d(mMovieFile->streams[mVideoStreamIndex]->time_base), av_q2d(mMovieFile->streams[mVideoStreamIndex]->time_base),
										pPacket.size, pPacket.duration, pPacket.flags & AV_PKT_FLAG_KEY);
#endif
							}
							else if (pPacket.stream_index == mAudioStreamIndex)
							{
							  /* BE CAREFUL: mRecorderAudio can be NULL here. */
//							  if(mStopRecording == false && mRecorderAudio != NULL)
//							  {
//							    mDecoderAudio->setRecorder((void *)mRecorderAudio);
//							  }

								mDecoderAudio->enqueue(&pPacket);
#if DEBUG_READ_AUDIO_PKT
								log_info(
										"Audio pkt: pts: %lld AAAAA pts_double: %f, timebase: %f, pkt_size: %d, duration %lld\n",
										pPacket.pts, pPacket.pts*av_q2d(mMovieFile->streams[mAudioStreamIndex]->time_base), av_q2d(mMovieFile->streams[mAudioStreamIndex]->time_base),
										pPacket.size, pPacket.duration);
#endif
							}
							else
							{
								// Free the packet that was allocated by av_read_frame
								av_free_packet(&pPacket);
							}

						} // While - loop


					}


					if (url != NULL)
					{
						free(url);
						url = NULL;
					}


					if  (externalInterruptSignal == true)
					{
						log_error(" break break break");
						//mDuration = 0;
						break;
					}

					/*
					 * ret value maybe:
					 * MEDIA_PLAYBACK_STATUS_STARTED: if has next clip
					 * MEDIA_PLAYBACK_STATUS_IN_PROGRESS: getting next clip
					 * MEDIA_PLAYBACK_STATUS_COMPLETE: no more clip
					 */
					ret = mListener->getNextClip((char**)&url);
					if (ret == MEDIA_PLAYBACK_STATUS_COMPLETE)
					{
						// Finish reading data from last clip, wait until video buffer is empty.
						while (mDecoderVideo != NULL && mDecoderVideo->packets() > 0
								&& !externalInterruptSignal
								&& mCurrentState != MEDIA_PLAYER_SEEKING)
						{
							log_error("Waiting for video decoder completed, buffer remaining %d\n",
									mDecoderVideo->packets());

							usleep(100000);
						}

						if (mCurrentState == MEDIA_PLAYER_SEEKING)
						{
							// seeking --> continue to read frame from stream
							ret = MEDIA_PLAYBACK_STATUS_STARTED;
						}

						log_error("After sleeping..");
					}
					else if (ret == MEDIA_PLAYBACK_STATUS_IN_PROGRESS)
					{
						usleep(100000);
					}

				} // end while (ret != MEDIA_PLAYBACK_STATUS_COMPLETE).

				//notify back to Java
				notify(MEDIA_PLAYBACK_COMPLETE, 0, 0);


			} // END PLAYBACK MODE
		} // shouldInterrupt == false

#if ANDROID_BUILD
		dettachVmFromThread();
#endif

		//waits on end of video thread
		ret = -1;
		if( (mDecoderVideo != NULL) &&
				(ret = mDecoderVideo->wait()) != 0)
		{
			log_error("Couldn't cancel video thread: %i\n", ret);
		}

		if( (mDecoderAudio != NULL) &&
				(ret = mDecoderAudio->wait()) != 0)
		{
			log_error("Couldn't cancel audio thread: %i\n", ret);
		}

		if(mCurrentState == MEDIA_PLAYER_STATE_ERROR)
		{
			log_info("playing err\n");
		}

		mCurrentState = MEDIA_PLAYER_PLAYBACK_COMPLETE;
	}
}

/**
 * Return the remaining time of the clip in microseconds.
 */
int64_t MediaPlayer::calculateRemainingTime()
{
	/* mCurrentPosition will be updated every decoded frame. */
	int64_t remaining_time = mDuration - mCurrentPosition * 1000;

	if (remaining_time < 0)
	{
		remaining_time = 0;
	}

	return remaining_time;
}

bool MediaPlayer::checkStreamValid()
{
	bool isValid = false;
	if (mDecoderVideo != NULL && mDecoderAudio != NULL)
	{
		// session is valid only if all streams are alive
		if (mDecoderVideo->checkStreamAlive() == true && mDecoderAudio->checkStreamAlive() == true)
		{
			isValid = true;
		}
		else
		{
			isValid = false;
		}
	}
	else if (mDecoderAudio != NULL) //only audio
	{
		isValid = mDecoderAudio->checkStreamAlive();
	}
	else if (mDecoderVideo != NULL) //only video
	{
		isValid = mDecoderVideo->checkStreamAlive();
	}

	return isValid;
}

void MediaPlayer::flushAllBuffers()
{
	if (mDecoderAudio != NULL)
	{
		mDecoderAudio->flush();
		Output::AudioDriver_flush();
	}

	if (mDecoderVideo != NULL)
	{
		mDecoderVideo->flush();
		mDecoderVideo->setSkipUntilNextKeyFrame(true);
	}
}

void MediaPlayer::checkAndFlushAllBuffers()
{
	if (mDecoderVideo != NULL)
	{
		mIsInPTZMode = true;
		decoded_frames = 0;
		if ( (max_video_buf_size != 0 && mDecoderVideo->packets() > max_video_buf_size) ||
				(max_audio_buf_size != 0 && mDecoderAudio != NULL && mDecoderAudio->packets() > max_audio_buf_size) )
		{
			log_info(
					"**************Need to flush to reduce latency.\n");
			flushAllBuffers();
			/* Show loading icon to indicate the flush. */
//			mReceiveDecodedFrame = false;
//			isLoadingDialogShowing = true;
//			notify(MEDIA_INFO_CORRUPT_FRAME_TIMEOUT, -1, -1);
		}
	}
}

void MediaPlayer::mp_log(void* ptr, int level, const char* fmt, ...)
{

	va_list vl;
	va_start(vl, fmt);
	log_info(fmt, vl);
	vprintf(fmt,vl);
	va_end(vl);


}


void* MediaPlayer::startPlayer(void* ptr)
{
	if (sPlayer != NULL)
	{
		log_info("starting main player thread\n");
		data_in_bytes = 0;
		t0 = av_gettime();
		sPlayer->decodeMovie(ptr);
	}

	return NULL;
}

void * MediaPlayer::startRecording(void* ptr)
{
	if (sPlayer != NULL)
	{
		log_info("starting main recording thread\n");
		sPlayer->recordMovie(ptr);
	}

	return NULL;
}

status_t MediaPlayer::startRecord(const char *url_1)
{
	if (mCurrentState != MEDIA_PLAYER_PREPARED &&
	    mCurrentState != MEDIA_PLAYER_STARTED)
	{
		log_info(
				"mCurrentState %d is invalid, don't start recording thread\n", mCurrentState);
		return INVALID_OPERATION;
	}
	int status;
	mStopRecording = false;
	record_url = (char *) malloc(strlen(url_1)+1);
	memcpy(record_url, url_1, strlen(url_1));
	record_url[strlen(url_1)] = '\0';


	log_info("record_url: %s\n", record_url);

	//pass url as arg - will use later when the recording thread is started
	status = pthread_create(&mRecordingThread, NULL, startRecording, (void*) record_url);

	if (isRecordMode == true) {
	  log_info("Recording mode only is enabled, mute audio stream now\n");
	  Output::AudioDriver_setAudioStreamMute(true);
	}

	return NO_ERROR;
}

bool MediaPlayer::isRecording()
{
	return !mStopRecording;
}

void MediaPlayer::initRecorders()
{
	// Set recorder to video decoders
	if (mDecoderVideo1 != NULL) {
		mDecoderVideo1->setRecorder((void *) mRecorderVideo);
	}
	if (mDecoderVideo != NULL) {
		mDecoderVideo->setRecorder((void *) mRecorderVideo);
	}

	// Set recorder to audio decoders
	if (mDecoderAudio1 != NULL) {
		mDecoderAudio1->setRecorder((void *) mRecorderAudio);
	}
	if (mDecoderAudio != NULL) {
		mDecoderAudio->setRecorder((void *) mRecorderAudio);
	}

//	if (mRmcClient != NULL)
//	{
//		mRmcClient->setRecorders(mRecorderVideo, mRecorderAudio);
//		mRmcClient->startRecording();
//	}
}

void MediaPlayer::destroyRecorders()
{
	// Set recorder to video decoders
	if (mDecoderVideo1 != NULL) {
		mDecoderVideo1->setRecorder(NULL);
	}
	if (mDecoderVideo != NULL) {
		mDecoderVideo->setRecorder(NULL);
	}

	// Set recorder to audio decoders
	if (mDecoderAudio1 != NULL) {
		mDecoderAudio1->setRecorder(NULL);
	}
	if (mDecoderAudio != NULL) {
		mDecoderAudio->setRecorder(NULL);
	}

//	if (mRmcClient != NULL) {
//		mRmcClient->stopRecording();
//		mRmcClient->setRecorders(NULL, NULL);
//	}
}

status_t MediaPlayer::stopRecord()
{
  if (mStopRecording == false) {
    //Signal to stop - the real stop is done on the recording thread
    mStopRecording = true;

    LOGI(10, "joining recording Thread ");
    pthread_join(mRecordingThread, NULL);
  }

  if (record_url  != NULL)
  {
    free(record_url);
    record_url = NULL;
  }

	return NO_ERROR;

}


status_t MediaPlayer::start()
{
	if (mCurrentState != MEDIA_PLAYER_PREPARED)
	{
		log_info("mCurrentState != MEDIA_PLAYER_PREPARED, don't start playerthread \n");
		return INVALID_OPERATION;
	}

	int status;
	status = pthread_create(&mPlayerThread, NULL, startPlayer, NULL);

	//start video timer to run periodically
	if (mMovieFile != NULL)
	{
		mVideoTimerRunning = true;
		status = pthread_create(&mVideoTimerThread, NULL, videoTimerTask, NULL);
	}
	else
	{
		status = pthread_create(&mVideoBitrateThread, NULL, videoBitrateTask, NULL);
	}

	return NO_ERROR;
}

status_t MediaPlayer::stop()
{
	log_info("stopping media player\n");

	//pthread_mutex_lock(&mLock);
	//pthread_mutex_unlock(&mLock);
	if (mCurrentState != MEDIA_PLAYER_STOPPED)
	{
		mCurrentState = MEDIA_PLAYER_STOPPED;
		log_info("player is playing.. ");

		//Try to stop recording first - blocking call
		if (mStopRecording == false)
		{
			stopRecord();
		}

		if (mDecoderAudio != NULL)
		{
			log_info("stop audio decoder...\n");
			if (mDecoderAudio->isPaused())
			{
				mDecoderAudio->resume();
			}
			Output::AudioDriver_stop();
			mDecoderAudio->stop();
		}


		if(mDecoderVideo != NULL)
		{
			log_info("stop video decoder...\n");
			if (mDecoderVideo->isPaused())
			{
				mDecoderVideo->resume();
			}
			mDecoderVideo->stop();
		}

		if (mRmcClient != NULL)
		{
		  mRmcClient->stop();
		}
		//		if (mAVAPIsClient != NULL)
		//		{
		//			mAVAPIsClient->stop();
		//			mAVAPIsClient->destroy();
		//		}

		if(mDecoderVideo1 != NULL)
		{
			log_info("stop video1 decoder...\n");
			if (mDecoderVideo1->isPaused())
			{
				mDecoderVideo1->resume();
			}
			mDecoderVideo1->stop();
		}
		if(mDecoderAudio1 != NULL)
		{
			log_info("stop audio1 decoder...\n");
			if (mDecoderAudio1->isPaused())
			{
				mDecoderAudio1->resume();
			}
			Output::AudioDriver_stop();
			mDecoderAudio1->stop();
		}

		log_info("Waiting player thread stop...\n");
		if(pthread_join(mPlayerThread, NULL) != 0) {
			log_error("Couldn't cancel player thread");
		}

		log_info("Player thread stopped\n");
	}
	else
	{
		mCurrentState = MEDIA_PLAYER_STOPPED;
		log_info("player is not playing yet");
		//only set it and wait
	}

	//close OS drivers
	if (mDecoderAudio != NULL)
	{
		Output::AudioDriver_unregister();
	}

	if (mDecoderVideo != NULL)
	{
		Output::VideoDriver_unregister();
	}

	if (mDecoderAudio1 != NULL)
	{
		Output::AudioDriver_unregister();
	}

	if (mDecoderVideo1 != NULL)
	{
		Output::VideoDriver_unregister();
	}
	Output::unregister();

	log_info("media player stopped\n");

	return NO_ERROR;
}

status_t MediaPlayer::pause()
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED) {
		return INVALID_OPERATION;
	}
	//pthread_mutex_lock(&mLock);
	mCurrentState = MEDIA_PLAYER_PAUSED;
	timeStartPause = av_gettime();
	pauseDecoders();
	//pthread_mutex_unlock(&mLock);
	return NO_ERROR;
}


void MediaPlayer::pauseDecoders()
{
	if (externalInterruptSignal)
	{
		return;
	}

	if (mDecoderAudio != NULL)
	{
		if (!mDecoderAudio->isPaused())
		{
			mDecoderAudio->pause();
			Output::AudioDriver_pause();
		}

	}

	if (mDecoderVideo != NULL && !mDecoderVideo->isPaused())
	{
		mDecoderVideo->pause();
	}
}

void MediaPlayer::resumeDecoders()
{
	if (mDecoderAudio != NULL)
	{
		if (mDecoderAudio->isPaused())
		{
			mDecoderAudio->resume();
			Output::AudioDriver_resume();
		}

	}

	if (mDecoderVideo != NULL && mDecoderVideo->isPaused())
	{
		mDecoderVideo->resume();
	}
}

bool MediaPlayer::isPlaying()
{
	return (mCurrentState != MEDIA_PLAYER_STOPPED &&
			mCurrentState != MEDIA_PLAYER_PAUSED);
}

status_t MediaPlayer::getVideoWidth(int *w)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED) {
		return INVALID_OPERATION;
	}
	*w = mVideoWidth;
	return NO_ERROR;
}

status_t MediaPlayer::getVideoHeight(int *h)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED) {
		return INVALID_OPERATION;
	}
	*h = mVideoHeight;
	return NO_ERROR;
}

/**
 * Get current duration in milliseconds then update msec variable.
 */
status_t MediaPlayer::getCurrentPosition(int *msec)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED) {
		return INVALID_OPERATION;
	}

	*msec = mCurrentPosition;
//	log_info("getCurrentPosition: %d", *msec / 1000);

	return NO_ERROR;
}

status_t  MediaPlayer::getCurrentStreamPosition(int *msec)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED || mMovieFile == NULL || mMovieFile->pb == NULL)
	{
		*msec = 0;
	}
	else
	{
		*msec = (int) (mMovieFile->pb->pos / 1000.0);
	}

	return NO_ERROR;
}

status_t MediaPlayer::getDuration(int *msec)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED) {
		return INVALID_OPERATION;
	}

	if (shouldShowDuration)
	{
		*msec = mDuration / 1000;
	}
	else
	{
		*msec = -1000;
	}

	return NO_ERROR;
}

bool MediaPlayer::getIsSdcardStreaming()
{
  if (mCurrentState < MEDIA_PLAYER_PREPARED) {
    return false;
  }

  return mIsSdcardStreaming;
}

/**
 * Return remaining duration in microseconds.
 */
int64_t MediaPlayer::getRemainingDuration()
{
  int64_t remainingDuration = -1;
  if (shouldShowDuration == true) {
    remainingDuration = mDuration - mCurrTimestamp;
  }
  return remainingDuration;
}

status_t MediaPlayer::setDuration(int msec)
{
	log_info("setDuration %d\n", msec);
	shouldShowDuration = true;
	if (msec != -1000)
	{
		shouldUpdateDuration = false;
		// 20161229 HOANG In case setDuration from outside, don't need to use accumulate duration, so reset it
		mAccumulateDuration = 0;
		mDuration = msec * 1000;
	}

	return NO_ERROR;
}

status_t MediaPlayer::seekTo(int msec)
{
	if (mCurrentState < MEDIA_PLAYER_PREPARED || mCurrentState == MEDIA_PLAYER_SEEKING) {
		return INVALID_OPERATION;
	}

	pauseDecoders();
	mCurrentState = MEDIA_PLAYER_SEEKING;
	mSeekPosition = (int64_t) msec/1000.0*AV_TIME_BASE + mMovieFile->start_time;
	mCurrentPosition = msec;
	return NO_ERROR;
}

status_t MediaPlayer::reset()
{
	return INVALID_OPERATION;
}

status_t MediaPlayer::setAudioStreamType(int type)
{
	return NO_ERROR;
}

void MediaPlayer::ffmpegNotify(void* ptr, int level, const char* fmt, va_list vl) {

	switch(level) {
	/**
	 * Something went really wrong and we will crash now.
	 */
	case AV_LOG_PANIC:
		log_error("AV_LOG_PANIC: %s", fmt);
		//sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
		break;

		/**
		 * Something went wrong and recovery is not possible.
		 * For example, no header was found for a format which depends
		 * on headers or an illegal combination of parameters is used.
		 */
	case AV_LOG_FATAL:
		log_error("AV_LOG_FATAL: %s", fmt);
		//sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
		break;

		/**
		 * Something went wrong and cannot losslessly be recovered.
		 * However, not all future data is affected.
		 */
	case AV_LOG_ERROR:
		log_error("AV_LOG_ERROR: %s", fmt);
		//sPlayer->mCurrentState = MEDIA_PLAYER_STATE_ERROR;
		break;

		/**
		 * Something somehow does not look correct. This may or may not
		 * lead to problems. An example would be the use of '-vstrict -2'.
		 */
	case AV_LOG_WARNING:
		log_error("AV_LOG_WARNING: %s", fmt);
		break;

	case AV_LOG_INFO:
		log_info("%s", fmt);
		break;

	case AV_LOG_DEBUG:
		log_debug("%s", fmt);
		break;

	}
}

void MediaPlayer::notify(int msg, int ext1, int ext2)
{
	bool send = true;
	bool locked = false;

	if ((mListener != 0) && send)
	{
		mListener->notify(msg, ext1, ext2);
	}
}

/* Set Playing Options
   - Flag the player to switch to I-frame Only mode
   Future work: can support other runtime flags
 */
int  MediaPlayer::setPlayOption(int opts)
{

	//Only effective while streaming
	if (mPlayBackOnly == false)
	{
		switch (opts)
		{
		case MEDIA_STREAM_ALL_FRAME:
			if (mDecoderVideo != NULL)
			{
				log_debug(" Play ALL FRAME >> ");
				mDecoderVideo->setPlayIFrameOnly(false);
			}
			break;
		case MEDIA_STREAM_IFRAME_ONLY:
			if (mDecoderVideo != NULL)
			{
				log_debug(" Play I FRAME >> ");
				mDecoderVideo->setPlayIFrameOnly(true);
			}
			break;
		case MEDIA_STREAM_RTSP_WITH_TCP:
			//TODO: check current player status mCurrentState
			mRtspWithTcp = true;
			break;
		case MEDIA_STREAM_VIDEO_SKIP_TO_KEYFRAME:
			if (mDecoderVideo != NULL)
			{
				mDecoderVideo->setSkipUntilNextKeyFrame(true);
			}
			break;
		case MEDIA_STREAM_DISABLE_SYNC:
			if (mDecoderVideo != NULL)
			{
				log_debug("MediaPlayer disable sync");
				mDecoderVideo->setSyncMaster(AV_NO_SYNC);
			}
			break;
		case MEDIA_STREAM_ENABLE_SYNC:
			if (mDecoderVideo != NULL)
			{
				log_debug("MediaPlayer enable sync");
				if (mDecoderAudio != NULL)
				{
					mDecoderVideo->setSyncMaster(AV_SYNC_AUDIO_MASTER);
				}
				else
				{
					mDecoderVideo->setSyncMaster(AV_SYNC_EXTERNAL_MASTER);
				}
			}
			break;
		case MEDIA_STREAM_ADJUST_BITRATE:
			log_debug("MediaPlayer adjust bitrate");
			data_in_bytes = 0;
			t0 = av_gettime();
			break;
			//		case MEDIA_STREAM_ENABLE_LIB_YUV:
			//			log_debug("MediaPlayer enable libYUV");
			//			shouldUseLibYUV = true;
			//			break;
			//		case MEDIA_STREAM_DISABLE_LIB_YUV:
			//			log_debug("MediaPlayer disable libYUV");
			//			shouldUseLibYUV = false;
			//			break;
		case MEDIA_STREAM_TURN_ON_DEBUG_LOG:
			av_log_set_level(AV_LOG_DEBUG);
			break;
		case MEDIA_STREAM_TURN_OFF_DEBUG_LOG:
			av_log_set_level(AV_LOG_INFO);
			break;
		default :
			break;

		}
	}
	else
	{
		switch (opts)
		{
		case MEDIA_STREAM_SHOW_DURATION:
			shouldShowDuration = false;
			break;

		default :
			break;
		}
	}
	return 0 ;
}

//will be called from FFMpegPlayer java
int MediaPlayer::updateAudioClk(double pts)
{
#if DEBUG_UPDATE_AUDIO_CLK
	log_info("MediaPlayer update audio clk %f\n", pts);
#endif

	if (mDecoderAudio != NULL)
	{
		mDecoderAudio->updateClk(pts);
	}
	if (mDecoderAudio1 != NULL)
	{
		mDecoderAudio1->updateClk(pts);
	}

	return 0;
}

int MediaPlayer::getSnapShot(void** buffer, int* len)
{
	if (snapBuf == NULL)
	{
		return -1;
	}
	//	log_debug("Snapshot size: width: %d, height: %d\n",
	//			mVideoWidth, mVideoHeight);
	*len = avpicture_get_size(AV_PIX_FMT_ARGB,
			mVideoWidth,
			mVideoHeight);
	//log_debug("buffer len: %d\n", *len);
	*buffer = (void *) av_malloc(*len);
	memcpy(*buffer, snapBuf, *len);
	return 0;
}

void MediaPlayer::setEncryptionEnable(bool isEncrypted)
{
	isEncryptionEnable = isEncrypted;
}

bool MediaPlayer::isEncryptionEnabled()
{
	return isEncryptionEnable;
}

void MediaPlayer::setEncryptionKey(const char* encryption_key)
{
	this->encryption_key = (char *) malloc( (strlen(encryption_key) + 1) * sizeof(char));
	if (this->encryption_key != NULL)
	{
		strcpy(this->encryption_key, encryption_key);
		//log_info("set encryption key: %s\n", this->encryption_key);
	}
}

void MediaPlayer::setEncryptionIv(const char* encryption_iv)
{
	this->encryption_iv = (char *) malloc( (strlen(encryption_iv) + 1) * sizeof(char));
	if (this->encryption_iv != NULL)
	{
		strcpy(this->encryption_iv, encryption_iv);
		//log_info("set encryption key: %s\n", this->encryption_key);
	}
}

void MediaPlayer::setAudioEnabled(bool isEnabled)
{
	isAudioEnabled = isEnabled;
}

void MediaPlayer::setRecordModeEnabled(bool isEnabled)
{
  isRecordMode = isEnabled;
}

void MediaPlayer::setBufferSize(int size_in_kb)
{
	/* Usually 40kB for local, and 120kB for remote. */
	if (size_in_kb >= 0 && size_in_kb <= 200)
	{
		ffmpeg_buffer_size_in_kb = size_in_kb;
	}
	else
	{
		log_info("Invalid buffer size, use default settings.\n");
	}
}

/**
 * Return buffer size in number of audio packets.
 * size: size in kB
 */
int MediaPlayer::calculateAudioBufferSize(int size)
{
	return size * 2;
}

/**
 * Return buffer size in number of audio packets,
 * assume gop_size 14 pkts ~ 75kB
 * size: size in kB
 */
int MediaPlayer::calculateVideoBufferSize(int size)
{
	int nb_pkts = (int) (((double) size / (double) 75) * 14);
	return nb_pkts;
}

void MediaPlayer::setP2pSessionCount(int p2p_count)
{
  this->p2pSessionCount = p2p_count;
  log_info("Set p2p session count %d\n", p2pSessionCount);
}

void MediaPlayer::setRmcChannelInfo(void *rmcChannelInfo)
{
	this->mRmcChannelInfo = rmcChannelInfo;
}

void MediaPlayer::setUIDs(char **UIDs, int numId)
{
	mNumId = numId;
	if (mNumId > 4)
	{
		mNumId = 4;
	}

	for (int i=0; i<mNumId; i++)
	{
		UID[i] = (char *) malloc(strlen(UIDs[i]) + 1);
		if (!UID[i])
		{
			log_info("Cannot allocate UID[%d]\n", i);
			continue;
		}
		strcpy(UID[i], UIDs[i]);
	}
}

void MediaPlayer::sendCommand(const char *request, char **response)
{
	if (mRmcClient != NULL)
	{
	  mRmcClient->sendCommand(request, response);
	}
}

void MediaPlayer::sendTalkbackData(uint8_t *talkbackData, int offset, int length)
{
	if (mRmcClient != NULL)
	{
	  mRmcClient->sendTalkbackData(talkbackData, offset, length);
	}
}

bool MediaPlayer::isP2pConnected()
{
	bool is_connected = true;
	return is_connected;
}

void MediaPlayer::setP2pPlayByTimestampEnabled(bool isEnabled)
{
  this->mIsP2pPlayByTimestampEnabled = isEnabled;
}

void MediaPlayer::setBackgroundModeEnabled(bool isEnabled)
{
  this->mIsBackgroundModeEnabled = isEnabled;
}

void MediaPlayer::setProbeSize(long probeSize)
{
	this->mProbeSize = probeSize;
}
