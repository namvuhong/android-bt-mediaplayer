#include <stdlib.h>
#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#include <android/bitmap.h>
#endif



extern "C" {

#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/time.h"

} // end of extern C


#include "output.h"


#define LOG_TAG "FFMpegMediaPlayer-native"
#define LOG_LEVEL 10


#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
#define LOGI(level, ...) if (level <= LOG_LEVEL) {printf(__VA_ARGS__);}
#endif

#define USE_ANATIVE_WINDOW 1

jmethodID Output::audio_write_cb;
jmethodID Output::audio_stop_cb;
jmethodID Output::audio_mute_cb;
jmethodID Output::get_audio_buff_cb;
jmethodID Output::get_audio_unread_buff;

jmethodID Output::audio_flush_buff_cb;
jmethodID Output::audio_process_cb;
jmethodID Output::is_audio_buff_full;

jmethodID Output::audio_pause_cb;
jmethodID Output::audio_resume_cb;

jmethodID Output::native_getNewBitMap;
jmethodID Output::native_updateVideoSurface;

jobject Output::mediaPlayerObject;

void * Output::pictureRGB = NULL;
ANativeWindow* Output::theNativeWindow;
ANativeWindow_Buffer * Output::pictureBuffer = NULL;
jbyte* Output::g_buffer;

jmethodID Output::surface_init_cb;
jmethodID Output::surface_render_cb;
jmethodID Output::surface_release_cb;

int Output::AudioDriver_register(JNIEnv * env, jobject thiz)
{

#ifdef ANDROID_BUILD

    JNIEnv * env_ = env;
    if (mediaPlayerObject == NULL) {
      mediaPlayerObject = env_->NewGlobalRef(thiz);
    }

    jclass clazz;
    clazz = env_->FindClass("com/media/ffmpeg/FFMpegPlayer");

    //Setup audio buffer playback call back

    audio_flush_buff_cb = env->GetMethodID(clazz, "flushPCMBuffer", "()V");
    if (audio_flush_buff_cb == 0)
    {
    	return -1;
    }

    audio_mute_cb = env->GetMethodID(clazz, "setAudioStreamMuted", "(Z)Z");
    if (audio_mute_cb == 0)
    {
      return -1;

    }

    get_audio_buff_cb = env->GetMethodID(clazz, "get_audio_buff","(I)[B");
    if (get_audio_buff_cb == 0)
    {
        return -1;
    }

    audio_pause_cb = env->GetMethodID(clazz, "pausePCMPlayer","()V");
    if (audio_pause_cb == 0)
    {
    	return -1;
    }

    audio_resume_cb = env->GetMethodID(clazz, "resumePCMPlayer","()V");
    if (audio_resume_cb == 0)
    {
    	return -1;
    }

    audio_stop_cb = env->GetMethodID(clazz, "stopPCMPlayer","()V");
    if (audio_stop_cb == 0)
    {
    	return -1;
    }

    //audio_process_cb = env->GetMethodID(clazz, "process_audio_buff_callback","(I)I");
    audio_process_cb = env->GetMethodID(clazz, "process_audio_buff_callback_with_pts","(ID)I");
     if (audio_process_cb == 0)
    {
        return -1;
    }

     get_audio_unread_buff = env->GetMethodID(clazz, "get_unread_buff_len","()J");

     if (get_audio_unread_buff == 0)
     {
    	 return -1;
     }

     is_audio_buff_full = env->GetMethodID(clazz, "is_audio_buff_full", "()I");
     if (is_audio_buff_full == 0)
     {
    	 return -1;
     }

#endif
	return 0 ; // not supported
}

int Output::AudioDriver_unregister()
{
//#ifdef ANDROID_BUILD
//	JNIEnv *env_ = getJNIEnv();
//	if (mediaPlayerObject != NULL) {
//		env_->DeleteGlobalRef(mediaPlayerObject);
//		mediaPlayerObject = NULL;
//	}
//#endif
//    LOGI(9,"Output::AudioDriver_unregister");
    return 0;
}

int Output::AudioDriver_start()
{
	//return AndroidAudioTrack_start();

    LOGI(9,"Output::AudioDriver_start");
    return 0;
}

int Output::AudioDriver_pause()
{
#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL) {
	    JNIEnv *env_ = getJNIEnv();
	    //LOGI(9,"Output::AudioDriver_pause");
	    env_->CallVoidMethod(mediaPlayerObject, audio_pause_cb);
	}
#endif
	return 0;
}

int Output::AudioDriver_resume()
{
#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL) {
	    JNIEnv *env_ = getJNIEnv();
	    //LOGI(9,"Output::AudioDriver_resume");
	    env_->CallVoidMethod(mediaPlayerObject, audio_resume_cb);
	}
#endif
	return 0;
}

int Output::AudioDriver_set(int streamType,
							uint32_t sampleRate,
							int format,
							int channels)
{
    LOGI(9,"Output::AudioDriver_set: str Type: %d, sampleRate: %d, format:%d, channels:%d",
            streamType, sampleRate,format,channels);

//	return AndroidAudioTrack_set(streamType,
//								 sampleRate,
//								 format,
//								 channels);
    return 0;
}

int Output::AudioDriver_flush()
{
#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL)
	{
		JNIEnv *env_ = getJNIEnv();
		LOGI(9,"Output::AudioDriver_flush");
		env_->CallVoidMethod(mediaPlayerObject, audio_flush_buff_cb);
	}
#endif

    return 0;
}

int Output::AudioDriver_stop()
{

#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL)
	{
		JNIEnv *env_ = getJNIEnv();
        LOGI(9,"Output::AudioDriver_stop");
		if (audio_stop_cb != NULL) {
			env_->CallVoidMethod(mediaPlayerObject, audio_stop_cb);
		}
	}
#endif
    return 0;
}

int Output::AudioDriver_reload()
{

    LOGI(9,"Output::AudioDriver_reload");
//	return AndroidAudioTrack_reload();
    return 0;
}

//int Output::AudioDriver_write(void *buffer, int buffer_size)
int Output::AudioDriver_write(void *buffer, int buffer_size, double pts)
{
    int ret = 0;
#ifdef ANDROID_BUILD
    if (mediaPlayerObject != NULL) {
    JNIEnv *env_ = getJNIEnv();
    //Throw the buffer back to java.

    jbyteArray bArray = (jbyteArray) env_->CallObjectMethod(mediaPlayerObject,get_audio_buff_cb,buffer_size);

    env_->SetByteArrayRegion(bArray, 0, buffer_size, (jbyte*)buffer);

    // VERY IMPORTANT----LiNE
    //Fixed for  ReferenceTable overflow (max=512) ERROR --- DONT UNDERSTAND though
    env_->DeleteLocalRef(bArray);

    //LOGI(9,"Output::audio_write 0666");

    ret = env_->CallIntMethod(mediaPlayerObject,audio_process_cb,buffer_size, pts);


    }
#endif
    return ret ;
}

long Output::AudioDriver_buff_size()
{
	long currentUnreadBuff = 0;
#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL) {
		JNIEnv *env_ = getJNIEnv();
		currentUnreadBuff =  env_->CallLongMethod(mediaPlayerObject,get_audio_unread_buff);

	}
#endif

	return currentUnreadBuff;
}

int Output::AudioDriver_isAudioBuffFull()
{
	int isFull = 0;
#ifdef ANDROID_BUILD
	if (mediaPlayerObject != NULL) {
	JNIEnv *env_ = getJNIEnv();
	isFull =  env_->CallIntMethod(mediaPlayerObject, is_audio_buff_full);

	}
#endif
	return isFull;
}

int Output::AudioDriver_setAudioStreamMute(bool isMuted)
{
#ifdef ANDROID_BUILD
  JNIEnv *env_ = getJNIEnv();
  env_->CallBooleanMethod(mediaPlayerObject, audio_mute_cb, isMuted);

#endif
  return 0;
}

//-------------------- Video driver --------------------

int Output::VideoDriver_register(JNIEnv* env, jobject thiz, jobject jsurface)
{
#if ANDROID_BUILD

	mediaPlayerObject = env->NewGlobalRef(thiz);

	jclass clazz;
	clazz = env->FindClass("com/media/ffmpeg/FFMpegPlayer");

	//Setup audio buffer playback call back
	native_getNewBitMap = env->GetMethodID(clazz, "native_getNewBitMap", "()Landroid/graphics/Bitmap;");
	if (native_getNewBitMap == 0)
	{
		LOGI(9,"Output::VideoDriver_register 01");
		return -1;
	}

	native_updateVideoSurface = env->GetMethodID(clazz, "native_updateVideoSurface","(Landroid/graphics/Bitmap;)V");
	if (native_updateVideoSurface == 0)
	{

		LOGI(9,"Output::VideoDriver_register 02");
		return -1;
	}

#if USE_ANATIVE_WINDOW
	/* IMPORTANT: donot get ANativeWindow if you want to use
	 * Canvas API because it could cause lockCanvas return null. */
	if (jsurface != NULL)
	{
		// obtain a native window from a Java surface
		theNativeWindow = ANativeWindow_fromSurface(env, jsurface);
		//int32_t ANativeWindow_getWidth(ANativeWindow* window);
		//int32_t ANativeWindow_getHeight(ANativeWindow* window);

		int width = ANativeWindow_getWidth (theNativeWindow);
		int height = ANativeWindow_getHeight(theNativeWindow);
		LOGI(9,"Output::VideoDriver_register surface: w:%d h:%d", width, height);
	}

	//int32_t ANativeWindow_getWidth(ANativeWindow* window);
	//int32_t ANativeWindow_getHeight(ANativeWindow* window);

#else
	surface_init_cb = env->GetMethodID(clazz, "surfaceInit","(II)Ljava/nio/ByteBuffer;");
	if (surface_init_cb == 0)
	{
		LOGI(9,"Output::VideoDriver_register surface_init_cb is null");
		return -1;
	}
	surface_render_cb = env->GetMethodID(clazz, "surfaceRender","()V");
	if (surface_render_cb == 0)
	{
		LOGI(9,"Output::VideoDriver_register surface_render_cb is null");
		return -1;
	}
	surface_release_cb = env->GetMethodID(clazz, "surfaceRelease","()V");
	if (surface_release_cb == 0)
	{
		LOGI(9,"Output::VideoDriver_register surface_release_cb is null");
		return -1;
	}
#endif

#endif

	return 0;
}

int Output::VideoDriver_unregister()
{

#if ANDROID_BUILD
    LOGI(9,"Output::VideoDriver_unregister");
    // make sure we don't leak native windows
#if USE_ANATIVE_WINDOW
    if (theNativeWindow != NULL) {
        ANativeWindow_release(theNativeWindow);
        theNativeWindow = NULL;
    }
#endif

    if (pictureRGB != NULL)
    {
    	av_free(pictureRGB);
    	pictureRGB = NULL;
    }

    if (pictureBuffer != NULL)
    {
    	free(pictureBuffer);
    	pictureBuffer = NULL;
    }

//    LOGI(9,"mediaPlayerObject is NULL? %d\n",
//    		(mediaPlayerObject==NULL));
//    if (mediaPlayerObject != NULL)
//    {
//    	JNIEnv *env = getJNIEnv();
//#if !USE_ANATIVE_WINDOW
//    	env->CallVoidMethod(mediaPlayerObject, surface_release_cb);
//    	g_buffer = NULL;
//#endif
//
//    	env->DeleteGlobalRef(mediaPlayerObject);
//    	mediaPlayerObject = NULL;
//    }

#endif
    return 0;
}

int Output::VideoDriver_getPixels(int width, int height, void** pixels_out)
{

    LOGI(9,"Output::VideoDriver_getPixels: w:%d, h:%d", width, height);

#if ANDROID_BUILD

#if !USE_ANATIVE_WINDOW
    JNIEnv *env = getJNIEnv();
    jobject buf = env->CallObjectMethod(mediaPlayerObject, surface_init_cb, width, height);
    if (buf == NULL)
    {
    	LOGI(9,"Output::VideoDriver_getPixels buf is null");
    	return -1;
    }
    g_buffer = (jbyte*) env->GetDirectBufferAddress(buf);
    if (g_buffer == NULL)
    {
    	LOGI(9,"Output::VideoDriver_getPixels g_buffer is null");
    	return -1;
    }
#endif

    //pictureRGB;
    int size;

    // Determine required buffer size and allocate buffer
    size = avpicture_get_size(AV_PIX_FMT_ARGB, width, height);
    if (pictureRGB != NULL)
    {
      LOGI(9,"Output::VideoDriver_getPixels release old pictureRGB");
      av_free(pictureRGB);
    }
    pictureRGB  = av_malloc(size);
    if (!pictureRGB)
        return -1;


    *pixels_out = pictureRGB;

    if (pictureBuffer != NULL)
    {
      LOGI(9,"Output::VideoDriver_getPixels release old pictureBuffer");
      free(pictureBuffer);
    }
    pictureBuffer = (ANativeWindow_Buffer *) malloc( sizeof(ANativeWindow_Buffer));
    pictureBuffer->width = width;
    pictureBuffer->height = height;
    pictureBuffer->format = WINDOW_FORMAT_RGBA_8888;
    //pictureBuffer->format = WINDOW_FORMAT_RGB_565;
    pictureBuffer->bits =  malloc(width*height*sizeof(uint32_t));

    if (theNativeWindow != NULL)
    {
    	ANativeWindow_setBuffersGeometry(theNativeWindow,
    			pictureBuffer->width, pictureBuffer->height,
				pictureBuffer->format);
    }

    //LOGI(9,"Output::VideoDriver_getPixels: picRGB size: %d bitsize:%d",size, width*height*sizeof(uint16_t));

#endif
    return 0;
}

int Output::VideoDriver_getSnapShot(void** buffer, int* len)
{
#if ANDROID_BUILD
	*len = avpicture_get_size(AV_PIX_FMT_ARGB, pictureBuffer->width, pictureBuffer->height)
			* sizeof(uint32_t);
	*buffer = (void *) av_malloc(*len);
	memcpy(*buffer, pictureRGB, *len);
#endif
	return 0;
}

int Output::VideoDriver_getHeight()
{
	//LOGI(9,"VideoDriver_getHeight: %d\n", ANativeWindow_getHeight(theNativeWindow));
	return ANativeWindow_getHeight(theNativeWindow);
	//return 360;//  ANativeWindow_getHeight(theNativeWindow);
}

int Output::VideoDriver_getWidth()
{
    int width = ANativeWindow_getWidth(theNativeWindow);

    //int width = 640;// width - (width % 16 );
    //LOGI(9,"VideoDriver_getWidth: %d\n", width);

	return width;
}

int Output::VideoDriver_updateSurface()
{
#if ANDROID_BUILD
	//Another way: update pictureRGB directly to theNativeWindow
	int height = pictureBuffer->height;
	int width = pictureBuffer->width;

	if ( 0 > ANativeWindow_lock( theNativeWindow, pictureBuffer, NULL ) )
	{
		return 0;
	}
	//pictureBuffer->stride = ANativeWindow_getWidth(theNativeWindow);
	//Copy picture
	//LOGI(9,"VideoDriver_updateSurface: height %d, width %d,", height, width);
	int64_t start = av_gettime();

	for (int h = 0; h < height ; h++)
	{
		memcpy(pictureBuffer->bits + h * pictureBuffer->stride * 4,
				pictureRGB + h * width * 4 ,
				width * 4);
	}
	/*
			memcpy(pictureBuffer->bits, pictureRGB,
					pictureBuffer->width* pictureBuffer->height* sizeof(uint32_t));
	 */
	ANativeWindow_unlockAndPost( theNativeWindow );

	int total = (av_gettime() - start) / 1000;
	//			LOGI(9,"VideoDriver_updateSurface render time %d.", total);
	//LOGI(9,"VideoDriver_updateSurface: width %d, height %d, stride %d, render time %d\n",
	//		ANativeWindow_getWidth(theNativeWindow), ANativeWindow_getHeight(theNativeWindow), pictureBuffer->stride, total );
	/*
			 if (g_buffer != NULL)
			 {
			 //g_buffer is the buffer that will contains the bitmap pixels.
			 // Filled it and call the JNI method to render the image to Surface View.
			 memcpy(g_buffer, pictureRGB, pictureBuffer->width* pictureBuffer->height* sizeof(uint32_t)); // RGB565 pixels
			 JNIEnv *env = getJNIEnv();
			 env->CallVoidMethod(mediaPlayerObject, surface_render_cb);
			 }
	 */
#endif
	return 0;
}

int Output::unregister()
{
#if ANDROID_BUILD
    LOGI(9,"Output::unregister player instance");
    if (mediaPlayerObject != NULL)
    {
    	JNIEnv *env = getJNIEnv();
    	env->DeleteGlobalRef(mediaPlayerObject);
    	mediaPlayerObject = NULL;
    }
    LOGI(9,"Output::unregister player instance DONE");
#endif
	return 0;
}
