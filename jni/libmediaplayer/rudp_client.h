#ifndef __RUDP_CLIENT_H__
#define __RUDP_CLIENT_H__

extern "C" {
#include "inc/RMC.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
}
#include <unistd.h>
#include <pthread.h>
#include "output.h"
#include "decoder_video1.h"
#include "decoder_audio1.h"
#include "recorder_video.h"

class MediaPlayer;

#define MAX_CONN 2

typedef enum P2pMode
{
	P2P_MODE_LOCAL = 0,
	P2P_MODE_REMOTE = 1,
	P2P_MODE_RELAY = 2
} P2pMode;

typedef struct ConnectionInfo {
	char *src_ip;
	int port;
	int sock_fd;
	enum P2pMode p2p_mode;
	char *random_number;
	char *mac;
	char *enc_key;
	void *sess;
	MediaPlayer *event_listener;
	bool isConnected;
} ConnectionInfo;

class RUDPClient
{
public:
	RUDPClient();
	~RUDPClient();
	void init();
	void start();
	void stop();
	void setDecoderVideo(DecoderVideo1* decoder_video1);
	void setDecoderAudio(DecoderAudio1* decoder_audio1);
	void setRecorders(RecorderVideo *rVideo, RecorderAudio *rAudio);
	void setSourceIp(char *sourceIp);
	void setUserIp(char *userIp);
	void setVideoSourcePort(uint32_t sourcePort);
	void setVideoReceiverPort(uint32_t receiverPort);
	void setAudioSourcePort(uint32_t sourcePort);
	void setAudioReceiverPort(uint32_t receiverPort);
	void setInLocal(bool isInLocal);
	void setConnInfoCount(int connInfoCount);
	void setConnInfo(ConnectionInfo** connInfos);
	void setConnInfoDefaultParams(ConnectionInfo *connInfo);
	void copyConnInfo(ConnectionInfo *dst, ConnectionInfo *src);
	void freeConnInfo(ConnectionInfo *connInfo);
	int getCurrentVideoBitrate();
	void sendCommand(const char *request, char **response);
	void handShake(ConnectionInfo *handShakeInfo);
	void sendTalkbackData(uint8_t *talkbackData, int offset, int length);
	bool isP2pHandshakeSucceeded();
	void startRecording();
	void stopRecording();

private:
	//void decodeTask(void* data, uint32_t size);
	pthread_mutex_t             mLock;
	pthread_t					mVideoThread;
	pthread_t					mVideoTimerThread;
	static void*				video_worker_thread(void *ptr);
	static void*				sendRelayCmd(ConnectionInfo *connInfo, eMediaSubType cmd_type);
	static int					tryMultiSessionHandshake();
	static bool					tryConnectToDestAddress(int sock_fd, char *dest_ip, int dest_port);

	bool initialized;
	char* src_ip;
	uint32_t data_port, data_fd, ack_port, ack_fd;
	static int getSessionIndex();
	static void* command_worker_thread(void *ptr);
	static void* video_timer_task(void *ptr);
};

#endif /* __RUDP_CLIENT_H__ */
