#include "decoder_video1.h"
#include "recorder_video.h"
#include "output.h"

#include <unistd.h>

#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#endif


#define TAG "FFMpegVideoDecoder"
#define DEBUG_VIDEO_SYNC 0
#define DEBUG_CHECK_VIDEO_STREAM_ALIVE 0
#define DEBUG_VIDEO_CLK 0
#define DEBUG_PROCESSING_PKT_TIME 0
#define FFMPEG_FILTER_CORRUPTED_FRAME 0
#define DEBUG_FILTER_CORRUPTED_FRAME 0
#define DEBUG_EXTEND_TIMEOUT 0

#define DEFAULT_CONST_DELAY 0.04
#define MAX_VIDEO_DECODER_TIMEOUT 7
#define EXTEND_TIMEOUT 4

#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
#define log_info(...) fprintf(stdout, __VA_ARGS__)

#define FFMAX(a,b) ((a) > (b) ? (a) : (b))
#define FFMIN(a,b) ((a) > (b) ? (b) : (a))


extern void print( int level, char * format, ...);
#define LOGI print

#endif
static uint64_t global_video_pkt_pts = AV_NOPTS_VALUE;


//DecoderVideo1::DecoderVideo1(AVCodecContext* avctx) : IDecoder(avctx)
//{
////	mStream->codec->get_buffer = getBuffer;
////	mStream->codec->release_buffer = releaseBuffer;
//
//	mShowingIFrame = false;
//	mSkipUntilNextKeyFrame = true;
//	/*
//	 * Default FFBufQueue size is 32
//	 * To change FFBufQueue size: redefine FF_BUFQUEUE_SIZE
//	 */
//	clock_sync = AV_SYNC_AUDIO_MASTER;//AV_NO_SYNC;AV_SYNC_AUDIO_MASTER
//	shouldUpdateClockDiff = false;
//	mIsP2pPlayByTimestampEnabled = false;
//	mIsP2pFrameFilteringEnabled = true;
//	max_timeout = MAX_VIDEO_DECODER_TIMEOUT;
//}

DecoderVideo1::DecoderVideo1(AVStream* stream) : IDecoder(stream)
{
//  mStream->codec->get_buffer = getBuffer;
//  mStream->codec->release_buffer = releaseBuffer;

	mShowingIFrame = false;
	mSkipUntilNextKeyFrame = true;
	/*
	 * Default FFBufQueue size is 32
	 * To change FFBufQueue size: redefine FF_BUFQUEUE_SIZE
	 */
	clock_sync = AV_SYNC_AUDIO_MASTER;//AV_NO_SYNC;AV_SYNC_AUDIO_MASTER
	shouldUpdateClockDiff = false;
	mIsP2pPlayByTimestampEnabled = false;
	mIsP2pFrameFilteringEnabled = true;
	max_timeout = MAX_VIDEO_DECODER_TIMEOUT;
}

DecoderVideo1::~DecoderVideo1()
{

	if (mFrame)
	{
		av_frame_free(&mFrame);
	}

	if (mStream) {
		if (mStream->codec)
		{
			avcodec_close(mStream->codec);
		}
	}

	if (avctx)
	{
		avcodec_close(avctx);
	}

	log_info("DecoderVideo1 destroyed...done \n");
}

void DecoderVideo1::setP2pPlayByTimestampEnabled(bool isEnabled) {
  this->mIsP2pPlayByTimestampEnabled = isEnabled;
}

bool DecoderVideo1::prepare()
{
	mFrame = av_frame_alloc();
	if (mFrame == NULL) {
		return false;
	}

	frame_timer = (double) av_gettime() / 1000000.0;
	frame_last_pts = 0;
	frame_last_delay = DEFAULT_CONST_DELAY;
	//last_updated = (double) av_gettime() / 1000000.0;
	return true;
}

double DecoderVideo1::synchronize(AVFrame *src_frame, double pts) {

	double frame_delay;

	if (pts != 0) {
		/* if we have pts, set video clock to it */
		mVideoClock = pts;
	} else {
		/* if we aren't given a pts, set it to the clock */
		pts = mVideoClock;
	}
	/* update the video clock */
	//frame_delay = av_q2d(mStream->codec->time_base);
	/* if we are repeating a frame, adjust clock accordingly */
	frame_delay += src_frame->repeat_pict * (frame_delay * 0.5);
	mVideoClock += frame_delay;
	return pts;
}



/* Return delay in Seconds */
double  DecoderVideo1::cal_delay(double pts)
{
	double actual_delay, delay, sync_threshold, ref_clock, diff = 0;

	//first time
//	if (frame_last_pts == 0)
//	{
//		frame_last_pts = pts;
//		return 0;
//	}

	if (pts > 0)
	{
		delay = pts - frame_last_pts; /* the pts from last time */
	}
	else
	{
		delay = frame_last_delay;
	}
	/* For playback: the pts diff could be > 200ms here,
	 * so we extend the upper bound. */
	if (delay <= 0 || delay >= 0.35)
	{
		/* if incorrect delay, maybe lost frame, use previous one */
		delay = frame_last_delay;
	}
	/* save for next time */
	frame_last_delay = delay;
	frame_last_pts = pts;
#if DEBUG_VIDEO_SYNC
	idecoder_log(NULL, 1, "cal_delay: pts diff %f", delay);
#endif

	if (clock_sync ==  AV_SYNC_EXTERNAL_MASTER)
	{
		ref_clock = (av_gettime() -  external_clock_time)/1000000.0 ;
		if (shouldUpdateClockDiff == true)
		{
			/* Recalculate clock_diff. */
			clock_diff = pts - ref_clock;
#if DEBUG_VIDEO_SYNC
			idecoder_log(NULL, 1, "cal_delay: re-calc clock_diff %f", clock_diff);
#endif
		}
	}
	else if (clock_sync == AV_SYNC_AUDIO_MASTER)//sync it to audio clock
	{
		/* update delay to sync to audio */
		if (audioDecoder == NULL)
		{
			//return 0;
			double time = (double) av_gettime() / 1000000.0;
			delay -= time - last_updated;
			if (delay < 0)
			{
				delay = 0;
			}
			return delay;
		}
		ref_clock = audioDecoder->get_audio_clock();
	}

	if (clock_sync != AV_NO_SYNC)
	{
		//diff = pts - ref_clock;
		/* clock_diff should be 0 if clock_sync = AUDIO_MASTER or NO_SYNC */
		diff = pts - (ref_clock + clock_diff);

#if DEBUG_VIDEO_SYNC
		idecoder_log(NULL, 1, "cal_delay: ref_clock %f, clock_diff %f", ref_clock, clock_diff);
#endif

		/* Skip or repeat the frame. Take delay into account
	 	 * FFPlay still doesn't "know if this is the best guess." */
		//sync_threshold = (delay > AV_SYNC_THRESHOLD_MIN) ? delay : AV_SYNC_THRESHOLD_MIN;
		sync_threshold = FFMAX(AV_SYNC_THRESHOLD_MIN, FFMIN(AV_SYNC_THRESHOLD_MAX, delay));
		if(fabs(diff) < AV_NOSYNC_THRESHOLD)
		{
			if(diff <= -sync_threshold)
			{
				//delay = 0;
				delay = 0.2 * delay;
				//delay = FFMAX(0, delay + diff);
			} else if(diff >= sync_threshold) {
				delay = 2 * delay;
			}
		}
		else
		{
#if DEBUG_VIDEO_SYNC
			idecoder_log(NULL, 1, "cal_delay: diff %f > AV_NOSYNC_THRESHOLD ", diff);
#endif
		}
	}

	if (shouldUpdateClockDiff == false)
	{
		double time = (double) av_gettime() / 1000000.0;
#if DEBUG_VIDEO_SYNC
  idecoder_log(NULL, 1, "cal_delay: decoding time %f", (time - mDecodePktStartTime));
#endif
		delay -= time - mDecodePktStartTime;
		if (delay < 0.01)
		{
			delay = 0.01;
		} else if (delay > 1.0) {
			delay = 1.0;
		}
	}
	else
	{
		// reset the flag to use next time.
		shouldUpdateClockDiff = false;
	}

	//frame_timer += delay;
#if DEBUG_VIDEO_SYNC
	idecoder_log(NULL, 1, "cal_delay: diff %f, delay %f", diff, delay);
	//    idecoder_log(NULL, 1, "cal_delay: frame_timer %f",
	//                frame_timer  );
#endif

	/* computer the REAL delay */
	//actual_delay = frame_timer - (av_gettime() / 1000000.0);

	return delay;
}

void DecoderVideo1::enqueuePacketToRecorder(AVPacket *packet) {
	if (mRecorder != NULL) {
		RecorderVideo *recorderVideo = (RecorderVideo *) mRecorder;
		AVPacket rPacket;
		av_new_packet(&rPacket, packet->size);
		rPacket.pts = packet->pts;
		rPacket.dts = packet->dts;
		rPacket.pos = packet->pos;
		rPacket.duration = packet->duration;
		rPacket.convergence_duration = packet->convergence_duration;
		rPacket.stream_index = recorderVideo->getVideoStream()->index;
		rPacket.flags = packet->flags;
		memcpy(rPacket.data, packet->data, packet->size);
//		__android_log_print(ANDROID_LOG_INFO, TAG,
//				"Enqueue video pkt VVVVV, pts: %lld pts_double: %f, timebase: %f, size %d, isKey? %d\n",
//				packet->pts, packet->pts*av_q2d(mStream->time_base), av_q2d(mStream->time_base), packet->size, packet->flags & AV_PKT_FLAG_KEY);
		recorderVideo->enqueue(&rPacket);
	}
}

bool DecoderVideo1::process(AVPacket *packet)
{
	bool result = false;
	int	completed = 0;
	double pts = 0;

#if DEBUG_VIDEO_SYNC
	log_info("Process pkt VVVVV, pts: %lld pts_double: %f, timebase: %f, size %d, isKey? %d\n",
    		packet->pts, packet->pts*av_q2d(mStream->time_base), av_q2d(mStream->time_base), packet->size, packet->flags & AV_PKT_FLAG_KEY);
#endif

    // Add video packet to recorder queue if any
    enqueuePacketToRecorder(packet);

	pts = packet->pts;

	/* PTS is in millisecond --> convert to second */
	pts /= 1000.0;
	mVideoClock = pts;

	/*
	 * 20160122: HOANG: Fix sync issue here.
	 * We have to update clock before starting decoding for calculating decoding time.
	 */
	mDecodePktStartTime = (double) av_gettime() / 1000000.0;

	// Decode video frame
	int status = avcodec_decode_video2(mStream->codec,
			mFrame,
			&completed,
			packet);

	double refresh_time = 0;

	refresh_time = cal_delay(pts);

	if (completed) {

#if FFMPEG_FILTER_CORRUPTED_FRAME
		H264Context *h264_ctx = (H264Context*) mStream->codec->priv_data;
		ERContext *err_ctx = (ERContext*) &h264_ctx->er;
		int decode_err = av_frame_get_decode_error_flags(mFrame);
//		FrameThreadContext *fctx = mStream->codec->thread_opaque;
//		int finished = fctx->next_finished;
//		PerThreadContext *p = &fctx->threads[fctx->next_decoding];
//
//		log_info("ERContext offset: %d\n", (int) err_ctx - (int) h264_ctx);
		/*
		 * 20131021: hoang: discard corrupt frame
		 * error_occurred = 0: no error while decoding
		 */
#if DEBUG_FILTER_CORRUPTED_FRAME
		log_info("err_occur: %d, err_count: %d, decode_err_flags: %d, isKey: %d\n",
				err_ctx->error_occurred, err_ctx->error_count, decode_err, mFrame->key_frame);
#endif

		if (err_ctx->error_occurred == 0 && decode_err == 0)
		//if (decode_err == 0)
		{
			if (mFrame->key_frame)
			{
				//decode key frame success --> reset
				mSkipUntilNextKeyFrame = false;
			}

			if (!mSkipUntilNextKeyFrame)
#endif
			{
				pts = synchronize(mFrame, pts);
				long sleep_time;
				refresh_time *= 1000000.00;//convert to usec
				if (mIsP2pPlayByTimestampEnabled) {
					sleep_time = (long) refresh_time;
				} else {
					double time = (double) av_gettime() / 1000000.0;
					double decodingTime = time - mDecodePktStartTime;
				    sleep_time = 60000 - (long) (decodingTime * 1000000);
				}
//				log_info("sleep_time %ld\n", sleep_time);

				int64_t start_time = av_gettime();
				//if (mFrame->key_frame == 1)
				{
					if (mShowingIFrame == true)
					{
						if (mFrame->key_frame == 1 )
						{
							onDecode(mFrame, pts);
						}
					}
					else
					{
						onDecode(mFrame, pts);
					}
				}
//				else
//				{
//					//log_info("Drop frame\n");
//				}

				/* update video clock to use later */
				updateClk(pts);

				long render_time = av_gettime() - start_time;
				if (sleep_time > render_time)
				{
					sleep_time -= render_time;
				}
				else
				{
					sleep_time = 0;
				}
#if DEBUG_VIDEO_SYNC
				idecoder_log(NULL, 1, "video: render_time %ld, sleep_time %ld\n",
						render_time, sleep_time);
#endif

				if (sleep_time >= 10000) {
					// 20161006: HOANG: for recording mode only, don't sleep
					if (!mRecordingModeOnly) {
						// Split sleep_time into multiple small part so that app can interrupt decode thread faster.
						long sleep_time_counter = sleep_time;
						while (!mInterrupted && sleep_time_counter > 0) {
							if (sleep_time_counter > 20000) {
								usleep(20000);
								sleep_time_counter -= 20000;
							} else {
								usleep(sleep_time_counter);
								sleep_time_counter = 0;
							}
						}
					}
				}

				result = true;
			}
#if FFMPEG_FILTER_CORRUPTED_FRAME
			else
			{
#if DEBUG_FILTER_CORRUPTED_FRAME
				//Skip until next key frame
				log_info("Skip frame\n");
#endif
			}
		}
		else
		{
			////Corrupted frame, should skip until next key frame
			mSkipUntilNextKeyFrame = true;
#if DEBUG_FILTER_CORRUPTED_FRAME
			log_info("Corrupted frame, should skip until next key frame\n");
#endif
		}

	}
	else
	{
		//Decode frame failed, should skip until next key frame
		mSkipUntilNextKeyFrame = true;
#if DEBUG_FILTER_CORRUPTED_FRAME
		log_info("Decode frame failed, should skip until next key frame\n");
#endif
#endif //#if FFMPEG_FILTER_CORRUPTED_FRAME
	}

	/* free frame buffer */
	av_frame_unref(mFrame);

//	log_info("DEBUG_VIDEO_SYNC, pts: %lld VVVVV size %d, isKey? %d\n",
//				packet->pts, packet->size, packet->flags & AV_PKT_FLAG_KEY);

	return result;
}


void DecoderVideo1::updateClk(double pts)
{
#if DEBUG_VIDEO_CLK
	log_info("Video decoder update clk %f\n", pts);
#endif

	last_updated = (double) av_gettime() / 1000000.0;
	if (max_timeout > MAX_VIDEO_DECODER_TIMEOUT) {
#if DEBUG_EXTEND_TIMEOUT
	  log_info("Current video decoder timeout is extended, reset it now\n");
#endif
	  max_timeout = MAX_VIDEO_DECODER_TIMEOUT;
	}
}

void DecoderVideo1::extendTimeout()
{
  if (max_timeout <= MAX_VIDEO_DECODER_TIMEOUT) {
#if DEBUG_EXTEND_TIMEOUT
    log_info("Extend video decoder timeout\n");
#endif
    max_timeout += EXTEND_TIMEOUT;
  }
}

bool DecoderVideo1::checkStreamAlive()
{
	bool isAlive = true;
	double now = (double) av_gettime() / 1000000.0;

#if DEBUG_CHECK_VIDEO_STREAM_ALIVE
	log_info("VVVVV time from last updated %f", (now - last_updated));
#endif

	if (last_updated == -1 || now - last_updated < max_timeout)
	{
		isAlive = true;
	}
	else
	{
		log_info("Video stream is not alive anymore.");
		isAlive = false;
	}

	return isAlive;
}

bool DecoderVideo1::decode(void* ptr)
{
	AVPacket        pPacket;
	//	mFrameBuf = (AVFrame **) av_malloc(FFMPEG_VIDEO_BUFFER_SIZE * sizeof(*mFrameBuf));
	//	mFrameBufSize = 0;
	//	*frame_queue.queue = (AVFrame*) av_mallocz(FF_BUFQUEUE_SIZE * sizeof(AVFrame*));
	log_info(">>>>> decoding video1 -- Attach VM to thread\n");
#ifdef ANDROID_BUILD
	if(attachVmToThread () != JNI_OK)
	{
		log_info("decoding video1 -- Attach VM to thread Failed");
		mInterrupted = true;
	}
#endif

	while (!mInterrupted)
	{
		while (!mInterrupted && isPause)
		{
//			log_info("video decoder is sleeping\n");
			usleep(100);
		}

		if(mQueue->get(&pPacket, true) < 0)
		{
			mInterrupted = true;
			continue;
		}

#if DEBUG_PROCESSING_PKT_TIME
		int64_t vt0 = av_gettime();
#endif
		//if (last_pts != pPacket.pts)
		{
			last_pts = pPacket.pts;
			if(!process(&pPacket))
			{
				//log_info("DecoderVideo::decode proccess return false:ignore");
				//mRunning = false;
				//continue;
			}
		}
#if DEBUG_PROCESSING_PKT_TIME
		int64_t vt1 = av_gettime();
		double process_time = (vt1 - vt0)/(double) AV_TIME_BASE;
		log_info("DecoderVideo processing pkt pts %f, time %f\n",
				pPacket.pts*av_q2d(mStream->time_base), process_time);
#endif
#ifdef USE_FFMPEG_3_0
		av_packet_unref(&pPacket);
#else
		// Free the packet that was allocated by av_read_frame
		av_free_packet(&pPacket);
#endif

	}

	log_info("decoding video1 ended -- Dettach VM fr Thread\n");
#ifdef ANDROID_BUILD
	dettachVmFromThread();
#endif
	return true;
}

void DecoderVideo1::setSkipUntilNextKeyFrame(bool shouldSkip)
{
	mSkipUntilNextKeyFrame = shouldSkip;
	log_info("setSkipUntilNextKeyFrame: %d", shouldSkip);
}

void DecoderVideo1::setPlayIFrameOnly(bool status)
{
	mShowingIFrame = status;

	log_info("setPlayIFrameOnly: %d", status);
	return;
}

void DecoderVideo1::dropToNextKeyFrame()
{
	if (mQueue)
	{
		mQueue->dropToNextKeyFrame();
	}
}

/* These are called whenever we allocate a frame
 * buffer. We use this to store the global_pts in
 * a frame at the time it is allocated.
 */
//int DecoderVideo1::getBuffer(struct AVCodecContext *c, AVFrame *pic) {
//	int ret = avcodec_default_get_buffer(c, pic);
//	uint64_t *pts = (uint64_t *)av_malloc(sizeof(uint64_t));
//	*pts = global_video_pkt_pts;
//	pic->opaque = pts;
//	return ret;
//}
//void DecoderVideo1::releaseBuffer(struct AVCodecContext *c, AVFrame *pic) {
//	if (pic)
//		av_freep(&pic->opaque);
//	avcodec_default_release_buffer(c, pic);
//}


