#ifndef __RMC_CLIENT_H__
#define __RMC_CLIENT_H__

extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "rmc_inc/rmc_channel.h"
#include "mediaplayer/g722_enc.h"
#include "mediaplayer/g722_dec.h"
}
#include <unistd.h>
#include <pthread.h>
#include "output.h"

#define MAX_CONN 2

typedef enum P2pMode
{
	P2P_MODE_LOCAL = 0,
	P2P_MODE_REMOTE = 1,
	P2P_MODE_RELAY = 2
} P2pMode;

#define WORKING_MODE_LIVE_STREAMING 0
#define WORKING_MODE_PLAYBACK 1
#define WORKING_MODE_RECORDING_ONLY 2

typedef struct P2pContext {
	g722_enc_t g722Enc;
	void *rmcChannel;
	void *decoderVideo;
	void *decoderAudio;
	void *recorderVideo;
	void *recorderAudio;
	int recordingVideoIdx; // use for recording
	int isRunning;
	int isRecording;
	int isFileOpen;
	int workingMode;
	int isPlaybackCompleted;
	FILE *file;
	int keyFrameTotal;
	int currentRmcBandwidth;
	pthread_t videoTimerThread;
	void *eventListener;
	int64_t firstVideoTs;
	uint8_t out_buf[4 * 1024];
	int16_t dec_buf[4 * 1024];
} P2pContext;

class RmcClient
{
public:
	RmcClient();
	~RmcClient();
	void init();
	void start();
	void stop();
	void setDecoderVideo(void *decoderVideo);
	void setDecoderAudio(void *decoderAudio);
	void setRecorders(void *rVideo, void *rAudio);
	int getCurrentVideoBitrate();
	int getCurrentRmcBandwidth();
	void sendCommand(const char *request, char **response);
	void sendTalkbackData(uint8_t *talkbackData, int offset, int length);
	void startRecording();
	void stopRecording();
	void setRmcChannelInfo(void *rmcChannelInfo);
	void setEventListener(void *eventListener);
	void setWorkingMode(int workingMode);
	int64_t getFirstVideoTs();

private:
	//void decodeTask(void* data, uint32_t size);
	P2pContext *mP2pCtx;

	bool initialized;
	static void* video_timer_task(void *ptr);
	static void* fileTransferStatusTask(void *ptr);
	void initP2pContext();
	void destroyP2pContext();
};

#endif /* __RMC_CLIENT_H__ */
