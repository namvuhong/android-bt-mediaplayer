#ifndef FFMPEG_MEDIAPLAYER_H
#define FFMPEG_MEDIAPLAYER_H

#include <pthread.h>

#ifdef ANDROID_BUILD
#include <jni.h>
#endif

#include <android/Errors.h>
#include "decoder_audio.h"
#include "decoder_audio1.h"
#include "decoder_video.h"
#include "decoder_video1.h"
#include "recorder_audio.h"
#include "recorder_video.h"
#include "rmc_client.h"
//#include "AVAPIs_Client.h"

#if !defined(ANDROID_BUILD)

#warning "ANDROID_BUILD not defined"
#define JNIEnv void
#define jobject void *

#endif

#define GOP_SIZE 14
#define FFMPEG_PLAYER_MAX_QUEUE_SIZE 48
#define FFMPEG_PLAYBACK_MAX_QUEUE_SIZE 200

#define FFMPEG_PLAYER_LOCAL_MIN_AUDIO_QUEUE_SIZE 4
#define FFMPEG_PLAYER_LOCAL_MIN_VIDEO_QUEUE_SIZE 8
#define FFMPEG_PLAYER_LOCAL_MAX_AUDIO_QUEUE_SIZE 8
#define FFMPEG_PLAYER_LOCAL_MAX_VIDEO_QUEUE_SIZE 16

#define FFMPEG_PLAYER_REMOTE_MIN_AUDIO_QUEUE_SIZE 32
#define FFMPEG_PLAYER_REMOTE_MIN_VIDEO_QUEUE_SIZE 32
#define FFMPEG_PLAYER_REMOTE_MAX_AUDIO_QUEUE_SIZE 45
#define FFMPEG_PLAYER_REMOTE_MAX_VIDEO_QUEUE_SIZE 45

//user for playback
// buffer less than player to handle short clips
#define FFMPEG_PLAYBACK_MIN_AUDIO_QUEUE_SIZE 30
#define FFMPEG_PLAYBACK_MIN_VIDEO_QUEUE_SIZE 30

#define CORRUPT_FRAME_TIMEOUT 3
#define BUFFER_CHECKING_INTERVAL_IN_S 5

using namespace android;
enum media_event_type {
    MEDIA_NOP               = 0, // interface test message
    MEDIA_PREPARED          = 1,
    MEDIA_PLAYBACK_COMPLETE = 2,
    MEDIA_BUFFERING_UPDATE  = 3,
    MEDIA_SEEK_COMPLETE     = 4,
    MEDIA_SET_VIDEO_SIZE    = 5,
    MEDIA_VIDEO_STREAM_HAS_STARTED = 6,
    MEDIA_SEND_KEEP_ALIVE = 7,
    MEDIA_ERROR             = 100,
    MEDIA_INFO              = 200,
};

// Generic error codes for the media player framework.  Errors are fatal, the
// playback must abort.
//
// Errors are communicated back to the client using the
// MediaPlayerListener::notify method defined below.
// In this situation, 'notify' is invoked with the following:
//   'msg' is set to MEDIA_ERROR.
//   'ext1' should be a value from the enum media_error_type.
//   'ext2' contains an implementation dependant error code to provide
//          more details. Should default to 0 when not used.
//
// The codes are distributed as follow:
//   0xx: Reserved
//   1xx: Android Player errors. Something went wrong inside the MediaPlayer.
//   2xx: Media errors (e.g Codec not supported). There is a problem with the
//        media itself.
//   3xx: Runtime errors. Some extraordinary condition arose making the playback
//        impossible.
//
enum media_error_type {
    // 0xx
    MEDIA_ERROR_UNKNOWN = 1,
    // 1xx
    MEDIA_ERROR_SERVER_DIED = 100,
    MEDIA_ERROR_TIMEOUT_WHILE_STREAMING,
    // 2xx
    MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200,
    // 3xx
};


// Info and warning codes for the media player framework.  These are non fatal,
// the playback is going on but there might be some user visible issues.
//
// Info and warning messages are communicated back to the client using the
// MediaPlayerListener::notify method defined below.  In this situation,
// 'notify' is invoked with the following:
//   'msg' is set to MEDIA_INFO.
//   'ext1' should be a value from the enum media_info_type.
//   'ext2' contains an implementation dependant info code to provide
//          more details. Should default to 0 when not used.
//
// The codes are distributed as follow:
//   0xx: Reserved
//   7xx: Android Player info/warning (e.g player lagging behind.)
//   8xx: Media info/warning (e.g media badly interleaved.)
//
enum media_info_type {
    // 0xx
    MEDIA_INFO_UNKNOWN = 1,
    // 7xx
    // The video is too complex for the decoder: it can't decode frames fast
    // enough. Possibly only the audio plays fine at this stage.
    MEDIA_INFO_VIDEO_TRACK_LAGGING = 700,
    // 8xx
    // Bad interleaving means that a media has been improperly interleaved or not
    // interleaved at all, e.g has all the video samples first then all the audio
    // ones. Video is playing but a lot of disk seek may be happening.
    MEDIA_INFO_BAD_INTERLEAVING = 800,
    // The media is not seekable (e.g live stream).
    MEDIA_INFO_NOT_SEEKABLE = 801,
    // New media metadata is available.
    MEDIA_INFO_METADATA_UPDATE = 802,

    MEDIA_INFO_FRAMERATE_VIDEO = 900,
    MEDIA_INFO_FRAMERATE_AUDIO,
    MEDIA_INFO_VIDEO_SIZE,
    MEDIA_INFO_BITRATE_BPS,
    MEDIA_INFO_CORRUPT_FRAME_TIMEOUT,
    MEDIA_INFO_RECEIVED_VIDEO_FRAME,
    MEDIA_INFO_RECORDING_TIME,
    MEDIA_INFO_START_BUFFERING,
    MEDIA_INFO_STOP_BUFFERING,
	MEDIA_INFO_CLIP_URL,
	MEDIA_INFO_HANDSHAKE_RESULT,
	MEDIA_INFO_P2P_SESSION_INDEX,
	MEDIA_INFO_HANDSHAKE_FAILED
};



enum media_player_states {
    MEDIA_PLAYER_STATE_ERROR        = 0,
    MEDIA_PLAYER_IDLE               = 1 << 0,
    MEDIA_PLAYER_INITIALIZED        = 1 << 1,
    MEDIA_PLAYER_PREPARING          = 1 << 2,
    MEDIA_PLAYER_PREPARED           = 1 << 3,
	MEDIA_PLAYER_DECODED            = 1 << 4,
    MEDIA_PLAYER_STARTED            = 1 << 5,
    MEDIA_PLAYER_PAUSED             = 1 << 6,
    MEDIA_PLAYER_STOPPED            = 1 << 7,
    MEDIA_PLAYER_PLAYBACK_COMPLETE  = 1 << 8,
    MEDIA_PLAYER_SEEKING            = 1 << 9
};

enum interrupt_callback_id {
	INTERRUPT_CALLBACK_OFF = 0,
	INTERRUPT_CALLBACK_FIND_STREAM_INFO ,
	INTERRUPT_CALLBACK_OPEN_INPUT ,
	INTERRUPT_CALLBACK_AV_READ_FRAME,
	INTERRUPT_CALLBACK_SEEK_FILE
};

enum media_playback_status {
	MEDIA_PLAYBACK_STATUS_STARTED = 0,
	MEDIA_PLAYBACK_STATUS_IN_PROGRESS,
	MEDIA_PLAYBACK_STATUS_COMPLETE
};

enum media_runtime_opt
{
    MEDIA_STREAM_ALL_FRAME = 0,
    MEDIA_STREAM_IFRAME_ONLY ,
    MEDIA_STREAM_RTSP_WITH_TCP,
    MEDIA_STREAM_VIDEO_SKIP_TO_KEYFRAME,
    MEDIA_STREAM_DISABLE_SYNC,
    MEDIA_STREAM_ENABLE_SYNC,
    MEDIA_STREAM_ADJUST_BITRATE,
    MEDIA_STREAM_SHOW_DURATION,
	MEDIA_STREAM_TURN_ON_DEBUG_LOG,
	MEDIA_STREAM_TURN_OFF_DEBUG_LOG
};

// ----------------------------------------------------------------------------
// ref-counted object for callbacks
class MediaPlayerListener
{
public:
    virtual void notify(int msg, int ext1, int ext2) = 0;
    virtual int getNextClip(char** );
};

class MediaPlayer
{
public:
	MediaPlayer(bool forPlayback, bool forSharedCam);
	~MediaPlayer();
	status_t initFFmpegEngine();
	status_t setAudioBufferCallBack(JNIEnv* env, jobject mediaPlayerClass) ;

	status_t        setDataSource(const char *url);
	status_t        setVideoSurface(JNIEnv* env, jobject mediaPlayerClass, jobject jsurface);
	status_t        resetVideoSurface(JNIEnv* env, jobject mediaPlayerClass, jobject jsurface);
	status_t        setListener(MediaPlayerListener *listener);
	status_t        start();
	status_t		startRecord(const char * url);
	status_t		stopRecord();
	status_t        stop();
	status_t        pause();
	bool            isPlaying();
	bool			isRecording();
	status_t        getVideoWidth(int *w);
	status_t        getVideoHeight(int *h);
	status_t        seekTo(int msec);
	status_t        getCurrentPosition(int *msec);
	status_t        getCurrentStreamPosition(int *msec);
	status_t        getDuration(int *msec);
	status_t		setDuration(int msec);
	status_t        reset();
	status_t        setAudioStreamType(int type);
	status_t		prepare(JNIEnv *env, jobject thiz);
	void            notify(int msg, int ext1, int ext2);
	status_t        suspend();
	status_t        resume();
	time_t			getStartTime();
	void			setStartTime(time_t start_time);
	interrupt_callback_id getInterruptCallbackId();
	void			setInterruptCallbackId(interrupt_callback_id interrupt_cb_id);
	void			setFFmpegInterrupt(bool interrupt);
	int             setPlayOption(int opts);
	MediaPlayerListener* getListener();
	status_t			reconfigureVideo();
	int				getCurrentState();
	int					getSnapShot(void** buffer, int* len);
	int				updateAudioClk(double pts);
	bool			getExternalInterruptFlag();
	bool			isPlaybackMode();
	void			setEncryptionEnable(bool isEncrypted);
	bool			isEncryptionEnabled();
	void			setEncryptionKey(const char* encryption_key);
	void			setEncryptionIv(const char* encryption_iv);
	void			setAudioEnabled(bool isEnabled);
	void			setBufferSize(int size_in_kb);
	void			flushAllBuffers();
	void			checkAndFlushAllBuffers();
	void			setRmcChannelInfo(void *rmcChannelInfo);
	void			setUIDs(char **UIDs, int numId);
	void			sendCommand(const char *request, char **response);
	void			sendTalkbackData(uint8_t *talkbackData, int offset, int length);
	bool			isP2pConnected();
	void      setP2pSessionCount(int p2p_count);
	bool      getIsSdcardStreaming();
	int64_t   getRemainingDuration();
	void      setRecordModeEnabled(bool isEnabled);
	void      setP2pPlayByTimestampEnabled(bool isEnabled);
	void      setBackgroundModeEnabled(bool);
	void	  setProbeSize(long probeSize);

private:
	void						resumeDecoders();
	void						pauseDecoders();
	bool						checkStreamValid();
	AVDictionary 				**setup_find_stream_info_opts(AVFormatContext *s);
	status_t					prepareAudio(JNIEnv *env, jobject thiz);
	status_t					prepareVideo();
	status_t					prepareVideo2();
	bool						shouldCancel(PacketQueue* queue);
	static void					ffmpegNotify(void* ptr, int level, const char* fmt, va_list vl);
	static void*				startPlayer(void* ptr);
	static void*				startRecording(void* ptr);
	static void 				decode(AVFrame* frame, double pts);
    static void 				decode(uint8_t* buffer, int buffer_size, double pts);
    static void					record(AVFrame* frame, double pts);
    static void					record(uint8_t* buffer, int buffer_size);

    status_t					resetDataSource(const char *new_url);
    static void					mp_log(void* ptr, int level, const char* fmt, ...);
    static void*				videoTimerTask(void* ptr);
    static void*				videoBitrateTask(void* ptr);
    void						cancelVideoTimer();
    void						cancelVideoBitrateTask();
    static AVStream *           add_stream(AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id);


    int 						recordMovie(void* ptr);
	void						decodeMovie(void* ptr);
	int64_t						calculateRemainingTime();
	int							calculateAudioBufferSize(int size_in_kb);
	int							calculateVideoBufferSize(int size_in_kb);
	void						initRecorders();
	void						destroyRecorders();

	char*						url, * record_url, *encryption_key, *encryption_iv;
	double 						mTime;
	pthread_mutex_t             mLock;
	pthread_t					mPlayerThread;
	pthread_t					mRecordingThread;
	PacketQueue*				mVideoQueue;
    MediaPlayerListener*		mListener;
    AVFormatContext*			mMovieFile;
    AVFormatContext*			mOutputFile;
    char*						output_file;

    int 						mAudioStreamIndex;
    int 						mVideoStreamIndex;
    DecoderAudio*				mDecoderAudio;
	DecoderVideo*             	mDecoderVideo;
	DecoderVideo1*             	mDecoderVideo1;
	DecoderAudio1*				mDecoderAudio1;

    RecorderAudio *             mRecorderAudio;
    RecorderVideo *             mRecorderVideo;

	AVFrame*					mFrame;
	AVFrame*					decoded_frame;
	AVFrame*					mCurrentFrame; //use for take snapshot
	struct SwsContext*			mConvertCtx;
	uint8_t*					snapBuf; //buffer to save current frame pixel data

    void*                       mCookie;
    media_player_states         mCurrentState;
    int64_t                    	mDuration;
    int64_t						mFirstClipStartTime; // in microseconds
    int64_t                     mStartPosition; //in ms
    int							mCurrentPosition; //in milliseconds
    int64_t                     mSeekPosition;
    bool                        mPrepareSync;
    status_t                    mPrepareStatus;
    int                         mStreamType;
    int                         mVideoWidth;
    int                         mVideoHeight;
    bool                        ffmpegEngineInitialized;

    time_t 						startTime;
    interrupt_callback_id 		interrupt_cb_id;
    bool						shouldInterrupt;
    bool						externalInterruptSignal;
    bool                        mPlayBackOnly;
    bool						mForSharedCam;
    bool						mIsLocal;
    bool                        mRtspWithTcp;
    bool						firstKeyFrameDisplayed;

    pthread_t					mVideoTimerThread;
    bool						mVideoTimerRunning;
    bool						mReceiveDecodedFrame;
    bool						isLoadingDialogShowing;
    long            mVideoTimeoutStart;

    bool                        mStopRecording;
    int							ffmpeg_buffer_size, ffmpeg_video_buff_size, ffmpeg_audio_buff_size;
    int							max_video_buf_size, max_audio_buf_size;
    int64_t                     timeStartPause;
    int64_t                     timeTotalPause;
    int64_t                     timeDeltaSeek;
    bool						isEncryptionEnable;
    bool						isAudioEnabled;
    int							ffmpeg_buffer_size_in_kb;
    long						mProbeSize;
    double						last_video_pts;
    bool						mIsInPTZMode;
    int							decoded_frames;
    bool						shouldShowDuration;
    bool						shouldUpdateDuration;
    bool						shouldUseLibYUV;

    /* Use for P2P. */
    AVCodecContext				*videoCtx, *audioCtx;
    bool						isInLocal;

    pthread_t					mVideoBitrateThread;
    bool						mVideoBirateRunning;

    void *mRmcChannelInfo;
    RmcClient *mRmcClient;
    int p2pSessionCount;
//    AVAPIsClient *mAVAPIsClient;
    char *UID[4];
    int mNumId;

    /*
     * 20150624: hoang: fix multiclip duration issue for Focus86
     * The timestamp of focus86 clips is reset on every clip.
     * Player should padding this timestamp with accumulate duration.
     */
    int mAccumulateDuration; // in AV_TIME_BASE seconds unit
    int mPrevDuration; // the previous clip duration
    int mCurrDuration; // the current clip duration
    bool mIsSdcardStreaming; // sdcard streaming flag
    int64_t mCurrTimestamp; // the current timestamp of video in microseconds, used for sdcard streaming
    bool isRecordMode;
    bool mIsP2pPlayByTimestampEnabled;
    bool mIsBackgroundModeEnabled;
};

#endif // FFMPEG_MEDIAPLAYER_H
