#ifndef FFMPEG_DECODER_VIDEO_H
#define FFMPEG_DECODER_VIDEO_H

extern "C" {

//#include "libavfilter/bufferqueue.h"
#include "libavutil/time.h"
#include "libavcodec/error_resilience.h"
//#include "libavcodec/h264.h"
}


#include "decoder.h"
#include "decoder_audio.h"
#include <signal.h>
typedef void (*VideoDecodingHandler) (AVFrame*,double);

#define	FFMPEG_VIDEO_BUFFER_SIZE 8
#define NUM_OF_IFRAME_AND_ITS_PFRAME 8

//./ffplay.c:75:
#define AV_SYNC_THRESHOLD_MIN 0.01
#define AV_SYNC_THRESHOLD_MAX 0.1
#define AV_NOSYNC_THRESHOLD 10.0


typedef struct H264Context {
    AVCodecContext *avctx;
    char vdsp[8]; //VideoDSPContext vdsp;
    char h264dsp[132]; //H264DSPContext h264dsp;
    char h264chroma[32]; //H264ChromaContext h264chroma;
    char h264qpel[512]; //H264QpelContext h264qpel;
    char me[344]; //MotionEstContext me;
    char parse_context[40]; //ParseContext parse_context;
    char gb[20]; //GetBitContext gb;
    char dsp[1256]; //DSPContext dsp;
    ERContext er;
} H264Context;

class DecoderVideo : public IDecoder
{
public:
    DecoderVideo(AVStream* stream);
    ~DecoderVideo();
    DecoderAudio *              audioDecoder;
    VideoDecodingHandler		onDecode;
    void                        setPlayIFrameOnly(bool);
    void						setSkipUntilNextKeyFrame(bool);
    bool						checkStreamAlive();
    void						dropToNextKeyFrame();
    void increaseAccumulateTimeout(long delta_delay);

private:
	AVFrame*					mFrame;
	double						mVideoClock;
    int                        	mShowingIFrame;
    bool						mSkipUntilNextKeyFrame;
    bool                        mFirstKeyFrameDecoded;

    struct FFBufQueue 			frame_queue;
    bool                        prepare();
    double 						synchronize(AVFrame *src_frame, double pts);
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    static int					getBuffer(struct AVCodecContext *c, AVFrame *pic);
    static void					releaseBuffer(struct AVCodecContext *c, AVFrame *pic);

    double                      cal_delay(double pts);
    double cal_delay_playback(double pts);
    void						updateClk(double pts);
    void						enqueuePacketToRecorder(AVPacket *packet);

    double frame_last_pts;
    double frame_last_delay;
    double frame_timer;
    long mAccumulateTimeout;
    long mStartTimeDelta;

};

#endif //FFMPEG_DECODER_AUDIO_H
