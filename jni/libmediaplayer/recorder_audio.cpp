#include "recorder_audio.h"

#include <unistd.h>


#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#endif


#include "libavutil/mathematics.h"

#define TAG "FFMpegAudioRecorder"
#define LOG_TAG TAG

#define LOG_LEVEL 100

#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio


#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
#define log_info(...) printf(__VA_ARGS__)
//#define LOGI (level, format, ...)  if (level <= LOG_LEVEL) { print(format,__VA_ARGS__);}
#define LOGI print

void print( int level, char * format, ...)
{
    va_list args;

    va_start(args, format);
    vprintf(format, args);
    va_end(args);
}
#endif

#define DEBUG_RECORDING 0
#define DEBUG_WRITE_TO_FILE 0

/** use ffmpeg filter 'aresample, aformat' to resample  */
#define FILTER_DESC  "aresample=11025,aformat=sample_fmts=s16:channel_layouts=mono"


RecorderAudio::RecorderAudio(AVStream* stream) : IDecoder(stream)
{
    mInterrupted = false;
    first_ts_set = false;
}

RecorderAudio::RecorderAudio(AVCodecContext* avctx) : IDecoder(avctx)
{
	mInterrupted = false;
	first_ts_set = false;
}


RecorderAudio::~RecorderAudio()
{
    //if (mStream->codec)
	//{
	//	avcodec_close(mStream->codec);
	//}
}

void RecorderAudio::cleanUp()
{
	log_info("RecorderAudio clean up.");

    if (mRecordPacket != NULL)
    {
        av_free(mRecordPacket);
        mRecordPacket = NULL;
    }
    if (leftOverBuff != NULL)
    {
        av_free(leftOverBuff);
        leftOverBuff = NULL;
    }

    if (swr2 != NULL)
    {
        swr_free(&swr2);
    }

    //* should be done by free_context out in main loop
    //avcodec_close(adpcm_c);
    ///av_free(adpcm_c); */
    flush_all_audio();
    destroyBuffQueue();
}

bool RecorderAudio::prepare( AVFormatContext *oc, AVStream * in_stream)
{

    mSamplesSize = MAX_AUDIO_FRAME_SIZE;

    mRecordPacket = (uint8_t*) av_malloc(mSamplesSize);
    if(mRecordPacket == NULL) {
        return false;
    }

    leftOverBuff  =  (uint8_t*) av_malloc(mSamplesSize);
    if (leftOverBuff  == NULL)
    {
        return false;
    }
    leftOverLen = 0;

    src_stream =  in_stream;
    src_codec_context = in_stream->codec;
    encodeToAdpcm_init(oc);

    initBuffQueue();

    //!!! called after src_codec_context is set.
    if (( init_filters(FILTER_DESC)) < 0)
    {

        LOGI(10, "Failed to init filter!! ");
        return false;
    }



    return true;
}

bool RecorderAudio::prepare( AVFormatContext *oc)
{

    mSamplesSize = MAX_AUDIO_FRAME_SIZE;

    mRecordPacket = (uint8_t*) av_malloc(mSamplesSize);
    if(mRecordPacket == NULL) {
        return false;
    }

    leftOverBuff  =  (uint8_t*) av_malloc(mSamplesSize);
    if (leftOverBuff  == NULL)
    {
        return false;
    }
    leftOverLen = 0;

    src_stream = NULL;
    src_codec_context = avctx;
    encodeToAdpcm_init(oc);

    initBuffQueue();

    //!!! called after src_codec_context is set.
    if (( init_filters(FILTER_DESC)) < 0)
    {

        LOGI(10, "Failed to init filter!! ");
        return false;
    }



    return true;
}



/* Obsolete - do not use */
void RecorderAudio::write_audio_frame2(AVFormatContext *oc)
{
    AVFrame * mFrame = NULL;
    int decoded_len  ;
    int ret;

#if DEBUG_WRITE_TO_FILE //  write raw to file
    FILE * pFile ;
    pFile = fopen("audio.raw", "ab");

#endif

    LOGI(10, "getting Frame()\n");
    mFrame = getFrame() ;

    if (mFrame != NULL)
    {
        // Resample - output data is put into mRecordPacket
        ret = resample_audio_for_record(adpcm_c, src_codec_context , mFrame, &decoded_len);

        LOGI(10, "decoded resampled len :  %d vs %d  \n",  decoded_len, ret);
        if (ret>  0)
        {
            //Decode & resample successful
            // mRecordPacket : has raw audio data - resampled to 11025
            // decoded_len  : has len of that chunk

            //Encode now to new packet - possible need to encode multiple time

#if DEBUG_WRITE_TO_FILE //  write raw to file
            {
                int data_size = av_samples_get_buffer_size(NULL, src_codec_context->channels,
                        mFrame->nb_samples,
                        src_codec_context->sample_fmt, 1);
                fwrite( mRecordPacket , sizeof(uint8_t), decoded_len,pFile);
            }
#endif

            write_audio_to_stream(oc, mRecordPacket ,  decoded_len);
        }

        LOGI(10, "freeing Frame()\n");
        av_frame_free(&mFrame);
    }

#if DEBUG_WRITE_TO_FILE //  write raw to file
    fclose(pfile);
#endif


}

void RecorderAudio::set_first_ts(int64_t first_pts, int64_t first_dts) {
	LOGI(10, "AAARecorder set first packet: pts %lld, dts %lld\n", first_pts, first_dts);
	this->first_pts = av_rescale_q(first_pts, src_stream->time_base, src_stream->codec->time_base);
	this->first_dts = av_rescale_q(first_dts, src_stream->time_base, src_stream->codec->time_base);
	first_ts_set = true;
}

bool RecorderAudio::is_first_ts_set() {
	return first_ts_set;
}

void RecorderAudio::write_audio_frame4(AVFormatContext *oc)
{
    AVFrame * mFrame = NULL;
    int ret;


#if DEBUG_WRITE_TO_FILE //  write raw to file
    FILE * pFile , *pFile1;
    const char * file_path = "audio_raw_b4_resample.pcm";

    const char * file_path1 = "audio_raw_after_resample.pcm";

    //"audio_raw_after_resample.pcm"

    pFile = fopen(file_path, "ab");
    if (pFile == NULL)
    {
        LOGI(NULL,10, "Can't open audio raw file, may crash any time");
    }

    pFile1 = fopen(file_path1,"ab");
    if (pFile1 == NULL)
    {
        LOGI(NULL,10, "Can't open audio raw file, may crash any time");
    }

#endif

    mFrame = getFrame() ;

    if (mFrame != NULL)
        // while (mFrame != NULL)
    {
    	if (first_ts_set == false)
    	{
    		LOGI(10, "First audio packet pts %lld, dts %lld",
    				av_rescale_q(mFrame->pts, src_stream->codec->time_base, src_stream->time_base),
					av_rescale_q(mFrame->pkt_dts, src_stream->codec->time_base, src_stream->time_base));
    		first_ts_set = true;
    		first_pts = mFrame->pts > 0 ? mFrame->pts : 0;
    		first_dts = mFrame->pkt_dts > 0 ? mFrame->pkt_dts : 0;
    	}

#if DEBUG_RECORDING
    	LOGI(10, "Audio frame pts %lld, first pts %lld", mFrame->pts, first_pts);
#endif
    	if (mFrame->pts < first_pts) {
    		LOGI(10, "First audio pts %lld -> skip this frame %lld", first_pts, mFrame->pts);
    		mFrame->pts = first_pts;
    	}
//    	else
    	{
    		mFrame->pts -= first_pts;
    		mFrame->pkt_dts -= first_dts;

#if DEBUG_RECORDING
    		LOGI(10, "Got frame from queue: pts %lld, pts_double %f, src_st_tb %f, src_codec_tb %f, dst_st_tb %f, dst_codec_tb %f, duration %lld, size: %d, nb_samples %d",
    				mFrame->pts, mFrame->pts * av_q2d(src_stream->codec->time_base),
					av_q2d(src_stream->time_base), av_q2d(src_stream->codec->time_base), av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base),
					mFrame->pkt_duration, mFrame->pkt_size, mFrame->nb_samples);
#endif

#if  DEBUG_WRITE_TO_FILE //  write raw to file
    		{

    			int size     = mFrame->nb_samples *
    					src_codec_context->channels *
						av_get_bytes_per_sample(src_codec_context->sample_fmt);


    			fwrite (mFrame->data[0],sizeof(uint8_t), size,pFile);


    			fflush (pFile);

    		}
#endif

    		AVFrame *filt_frame = av_frame_alloc();


    		// Resample - output data is put into mRecordPacket
    		//uint8_t * out_buf = NULL;


        /* push the audio data from decoded frame into the filtergraph */
        if (av_buffersrc_add_frame_flags(buffersrc_ctx, mFrame, 0) < 0)
        {
            log_error("Error while feeding the audio filtergraph\n");

    		}



    		/* pull filtered audio from the filtergraph */
    		while (1) {
    			ret = av_buffersink_get_frame(buffersink_ctx, filt_frame);
    			if(ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
    			{
    				//No more filtered frame
    				break;
    			}
    			if(ret < 0)
    			{
    				break;
    			}


    			//Write frame..
    			int  n = filt_frame->nb_samples *
    					av_get_channel_layout_nb_channels(av_frame_get_channel_layout(filt_frame));

    			//Encode now to new packet - possible need to encode multiple time
#if DEBUG_WRITE_TO_FILE //  write raw to file
    			{
    				fwrite (filt_frame->data[0],sizeof(uint16_t), n , pFile1);
    				fflush (pFile1);
    			}
#endif
#if DEBUG_RECORDING
    			LOGI(10, "Got filter frame, before convert pts: pts %lld, pts_double %f, src_st_tb %f, src_codec_tb %f, dst_st_tb %f, dst_codec_tb %f, duration %lld, size: %d, nb_samples %d",
    					filt_frame->pts, filt_frame->pts * av_q2d(src_stream->time_base),
						av_q2d(src_stream->time_base), av_q2d(src_stream->codec->time_base), av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base),
						filt_frame->pkt_duration, filt_frame->pkt_size, filt_frame->nb_samples);
#endif
    			//        filt_frame->pts = av_rescale_q(mFrame->pts, src_stream->codec->time_base, mStream->codec->time_base);
#if DEBUG_RECORDING
    			LOGI(10, "Got filter frame, after convert pts: pts %lld, pts_double %f, src_st_tb %f, src_codec_tb %f, dst_st_tb %f, dst_codec_tb %f, duration %lld, size: %d, nb_samples %d",
    					filt_frame->pts, filt_frame->pts * av_q2d(src_stream->time_base),
						av_q2d(src_stream->time_base), av_q2d(src_stream->codec->time_base), av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base),
						filt_frame->pkt_duration, filt_frame->pkt_size, filt_frame->nb_samples);
#endif
    			//        write_audio_to_stream(oc, filt_frame->data[0] , n*2);
    			encode_write_frame(oc, filt_frame);


    			av_frame_unref(filt_frame);
    		}

    	}
    	av_frame_free(&mFrame);
    }
#if DEBUG_WRITE_TO_FILE //  write raw to file
    fclose(pFile);
    fclose(pFile1);
#endif

}


int RecorderAudio::write_audio_to_stream(AVFormatContext *oc,  uint8_t * buffer, int len)
{
    AVPacket pkt = { 0 }; // data and size must be 0;
    AVFrame * frame = NULL;
    int got_packet, ret;
    int needed_size = 0;

    int frame_count = 0;
    AVCodecContext *c;

    uint8_t * buff_ptr = NULL,  * data_ptr = NULL;
    int buff_size = 0;

    c =   mStream->codec;

    int osize =  av_get_bytes_per_sample(c->sample_fmt);


    buff_size = leftOverLen + len;
    buff_ptr = (uint8_t*) av_mallocz(buff_size);

    if (buff_ptr == NULL)
    {
        LOGI( 10,"Cant' allocate buffer\n");
    }

    // LOGI(NULL,10, "1 leftOverLen :%d \n", leftOverLen);

    if (leftOverLen > 0 )
    {
        //Merge 2 buffer into 1 big one for processing
        memcpy(buff_ptr, leftOverBuff, leftOverLen);

        memcpy(buff_ptr+leftOverLen, buffer, len);



        //clear data & reset pointer
        memset(leftOverBuff, 0, leftOverLen);
        leftOverLen = 0;
    }
    else
    {
        //copy over only data in buffer
        memcpy(buff_ptr, buffer, len);
    }

    data_ptr = buff_ptr;


#if DEBUG_WRITE_TO_FILE //  write raw to file
    FILE * pFile ;
    //Simulator
    const char * file_path = "audio_adpcm_after_encode.raw";
    //audio_adpcm_after_encode.raw
    pFile = fopen(file_path, "ab");
    if (pFile == NULL)
    {
        LOGI(NULL,10, "Can't open audio raw file, may crash any time");
    }
#endif


    //Param @buffer / len : OK here

    frame = av_frame_alloc();

    do
    {


        if ((buff_size - frame_count) > (c->frame_size * osize))
        {
            frame->nb_samples = (c->frame_size !=0)?c->frame_size:buff_size/2;
        }
        else
        {
            //if not enough 1 frame, we will break here the leftover data will be used again later
            break;
        }

        frame->sample_rate =    c->sample_rate;
        frame->format         = c->sample_fmt;
        frame->channel_layout = c->channel_layout;



        needed_size = av_samples_get_buffer_size(NULL, c->channels,
                frame->nb_samples, c->sample_fmt, 0);


#if DEBUG



        LOGI(NULL, 10, "len:%d Buffer size: %d\n", (int)len, buff_size);
        LOGI(NULL, 10, "Needed size: %d\n", needed_size);
        LOGI(NULL, 10, "data_ptr : %p buff_ptr: %p \n", data_ptr, buff_ptr);

        LOGI(NULL, 10, "osize: %d nb_samples: %d",  osize ,  frame->nb_samples);
#endif


        //HERE WRONG
        ret  = avcodec_fill_audio_frame(frame, c->channels, c->sample_fmt,
                data_ptr , needed_size,0);

#if  DEBUG_WRITE_TO_FILE //  write raw to file
        {
            fwrite( frame->data[0]    , sizeof(uint8_t), needed_size,pFile);
        }
#endif


        if (ret < 0)
        {
            log_error("Fill audio frame failed with error code %d\n", ret);
            break;
        }

        frame_count += needed_size;

#if  DEBUG
        LOGI(10, "streamid: %d  channel: %d, sample_fmt:%d, rate: %d frame_size:%d\n",
                mStream->id, c->channels, c->sample_fmt, c->sample_rate, c->frame_size);
#endif
        data_ptr += needed_size;

        pkt.data = NULL; // mRecordPacket;
        pkt.size = 0; //mSamplesSize;
        av_init_packet(&pkt);

#if DEBUG_RECORDING
        LOGI(10, "After fill frame, before encode pkt AAAAA frame: size: %d, pts %lld, pts_double %f, duration: %lld",
        		frame->pkt_size, frame->pkt_pts, frame->pkt_pts * av_q2d(mStream->time_base), frame->pkt_duration);
#endif

        ret = avcodec_encode_audio2(c, &pkt, frame, &got_packet);
        if (ret < 0) {

            LOGI(10, "Error encoding audio frame:  %d: %s\n", ret, av_err2str(ret));

            break;
        }

#if DEBUG_RECORDING
        LOGI(10, "After encode pkt AAAAA: duration: %d, size: %d, pts %lld, pts_double %f",
        		pkt.duration, pkt.size, pkt.pts, pkt.pts * av_q2d(mStream->time_base));
#endif

        if (!got_packet)
        {
            break;
        }

        pkt.pts = AV_NOPTS_VALUE; // av_rescale_q_rnd(pkt.pts, c->time_base, mStream->time_base,  AV_ROUND_NEAR_INF);
        pkt.dts = AV_NOPTS_VALUE;//frame->pkt_dts; // av_rescale_q_rnd(pkt.dts, c->time_base, mStream->time_base, AV_ROUND_NEAR_INF);

        pkt.stream_index = mStream->index;
//        av_packet_rescale_ts(&pkt, mStream->codec->time_base, mStream->time_base);

#if DEBUG_RECORDING
        LOGI(10, "Write pkt AAAAA: size: %d, pts %lld, pts_double %f, duration: %lld",
        		pkt.size, pkt.pts, pkt.pts * av_q2d(mStream->time_base), pkt.duration);
#endif


        /* Write the compressed frame to the media file. */
        ret = av_interleaved_write_frame(oc, &pkt);
        //ret = av_write_frame(oc, &pkt);

        if (ret != 0) {
            LOGI(10, "Error while writing  audio frame: %s\n", av_err2str(ret));

            break;
        }



#ifdef USE_FFMPEG_3_0
        av_packet_unref(&pkt);
#else
        // Free the packet that was allocated by av_read_frame
        av_free_packet(&pkt);

#endif

    }
    while (frame_count < buff_size);

#if  DEBUG
    LOGI(NULL, 10, "end of write func: frame_count : %d\n", frame_count);
#endif
    if (frame_count >= buff_size)
    {
        //No left over
        leftOverLen = 0;
    }
    else
    {
        //Some data is left
        leftOverLen = buff_size - frame_count;


        memcpy(leftOverBuff,buff_ptr+frame_count, leftOverLen);


    }


    av_free(buff_ptr);
    av_frame_free(&frame);



#if DEBUG_WRITE_TO_FILE //  write raw to file
    fclose(pFile);
#endif



    return 0;
}


int RecorderAudio::encode_write_frame(AVFormatContext *oc, AVFrame *filter_frame)
{
	AVPacket pkt = { 0 }; // data and size must be 0;
	AVFrame * frame = NULL;
	int got_packet, ret;
	int needed_size = 0;
	int sub_pkt = 0;

	int frame_count = 0;
	AVCodecContext *c;

	uint8_t * buff_ptr = NULL,  * data_ptr = NULL;
	int buff_size = 0;

	c =   mStream->codec;

	int osize =  av_get_bytes_per_sample(c->sample_fmt);

	uint8_t * buffer = filter_frame->data[0];
	int len = filter_frame->nb_samples * osize * filter_frame->channels;

#if DEBUG_RECORDING
	LOGI( 10,"encode_write_frame: samples %d, length %d, filter frame pts %lld\n", filter_frame->nb_samples, len, filter_frame->pts);
#endif

	buff_size = leftOverLen + len;
	buff_ptr = (uint8_t*) av_mallocz(buff_size);

	if (buff_ptr == NULL)
	{
		LOGI( 10,"Cant' allocate buffer\n");
	}

	// LOGI(NULL,10, "1 leftOverLen :%d \n", leftOverLen);



	if (leftOverLen > 0 )
	{
		//Merge 2 buffer into 1 big one for processing
		memcpy(buff_ptr, leftOverBuff, leftOverLen);

		memcpy(buff_ptr+leftOverLen, buffer, len);



		//clear data & reset pointer
		memset(leftOverBuff, 0, leftOverLen);
		leftOverLen = 0;
	}
	else
	{
		//copy over only data in buffer
		memcpy(buff_ptr, buffer, len);
	}

	data_ptr = buff_ptr;


#if DEBUG_WRITE_TO_FILE //  write raw to file
	FILE * pFile ;
	//Simulator
	const char * file_path = "audio_adpcm_after_encode.raw";
	//audio_adpcm_after_encode.raw
	pFile = fopen(file_path, "ab");
	if (pFile == NULL)
	{
		LOGI(NULL,10, "Can't open audio raw file, may crash any time");
	}
#endif


	//Param @buffer / len : OK here

	frame = av_frame_alloc();

	do
	{


		if ((buff_size - frame_count) > (c->frame_size * osize))
		{
			frame->nb_samples = (c->frame_size !=0)?c->frame_size:buff_size/2;
		}
		else
		{
			//if not enough 1 frame, we will break here the leftover data will be used again later
			break;
		}

		frame->sample_rate =    c->sample_rate;
		frame->format         = c->sample_fmt;
		frame->channel_layout = c->channel_layout;



		needed_size = av_samples_get_buffer_size(NULL, c->channels,
				frame->nb_samples, c->sample_fmt, 0);


#if DEBUG



		LOGI(NULL, 10, "len:%d Buffer size: %d\n", (int)len, buff_size);
		LOGI(NULL, 10, "Needed size: %d\n", needed_size);
		LOGI(NULL, 10, "data_ptr : %p buff_ptr: %p \n", data_ptr, buff_ptr);

		LOGI(NULL, 10, "osize: %d nb_samples: %d",  osize ,  frame->nb_samples);
#endif


		//HERE WRONG
		ret  = avcodec_fill_audio_frame(frame, c->channels, c->sample_fmt,
				data_ptr , needed_size,0);
		int pts_delta = (int) (sub_pkt * frame->nb_samples / (double) frame->sample_rate / av_q2d(mStream->codec->time_base));
		frame->pts = filter_frame->pts;// + pts_delta;
#if DEBUG_RECORDING
		LOGI(10, "Fill audio frame: sub_pkt %d, filter frame pts %lld, pts %lld, nb_samples %d, sample_rate %d, codec_tb %f -> pts_delta %d",
				sub_pkt, filter_frame->pts, frame->pts, frame->nb_samples, frame->sample_rate, av_q2d(mStream->codec->time_base), pts_delta);
#endif
#if  DEBUG_WRITE_TO_FILE //  write raw to file
		{
			fwrite( frame->data[0]    , sizeof(uint8_t), needed_size,pFile);
		}
#endif


		if (ret < 0)
		{
			log_error("Fill audio frame failed with error code %d\n", ret);
			break;
		}

		frame_count += needed_size;

#if  DEBUG
		LOGI(10, "streamid: %d  channel: %d, sample_fmt:%d, rate: %d frame_size:%d\n",
				mStream->id, c->channels, c->sample_fmt, c->sample_rate, c->frame_size);
#endif
		data_ptr += needed_size;

		pkt.data = NULL; // mRecordPacket;
		pkt.size = 0; //mSamplesSize;
		av_init_packet(&pkt);

#if DEBUG_RECORDING
		LOGI(10, "After fill frame, before encode pkt frame: size: %d, pts %lld, filter frame pts %lld, duration: %lld, st_tb %f, codec_tb %f",
				frame->pkt_size, frame->pts, filter_frame->pkt_pts, frame->pkt_duration,
				av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base));
#endif

		ret = avcodec_encode_audio2(c, &pkt, frame, &got_packet);
		if (ret < 0) {

			LOGI(10, "Error encoding audio frame:  %d: %s\n", ret, av_err2str(ret));

			break;
		}

#if DEBUG_RECORDING
		LOGI(10, "After encode pkt AAAAA: pts %lld, st_tb %f, codec_tb %f, duration: %lld, size: %d",
				pkt.pts, av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base), pkt.duration, pkt.size);
#endif

		if (!got_packet)
		{
			break;
		}

		pkt.stream_index = mStream->index;
		av_packet_rescale_ts(&pkt, mStream->codec->time_base, mStream->time_base);
		pkt.dts = pkt.pts;

#if DEBUG_RECORDING
		LOGI(10, "Write pkt AAAAA: pts %lld, dts %lld, pts_double %f, st_tb %f, codec_tb %f, duration: %lld, size: %d",
				pkt.pts, pkt.dts, pkt.pts*av_q2d(mStream->time_base), av_q2d(mStream->time_base),
				av_q2d(mStream->codec->time_base), pkt.duration, pkt.size);
#endif

		double currentPktPts = pkt.pts*av_q2d(mStream->time_base);
		/* Write the compressed frame to the media file. */
		ret = av_interleaved_write_frame(oc, &pkt);
		//ret = av_write_frame(oc, &pkt);

		if (ret != 0) {
			LOGI(10, "Error while writing  audio frame: %f, error: %s\n", currentPktPts, av_err2str(ret));
			break;
		} else {
//			LOGI(10, "Writing  audio frame success: %f\n", currentPktPts);
		}



#ifdef USE_FFMPEG_3_0
		av_packet_unref(&pkt);
#else
		// Free the packet that was allocated by av_read_frame
		av_free_packet(&pkt);

#endif
		sub_pkt++;
	}
	while (frame_count < buff_size);

#if  DEBUG
	LOGI(NULL, 10, "end of write func: frame_count : %d\n", frame_count);
#endif
	if (frame_count >= buff_size)
	{
		//No left over
		leftOverLen = 0;
	}
	else
	{
		//Some data is left
		leftOverLen = buff_size - frame_count;


		memcpy(leftOverBuff,buff_ptr+frame_count, leftOverLen);


	}


	av_free(buff_ptr);
	av_frame_free(&frame);



#if DEBUG_WRITE_TO_FILE //  write raw to file
	fclose(pFile);
#endif



	return 0;
}


/* Obsolete - do not use */
#if 0
void RecorderAudio::write_audio_frame_old(AVFormatContext *oc)
{
    AVPacket pPacket;
    int size = 0;
    int ret = 0;
    int got_frame;
    AVFrame * mFrame = NULL;
    int decoded_len  ;


#if DEBUG_WRITE_TO_FILE //  write raw to file
    FILE * pFile ;
    pFile = fopen("audio.raw", "ab");

#endif


    //Dequeue
    if ( mQueue->get(&pPacket,true )<0)
    {
        //failed to dequeue
        return;
    }

    size = pPacket.size;
    mFrame = av_frame_alloc() ;


    do
    {
        //Decode
        ret = avcodec_decode_audio4(src_codec_context,mFrame, &got_frame, &pPacket);

        //LOGI(10, "decoded frame samples: %d",  mFrame->nb_samples);
        if (got_frame == 1)
        {
            // Resample - output data is put into mRecordPacket
            ret = resample_audio_for_record(adpcm_c, src_codec_context , mFrame, &decoded_len);

            //LOGI(10, "decoded resampled len :  %d vs %d  \n",  decoded_len, ret);
            if (ret>  0)
            {
                //Decode & resample successful
                // mRecordPacket : has raw audio data - resampled to 11025
                // decoded_len  : has len of that chunk

                //Encode now to new packet - possible need to encode multiple time

#if DEBUG_WRITE_TO_FILE //  write raw to file
                {
                    int data_size = av_samples_get_buffer_size(NULL, src_codec_context->channels,
                            mFrame->nb_samples,
                            src_codec_context->sample_fmt, 1);
                    fwrite( mRecordPacket, sizeof(uint8_t), decoded_len,pFile);
                }
#endif

                write_audio_to_stream(oc, mRecordPacket ,  decoded_len);
            }

        }
        pPacket.data += ret;
        pPacket.size -= ret;
    }
    while(pPacket.size > 0 || !pPacket.data);


    av_frame_free(&mFrame);
    av_free_packet(&pPacket);

#if DEBUG_WRITE_TO_FILE //  write raw to file
    fclose(pfile);
#endif

    return;
}
#endif





int RecorderAudio::resample_audio_for_record(AVCodecContext *  enc, AVCodecContext * dec,
        AVFrame *decoded_frame,  int * out_len)
{

    uint8_t *buftmp ;
    int    size_out ;

    int osize = av_get_bytes_per_sample(enc->sample_fmt);
    int isize = av_get_bytes_per_sample(dec->sample_fmt);
    const uint8_t **buf = (const uint8_t **) decoded_frame->extended_data;
    int size     = decoded_frame->nb_samples * dec->channels * isize;


    int audio_resample = 0;
    int audio_sync_method  = 0;

#if DEBUG
    log_info("need resample?  %d channels @ %d Hz (%d)  to %d channels @ %d Hz (%d) \n",
            dec->channels, dec->sample_rate,dec->sample_fmt,
            enc->channels, enc->sample_rate, enc->sample_fmt );
#endif

    if (enc->channels != dec->channels
            || enc->sample_fmt != dec->sample_fmt
            || enc->sample_rate!= dec->sample_rate
       )
    {
        audio_resample = 1;
    }

#if DEBUG
    log_info("audio_resample: %d", audio_resample);
#endif

    if ( audio_resample && (swr2 == NULL))
    {

        //Set some swr2
        swr2 = swr_alloc_set_opts(NULL,
                enc->channel_layout, enc->sample_fmt, enc->sample_rate,
                dec->channel_layout, dec->sample_fmt, dec->sample_rate,
                0, NULL);

        if(swr2 && swr_init(swr2) < 0){
            log_info(
                    "swr_init() failed\n");

            swr_free(&swr2);
        }


       if (!swr2) {
            log_info(
                    "Can not resample %d channels @ %d Hz to %d channels @ %d Hz\n",
                    dec->channels, dec->sample_rate,
                    enc->channels, enc->sample_rate);
            return -1;
        }


    }

    if (audio_resample)
    {
        buftmp = mRecordPacket ;

        size_out = swr_convert(swr2, ( uint8_t*[]){buftmp}, mSamplesSize  / (enc->channels * osize),
                buf, size / (dec->channels * isize));

        if (size_out ==  (mSamplesSize  / (enc->channels * osize)))
        {
            log_info("Warning: audio buff is probably too small");
        }

        size_out = size_out * enc->channels * osize;

        *out_len = size_out;
    }
    else
    {
        buftmp = mRecordPacket ;

        ///copy over to buffer
        memcpy(mRecordPacket, *buf, size);

        size_out = size;
        *out_len = size_out;

#if DEBUG
        LOGI( 10,"size: %d vs %d buftmp: %p \n "  ,  size_out ,isize, buftmp   );
#endif
    }
    return size_out ;
}

AVStream *  RecorderAudio::add_stream(AVFormatContext *oc, AVCodec **codec,
        enum AVCodecID codec_id)
{
    AVCodecContext *c;
    AVStream *st;

    /* find the encoder */
    *codec = avcodec_find_encoder(codec_id);
    if (!(*codec)) {
    	log_error("Could not find encoder for '%s'\n",
                avcodec_get_name(codec_id));
        exit(1);
    }

    st = avformat_new_stream(oc, *codec);
    if (!st) {
    	log_error("Could not allocate stream\n");
        exit(1);
    }

    st->time_base = (AVRational) {1, 1000};
    st->id = oc->nb_streams-1;
    c = st->codec;

    switch ((*codec)->type) {
        case AVMEDIA_TYPE_AUDIO:
            c->sample_fmt  = AV_SAMPLE_FMT_S16;
            c->sample_rate = 11025;
            c->channels    = 1;
            c->channel_layout = av_get_default_channel_layout(c->channels);
            c->time_base = (AVRational) {1, c->sample_rate};
            break;
        case AVMEDIA_TYPE_VIDEO:
            c->codec_id = codec_id;

            c->bit_rate = 400000;
            /* Resolution must be a multiple of two. */
            c->width    = 352;
            c->height   = 288;
            /* timebase: This is the fundamental unit of time (in seconds) in terms
             * of which frame timestamps are represented. For fixed-fps content,
             * timebase should be 1/framerate and timestamp increments should be
             * identical to 1. */
//            c->time_base.den = 15; // STREAM_FRAME_RATE;
//            c->time_base.num = 1;
            c->gop_size      = 12; /* emit one intra frame every twelve frames at most */
            c->pix_fmt       = AV_PIX_FMT_YUV420P;
            if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
                /* just for testing, we also add B frames */
                c->max_b_frames = 2;
            }
            if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
                /* Needed to avoid using macroblocks in which some coeffs overflow.
                 * This does not happen with normal video, it just happens here as
                 * the motion of the chroma plane does not match the luma plane. */
                c->mb_decision = 2;
            }
            c->time_base = src_stream->codec->time_base;
            break;

        default:
            break;
    }

    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;

    return st;
}

AVStream * RecorderAudio::getAudioStream()
{

    return mStream;

}

//Prepare the context & codec for PCM encoding
int RecorderAudio::encodeToAdpcm_init(AVFormatContext * oc )
{
    adpcm_codec = NULL;
    mStream = add_stream(oc, &adpcm_codec, AV_CODEC_ID_ADPCM_SWF);
    adpcm_c = mStream->codec;

    /* open it */
    if (avcodec_open2(adpcm_c, adpcm_codec, NULL) < 0)
    {
        log_error("Encode error open context ");
        return -2;
    }

    swr2 = NULL;

    return 0;
}


void RecorderAudio::stopRecording()
{
    mStopRecording = true;
    return ;
}


void RecorderAudio::initBuffQueue()
{
    audioFrameQueue = (FFBufQueue *) av_calloc(1, sizeof(audioFrameQueue[0]));
    if (audioFrameQueue == NULL)
    {
        LOGI(10,"Failed to alloc buff queue\n");
    }

    pthread_mutex_init(&mLock,NULL);
    pthread_cond_init(&mCondition, NULL);
}

void RecorderAudio::destroyBuffQueue()
{
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCondition);
    av_free(audioFrameQueue);
}
void RecorderAudio::addFrame (AVFrame * frame)
{
    if (audioFrameQueue != NULL)
    {
        pthread_mutex_lock(&mLock);

#if DEBUG_RECORDING
        LOGI(10, "Adding audio frame to recorder: pts %lld, available? %d, queue_size %d", frame->pts, audioFrameQueue->available, FF_BUFQUEUE_SIZE);
#endif
        ff_bufqueue_add(NULL, audioFrameQueue,  frame);

        pthread_cond_signal(&mCondition);
        pthread_mutex_unlock(&mLock);
    }
}

AVFrame *  RecorderAudio::getFrame()
{

    AVFrame * ret = NULL;

    if (audioFrameQueue != NULL && (audioFrameQueue->available > 0))
    {
        pthread_mutex_lock(&mLock);

        ret =  ff_bufqueue_get(audioFrameQueue);

        pthread_mutex_unlock(&mLock);
    }


    return ret;
}
void RecorderAudio::flush_all_audio()
{
    if (audioFrameQueue != NULL && (audioFrameQueue->available > 0))
    {
        pthread_mutex_lock(&mLock);

        ff_bufqueue_discard_all(audioFrameQueue);

        pthread_mutex_unlock(&mLock);
    }
}


bool     RecorderAudio::hasUnencodedFrames()
{
    if (audioFrameQueue != NULL)
    {
        return (audioFrameQueue->available > 0);
    }

    return false;
}


int RecorderAudio::init_filters(const char *filters_descr)
{
    char args[512];
    int ret;
    AVFilter *abuffersrc  = avfilter_get_by_name("abuffer");
    AVFilter *abuffersink = avfilter_get_by_name("abuffersink");
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    const enum AVSampleFormat out_sample_fmts[] = { AV_SAMPLE_FMT_S16, (enum AVSampleFormat) -1 };
    const int64_t out_channel_layouts[] = { AV_CH_LAYOUT_MONO, -1 };
    const int out_sample_rates[] = { 11025, -1 };
    const AVFilterLink *outlink;
    AVRational time_base;
    time_base = src_stream->codec->time_base;

    filter_graph = avfilter_graph_alloc();

    /* buffer audio source: the decoded frames from the decoder will be inserted here. */
    if (!src_codec_context->channel_layout)
    {
        src_codec_context->channel_layout = av_get_default_channel_layout(src_codec_context->channels);
    }


#if  0
    snprintf(args, sizeof(args),
            "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%"PRIx64,
            time_base.num, time_base.den, src_codec_context->sample_rate,
            av_get_sample_fmt_name(src_codec_context->sample_fmt),
            src_codec_context->channel_layout);

#endif
    //#define   PRIX64          "llX"       /* uint64_t */
    snprintf(args, sizeof(args),
            "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%llX",
            time_base.num, time_base.den, src_codec_context->sample_rate,
            av_get_sample_fmt_name(src_codec_context->sample_fmt),
            src_codec_context->channel_layout);

    LOGI(10, "Init audio input filter: %s\n", args);

    ret = avfilter_graph_create_filter(&buffersrc_ctx, abuffersrc, "in",
            args, NULL, filter_graph);
    if (ret < 0) {
        log_error("Cannot create audio buffer source\n");
        return ret;
    }

    /* buffer audio sink: to terminate the filter chain. */
    ret = avfilter_graph_create_filter(&buffersink_ctx, abuffersink, "out",
            NULL, NULL, filter_graph);
    if (ret < 0) {
        log_error("Cannot create audio buffer sink\n");
        return ret;
    }

    ret = av_opt_set_int_list(buffersink_ctx, "sample_fmts", out_sample_fmts, -1,
            AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        log_error("Cannot set output sample format\n");
        return ret;
    }

    ret = av_opt_set_int_list(buffersink_ctx, "channel_layouts", out_channel_layouts, -1,
            AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        log_error("Cannot set output channel layout\n");
        return ret;
    }

    ret = av_opt_set_int_list(buffersink_ctx, "sample_rates", out_sample_rates, -1,
            AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        log_error("Cannot set output sample rate\n");
        return ret;
    }

    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;

    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;

    if ((ret = avfilter_graph_parse_ptr(filter_graph, filters_descr,
                    &inputs, &outputs, NULL)) < 0)
        return ret;

    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
        return ret;

    /* Print summary of the sink buffer
     * Note: args buffer is reused to store channel layout string */
    outlink = buffersink_ctx->inputs[0];
    av_get_channel_layout_string(args, sizeof(args), -1, outlink->channel_layout);
    log_info("Output: srate:%dHz fmt:%s chlayout:%s\n",
            (int)outlink->sample_rate,
            (char *)av_x_if_null(av_get_sample_fmt_name((enum AVSampleFormat)outlink->format), "?"),
            args);

    return 0;
}

