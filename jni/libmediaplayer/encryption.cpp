/*******************************************************************************
* Nxcomm 2013
* Product: Cmdserver library
* @Author: trungdo@
* @DateTime: 12Dec2013 - 9:30:00@
* @Version:  0.1@
* Description: H264 encryption
*******************************************************************************/

#include "encryption.h"

// DEBUG  decryption
#define DEBUG_DECRYPT_STREAM 0

#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#endif

#define TAG "libffmpeg"
#define LOG_TAG TAG
#define LOG_LEVEL 100

#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
#define __android_log_print(ANDROID_LOG_INFO, TAG, ...) printf(__VA_ARGS__)
//#define LOGI (level, format, ...)  if (level <= LOG_LEVEL) { print(format,__VA_ARGS__);}
#define LOGI print

extern void print( int level, char * format, ...);
#endif

// H264 enc params
#define NAL_DATAOFFSET 1
#define NAL_ENC_LEN 16
#define NAL_ENC_DISTANCE 1024

EncryptionContext::EncryptionContext()
{
	key = NULL;
	iv = NULL;
}

EncryptionContext::~EncryptionContext()
{
	if (iv != NULL)
	{
		av_freep(&iv);
	}

	if (key != NULL)
	{
		av_freep(&key);
	}
}

//////////////////////// ALAW DECRYPTION ///////////////////////////////////////
int EncryptionContext::alaw_aes_init(aes_context *ctx, uint8_t *key, uint8_t nbytes)
{
	char mode[16] = {'e', 'c', 'b', 0}; // ecb  mode
	int lreturn;

	if ((nbytes!=16) && (nbytes!=24) && (nbytes!=32)) // AES 128, 192, 256 bits
		return 1;

	// Intialize AES roundkey + mode
	lreturn = aes_set_key(ctx, key, nbytes * 8, mode);

	return lreturn;
}


//////////////////////// H264 ENCRYPTION ///////////////////////////////////////
//******************************************************************************
// @description: Init roundkey for h264 encryption
// @param:
//	ctx: aes derived key and mode
//	key: pointer to key
//	nbytes: key length
// @return: 0 if OK, otherwise error
// @details: key expansion and mode setting
//******************************************************************************
int EncryptionContext::h264_aes_init(struct aes_context *ctx, uint8_t *key, uint8_t nbytes){
	char mode[16] = {'c', 'b', 'c', 0}; // CBC mode
	int lreturn;

	if ((nbytes!=16) && (nbytes!=24) && (nbytes!=32)) // AES 128, 192, 256 bits
		return 1;

	// Intialize AES roundkey + mode
	lreturn = aes_set_key(ctx, key, nbytes * 8, mode);

	return lreturn;
}

//******************************************************************************
// @description: Encrypt h264 NAL package
// @param:
//	ctx: aes derived key and mode
//	iv: initial value
//	nal_data: pointer to frame data excluding NAL marker (0x00000001)
//	nal_length: length of frame data
// @return:
// @details: starting from nal_data, with an offset NAL_DATAOFFSET,
//           for each chunk of NAL_ENC_DISTANCE bytes,
//           encrypt NAL_ENC_LEN length of data
//******************************************************************************
void EncryptionContext::h264_aes_encrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length){
	unsigned long j;

	j=NAL_DATAOFFSET;
	while((j+NAL_ENC_LEN)<=nal_length){
		aes_encrypt(ctx, iv, &nal_data[j], NAL_ENC_LEN);
		j+=NAL_ENC_DISTANCE;
	}
}

//******************************************************************************
// @description: Encrypt h264 NAL package
// @param:
//	ctx: aes derived key and mode
//	iv: initial value
//	nal_data: pointer to frame data excluding NAL marker (0x00000001)
//	nal_length: length of frame data
// @return:
// @details: starting from nal_data, with an offset NAL_DATAOFFSET,
//           for each chunk of NAL_ENC_DISTANCE bytes,
//           decrypt NAL_ENC_LEN length of data
//******************************************************************************
void EncryptionContext::h264_aes_decrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length){
	unsigned long j;

	j=NAL_DATAOFFSET;
	while((j+NAL_ENC_LEN)<=nal_length){
		aes_decrypt(ctx, iv, &nal_data[j], NAL_ENC_LEN);
		j+=NAL_ENC_DISTANCE;
	}
}

/**
 * Initialize key and iv
 * Need to call prepare before using all encrypt/decrypt methods.
 */
void EncryptionContext::prepare(const char* key_str, const char* iv_str)
{
//	av_log(NULL, AV_LOG_INFO, "prepare encryption ctx with key %s and iv %s\n",
//			key_str, iv_str);
	if (set_string_binary(key_str, &key, &keylen) == -1)
	{
		//init key failed
		av_log(NULL, AV_LOG_ERROR, "init key %s failed\n", key_str);
	}

	if (set_string_binary(iv_str, &iv, &ivlen) == -1)
	{
		//init iv failed
		av_log(NULL, AV_LOG_ERROR, "init iv %s failed\n", iv_str);
	}

	pcm_enc_init(key, keylen);
	h264_enc_init(key, keylen, iv, ivlen);
}

int EncryptionContext::set_string_binary(const char *val, uint8_t **dst, int* keylen)
{
    uint8_t *bin, *ptr;
    int len = strlen(val);
    if (len & 1)
    {
        return -1;
    }
    len /= 2;

    ptr = bin = (uint8_t*) av_malloc(len);
    while (*val) {
        int a = hexchar2int(*val++);
        int b = hexchar2int(*val++);
        if (a < 0 || b < 0) {
            av_free(bin);
            return -1;
        }
        *ptr++ = (a << 4) | b;
    }
    *dst = bin;
    *keylen = len;

    return 0;
}


//******************************************************************************
//  API
//******************************************************************************
//static uint8_t  mKey  []= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//static uint8_t  iv  []= {21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
//static struct aes_context sw_ctx;

//static uint8_t  iv_a  []= {21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};

//static struct aes_context sw_ctx_a;
int EncryptionContext::pcm_enc_init(uint8_t* key, int keylen)
{
    intarray2hexstr(key, keylen);

    if (alaw_aes_init(&sw_ctx_a, key, keylen))
    {
    	av_log(NULL, AV_LOG_ERROR, "init AES failed\n");

    	return -1;
    }

    return 0;
}

int EncryptionContext::h264_enc_init(uint8_t* key, int keylen, uint8_t* new_iv, int iv_len)
{
    intarray2hexstr(key, keylen);

    if (h264_aes_init(&sw_ctx, key, keylen)) {
    	av_log(NULL, AV_LOG_ERROR, "set key failed\n");
    	return -1;
    }

    if (set_iv(new_iv, iv_len) != 0)
    {
    	av_log(NULL, AV_LOG_ERROR, "set iv failed\n");
    	return -1;
    }

    return 0;
}

int EncryptionContext::set_iv(uint8_t* new_iv, int iv_len)
{
	if (iv_len != 16)
	{
		return -1;
	}
	int i;
	for (i=0; i<iv_len; i++)
	{
		iv[i] = new_iv[i];
	}

	return 0;
}

//! Byte swap unsigned int 32
uint32_t EncryptionContext::swap_uint32( uint32_t val )
{
    val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0xFF00FF );
    return (val << 16) | (val >> 16);
}

//! Byte swap unsigned int 24
uint32_t EncryptionContext::swap_uint24( uint32_t val )
{
    return (val & 0x0000FF00) | ((val << 16) & 0x00FF0000) | ((val >> 16) & 0x000000FF);
}

int EncryptionContext::decryptFile(const char* filePath)
{
	av_log(NULL, AV_LOG_INFO, "Open file: %s\n", filePath);
	FILE* fp;
	fp = fopen(filePath, "r+b");

	if (!fp)
	{
		av_log(NULL, AV_LOG_ERROR, "Cannot open file!\n");
		return -1;
	}

	/* Read header. */
	/* Skip first 4 bytes */
	fseek(fp, 4, SEEK_SET);
	/* Read flags */
	uint8_t flags;
	uint32_t offset, prev_tag_size;
	fread(&flags, 1, 1, fp);
	//av_log(NULL, AV_LOG_INFO, "flags %d\n", flags);
	if (flags & 1)
	{
		//av_log(NULL, AV_LOG_INFO, "FLV file has video\n");
	}

	if (flags & 4)
	{
		//av_log(NULL, AV_LOG_INFO, "FLV file has audio\n");
	}

	fread(&offset, 4, 1, fp);
	offset = swap_uint32(offset);
	//av_log(NULL, AV_LOG_INFO, "offset %d\n", offset);
	fseek(fp, offset, SEEK_SET);

	/* Skip first tag. */
	fseek(fp, 4, SEEK_CUR);

	/* Parse FLV tags. */
	int buf_size, pkt_type, dlen, isSPS;
	int ret;
	uint8_t* buf;
	int read_at = 0, write_at = 0;
	while (!feof(fp))
	{
		buf_size = 0;
		read_flv_tag(fp, &buf, &buf_size, &pkt_type, &isSPS);
		if (pkt_type == 0x08)
		{
			/* Decrypt audio pkt. */
			dlen = buf_size;
			if (buf_size % 16 !=0)
			{
				dlen = (buf_size/16) * 16;
			}
			try_decrypt_audio(buf, dlen);
			write_at = ftell(fp) - buf_size;
			fseek(fp, write_at, SEEK_SET);
			ret = fwrite(buf, sizeof(uint8_t), buf_size, fp);
//			av_log(NULL, AV_LOG_INFO, "Write decrypted audio buf %d, byte wrote %d\n",
//					buf_size, ret);
			av_free(buf);
		}
		else if (pkt_type == 0x09)
		{
			/* Decrypt video pkt. */
			if (isSPS != 0)
			{
				try_decrypt3(buf + 4, buf_size - 4);

				write_at = ftell(fp) - buf_size;
				fseek(fp, write_at, SEEK_SET);
				ret = fwrite(buf, sizeof(uint8_t), buf_size, fp);
//				av_log(NULL, AV_LOG_INFO, "Write decrypted video buf %d, byte wrote %d\n",
//						buf_size, ret);
				av_free(buf);
			}
			else
			{
//				av_log(NULL, AV_LOG_INFO, "Skip SPS packet.\n");
			}
		}

		/* Skip tag size field. */
		read_at = ftell(fp);
		fseek(fp, read_at, SEEK_SET);
		fread(&prev_tag_size, 4, 1, fp);
		prev_tag_size = swap_uint32(prev_tag_size);
//		av_log(NULL, AV_LOG_INFO, "prev_tag_size: %d\n", prev_tag_size);
	}


	fclose(fp);
	av_log(NULL, AV_LOG_INFO, "Decrypt done.\n");
	return 0;
}

int EncryptionContext::encryptFile(const char* filePath)
{
	av_log(NULL, AV_LOG_INFO, "Open file: %s\n", filePath);
	FILE* fp;
	fp = fopen(filePath, "r+b");

	if (!fp)
	{
		av_log(NULL, AV_LOG_ERROR, "Cannot open file!\n");
		return -1;
	}

	/* Read header. */
	/* Skip first 4 bytes */
	fseek(fp, 4, SEEK_SET);
	/* Read flags */
	uint8_t flags;
	uint32_t offset, prev_tag_size;
	fread(&flags, 1, 1, fp);
	//	av_log(NULL, AV_LOG_INFO, "flags %d\n", flags);
	av_log(NULL, AV_LOG_INFO, "flags %d\n", flags);
	if (flags & 1)
	{
		av_log(NULL, AV_LOG_INFO, "FLV file has video\n");
	}

	if (flags & 4)
	{
		av_log(NULL, AV_LOG_INFO, "FLV file has audio\n");
	}

	fread(&offset, 4, 1, fp);
	offset = swap_uint32(offset);
	av_log(NULL, AV_LOG_INFO, "offset %d\n", offset);
	fseek(fp, offset, SEEK_SET);

	/* Skip first tag. */
	fseek(fp, 4, SEEK_CUR);

	/* Parse FLV tags. */
	int buf_size, pkt_type, dlen, isSPS;
	int ret;
	uint8_t* buf;
	int read_at = 0, write_at = 0;
	while (!feof(fp))
	{
		buf_size = 0;
		read_flv_tag(fp, &buf, &buf_size, &pkt_type, &isSPS);
		if (pkt_type == 0x08)
		{
			/* Decrypt audio pkt. */
			dlen = buf_size;
			if (buf_size % 16 !=0)
			{
				dlen = (buf_size/16) * 16;
			}
			try_encrypt_audio(buf, dlen);
			write_at = ftell(fp) - buf_size;
			fseek(fp, write_at, SEEK_SET);
			ret = fwrite(buf, sizeof(uint8_t), buf_size, fp);
			av_log(NULL, AV_LOG_INFO, "Write decrypted audio buf %d, byte wrote %d\n",
					buf_size, ret);
			av_free(buf);
		}
		else if (pkt_type == 0x09)
		{
			/* Decrypt video pkt. */
			if (isSPS != 0)
			{
				try_encrypt3(buf + 4, buf_size - 4);

				write_at = ftell(fp) - buf_size;
				fseek(fp, write_at, SEEK_SET);
				ret = fwrite(buf, sizeof(uint8_t), buf_size, fp);
				av_log(NULL, AV_LOG_INFO, "Write decrypted video buf %d, byte wrote %d\n",
						buf_size, ret);
				av_free(buf);
			}
			else
			{
				av_log(NULL, AV_LOG_INFO, "Skip SPS packet.\n");
			}
		}

		/* Skip tag size field. */
		read_at = ftell(fp);
		fseek(fp, read_at, SEEK_SET);
		fread(&prev_tag_size, 4, 1, fp);
		prev_tag_size = swap_uint32(prev_tag_size);
//		av_log(NULL, AV_LOG_INFO, "prev_tag_size: %d\n", prev_tag_size);
	}


	fclose(fp);
	av_log(NULL, AV_LOG_INFO, "Encrypt done.\n");
	return 0;
}

void EncryptionContext::read_flv_tag(FILE *fp, uint8_t** buf, int* buf_size, int* pkt_type, int* isSPS)
{
	int read_at = 0;
	uint8_t avcPktType = 0;
	read_at = ftell(fp);
	fseek(fp, read_at, SEEK_SET);
	int ret, fp_pos = 0;
	uint8_t flv_tag_type;
	int size = 0;
	fread(&flv_tag_type, 1, 1, fp);
	*pkt_type = flv_tag_type;
	fread(&size, 3, 1, fp);
	size &= 0x00FFFFFF;
	size = swap_uint24(size); //handle for big/little endian
	fseek(fp, 7, SEEK_CUR);

	if (size == 0)
	{
		return;
	}

	if (*pkt_type == 0x08)
	{
		/* Read audio data. */
		fseek(fp, 1, SEEK_CUR);
		size -= 1;
		*buf_size = size;
		*buf = (uint8_t*) av_mallocz(size * sizeof(uint8_t));
		fread(*buf, sizeof(uint8_t), size, fp);
	}
	else if (*pkt_type == 0x09)
	{
		/* Read video data. */
		fseek(fp, 1, SEEK_CUR);

		/* Read AVC Packet Type. */
		fread(&avcPktType, sizeof(uint8_t), 1, fp);
		*isSPS = avcPktType;

		/* Skip Composition Time. */
		fseek(fp, 3, SEEK_CUR);
		size -= 5;
		*buf_size = size;
		if (*isSPS != 0)
		{
			/* Not PPS packet, read data. */
			*buf = (uint8_t*) av_mallocz(size * sizeof(uint8_t));
			fread(*buf, sizeof(uint8_t), size, fp);
		}
		else
		{
			/* PPS packet, just skip. */
			fseek(fp, size, SEEK_CUR);
		}
	}
	else
	{
		/* Skip meta data. */
		*buf_size = size;
		fseek(fp, size, SEEK_CUR);
	}
//	av_log(NULL, AV_LOG_INFO, "pkt type: %d, buf_size: %d\n", *pkt_type, *buf_size);
}

int EncryptionContext::intarray2hexstr(uint8_t* c, int len)
{
	if (len > 0)
	{
		char *key = (char*) malloc((2*len+1) * sizeof(char));
		key[2*len] = '\0';
		char hexchar[3];
		int i;
		for (i=0; i<len; i++)
		{
			snprintf(hexchar, 3, "%02X", c[i]);
			//LOGI(10, "key[%d] = %s\n", i, hexchar);
			strncpy(key + i*2, hexchar, 2);
		}
		//LOGI(10, "key = %s\n", key);
		free(key);
	}
    return 0;
}

int EncryptionContext::hexchar2int(char c)
{
    if (c >= '0' && c <= '9') return c - '0';
    if (c >= 'a' && c <= 'f') return c - 'a' + 10;
    if (c >= 'A' && c <= 'F') return c - 'A' + 10;
    return -1;
}

void EncryptionContext::try_decrypt2(uint8_t * data , int len )
{

    /* Decrypt frame */
    uint8_t * buff_plain_orig = data;
    uint8_t *NAL_pointer; // pointer to NAL data
    unsigned long cur_NAL_LEN=0,cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;
    for (i = 4; i<len;i++)
    {

        if ( (buff_plain_orig[i-3]==0x00) &&
                (buff_plain_orig[i-2]==0x00) && (buff_plain_orig[i-1]==0x00) && (buff_plain_orig[i]==0x01))
        {

#if DEBUG_DECRYPT_STREAM
            av_log(NULL, AV_LOG_DEBUG,  "found start sequenct 0001 at i:%d totallen:%d\n",i, len);

            {
                av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                        buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                        buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                        buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                        buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                      );
            }

#endif


            cur_NAL_LEN = i-3-cur_NAL_POS;
            NAL_pointer = &buff_plain_orig[cur_NAL_POS+4];

#if DEBUG_DECRYPT_STREAM
            av_log(NULL, AV_LOG_DEBUG, " 1 cur_NAL_LEN: %lu \n",  cur_NAL_LEN);
#endif
            h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, cur_NAL_LEN-4);

#if DEBUG_DECRYPT_STREAM
            {
                av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                        buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                        buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                        buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                        buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                      );
            }
#endif
            cur_NAL_POS= i -3;
        }
    }
    cur_NAL_LEN=len - cur_NAL_POS;

    //We only continue if more than 1 block is available
    if (cur_NAL_LEN > 16)
    {
#if DEBUG_DECRYPT_STREAM

        {
            av_log(NULL, AV_LOG_DEBUG, "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

        av_log(NULL, AV_LOG_DEBUG, " 2 cur_NAL_LEN: %lu \n",  cur_NAL_LEN);

#endif
        NAL_pointer = &buff_plain_orig[cur_NAL_POS+4];
        h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, cur_NAL_LEN-4);

#if DEBUG_DECRYPT_STREAM


        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif
    }


}
void EncryptionContext::try_decrypt3(uint8_t * data , int len )
{
    /* Decrypt frame */
    uint8_t * buff_plain_orig = data;

    uint8_t *NAL_pointer; // pointer to NAL data
    unsigned long cur_NAL_LEN=0,cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;

    {



#if DEBUG_DECRYPT_STREAM

        {
            av_log(NULL, AV_LOG_DEBUG, "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

#endif


        cur_NAL_LEN=len;
        NAL_pointer = &buff_plain_orig[0];
#if DEBUG_DECRYPT_STREAM
        av_log(NULL, AV_LOG_DEBUG, " cur_NAL_LEN: %lu \n",  cur_NAL_LEN);
#endif
        h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, len);

#if DEBUG_DECRYPT_STREAM
        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif
    }

}

// Full frame decryption
void EncryptionContext::try_decrypt_audio(uint8_t * data , int len)
{
    uint8_t * buff_plain_orig = data;
    unsigned long cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;
    //We only continue if more than 1 block is available
    if (len > 16)
    {
#if DEBUG_DECRYPT_STREAM

        av_log(NULL, AV_LOG_DEBUG, " 2 decrypt audio len : %lu \n",  len);
        {
            av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

#endif
        //Phung: pass @iv here as a dummy parameter
        //     since we encrypt using ECB mode, iv is not used anyway
		aes_decrypt(&sw_ctx_a, iv, data , len);

#if DEBUG_DECRYPT_STREAM
        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif

    }


}

void EncryptionContext::try_encrypt3(uint8_t * data , int len )
{
    /* Decrypt frame */
    uint8_t * buff_plain_orig = data;

    uint8_t *NAL_pointer; // pointer to NAL data
    unsigned long cur_NAL_LEN=0,cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;

    cur_NAL_LEN=len;
    NAL_pointer = &buff_plain_orig[0];
    h264_aes_encrypt(&sw_ctx, iv, NAL_pointer, len);
}

void EncryptionContext::try_encrypt_audio(uint8_t * data , int len)
{
    uint8_t * buff_plain_orig = data;
    unsigned long cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;
    //We only continue if more than 1 block is available
    if (len > 16)
    {
#if DEBUG_DECRYPT_STREAM

        av_log(NULL, AV_LOG_DEBUG, " 2 decrypt audio len : %lu \n",  len);
        {
            av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

#endif
        //Phung: pass @iv here as a dummy parameter
        //     since we encrypt using ECB mode, iv is not used anyway
		aes_encrypt(&sw_ctx_a, iv, data , len);

#if DEBUG_DECRYPT_STREAM
        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif

    }


}

// @log:h264_encryption.c@
// DateTime: 12Dec2013 - 09:30:00
// Author: trungdo
// Description: Initial version
