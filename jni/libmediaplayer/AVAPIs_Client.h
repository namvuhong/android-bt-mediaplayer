extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "tutk_inc/AVAPIs.h"
#include "tutk_inc/IOTCAPIs.h"
#include "tutk_inc/AVFRAMEINFO.h"
#include "tutk_inc/AVIOCTRLDEFs.h"
}
#include <pthread.h>
#include "output.h"
#include "decoder_video1.h"
#include "decoder_audio1.h"

class AVAPIsClient
{
public:
	AVAPIsClient();
	~AVAPIsClient();
	void init();
	void destroy();
	void start();
	void stop();
	void setDecoderVideo(DecoderVideo1* decoder_video1);
	void setDecoderAudio(DecoderAudio1* decoder_audio1);
	void setUIDs(char **UID, int numbId);
	int getCurrentVideoBitrate();

private:
	//void decodeTask(void* data, uint32_t size);
	pthread_mutex_t             mLock;
	static void*				thread_Speaker(void *ptr);
	static void*				thread_ReceiveAudio(void *ptr);
	static void*				thread_ReceiveVideo(void *ptr);
	static void*				thread_ConnectCCR(void *ptr);
	void 						PrintErrHandling(int nErr);
	bool initialized;
	int nNumUID;
	char *UID[4];
};
