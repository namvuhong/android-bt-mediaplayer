#ifndef FFMPEG_RECORDER_AUDIO_H
#define FFMPEG_RECORDER_AUDIO_H

#include "decoder.h"

#define FF_BUFQUEUE_SIZE  128

extern "C" {
#include "libavfilter/bufferqueue.h"
#include "libavfilter/buffersrc.h"
#include "libavfilter/buffersink.h"
#include "libavutil/mathematics.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
#include "libavutil/time.h"
#include "libavutil/samplefmt.h"
        
        
}
class RecorderAudio : public IDecoder
{
    public:
        RecorderAudio(AVStream* stream);
        RecorderAudio(AVCodecContext* avctx);

        ~RecorderAudio();

        void     write_audio_frame4(AVFormatContext *oc); 

        void     write_audio_frame2(AVFormatContext *oc); 
        bool     prepare( AVFormatContext *oc);
        bool     prepare( AVFormatContext *oc,  AVStream * src_stream );

        AVStream * getAudioStream(); 
        void     cleanUp(); 
        void     addFrame (AVFrame * frame); 

        void     initBuffQueue(); 
        void     destroyBuffQueue(); 

        bool     hasUnencodedFrames();
        void     flush_all_audio();
        void	set_first_ts(int64_t first_pts, int64_t first_dts);
        bool	is_first_ts_set();

    private:

        pthread_mutex_t     mLock;                                          
        pthread_cond_t      mCondition;  


        AVCodecContext * src_codec_context ;
        AVStream * src_stream;

        AVCodec *adpcm_codec; //use for recording
        AVCodecContext *adpcm_c; //use for recording

        AVFilterContext *buffersink_ctx;
        AVFilterContext *buffersrc_ctx;
        AVFilterGraph *filter_graph;
        struct SwrContext * swr2;

        uint8_t*  mRecordPacket; 
        int       mSamplesSize;

        uint8_t * leftOverBuff; 
        int       leftOverLen; 
        bool first_ts_set ;
        int64_t first_pts, first_dts;

        struct FFBufQueue * audioFrameQueue; 
        bool                       mStopRecording;

        int							encodeToAdpcm_init(AVFormatContext *oc);
        int                         init_filters(const char * desc); 

        int resample_audio_for_record(AVCodecContext *  enc, AVCodecContext * dec,  
                AVFrame *decoded_frame,   int*  out_len);

        int   write_audio_to_stream(AVFormatContext *oc,  uint8_t * buffer, int  len);
        int encode_write_frame(AVFormatContext *oc, AVFrame *filter_frame);

        void stopRecording(); 
        AVStream *    add_stream(AVFormatContext *oc, AVCodec **codec,
                enum AVCodecID codec_id);

        AVFrame * getFrame(); 
};

#endif 
