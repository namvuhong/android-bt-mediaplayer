#include "../mediaplayer.h"
#include <stdio.h>
#include <unistd.h> 

int main (int argc, char ** argv)
{

    printf("Test mediaplay built on ffmpeg 0.10.3\n"); 

    char * testFile = "cam1_dump.flv";
    if (argc < 2)
    {
        printf("usage: %s file_path \n", argv[0]); 
        exit(1); 
    }
    testFile = argv[1]; 


    MediaPlayer* mp = new MediaPlayer(false ); 

    //const char * testFile = "rtmp://112.213.86.13:1935/live/cam1.stream";
    //const char * testFile = "rtmp://127.0.0.1:1935/flvplayback/live";
    status_t status; 
    


    status = mp->setDataSource(testFile); 

    printf("setDataSource return: %d\n", status);

    if (status != NO_ERROR) // NOT OK
    {

        printf("setDataSource error: %d\n", status); 
        exit(1);
    }



    // Prepare the player 

    status=  mp->prepare(NULL, NULL ); 

    printf("prepare return: %d\n", status);
    if (status != NO_ERROR) // NOT OK
    {

        printf("prepare() error: %d\n", status); 
        exit(1);
    }

    // Play anyhow

    status=  mp->start(); 

    printf("start() return: %d\n", status);
    if (status != NO_ERROR) // NOT OK
    {

        printf("start() error: %d\n", status); 
        exit(1);
    }
    // sleep some time to see if the thread is really working 
    while (!mp->isPlaying());


    printf("start recording \n");
    mp->startRecord( "test.flv");

    bool exit = false; 
    int input; 
    while (exit == false)
    {
        sleep (3);

        //wake up and ask user 
        printf ("Do you want to exit? enter 1 to exit\n"); 
        scanf("%d",&input); 

        if ( input == 1)
        {
            break;  
        }
        else if (input == 2)
        {
       }
        else if (input ==3 )
        {
            mp->stopRecord();
        }

    }



    mp->suspend();
    mp->stop(); 

    printf ("Exiting...\n "); 
    
    delete mp;

    /////




    return 0; 
}
