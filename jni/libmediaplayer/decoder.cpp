#include "decoder.h"
#ifdef ANDROID_BUILD
#include <android/log.h>
#endif

#define TAG "FFMpegIDecoder"

#ifdef ANDROID_BUILD
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define log_info(...) printf(__VA_ARGS__)
#endif


IDecoder::IDecoder(AVStream* stream)
{
	mQueue = new PacketQueue();
	mStream = stream;
	avctx = NULL;
	idecoder_log = NULL;
	clock_sync = AV_SYNC_AUDIO_MASTER;//AV_SYNC_EXTERNAL_MASTER;
	forPlayback = false;
	isPause = false;
	last_pts = -1;
	clock_diff = 0;
	shouldUpdateClockDiff = true;
	mRecordingModeOnly = false;
	mInterrupted = false;
	mRecorder = NULL;
}

IDecoder::IDecoder(AVCodecContext* avctx)
{
	mQueue = new PacketQueue();
	this->avctx = avctx;
	mStream = NULL;
	idecoder_log = NULL;
	clock_sync = AV_SYNC_AUDIO_MASTER;//AV_SYNC_EXTERNAL_MASTER;
	forPlayback = false;
	isPause = false;
	last_pts = -1;
	clock_diff = 0;
	shouldUpdateClockDiff = true;
	mRecordingModeOnly = false;
	mInterrupted = false;
}

IDecoder::~IDecoder()
{
	isPause = false;
	if(mRunning)
    {
        stop();
    }

	//free(mQueue);
	/*
	 * use free(mQueue) would not call destructor of PacketQueue()
	 */
	delete mQueue;
}

void IDecoder::setRecorder(void *recorder) {
	mRecorder = recorder;
}

double IDecoder::getCurrentBufferDelay(double time_base)
{
	double current_delay = 0;
	if (mQueue != NULL)
	{
		current_delay = mQueue->getCurrentBufferDelay(time_base);
	}
	return current_delay;
}

void IDecoder::setSyncMaster(int sync_type)
{
	clock_sync = sync_type;
}

void IDecoder::pause()
{
	isPause = true;
	/* Reset the flag here, used for updating external clock diff. */
	shouldUpdateClockDiff = true;
}

bool IDecoder::isPaused()
{
	return isPause;
}

void IDecoder::resume()
{
	isPause = false;
}

void IDecoder::flush()
{
	mQueue->flush();
}

void IDecoder::enqueue(AVPacket* packet)
{
	mQueue->put(packet);
}

int IDecoder::dequeue(AVPacket* packet, bool block)
{
	return mQueue->get(packet, block);
}

int IDecoder::packets()
{
	return mQueue->size();
}

int IDecoder::bufferSize()
{
	return mQueue->sizeInKb();
}

void IDecoder::stop()
{
	// AA-2552 set mRunning flag to interrupt all pending tasks
	// Updated: don't set mRunning flag here, because the wait() method will check it before calling pthread_join().
	// Solution: use another flag
	mInterrupted = true;
	log_info("Aborting packet queue...");
	mQueue->abort();
	log_info("Waiting decoder stopped...");
	int ret = -1;
	if((ret = wait()) != 0) {
		log_error("Couldn't cancel decoder: %i", ret);
		return;
	}
	log_info("Decoder stopped");
}

void IDecoder::setPlaybackMode(bool isForPlayback)
{
	this->forPlayback = isForPlayback;
}

void IDecoder::setRecordingModeOnly(bool recordingModeOnly)
{
	this->mRecordingModeOnly = recordingModeOnly;
}

void IDecoder::handleRun(void* ptr)
{
	if(!prepare())
    {
		log_info("Couldn't prepare decoder");
        return;
    }
	decode(ptr);
}

bool IDecoder::prepare()
{
    return false;
}

bool IDecoder::process(AVPacket *packet)
{
	return false;
}

bool IDecoder::decode(void* ptr)
{
    return false;
}

void IDecoder::setLog( void (* log_callback)      (void* ptr, int level, const char* fmt, ...))
{

    idecoder_log = log_callback;
    return;
}
