#include "decoder.h"
#include "mediaplayer.h"
#include "rmc_inc/mylog.h"
#include "jniUtils.h"

extern "C"
{
#include "rmc_inc/flv_parser.h"
#include "rmc_client.h"
#include "file_streamer.h"
#include "libavutil/time.h"
}

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>

#define MAX_HANDSHAKE_BUF_SIZE 1024
#define HANDSHAKE_BUF_SIZE 96
#define MIN_HANDSHAKE_BUF_SIZE 50

#define MAX_LIVE_STREAM_CONN_TIMEOUT 10*1000000
#define MAX_PLAYBACK_CONN_TIMEOUT 15*1000000
#define MAX_COMMAND_TIMEOUT 5*1000000 //5s

#define MAX_HANDSHAKE_TIME 4*1000000 //4s
#define MAX_HANDSHAKE_SEND_TIME 1000000 //2s
#define BUFFER_SIZE 0.3
#define VIDEO_BUFFER_SIZE_MAX_IN_PKTS 45
#define MAX_BUFFER_SIZE 2

#define DEBUG_PKT_TIMESTAMP 0
#define DEBUG_BUFFERING 0
#define DEBUG_WRITE_TALKBACK_DATA_TO_FILE 0

#define TAG "librmc-client"

static int currentDataReceived, currentVideoBitrate;
static double start_time; //in micro seconds

static int gConnInfoCount;

static void flushDecoders(P2pContext *p2pCtx) {
    log_info("Flush decoders\n");
    if (p2pCtx == NULL) {
        log_info("flushDecoders, p2p context is NULL\n");
        return;
    }

    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
	if (sDecoderVideo1 != NULL)
	{
		sDecoderVideo1->flush();
		sDecoderVideo1->setSkipUntilNextKeyFrame(true);
	}

    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    if (sDecoderAudio1 != NULL) {
        sDecoderAudio1->flush();
        Output::AudioDriver_flush();
    }
}

static void resumeDecoders(P2pContext *p2pCtx) {
    if (p2pCtx == NULL) {
        log_info("resumeDecoders, p2p context is NULL\n");
        return;
    }

    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
    if (sDecoderAudio1 != NULL) {
        if (sDecoderAudio1->isPaused()) {
            sDecoderAudio1->resume();
            Output::AudioDriver_resume();
        }
    }

    if (sDecoderVideo1 != NULL && sDecoderVideo1->isPaused()) {
        sDecoderVideo1->resume();
    }
}

static void pauseDecoders(P2pContext *p2pCtx) {
    if (p2pCtx == NULL) {
        log_info("pauseDecoders, p2p context is NULL\n");
        return;
    }

    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
    
    if (sDecoderAudio1 != NULL) {
        if (!sDecoderAudio1->isPaused()) {
            sDecoderAudio1->pause();
            Output::AudioDriver_pause();
        }
    }

    if (sDecoderVideo1 != NULL && !sDecoderVideo1->isPaused()) {
        sDecoderVideo1->pause();
    }
}

static void processAudioBuffering(P2pContext *p2pCtx) {
    if (p2pCtx == NULL) {
        log_info("processAudioBuffering, p2p context is NULL\n");
        return;
    }

    MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;
    DecoderAudio1 *decoderAudio = (DecoderAudio1 *) p2pCtx->decoderAudio;
//buffering audio & video
    if (decoderAudio != NULL) {
        double aDelay = decoderAudio->getCurrentBufferDelay(1.0 / 1000.0);
        if (aDelay == 0) {
            if (!decoderAudio->isPaused()) {
#if DEBUG_BUFFERING
                log_info("stop decoders, buffering...\n");
#endif
                if (eventListener != NULL) {
                    eventListener->notify(MEDIA_INFO_START_BUFFERING, -1, -1);
                }
                pauseDecoders(p2pCtx);
            }
        }
        else if (aDelay > MAX_BUFFER_SIZE) {
#if DEBUG_BUFFERING
            log_info("audio delay %d, flush audio decoder...\n");
#endif
            flushDecoders(p2pCtx);
        }
        else if (aDelay > BUFFER_SIZE) {
            if (decoderAudio->isPaused()) {
#if DEBUG_BUFFERING
                log_info("start decoders, stop buffering...\n");
#endif
                if (eventListener != NULL) {
                    eventListener->notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
                }
                resumeDecoders(p2pCtx);
            }
            else {
#if DEBUG_BUFFERING
                log_info("audio delay: %f\n", aDelay);
#endif
            }
        }
        else {
#if DEBUG_BUFFERING
            log_info("audio delay: %f\n", aDelay);
#endif
        }
    }
}

static void processBufferingForLiveStream(P2pContext *p2pCtx) {
    if (p2pCtx == NULL) {
        log_error("processBuffering, p2p context is NULL\n");
        return;
    }

    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;

    if (sDecoderVideo1 != NULL) {
        int videoPkts = sDecoderVideo1->packets();
        if (videoPkts > VIDEO_BUFFER_SIZE_MAX_IN_PKTS) {
            log_info("Current video buffer size %d, drop to next key frame\n",
                                videoPkts);
            sDecoderVideo1->dropToNextKeyFrame();
        }
    }
}

static void processBufferingForFileStream(P2pContext *p2pCtx) {
    if (p2pCtx == NULL) {
        log_error("processBuffering, p2p context is NULL\n");
        return;
    }

    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;

    //buffering 6 seconds for videos
    if (sDecoderVideo1 != NULL && sDecoderAudio1 != NULL) {
        int videoPkts = sDecoderVideo1->packets();
        if (videoPkts == 0) {
            if (!sDecoderVideo1->isPaused() || !sDecoderAudio1->isPaused()) {
                if (eventListener != NULL) {
                    eventListener->notify(MEDIA_INFO_START_BUFFERING, -1, -1);
                }
                pauseDecoders(p2pCtx);
            }
        }
        else if (videoPkts > VIDEO_BUFFER_SIZE_MAX_IN_PKTS ) { // buffer video for 6 seconds
            if (sDecoderAudio1->isPaused() || sDecoderAudio1->isPaused()) {
                if (eventListener != NULL) {
                    eventListener->notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
                }
                resumeDecoders(p2pCtx);
            }
        }
        else {
//            log_debug("VideoPKts size %d", videoPkts);
        }
    }
}

static void processRecording(P2pContext *p2pCtx, AVPacket *packet, int type) {
    RecorderVideo *sRecorderVideo = NULL;
    DecoderAudio1 *decoderAudio = NULL;
    RecorderAudio *recorderAudio = NULL;
    if (p2pCtx != NULL) {
        sRecorderVideo = (RecorderVideo *) p2pCtx->recorderVideo;
        decoderAudio = (DecoderAudio1 *) p2pCtx->decoderAudio;
        recorderAudio = (RecorderAudio *) p2pCtx->recorderAudio;
    }

    // HOANG Check whether first timestamp of video recorder is set
    bool first_ts_set = false;
    if (sRecorderVideo != NULL) {
    	if (!sRecorderVideo->is_first_ts_set()) {
    		if (type == eMediaTypeVideo && (packet->flags & AV_PKT_FLAG_KEY == AV_PKT_FLAG_KEY))
    		{
    			log_info("Found first video key packet: pts %lld", packet->pts);
    			sRecorderVideo->set_first_ts(packet->pts, packet->dts);
    			if (decoderAudio != NULL) {
    				if (recorderAudio != NULL) {
    					recorderAudio->set_first_ts(packet->pts, packet->dts);
    					decoderAudio->setRecorder((void *)recorderAudio);
    				}
    			}
    		} else {
    			log_info("Skip this packet: pts %lld", packet->pts);
    		}
    	}
    	first_ts_set = sRecorderVideo->is_first_ts_set();
    }

    if (first_ts_set) {
    	switch (type) {
    	case eMediaTypeAudio:
    		//		if(sDecoderAudio1 != NULL)
    		//		{
    		//			sDecoderAudio1->setRecorderAudio(sRecorderAudio);
    		//		}
    		break;

    	case eMediaTypeVideo:
    		if (sRecorderVideo != NULL) {
    			AVPacket rPacket;
    			av_new_packet(&rPacket, packet->size);
    			rPacket.pts = packet->pts;
    			rPacket.dts = packet->dts;
    			rPacket.pos = packet->pos;
    			rPacket.duration = packet->duration;
    			rPacket.convergence_duration = packet->convergence_duration;
    			rPacket.stream_index = p2pCtx->recordingVideoIdx;
    			rPacket.flags = packet->flags;
    			memcpy(rPacket.data, packet->data, packet->size);
    			// log_info("video buffer size: %d\n", mRecorderVideo->packets());
    			sRecorderVideo->enqueue(&rPacket);
    		}
    		break;
    	}
    }
}

static void enqueueMediaPacket(int type, AVPacket packet, P2pContext *p2pCtx) {
    switch (type) {
        case eMediaTypeVideo:

        	if (p2pCtx->firstVideoTs == -1) {
        		log_info("First P2P video package pts: %lld\n", packet.pts);
        		p2pCtx->firstVideoTs = packet.pts;
        	}

            if (p2pCtx->decoderVideo != NULL) {
                ((DecoderVideo1 *) (p2pCtx->decoderVideo))->enqueue(&packet);
                // log_info("Video buffer size %d\n", sDecoderVideo1->packets());
                /* 20151030: HOANG: fix for VTech camera.
                 * Vtech camera takes 4-5s to stream out video
                 * So when app received video, extend timeout for audio decoder
                 */
                if (p2pCtx->decoderAudio != NULL) {
                    ((DecoderAudio1 *) (p2pCtx->decoderAudio))->extendTimeout();
                }
            }
            break;
        case eMediaTypeAudio:
                // log_info("AAAAA Receive %d bytes audio, ts %d\n", size, ts);
            if (p2pCtx->decoderAudio != NULL) {
                // log_info("AAAAA Curr audio delay %f\n", sDecoderAudio1->getCurrentBufferDelay(1.0 / 1000.0));
                ((DecoderAudio1 *) (p2pCtx->decoderAudio))->enqueue(&packet);
                // log_info("Audio Buffer size %d\n", sDecoderAudio1->packets());
                /* 20151030: HOANG: fix for VTech camera.
                 * Vtech camera takes 4-5s to stream out video
                 * So when app received audio, extend timeout for video decoder
                 */
                if (p2pCtx->decoderVideo != NULL) {
                    ((DecoderVideo1 *) (p2pCtx->decoderVideo))->extendTimeout();
                }
            }
            break;
        default:
            break;
    }
}

static void processLatencyChecking(P2pContext *p2pCtx) {
	if (p2pCtx == NULL) {
		log_error("processLatencyChecking, p2p context is NULL\n");
		return;
	}

	DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
	DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
	MediaPlayer *eventListener = (MediaPlayer *)p2pCtx->eventListener;

	/* Checking to flush buffer every 5 seconds. */
	timeval pTime2;
	static double time0 = -1;
	static double time1 = -1;
	gettimeofday(&pTime2, NULL);
	time1 = pTime2.tv_sec + (pTime2.tv_usec / 1000000.0);
	if (time0 == -1 || time1 > time0 + BUFFER_CHECKING_INTERVAL_IN_S)
	{
		if (time0 != -1)
		{
			if (sDecoderVideo1 != NULL)
			{
				int videoPkts = sDecoderVideo1->packets();
				log_info("*******************Process latency checking**************v %d, a %d", videoPkts, sDecoderAudio1 != NULL?sDecoderAudio1->packets():0);
				if (videoPkts > VIDEO_BUFFER_SIZE_MAX_IN_PKTS)
				{
					log_info("P2P buffers are too large now --> flush old packets to reduce latency.\n");
					flushDecoders(p2pCtx);
//					if (eventListener != NULL) {
//						eventListener->notify(MEDIA_INFO_CORRUPT_FRAME_TIMEOUT, -1, -1);
//					}
				}
			}
		}
		time0 = time1;
	}
//	log_info("P2P video buffer size: %d\n", sDecoderVideo1->packets());
}

/**
 * parse multiple flv packet to audio packet or video packet then send these packets to ffmpeg decoder
 * @param pkt data received from p2p channel
 * @param pktSize size of pkt
 * @param p2pCtx P2pContext
 * @return -1 if last packet
 */
static int FLVPacketParse(void *pkt, int pktSize, P2pContext *p2pCtx) {

    char cFrameBuf[200 * 1024] = {0};
    int iOutFrameSize = 0;
    int iSize = 0;
    int iRemainSize = pktSize;
    int iPktType = 0;
    unsigned char *pOffset = (unsigned char *) pkt;
    uint8_t *bff = (uint8_t *) pkt;

    if (pkt == NULL) return -2;
    if (pktSize <= 0 || pktSize < 4) return -3;
    // check if this is last packet from sender
    if (bff[0] == 0x98 && bff[1] == 'S' && bff[2] == 'T' && bff[3] == 'O' && bff[4] == 'P') {
    	if (!p2pCtx->isPlaybackCompleted) {
    		log_info("FLVPacketParse, received STOP packet");
    		p2pCtx->isPlaybackCompleted = 1;
    		MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;
    		if (eventListener != NULL) {
    			eventListener->notify(MEDIA_PLAYBACK_COMPLETE, 0, 0);
    		}
    	}
    	iRemainSize = 0;
    } else {
    	AVPacket packet = {0};
    	while (iRemainSize > 4) {
    		int subType = -1;
    		int type = -1;
    		int videoFrameType = -1;
    		iPktType = pOffset[0];
    		iSize = (pOffset[1] << 16 | pOffset[2] << 8 | pOffset[3]) & 0x00FFFFFF;
    		iSize += 15;
    		int ts = 0;
    		switch (iPktType) {
    		case 8:
    			if (FLVALawPacket2G722(pOffset, iSize, &(p2pCtx->g722Enc), cFrameBuf, &iOutFrameSize, &ts) == 0) {
    				// p2p audio type
    				subType = 23;
    				packet.data = (uint8_t *) cFrameBuf; //[frameData bytes];
    				packet.size = iOutFrameSize; //[frameData length];
    				packet.pts = ts;
    				packet.dts = ts;

    				if (p2pCtx->workingMode != WORKING_MODE_RECORDING_ONLY) {
    					// send to ffmpeg decode queue
    					processBufferingForFileStream(p2pCtx);
    				}

    				type = subType / 10;
    				if (p2pCtx->isRecording == 1) {
    					processRecording(p2pCtx, &packet, type);
    				}

    				enqueueMediaPacket(type, packet, p2pCtx);
    			}
    			break;
    		case 9:
    			videoFrameType = FLVideoPacket2H264(pOffset, iSize, cFrameBuf, &iOutFrameSize, &ts);
    			packet.data = (uint8_t *) cFrameBuf; //[frameData bytes];
    			packet.size = iOutFrameSize; //[frameData length];
    			packet.pts = ts;
    			packet.dts = ts;
    			//                log_error("FLVideoPacket2H264, type: %d, size: %d, ts: %d", videoFrameType, iSize, ts);
    			// check video frame type I-Frame or P-Frame
    			if (videoFrameType == 1) {
    				subType = 10;
    				packet.flags = AV_PKT_FLAG_KEY;
    				p2pCtx->keyFrameTotal++;
    				//                    log_error("Found KEY FRAME, size: %d, ts: %d", iSize, ts);
    			}
    			else if (videoFrameType == 0) {
    				subType = 12;
    			}

    			if (p2pCtx->workingMode != WORKING_MODE_RECORDING_ONLY) {
    				// send to ffmpeg decode queue
    				processBufferingForFileStream(p2pCtx);
    			}

    			type = subType / 10;
    			if (p2pCtx->isRecording == 1) {
    				processRecording(p2pCtx, &packet, type);
    			}

    			enqueueMediaPacket(type, packet, p2pCtx);
    		default:
    			break;
    		}

    		if (iRemainSize > iSize) {
    			iRemainSize -= iSize;
    		} else {
    			iRemainSize = 0;
    		}
    		pOffset += iSize;
    		//        log_info("FLVPacketParse iSize %d, remaining size %d, pOffset %p", iSize, iRemainSize, pOffset);
    	}
    }
    return iRemainSize;
}

static void decodeVideoTask(void *data, uint32_t size, eMediaSubType media_type, uint32_t ts, void *priv) {
    P2pContext *p2pCtx = (P2pContext *) priv;
    if (p2pCtx == NULL) {
        log_error("decodeVideoTask p2p context is NULL\n");
        return;
    }

    if (p2pCtx->isRunning == 1) {
        timeval pTime;
        gettimeofday(&pTime, NULL);
        start_time = pTime.tv_sec * 1000000.0 + pTime.tv_usec;

#if DEBUG_PKT_TIMESTAMP
        log_info("Receive %d bytes, type %d, ts %ld\n", size, media_type, ts);
#endif

//        static int imax_data_size = 100 * 1024;
//        void *ppFrameData = (void *) malloc(sizeof(char) * imax_data_size);
//        int iFrameSize = 0;
        int type = 0;

        AVPacket packet = {0};
        // 20160920: HOANG: handle SD card streaming data
        if (media_type == 29) {
            //			if (tsize == -1) {
            //				tsize = 0;
            //
            //				if (pFile == NULL) {
            //					pFile = fopen(_remoteFilePath.UTF8String, "ab");
            //				}
            //
            //				unsigned char rawData[57] = {
            //						0x46, 0x4C, 0x56, 0x01, 0x05, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00,
            //						0x00, 0x09, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            //						0x17, 0x00, 0x00, 0x00, 0x00, 0x01, 0x42, 0x00, 0x1F, 0xFF, 0xE1, 0x00,
            //						0x09, 0x67, 0x42, 0x00, 0x1F, 0xE9, 0x00, 0xA0, 0x0B, 0x72, 0x01, 0x00,
            //						0x04, 0x68, 0xCE, 0x38, 0x80, 0x00, 0x00, 0x00, 0x28
            //				};
            //
            //
            //				parseComingData(rawData+13, 44, &(_g722Enc), ppFrameData, &iFrameSize, &sType);
            //
            //				if (pFile) {
            //					fwrite (rawData, sizeof(uint8_t), 57, pFile);
            //				}
            //			}
            int64_t start = av_gettime();

            /* disable write to file
            if (p2pCtx->file == NULL) {
                log_info("Create FILE to save streaming clip");
                unsigned char rawData[57] =
                        {0x46, 0x4C, 0x56, 0x01, 0x05, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00,
                         0x00,
                         0x1D, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x17, 0x00, 0x00, 0x00, 0x00, 0x01, 0x42, 0x00, 0x1F, 0xFF, 0xE1, 0x00,
                         0x09,
                         0x67, 0x42, 0x00, 0x1F,
                         0xE9, 0x00, 0xA0, 0x0B, 0x72, 0x01, 0x00, 0x04, 0x68, 0xCE, 0x38, 0x80, 0x00, 0x00, 0x00,
                         0x28};

                p2pCtx->file = fopen("/mnt/sdcard/hubble/file_stream/file_stream.flv", "wb");
                fwrite(rawData, 1, sizeof(rawData), p2pCtx->file);
                log_info("write file header done");
                fflush(p2pCtx->file);
                fsync(fileno(p2pCtx->file));
            }

            if (p2pCtx->file == NULL) {
                log_info("Cannot open FILE");
            }

            if (p2pCtx->file != NULL) {
                if (data == NULL || size > 200000) {
                    log_error("Data is null or size %d", size);
                }
                else {
                    fwrite(data, 1, size, p2pCtx->file);
                    fflush(p2pCtx->file);
                    fsync(fileno(p2pCtx->file));
                }
            }
            */

            //int ts = parseComingData(data, size, &p2pCtx->g722Enc, ppFrameData, &iFrameSize, &subType);
            int parseCode = FLVPacketParse(data, size, p2pCtx);
            int total = (av_gettime() - start) / 1000;
//            log_info("FLVPacketParse parse time: %d ms\n", total);

            /* disable
            if (parseCode == -1) {
                log_info("End of FILE stream, close FILE");
                if (p2pCtx->file != NULL) {
                    fclose(p2pCtx->file);
                }
            }
            */
        }
        else {
        	type = media_type / 10;
            packet.data = (uint8_t *) data; //[frameData bytes];
            packet.size = size; //[frameData length];
            packet.pts = ts;
            packet.dts = ts;

            if (media_type == eMediaSubTypeVideoIFrame) {
                packet.flags = AV_PKT_FLAG_KEY;
                p2pCtx->keyFrameTotal++;
//                log_error("Found KEY FRAME, size: %d, ts: %d", size, ts);
            }

            if (type == eMediaTypeVideo) {
            	processLatencyChecking(p2pCtx);
            }
//            processBufferingForLiveStream(p2pCtx);

            if (p2pCtx->isRecording == 1) {
                processRecording(p2pCtx, &packet, type);
            }

            enqueueMediaPacket(type, packet, p2pCtx);
        }

        /**
         * calculate bit rate
         */
        static double t1 = -1;
        static double t2 = -1;
        gettimeofday(&pTime, NULL);
        currentDataReceived += size;
        if (t1 == -1) {
            t1 = pTime.tv_sec + pTime.tv_usec / 1000000.0;
        }
        t2 = pTime.tv_sec + pTime.tv_usec / 1000000.0;
        if (t2 >= t1 + 1) {
            currentVideoBitrate = (int) (currentDataReceived / (t2 - t1));
            currentDataReceived = 0;
            t1 = t2;
        }
    }
}

void *RmcClient::fileTransferStatusTask(void *ptr) {
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach file transfer status thread to VM failed");
    return NULL;
  }
#endif
	P2pContext *p2pCtx = (P2pContext *) ptr;
	if (p2pCtx != NULL) {
		RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) p2pCtx->rmcChannel;
		if (rmcChannelInfo != NULL) {
			RmcFileTransferContext *rmcFileTransferCtx = rmcChannelInfo->rmcTransferFileContext;
			if (rmcFileTransferCtx != NULL) {

				while (p2pCtx->isRunning == 1) {
					// check whether buffer has data or not
					int downloaded = rmcFileTransferCtx->fileBuffDownloaded;
					int consumed = rmcFileTransferCtx->fileBuffConsumed;
//					log_debug("RmcClient: consumed %d, downloaded %d", consumed, downloaded);
//					log_debug("Current download buff start index %p", &rmcFileTransferCtx->fileBuff[rmcFileTransferCtx->fileBuffConsumed]);

					if (rmcFileTransferCtx->fileTransferCompleted == 1) {
						// Check whether app already consumed whole buffer (remember to include the last 4 bytes)
						if (rmcFileTransferCtx->fileBuffConsumed + FLV_PREV_TAG_BYTE_COUNT >= rmcFileTransferCtx->fileBuffDownloaded) {
							log_info("RMC client, waiting decoders completed...");
							p2pCtx->isPlaybackCompleted = 1;
							break;
						}
					}

					/*
					 * Need to split sleep time into smaller sleep
					 */
					int count = 10;
					while (count-- > 0 && p2pCtx->isRunning == 1)
					{
						usleep(100000);
					}
				}

				// Check whether decoder buffers still have data
				if (p2pCtx->isPlaybackCompleted) {
					DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
					int videoPkts = 0;
					do {
						videoPkts = sDecoderVideo1 != NULL ? sDecoderVideo1->packets() : 0;
						if (videoPkts <= 0) {
							MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;
							if (eventListener != NULL) {
								eventListener->notify(MEDIA_PLAYBACK_COMPLETE, 0, 0);
							}
						} else {
							log_info("RMC client, waiting video decoder completed, remaining buffer: %d", videoPkts);
							resumeDecoders(p2pCtx);
						}

						/*
						 * Need to split sleep time into smaller sleep
						 */
						int count = 10;
						while (count-- > 0 && p2pCtx->isRunning == 1)
						{
							usleep(100000);
						}
					} while (videoPkts > 0 && p2pCtx->isRunning == 1);
				}
			} else {
				log_error("Start main file transfer status task failed, RMC file transfer context is NULL");
			}
		} else {
			log_error("Start main file transfer status task failed, RMC channel is NULL");
		}
	} else {
		log_error("Start main file transfer status task failed, p2p context is NULL");
	}
#if ANDROID_BUILD
  log_info("Dettach file transfer status thread from VM...");
  dettachVmFromThread();
#endif
}

int RmcClient::getCurrentVideoBitrate() {
    return currentVideoBitrate;
}

static void updateRmcBandwidthTask(void *privData, uint32_t rmcBandwidth) {
	P2pContext *p2pCtx = (P2pContext *) privData;
	if (p2pCtx != NULL) {
//		log_debug("updateRmcBandwidthTask %d", rmcBandwidth);
		if (p2pCtx->isRunning) {
			p2pCtx->currentRmcBandwidth = rmcBandwidth;
		}
	}
}

int RmcClient::getCurrentRmcBandwidth() {
	int currentRmcBandwidth = 0;
	if (mP2pCtx != NULL && mP2pCtx->isRunning) {
		currentRmcBandwidth = mP2pCtx->currentRmcBandwidth;
	}
	return currentRmcBandwidth;
}

void RmcClient::setDecoderVideo(void *decoderVideo) {
    mP2pCtx->decoderVideo = decoderVideo;
}

void RmcClient::setDecoderAudio(void *decoderAudio) {
    mP2pCtx->decoderAudio = decoderAudio;
}

void RmcClient::setRecorders(void *rVideo, void *rAudio) {
//    mP2pCtx->recorderVideo = rVideo;
//    mP2pCtx->recorderAudio = rAudio;
	mP2pCtx->recorderVideo = NULL;
	mP2pCtx->recorderAudio = NULL;

    RecorderVideo *recorderVideo = (RecorderVideo *) mP2pCtx->recorderVideo;
//    DecoderAudio1 *sDecoderAudio = (DecoderAudio1 *) mP2pCtx->decoderAudio;
//    RecorderAudio *sRecorderAudio = (RecorderAudio *) mP2pCtx->recorderAudio;
//    if (sDecoderAudio != NULL) {
//        sDecoderAudio->setRecorderAudio(sRecorderAudio);
//    }

    if (recorderVideo != NULL) {
        mP2pCtx->recordingVideoIdx = recorderVideo->getVideoStream()->index;
    }
}

void RmcClient::setRmcChannelInfo(void *rmcChannelInfo) {
    if (mP2pCtx != NULL) {
        mP2pCtx->rmcChannel = rmcChannelInfo;
    }
    else {
        log_error("Set rmc channel info, input is NULL\n");
    }
}

void RmcClient::setWorkingMode(int workingMode) {
    if (mP2pCtx != NULL) {
    	log_info("RMC working mode %d", workingMode);
        mP2pCtx->workingMode = workingMode;
    }
    else {
        log_error("Set rmc channel info, input is NULL\n");
    }
}

void RmcClient::setEventListener(void *eventListener) {
    if (mP2pCtx != NULL) {
        mP2pCtx->eventListener = eventListener;
    }
    else {
        log_error("Set event listener, input is NULL\n");
    }
}

void *RmcClient::video_timer_task(void *ptr) {
#if ANDROID_BUILD
    if(attachVmToThread () != JNI_OK)
    {
        log_info("Attach VM to thread video timer failed");
        return NULL;
    }
#endif
    timeval pTime;
    double time_now = -1;
    bool timeout = false;

    P2pContext *p2pCtx = (P2pContext *) ptr;
    DecoderAudio1 *sDecoderAudio1 = (DecoderAudio1 *) p2pCtx->decoderAudio;
    DecoderVideo1 *sDecoderVideo1 = (DecoderVideo1 *) p2pCtx->decoderVideo;
    MediaPlayer *eventListener = (MediaPlayer *) p2pCtx->eventListener;

//	if (sDecoderAudio1 != NULL)
//	{
//		sDecoderAudio1->last_updated = -1;
//		sDecoderVideo1->last_updated = -1;
//	}

    long maxConnectionTimeout = 0;
    if (p2pCtx->workingMode == WORKING_MODE_LIVE_STREAMING) {
    	maxConnectionTimeout = MAX_LIVE_STREAM_CONN_TIMEOUT;
    } else {
    	maxConnectionTimeout = MAX_PLAYBACK_CONN_TIMEOUT;
    }

    while (p2pCtx->isRunning == 1) {
        gettimeofday(&pTime, NULL);
        time_now = pTime.tv_sec * 1000000.0 + pTime.tv_usec;
        if (start_time != -1 && (time_now - start_time > maxConnectionTimeout)) {
            log_error("MediaSession received timeout.\n");
            timeout = true;
        }

        if (p2pCtx->workingMode == WORKING_MODE_LIVE_STREAMING) {
        	if (sDecoderVideo1->checkStreamAlive() == false ||
        			(sDecoderAudio1 != NULL && sDecoderAudio1->checkStreamAlive() == false)) {
        		timeout = true;
        	}
        }

        if (timeout == true) {
        	// Suspend media player as soon as possible to avoid wrong state
        	log_info("Interrupt media player due to timeout");
            if (eventListener != NULL) {
            	int keyFrameTotal = 0;
            	p2pCtx->isRunning = 0;
            	keyFrameTotal = p2pCtx->keyFrameTotal;
            	if (p2pCtx->workingMode == WORKING_MODE_LIVE_STREAMING) {
            		eventListener->suspend();
            		eventListener->notify(MEDIA_ERROR_SERVER_DIED, keyFrameTotal, 0);
            	} else {
            		if (p2pCtx->isPlaybackCompleted) {
            			log_info("Playback is completed, don't send error message");
            		} else {
            			eventListener->suspend();
            			eventListener->notify(MEDIA_ERROR_SERVER_DIED, keyFrameTotal, 0);
            		}
            	}
                break;
            }
        }

        /*
         * Need to split sleep time into smaller sleep
         */
        int count = 10;
        while (count-- > 0 && p2pCtx != NULL && p2pCtx->isRunning == 1) {
            usleep(100000);
        }
    }
#if ANDROID_BUILD
    log_info("Dettach video timer thread from VM thread...\n");
    dettachVmFromThread();
#endif
}

void RmcClient::sendCommand(const char *request, char **response) {
    log_info("RmcClient, send command: %s\n", request);
    if (mP2pCtx->isRunning == 0) {
        *response = NULL;
        log_info("send command err, RmcClient has stopped -> exit now\n");
    }
    else {
        rmcChannelSendCmd((RmcChannelInfo *) mP2pCtx->rmcChannel, request, response);
    }
}

void RmcClient::sendTalkbackData(uint8_t *talkbackData, int offset, int length) {
    static uint32_t ts = 0;
//	log_info("RmcClient, send talkback data, offset %d, length %d\n", offset, length);
//	int i;
//	for (i=offset; i<offset+8; i++)
//	{
//		log_info("%02x", talkbackData[i]);
//	}

    int out_size;
    g722_enc_encode(&mP2pCtx->g722Enc, (short *) talkbackData, length / 2, mP2pCtx->out_buf, &out_size);
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE //  write raw to file
    size_t dec_sample;
    g722_dec_decode((g722_dec_t *) mP2pCtx->g722Dec, mP2pCtx->out_buf, out_size, mP2pCtx->dec_buf, &dec_sample);
    FILE * pFile;
    //Simulator
    const char * file_path = "/storage/emulated/0/debug_talkback_data.raw";
    //audio_adpcm_after_encode.raw
    pFile = fopen(file_path, "ab");
    fwrite(mP2pCtx->dec_buf, sizeof(int16_t), dec_sample, pFile);
    fclose(pFile);
#endif

    if (mP2pCtx->isRunning == 1) {
        if (rmcChannelSendTalkbackData((RmcChannelInfo *) mP2pCtx->rmcChannel, talkbackData, offset, length) < 0)
            //if(rmcChannelSendTalkbackData((RmcChannelInfo *) mP2pCtx->rmcChannel, mP2pCtx->out_buf, out_size, ts++) < 0)
        {
            log_error("(%s:%d) Send talk back data failed", __FILE__, __LINE__);
        }
        else {
            // log_debug("(%s:%d) Send talk back data size %d, ts %ld", __FILE__, __LINE__, out_size, ts);
        }
    }
    else {
        log_error("mRmcClient has stopped, dont send talkback data");
    }
}

RmcClient::RmcClient() {
    currentVideoBitrate = 0;
    currentDataReceived = 0;
    start_time = -1;
    initP2pContext();
}

void RmcClient::initP2pContext() {
    mP2pCtx = (P2pContext *) malloc(sizeof(P2pContext));
    if (mP2pCtx != NULL) {
        mP2pCtx->decoderAudio = NULL;
        mP2pCtx->decoderVideo = NULL;
        mP2pCtx->recorderAudio = NULL;
        mP2pCtx->recorderVideo = NULL;
        mP2pCtx->rmcChannel = NULL;
        mP2pCtx->recordingVideoIdx = -1;
        mP2pCtx->eventListener = NULL;
        mP2pCtx->keyFrameTotal = 0;
        mP2pCtx->isFileOpen = 0;
        mP2pCtx->currentRmcBandwidth = 0;
        mP2pCtx->workingMode = WORKING_MODE_LIVE_STREAMING;
        mP2pCtx->isPlaybackCompleted = 0;
        mP2pCtx->file = NULL;
        mP2pCtx->firstVideoTs = -1;
//        log_info("Init first video ts %lld\n", mP2pCtx->firstVideoTs);
        //mP2pCtx->g722Enc = (void *) malloc(sizeof(g722_enc_t));
        //g722_enc_init((g722_enc_t *)mP2pCtx->g722Enc);
        g722_enc_init(&mP2pCtx->g722Enc);
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE
        mP2pCtx->g722Dec = (void *) malloc(sizeof(g722_dec_t));
        g722_dec_init((g722_dec_t *)mP2pCtx->g722Dec);
#endif
    } else {
    	log_error("Failed to alloc P2pContext\n");
    }
}

int64_t RmcClient::getFirstVideoTs()
{
	int64_t firstVideoTs = -1;
	if (mP2pCtx != NULL) {
		firstVideoTs = mP2pCtx->firstVideoTs;
	}
	return firstVideoTs;
}

void RmcClient::destroyP2pContext() {
    if (mP2pCtx != NULL) {
        log_info("Destroy p2p context");
        //g722_enc_deinit((g722_enc_t *)mP2pCtx->g722Enc);
        g722_enc_deinit(&mP2pCtx->g722Enc);
        //free(mP2pCtx->g722Enc);
        //mP2pCtx->g722Enc = NULL;
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE
        g722_dec_deinit((g722_dec_t *)mP2pCtx->g722Dec);
        free(mP2pCtx->g722Dec);
        mP2pCtx->g722Dec = NULL;
#endif
        free(mP2pCtx);
        mP2pCtx = NULL;
    }
}

RmcClient::~RmcClient() {
    destroyP2pContext();
}

void RmcClient::startRecording() {
    mP2pCtx->isRecording = 1;
}

void RmcClient::stopRecording() {
    mP2pCtx->isRecording = 0;
}

/*Must call setConnInfoCount & setConnInfo before start RmcClient
 */
void RmcClient::start() {
//mylog_info("Listening at port %d\n", sess->endpoint->local_port);
    mP2pCtx->isRunning = 1;
//log_info("start RmcClient\n");
    int status;
    log_info("RmcClient, start media receiver thread\n");
    timeval pTime;
    gettimeofday(&pTime, NULL);
    start_time = pTime.tv_sec * 1000000.0 + pTime.tv_usec;

    RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) mP2pCtx->rmcChannel;
    rmcChannelInfoRegisterCallback(rmcChannelInfo, (void *) mP2pCtx, decodeVideoTask, updateRmcBandwidthTask);
    if (rmcChannelInfo->rmcSessionType == RMC_SESSION_TYPE_LIVE_STREAMING) {
    	log_info("RmcClient, start video timer thread\n");
    	status = pthread_create(&mP2pCtx->videoTimerThread, NULL, video_timer_task, (void *) mP2pCtx);
    	if (status < 0) {
    		log_info("RmcClient, start video timer thread failed, ret %d\n", status);
    	}
    } else {
    	log_info("RmcClient, start playing from file transfer thread\n");
    	status = pthread_create(&mP2pCtx->videoTimerThread, NULL, fileTransferStatusTask, (void *) mP2pCtx);
    	if (status < 0) {
    		log_info("RmcClient, start playing from file transfer thread failed, ret %d\n", status);
    	}
    }
}

void RmcClient::stop() {
    if (mP2pCtx != NULL) {
        mP2pCtx->isRecording = 0;
        mP2pCtx->isRunning = 0;
        log_info("stop RMC Client\n");
        int i;
        if (mP2pCtx->rmcChannel != NULL) {
            rmcChannelInfoUnregisterCallback((RmcChannelInfo *) mP2pCtx->rmcChannel);

            //rmcChannelStopSession((RmcChannelInfo *) mP2pCtx->rmcChannel);
            log_info("Waiting rmc client timer thread stopped...\n");
            if (pthread_join(mP2pCtx->videoTimerThread, NULL) != 0) {
                log_error("Cannot stop rmc client timer thread\n");
            }
            log_info("RMC client timer thread stopped...DONE\n");
        }
    }
}
