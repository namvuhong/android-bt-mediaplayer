#ifndef FFMPEG_OUTPUT_H
#define FFMPEG_OUTPUT_H
#include <sys/types.h>

#ifdef ANDROID_BUILD
#include <jni.h>

#include <android/native_window_jni.h>
#endif

#if !defined(ANDROID_BUILD)

#define JNIEnv void
#define jobject void *
#define jmethodID void *

#define ANativeWindow void
#define ANativeWindow_Buffer void *

#endif



class Output
{
    private:
    static jmethodID audio_write_cb;
    static jmethodID audio_stop_cb;
    static jmethodID audio_mute_cb;
    static jobject mediaPlayerObject;
    static jmethodID get_audio_buff_cb;
    static jmethodID audio_process_cb;
    static jmethodID get_audio_unread_buff;
    static jmethodID audio_flush_buff_cb;
    static jmethodID is_audio_buff_full;
    static jmethodID audio_pause_cb;
    static jmethodID audio_resume_cb;
    static jmethodID surface_init_cb;
    static jmethodID surface_render_cb;
    static jmethodID surface_release_cb;

    static jmethodID native_getNewBitMap;
    static jmethodID native_updateVideoSurface;

    static void * pictureRGB;

    static ANativeWindow* theNativeWindow;
    static ANativeWindow_Buffer * pictureBuffer;
    static jbyte* g_buffer;

    public:

    static int					unregister();
    static int AudioDriver_register(JNIEnv * env, jobject thiz);

    static int					AudioDriver_set(int streamType,
            uint32_t sampleRate,
            int format,
            int channels);
    static int					AudioDriver_start();
    static int					AudioDriver_flush();
    static int					AudioDriver_pause();
    static int					AudioDriver_resume();
    static int					AudioDriver_stop();
    static int					AudioDriver_reload();
    static int          AudioDriver_setAudioStreamMute(bool isMuted);
    static int					AudioDriver_write(void *buffer, int buffer_size, double pts);
    static long                 AudioDriver_buff_size();
    static int					AudioDriver_isAudioBuffFull();
    static int					AudioDriver_unregister();

    static int					VideoDriver_register(JNIEnv* env, jobject thiz, jobject jsurface);
    static int					VideoDriver_getPixels(int width, int height, void** pixels);
    static int					VideoDriver_getHeight();
    static int					VideoDriver_getWidth();
    static int					VideoDriver_updateSurface();
    static int					VideoDriver_unregister();
    static int					VideoDriver_getSnapShot(void** buffer, int* len);
};

#endif //FFMPEG_DECODER_H
