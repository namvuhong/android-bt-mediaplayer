#ifndef FFMPEG_DECODER_H
#define FFMPEG_DECODER_H

extern "C" {

#include "libavcodec/avcodec.h"
#include "libavcodec/thread.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/time.h"
#include "libavutil/log.h"
#include "mylog.h"
}

#include "thread.h"
#include "packetqueue.h"

enum {
	AV_NO_SYNC,
    AV_SYNC_AUDIO_MASTER,
    AV_SYNC_VIDEO_MASTER,
    AV_SYNC_EXTERNAL_MASTER,
};

class IDecoder : public Thread
{
public:
	IDecoder(AVStream* stream);
	IDecoder(AVCodecContext* avctx);
	~IDecoder();

    void						stop();
	void						enqueue(AVPacket* packet);
	int							dequeue(AVPacket* packet, bool block);
	int							packets();
	double						getCurrentBufferDelay(double time_base);
	int							bufferSize();
	int                         clock_sync;
	int64_t                     external_clock_time;
	double 						last_updated;
	double clock_diff;
	bool shouldUpdateClockDiff;
	void                        setLog( void (* log_callback)      (void* ptr, int level, const char* fmt, ...));
	bool						forPlayback;
	void						setPlaybackMode(bool isForPlayback);
	void						pause();
	bool						isPaused();
	void						flush();
	void						resume();
	bool						isPause;
	bool						mInterrupted;
	void						setSyncMaster(int sync_type);
	void						setRecordingModeOnly(bool);
	void						setRecorder(void*);

protected:
    PacketQueue*                mQueue;
    AVStream*             		mStream;
    AVCodecContext*				avctx;
    void*						mRecorder;
    double						mDecodePktStartTime;
    bool						mRecordingModeOnly;
    virtual bool                prepare();
    virtual bool                decode(void* ptr);
    virtual bool                process(AVPacket *packet);
	void						handleRun(void* ptr);
	void (* idecoder_log)      (void* ptr, int level, const char* fmt, ...);
	int64_t						last_pts;
};

#endif //FFMPEG_DECODER_H
