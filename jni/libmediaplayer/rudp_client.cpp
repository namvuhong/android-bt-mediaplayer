#include "decoder.h"
#include "rudp_client.h"
#include "mediaplayer.h"
#include <android/log.h>
#include "../zjni/jniUtils.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
extern "C"
{
#include "g722_enc.h"
#include "g722_dec.h"
}

#define MAX_HANDSHAKE_BUF_SIZE 1024
#define HANDSHAKE_BUF_SIZE 96
#define MIN_HANDSHAKE_BUF_SIZE 50

#define MAX_CONN_TIMEOUT 7*1000000 //7s
#define MAX_COMMAND_TIMEOUT 5*1000000 //5s

#define MAX_HANDSHAKE_TIME 4*1000000 //4s
#define MAX_HANDSHAKE_SEND_TIME 1000000 //2s
#define BUFFER_SIZE 0.3
#define MAX_BUFFER_SIZE 2

#define DEBUG_BUFFERING 0
#define DEBUG_WRITE_TALKBACK_DATA_TO_FILE 0

#define TAG "librudp-client"

static int currentDataReceived, currentVideoBitrate;
static DecoderVideo1* sDecoderVideo1;
static DecoderAudio1* sDecoderAudio1;
static RecorderVideo *sRecorderVideo;
static RecorderAudio *sRecorderAudio;
static bool isRunning;
static double start_time; //in micro seconds

static ConnectionInfo** gConnInfo;
static int gConnInfoCount;
static int media_idx;
static pthread_mutex_t send_cmd_lock;
static g722_enc_t g722_enc;
static g722_dec_t g722_dec;
static uint8_t out_buf[4 * 1024];
static int16_t dec_buf[4 * 1024];
static bool isHandshakeSucceeded;
static int video_stream_idx;
static bool isRecording;

static void flushDecoders()
{
	__android_log_print(ANDROID_LOG_INFO, TAG, "Flush decoders\n");
//	if (sDecoderVideo1 != NULL)
//	{
//		sDecoderVideo1->flush();
//	}

	if (sDecoderAudio1 != NULL)
	{
		sDecoderAudio1->flush();
		Output::AudioDriver_flush();
	}
}

static ConnectionInfo* getConnectionInfo(int index)
{
	return gConnInfo[index];
}


static void resumeDecoders()
{
	if (sDecoderAudio1 != NULL)
	{
		if (sDecoderAudio1->isPaused())
		{
			sDecoderAudio1->resume();
			Output::AudioDriver_resume();
		}
	}

//	if (sDecoderVideo1 != NULL && sDecoderVideo1->isPaused())
//	{
//		sDecoderVideo1->resume();
//	}
}

static void pauseDecoders()
{
	if (sDecoderAudio1 != NULL)
	{
		if (!sDecoderAudio1->isPaused())
		{
			sDecoderAudio1->pause();
			Output::AudioDriver_pause();
		}
	}

//	if (sDecoderVideo1 != NULL && !sDecoderVideo1->isPaused())
//	{
//		sDecoderVideo1->pause();
//	}
}

static void processAudioBuffering()
{
  //buffering audio & video
  if (sDecoderAudio1 != NULL)
  {
    double aDelay = sDecoderAudio1->getCurrentBufferDelay(1.0/1000.0);
    if (aDelay == 0)
    {
      if (!sDecoderAudio1->isPaused())
      {
#if DEBUG_BUFFERING
        __android_log_print(ANDROID_LOG_INFO, TAG, "stop decoders, buffering...\n");
#endif
        if (gConnInfo[media_idx]->event_listener != NULL)
        {
          gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_START_BUFFERING, -1, -1);
        }
        pauseDecoders();
      }
    }
    else if (aDelay > MAX_BUFFER_SIZE)
    {
#if DEBUG_BUFFERING
        __android_log_print(ANDROID_LOG_INFO, TAG, "audio delay %d, flush audio decoder...\n");
#endif
      flushDecoders();
    }
    else if (aDelay > BUFFER_SIZE)
    {
      if (sDecoderAudio1->isPaused())
      {
#if DEBUG_BUFFERING
        __android_log_print(ANDROID_LOG_INFO, TAG, "start decoders, stop buffering...\n");
#endif
        if (gConnInfo[media_idx]->event_listener != NULL)
        {
          gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
        }
        resumeDecoders();
      }
      else
      {
#if DEBUG_BUFFERING
        __android_log_print(ANDROID_LOG_INFO, TAG,
            "audio delay: %f\n", aDelay);
#endif
      }
    }
    else
    {
#if DEBUG_BUFFERING
      __android_log_print(ANDROID_LOG_INFO, TAG,
          "audio delay: %f\n", aDelay);
#endif
    }
  }
}

static void processBuffering()
{
	//buffering audio & video
	if (sDecoderAudio1 != NULL && sDecoderVideo1 != NULL)
	{
	  double aDelay = sDecoderAudio1->getCurrentBufferDelay(1.0/1000.0);
	  double vDelay = sDecoderVideo1->getCurrentBufferDelay(1.0/1000.0);
		if (aDelay == 0)
		{
			if (!sDecoderVideo1->isPaused() || !sDecoderAudio1->isPaused())
			{
#if DEBUG_BUFFERING
				__android_log_print(ANDROID_LOG_INFO, TAG, "stop decoders, buffering...\n");
#endif
				if (gConnInfo[media_idx]->event_listener != NULL)
				{
					gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_START_BUFFERING, -1, -1);
				}
				pauseDecoders();
			}
		}
		else if (aDelay > BUFFER_SIZE && vDelay > BUFFER_SIZE)
		{
			if (sDecoderAudio1->isPaused() || sDecoderAudio1->isPaused())
			{
#if DEBUG_BUFFERING
				__android_log_print(ANDROID_LOG_INFO, TAG, "start decoders, stop buffering...\n");
#endif
				if (gConnInfo[media_idx]->event_listener != NULL)
				{
					gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
				}
				resumeDecoders();
			}
			else
			{
#if DEBUG_BUFFERING
				__android_log_print(ANDROID_LOG_INFO, TAG,
						"audio delay: %f, video delay: %f\n", aDelay, vDelay);
#endif
			}
		}
		else
		{
#if DEBUG_BUFFERING
			__android_log_print(ANDROID_LOG_INFO, TAG,
					"audio delay: %f, video delay: %f\n", aDelay, vDelay);
#endif
		}
	}
	else if (sDecoderVideo1 != NULL) //no audio
	{
	  double vDelay = sDecoderVideo1->getCurrentBufferDelay(1.0/1000.0);
		if (vDelay == 0)
		{
			if (!sDecoderVideo1->isPaused())
			{
#if DEBUG_BUFFERING
				__android_log_print(ANDROID_LOG_INFO, TAG, "stop video decoder, buffering...\n");
#endif
				if (gConnInfo[media_idx]->event_listener != NULL)
				{
					gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_START_BUFFERING, -1, -1);
				}
				pauseDecoders();
			}
		}
		else if (vDelay > BUFFER_SIZE)
		{
			if (sDecoderVideo1->isPaused())
			{
#if DEBUG_BUFFERING
				__android_log_print(ANDROID_LOG_INFO, TAG, "start video decoder, stop buffering...\n");
#endif
				if (gConnInfo[media_idx]->event_listener != NULL)
				{
					gConnInfo[media_idx]->event_listener->notify(MEDIA_INFO_STOP_BUFFERING, -1, -1);
				}
				resumeDecoders();
			}
		}
		else
		{
#if DEBUG_BUFFERING
			__android_log_print(ANDROID_LOG_INFO, TAG, "video delay: %f\n", vDelay);
#endif
		}
	}
}

static void processRecording(AVPacket *packet, int type)
{
	switch (type)
	{
	case eMediaTypeAudio:
//		if(sDecoderAudio1 != NULL)
//		{
//			sDecoderAudio1->setRecorderAudio(sRecorderAudio);
//		}
		break;

	case eMediaTypeVideo:
		if (sRecorderVideo != NULL)
		{
			AVPacket rPacket;
			av_new_packet(&rPacket, packet->size);
			rPacket.pts = packet->pts;
			rPacket.dts = packet->dts;
			rPacket.pos = packet->pos;
			rPacket.duration = packet->duration;
			rPacket.convergence_duration = packet->convergence_duration;
			rPacket.stream_index = video_stream_idx;
			rPacket.flags = packet->flags;
			memcpy(rPacket.data, packet->data, packet->size);
			// __android_log_print(ANDROID_LOG_INFO, TAG,  "video buffer size: %d\n", mRecorderVideo->packets());
			sRecorderVideo->enqueue(&rPacket);
		}
		break;
	}
}

static void decodeVideoTask(void* data, uint32_t size, eMediaSubType media_type, uint32_t ts)
{
	if (isRunning == true)
	{
		timeval pTime;
		gettimeofday(&pTime, NULL);
		start_time = pTime.tv_sec*1000000.0 + pTime.tv_usec;
		int type = media_type / 10;
		//__android_log_print(ANDROID_LOG_INFO, TAG, "Receive %d bytes, type %d\n", size, media_type);
		AVPacket packet = {0};
		packet.data = (uint8_t*)data;//[frameData bytes];
		packet.size = size;//[frameData length];
		packet.pts = ts;
		packet.dts = ts;
		if (media_type == eMediaSubTypeVideoIFrame)
		{
			packet.flags = AV_PKT_FLAG_KEY;
		}

		if (gConnInfo[0]->p2p_mode != P2P_MODE_LOCAL)
		{
			//processBuffering();
		  processAudioBuffering();
		}

		if (isRecording == true)
		{
			processRecording(&packet, type);
		}

		switch (type) {
		case eMediaTypeVideo:
//			__android_log_print(ANDROID_LOG_INFO, TAG, "VVVVV Receive %d bytes video, ts %d, isKey? %d\n", size, ts, (media_type == eMediaSubTypeVideoIFrame));
			if (sDecoderVideo1 != NULL)
			{
				sDecoderVideo1->enqueue(&packet);
//				__android_log_print(ANDROID_LOG_INFO, TAG, "Video buffer size %d\n", sDecoderVideo1->packets());
				/* 20151030: HOANG: fix for VTech camera.
				 * Vtech camera takes 4-5s to stream out video
				 * So when app received video, extend timeout for audio decoder
				 */
				if (sDecoderAudio1 != NULL) {
				  sDecoderAudio1->extendTimeout();
				}
			}
			break;
		case eMediaTypeAudio:
//			__android_log_print(ANDROID_LOG_INFO, TAG, "AAAAA Receive %d bytes audio, ts %d\n", size, ts);
			if (sDecoderAudio1 != NULL)
			{
//				__android_log_print(ANDROID_LOG_INFO, TAG, "AAAAA Curr audio delay %f\n", sDecoderAudio1->getCurrentBufferDelay(1.0 / 1000.0));
				sDecoderAudio1->enqueue(&packet);
//				__android_log_print(ANDROID_LOG_INFO, TAG, "Audio Buffer size %d\n", sDecoderAudio1->packets());
				/* 20151030: HOANG: fix for VTech camera.
				 * Vtech camera takes 4-5s to stream out video
				 * So when app received audio, extend timeout for video decoder
				 */
				if (sDecoderVideo1 != NULL) {
				  sDecoderVideo1->extendTimeout();
				}
			}
			break;
		default:
			break;
		}

		static double t1 = -1;
		static double t2 = -1;
		gettimeofday(&pTime, NULL);
		currentDataReceived += size;
		if (t1 == -1)
		{
			t1 = pTime.tv_sec + pTime.tv_usec / 1000000.0;
		}
		t2 = pTime.tv_sec + pTime.tv_usec / 1000000.0;
		if (t2 >= t1 + 1)
		{
			currentVideoBitrate = (int) (currentDataReceived / (t2 - t1));
			currentDataReceived = 0;
			t1 = t2;
		}
	}
}

int RUDPClient::getCurrentVideoBitrate()
{
	return currentVideoBitrate;
}

void RUDPClient::setDecoderVideo(DecoderVideo1 *decoder_video1)
{
	sDecoderVideo1 = decoder_video1;
}

void RUDPClient::setDecoderAudio(DecoderAudio1 *decoder_audio1)
{
	sDecoderAudio1 = decoder_audio1;
}

void RUDPClient::setRecorders(RecorderVideo *rVideo, RecorderAudio *rAudio)
{
	sRecorderVideo = rVideo;
	sRecorderAudio = rAudio;
	if(sDecoderAudio1 != NULL)
	{
		sDecoderAudio1->setRecorderAudio(sRecorderAudio);
	}

	if (sRecorderVideo != NULL)
	{
		video_stream_idx = sRecorderVideo->getVideoStream()->index;
	}
}

static int remoteHandshakeDataPort(int sock_fd, char *dest_ip, int dest_port)
{
	__android_log_print(ANDROID_LOG_INFO, TAG, "Handshake DATA port, dst addr %s, dst port %d\n", dest_ip, dest_port);
	struct sockaddr_in  addr;
	struct in_addr      destAddr;
	/* Connect to destination address */
	if(inet_aton(dest_ip, &destAddr) == 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Address %s is invalid\n", dest_ip);
		return -1;
	}

	addr.sin_family =  AF_INET;
	addr.sin_addr = destAddr;
	addr.sin_port = htons(dest_port);
	if(connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Error when connecting socket %d\n", sock_fd);
		return -1;
	}

	int ret = -1;
	int handshakeSend = 1000;
	int handshakeRecv = -1;
	char                sendBuf[HANDSHAKE_BUF_SIZE];
	char                recvBuf[MAX_HANDSHAKE_BUF_SIZE];

	int tmpNum = -1;
	int size = -1;
	while (ret == -1 && isRunning)
	{
		memset(recvBuf, 0, sizeof(recvBuf));
		memset(sendBuf, 0, sizeof(sendBuf));
		sprintf(sendBuf, "%d", handshakeSend);
		//size = send(sock_fd, sendBuf, strlen(sendBuf), 0);
		size = send(sock_fd, sendBuf, sizeof(sendBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d, ret %d\n",
				sendBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info failed");
		}
		//__android_log_print(ANDROID_LOG_INFO, TAG, "video worker thread: receive from fd %d\n", vHSEndpoint->recvFd);
		size = recv(sock_fd, recvBuf, sizeof(recvBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info %s from fd %d ret %d\n", recvBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
		}
		tmpNum = atoi(recvBuf);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receive handshake no %d\n", tmpNum);
		if (tmpNum == handshakeSend + 1)
		{
			//handshake OK
			ret = 0;
			__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE DATA PORT OK---\n");
		}
		else if (tmpNum > 0)
		{
			// make a timestamp
			ret = 0;
			handshakeRecv = tmpNum + 1;
			memset(sendBuf, 0, sizeof(sendBuf));
			sprintf(sendBuf, "%d", handshakeRecv);
//			size = send(sock_fd, sendBuf, strlen(sendBuf), 0);
			size = send(sock_fd, sendBuf, sizeof(sendBuf), 0);
			if(size < MIN_HANDSHAKE_BUF_SIZE)
			{
				__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d, ret %d\n",
						sendBuf, sock_fd, size);
			}

			timeval pTime0, pTime1;
			double time0 = -1;
			double time1 = -1;
			gettimeofday(&pTime0, NULL);
			time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
			while (time1 - time0 < 2000000.0)
			{
				gettimeofday(&pTime1, NULL);
				time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
				memset(recvBuf, 0, sizeof(recvBuf));
				size = recv(sock_fd, recvBuf, sizeof(recvBuf), 0);
				__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info %s from fd %d ret %d\n", recvBuf, sock_fd, size);
				if(size < MIN_HANDSHAKE_BUF_SIZE)
				{
					//__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
				}

				tmpNum = atoi(recvBuf);
				__android_log_print(ANDROID_LOG_INFO, TAG, "Receive handshake no %d\n", tmpNum);
				if (tmpNum == handshakeSend + 1 || tmpNum == handshakeRecv + 1)
				{
					__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE DATA PORT OK---\n");
					break;
				}

				usleep(100000);
			}
		}

		usleep(100000);
	}

	return ret;
}

static int remoteHandshakeAckPort(int sock_fd, char *dest_ip, int dest_port)
{
	__android_log_print(ANDROID_LOG_INFO, TAG, "Handshake port, dst addr %s, dst port %d\n", dest_ip, dest_port);
	struct sockaddr_in  addr;
	struct in_addr      destAddr;
	/* Connect to destination address */
	if(inet_aton(dest_ip, &destAddr) == 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Address %s is invalid\n", dest_ip);
		return -1;
	}

	addr.sin_family =  AF_INET;
	addr.sin_addr = destAddr;
	addr.sin_port = htons(dest_port);
	if(connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Error when connecting socket %d\n", sock_fd);
		return -1;
	}

	int ret = -1;
	int handshakeSend = 1000;
	int handshakeRecv = -1;
	char                sendBuf[HANDSHAKE_BUF_SIZE];
	char                recvBuf[MAX_HANDSHAKE_BUF_SIZE];
	int tmpNum = -1;
	int size = -1;
	timeval pTime0, pTime1;
	double time0 = -1;
	double time1 = -1;
	gettimeofday(&pTime0, NULL);
	time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
	while (ret == -1 && (time1 - time0 < MAX_HANDSHAKE_TIME) && isRunning)
	{
		gettimeofday(&pTime1, NULL);
		time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
		memset(recvBuf, 0, sizeof(recvBuf));
		memset(sendBuf, 0, sizeof(sendBuf));
		sprintf(sendBuf, "%d", handshakeSend);
//		size = send(sock_fd, sendBuf, strlen(sendBuf), 0);
		size = send(sock_fd, sendBuf, sizeof(sendBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d, ret %d\n",
				sendBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			//__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info failed");
		}
		//__android_log_print(ANDROID_LOG_INFO, TAG, "video worker thread: receive from fd %d\n", vHSEndpoint->recvFd);
		size = recv(sock_fd, recvBuf, sizeof(recvBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info %s from fd %d ret %d\n", recvBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			//__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
		}
		tmpNum = atoi(recvBuf);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receive handshake no %d\n", tmpNum);
		if (tmpNum == handshakeSend + 1)
		{
			//hand shake OK
			ret = 0;
			__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE PORT OK---\n");
			break;
		}
		else if (tmpNum > 0)
		{
			ret = 0;
			handshakeRecv = tmpNum + 1;
			memset(sendBuf, 0, sizeof(sendBuf));
			sprintf(sendBuf, "%d", handshakeRecv);
			time0 = -1;
			time1 = -1;
			gettimeofday(&pTime0, NULL);
			time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
			while ((time1 - time0 < MAX_HANDSHAKE_SEND_TIME) && isRunning)
			{
				gettimeofday(&pTime1, NULL);
				time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
//				size = send(sock_fd, sendBuf, strlen(sendBuf), 0);
				size = send(sock_fd, sendBuf, sizeof(sendBuf), 0);
				__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d ret %d\n",
						sendBuf, sock_fd, size);
				if(size < MIN_HANDSHAKE_BUF_SIZE)
				{
					//__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
				}

				usleep(100000);
			}
			__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE PORT OK---\n");
		}

		usleep(100000);
	}

	return ret;
}


static int remoteHandshake(int sock_fd, char *dest_ip, int dest_port)
{
	__android_log_print(ANDROID_LOG_INFO, TAG, "Handshake port, dst addr %s, dst port %d\n", dest_ip, dest_port);
	struct sockaddr_in  addr;
	struct in_addr      destAddr;
	/* Connect to destination address */
	if(inet_aton(dest_ip, &destAddr) == 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Address %s is invalid\n", dest_ip);
		return -1;
	}

	addr.sin_family =  AF_INET;
	addr.sin_addr = destAddr;
	addr.sin_port = htons(dest_port);
	if(connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Error when connecting socket %d\n", sock_fd);
		return -1;
	}

	int ret = -1;
	int handshakeRecv = -1;
	char                sendBuf[HANDSHAKE_BUF_SIZE];
	char                recvBuf[MAX_HANDSHAKE_BUF_SIZE];
	int tmpNum = -1;
	int size = -1;
	timeval pTime0, pTime1;
	double time0 = -1;
	double time1 = -1;
	gettimeofday(&pTime0, NULL);
	time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
	while (ret == -1 && (time1 - time0 < MAX_HANDSHAKE_TIME) && isRunning)
	{
		gettimeofday(&pTime1, NULL);
		time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
		memset(sendBuf, 0, sizeof(sendBuf));
		memset(recvBuf, 0, sizeof(recvBuf));
		sprintf(sendBuf, "%s", "HIHI");
		__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info to fd %d\n", sock_fd);
		size = send(sock_fd, sendBuf, sizeof(sendBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d, ret %d\n",
				sendBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			//__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info failed");
		}
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info from fd %d\n", sock_fd);
		size = recv(sock_fd, recvBuf, sizeof(recvBuf), 0);
		__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info %s from fd %d ret %d\n", recvBuf, sock_fd, size);
		if(size < MIN_HANDSHAKE_BUF_SIZE)
		{
			//__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
		}

		if (recvBuf[0] == 'V' && recvBuf[1] == 'L' &&
				recvBuf[2] == 'V' && recvBuf[3] == 'L')
		{
			//hand shake OK
			ret = 0;
			__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE PORT OK---\n");
			break;
		}

		usleep(100000);
	}

	return ret;
}

void* RUDPClient::sendRelayCmd(ConnectionInfo *connInfo, eMediaSubType cmd_type)
{
	if (connInfo != NULL)
	{
		//RMCEncryptionSet(connInfo->sess, 1, (uint8_t *)connInfo->enc_key);
		//__android_log_print(ANDROID_LOG_INFO, TAG, "%s: random number is %s\n", __FUNCTION__, connInfo->random_number);
		if (cmd_type == eMediaSubTypeCommandAccessStream)
		{
			for (int i=0; i<8; i++)
			{

				RMCRelayRequestSend(connInfo->sess, cmd_type,
						connInfo->mac,
						connInfo->random_number);
				usleep(100000);
			}
		}
		else
		{
			RMCRelayRequestSend(connInfo->sess, cmd_type,
					connInfo->mac,
					connInfo->random_number);
		}
		//RMCEncryptionSet(connInfo->sess, 0, (uint8_t *)connInfo->enc_key);
	}
	else
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "%s: connection info is NULL\n", __FUNCTION__);
	}
}

bool RUDPClient::tryConnectToDestAddress(int sock_fd, char *dest_ip, int dest_port)
{
	bool ret = false;
	struct sockaddr_in  addr;
	struct in_addr      destAddr;
	if (dest_ip != NULL)
	{
		/* Connect to destination address */
		if(inet_aton(dest_ip, &destAddr) == 0)
		{
			__android_log_print(ANDROID_LOG_ERROR, TAG, "(%s:%d) Address %s is invalid\n", __FILE__, __LINE__, dest_ip);
		}
		else
		{
			addr.sin_family =  AF_INET;
			addr.sin_addr = destAddr;
			addr.sin_port = htons(dest_port);
			if(connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, TAG, "(%s:%d) Error when connecting socket %d\n", __FILE__, __LINE__, sock_fd);
			}
			else
			{
				ret = true;
			}
		}
	}
	else
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "(%s:%d) Dest ip %d is invalid\n", __FILE__, __LINE__, dest_ip);
	}

	return ret;
}

int RUDPClient::tryMultiSessionHandshake()
{
	int ret_idx = -1;
	__android_log_print(ANDROID_LOG_INFO, TAG, "Try multi handshake\n");
	struct sockaddr_in  addr;
	struct in_addr      destAddr;
	int i, j;
	for (i=0; i<gConnInfoCount; i++)
	{
		if (isRunning == false)
		{
			break;
		}

		if (gConnInfo[i] != NULL)
		{
			gConnInfo[i]->isConnected = tryConnectToDestAddress(gConnInfo[i]->sock_fd,
					gConnInfo[i]->src_ip, gConnInfo[i]->port);
			__android_log_print(ANDROID_LOG_ERROR, TAG, "Connection info %d is connected? %d\n", i,
					gConnInfo[i]->isConnected?1:0);

		}
		else
		{
			__android_log_print(ANDROID_LOG_ERROR, TAG, "Connection info %d is NULL\n", i);
		}
	}

	int ret = -1;
	int handshakeRecv = -1;
	time_t t;
	srand((unsigned) time(&t));
	int ran_no;
	char                sendBuf[MAX_CONN][MAX_HANDSHAKE_BUF_SIZE];
	char                recvBuf[MAX_CONN][MAX_HANDSHAKE_BUF_SIZE];

	/* Prepare send & receive buffers. */
	for (i = 0; i < gConnInfoCount; i++)
	{
		memset(sendBuf[i], 0, sizeof(sendBuf[i]));
		sprintf(sendBuf[i], "%s", "HIHI");
		memset(recvBuf[i], 0, sizeof(recvBuf[i]));
	}

	int tmpNum = -1;
	int size = -1;
	timeval pTime0, pTime1;
	double time0 = -1;
	double time1 = -1;
	gettimeofday(&pTime0, NULL);
	time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
	while (ret_idx == -1 && (time1 - time0 < MAX_HANDSHAKE_TIME) && isRunning)
	{
		gettimeofday(&pTime1, NULL);
		time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
		if (isRunning == true)
		{
			for (i=0; i<gConnInfoCount; i++)
			{
				if (gConnInfo[i]->isConnected == true)
				{
					__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info to fd %d\n", gConnInfo[i]->sock_fd);
					for (j=0; j<10; j++)
					{
						ran_no = rand() % (MAX_HANDSHAKE_BUF_SIZE - MIN_HANDSHAKE_BUF_SIZE);
						size = send(gConnInfo[i]->sock_fd, sendBuf[i], MIN_HANDSHAKE_BUF_SIZE + ran_no, 0);
						__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info %s from fd %d, ret %d\n",
								sendBuf[i], gConnInfo[i]->sock_fd, size);
						if(size < MIN_HANDSHAKE_BUF_SIZE)
						{
							//__android_log_print(ANDROID_LOG_INFO, TAG, "Sending handshake info failed");
						}
					}
				}
			} //end for loop send
		}

		if (isRunning == true)
		{
			for (i=0; i<gConnInfoCount; i++)
			{
				if (gConnInfo[i]->isConnected == true)
				{
					__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info from fd %d\n", gConnInfo[i]->sock_fd);
					size = recv(gConnInfo[i]->sock_fd, recvBuf[i], sizeof(recvBuf[i]), 0);
					__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info %s from fd %d ret %d\n", recvBuf[i], gConnInfo[i]->sock_fd, size);
					if(size < MIN_HANDSHAKE_BUF_SIZE)
					{
						//__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
					}

					if (recvBuf[i][0] == 'V' && recvBuf[i][1] == 'L' &&
							recvBuf[i][2] == 'V' && recvBuf[i][3] == 'L')
					{
						//hand shake OK
						ret_idx = i;
						__android_log_print(ANDROID_LOG_INFO, TAG, "---HANDSHAKE PORT %d OK---\n", gConnInfo[i]->port);
						break;
					}
				}
			} //end for loop receive
		}
	} //end while loop max handshake timeout

	return ret_idx;
}

void RUDPClient::setConnInfo(ConnectionInfo **connInfos)
{
	for (int i=0; i<gConnInfoCount; i++)
	{
		copyConnInfo(gConnInfo[i], connInfos[i]);
	}
}

void RUDPClient::copyConnInfo(ConnectionInfo *dst, ConnectionInfo *src)
{
	dst->src_ip = (char *) malloc(strlen(src->src_ip) + 1);
	strcpy(dst->src_ip, src->src_ip);
	dst->p2p_mode = src->p2p_mode;
	dst->port = src->port;
	dst->sock_fd = src->sock_fd;
	dst->event_listener = src->event_listener;
	dst->random_number = (char *) malloc(strlen(src->random_number) + 1);
	strcpy(dst->random_number, src->random_number);
	dst->enc_key = (char *) malloc(strlen(src->enc_key) + 1);
	strcpy(dst->enc_key, src->enc_key);
	dst->mac = (char *) malloc(strlen(src->mac) + 1);
	strcpy(dst->mac, src->mac);
}

//void* RUDPClient::command_worker_thread(void *ptr)
//{
//	int cmd_idx = getSessionIndex();
//	const char *request = "dummy";
//	int i;
//	tCmdResponse *res = NULL;
//	tCmdResponseBuffer req;
//	req.data = (void *)request;
//	req.size = strlen(request);
//	int commandID = 0;
//	for (i=0; i<10; i++)
//	{
//		__android_log_print(ANDROID_LOG_INFO, TAG, "Dummy send cmd request: %s\n", request);
//		commandID = OPT2_MediaRxCommandSend(
//				gConnInfo[cmd_idx]->sess, &req);
//	}
//}

void* RUDPClient::video_timer_task(void *ptr)
{
#if ANDROID_BUILD
	if(attachVmToThread () != JNI_OK)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "Attach VM to thread video timer failed");
		return NULL;
	}
#endif
	timeval pTime;
	double time_now = -1;
	bool timeout = false;

	if (sDecoderAudio1 != NULL)
	{
		sDecoderAudio1->last_updated = -1;
		sDecoderVideo1->last_updated = -1;
	}

	while (isRunning == true)
	{
		gettimeofday(&pTime, NULL);
		time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
		if (start_time != -1 && (time_now - start_time > MAX_CONN_TIMEOUT))
		{
			__android_log_print(ANDROID_LOG_ERROR, TAG, "MediaSession received timeout.\n");
			timeout = true;
		}

		if (sDecoderVideo1->checkStreamAlive() == false ||
				sDecoderAudio1->checkStreamAlive() == false)
		{
			timeout = true;
		}

		if (timeout == true)
		{
			int media_idx = getSessionIndex();
			if (media_idx != -1 && gConnInfo != NULL && gConnInfo[media_idx] != NULL && gConnInfo[media_idx]->event_listener != NULL)
			{
			  // Suspend media player as soon as possible to avoid wrong state
			  __android_log_print(ANDROID_LOG_INFO, TAG, "Interrupt media player due to timeout");
			  isRunning = false;
			  gConnInfo[media_idx]->event_listener->suspend();
				gConnInfo[media_idx]->event_listener->notify(MEDIA_ERROR_SERVER_DIED, 0, 0);
				break;
			}
		}

		/*
		 * Need to split sleep time into smaller sleep
		 */
		int count = 10;
		while (count-- > 0 && isRunning == true)
		{
		  usleep(100000);
		}
	}
#if ANDROID_BUILD
	__android_log_print(ANDROID_LOG_INFO, TAG, "Dettach video timer thread from VM thread...\n");
	dettachVmFromThread();
#endif
}

void* RUDPClient::video_worker_thread(void *ptr)
{
#if ANDROID_BUILD
	if(attachVmToThread () != JNI_OK)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "Attach VM to thread video worker failed");
		return NULL;
	}
#endif
	int i = -1;
	isHandshakeSucceeded = false;

	if (gConnInfo[0]->p2p_mode == P2P_MODE_LOCAL)
	{
		i = 0;
	}
	else if (gConnInfo[0]->p2p_mode == P2P_MODE_REMOTE)
	{
		/* Only handshake in remote. */
		i = tryMultiSessionHandshake();
	}
	else
	{
		i = 0;
	}

	if (i != -1)
	{
		isHandshakeSucceeded = true;
		media_idx = i;
	}

	if (isRunning == true)
	{
		if (isHandshakeSucceeded == true)
		{
			__android_log_print(ANDROID_LOG_INFO, TAG, "video_worker_thread, p2p_mode %d\n",
					gConnInfo[i]->p2p_mode);
			gConnInfo[i]->sess = RMCAllocUseExternalSocket(
					gConnInfo[i]->src_ip, gConnInfo[i]->port,
					gConnInfo[i]->sock_fd);
			if (gConnInfo[i]->sess != NULL)
			{
				RMCEncryptionSet(gConnInfo[i]->sess, 1, (uint8_t *)gConnInfo[i]->enc_key);
				if (gConnInfo[i]->p2p_mode == P2P_MODE_LOCAL)
				{
					RMCRXSideTimeoutSet(gConnInfo[i]->sess, 300, 800);
				}
				else if (gConnInfo[i]->p2p_mode == P2P_MODE_REMOTE)
				{
					RMCRXSideTimeoutSet(gConnInfo[i]->sess, 400, 1000);
				}
				else
				{
					RMCRXSideTimeoutSet(gConnInfo[i]->sess, 400, 1000);
					__android_log_print(ANDROID_LOG_INFO, TAG, "Send relay stream access request...\n");
					sendRelayCmd(gConnInfo[i], eMediaSubTypeCommandAccessStream);
				}
			}
			else
			{
				__android_log_print(ANDROID_LOG_INFO, TAG, "gConnInfo[i]->sess is NULL\n");
			}
		}
	}

	if (isRunning == true)
	{
		if (isHandshakeSucceeded == true)
		{
			timeval pTime;
			gettimeofday(&pTime, NULL);
			start_time = pTime.tv_sec*1000000.0 + pTime.tv_usec;
			__android_log_print(ANDROID_LOG_INFO, TAG, "Start receiving video from p2p channel idx %d, port %d\n", i, gConnInfo[i]->port);
			if (gConnInfo[i] != NULL && gConnInfo[i]->event_listener != NULL)
			{
				gConnInfo[i]->event_listener->notify(MEDIA_INFO_P2P_SESSION_INDEX, i, 0);
			}

			double curr_time = (double) av_gettime() / 1000000.0;
			if (sDecoderVideo1 != NULL)
			{
				sDecoderVideo1->last_updated = curr_time;
			}
			if (sDecoderAudio1 != NULL)
			{
				sDecoderAudio1->last_updated = curr_time;
			}

			RMCRXSideRecv(gConnInfo[i]->sess, decodeVideoTask);
		}
		else
		{
			if (gConnInfo[0] != NULL && gConnInfo[0]->event_listener != NULL)
			{
				gConnInfo[0]->event_listener->notify(MEDIA_INFO_HANDSHAKE_FAILED, 0, 0);
			}
		}
	}
	else
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient is stopped, dont start media session\n");
	}

#if ANDROID_BUILD
	__android_log_print(ANDROID_LOG_INFO, TAG, "Dettach thread video worker from VM thread...\n");
	dettachVmFromThread();
#endif
}

/**We could open muliple session to handshake simulteniously,
 * so we get current session index here.
 */
int RUDPClient::getSessionIndex()
{
	return media_idx;
}

void RUDPClient::handShake(ConnectionInfo *handShakeInfo)
{
	int ret = -1;
	if (handShakeInfo != NULL)
	{
//		ret = remoteHandshakeAckPort(handShakeInfo->sock_fd,
//				handShakeInfo->src_ip, handShakeInfo->port);
		ret = remoteHandshake(handShakeInfo->sock_fd,
						handShakeInfo->src_ip, handShakeInfo->port);
		if (ret == -1)
		{
			__android_log_print(ANDROID_LOG_ERROR, TAG, "Handshake task, port %d failed\n", handShakeInfo->port);
		}

	}

	if (handShakeInfo != NULL && handShakeInfo->event_listener != NULL)
	{
		handShakeInfo->event_listener->notify(MEDIA_INFO_HANDSHAKE_RESULT, ret, -1);
	}
	else
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Media event listener is NULL, dont notify\n");
	}
}

void RUDPClient::sendCommand(const char *request, char **response)
{
  __android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, send command: %s\n", request);
  if (isRunning == false)
  {
    *response = NULL;
    __android_log_print(ANDROID_LOG_INFO, TAG, "send command err, RUDPClient has stopped -> exit now\n");
  }
  else
  {
    pthread_mutex_lock(&send_cmd_lock);
    tCmdResponse *res = NULL;

    if (isRunning == false)
    {
      *response = NULL;
      __android_log_print(ANDROID_LOG_INFO, TAG, "send command err, RUDPClient has stopped\n");
    }
    else
    {
      int cmd_idx = getSessionIndex();
      if (cmd_idx == -1 || gConnInfo[cmd_idx]->sess == NULL)
      {
        *response = NULL;
        __android_log_print(ANDROID_LOG_INFO, TAG, "send command err, MediaSession is NULL\n");
      }
      else
      {
        tCmdResponseBuffer req;
        req.data = (void *)request;
        req.size = strlen(request);
        int commandID = 0;
        commandID = RMCRxSideCmdReqSend(gConnInfo[cmd_idx]->sess, &req);
        if(commandID >= 0)
        {
          timeval pTime;
          double time_now = -1;
          gettimeofday(&pTime, NULL);
          time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
          // Default timeout for remote p2p command is 10s,
          // Local command will be sent via http, so don't care here
          double expired_time = time_now + MAX_COMMAND_TIMEOUT;
          bool isSucceeded = false;
          while(time_now < expired_time && isRunning == true)
          {
            res = RMCRxSideCmdResGet(gConnInfo[cmd_idx]->sess, commandID);
            if(res != NULL && res->status == eMediaErrorOK)
            {
              isSucceeded = true;
              break;
            }
            else
            {
              usleep(20000);
              // Update time now
              gettimeofday(&pTime, NULL);
              time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
            }
          }

          if (isSucceeded == true)
          {
            __android_log_print(ANDROID_LOG_INFO, TAG, "Response: %s, length %d\n", (char*)res->CmdResponse.data, res->CmdResponse.size);
            if (res->CmdResponse.size > 0) {
              *response = (char *) malloc(res->CmdResponse.size + 1);
              memset(*response, 0, res->CmdResponse.size + 1);
              strncpy(*response, (char*)res->CmdResponse.data, res->CmdResponse.size);
            } else {
              __android_log_print(ANDROID_LOG_INFO, TAG, "Send p2p command failed, response is empty\n");
              *response = NULL;
            }
          }
          else
          {
            __android_log_print(ANDROID_LOG_INFO, TAG, "Send p2p command failed timeout\n");
            *response = NULL;
          }
        } else {
          __android_log_print(ANDROID_LOG_INFO, TAG, "Send p2p command failed, command id %d\n", commandID);
          *response = NULL;
        }
      } //else cmd_idx != -1
    } //else isRunning == true
    pthread_mutex_unlock(&send_cmd_lock);
  }
}

void RUDPClient::sendTalkbackData(uint8_t *talkbackData, int offset, int length)
{
	static uint32_t ts = 0;
//	__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, send talkback data, offset %d, length %d\n", offset, length);
//	int i;
//	for (i=offset; i<offset+8; i++)
//	{
//		__android_log_print(ANDROID_LOG_INFO, TAG, "%02x", talkbackData[i]);
//	}

	int out_size;
	g722_enc_encode(&g722_enc, (short *)talkbackData, length/2, out_buf, &out_size);
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE //  write raw to file
	size_t dec_sample;
	g722_dec_decode(&g722_dec, out_buf, out_size, dec_buf, &dec_sample);
    FILE * pFile;
    //Simulator
    const char * file_path = "/storage/emulated/0/debug_talkback_data.raw";
    //audio_adpcm_after_encode.raw
    pFile = fopen(file_path, "ab");
    fwrite(dec_buf, sizeof(int16_t), dec_sample, pFile);
    fclose(pFile);
#endif

	int tb_idx = getSessionIndex();
	if (tb_idx == -1 || gConnInfo == NULL || gConnInfo[tb_idx]->sess == NULL)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "send talkback err, MediaTransferSession is NULL\n");
	}
	else
	{
		if (isRunning == true)
		{
//			if(RMCRxSideAudioSend(gConnInfo[tb_idx]->sess, talkbackData, length, ts++) < 0)
			if(RMCRxSideAudioSend(gConnInfo[tb_idx]->sess, out_buf, out_size, ts++) < 0)
			{
				__android_log_print(ANDROID_LOG_ERROR, TAG, "(%s:%d) Send talk back data failed", __FILE__, __LINE__);
			}
			else
			{
				__android_log_print(ANDROID_LOG_INFO, TAG, "(%s:%d) Send talk back data size %d", __FILE__, __LINE__, out_size);
			}
		}
		else
		{
			__android_log_print(ANDROID_LOG_INFO, TAG, "mRUDPClient has stopped, dont send talkback data");
		}
	}
}

void RUDPClient::setConnInfoDefaultParams(ConnectionInfo *connInfo)
{
	connInfo->src_ip = NULL;
	connInfo->random_number = NULL;
	connInfo->enc_key = NULL;
	connInfo->mac = NULL;
	connInfo->p2p_mode = P2P_MODE_RELAY;
	connInfo->sess = NULL;
	connInfo->sock_fd = -1;
	connInfo->port = -1;
	connInfo->event_listener = NULL;
	connInfo->isConnected = false;
}

void RUDPClient::setConnInfoCount(int connInfoCount)
{
	gConnInfoCount = connInfoCount;
	if (gConnInfoCount > 0)
	{
		gConnInfo = (ConnectionInfo **) malloc(gConnInfoCount * sizeof(ConnectionInfo *));
		for (int i=0; i<gConnInfoCount; i++)
		{
			gConnInfo[i] = (ConnectionInfo *) malloc(sizeof(ConnectionInfo));
			if (gConnInfo[i] != NULL)
			{
				setConnInfoDefaultParams(gConnInfo[i]);
			}
			else
			{
				__android_log_print(ANDROID_LOG_ERROR, TAG, "Alloc gConnInfo failed\n");
			}
		}
	}
}

RUDPClient::RUDPClient()
{
	sRecorderVideo = NULL;
	sRecorderAudio = NULL;
	data_port = -1;
	data_fd = -1;
	ack_port = -1;
	ack_fd = -1;
	sDecoderAudio1 = NULL;
	sDecoderVideo1 = NULL;
	currentVideoBitrate = 0;
	currentDataReceived = 0;
	mVideoThread = NULL;
	isRunning = false;
	isHandshakeSucceeded = false;
	media_idx = -1;
	isRecording = false;
	start_time = -1;
	pthread_mutex_init(&send_cmd_lock, NULL);
	g722_enc_init(&g722_enc);
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE
	g722_dec_init(&g722_dec);
#endif
	gConnInfoCount = 0;
	gConnInfo = NULL;

}

RUDPClient::~RUDPClient()
{
	sDecoderVideo1 = NULL;
	sDecoderAudio1 = NULL;

	if (gConnInfoCount > 0)
	{
		for (int i=0; i<gConnInfoCount; i++)
		{
			freeConnInfo(gConnInfo[i]);
			gConnInfo[i] = NULL;
		}
		free(gConnInfo);
		gConnInfo = NULL;
	}

	isRecording = false;
	isRunning = false;
	isHandshakeSucceeded = false;
	pthread_mutex_destroy(&send_cmd_lock);
	g722_enc_deinit(&g722_enc);
#if DEBUG_WRITE_TALKBACK_DATA_TO_FILE
	g722_dec_deinit(&g722_dec);
#endif
}

void RUDPClient::startRecording()
{
	isRecording = true;
}

void RUDPClient::stopRecording()
{
	isRecording = false;
}

void RUDPClient::freeConnInfo(ConnectionInfo *connInfo)
{
	if (connInfo->src_ip != NULL)
	{
		free(connInfo->src_ip);
		connInfo->src_ip = NULL;
	}
	free(connInfo);
	connInfo = NULL;
}

void RUDPClient::init()
{
	__android_log_print(ANDROID_LOG_INFO, TAG, "init RUDPClient");
	int i;
	for (i=0; i<gConnInfoCount; i++)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG,
			"init session %d: src_ip %s, port %d, sock_fd %d, p2p_mode %d\n",
			i, gConnInfo[i]->src_ip, gConnInfo[i]->port, gConnInfo[i]->sock_fd,
			gConnInfo[i]->p2p_mode);
	}
}

/*Must call setConnInfoCount & setConnInfo before start RUDPClient
 */
void RUDPClient::start()
{
	//mylog_info("Listening at port %d\n", sess->endpoint->local_port);
	isRunning = true;
	//__android_log_print(ANDROID_LOG_INFO, TAG, "start RUDPClient\n");
	int status;
	__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, start media receiver thread\n");
	status = pthread_create(&mVideoThread, NULL, video_worker_thread, NULL);
	if (status < 0)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, start media receiver thread failed, ret %d\n", status);
	}

	__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, start video timer thread\n");
	status = pthread_create(&mVideoTimerThread, NULL, video_timer_task, NULL);
	if (status < 0)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, start video timer thread failed, ret %d\n", status);
	}
}

void RUDPClient::stop()
{
	isRecording = false;
	isHandshakeSucceeded = false;
	isRunning = false;
	__android_log_print(ANDROID_LOG_INFO, TAG, "stop RUDPClient\n");
	int i;
	for (i=0; i<gConnInfoCount; i++)
	{
		if (gConnInfo[i]->sess != NULL)
		{
			if (gConnInfo[i]->p2p_mode == P2P_MODE_RELAY)
			{
				__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, send close relay session cmd\n");
				sendRelayCmd(gConnInfo[i], eMediaSubTypeCommandCloseStream);
			}

			__android_log_print(ANDROID_LOG_INFO, TAG, "RUDPClient, cleanup rudp session\n");
			RMCCleanup(gConnInfo[i]->sess);
			gConnInfo[i]->sess = NULL;
		}
	}

	if (mVideoThread != NULL)
	{
		__android_log_print(ANDROID_LOG_INFO, TAG, "Waiting rudp thread stopped...\n");
		if (pthread_join(mVideoThread, NULL) != 0)
		{
			__android_log_print(ANDROID_LOG_ERROR, TAG, "Cannot stop media recv thread\n");
		}
	}

	__android_log_print(ANDROID_LOG_INFO, TAG, "Waiting video timer thread stopped...\n");
	if (pthread_join(mVideoTimerThread, NULL) != 0)
	{
		__android_log_print(ANDROID_LOG_ERROR, TAG, "Cannot stop video timer thread\n");
	}
}

bool RUDPClient::isP2pHandshakeSucceeded()
{
	return isHandshakeSucceeded;
}

