/*******************************************************************************
 * Copyright (C) 2016 Cinatic Technology
 *  
 * This unpublished material is proprietary to Cinatic Technology.
 * All rights reserved. The methods and
 * techniques described herein are considered trade secrets
 * and/or confidential. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of Cinatic Technology.
 *******************************************************************************/
#ifndef __AES_SW_H__
#define __AES_SW_H__

#include <stdint.h>

struct aes_context
{
    int nr;
    uint32_t erk[64];
    uint32_t drk[64];
	char mode[16];
};

int  aes_set_key( struct aes_context *ctx, uint8_t *key, int nbits, const char *mode);
void aes_encrypt( struct aes_context *ctx, uint8_t iv[16], uint8_t* data, uint32_t length);
void aes_decrypt( struct aes_context *ctx, uint8_t iv[16], uint8_t* data, uint32_t length);


#endif  // end of __AES_SW_H__
