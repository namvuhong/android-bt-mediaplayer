/* mediaplayer.cpp
**
*/

#define TAG "FFMpegPacketQueue"

#ifdef ANDROID_BUILD
#include <android/log.h>
#include "mylog.h"
#endif
#include "packetqueue.h"

PacketQueue::PacketQueue()
{
	pthread_mutex_init(&mLock, NULL);
	pthread_cond_init(&mCondition, NULL);
	mFirst = NULL;
	mLast = NULL;
	mNbPackets = 0;
    mSize = 0;
    mAbortRequest = false;
}

PacketQueue::~PacketQueue()
{
	flush();
	pthread_mutex_destroy(&mLock);
	pthread_cond_destroy(&mCondition);
}

int PacketQueue::size()
{
	pthread_mutex_lock(&mLock);
    int size = mNbPackets;
    pthread_mutex_unlock(&mLock);
	return size;
}

int PacketQueue::sizeInKb()
{
	pthread_mutex_lock(&mLock);
	int sizeKb = mSize;
	pthread_mutex_unlock(&mLock);
	return sizeKb;
}

void PacketQueue::flush()
{
	AVPacketList *pkt, *pkt1;
    pthread_mutex_lock(&mLock);

    if (mNbPackets > 0)
    {
    	for(pkt = mFirst; pkt != NULL; pkt = pkt1)
    	{
    		pkt1 = pkt->next;
#ifdef USE_FFMPEG_3_0
    		av_packet_unref(&pkt->pkt);
#else
    		av_free_packet(&pkt->pkt);
#endif
    		av_freep(&pkt);
    	}

    	mLast = NULL;
    	mFirst = NULL;
    	mNbPackets = 0;
    	mSize = 0;
    }

    pthread_mutex_unlock(&mLock);
}

double PacketQueue::getCurrentBufferDelay(double time_base)
{
	double current_delay = 0;
	AVPacketList *pkt, *pkt1;
	pthread_mutex_lock(&mLock);
	if (mNbPackets > 0)
	{
		if (mFirst != NULL && mLast != NULL)
		{
			current_delay = (mLast->pkt.pts - mFirst->pkt.pts) * time_base;
		}
	}
	pthread_mutex_unlock(&mLock);
	return current_delay;
}

void PacketQueue::dropToNextKeyFrame()
{
	if (mFirst == NULL)
	{
		log_info("Packet buffer is empty.\n");
		return;
	}

	AVPacketList *pkt;
	pthread_mutex_lock(&mLock);
	int dropped_pkt = 0;
	do
	{
		pkt = mFirst;
		mFirst = pkt->next;
		if (mFirst == NULL)
		{
			mLast = NULL;
		}
		mNbPackets--;
		mSize -= pkt->pkt.size + sizeof(*pkt);
#ifdef USE_FFMPEG_3_0
    	av_packet_unref(&pkt->pkt);
#else
		av_free_packet(&pkt->pkt);
#endif
		av_freep(&pkt);
		dropped_pkt++;
	}
	while (mFirst != NULL && (mAbortRequest == false) && ((mFirst->pkt.flags & AV_PKT_FLAG_KEY) != AV_PKT_FLAG_KEY) );
	log_info("Dropped %d packet from the buffer.\n", dropped_pkt);

	pthread_mutex_unlock(&mLock);
}

int PacketQueue::put(AVPacket* pkt)
{
	AVPacketList *pkt1;

    /* duplicate the packet */
    if (av_dup_packet(pkt) < 0)
        return -1;

    pkt1 = (AVPacketList *) av_malloc(sizeof(AVPacketList));
    if (!pkt1)
        return -1;
    pkt1->pkt = *pkt;
    pkt1->next = NULL;

    pthread_mutex_lock(&mLock);

    if (!mLast) {
        mFirst = pkt1;
	}
    else {
        mLast->next = pkt1;
	}

    mLast = pkt1;
    mNbPackets++;
    mSize += pkt1->pkt.size + sizeof(*pkt1);

	pthread_cond_signal(&mCondition);
    pthread_mutex_unlock(&mLock);

    return 0;

}

/* return < 0 if aborted, 0 if no packet and > 0 if packet.  */
int PacketQueue::get(AVPacket *pkt, bool block)
{
	AVPacketList *pkt1;
    int ret;

    pthread_mutex_lock(&mLock);

    for(;;) {
        if (mAbortRequest) {
            ret = -1;
            break;
        }

        pkt1 = mFirst;
        if (pkt1) {
            mFirst = pkt1->next;
            if (!mFirst)
                mLast = NULL;
            mNbPackets--;
            mSize -= pkt1->pkt.size + sizeof(*pkt1);
            *pkt = pkt1->pkt;
            av_free(pkt1);
            ret = 1;
            break;
        } else if (!block) {
            ret = 0;
            break;
        } else {
			pthread_cond_wait(&mCondition, &mLock);
		}

    }
    pthread_mutex_unlock(&mLock);
    return ret;

}

void PacketQueue::abort()
{
    pthread_mutex_lock(&mLock);
    mAbortRequest = true;
    pthread_cond_signal(&mCondition);
    pthread_mutex_unlock(&mLock);
}
