//
//  file_streamer.h
//  Hubble
//
//  Created by Kronus on 9/8/16.
//  Copyright © 2016 Hubble Connected Ltd. All rights reserved.
//

#ifndef file_streamer_h
#define file_streamer_h
#include "g722_enc.h"

typedef void (*DataOutputCallback) (void* pFrameData, int iFrameSize, int, int);

int FileStreamEventSend(char* fileName, void* audEnc, void* dataBuf, int bufSize, void* pFrameData, int ts_offset, DataOutputCallback opCallback);
int FLVALawPacket2G722(void *pkt, int pktSize, g722_enc_t *audEnc, void *frame, int *frameSize, int *ts);
int FLVideoPacket2H264(void *pkt, int pktSize, void *frame, int *frameSize, int *ts);
int parseComingData(void *buffer, int bufferSize, void *audEnc, void *pFrameData, int *iFrameSize, int *type);

#endif /* file_streamer_h */
