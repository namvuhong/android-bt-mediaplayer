#ifndef FFMPEG_DECODER_VIDEO1_H
#define FFMPEG_DECODER_VIDEO1_H

extern "C" {

//#include "libavfilter/bufferqueue.h"
#include "libavutil/time.h"
#include "libavcodec/error_resilience.h"
//#include "libavcodec/h264.h"
}


#include "decoder.h"
#include "decoder_audio1.h"
#include <signal.h>
typedef void (*VideoDecodingHandler) (AVFrame*,double);

#define	FFMPEG_VIDEO_BUFFER_SIZE 8
#define NUM_OF_IFRAME_AND_ITS_PFRAME 8

//./ffplay.c:75:
#define AV_SYNC_THRESHOLD_MIN 0.01
#define AV_SYNC_THRESHOLD_MAX 0.1
#define AV_NOSYNC_THRESHOLD 10.0


class DecoderVideo1 : public IDecoder
{
public:
//    DecoderVideo1(AVCodecContext* avctx);
	DecoderVideo1(AVStream* stream);
    ~DecoderVideo1();
    DecoderAudio1               *audioDecoder;
    VideoDecodingHandler		onDecode;
    void                        setPlayIFrameOnly(bool);
    void						setSkipUntilNextKeyFrame(bool);
    bool						checkStreamAlive();
    void						dropToNextKeyFrame();
    void            extendTimeout();
    void            setP2pPlayByTimestampEnabled(bool);
    void            setP2pFrameFilteringEnabled(bool);

private:
	AVFrame*					mFrame;
	double						mVideoClock;
    int                        	mShowingIFrame;
    bool						mSkipUntilNextKeyFrame;

    struct FFBufQueue 			frame_queue;
    bool                        prepare();
    double 						synchronize(AVFrame *src_frame, double pts);
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    static int					getBuffer(struct AVCodecContext *c, AVFrame *pic);
    static void				releaseBuffer(struct AVCodecContext *c, AVFrame *pic);

    double                      cal_delay(double pts);
    void						updateClk(double pts);
    void						enqueuePacketToRecorder(AVPacket *packet);

    double frame_last_pts;
    double frame_last_delay;
    double frame_timer;
    int max_timeout; // in seconds
    bool mIsP2pPlayByTimestampEnabled;
    bool mIsP2pFrameFilteringEnabled;
};

#endif //FFMPEG_DECODER_AUDIO_H
