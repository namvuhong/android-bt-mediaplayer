#ifndef FFMPEG_DECODER_AUDIO1_H
#define FFMPEG_DECODER_AUDIO1_H

#include "decoder.h"
#include "recorder_audio.h"
extern "C"
{
#include "mediaplayer/g722_dec.h"
}

#define MAX_AUDIO_SIZE 4*1024

//typedef void (*AudioDecodingHandler) (int16_t*,int);
typedef void (*AudioDecodingHandler) (uint8_t*, int, double);

class DecoderAudio1 : public IDecoder
{
public:
//    DecoderAudio1(AVCodecContext* avctx);
    DecoderAudio1(AVStream* stream);
    ~DecoderAudio1();

    AudioDecodingHandler		onDecode;
    /* need to pass the current Audio buffer size */
    double 						get_audio_clock();
    void						updateClk(double clk);
    bool						checkStreamAlive();
    double						getCurrentBufferDelay(double time_base);
    void            			extendTimeout();

private:
    AVCodec *pcm_codec;
    AVCodecContext *pcm_c;
    struct SwrContext * swr;
    g722_dec_t g722_dec;
    int16_t audio_data_out[MAX_AUDIO_SIZE];

    uint8_t* 					backupData;
    int16_t*                    mSamples;
    int16_t*					mRecordSamples;
    int                         mSamplesSize;
    double                      audio_clock; //maintain the latest pts
    bool                        prepare();
    bool                        decode(void* ptr);
    bool                        process(AVPacket *packet);
    int64_t						startTime;
    double						current_frame_pts;

    int                         encodeToPcm_init();
    int							encodeToAdpcm_init();
    int encode_audio_with_resampling( AVCodecContext *  enc, AVCodecContext * dec,  AVFrame *decoded_frame);
    int resample_audio_for_record(AVCodecContext *  enc, AVCodecContext * dec,  AVFrame *decoded_frame);
    int							synchronize_audio( AVCodecContext * codec_context, uint8_t *samples, int samples_size);

    //static void sighand(int signo);
    int max_timeout; // in seconds

};

#endif //FFMPEG_DECODER_AUDIO_H
