LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

USE_FFMPEG_3_0 := true
LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS -DANDROID_BUILD
LOCAL_CPPFLAGS := -DANDROID_BUILD=1 -D__STDC_CONSTANT_MACROS
ifeq ($(USE_FFMPEG_3_0),true)
LOCAL_CFLAGS += -DUSE_FFMPEG_3_0
LOCAL_CPPFLAGS += -DUSE_FFMPEG_3_0=1
endif

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include \
    $(LOCAL_PATH)/../include/mediaplayer \
    $(LOCAL_PATH)/../include/rmc_inc

ifeq ($(USE_FFMPEG_3_0),true)
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../ffmpeg-3.0.2
else
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../ffmpeg
endif

LOCAL_SRC_FILES += \
    packetqueue.cpp \
    output.cpp \
    mediaplayer.cpp \
    decoder.cpp \
    decoder_audio.cpp \
    decoder_audio1.cpp \
    decoder_video.cpp \
    decoder_video1.cpp \
    thread.cpp \
	recorder_audio.cpp \
	recorder_video.cpp \
	rmc_client.cpp \
	encryption.cpp \
    aes_sw.c \
	flv.c \
	types.c \
	file_streamer.c \
	audio_conv.c \
	../zjni/onLoad.cpp  #some JNI utils 

#LOCAL_LDLIBS := -L$(LOCAL_PATH)/../ffmpeg/android/armeabi/lib \
#				-lavcodec  -lswscale -lavutil  -lavformat -lpostproc \
#				-lz -lm -llog 
LOCAL_LDLIBS := -lz -lm -llog  -ljnigraphics -landroid
ifeq ($(USE_FFMPEG_3_0),true)
LOCAL_LDLIBS += \
	-L$(LOCAL_PATH)/../ffmpeg-3.0.2/android/armeabi \
	-L$(LOCAL_PATH)/../ffmpeg-3.0.2/android/x86
else
LOCAL_LDLIBS += \ 
	-L$(LOCAL_PATH)/../ffmpeg/android/armeabi \
	-L$(LOCAL_PATH)/../ffmpeg/android/x86
endif
#-lffmpeg 

LOCAL_SHARED_LIBRARIES := ffmpeg-prebuilt libyuv librmc librmc_client
#LOCAL_STATIC_LIBRARIES := libavcodec libavformat libavutil libpostproc libswscale
LOCAL_MODULE := libmediaplayer


#include $(BUILD_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)
