#include "recorder_video.h"

#include <unistd.h>

#ifdef ANDROID_BUILD
#include "jniUtils.h"
#include <android/log.h>
#endif


#define TAG "FFMpegVideoRecorder"
#define LOG_TAG TAG

#define LOG_LEVEL 100

#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio


#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
#define __android_log_print(ANDROID_LOG_INFO, TAG, ...) printf(__VA_ARGS__)
//#define LOGI (level, format, ...)  if (level <= LOG_LEVEL) { print(format,__VA_ARGS__);}
#define LOGI print

extern void print( int level, char * format, ...);
#endif

#define DEBUG_RECORDING 0
#define DEBUG_WRITE_TO_FILE 0

RecorderVideo::RecorderVideo(AVStream* stream) : IDecoder(stream)
{
    first_ts_set = false;
    mInterrupted = false;
    isEncrypted = false;
    enc_ctx = NULL;

    mStopRecording = false;
}

RecorderVideo::RecorderVideo(AVCodecContext* avctx) : IDecoder(avctx)
{
    first_ts_set = false;
    mInterrupted = false;
    isEncrypted = false;
    enc_ctx = NULL;

    mStopRecording = false;
}

RecorderVideo::~RecorderVideo()
{
	mStopRecording = true;

	if (mStream->codec)
	{
		avcodec_close(mStream->codec);
	}

	if (enc_ctx != NULL)
	{
		delete enc_ctx;
		enc_ctx = NULL;
	}
}

void RecorderVideo::cleanUp()
{
    first_pts = 0;
    first_dts = 0;
}

void RecorderVideo::set_first_ts(int64_t first_pts, int64_t first_dts) {
	LOGI(10, "VVVRecorder set first packet: pts %lld, dts %lld\n", first_pts, first_dts);
	this->first_pts = first_pts;
	this->first_dts = first_dts;
	first_ts_set = true;
}

bool RecorderVideo::is_first_ts_set() {
	return first_ts_set;
}

void RecorderVideo::setEncryptionEnabled(bool isEnabled)
{
	isEncrypted = isEnabled;
}

bool RecorderVideo::prepare( AVFormatContext *oc, AVStream * in_stream)
{
    src_stream =  in_stream;
    src_codec_context = in_stream->codec; 
    AVCodec * vcodec = NULL;

    mStream = add_stream(oc,&vcodec, AV_CODEC_ID_H264);


    return true;
}

bool RecorderVideo::prepare( AVFormatContext *oc)
{
    src_stream =  NULL;
    src_codec_context = avctx;
    AVCodec * vcodec = NULL;

    mStream = add_stream(oc,&vcodec, AV_CODEC_ID_H264);

    return true;
}

AVStream * RecorderVideo::getVideoStream()
{

    return mStream;

}
AVStream *  RecorderVideo::add_stream(AVFormatContext *oc, AVCodec **codec,
        enum AVCodecID codec_id)
{
    AVCodecContext *c;
    AVStream *st;

    LOGI(10, "add_stream enter 01 \n");


    // We need this to copy stuff over "-vcodec copy"
    if (src_codec_context == NULL)
    {
        return NULL;
    }

    codec_id = src_codec_context->codec_id;

    const AVCodec *codec_1 = (const AVCodec*) src_codec_context->codec;
    st = avformat_new_stream(oc, codec_1);
    if (!st) {
        LOGI(10, "Could not allocate stream\n");
        return NULL;
    }

    st->time_base = (AVRational) {1, 1000};
    st->id = oc->nb_streams-1;
    c = st->codec;

    uint64_t extra_size = (uint64_t)src_codec_context->extradata_size + FF_INPUT_BUFFER_PADDING_SIZE;

    if (extra_size > INT_MAX) {
        exit(1);
    }


    LOGI(10, "before copy settings src timebase: %f\n", av_q2d(src_stream->time_base));
    switch ((codec_1)->type) {
        case AVMEDIA_TYPE_AUDIO:
            c->sample_fmt  = AV_SAMPLE_FMT_S16;
            c->sample_rate = 11025;
            c->channels    = 1;
            c->channel_layout = av_get_default_channel_layout(c->channels);
            c->time_base = (AVRational) {1, c->sample_rate};
            break;
            //Copy everything from src_codec_context
        case AVMEDIA_TYPE_VIDEO:
            c->rc_max_rate =  src_codec_context->rc_max_rate ;
            c->rc_buffer_size = src_codec_context->rc_buffer_size;
            c->field_order = src_codec_context->field_order;
            c->extradata      = (uint8_t *) av_mallocz((int)extra_size);
            if (!c->extradata) {
                return NULL;
            }
            memcpy(c->extradata, src_codec_context->extradata, src_codec_context->extradata_size);

            c->extradata_size= src_codec_context->extradata_size;
            c->bits_per_coded_sample  = src_codec_context->bits_per_coded_sample;

            c->codec_id = codec_id;
            c->bit_rate = src_codec_context->bit_rate;
            c->width    = src_codec_context->width;
            c->height   = src_codec_context->height; 

            c->gop_size      = src_codec_context->gop_size; 
            c->pix_fmt       = src_codec_context->pix_fmt;
            c->time_base = src_stream->codec->time_base;
            break;

        default:
            break;
    }

    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;

    return st;
}

void RecorderVideo::enqueue(AVPacket* packet)
{
	if (mStopRecording == false)
	{
		/* Try decrypt packet before enqueue. */
		if (isEncrypted == true)
		{
			tryDecryptPacket(packet);
		}
#if DEBUG_RECORDING
		LOGI( 10, "Enqueue video packet: pts %lld, src_st_tb %f, src_codec_tb %f, pts_double %f, dst_tb %f, dst_codec_tb %f",
				packet->pts, av_q2d(src_stream->time_base), av_q2d(src_stream->codec->time_base), packet->pts*av_q2d(src_stream->time_base),
				av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base));
#endif
		mQueue->put(packet);
	}
}

void RecorderVideo::write_video_frame(AVFormatContext *oc)
{
    AVPacket pPacket;
    int size = 0;
    int ret = 0;
    int got_frame;
    AVFrame * mFrame = NULL;

    //Dequeue
    if (mQueue->size() <= 0 || mQueue->get(&pPacket,true)<0)
    {
        //failed to dequeue sleep for 500ms to wait for packet to queue up 
        usleep(500000); 
        return;
    }

    if (first_ts_set == false)
    {
    	LOGI( 10, "First video packet: pts %lld, dts %lld", pPacket.pts, pPacket.dts);
    	first_ts_set = true;
    	first_pts = pPacket.pts > 0 ? pPacket.pts : 0;
    	first_dts = pPacket.dts > 0 ? pPacket.dts : 0;

        if ( !(pPacket.flags & AV_PKT_FLAG_KEY))
        {
            //This is not a key packet, forget it
            LOGI( 10, "not key , not key");
#ifdef USE_FFMPEG_3_0
            av_packet_unref(&pPacket);
#else
            // Free the packet that was allocated by av_read_frame
            av_free_packet(&pPacket);
#endif
            return;
        }
        else
        {
            LOGI( 10, "Found key packet, first pts: %lld", pPacket.pts);
        }
    }

#if DEBUG_RECORDING
    LOGI(10, "Video pkt pts %lld, first pts %lld", pPacket.pts, first_pts);
#endif
    if (pPacket.pts < first_pts) {
    	LOGI(10, "First video pts %lld -> skip this pkt %lld", first_pts, pPacket.pts);
    } else {

    	AVCodecContext  * c;
    	c = mStream->codec;

    	pPacket.pts -= first_pts;
    	pPacket.dts -= first_dts;
    	pPacket.stream_index = mStream->index;
    	av_packet_rescale_ts(&pPacket, src_stream->time_base, mStream->time_base);

#if DEBUG_RECORDING
    	LOGI(10, "Write pkt VVVVV: pts %lld, dts %lld, pts_double %f, st_tb %f, codec_tb %f, size: %d, isKey %d",
    			pPacket.pts, pPacket.dts, pPacket.pts * av_q2d(mStream->time_base), av_q2d(mStream->time_base),
				av_q2d(mStream->codec->time_base), pPacket.size, pPacket.flags & AV_PKT_FLAG_KEY);
#endif

    	double currPktPts = pPacket.pts*av_q2d(mStream->time_base);
    	/* Write the compressed frame to the media file. */
    	ret = av_interleaved_write_frame(oc, &pPacket);
    	//ret = av_write_frame(oc, &pPacket);
    	if (ret != 0) {
    		LOGI(10, "Error while writing video packet: %f, error: %s\n", currPktPts, av_err2str(ret));
    	} else {
//    		LOGI(10, "Writing  video packet success: %f\n", currPktPts);
    	}
    	c->frame_number++;
    }

    //LOGI(10, "write video frame done %d!\n", c->frame_number);
#ifdef USE_FFMPEG_3_0
    av_packet_unref(&pPacket);
#else
    // Free the packet that was allocated by av_read_frame
    av_free_packet(&pPacket);
#endif

    return;
}

void RecorderVideo::tryDecryptPacket(AVPacket* packet)
{
	if (enc_ctx == NULL)
	{
		return;
	}

	uint8_t *buf = packet->data;
	int buf_size = packet->size;
	int buf_index = 0;
	bool found_init_seq = false;

	// Copy from h264.c
	/* start code prefix search */
	for (; buf_index + 3 < buf_size; buf_index++)
	{
		// This should always succeed in the first iteration.
		if (buf[buf_index] == 0 &&
				buf[buf_index + 1] == 0 &&
				buf[buf_index + 2] == 1)
		{
			found_init_seq = true;
			break;
		}
	}

	if (found_init_seq == true) // RTSP Stream
	{
		buf_index += 3;
		//this will try to decrypt the whole buffer starting from buf_index -3
		enc_ctx->try_decrypt2(buf + ( buf_index - 4 ),  buf_size - buf_index);
	}
	else  // FLV stream
	{
		buf_index = 4;
		if (buf_size - buf_index > 16)
		{
			enc_ctx->try_decrypt3(buf + buf_index, buf_size - buf_index);
		}
	}
}

void RecorderVideo::setEncryptionKey(const char* val)
{
	if (isEncrypted == true)
	{
		LOGI(10, "Set key to recorder %s.\n", val);

		enc_ctx = new EncryptionContext();
		enc_ctx->prepare(val, val);
	}
	else
	{
		LOGI(10, "Encryption is not enable, do nothing.\n");
	}
}

void RecorderVideo::stopRecording()
{
    mStopRecording = true;
    return ;
}

bool    RecorderVideo::hasUnencodedFrames()
{
    return (mQueue->size() > 0);
}
