#include "decoder_audio1.h"

#include <unistd.h>

#ifdef ANDROID_BUILD
#include <android/log.h>
#include "jniUtils.h"
#endif
#include "output.h"


#define TAG "FFMpegAudioDecoder"

#ifdef  AV_NOSYNC_THRESHOLD
#undef  AV_NOSYNC_THRESHOLD
#endif

#define  AV_NOSYNC_THRESHOLD 3.0 //10.0
#define SAMPLE_CORRECTION_PERCENT_MAX 10
#define AUDIO_DIFF_AVG_NB 20


#define DEBUG_AUDIO_SYNC 0
#define DEBUG_CHECK_AUDIO_STREAM_ALIVE 0
#define DEBUG_AUDIO_CLK 0
#define DEBUG_WRITE_TO_FILE 0
#define DEBUG_EXTEND_TIMEOUT 0

#define MAX_AUDIO_DECODER_TIMEOUT 7
#define EXTEND_TIMEOUT 4

#ifdef ANDROID_BUILD

#ifdef LOGGING_ENABLED
#define LOGI(level, ...) if (level <= LOG_LEVEL) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#else
#define LOGI(level, ...) do {} while(0)
#endif

#else
extern void print( int level, char * format, ...);

#define log_info(...) printf(__VA_ARGS__)
#define LOGI print
#endif


//DecoderAudio1::DecoderAudio1(AVCodecContext* avctx) : IDecoder(avctx)
//{
//	startTime = av_gettime();
//	g722_dec_init(&g722_dec);
//	max_timeout = MAX_AUDIO_DECODER_TIMEOUT;
//	mRecorder = NULL;
//}

DecoderAudio1::DecoderAudio1(AVStream* stream) : IDecoder(stream)
{
	startTime = av_gettime();
	g722_dec_init(&g722_dec);
	max_timeout = MAX_AUDIO_DECODER_TIMEOUT;
}

DecoderAudio1::~DecoderAudio1()
{
	if (swr != NULL)
	{
		swr_free(&swr);
	}

	if (pcm_c)
	{
		avcodec_close(pcm_c);
		av_free(pcm_c);
	}

	if (mStream) {
		if (mStream->codec)
		{
			avcodec_close(mStream->codec);
		}
	}

    if (avctx != NULL)
	{
		avcodec_close(avctx);
	}

    if (backupData != NULL)
    {
    	av_freep(&backupData);
    }
    g722_dec_deinit(&g722_dec);
    log_info("DecoderAudio1 destroyed...\n");
}

bool DecoderAudio1::prepare()
{
    //mSamplesSize = AVCODEC_MAX_AUDIO_FRAME_SIZE; //AVCODEC_MAX_AUDIO_FRAME_SIZE is deprecated
    mSamplesSize = MAX_AUDIO_SIZE;
    mSamples = (int16_t *) av_malloc(mSamplesSize);
    backupData = (uint8_t*) av_malloc(MAX_AUDIO_SIZE);
    if(mSamples == NULL) {
    	return false;
    }
    audio_clock = -1;
    current_frame_pts = 0;
    //last_updated = (double) av_gettime() / 1000000.0;

    if(encodeToPcm_init() <0)
    {
        return false;
    }

    return true;
}

#include <unistd.h>
bool DecoderAudio1::process(AVPacket *packet)
{
    int size = 0;

    int ret = 0;
    int got_frame;
    AVFrame * mFrame;

#if DEBUG_AUDIO_SYNC
    log_info("Process pkt AAAAA, pts: %lld pts_double: %f, timebase: %f, duration %d, size %d\n",
    		packet->pts, packet->pts*av_q2d(mStream->time_base), av_q2d(mStream->time_base), packet->duration, packet->size);

#endif
    AVCodecContext *codec_ctx = mStream->codec;

#if DEBUG_WRITE_TO_FILE //  write raw to file
    FILE * pFile;
    //Simulator
    const char * file_path = "/storage/emulated/0/audio_at_enqueue.raw";
    //audio_adpcm_after_encode.raw
    pFile = fopen(file_path, "ab");

#endif

    size = packet->size;
    //memcpy(backupData, packet->data, size);

    /* if update, update the audio clock w/pts */
    if( packet->pts != 0)
    {
    	//audio_clock = av_q2d(mStream->time_base)*packet->pts;
    	current_frame_pts = packet->pts * av_q2d(mStream->time_base);
    }
    //1 packet can contain several frames ..

    int64_t total_sleep_time = 0;
//    mFrame = av_frame_alloc();
//    do
//    {
//        ret = avcodec_decode_audio4(avctx, mFrame, &got_frame, packet);
//        long sleep_time = (long) (mFrame->nb_samples / (double) avctx->sample_rate * 1000000);
//        total_sleep_time += sleep_time;
//        encode_audio_with_resampling(pcm_c, avctx,  mFrame);
//
//        packet->data += ret;
//        packet->size -= ret;
//    }
//    while (packet->size >0  || !packet->data);
//
//    //Reset the point so that av_free .. can be called
//    packet->data -= size;
//    packet->size = size;
//
//    av_frame_free(&mFrame);

    size_t out_sample;
    g722_dec_decode(&g722_dec, packet->data, packet->size, audio_data_out, &out_sample);
//    log_info("Decode g722: packet->size %d, out_sample %d\n", packet->size, out_sample);
#if DEBUG_WRITE_TO_FILE //  write raw to file
    fwrite(audio_data_out, sizeof(int16_t), out_sample, pFile);
    fclose(pFile);
#endif
    onDecode((uint8_t *)audio_data_out, out_sample*2, current_frame_pts);

#if DEBUG_RECORDING
    log_info("Before rescale packet ts: pts %lld, st_tb %f, codec_tb %f\n",
    		packet->pts, av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base));
#endif

    av_packet_rescale_ts(packet, mStream->time_base, mStream->codec->time_base);

#if DEBUG_RECORDING
    log_info("After rescale packet ts: pts %lld, st_tb %f, codec_tb %f\n",
        			packet->pts, av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base));
#endif

    //Do it for recorder
    AVPacket *packet2 = packet;
    AVFrame * mFrame2 = NULL, *mFrame3 = NULL;

    if (mRecorder != NULL)
    {
    	int frame_size = av_samples_get_buffer_size(NULL,
    			codec_ctx->channels, out_sample,
				codec_ctx->sample_fmt, 0);

    	mFrame2 = av_frame_alloc();
    	mFrame2->sample_rate = codec_ctx->sample_rate;
    	mFrame2->format = codec_ctx->sample_fmt;
    	mFrame2->nb_samples = out_sample;
    	mFrame2->channels = codec_ctx->channels;
    	mFrame2->channel_layout = codec_ctx->channel_layout;
    	int ret = avcodec_fill_audio_frame(mFrame2, codec_ctx->channels,
    			codec_ctx->sample_fmt, (uint8_t *)audio_data_out, frame_size, 0);
    	if (ret < 0)
    	{
    		log_info("Fill data to audio frame failed, ret %d\n", ret);
    	}

    	mFrame2->pts = packet->pts;
    	mFrame2->pkt_dts = packet->dts;
    	mFrame2->pkt_duration = packet->duration;

#if DEBUG_RECORDING
    	log_info("After fill audio frame2: pts %lld, st_tb %f, codec_tb %f, duration %lld, nb_samples %d\n",
    			mFrame2->pts, av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base),
				mFrame2->pkt_duration, mFrame2->nb_samples);
#endif

#if DEBUG_WRITE_TO_FILE //  write raw to file
    	{
    		// View local-share : this is MULAW 8000 / local camera : alaw  8000
    		int size     = mFrame3->nb_samples *
    				avctx->channels *
					av_get_bytes_per_sample(codec_ctx->sample_fmt);
    		fwrite (mFrame3->data[0],sizeof(uint8_t), size,pFile);
    	}
#endif

    	// Need to calculate packet timestamp for mFrame2
    	// Convert stream timebase to codec timebase

    	/*
    	 * IMPORTANT: You have to clone frame here before adding it to recorder.
    	 * Don't add mFrame2 directly. If you do like that, you should receive wrong
    	 * frame when you get it fom ffbufqueue.
    	 */
    	mFrame3 = av_frame_clone(mFrame2);
    	RecorderAudio *recoderAudio = (RecorderAudio *) mRecorder;
    	if (recoderAudio != NULL)
    	{
#if DEBUG_RECORDING
    		log_info("Adding audio frame to recorder: pts %lld, st_tb %f, codec_tb %f, duration %lld, nb_samples %d\n",
    				mFrame3->pts, av_q2d(mStream->time_base), av_q2d(mStream->codec->time_base),
					mFrame3->pkt_duration, mFrame3->nb_samples);
#endif
    		recoderAudio->addFrame(mFrame3);
    	} else {
    		log_error("No audio recorder found in audio decoder\n");
    	}

    }
    return true;
}

double DecoderAudio1::getCurrentBufferDelay(double time_base)
{
	double current_delay = 0;
	if (mQueue != NULL)
	{
		current_delay = mQueue->getCurrentBufferDelay(time_base);
	}
	long curr_buff = Output::AudioDriver_buff_size();
//	log_info("Decoding audio1, curr buff size %ld, curr delay %f\n", curr_buff, current_delay);
	current_delay += curr_buff / 16000.0;

	return current_delay;
}

bool DecoderAudio1::decode(void* ptr)
{
    AVPacket        pPacket;

    log_info(">>>>> decoding audio1 -- Attach VM to thread");


#ifdef  ANDROID_BUILD
    if(attachVmToThread () != JNI_OK)
    {
        log_info("decoding audio1 -- Attach VM to thread Failed");
        mInterrupted = true;
    }
#endif



    while (!mInterrupted)
    {
    	while (!mInterrupted && isPause)
    	{
//    		log_info("audio decoder is sleeping\n");
    		usleep(100);
    	}

        if(mQueue->get(&pPacket, true) < 0)
        {
        	mInterrupted = true;
            continue;
        }

        if(!process(&pPacket))
        {
        	mInterrupted = true;
        }


#ifdef USE_FFMPEG_3_0
		av_packet_unref(&pPacket);
#else
        // Free the packet that was allocated by av_read_frame
		av_free_packet(&pPacket);
#endif

    }


    // Free audio samples buffer
    av_free(mSamples);


    log_info("decoding audio1 ended -- Dettach VM fr Thread");
#ifdef ANDROID_BUILD
    dettachVmFromThread();
#endif

    return true;
}



double DecoderAudio1::get_audio_clock()
{
    double pts;
    int  bytes_per_sec, n;

    long hw_buf_size = Output::AudioDriver_buff_size();
    if (hw_buf_size < 0)
    {
    	hw_buf_size = 0;
    }
    pts = audio_clock; /* maintained in the audio thread */
   // hw_buf_size = audio_buf_size - audio_buf_index;
    bytes_per_sec = 0;
    n = pcm_c->channels * 2;
    bytes_per_sec = pcm_c->sample_rate * n;

    if(bytes_per_sec)
    {
        pts -= (double) hw_buf_size / bytes_per_sec;
    }
#if DEBUG_AUDIO_SYNC
    idecoder_log(NULL, 1, "hw_buf_size: %d\n",hw_buf_size);


    idecoder_log(NULL, 1, "audio_clock: %f\n",audio_clock);

    idecoder_log(NULL, 1, "bytes_per_sec: %d\n",bytes_per_sec);
#endif

    //return pts;
    double time = (double) av_gettime() / 1000000.0;
    return audio_clock + time - last_updated;
}


/* Add or subtract samples to get a better sync, return new
 audio buffer size */
int DecoderAudio1::synchronize_audio( AVCodecContext * codec_context, uint8_t *samples, int samples_size)
{
    int n;
    double ref_clock;

    n = 2 * codec_context->channels;

    if (clock_sync  != AV_SYNC_AUDIO_MASTER)
    {
        double diff, avg_diff;
        int wanted_size, min_size, max_size, nb_samples;

        //sync to video clock
//        if (video_decoder != NULL)
//        {
//        	ref_clock = video_decoder->get_video_clock();
//        }
//        else
//        {
        ref_clock = (av_gettime() -  external_clock_time)/1000000.0; //get_master_clock(is);
//    	}

        //diff = get_audio_clock() - ref_clock;
        diff =  ref_clock - get_audio_clock();

#if DEBUG_AUDIO_SYNC
        idecoder_log(NULL, 1, "synchronize_audio: ref_clock :%f \n",ref_clock );

        idecoder_log(NULL, 1, "synchronize_audio: diff :%f \n",diff );
#endif

#if 1
        if (diff < 1.0)
        {
            //do nothing
        }
        else if (diff > 1.0) //audio is too old
        {
            //idecoder_log(NULL, 1, "flush the rest of audio");
            //samples_size = 0;
//            Output::AudioDriver_flush();
//
//            //flush all packet in queue now
//            int currentQueueSize = mQueue->size();
//            for (int i=0; i<currentQueueSize; i++)
//            {
//            	AVPacket packet;
//            	mQueue->get(&packet, true);
//            	av_free_packet(&packet);
//            }

        }
#else

        if(diff < AV_NOSYNC_THRESHOLD)
        {
            // accumulate the diffs
            audio_diff_cum = diff +  audio_diff_avg_coef* audio_diff_cum;

#if DEBUG_AUDIO_SYNC
            idecoder_log(NULL, 1, "synchronize_audio: audio_diff_cum :%f \n",audio_diff_cum );
            idecoder_log(NULL, 1, "synchronize_audio: audio_diff_avg_coef :%f \n",audio_diff_avg_coef );


#endif
            if(audio_diff_avg_count < AUDIO_DIFF_AVG_NB)
            {
                audio_diff_avg_count++;
            }
            else
            {
                avg_diff = audio_diff_cum * (1.0 - audio_diff_avg_coef);

#if DEBUG_AUDIO_SYNC
                idecoder_log(NULL, 1, "synchronize_audio: avg_diff :%f \n",avg_diff );
                idecoder_log(NULL, 1, "synchronize_audio: audio_diff_threshold :%f \n",audio_diff_threshold );
#endif
                if(fabs(avg_diff) >= audio_diff_threshold)
                {


                    wanted_size = samples_size + ((int)(diff * codec_context->sample_rate) * n);
                    //min_size = samples_size * ((100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100);
                    // max_size = samples_size * ((100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100);
                    min_size = samples_size  - (samples_size *SAMPLE_CORRECTION_PERCENT_MAX) /100;
                   max_size = samples_size  +(samples_size *SAMPLE_CORRECTION_PERCENT_MAX) /100;


#if DEBUG_AUDIO_SYNC

                    idecoder_log(NULL, 1, "synchronize_audio: diffsize :%d \n",((int)(diff * codec_context->sample_rate) * n) );


                    idecoder_log(NULL, 1, "synchronize_audio: samples_size :%d \n",samples_size );
                    idecoder_log(NULL, 1, "synchronize_audio: wanted_size :%d \n",wanted_size );
#endif


                    if(wanted_size < min_size)
                    {
                        wanted_size = min_size;
                    }
                    else if (wanted_size > max_size)
                    {
                        wanted_size = max_size;
                    }


#if DEBUG_AUDIO_SYNC
                    idecoder_log(NULL, 1, "synchronize_audio: 2 wanted_size :%d \n",wanted_size );
#endif

                    if(wanted_size < samples_size)
                    {
                        idecoder_log(NULL, 1, ">>>>>>>>>>>> remove samples  \n");
                        /* remove samples */
                        samples_size = wanted_size;
                    }
                    else if(wanted_size > samples_size)
                    {
                        idecoder_log(NULL, 1, "<<<<<<<<<<<<<< add samples  \n");
                        uint8_t *samples_end, *q;
                        int nb;

                        /* add samples by copying final sample*/
                        nb = (samples_size - wanted_size);
                        samples_end = (uint8_t *)samples + samples_size - n;
                        q = samples_end + n;
                        while(nb > 0)
                        {
                            memcpy(q, samples_end, n);
                            q += n;
                            nb -= n;
                        }
                        samples_size = wanted_size;
                    }
                    else
                    {
                        idecoder_log(NULL, 1, "())()()()()()( not add nor remove samples \n");
                    }

                }
            }
        }
        else
        {
            /* difference is TOO big; reset diff stuff */
            audio_diff_avg_count = 0;
            audio_diff_cum = 0;
        }
#endif
    }

    return samples_size;
}





//Prepare the context & codec for PCM encoding
int DecoderAudio1::encodeToPcm_init()
{
    pcm_codec = NULL;
    pcm_c = NULL;

    /* find the PCM_ encoder */
    pcm_codec = avcodec_find_encoder(AV_CODEC_ID_PCM_S16LE);
    if (!pcm_codec) {
        log_info("Encode to pcm init: cant'find pcm codec ");
        return -1 ;
    }

    pcm_c = avcodec_alloc_context3(pcm_codec);

    /* put sample parameters */
    pcm_c->sample_rate = 8000;
    //pcm_c->channels = avctx->channels;
    pcm_c->channels = 1;
    pcm_c->sample_fmt = AV_SAMPLE_FMT_S16;
    pcm_c->channel_layout = av_get_default_channel_layout(pcm_c->channels);

    /* open it */
    //if (avcodec_open(pcm_c, pcm_codec) < 0) {
    if (avcodec_open2(pcm_c, pcm_codec, NULL) < 0) {
        log_info("Encode error open context ");
        return -2;

    }


    swr = NULL;

    return 0;

}

void DecoderAudio1::updateClk(double clk)
{
#if DEBUG_AUDIO_CLK
	log_info("Audio decoder update clk %f\n", clk);
#endif

	audio_clock = clk;

	last_updated = (double) av_gettime() / 1000000.0;
	if (max_timeout > MAX_AUDIO_DECODER_TIMEOUT) {
#if DEBUG_EXTEND_TIMEOUT
	  log_info("Current audio decoder timeout is extended, reset it now\n");
#endif
	  max_timeout = MAX_AUDIO_DECODER_TIMEOUT;
	}
}

void DecoderAudio1::extendTimeout()
{
  if (max_timeout <= MAX_AUDIO_DECODER_TIMEOUT) {
#if DEBUG_EXTEND_TIMEOUT
    log_info("Extend audio decoder timeout\n");
#endif
    max_timeout += EXTEND_TIMEOUT;
  }
}

bool DecoderAudio1::checkStreamAlive()
{
	bool isAlive = true;
	double now = (double) av_gettime() / 1000000.0;

#if DEBUG_CHECK_AUDIO_STREAM_ALIVE
	log_info("AAAAA time from last updated %f", (now - last_updated));
#endif

	if (last_updated == -1 || now - last_updated < max_timeout)
	{
		isAlive = true;
	}
	else
	{
		log_info("Audio stream is not alive anymore.");
		isAlive = false;
	}

	return isAlive;
}

// SRC: ffmpeg.c ---- do_audio_out()..
int DecoderAudio1::encode_audio_with_resampling( AVCodecContext *  enc, AVCodecContext * dec,  AVFrame *decoded_frame)
{

    uint8_t *buftmp;
    int64_t   size_out;

    int osize = av_get_bytes_per_sample(enc->sample_fmt);
    int isize = av_get_bytes_per_sample(dec->sample_fmt);
    const uint8_t **buf = (const uint8_t **) decoded_frame->extended_data;
    int size     = decoded_frame->nb_samples * dec->channels * isize;


    int audio_resample = 0;
    int audio_sync_method  = 0;

    if (enc->channels != dec->channels
            || enc->sample_fmt != dec->sample_fmt
            || enc->sample_rate!= dec->sample_rate
       )
    {
        audio_resample = 1;
    }

    if ( audio_resample && (swr == NULL))
    {

        //Set some swr
        swr = swr_alloc_set_opts(NULL,
                enc->channel_layout, enc->sample_fmt, enc->sample_rate,
                dec->channel_layout, dec->sample_fmt, dec->sample_rate,
                0, NULL);

        if(swr && swr_init(swr) < 0){
            __android_log_print(ANDROID_LOG_INFO, TAG,
                    "swr_init() failed\n");

            swr_free(&swr);
        }


       if (!swr) {
            log_info("Can not resample %d channels @ %d Hz to %d channels @ %d Hz\n",
                    dec->channels, dec->sample_rate,
                    enc->channels, enc->sample_rate);
            return -1;
        }


    }

    if (audio_resample)
    {
        buftmp = (uint8_t *) mSamples;
        size_out = swr_convert(swr, ( uint8_t*[]){buftmp}, mSamplesSize  / (enc->channels * osize),
                buf, size / (dec->channels * isize));

        if (size_out ==  (mSamplesSize  / (enc->channels * osize)))
        {
            log_info("Warning: audio buff is probably too small");
        }

        size_out = size_out * enc->channels * osize;
    }
    else
    {
        buftmp = (uint8_t *) *buf;
        size_out = size;

    }

    if (forPlayback == false)
    {
    	/* Keep audio_clock up-to-date */
    	int n = 2 * enc->channels;
    	//audio_clock += (double)size_out /(double)(n * enc->sample_rate);

    	/* Sync to external clock */
    	int audio_size;
    	audio_size = synchronize_audio(enc, buftmp, size_out  );
    }
    //size_out = audio_size;

//    __android_log_print(ANDROID_LOG_INFO, TAG,
//            " size:%d  isize:%d osize:%d size_out:%d \n",
//             size, isize, osize, size_out);

    onDecode(buftmp, size_out, current_frame_pts);
    //audio_clock = current_frame_pts;

    return 0;
}

//void DecoderAudio1::setRecorderAudio(RecorderAudio * rec)
//{
//    mRecorder = rec;
//}

//static void DecoderAudio1::sighand(int signo)
//{
//    log_info("Got signal %d", signo);
//
//}


#if 0
/* SRC: ffmpeg.c ---  int encode_audio_frame(....)
    TODO:  clean up
 */

int DecoderAudio1::encodeAmrnbToPcm(uint8_t * buf, int buf_size)
{
    AVCodecContext *enc = pcm_c; //  ost->st->codec;
    AVFrame *frame = NULL;
    AVPacket pkt;
    int ret, got_packet;

    av_init_packet(&pkt);
    pkt.data = NULL;
    pkt.size = 0;

    if (buf) {
        frame = avcodec_alloc_frame();
        if (frame->extended_data != frame->data)
            av_freep(&frame->extended_data);
        avcodec_get_frame_defaults(frame);

        frame->nb_samples  = buf_size /
            (enc->channels * av_get_bytes_per_sample(enc->sample_fmt));

        if ((ret = avcodec_fill_audio_frame(frame, enc->channels, enc->sample_fmt,
                        buf, buf_size, 1)) < 0) {
            log_info("Audio encoding failed\n" );
            return -1;
        }
    }

    got_packet = 0;
    if (avcodec_encode_audio2(enc, &pkt, frame, &got_packet) < 0) {
        log_info("Audio encoding failed\n" );
        return -1;
    }

    ret = pkt.size;

    if (got_packet) {
        //pkt.stream_index = ost->index;
        if (pkt.pts != AV_NOPTS_VALUE)
            pkt.pts      = av_rescale_q(pkt.pts, enc->time_base, mStream->time_base);
        if (pkt.duration > 0)
        {

            pkt.duration = av_rescale_q(pkt.duration, enc->time_base, mStream->time_base);

            log_info("pkt.duration =%d pkt.size=%d\n", pkt.duration, pkt.size );
        }

        //
        //write_frame(s, &pkt, ost);

        onDecode((int16_t*)pkt.data, pkt.size);

        av_free_packet(&pkt);
    }

    return ret;
}
#endif

#if 0
void DecoderAudio1::encodeAdpcmToPcm(int16_t * adpcm_buffer, int len)
{
    int out_size = -1;

    int outbuf_size = len;
    uint8_t *  outbuf = (uint8_t *) malloc(outbuf_size);



    out_size = avcodec_encode_audio(pcm_c, outbuf, outbuf_size, adpcm_buffer);

    log_info("encodeAdpcmToPcm size: %d\n", out_size );
    if (outbuf_size >0)
    {
        onDecode( (int16_t*)outbuf, out_size);
    }


    free(outbuf);
}
#endif

