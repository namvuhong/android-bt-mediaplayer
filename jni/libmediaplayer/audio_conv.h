#ifndef __SOUND_CONV_H__
#define __SOUND_CONV_H__

void sound_alaw_dec(char *pc_alaw_data,
                    char *pc_s16_data,
                    int i32Limit);
#endif // #define __SOUND_CONV_H__
