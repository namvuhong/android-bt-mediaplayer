/* FILE   : streamer.c
 * AUTHOR : leon
 * DATE   : May 19, 2015
 * DESC   :
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/stat.h>
#include <dirent.h>
#include "flv.h"
#include "file_streamer.h"
//#include "session.h"
//#include "relay.h"

#include "g722_enc.h"

#include "audio_conv.h"

//#include "mylogger.h"
typedef enum eFSStat {
    eFSStatIdle,
    eFSStatSessionCreated,
    eFSStatSessionStarted,
} eFSStat;

typedef enum eFSCmd {
    eFSCmdServiceStart,
    eFSCmdEventStart,
    eFSCmdEventStop,
    eFSCmdServiceStop,
} eFSCmd;

typedef struct tH264SeqParam {
    int spsSize;
    int ppsSize;
    char spsData[64];
    char ppsData[64];
} tH264SeqParam;

typedef struct tFileStreamer {
    int total_file;
    int cur_idx;
    char clip_name[8][128];
    char clip_checksum[8][64];
    char mac[16];
    volatile eFSStat stat;
    volatile eFSCmd cmd;
    pthread_mutex_t lock;
    pthread_cond_t cond;
    pthread_t thread;
} tFileStreamer;

static tFileStreamer gFileCtx;
static volatile int gInterruptFlag = 0;
static tH264SeqParam gVidInfo = {0, 0};
static unsigned char gFixSPS[] = {0x00, 0x00, 0x00, 0x01, 0x67, 0x42, 0x00, 0x1F, 0xE9, 0x00, 0xA0, 0x0B, 0x72, 0x01, 0x00, 0x04};
static unsigned char gFixPPS[] = {0x00, 0x00, 0x00, 0x01, 0x68, 0xCE, 0x38, 0x80};

/*
 * PURPOSE : Get time in milliseconds
 * INPUT   : None
 * OUTPUT  : None
 * RETURN  : Time in milliseconds
 * DESCRIPT: None
 */
//static uint32_t
//RealTimeMsGet()
//    {
//    struct timespec tpStart;
//    clock_gettime(CLOCK_MONOTONIC, &(tpStart));
//    return (tpStart.tv_sec * 1000) + (tpStart.tv_nsec / 1000000);
//    }

void
hexdump(char *text, unsigned char *a, int len) {
    int i;
    char buf[1500] = {0};
    int size;

    if (!a)
        return;
    if (len > 1498 / 3)
        size = 1498 / 3;
    else
        size = len;

    for (i = 0; i < size; i++)
        sprintf(buf + strlen(buf), "%02x ", a[i]);
//    mylog_info("%s %s", text, buf);
}

static int
ScanAllEventClip(char *path, char *event, tFileStreamer *res) {
    struct dirent *hFile;
    DIR *dirFile = NULL;
    char oldDestFileName[128] = {0};
    char *pTmp = NULL;
    char *pSuffix = NULL;
    int clipNo = 0;

    if (path == NULL)
        return 0;
    dirFile = opendir(path);
    if (dirFile) {
        while ((hFile = readdir(dirFile)) != NULL) {
            if (strstr(hFile->d_name, event) == NULL)
                continue;
            strcpy(oldDestFileName, hFile->d_name);
            pSuffix = strstr(oldDestFileName, ".flv");
            pTmp = oldDestFileName + strlen(event) + 1;
            if (pTmp == NULL || pSuffix == NULL)
                continue;

            *pSuffix = 0;
            clipNo = atoi(pTmp);
            if (clipNo > 0 && clipNo < 8) {
                strncpy(res->clip_name[clipNo - 1], hFile->d_name, sizeof(res->clip_name[clipNo - 1]) - 1);
                res->total_file++;
            }
        }
        closedir(dirFile);
    }
    return res->total_file;
}

int FLVALawPacket2G722(void *pkt, int pktSize, g722_enc_t *audEnc, void *frame, int *frameSize, int *ts) {
    char *frameOffset = 0;
    int iFrameSize = 0;
    char *buffer = (char *) pkt;
    char *out = (char *) frame;

    if (ts)
        *ts = (buffer[7] << 24 | buffer[4] << 16 | buffer[5] << 8 | buffer[6]) & 0x7FFFFFFF;
    iFrameSize = pktSize - 4 - 11 - 1;
    frameOffset = &(buffer[FLV_TAG_SIZE + 1]);
    sound_alaw_dec(frameOffset, out + iFrameSize + 1, iFrameSize);
    g722_enc_encode(audEnc, (short *) (out + iFrameSize + 1), iFrameSize, out, frameSize);
    return 0;
}

int FLVideoPacket2H264(void *pkt, int pktSize, void *frame, int *frameSize, int *ts) {
    int isHaveSeqParam = 0;
    char *buffer = (char *) pkt;
    char *out = (char *) frame;
    int i, j;
    const int flvHeaderSize = FLV_TAG_SIZE;
    int videoFrameType = 0;
    int iFrameSize = 0;
    char *frameOffset = 0;
    int  iFrameDataOffset = 0;

    isHaveSeqParam =
            buffer[flvHeaderSize + 1] << 24 | buffer[flvHeaderSize + 2] << 16 | buffer[flvHeaderSize + 3] << 8 |
            buffer[flvHeaderSize + 4];
    if (isHaveSeqParam == 0) {
        for (i = 0; i < pktSize - (flvHeaderSize + 4); i++) {
            if (buffer[flvHeaderSize + i] == 0x67)
                break;
        }

        for (j = i; j < pktSize - (flvHeaderSize + 4); j++) {
            if (buffer[flvHeaderSize + j] == 0x68)
                break;
        }
        gVidInfo.spsData[0] = 0x00;
        gVidInfo.spsData[1] = 0x00;
        gVidInfo.spsData[2] = 0x00;
        gVidInfo.spsData[3] = 0x01;
        memcpy(gVidInfo.ppsData, gVidInfo.spsData, 4);
        gVidInfo.spsSize = j - i + 1;
        gVidInfo.ppsSize = pktSize - (flvHeaderSize + j);
        memcpy(gVidInfo.spsData + 4, &(buffer[flvHeaderSize + i]), gVidInfo.spsSize);
        memcpy(gVidInfo.ppsData + 4, &(buffer[flvHeaderSize + j]), gVidInfo.ppsSize);
        return 2;
    }

    videoFrameType = (buffer[flvHeaderSize] >> 4) & 0x01;
    iFrameSize = pktSize - 4 - 11 - 9;
    frameOffset = &(buffer[flvHeaderSize + 9]);

    if (ts)
        *ts = (buffer[7] << 24 | buffer[4] << 16 | buffer[5] << 8 | buffer[6]) & 0x7FFFFFFF;

    if (videoFrameType) {

        /* Hard code SPS if not found */
        if (gVidInfo.spsSize == 0) {
            memcpy(out, gFixSPS, sizeof(gFixSPS));
            iFrameDataOffset = sizeof(gFixSPS);
        }
        else {
            memcpy(out, gVidInfo.spsData, gVidInfo.spsSize);
            iFrameDataOffset = gVidInfo.spsSize;
        }

        /* Hard code PPS if not found */
        if(gVidInfo.ppsSize == 0) {
            memcpy(out + sizeof(gFixSPS), gFixPPS, sizeof(gFixPPS));
            iFrameDataOffset += sizeof(gFixPPS);
        }
        else {
            memcpy(out + gVidInfo.spsSize, gVidInfo.ppsData, gVidInfo.ppsSize);
            iFrameDataOffset += gVidInfo.ppsSize;
        }

        /* Copy frame data out */
        out[iFrameDataOffset + 3] = 0x01;
        out[iFrameDataOffset + 2] = 0x00;
        out[iFrameDataOffset + 1] = 0x00;
        out[iFrameDataOffset] = 0x00;
        memcpy(out + iFrameDataOffset + 4, frameOffset, iFrameSize);
        if (frameSize)
            *frameSize = iFrameSize + iFrameDataOffset + 4;
        return 1;
    }
    else {
        out[3] = 0x01;
        out[2] = 0x00;
        out[1] = 0x00;
        out[0] = 0x00;
        memcpy(out + 4, frameOffset, iFrameSize);
        if (frameSize)
            *frameSize = iFrameSize + 4;
        return 0;
    }
}

//#if 1
//
//int
//FileStreamEventSend(char *fileName, void *audEnc, void *dataBuf, int bufSize, void *pFrameData, int ts_offset,
//                    DataOutputCallback opCallback) {
//    printf("%s\n", __func__);
//
//    int statusCode = -1;
////    flv_header      flvHeader;
//    flv_stream *flvin = NULL;
//    flv_tag tag;
//    int bufferSize = 0;
//    uint32 previousTagSize = 0;
////    int             firstPktTimeStamp = 0;
//    int curPktTimeStamp = 0;
////    struct timespec s_tsReq = {0, 100000000};
//    char *buffer = (char *) dataBuf;
//    char cFullFileName[2048];
//    int videoFrameType = 0;
//    int iFrameSize = 0;
////    uint32          u32CurTs = 0;
////    FILE            *pFile = fopen("/mnt/cache/test_sd_card.h264", "wb");
//
//    /* Check input */
//    if (fileName == NULL || buffer == NULL || pFrameData == NULL) {
//        printf("fileName:%p, buffer:%p, pFrameData:%p\n", fileName, buffer, pFrameData);
//        return -1;
//    }
//
//    /* Open file to read */
////    snprintf(cFullFileName, sizeof(cFullFileName) - 1,
////             "/mnt/sdcard/recorder_file/%s", fileName);
//
//    snprintf(cFullFileName, sizeof(cFullFileName) - 1,
//             "%s", fileName);
//
//    flvin = flv_open(cFullFileName);
//
//    if (flvin == NULL) {
//        printf("Open file %s error\n", cFullFileName);
//        return -2;
//    }
//
////    /* Read FLV header */
////    if(flv_read_header(flvin, &flvHeader) != FLV_OK)
////        {
////        flv_close(flvin);
////        return -3;
////        }
////    statusCode = RMCTxSideSend(sess, &flvHeader, sizeof(flvHeader), eMediaSubTypeAudioG722, 0);
//    memset(buffer, 0, bufSize);
//
//    /* Stream till the end of file */
//    gInterruptFlag = 0;
////    mylog_info("Streaming file %s", fileName);
////    u32CurTs = RealTimeMsGet() + 60;
//    curPktTimeStamp = ts_offset;
//
//    while (1) {
//        /* Get time stamp for re-send or get new frame */
//        /*if((RealTimeMsGet() - u32CurTs) < 100)
//            {
//            statusCode = RMCTxSideSend(sess, pFrameData, 0, eMediaSubTypeAudioG722, curPktTimeStamp);
//            nanosleep(&s_tsReq, NULL);
//            continue;
//            }*/
//
//        if (flv_read_tag(flvin, &tag) == FLV_ERROR_EOF) {
//            printf("FLV_ERROR_EOF\n");
//            break;
//        }
//
//        /* Adjust time stamp */
//        if (flv_tag_get_body_length(tag) > bufSize) {
//            printf("FLV packet is too large %d. Drop\n", flv_tag_get_body_length(tag));
//            continue;
//        }
//
//        /* Reset buffer */
//        flv_copy_tag(buffer, &tag, bufSize);
//
//        /* Get tag body */
//        statusCode = flv_read_tag_body(flvin, buffer + FLV_TAG_SIZE, bufSize - FLV_TAG_SIZE);
//
//        /* Get previous tag size */
//        flv_read_prev_tag_size(flvin, &previousTagSize);
//        flv_copy_prev_tag_size(buffer + FLV_TAG_SIZE + flv_tag_get_body_length(tag), previousTagSize,
//                               bufSize - (FLV_TAG_SIZE + flv_tag_get_body_length(tag)));
//
//        /* Send the packet */
//        bufferSize = FLV_TAG_SIZE + statusCode + sizeof(uint32);
//        /*if(flv_tag_get_timestamp(tag) > 0 && firstPktTimeStamp == 0)
//            firstPktTimeStamp = flv_tag_get_timestamp(tag);
//        curPktTimeStamp = flv_tag_get_timestamp(tag) - firstPktTimeStamp + ts_offset;
//        mylog_info("[%d] : %u - %u + %u = %u", tag.type,
//                   flv_tag_get_timestamp(tag), firstPktTimeStamp,
//                   ts_offset, curPktTimeStamp);*/
//        curPktTimeStamp += 60;
//        switch (tag.type) {
//            case 8: // auido
//                if (FLVALawPacket2G722(buffer, bufferSize, (g722_enc_t *) audEnc, pFrameData, &iFrameSize) == 0) {
////                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeAudioG722, curPktTimeStamp);
//                    opCallback(pFrameData, iFrameSize, 23, curPktTimeStamp);
//                }
//                break;
//            case 9: // video
//                videoFrameType = FLVideoPacket2H264(buffer, bufferSize, pFrameData, &iFrameSize);
//                if (videoFrameType == 1) {
////                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeVideoIFrame, curPktTimeStamp);
//                    opCallback(pFrameData, iFrameSize, 10, curPktTimeStamp); // IFrame
//                }
//                else if (videoFrameType == 0) {
////                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeVideoIFrame, curPktTimeStamp);
//                    opCallback(pFrameData, iFrameSize, 12, curPktTimeStamp); // PFrame
//                }
//                break;
//            default:
//                continue;
//        }
////        statusCode = RMCTxSideSend(sess, buffer, bufferSize, eMediaSubTypeVideoIFrame, flv_tag_get_timestamp(tag));
////        mylog_info("Tag [%d] : size %u time %u", tag.type, bufferSize, flv_tag_get_timestamp(tag));
////        if(statusCode < 0)
////            {
////            mylog_info("FLV packet size is %d = 0x%0x2 0x%0x2 0x%0x2", flv_tag_get_body_length(tag),
////                       buffer[1], buffer[2], buffer[3]);
////            curPktTimeStamp = -4;
////            break;
////            }
////        else
////            {
////            nanosleep(&s_tsReq, NULL);
////            statusCode = RMCTxSideSend(sess, pFrameData, 0, eMediaSubTypeAudioG722, curPktTimeStamp);
////            }
////
////        if(gInterruptFlag)
////            {
////            curPktTimeStamp = -5;
////            break;
////            }
//    }
////    fclose(pFile);
//    flv_close(flvin);
//    return curPktTimeStamp;
//}
//
//int parseComingData(void *buffer, int bufferSize, void *audEnc, void *pFrameData, int *iFrameSize, int *type) {
//    static int vdata = 0;
//    uint8_t *bff = (uint8_t *) buffer;
//    int ts = -1;
//
//    if (bff[0] == 0x08) {
////        printf("buffer)[0] == 0x08");
//        if (FLVALawPacket2G722(buffer, bufferSize, (g722_enc_t *) audEnc, pFrameData, iFrameSize) == 0) {
//            //                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeAudioG722, curPktTimeStamp);
//            //            opCallback(pFrameData, iFrameSize, 23, curPktTimeStamp);
//            *type = 23;
//            ts = (bff[7] << 24 | bff[4] << 16 | bff[5] << 8 | bff[6]);
//        }
//    }
//    else if (bff[0] == 0x09) { // video
////        printf("buffer)[0] == 0x09");
//        int videoFrameType = FLVideoPacket2H264(buffer, bufferSize, pFrameData, iFrameSize);
//
//        if (videoFrameType == 1) {
//            //                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeVideoIFrame, curPktTimeStamp);
////            opCallback(pFrameData, iFrameSize, 10, curPktTimeStamp); // IFrame
//            *type = 10;
//            ts = (bff[7] << 24 | bff[4] << 16 | bff[5] << 8 | bff[6]);
//            vdata++;
//        }
//        else if (videoFrameType == 0) {
//            //                    statusCode = RMCTxSideSend(sess, pFrameData, iFrameSize, eMediaSubTypeVideoIFrame, curPktTimeStamp);
////            opCallback(pFrameData, iFrameSize, 12, curPktTimeStamp); // PFrame
//            *type = 12;
//            ts = (bff[7] << 24 | bff[4] << 16 | bff[5] << 8 | bff[6]);
//            vdata++;
//        }
//    }
//    else if (bff[0] == 0x98 && bff[1] == 'S' && bff[2] == 'T' && bff[3] == 'O' && bff[4] == 'P') {
//        ts = 89;
//        *type = 89;
//    }
//    else {
//        printf("buffer[0]:%02x\n", bff[0]);
//    }
//
//    if (ts == -1 || ts == 89) {
//        printf("ts:%d %02x %02x %02x %02x %02x %02x %02x %02x\n", ts, bff[7], bff[4], bff[5], bff[6], bff[0], bff[1],
//               bff[2], bff[3]);
//    }
//
////    printf("ts:%d %02x %02x %02x %02x\n", ts, bff[7], bff[4], bff[5], bff[6]);
//    if (vdata == 1) {
//        printf("\nhas video data\n");
//    }
//
//    return ts;
//}
//#else
//static int
//FileStreamEventSend(void* sess, g722_enc_t* audEnc, char* fileName, void* dataBuf, int bufSize, void* pFrameData, int ts_offset)
//    {
//    int             statusCode = -1;
//    int             bufferSize = 0;
//    int             curPktTimeStamp = 0;
//    struct timespec s_tsReq = {0, 1000000};
//    char            *buffer = (char*)dataBuf;
//    char            cFullFileName[2048];
//    int             iFrameSize = 0;
//    uint32          u32CurTs = 0;
//    FILE            *pFile = NULL;
//
//
//    /* Check input */
//    if(sess == NULL || fileName == NULL || buffer == NULL || pFrameData == NULL)
//        return -1;
//
//    /* Open file to read */
//    snprintf(cFullFileName, sizeof(cFullFileName) - 1,
//             "/mnt/sdcard/recorder_file/%s", fileName);
//    pFile = fopen(cFullFileName, "rb");
//    if(pFile == NULL)
//        {
//        mylog_info("Open file %s error", cFullFileName);
//        return -2;
//        }
//
//    /* Read FLV header */
//    fread(buffer, sizeof(char), 9, pFile);
//
//    /* Stream till the end of file */
//    gInterruptFlag = 0;
//    mylog_info("Streaming file %s", fileName);
//    curPktTimeStamp = ts_offset;
//    while(!feof(pFile))
//        {
//        /* Read data */
//        bufferSize = fread(buffer, sizeof(char), 999, pFile);
//        statusCode = RMCTxSideSend(sess, buffer, bufferSize, eMediaSubTypeAudioAlaw, curPktTimeStamp);
//        curPktTimeStamp += 60;
//        if(statusCode < 0)
//            {
//            curPktTimeStamp = -4;
//            break;
//            }
//        else
//            {
//            nanosleep(&s_tsReq, NULL);
//            statusCode = RMCTxSideSend(sess, pFrameData, 0, eMediaSubTypeAudioG722, curPktTimeStamp);
//            }
//
//        if(gInterruptFlag)
//            {
//            curPktTimeStamp = -5;
//            break;
//            }
//        }
//    fclose(pFile);
//    return curPktTimeStamp;
//    }
//#endif
//static void*
//FileStreamSessionCreate(void* mem)
//    {
//    int     sock_val = -1;
//    void    *sess = NULL;
//    char    camera_ip[16];
//    int     camera_port = 0;
//
//    sock_val = RMCLocalSocketCreate(&camera_port, camera_ip);
//    if(sock_val < 0)
//        {
//        mylog_error("Can not create socket");
//        return NULL;
//        }
//
//    sess = RMCAllocUseExternalSocket(mem, RelayServerIPGet(), RelayServerPortGet(), sock_val);
//    if(sess == NULL)
//        {
//        mylog_error("Allocate media session error");
//        return NULL;
//        }
//    else
//        RelayStreamStatusUpdate(1);
//    return sess;
//    }
//
//static void *
//FileStreamerServiceHandler(void *d)
//    {
//    tFileStreamer   *ctx = (tFileStreamer*)d;
//    void            *pMemSess = malloc(sizeof(char) * RMCSizeGet());
//    int             imax_data_size = 100 * 1024;
//    void            *pPktData = malloc(sizeof(char) * imax_data_size);
//    void            *pFrameData = malloc(sizeof(char) * imax_data_size);
//    void            *sess = NULL;
//    int             last_clip_ts = 0;
//    g722_enc_t      g722AudioEnc;
//    int             i = 0;
//
//    if(pthread_mutex_init(&(ctx->lock), NULL) != 0)
//        return 0;
//    if(pthread_cond_init(&(ctx->cond), NULL) != 0)
//        return 0;
//    if(pMemSess == NULL || pPktData == NULL || pFrameData == NULL)
//        return 0;
//
//    while(ctx->cmd != eFSCmdServiceStop)
//        {
//        ctx->stat = eFSStatIdle;
//        pthread_mutex_lock(&(ctx->lock));
//        mylog_info("Waiting for command");
//        pthread_cond_wait(&(ctx->cond), &(ctx->lock));
//        pthread_mutex_unlock(&(ctx->lock));
//        mylog_info("Serving command %d", ctx->cmd);
//        if(ctx->cmd == eFSCmdServiceStop)
//            break;
//
//        /* Setup file session */
//        sess = FileStreamSessionCreate(pMemSess);
//        if(sess == NULL)
//            continue;
//
//        /* Start send open stream */
//        if(MediaSessionRelaySetup(sess) != 0)
//            {
//            RMCCleanup(sess);
//            continue;
//            }
//
//        /* Start stream file */
//        ctx->cur_idx = 0;
//        last_clip_ts = 0;
//        g722_enc_init(&g722AudioEnc);
//        mylog_info("Start to stream %d files", ctx->total_file);
//        sleep(1);
//        while(ctx->cmd == eFSCmdEventStart)
//            {
//            last_clip_ts = FileStreamEventSend(sess, &g722AudioEnc, ctx->clip_name[ctx->cur_idx],
//                                               pPktData, imax_data_size, pFrameData, last_clip_ts);
//            if(last_clip_ts > 0)
//                ctx->cur_idx++;
//            else
//                break;
//            if(ctx->cur_idx >= ctx->total_file)
//                {
//                sleep(5);
//                break;
//                }
//            }
//        RelayStreamStatusUpdate(0);
//        RMCCleanup(sess);
//        ctx->total_file = 0;
//        }
//
//    free(pMemSess);
//    free(pPktData);
//    free(pFrameData);
//    pthread_cond_destroy(&(ctx->cond));
//    pthread_mutex_destroy(&(ctx->lock));
//    return 0;
//    }
//
//int
//FileStreamerServiceRun()
//    {
//    int             iRetCode = -1;
//    size_t          stacksize = 32*1024;
//    pthread_attr_t  tattr;
//
//    memset(&gFileCtx, 0, sizeof(tFileStreamer));
//    nxcNetworkGetMacAddr(gFileCtx.mac);
//
//    if(pthread_attr_init(&tattr) != 0)
//        return -1;
//    if(pthread_attr_setstacksize(&tattr, stacksize) != 0)
//        return -2;
//    iRetCode = pthread_create(&(gFileCtx.thread), &tattr, FileStreamerServiceHandler, (void*)(&gFileCtx));
//    if(iRetCode != 0)
//        return -iRetCode;
//    pthread_attr_destroy(&tattr);
//    iRetCode = *(int *)(&gFileCtx.thread) & 0x7FFFFFFF;
//    return iRetCode;
//    }
//
//void
//FileStreamerServiceExit()
//    {
//    gFileCtx.cmd = eFSCmdServiceStop;
//    pthread_cond_signal(&(gFileCtx.cond));
//    }
//
//void
//FileStreamEventStop()
//    {
//    gFileCtx.cmd = eFSCmdEventStop;
//    gInterruptFlag = 1;
//    gFileCtx.total_file = 0;
//    }
//
//int
//FileStreamEventStart()
//    {
//    if(gFileCtx.total_file)
//        {
//        gFileCtx.cmd = eFSCmdEventStart;
//        pthread_cond_signal(&(gFileCtx.cond));
//        return 0;
//        }
//    return -1;
//    }
//
//int
//FileStreamEventAdd(char* filename, char* checksum)
//    {
//    int i;
//    if(gFileCtx.stat != eFSStatIdle)
//        return -1;
//    if(filename == NULL)
//        return -1;
//    mylog_info("Adding %d events", ScanAllEventClip("/mnt/sdcard/recorder_file", filename, &gFileCtx));
//    for(i = 0; i < gFileCtx.total_file; i++)
//        mylog_info("%s", gFileCtx.clip_name[i]);
//    return gFileCtx.total_file;
//    }
