#ifndef FFMPEG_RECORDER_VIDEO_H
#define FFMPEG_RECORDER_VIDEO_H

#include "decoder.h"

extern "C" {
#include "encryption.h"
}

class RecorderVideo : public IDecoder
{
    public:
        RecorderVideo(AVStream* stream);
        RecorderVideo(AVCodecContext* avctx);

        ~RecorderVideo();
        void	 enqueue(AVPacket* packet);
        void     write_video_frame(AVFormatContext *oc); 
        bool     prepare( AVFormatContext *oc,  AVStream * src_stream );
        bool     prepare( AVFormatContext *oc);
        //bool     prepare( AVFormatContext *oc,  AVCodecContext * src_context );
        AVStream * getVideoStream(); 
        void     cleanUp(); 
        bool     hasUnencodedFrames();
        void	 setEncryptionEnabled(bool isEnabled);
        void	 setEncryptionKey(const char* val);
        void	set_first_ts(int64_t first_pts, int64_t first_dts);
        bool	is_first_ts_set();

    private:

        AVStream * src_stream; 
        AVCodecContext * src_codec_context ;
        bool                       mStopRecording;
        bool first_ts_set;
        int64_t first_pts, first_dts;
        bool isEncrypted;
        EncryptionContext* enc_ctx;

        void  stopRecording(); 
        AVStream *    add_stream(AVFormatContext *oc, AVCodec **codec,
                        enum AVCodecID codec_id);
        void tryDecryptPacket(AVPacket* packet);
        int set_string_binary(const char *val, uint8_t **dst, int* keylen);
};

#endif 
