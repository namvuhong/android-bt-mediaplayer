/*
    $Id: flv.c 231 2011-06-27 13:46:19Z marc.noirot $

    FLV Metadata updater

    Copyright (C) 2007-2012 Marc Noirot <marc.noirot AT gmail.com>

    This file is part of FLVMeta.

    FLVMeta is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    FLVMeta is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with FLVMeta; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "flv.h"

/* FLV stream functions */
flv_stream * flv_open(const char * file) {
    flv_stream * stream = (flv_stream *) malloc(sizeof(flv_stream));
    if (stream == NULL) {
        return NULL;
    }
    stream->flvin = fopen(file, "r+");
    if (stream->flvin == NULL) {
        free(stream);
        printf("Error fopen\n");
        return NULL;
    }
    stream->current_tag_body_length = 0;
    stream->current_tag_body_overflow = 0;
    stream->current_tag_offset = 0;
    stream->state = FLV_STREAM_STATE_START;
    return stream;
}

int flv_read_header(flv_stream * stream, flv_header * header) {
    if (stream == NULL
    || stream->flvin == NULL
    || feof(stream->flvin)
    || stream->state != FLV_STREAM_STATE_START) {
        return FLV_ERROR_EOF;
    }

    if (fread(&header->signature, sizeof(header->signature), 1, stream->flvin) == 0
    || fread(&header->version, sizeof(header->version), 1, stream->flvin) == 0
    || fread(&header->flags, sizeof(header->flags), 1, stream->flvin) == 0
    || fread(&header->offset, sizeof(header->offset), 1, stream->flvin) == 0) {
        return FLV_ERROR_EOF;
    }

    if (header->signature[0] != 'F'
    || header->signature[1] != 'L'
    || header->signature[2] != 'V') {
        return FLV_ERROR_NO_FLV;
    }
    
    stream->state = FLV_STREAM_STATE_PREV_TAG_SIZE;
    return FLV_OK;
}

int flv_read_prev_tag_size(flv_stream * stream, uint32 * prev_tag_size) {
    uint32_be val;
    if (stream == NULL
    || stream->flvin == NULL
    || feof(stream->flvin)) {
        return FLV_ERROR_EOF;
    }

    /* skip remaining tag body bytes */
    if (stream->state == FLV_STREAM_STATE_TAG_BODY) {
        lfs_fseek(stream->flvin, stream->current_tag_offset + FLV_TAG_SIZE + uint24_be_to_uint32(stream->current_tag.body_length), SEEK_SET);
        stream->state = FLV_STREAM_STATE_PREV_TAG_SIZE;
    }

    if (stream->state == FLV_STREAM_STATE_PREV_TAG_SIZE) {
        if (fread(&val, sizeof(uint32_be), 1, stream->flvin) == 0) {
            return FLV_ERROR_EOF;
        }
        else {
            stream->state = FLV_STREAM_STATE_TAG;
            *prev_tag_size = swap_uint32(val);
            return FLV_OK;
        }
    }
    else {
        return FLV_ERROR_EOF;
    }
}

int flv_read_tag(flv_stream * stream, flv_tag * tag) {
    if (stream == NULL
    || stream->flvin == NULL
    || feof(stream->flvin)) {
        return FLV_ERROR_EOF;
    }

    /* skip header */
    if (stream->state == FLV_STREAM_STATE_START) {
        lfs_fseek(stream->flvin, FLV_HEADER_SIZE, SEEK_CUR);
        stream->state = FLV_STREAM_STATE_PREV_TAG_SIZE;
    }

    /* skip current tag body */
    if (stream->state == FLV_STREAM_STATE_TAG_BODY) {
        lfs_fseek(stream->flvin, stream->current_tag_offset + FLV_TAG_SIZE + uint24_be_to_uint32(stream->current_tag.body_length), SEEK_SET);
        stream->state = FLV_STREAM_STATE_PREV_TAG_SIZE;
    }
 
    /* skip previous tag size */
    if (stream->state == FLV_STREAM_STATE_PREV_TAG_SIZE) {
        lfs_fseek(stream->flvin, sizeof(uint32_be), SEEK_CUR);
        stream->state = FLV_STREAM_STATE_TAG;
    }
    
    if (stream->state == FLV_STREAM_STATE_TAG) {
        stream->current_tag_offset = lfs_ftell(stream->flvin);

        if (fread(&tag->type, sizeof(tag->type), 1, stream->flvin) == 0
        || fread(&tag->body_length, sizeof(tag->body_length), 1, stream->flvin) == 0
        || fread(&tag->timestamp, sizeof(tag->timestamp), 1, stream->flvin) == 0
        || fread(&tag->timestamp_extended, sizeof(tag->timestamp_extended), 1, stream->flvin) == 0
        || fread(&tag->stream_id, sizeof(tag->stream_id), 1, stream->flvin) == 0) {
            return FLV_ERROR_EOF;
        }
        else {
            memcpy(&stream->current_tag, tag, sizeof(flv_tag));
            stream->current_tag_body_length = uint24_be_to_uint32(tag->body_length);
            stream->current_tag_body_overflow = 0;
            stream->state = FLV_STREAM_STATE_TAG_BODY;
            return FLV_OK;
        }
    }
    else {
        return FLV_ERROR_EOF;
    }
}

size_t flv_read_tag_body(flv_stream * stream, void * buffer, size_t buffer_size) {
    size_t bytes_number;

    if (stream == NULL
    || stream->flvin == NULL
    || feof(stream->flvin)
    || stream->state != FLV_STREAM_STATE_TAG_BODY) {
        return 0;
    }

    bytes_number = (buffer_size > stream->current_tag_body_length) ? stream->current_tag_body_length : buffer_size;
    bytes_number = fread(buffer, sizeof(byte), bytes_number, stream->flvin);
    
    stream->current_tag_body_length -= (uint32)bytes_number;

    if (stream->current_tag_body_length == 0) {
        stream->state = FLV_STREAM_STATE_PREV_TAG_SIZE;
    }

    return bytes_number;
}

size_t flv_copy_tag(void * to, const flv_tag * tag, size_t buffer_size) {
    char * out = to;
    if (buffer_size < FLV_TAG_SIZE) {
        return 0;
    }

    memcpy(out, &tag->type, sizeof(tag->type));
    out += sizeof(tag->type);

    memcpy(out, &tag->body_length, sizeof(tag->body_length));
    out += sizeof(tag->body_length);

    memcpy(out, &tag->timestamp, sizeof(tag->timestamp));
    out += sizeof(tag->timestamp);

    memcpy(out, &tag->timestamp_extended, sizeof(tag->timestamp_extended));
    out += sizeof(tag->timestamp_extended);

    memcpy(out, &tag->stream_id, sizeof(tag->stream_id));

    return FLV_TAG_SIZE;
}

size_t flv_copy_prev_tag_size(void *to, uint32 prev_tag_size, size_t buffer_size) {
    uint32_be pts = swap_uint32(prev_tag_size);

    if (buffer_size < sizeof(uint32)) {
        return 0;
    }

    memcpy(to, &pts, sizeof(uint32_be));

    return sizeof(uint32_be);
}

void flv_close(flv_stream * stream) {
    if (stream != NULL) {
        if (stream->flvin != NULL) {
            fclose(stream->flvin);
        }
        free(stream);
    }
}
