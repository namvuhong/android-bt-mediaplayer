/*******************************************************************************
* Nxcomm 2013
* Product: Cmdserver library
* @Author: trungdo@
* @DateTime: 12Dec2013 - 9:30:00@
* @Version:  0.1@
* Description: H264 encryption header
*******************************************************************************/

#ifndef __ENCRYPTION_H__
#define __ENCRYPTION_H__

extern "C" {
#include <stdlib.h>
#include <stdio.h>
#include "libavutil/log.h"
#include "aes_sw.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
}

class EncryptionContext
{
public:
	EncryptionContext();
	~EncryptionContext();
	void try_decrypt2(uint8_t * data , int len );
	void try_decrypt3(uint8_t * data , int len );
	void try_encrypt3(uint8_t * data , int len );
	void try_decrypt_audio(uint8_t * data , int len );
	void try_encrypt_audio(uint8_t * data , int len);
	int decryptFile(const char* filePath);
	int encryptFile(const char* filePath);
	void prepare(const char* key, const char* iv);

private:
	aes_context sw_ctx;
	uint8_t* key;
	int keylen;
	uint8_t*  iv;
	int ivlen;
	aes_context sw_ctx_a;
	int set_iv(uint8_t* new_iv, int iv_len);
	int set_audio_iv(uint8_t* new_iv, int iv_len);
	int intarray2hexstr(uint8_t *c, int len);
	int hexchar2int(char c);
	int alaw_aes_init(aes_context *ctx, uint8_t *key, uint8_t nbytes);
	int h264_aes_init(aes_context *ctx, uint8_t *key, uint8_t nbytes);
	void h264_aes_encrypt(aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);
	void h264_aes_decrypt(aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);
	int set_string_binary(const char *val, uint8_t **dst, int* keylen);
	int pcm_enc_init(uint8_t* key, int keylen);
	int h264_enc_init(uint8_t* key, int keylen, uint8_t* new_iv, int iv_len);
	void read_flv_tag(FILE* fp, uint8_t** buf, int* buf_size, int* pkt_type, int* isSPS);
	uint32_t swap_uint32( uint32_t val );
	uint32_t swap_uint24( uint32_t val );
};

#endif  // end of __H264_AES_H__

// @log:h264_encryption.h@
// DateTime: 12Dec2013 - 09:30:00
// Author: trungdo
// Description: Initial version
