#!/bin/bash


######################################################
# Usage:
#   put this script in top of FFmpeg source tree
#   ./build_android
#
# It generates binary for following architectures:
#     ARMv6 
#     ARMv6+VFP 
#     ARMv7+VFPv3-d16 (Tegra2) 
#     ARMv7+Neon (Cortex-A8)
#
# Customizing:
# 1. Feel free to change ./configure parameters for more features
# 2. To adapt other ARM variants
#       set $CPU and $OPTIMIZE_CFLAGS 
#       call build_one
######################################################

CROSS_PREFIX=/home/hoang/Project/workspace/mxe/usr/bin/i686-w64-mingw32-
MXE_EXTRA_PATH=/home/hoang/Project/workspace/mxe/usr/i686-w64-mingw32
CFLAGS="-DDECLSPEC=''"
PKG_CONFIG="$CROSS_PREFIX"pkg-config

function build_one
{
./configure --prefix=$HOME \
    --target-os=mingw32 \
    --arch=x86 \
    --cross-prefix=$CROSS_PREFIX \
    --pkg-config="$PKG_CONFIG --static" \
    --enable-memalign-hack \
    --enable-version3 \
    --enable-asm \
    --disable-w32threads  \
    --extra-libs="-lgcc" \
    --extra-cflags=" -O3 -DPTW32_STATIC_LIB -fpic -DHAVE_SYS_UIO_H=1 -Dipv6mr_interface=ipv6mr_ifindex -fasm -Wno-psabi -fno-short-enums  -fno-strict-aliasing -finline-limit=300 -I$X264_PREFIX/include -I$OPENSSL_PREFIX/include $OPTIMIZE_CFLAGS -DRTCP_THRESHOLD=8 -DCVISION_RTP_WITH_STUN -DCVISION_GET_NAME_INFO -DCVISION_FILTER_CORRUPT_FRAMES -DCVISION_H264_ENCRYPTION -mfpmath=sse -msse -I$MXE_EXTRA_PATH/include" \
    --extra-ldflags=" -mconsole -L$MXE_EXTRA_PATH/lib" \
    --disable-shared \
    --enable-static \
    --enable-runtime-cpudetect \
    --disable-everything \
    --disable-doc \
    --enable-demuxer=h264 \
    --enable-demuxer=flv \
    --enable-demuxer=mpegps \
    --enable-demuxer=mpegts \
    --enable-demuxer=mpegvideo \
    --enable-demuxer=mpegtsraw \
    --enable-demuxer=pcm_s16le \
    --enable-muxer=flv \
    --enable-muxer=pcm_s16le \
    --enable-protocol=file \
    --enable-protocol=http \
    --enable-protocol=rtmp \
    --enable-protocol=rtmpt \
    --enable-protocol=rtmpe \
    --enable-protocol=rtmpte \
    --enable-protocol=rtp \
    --enable-protocol=tcp \
    --enable-protocol=udp \
    --enable-avformat \
    --enable-avcodec \
    --enable-decoder=imc \
    --enable-decoder=aac \
    --enable-decoder=mpeg4 \
    --enable-decoder=h264 \
    --enable-encoder=flv \
    --enable-decoder=flv \
    --enable-decoder=adpcm_swf \
    --enable-decoder=adpcm_ms \
    --enable-decoder=adpcm_ima_wav \
    --enable-decoder=adpcm_ima_qt \
    --enable-decoder=adpcm_yamaha \
    --enable-encoder=adpcm_swf \
    --enable-encoder=adpcm_ms \
    --enable-encoder=adpcm_ima_wav \
    --enable-encoder=adpcm_ima_qt \
    --enable-encoder=adpcm_yamaha \
    --enable-encoder=pcm_s16le \
    --enable-decoder=pcm_s16le \
    --enable-decoder=pcm_s16le_planar \
    --enable-parser=h264 \
    --enable-parser=mpeg4video \
    --enable-parser=mpegaudio \
    --enable-parser=mpegvideo \
    --enable-muxer=ffm --enable-demuxer=ffm --enable-muxer=rtp --enable-demuxer=rtp \
    --enable-muxer=rtsp --enable-demuxer=rtsp \
    --enable-decoder=amrnb \
    --enable-decoder=amrwb \
    --enable-decoder=adpcm_g722 \
    --enable-encoder=adpcm_g722 \
    --enable-muxer=g722 --enable-demuxer=g722 \
    --enable-encoder=pcm_mulaw \
    --enable-decoder=pcm_mulaw \
    --enable-encoder=pcm_alaw \
    --enable-decoder=pcm_alaw \
    --enable-muxer=pcm_mulaw \
    --enable-demuxer=pcm_mulaw \
    --enable-muxer=pcm_alaw \
    --enable-demuxer=pcm_alaw \
    --enable-filter=aresample \
    --enable-filter=aformat \
    $ADDITIONAL_CONFIGURE_FLAG


make clean
make  -j4 
make install
}

if [ $# -lt 1 ] ; then 
#full rebuild

#configureARMv7a 
#
#build_one || exit 1

#configureARM

build_one || exit 1


elif [ $1 -eq 0 ] ; then 
#- Dont reconfigure - just dirty build
echo "**************** WARNING: this will just rebuild the last configuration (arm or armv7) not both "

configureARM
dirty_build
fi



