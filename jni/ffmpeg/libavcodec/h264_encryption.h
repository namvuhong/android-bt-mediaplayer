/*******************************************************************************
* Nxcomm 2013
* Product: Cmdserver library
* @Author: trungdo@
* @DateTime: 12Dec2013 - 9:30:00@
* @Version:  0.1@
* Description: H264 encryption header
*******************************************************************************/

#ifndef __H264_AES_H__
#define __H264_AES_H__

#include <stdlib.h>
#include "libavutil/log.h"
#include "aes_sw.h"
#include "h264.h"

typedef struct PCMDecode {
    AVCodecContext *avctx;
    short   table[256];
    uint8_t decryption_enable;
    const uint8_t * key;
    int keylen; 
    const uint8_t * iv;
    int ivlen;
} PCMDecode;


int cvision_pcm_enc_init(PCMDecode  *h);
int cvision_h264_enc_init(H264Context *h);
int cvision_set_iv(uint8_t* new_iv, int iv_len);
int cvision_set_audio_iv(uint8_t* new_iv, int iv_len);
static int intarray2hexstr(uint8_t *c, int len);
void try_decrypt2(uint8_t * data , int len );

void try_decrypt3(uint8_t * data , int len );

void try_decrypt_audio(uint8_t * data , int len );


//int h264_aes_init(struct aes_context *ctx, uint8_t *key, uint8_t nbytes);
//void h264_aes_encrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);
//void h264_aes_decrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length);



#endif  // end of __H264_AES_H__

// @log:h264_encryption.h@
// DateTime: 12Dec2013 - 09:30:00
// Author: trungdo
// Description: Initial version
