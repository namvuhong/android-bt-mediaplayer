/*******************************************************************************
* Nxcomm 2013
* Product: Cmdserver library
* @Author: trungdo@
* @DateTime: 12Dec2013 - 9:30:00@
* @Version:  0.1@
* Description: H264 encryption
*******************************************************************************/

#include "h264_encryption.h"

// DEBUG  decryption
//#define DEBUG_DECRYPT_STREAM 1

// H264 enc params
#define NAL_DATAOFFSET 1
#define NAL_ENC_LEN 16
#define NAL_ENC_DISTANCE 1024



//////////////////////// ALAW DECRYPTION ///////////////////////////////////////
static int alaw_aes_init(struct aes_context *ctx, uint8_t *key, uint8_t nbytes)
{
	char mode[16] = {'e', 'c', 'b', 0}; // ecb  mode
	int lreturn;

	if ((nbytes!=16) && (nbytes!=24) && (nbytes!=32)) // AES 128, 192, 256 bits
		return 1;

	// Intialize AES roundkey + mode
	lreturn = aes_set_key(ctx, key, nbytes * 8, mode);

	return lreturn;
}


//////////////////////// H264 ENCRYPTION ///////////////////////////////////////
//******************************************************************************
// @description: Init roundkey for h264 encryption
// @param:
//	ctx: aes derived key and mode
//	key: pointer to key
//	nbytes: key length
// @return: 0 if OK, otherwise error
// @details: key expansion and mode setting
//******************************************************************************
static int h264_aes_init(struct aes_context *ctx, uint8_t *key, uint8_t nbytes){
	char mode[16] = {'c', 'b', 'c', 0}; // CBC mode
	int lreturn;

	if ((nbytes!=16) && (nbytes!=24) && (nbytes!=32)) // AES 128, 192, 256 bits
		return 1;

	// Intialize AES roundkey + mode
	lreturn = aes_set_key(ctx, key, nbytes * 8, mode);

	return lreturn;
}

//******************************************************************************
// @description: Encrypt h264 NAL package
// @param:
//	ctx: aes derived key and mode
//	iv: initial value
//	nal_data: pointer to frame data excluding NAL marker (0x00000001)
//	nal_length: length of frame data
// @return:
// @details: starting from nal_data, with an offset NAL_DATAOFFSET,
//           for each chunk of NAL_ENC_DISTANCE bytes,
//           encrypt NAL_ENC_LEN length of data
//******************************************************************************
static void h264_aes_encrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length){
	unsigned long j;

	j=NAL_DATAOFFSET;
	while((j+NAL_ENC_LEN)<=nal_length){
		aes_encrypt(ctx, iv, &nal_data[j], NAL_ENC_LEN);
		j+=NAL_ENC_DISTANCE;
	}
}

//******************************************************************************
// @description: Encrypt h264 NAL package
// @param:
//	ctx: aes derived key and mode
//	iv: initial value
//	nal_data: pointer to frame data excluding NAL marker (0x00000001)
//	nal_length: length of frame data
// @return:
// @details: starting from nal_data, with an offset NAL_DATAOFFSET,
//           for each chunk of NAL_ENC_DISTANCE bytes,
//           decrypt NAL_ENC_LEN length of data
//******************************************************************************
static void h264_aes_decrypt(struct aes_context *ctx, uint8_t iv[16], uint8_t* nal_data, uint32_t nal_length){
	unsigned long j;

	j=NAL_DATAOFFSET;
	while((j+NAL_ENC_LEN)<=nal_length){
		aes_decrypt(ctx, iv, &nal_data[j], NAL_ENC_LEN);
		j+=NAL_ENC_DISTANCE;
	}
}





//******************************************************************************
//  API
//******************************************************************************
//static uint8_t  mKey  []= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
static uint8_t  iv  []= {21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
static struct aes_context sw_ctx;

//static uint8_t  iv_a  []= {21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
static struct aes_context sw_ctx_a;


int cvision_pcm_enc_init(PCMDecode *h)
{
	if (!h)
	{
		return -1;
	}

    intarray2hexstr(h->key, h->keylen);

    if (alaw_aes_init(&sw_ctx_a, h->key, h->keylen)) {
    	assert("init AES failed\n");

    	return -1;
    }

   // if (cvision_set_audio_iv(h->iv, h->ivlen) != 0)
   // {
   // 	return -1;
   // }

    return 0;
}

int cvision_h264_enc_init(H264Context *h)
{
	if (!h)
	{
		return -1;
	}

    intarray2hexstr(h->key, h->keylen);

    if (h264_aes_init(&sw_ctx, h->key, h->keylen)) {
    	assert("init AES failed\n");

    	return -1;
    }

    if (cvision_set_iv(h->iv, h->ivlen) != 0)
    {
    	return -1;
    }

    return 0;
}

int cvision_set_iv(uint8_t* new_iv, int iv_len)
{
	if (iv_len != 16)
	{
		return -1;
	}
	for (int i=0; i<iv_len; i++)
	{
		iv[i] = new_iv[i];
	}

	return 0;
}
#if 0
int cvision_set_audio_iv(uint8_t* new_iv, int iv_len)
{
	if (iv_len != 16)
	{
		return -1;
	}
	for (int i=0; i<iv_len; i++)
	{
		iv_a[i] = new_iv[i];
		av_log(NULL, AV_LOG_DEBUG, "iv[%d] = %d\n", i, iv_a[i]);
	}

	return 0;
}
#endif

static int intarray2hexstr(uint8_t* c, int len) {

	if (len > 0)
	{
		char *key = (char*) malloc((2*len+1) * sizeof(char));
		key[2*len] = '\0';
		char hexchar[3];
		for (int i=0; i<len; i++)
		{
			snprintf(hexchar, 3, "%02X", c[i]);
			strncpy(key + i*2, hexchar, 2);
		}
		free(key);
	}
    return 0;
}

void try_decrypt2(uint8_t * data , int len )
{

    /* Decrypt frame */
    uint8_t * buff_plain_orig = data;
    uint8_t *NAL_pointer; // pointer to NAL data
    unsigned long cur_NAL_LEN=0,cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;
    for (i =4; i<len;i++)
    {

        if ( (buff_plain_orig[i-3]==0x00) &&
                (buff_plain_orig[i-2]==0x00) && (buff_plain_orig[i-1]==0x00) && (buff_plain_orig[i]==0x01))
        {

#ifdef DEBUG_DECRYPT_STREAM
            av_log(NULL, AV_LOG_DEBUG,  "found start sequenct 0001 at i:%d totallen:%d\n",i, len);

            {
                av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                        buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                        buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                        buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                        buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                      );
            }

#endif


            cur_NAL_LEN=i-3-cur_NAL_POS;
            NAL_pointer = &buff_plain_orig[cur_NAL_POS+4];

#ifdef DEBUG_DECRYPT_STREAM
            av_log(NULL, AV_LOG_DEBUG, " 1 cur_NAL_LEN: %lu \n",  cur_NAL_LEN);
#endif
            h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, cur_NAL_LEN-4);

#ifdef DEBUG_DECRYPT_STREAM
            {
                av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                        buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                        buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                        buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                        buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                      );
            }
#endif
            cur_NAL_POS= i -3;
        }
    }
    cur_NAL_LEN=len - cur_NAL_POS;

    //We only continue if more than 1 block is available
    if (cur_NAL_LEN > 16)
    {
#ifdef DEBUG_DECRYPT_STREAM

        {
            av_log(NULL, AV_LOG_DEBUG, "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

        av_log(NULL, AV_LOG_DEBUG, " 2 cur_NAL_LEN: %lu \n",  cur_NAL_LEN);

#endif
        NAL_pointer = &buff_plain_orig[cur_NAL_POS+4];
        h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, cur_NAL_LEN-4);

#ifdef DEBUG_DECRYPT_STREAM


        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif
    }


}
void try_decrypt3(uint8_t * data , int len )
{
    /* Decrypt frame */
    uint8_t * buff_plain_orig = data;

    uint8_t *NAL_pointer; // pointer to NAL data
    unsigned long cur_NAL_LEN=0,cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;

    {



#ifdef DEBUG_DECRYPT_STREAM

        {
            av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

#endif


        cur_NAL_LEN=len;
        NAL_pointer = &buff_plain_orig[0];

        av_log(NULL, AV_LOG_DEBUG, " cur_NAL_LEN: %lu \n",  cur_NAL_LEN);
        h264_aes_decrypt(&sw_ctx, iv, NAL_pointer, len);

#ifdef DEBUG_DECRYPT_STREAM
        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif
    }

}



// Full frame decryption 
void try_decrypt_audio(uint8_t * data , int len )
{
    uint8_t * buff_plain_orig = data;
    unsigned long cur_NAL_POS = 0; // input to h264_aes_encrypt/decrypt

    int i = 0;
    //We only continue if more than 1 block is available
    if (len > 16)
    {
#ifdef DEBUG_DECRYPT_STREAM

        av_log(NULL, AV_LOG_DEBUG, " 2 decrypt audio len : %lu \n",  len);
        {
            av_log(NULL, AV_LOG_DEBUG , "some data before : %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }

#endif
        //Phung: pass @iv here as a dummy parameter
        //     since we encrypt using ECB mode, iv is not used anyway
		aes_decrypt(&sw_ctx_a, iv, data , len);

#ifdef DEBUG_DECRYPT_STREAM
        {
            av_log(NULL, AV_LOG_DEBUG, "some data after: %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x ",
                    buff_plain_orig[cur_NAL_POS +0],buff_plain_orig[cur_NAL_POS +1],buff_plain_orig[cur_NAL_POS +2],buff_plain_orig[cur_NAL_POS +3],
                    buff_plain_orig[cur_NAL_POS +4],buff_plain_orig[cur_NAL_POS +5],buff_plain_orig[cur_NAL_POS +6],buff_plain_orig[cur_NAL_POS +7],
                    buff_plain_orig[cur_NAL_POS +8],buff_plain_orig[cur_NAL_POS +9],buff_plain_orig[cur_NAL_POS +10],buff_plain_orig[cur_NAL_POS +11],
                    buff_plain_orig[cur_NAL_POS +12],buff_plain_orig[cur_NAL_POS +13],buff_plain_orig[cur_NAL_POS +14],buff_plain_orig[cur_NAL_POS +15]
                  );
        }
#endif

    }


}

// @log:h264_encryption.c@
// DateTime: 12Dec2013 - 09:30:00
// Author: trungdo
// Description: Initial version
