LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)
#LOCAL_MODULE := libjnivideo
#LOCAL_SRC_FILES := libjnivideo.so
#include $(PREBUILT_SHARED_LIBRARY)
#
#include $(CLEAR_VARS)
#LOCAL_MODULE := libjniaudio
#LOCAL_SRC_FILES := libjniaudio.so
#include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE :=  libyuv
LOCAL_SRC_FILES := libyuv/$(TARGET_ARCH_ABI)/libyuv.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE :=  librmc
LOCAL_SRC_FILES := librmc/$(TARGET_ARCH_ABI)/librmc.so
include $(PREBUILT_SHARED_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_MODULE :=  libAVAPIs 
#LOCAL_SRC_FILES := libAVAPIs.so
#include $(PREBUILT_SHARED_LIBRARY)
#
#include $(CLEAR_VARS)
#LOCAL_MODULE :=  libIOTCAPIs 
#LOCAL_SRC_FILES := libIOTCAPIs.so
#include $(PREBUILT_SHARED_LIBRARY)
