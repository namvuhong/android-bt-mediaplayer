NDK=/home/hoang/android-ndk-r8e
#PLATFORM=$NDK/platforms/android-14/arch-arm/
PLATFORM=$NDK/platforms/android-14/arch-x86/

#Use GCC 4.6 will cause crash on some phones
ARM_GCC_VERSION=4.4.3
NOEXECSTACK_FLAG="-z,noexecstack"

#from 4.6 the flag change -- 
#ARM_GCC_VERSION=4.6
#NOEXECSTACK_FLAG="-z noexecstack"

#build for arm
#PREBUILT=$NDK/toolchains/arm-linux-androideabi-$ARM_GCC_VERSION/prebuilt/linux-x86_64
#EABIARCH=arm-linux-androideabi
#ARCH=arm
#CROSS_COMPILE_PREFIX=arm-linux-androideabi

#build for x86
PREBUILT=$NDK/toolchains/x86-$ARM_GCC_VERSION/prebuilt/linux-x86_64
EABIARCH=x86
ARCH=x86
CROSS_COMPILE_PREFIX=i686-linux-android
