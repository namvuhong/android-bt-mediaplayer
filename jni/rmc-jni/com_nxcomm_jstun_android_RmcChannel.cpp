/*
 * Copyright (C) 2015 NxComm Pte. Ltd.
 *
 * This unpublished material is proprietary to NxComm.
 * All rights reserved. The methods and
 * techniques described herein are considered trade secrets
 * and/or confidential. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of NxComm.
 */

/* FILE   : com_nxcomm_jstun_android_RmcChannel.cpp
 * AUTHOR : hoang
 * DATE   : Dec 09, 2015
 * DESC   : This is JNI part for StunClient Java class.
 */

#include "jniUtils.h"
extern "C" {
#include "rmc_inc/rmc_channel.h"
#include "rmc_inc/RMC.h"
#include "rmc_inc/mylog.h"
#include "mediaplayer/g722_enc.h"
}

#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

static const char* const kClassPathName = "com/nxcomm/jstun_android/RmcChannel";

static char* file_name_encoder = "/storage/emulated/0/test_encoder_recv.h264";
static char* file_name_file = "/storage/emulated/0/test_file_recv.h264";

static jboolean com_nxcomm_jstun_android_RmcChannel_init(JNIEnv *env, jobject thiz)
{
  bool isSuccess = false;
  int ret = -1;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  log_info("Initilizing RMC channel...");
  RmcChannelInfo *rmcInfo = rmcChannelInfoAlloc();
  if (rmcInfo == NULL)
  {
    log_error("Create rmc channel info failed\n");
    return isSuccess;
  }

  env->SetLongField(thiz, rmcChannelInfoPtrFid, (long)rmcInfo);
  rmcInfo->rmcChannelObj = env->NewGlobalRef(thiz);
  rmcInfo->notifyRmcChannelStatusMid = env->GetMethodID(clazz, "notifyRmcChannelStatus", "(I)V");
  rmcInfo->notifyRmcChannelHandshakeFailedMid = env->GetMethodID(clazz, "notifyRmcChannelHandshakeFailed", "()V");
  rmcInfo->updateRmcChannelTypeMid = env->GetMethodID(clazz, "setRmcChannelType", "(I)V");
  rmcInfo->updateJpegImgMid = env->GetMethodID(clazz, "updateJpegImg", "([BI)V");
  rmcInfo->updateTalkbackDataMid = env->GetMethodID(clazz, "updateTalkbackData", "([BI)V");

//  jfieldID p2pModeLocal_fid, p2pModeRemote_fid, p2pModeRelay_fid;
//  p2pModeLocal_fid = env->GetStaticFieldID(clazz, "P2P_MODE_LOCAL", "I");
//  p2pModeRemote_fid = env->GetStaticFieldID(clazz, "P2P_MODE_REMOTE", "I");
//  p2pModeRelay_fid = env->GetStaticFieldID(clazz, "P2P_MODE_RELAY", "I");
//  const int p2pModeLocal = env->GetStaticIntField(clazz, p2pModeLocal_fid);
//  const int p2pModeRemote = env->GetStaticIntField(clazz, p2pModeRemote_fid);
//  const int p2pModeRelay = env->GetStaticIntField(clazz, p2pModeRelay_fid);

  jmethodID getRmcVersionMid, getSockFdMid, getDestIpMid, getDestPortMid, getServerIpMid, getServerPortMid, getAesKeyMid, getCameraMacMid, getRandomNumberMid;
  jmethodID getRmcChannelTypeMid, setRmcChannelTypeMid, getRmcSessionTypeMid;
  jmethodID isAlwaysSendAccessStreamMid;

  getRmcChannelTypeMid = env->GetMethodID(clazz, "getRmcChannelType", "()I");
  setRmcChannelTypeMid = env->GetMethodID(clazz, "setRmcChannelType", "(I)V");
  getRmcSessionTypeMid = env->GetMethodID(clazz, "getRmcSessionType", "()I");

  getRmcVersionMid = env->GetMethodID(clazz, "getRmcVersion", "()I");
  getSockFdMid = env->GetMethodID(clazz, "getSockFd", "()I");
  getDestIpMid = env->GetMethodID(clazz, "getDestIp", "()Ljava/lang/String;");
  getDestPortMid = env->GetMethodID(clazz, "getDestPort", "()I");
  getServerIpMid = env->GetMethodID(clazz, "getServerIp", "()Ljava/lang/String;");
  getServerPortMid = env->GetMethodID(clazz, "getServerPort", "()I");

  getRandomNumberMid = env->GetMethodID(clazz, "getRandomNumber", "()Ljava/lang/String;");
  getCameraMacMid = env->GetMethodID(clazz, "getCameraMac", "()Ljava/lang/String;");
  getAesKeyMid = env->GetMethodID(clazz, "getAesKey", "()Ljava/lang/String;");
  isAlwaysSendAccessStreamMid = env->GetMethodID(clazz, "isAlwaysSendAccessStream", "()Z");

  rmcInfo->rmcVersion = env->CallIntMethod(thiz, getRmcVersionMid);
  rmcInfo->rmcChannelType = env->CallIntMethod(thiz, getRmcChannelTypeMid);
  rmcInfo->rmcSessionType = env->CallIntMethod(thiz, getRmcSessionTypeMid);
  log_info("channel type %d, session type %d\n", rmcInfo->rmcChannelType, rmcInfo->rmcSessionType);
  rmcInfo->sockFd = env->CallIntMethod(thiz, getSockFdMid);
  rmcInfo->destPort = env->CallIntMethod(thiz, getDestPortMid);
  log_info("destPort %d\n", rmcInfo->destPort);
  rmcInfo->serverPort = env->CallIntMethod(thiz, getServerPortMid);
  log_info("serverPort %d\n", rmcInfo->serverPort);

  jstring jdestIp = (jstring) env->CallObjectMethod(thiz, getDestIpMid);
  const char *destIp = env->GetStringUTFChars(jdestIp, NULL);
  log_info("destIp %s\n", destIp);
  rmcInfo->destIp = (char *) malloc(strlen(destIp) + 1);
  memset(rmcInfo->destIp, 0, sizeof(rmcInfo->destIp));
  strcpy(rmcInfo->destIp, destIp);
  env->ReleaseStringUTFChars(jdestIp, destIp);

  jstring jserverIp = (jstring) env->CallObjectMethod(thiz, getServerIpMid);
  const char *serverIp = env->GetStringUTFChars(jserverIp, NULL);
  log_info("serverIp %s\n", serverIp);
  rmcInfo->serverIp = (char *) malloc(strlen(serverIp) + 1);
  memset(rmcInfo->serverIp, 0, sizeof(rmcInfo->serverIp));
  strcpy(rmcInfo->serverIp, serverIp);
  env->ReleaseStringUTFChars(jserverIp, serverIp);

  jstring jrandomNumber = (jstring) env->CallObjectMethod(thiz, getRandomNumberMid);
  const char *randomNumber = env->GetStringUTFChars(jrandomNumber, NULL);
  log_info("randomNumber %s\n", randomNumber);
  rmcInfo->randomNumber = (char *) malloc(strlen(randomNumber) + 1);
  memset(rmcInfo->randomNumber, 0, sizeof(rmcInfo->randomNumber));
  strcpy(rmcInfo->randomNumber, randomNumber);
  env->ReleaseStringUTFChars(jrandomNumber, randomNumber);

  jboolean jalwaysSendAccessStream = env->CallBooleanMethod(thiz, isAlwaysSendAccessStreamMid);
  rmcInfo->alwaysSendAccessStream = jalwaysSendAccessStream ? 1 : 0;
  log_info("alwaysSendAccessStream %d\n", rmcInfo->alwaysSendAccessStream);

  jstring jcameraMac = (jstring) env->CallObjectMethod(thiz, getCameraMacMid);
  const char *cameraMac = env->GetStringUTFChars(jcameraMac, NULL);
  log_info("cameraMac %s\n", cameraMac);
  rmcInfo->cameraMac = (char *) malloc(strlen(cameraMac) + 1);
  memset(rmcInfo->cameraMac, 0, sizeof(rmcInfo->cameraMac));
  strcpy(rmcInfo->cameraMac, cameraMac);
  env->ReleaseStringUTFChars(jcameraMac, cameraMac);

  jstring jaesKey = (jstring) env->CallObjectMethod(thiz, getAesKeyMid);
  const char *aesKey = env->GetStringUTFChars(jaesKey, NULL);
  log_info("aesKey %s\n", aesKey);
  rmcInfo->aesKey = (char *) malloc(strlen(aesKey) + 1);
  memset(rmcInfo->aesKey, 0, sizeof(rmcInfo->aesKey));
  strcpy(rmcInfo->aesKey, aesKey);
  env->ReleaseStringUTFChars(jaesKey, aesKey);

  ret = rmcChannelInitSession(rmcInfo);
  log_info("Initilizing RMC channel...DONE ret %d\n", ret);

  if (ret >= 0) {
	  isSuccess = true;
  }
  env->DeleteLocalRef(clazz);
  return isSuccess;
}

static jboolean com_nxcomm_jstun_android_RmcChannel_start(JNIEnv *env, jobject thiz)
{
  bool isSuccess = false;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  int ret = -1;
  log_info("Starting RMC channel...");
  rmcChannelDumpInfo(rmcChannelInfo);
  if (rmcChannelInfo != NULL) {
    ret = rmcChannelStartSession(rmcChannelInfo);
  } else {
    log_error("rmcChannelInfo is NULL");
  }

  if (ret >= 0) {
    isSuccess = true;
  }

  log_info("Starting RMC channel...DONE, isSuccess? %d", isSuccess?1:0);
  env->DeleteLocalRef(clazz);
  return isSuccess;
}

static jboolean com_nxcomm_jstun_android_RmcChannel_stop(JNIEnv *env, jobject thiz)
{
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo != NULL) {
    rmcChannelStopSession(rmcChannelInfo);
  }
  env->DeleteLocalRef(clazz);
}


static void com_nxcomm_jstun_android_RmcChannel_destroy(JNIEnv *env, jobject thiz)
{
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo != NULL)
  {
    //log_info("Delete rmc channel\n");
    env->DeleteGlobalRef(rmcChannelInfo->rmcChannelObj);
    rmcChannelInfo->rmcChannelObj = NULL;
    rmcChannelInfoFree(rmcChannelInfo);
    rmcChannelInfo = NULL;
    env->SetLongField(thiz, rmcChannelInfoPtrFid, (long)rmcChannelInfo);
    //log_info("Delete rmc channel done\n");
  }
  else
  {
    log_info("Delete rmc channel, rmc channel is NULL\n");
  }
  env->DeleteLocalRef(clazz);
}

static jboolean com_nxcomm_jstun_android_RmcChannel_isConnected(JNIEnv *env, jobject thiz)
{
  bool isConnected = false;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo != NULL)
  {
    isConnected = rmcChannelInfo->isConnected==1 ? true : false;
  }

  env->DeleteLocalRef(clazz);
  return isConnected;
}

static jboolean com_nxcomm_jstun_android_RmcChannel_isValid(JNIEnv *env, jobject thiz)
{
  bool isValid = false;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo != NULL)
  {
    isValid = rmcChannelInfo->isValid==1 ? true : false;
  }

  env->DeleteLocalRef(clazz);
  return isValid;
}

static void com_nxcomm_jstun_android_RmcChannel_native_updateRmcChannelType(JNIEnv *env, jobject thiz, jint rmcChannelType)
{
  bool isValid = false;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo != NULL)
  {
    rmcChannelInfo->rmcChannelType = rmcChannelType;
  }

  env->DeleteLocalRef(clazz);
}

static void com_nxcomm_jstun_android_RmcChannel_setFrameFilteringEnabled(JNIEnv *env, jobject thiz, jboolean isEnabled)
{
  jfieldID rmcChannelInfoPtrFid;
  jclass clazz = env->GetObjectClass(thiz);
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo == NULL)
  {
    jniThrowException(env, "java/lang/IllegalStateException", "RmcChannel is not initialized yet");
  }
  else
  {
    log_debug("setFrameFilteringEnabled isEnabled? %d\n", isEnabled ? 1 : 0);
    rmcChannelInfo->isFrameFilteringEnabled = isEnabled ? 1 : 0;
  }

  env->DeleteLocalRef(clazz);
}

static jstring
com_nxcomm_jstun_android_RmcChannel_sendCommand(JNIEnv *env, jobject thiz, jstring jrequest)
{
  const char *request = env->GetStringUTFChars(jrequest, NULL);
  if (request == NULL)
  {
    env->ReleaseStringUTFChars(jrequest, request);
    jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
    return NULL;
  }

  jstring jresponse = NULL;
  char *response = NULL;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo == NULL)
  {
    log_info("sendCommand error: rmcChannelInfo is NULL\n");
  } else {
    rmcChannelSendCmd(rmcChannelInfo, request, &response);
    log_info("sendCommand res: %s\n", response);
    env->ReleaseStringUTFChars(jrequest, request);
    jresponse = env->NewStringUTF(response);
  }
  env->DeleteLocalRef(clazz);
  return jresponse;
}

static void
com_nxcomm_jstun_android_RmcChannel_sendChangeModeCmd(JNIEnv *env, jobject thiz, jstring jrequest)
{
  const char *request = env->GetStringUTFChars(jrequest, NULL);
  if (request == NULL)
  {
    env->ReleaseStringUTFChars(jrequest, request);
    jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
    return;
  }

  jstring jresponse = NULL;
  char *response = NULL;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo == NULL) {
    log_info("sendChangeModeCmd error: rmcChannelInfo is NULL\n");
  } else {
    rmcChannelSendChangeModeCmd(rmcChannelInfo, request);
    env->ReleaseStringUTFChars(jrequest, request);
  }
  env->DeleteLocalRef(clazz);
}

static jint
com_nxcomm_jstun_android_RmcChannel_native_sendTalkbackData(JNIEnv *env, jobject thiz, jbyteArray jpcmData, jint jpcmDataOffset, jint jpcmDataLength)
{
  jint ret = -1;
  jclass clazz = env->GetObjectClass(thiz);
  jfieldID rmcChannelInfoPtrFid;
  rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
  if (rmcChannelInfo == NULL) {
    log_error("send talkback error: rmcChannelInfo is NULL\n");
  } else {
    RmcTalkbackContext *talkbackContext = rmcChannelInfo->rmcTalkbackContext;
    if (talkbackContext != NULL) {
      memset(talkbackContext->pcmBuf, 0, sizeof(talkbackContext->pcmBuf));
      env->GetByteArrayRegion(jpcmData, jpcmDataOffset, jpcmDataLength, (int8_t *) talkbackContext->pcmBuf);
      ret = rmcChannelSendTalkbackData(rmcChannelInfo, (void *) talkbackContext->pcmBuf, jpcmDataOffset, jpcmDataLength);
    } else {
      log_error("send talkback error: talkback context is NULL\n");
    }
  }
  env->DeleteLocalRef(clazz);
  return ret;
}

static jint
com_nxcomm_jstun_android_RmcChannel_native_sendPcmTalkbackData(JNIEnv *env, jobject thiz, jbyteArray jpcmData, jint jpcmDataOffset, jint jpcmDataLength)
{
    jint ret = -1;
    jclass clazz = env->GetObjectClass(thiz);
    jfieldID rmcChannelInfoPtrFid;
    rmcChannelInfoPtrFid = env->GetFieldID(clazz, "mRmcChannelInfoPtr", "J");
    RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) env->GetLongField(thiz, rmcChannelInfoPtrFid);
    if (rmcChannelInfo == NULL) {
        log_error("send talkback error: rmcChannelInfo is NULL\n");
    } else {
        RmcTalkbackContext *talkbackContext = rmcChannelInfo->rmcTalkbackContext;
        if (talkbackContext != NULL) {
            memset(talkbackContext->pcmBuf, 0, sizeof(talkbackContext->pcmBuf));
            env->GetByteArrayRegion(jpcmData, jpcmDataOffset, jpcmDataLength, (int8_t *) talkbackContext->pcmBuf);
            ret = rmcChannelSendPcmTalkbackData(rmcChannelInfo, (void *) talkbackContext->pcmBuf, jpcmDataOffset, jpcmDataLength);
        } else {
            log_error("send talkback error: talkback context is NULL\n");
        }
    }
    env->DeleteLocalRef(clazz);
    return ret;
}

static JNINativeMethod gMethods[] =
{
    {"init",         "()Z",(void *) com_nxcomm_jstun_android_RmcChannel_init},
    {"start",         "()Z",(void *) com_nxcomm_jstun_android_RmcChannel_start},
    {"stop",         "()Z",(void *) com_nxcomm_jstun_android_RmcChannel_stop},
    {"destroy",      "()V",(void *)  com_nxcomm_jstun_android_RmcChannel_destroy},
    {"isConnected",      "()Z",(void *)  com_nxcomm_jstun_android_RmcChannel_isConnected},
    {"isValid",      "()Z",(void *)  com_nxcomm_jstun_android_RmcChannel_isValid},
	{"native_updateRmcChannelType",      "(I)V",(void *)  com_nxcomm_jstun_android_RmcChannel_native_updateRmcChannelType},
    {"setFrameFilteringEnabled",      "(Z)V",(void *)  com_nxcomm_jstun_android_RmcChannel_setFrameFilteringEnabled},
    {"sendCommand",    "(Ljava/lang/String;)Ljava/lang/String;",  (void *)com_nxcomm_jstun_android_RmcChannel_sendCommand},
    {"sendChangeModeCmd",    "(Ljava/lang/String;)V",  (void *)com_nxcomm_jstun_android_RmcChannel_sendChangeModeCmd},
    {"native_sendTalkbackData",    "([BII)I",  (void *)com_nxcomm_jstun_android_RmcChannel_native_sendTalkbackData},
    {"native_sendPcmTalkbackData",    "([BII)I",  (void *)com_nxcomm_jstun_android_RmcChannel_native_sendPcmTalkbackData}
};

int register_android_rmc_client(JNIEnv *env) {
  return jniRegisterNativeMethods(env, kClassPathName, gMethods, sizeof(gMethods) / sizeof(gMethods[0]));
}
