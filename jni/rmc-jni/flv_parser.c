#include "rmc_inc/flv_parser.h"
#include "rmc_inc/mylog.h"

int flv_get_header_size(uint8_t *flvBuf, uint32_t flvBufSize)
{
	int header_offset = 0;
	int offset = 0;
	int header_size = 0;
	/* Read header. */
	/* Skip first 4 bytes */
	log_info("FLV header: %c %c %c %d", flvBuf[0], flvBuf[1], flvBuf[2], flvBuf[3]);
	offset += 4;

	/* Read flags */
	uint8_t flags = flvBuf[offset];
	offset++;
	log_debug("flags %d\n", flags);

	if (flags & 1)
	{
		log_info("FLV file has video\n");
	}

	if (flags & 4)
	{
		log_info("FLV file has audio\n");
	}

	header_offset = (flvBuf[offset] << 24 | flvBuf[offset+1] << 16 | flvBuf[offset+2] << 8 | flvBuf[offset+3]) & 0xFFFFFFFF;
//	log_info("Header offset %d\n", header_offset);
//	av_log(NULL, AV_LOG_INFO, "pkt type: %d, buf_size: %d\n", *pkt_type, *buf_size);
	return header_offset;
}

/**
 * Get next FLV tag size.
 * Return: Next FLV tag size of -1 if flvBuf doesn't contain whole tag data,
 */
int flv_get_next_tag_size(uint8_t *flv_buf, uint32_t flv_buf_size) {
	int full_tag_size = -1;
	int offset = 0;
	if (flv_buf_size >= 4) {
		int tag_type = flv_buf[offset];
		int tag_data_size = (flv_buf[offset+1] << 16 | flv_buf[offset+2] << 8 | flv_buf[offset+3]) & 0x00FFFFFF;
//		log_debug("Tag size %d", tag_data_size);
		if (flv_buf_size >= tag_data_size + FLV_TAG_HEADER_BYTE_COUNT) {
			full_tag_size = tag_data_size + FLV_TAG_HEADER_BYTE_COUNT;
		} else {
//			log_debug("Not enough FLV tag data buf size %d, tag data size %d", flv_buf_size, tag_data_size);
		}
	}
	return full_tag_size;
}
