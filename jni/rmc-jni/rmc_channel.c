/*
 * Copyright (C) 2015 NxComm Pte. Ltd.
 *
 * This unpublished material is proprietary to NxComm.
 * All rights reserved. The methods and
 * techniques described herein are considered trade secrets
 * and/or confidential. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of NxComm.
 */

/* FILE   : rmc_channel.c
 * AUTHOR : hoang
 * DATE   : Dec 09, 2015
 * DESC   : This is the implementation for header file rmc_channel.h
 * Contains some API that communicated with RMC channel.
 */
#include "rmc_inc/rmc_channel.h"
#include "rmc_inc/mylog.h"
#include "rmc_inc/flv_parser.h"
#include "jniUtils.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>

#define MAX_CONN 2
#define MAX_HANDSHAKE_BUF_SIZE 1024
#define HANDSHAKE_BUF_SIZE 96
#define MIN_HANDSHAKE_BUF_SIZE 50

#define MAX_CONN_TIMEOUT 10*1000000
#define MAX_ACCESS_STREAM_TIMEOUT 2*1000000
#define MAX_FILE_TRANSFER_TIMEOUT 10*1000000
#define MAX_FILE_TRANSFER_TIMEOUT_PS 10*1000000
#define MAX_COMMAND_TIMEOUT 5*1000000

#define MAX_HANDSHAKE_TIME 8*1000000
#define MAX_HANDSHAKE_SEND_TIME 1000000
static const int gConnInfoCount = 1;
static int sLogCounter = 0;

RmcChannelInfo* rmcChannelInfoAlloc()
{
  RmcChannelInfo *rmcInfo = (RmcChannelInfo *) malloc(sizeof(RmcChannelInfo));
  rmcInfo->interrupted = 0;
  rmcInfo->terminated = 0;
  rmcInfo->isConnected = 0;
  rmcInfo->isValid = 0;
  rmcInfo->isFrameFilteringEnabled = 1;
  rmcInfo->alwaysSendAccessStream = 0;
  rmcInfo->preAllocMem = (void *) malloc(RMCSizeGet());
  rmcInfo->rmcSess = NULL;
  rmcInfo->rmcCallback = NULL;
  rmcInfo->rmcBandwidthCallback = NULL;
  rmcInfo->rmcPrivData = NULL;
  rmcInfo->destIp = NULL;
  rmcInfo->destPort = -1;
  rmcInfo->serverIp = NULL;
  rmcInfo->serverPort = -1;
  rmcInfo->randomNumber = NULL;
  rmcInfo->cameraMac = NULL;
  rmcInfo->aesKey = NULL;
  rmcInfo->lastReceived = -1;
  rmcInfo->rmcChannelType = RMC_CHANNEL_TYPE_REMOTE;
  rmcInfo->rmcSessionType = RMC_SESSION_TYPE_LIVE_STREAMING;
  rmcInfo->updateRmcChannelTypeMid = NULL;
  rmcInfo->updateJpegImgMid = NULL;
  rmcInfo->updateTalkbackDataMid = NULL;
  rmcInfo->notifyRmcChannelHandshakeFailedMid = NULL;
  rmcInfo->notifyRmcChannelStatusMid = NULL;
  //pthread_mutex_init(&rmcInfo->sendCmdLock, NULL);
  pthread_mutex_init(&rmcInfo->rmcChanelLock, NULL);

  // init talkback context
  rmcInfo->rmcTalkbackContext = (RmcTalkbackContext*) malloc(sizeof(RmcTalkbackContext));
  rmcInfo->rmcTalkbackContext->currentTimestamp = 0;
  g722_enc_init(&rmcInfo->rmcTalkbackContext->g722EncContext);

  rmcInfo->rmcTransferFileContext = NULL;

  return rmcInfo;
}

int rmcChannelFileTransferInit(RmcChannelInfo *rmcChannelInfo) {
	log_info("Init RMC file transfer context...");
	int ret = -1;
	if (rmcChannelInfo != NULL) {
		RmcFileTransferContext *fileTransferContext = (RmcFileTransferContext*) malloc(sizeof(RmcFileTransferContext));
		if (fileTransferContext != NULL) {
			fileTransferContext->fileBuffLength = TRANSFER_FILE_LENGTH_MAX;
			memset(fileTransferContext->fileBuff, 0, fileTransferContext->fileBuffLength);
			fileTransferContext->fileBuffDownloaded = 0;
			fileTransferContext->fileBuffPrevDownloaded = 0;
			fileTransferContext->fileBuffConsumed = 0; // data consumed by the media player
			fileTransferContext->blockDownloaded = 0;
			fileTransferContext->blockExpected = 0;
			fileTransferContext->fileTransferCompleted = 0;
			rmcChannelInfo->rmcTransferFileContext = fileTransferContext;
			ret = RMCFileMemSet(rmcChannelInfo->rmcSess,
					rmcChannelInfo->rmcTransferFileContext->fileBuff,
					&rmcChannelInfo->rmcTransferFileContext->fileBuffLength,
					&rmcChannelInfo->rmcTransferFileContext->blockExpected,
					&rmcChannelInfo->rmcTransferFileContext->blockDownloaded,
					&rmcChannelInfo->rmcTransferFileContext->fileBuffDownloaded);
		} else {
			log_error("Can't alloc file transfer context OOM");
		}
	} else {
		log_error("Init RMC file transfer context failed, RMC channel is NULL");
	}
	return ret;
}

void rmcChannelInfoFree(RmcChannelInfo *rmcChannelInfo)
{
  if (rmcChannelInfo != NULL) {
    log_info("Free rmc channel info");
    pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);

    rmcChannelInfo->interrupted = 1;
    if (rmcChannelInfo->destIp != NULL) {
      free(rmcChannelInfo->destIp);
      rmcChannelInfo->destIp = NULL;
    }
    if (rmcChannelInfo->serverIp != NULL) {
      free(rmcChannelInfo->serverIp);
      rmcChannelInfo->serverIp = NULL;
    }
    if (rmcChannelInfo->randomNumber != NULL) {
      free(rmcChannelInfo->randomNumber);
      rmcChannelInfo->randomNumber = NULL;
    }
    if (rmcChannelInfo->cameraMac != NULL) {
      free(rmcChannelInfo->cameraMac);
      rmcChannelInfo->cameraMac = NULL;
    }
    if (rmcChannelInfo->aesKey != NULL) {
      free(rmcChannelInfo->aesKey);
      rmcChannelInfo->aesKey = NULL;
    }

    if (rmcChannelInfo->preAllocMem != NULL) {
      free(rmcChannelInfo->preAllocMem);
      rmcChannelInfo->preAllocMem = NULL;
    }

    if (rmcChannelInfo->rmcTalkbackContext != NULL) {
      g722_enc_deinit(&rmcChannelInfo->rmcTalkbackContext->g722EncContext);
      free(rmcChannelInfo->rmcTalkbackContext);
      rmcChannelInfo->rmcTalkbackContext = NULL;
    }

    if (rmcChannelInfo->rmcTransferFileContext != NULL) {
    	free(rmcChannelInfo->rmcTransferFileContext);
    	rmcChannelInfo->rmcTransferFileContext = NULL;
    }

    pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
    //pthread_mutex_destroy(&rmcChannelInfo->sendCmdLock);
    pthread_mutex_destroy(&rmcChannelInfo->rmcChanelLock);

    free(rmcChannelInfo);
    log_info("Free rmc channel info...DONE");
  }
}

void rmcChannelInfoRegisterCallback(RmcChannelInfo *rmcInfo, void *privData, MediaFrameDataHdl callback, RmcBandwidthCallback bandwidthCallback) {
  //log_info("rmcChannelInfo register callback");
  if (rmcInfo != NULL) {
    rmcInfo->rmcPrivData = privData;
    rmcInfo->rmcCallback = (void *) callback;
    rmcInfo->rmcBandwidthCallback = (void *) bandwidthCallback;
  }
}

void rmcChannelInfoUnregisterCallback(RmcChannelInfo *rmcInfo) {
  log_info("rmcChannelInfo unregister callback");
  if (rmcInfo != NULL) {
    rmcInfo->rmcPrivData = NULL;
    rmcInfo->rmcCallback = NULL;
    rmcInfo->rmcBandwidthCallback = NULL;
  }
}

void rmcChannelSendRelayCmd(RmcChannelInfo *rmcChannelInfo, eMediaSubType cmdType)
{
	log_info("rmcChannelSendRelayCmd: type: %d, mac: %s, randomNumber %s.\n", cmdType, rmcChannelInfo->cameraMac, rmcChannelInfo->randomNumber);
  if (rmcChannelInfo != NULL)
  {
    if (cmdType == eMediaSubTypeCommandAccessStream)
    {
      int i;
      for (i=0; i<8; i++)
      {
        RMCRelayRequestSend(rmcChannelInfo->rmcSess, cmdType,
            rmcChannelInfo->cameraMac,
            rmcChannelInfo->randomNumber);
        //log_info("Send relay request: count: %d", i);
        usleep(100000);
      }
    }
    else
    {
      RMCRelayRequestSend(rmcChannelInfo->rmcSess, cmdType,
          rmcChannelInfo->cameraMac,
          rmcChannelInfo->randomNumber);
      //usleep(100000);
    }
    //RMCEncryptionSet(connInfo->sess, 0, (uint8_t *)connInfo->enc_key);
  }
  else
  {
    log_error("rmcChannelSendRelayCmd: connection info is NULL\n");
  }
}

void rmcChannelDirectPunching(RmcChannelInfo *rmcChannelInfo)
{
	log_info("rmcChannelDirectPunching");
	if (rmcChannelInfo != NULL) {
		int i;
		for (i=0; i<8; i++) {
			RMCRelayRequestSend(rmcChannelInfo->rmcSess, eMediaSubTypeCommandAccessStream,
					rmcChannelInfo->cameraMac,
					rmcChannelInfo->randomNumber);
			usleep(100000);
		}
	}
}

void rmcChannelSendChangeModeCmd(RmcChannelInfo *rmcChannelInfo, const char *request)
{
	log_info("rmcChannelSendChangModeCmd, cmd: %s\n", request);
	if (rmcChannelInfo == NULL)
	{
		log_error("send change mode cmd err, RMC channel has stopped -> exit now\n");
	}
	else
	{
		tCmdResponse *res = NULL;
		if (rmcChannelInfo->interrupted == 1)
		{
			log_info("send change mode cmd err, RMC channel has stopped\n");
		}
		else
		{
			if (rmcChannelInfo->rmcSess == NULL)
			{
				log_error("send change mode cmd err, RMC session is NULL\n");
			}
			else
			{
				int count = 8;
				while (--count > 0 && rmcChannelInfo->interrupted == 0 && rmcChannelInfo->rmcSess != NULL) {
					pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
					int commandID = -1;
					if (rmcChannelInfo->interrupted == 0 && rmcChannelInfo->rmcSess != NULL) {
						RMCRelayRequestSend(rmcChannelInfo->rmcSess, eMediaSubTypeCommandAccessStream,
								rmcChannelInfo->cameraMac,
								rmcChannelInfo->randomNumber);
						tCmdResponseBuffer req;
						req.data = (void *)request;
						req.size = strlen(request);
						commandID = RMCRxSideCmdReqSend(rmcChannelInfo->rmcSess, &req);
//						if(commandID >= 0)
//						{
//							log_debug("Send change mode cmd success, cmd id %d\n", commandID);
//						} else {
//							log_error("Send change mode cmd failed, cmd id %d\n", commandID);
//						}
					} else {
						log_error("Send change mode cmd req failed, rmcSess is interrupted\n");
					}
					pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);

					usleep(100000);
				}
			} //else cmd_idx != -1
		} //else isRunning == true
	}
}


void rmcChannelDumpInfo(RmcChannelInfo *rmcChannelInfo) {
  if (rmcChannelInfo == NULL) {
    log_info("RMC channel is NULL");
  } else {
    log_info("RMC channel: session type %s, channel type %d, fd %d, destIp %s, destPort %d, serverIp %s, serverPort %d, randomNumber %s, cameraMac %s, aesKey %s, always send access stream? %d",
        (rmcChannelInfo->rmcSessionType==0) ? "LiveStreaming" : "FileTransfer",
        rmcChannelInfo->rmcChannelType, rmcChannelInfo->sockFd, rmcChannelInfo->destIp, rmcChannelInfo->destPort,
        rmcChannelInfo->serverIp, rmcChannelInfo->serverPort, rmcChannelInfo->randomNumber,
        rmcChannelInfo->cameraMac, rmcChannelInfo->aesKey, rmcChannelInfo->alwaysSendAccessStream);
  }
}


int rmcChannelConnectToDestAddr(RmcChannelInfo *rmcChannelInfo)
{
  int ret = -1;
  struct sockaddr_in addr;
  struct in_addr destAddr;
  if (rmcChannelInfo != NULL) {
    if (rmcChannelInfo->destIp != NULL)
    {
      /* Connect to destination address */
      if(inet_aton(rmcChannelInfo->destIp, &destAddr) == 0)
      {
        log_error("(%s:%d) Address %s is invalid\n", __FILE__, __LINE__, rmcChannelInfo->destIp);
      }
      else
      {
        addr.sin_family =  AF_INET;
        addr.sin_addr = destAddr;
        addr.sin_port = htons(rmcChannelInfo->destPort);
        if(connect(rmcChannelInfo->sockFd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
          log_error("(%s:%d) Error when connecting socket %d, destIp %s, destPort %d\n", __FILE__, __LINE__,
              rmcChannelInfo->sockFd, rmcChannelInfo->destIp, rmcChannelInfo->destPort);
        }
        else
        {
          ret = 1;
        }
      }
    }
    else
    {
      log_error("(%s:%d) Dest ip %d is invalid\n", __FILE__, __LINE__, rmcChannelInfo->destIp);
    }
  } else {
    log_error("(%s:%d) rmcChannelInfo is NULL\n", __FILE__, __LINE__);
  }

  return ret;
}


int rmcChannelConnectToServerAddr(RmcChannelInfo *rmcChannelInfo)
{
  int ret = -1;
  struct sockaddr_in addr;
  struct in_addr destAddr;
  if (rmcChannelInfo != NULL) {
    if (rmcChannelInfo->serverIp != NULL)
    {
      /* Connect to destination address */
      if(inet_aton(rmcChannelInfo->serverIp, &destAddr) == 0)
      {
        log_error("(%s:%d) Address %s is invalid\n", __FILE__, __LINE__, rmcChannelInfo->serverIp);
      }
      else
      {
        addr.sin_family =  AF_INET;
        addr.sin_addr = destAddr;
        addr.sin_port = htons(rmcChannelInfo->serverPort);
        if(connect(rmcChannelInfo->sockFd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
          log_error("(%s:%d) Error when connecting socket %d, serverIp %s, serverPort %d\n", __FILE__, __LINE__,
              rmcChannelInfo->sockFd, rmcChannelInfo->serverIp, rmcChannelInfo->serverPort);
        }
        else
        {
          ret = 1;
        }
      }
    }
    else
    {
      log_error("(%s:%d) Dest ip %d is invalid\n", __FILE__, __LINE__, rmcChannelInfo->serverIp);
    }
  } else {
    log_error("(%s:%d) rmcChannelInfo is NULL\n", __FILE__, __LINE__);
  }

  return ret;
}


int rmcChannelRemoteHandshake(RmcChannelInfo *rmcChannelInfo)
{
  int ret_idx = -1;
  log_error("Try multi handshake\n");
  struct sockaddr_in  addr;
  struct in_addr destAddr;
  int i, j;

  if (rmcChannelInfo->interrupted == 1)
  {
    log_error("%s: interrupt flags is ON, exit thread now");
    return ret_idx;
  }

  if (rmcChannelInfo != NULL)
  {
    // rmcChannelInfo->isConnected = rmcChannelConnectToDestAddr(rmcChannelInfo);
    log_debug("Connection info is connected? %d\n", rmcChannelInfo->isConnected);
  }
  else
  {
    log_error("Connection info %d is NULL\n", i);
  }

  int ret = -1;
  int handshakeRecv = -1;
  time_t t;
  srand((unsigned) time(&t));
  int ran_no;
  char sendBuf[MAX_CONN][MAX_HANDSHAKE_BUF_SIZE];
  char recvBuf[MAX_CONN][MAX_HANDSHAKE_BUF_SIZE];

  /* Prepare send & receive buffers. */
  for (i = 0; i < gConnInfoCount; i++)
  {
    memset(sendBuf[i], 0, sizeof(sendBuf[i]));
    sprintf(sendBuf[i], "%s", "HIHI");
    memset(recvBuf[i], 0, sizeof(recvBuf[i]));
  }

  int tmpNum = -1;
  int size = -1;
  struct timeval pTime0, pTime1;
  double time0 = -1;
  double time1 = -1;
  gettimeofday(&pTime0, NULL);
  time0 = pTime0.tv_sec * 1000000.0 + pTime0.tv_usec;
  while (ret_idx == -1 && (time1 - time0 < MAX_HANDSHAKE_TIME) && rmcChannelInfo->interrupted == 0)
  {
    gettimeofday(&pTime1, NULL);
    time1 = pTime1.tv_sec * 1000000.0 + pTime1.tv_usec;
    //log_info("rmcChannelRemoteHandshake: rmc channel interrupt flag %d\n", rmcChannelInfo->interrupted);
    if (rmcChannelInfo->interrupted == 0)
    {
      for (i=0; i<gConnInfoCount; i++)
      {
        if (rmcChannelInfo->isConnected == 1)
        {
          log_info("Sending handshake info to fd %d\n", rmcChannelInfo->sockFd);
          for (j=0; j<10; j++)
          {
            ran_no = rand() % (MAX_HANDSHAKE_BUF_SIZE - MIN_HANDSHAKE_BUF_SIZE);
            size = send(rmcChannelInfo->sockFd, sendBuf[i], MIN_HANDSHAKE_BUF_SIZE + ran_no, 0);
            log_info("Sending handshake info %s from fd %d, ret %d\n",
                sendBuf[i], rmcChannelInfo->sockFd, size);
            if(size < MIN_HANDSHAKE_BUF_SIZE)
            {
              //log_info("Sending handshake info failed");
            }
          }
        }
      } //end for loop send
    }

    if (rmcChannelInfo->interrupted == 0)
    {
      for (i=0; i<gConnInfoCount; i++)
      {
        if (rmcChannelInfo->isConnected == 1)
        {
          log_info("Receiving handshake info from fd %d\n", rmcChannelInfo->sockFd);
          size = recv(rmcChannelInfo->sockFd, recvBuf[i], sizeof(recvBuf[i]), 0);
          log_info("Receiving handshake info %s from fd %d ret %d\n", recvBuf[i], rmcChannelInfo->sockFd, size);
          if(size < MIN_HANDSHAKE_BUF_SIZE)
          {
            //__android_log_print(ANDROID_LOG_INFO, TAG, "Receiving handshake info failed");
          }

          if (recvBuf[i][0] == 'V' && recvBuf[i][1] == 'L' &&
              recvBuf[i][2] == 'V' && recvBuf[i][3] == 'L')
          {
            //hand shake OK
            ret_idx = i;
            log_info("---HANDSHAKE PORT %d OK---\n", rmcChannelInfo->destPort);
            break;
          }
        }
      } //end for loop receive
    }
  } //end while loop max handshake timeout

  return ret_idx;
}

static void* rmcTimerTask(void *ptr)
{
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach VM to rmc timer thread failed");
    return;
  }
#endif
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
  if (rmcChannelInfo != NULL)
  {
      struct timeval pTime;
      double time_now = -1;
      int timeout = 0;

      while (rmcChannelInfo->interrupted == 0)
      {
        gettimeofday(&pTime, NULL);
        time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
        if (rmcChannelInfo->lastReceived != -1 && (time_now - rmcChannelInfo->lastReceived > MAX_CONN_TIMEOUT))
        {
          log_error("RMC channel received timeout, time_now %f, lastReceived %f, time %f.\n",
              time_now, rmcChannelInfo->lastReceived, (time_now - rmcChannelInfo->lastReceived));
          timeout = 1;
        }

        if (timeout == 1)
        {
          if (rmcChannelInfo != NULL)
          {
            // Suspend media player as soon as possible to avoid wrong state
            log_error("Interrupt RMC channel due to timeout");
            rmcChannelInfo->interrupted = 1;
            // notify disconnected event
            rmcChannelNotifyStatus(rmcChannelInfo, 0);
            break;
          }
        }

        // Update RMC bandwidth
        int rmcBandwidth = rmcChannelGetBandwidth(rmcChannelInfo);
        if (rmcChannelInfo->rmcBandwidthCallback != NULL) {
        	((RmcBandwidthCallback)rmcChannelInfo->rmcBandwidthCallback)(rmcChannelInfo->rmcPrivData, rmcBandwidth);
        }

        /*
         * Need to split sleep time into smaller sleep
         */
        int count = 10;
        while (count-- > 0 && rmcChannelInfo->interrupted == 0)
        {
          usleep(100000);
        }
      }
  }
#if ANDROID_BUILD
  log_info("Dettach thread rmc timer thread from VM...\n");
  dettachVmFromThread();
#endif
}

//-------------------------------
static void* rmcAccessStreamTimerTask(void *ptr)
{
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach VM to access stream timer thread failed");
    return;
  }
#endif
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
  if (rmcChannelInfo != NULL)
  {
      struct timeval pTime;
      double time_now = -1;
      int timeout = 0;
      int interrupted = 0;
      int timeoutCount = 1;


      gettimeofday(&pTime, NULL);
      double lastReceived = pTime.tv_sec*1000000.0 + pTime.tv_usec;
      while (interrupted == 0)
      {
        gettimeofday(&pTime, NULL);
        time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;

//        log_error("Access stream check timeout, time_now %f, lastReceived %f, time %f, threshold %d, timeoutCount %d.\n",
//        		time_now, lastReceived, (time_now - lastReceived), (MAX_ACCESS_STREAM_TIMEOUT * timeoutCount), timeoutCount);

        if (time_now - lastReceived > MAX_ACCESS_STREAM_TIMEOUT * timeoutCount)
        {
          log_info("Access stream received timeout, count: %d, time_now %f, lastReceived %f, time %f.\n",
              timeoutCount, time_now, lastReceived, (time_now - lastReceived));
          timeout = 1;
          timeoutCount++;
          //log_error("Access stream timeoutCount %d", timeoutCount);
        }

        //log_info("Access stream check timeout %d", timeout);
        if (timeout == 1)
        {
          if (rmcChannelInfo != NULL)
          {
            log_info("Sending stream access request...\n");
			rmcChannelSendRelayCmd(rmcChannelInfo, eMediaSubTypeCommandAccessStream);
			log_info("Sending stream access request...DONE\n");
			if(timeoutCount > 2){
				interrupted = 1;
				timeoutCount = 1;
				//log_error("Access stream break timeoutCount %d", timeoutCount);
				break;
			}
          }
          timeout = 0;
          //log_info("Access stream set timeout %d", timeout);
        }

        /*
         * Need to split sleep time into smaller sleep
         */
        int count = 10;
        while (count-- > 0 && rmcChannelInfo->interrupted == 0)
        {
          usleep(100000);
        }
      }
  }
#if ANDROID_BUILD
  log_info("Dettach thread access stream timer thread from VM...\n");
  dettachVmFromThread();
#endif
}

static void* rmcChannelFileTransferTimerTask(void *ptr) {
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach VM to file transfer timer thread failed");
    return;
  }
#endif

  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
  if (rmcChannelInfo != NULL)
  {
	  struct timeval pTime;
	  double time_now = -1;
	  int completed = 0;
	  int timeout = 0;
	  double maxFileTransferTimeout = MAX_FILE_TRANSFER_TIMEOUT;
//	  if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_RELAY) {
//		  log_debug("File transfer via P2P relay, extend timeout");
//		  maxFileTransferTimeout = MAX_FILE_TRANSFER_TIMEOUT_PS;
//	  } else {
//		  maxFileTransferTimeout = MAX_FILE_TRANSFER_TIMEOUT;
//	  }
	  while (rmcChannelInfo->interrupted == 0) {
		  gettimeofday(&pTime, NULL);
		  time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;

		  if (rmcChannelInfo->lastReceived != -1 && (time_now - rmcChannelInfo->lastReceived > maxFileTransferTimeout))
		  {
			  log_error("RMC channel file transfer session timeout, time_now %f, lastReceived %f, time %f.\n",
					  time_now, rmcChannelInfo->lastReceived, (time_now - rmcChannelInfo->lastReceived));
			  timeout = 1;
		  }

		  if (timeout == 1)
		  {
			  if (rmcChannelInfo != NULL)
			  {
				  // Suspend media player as soon as possible to avoid wrong state
				  log_error("Interrupt file transfer session due to timeout");
				  rmcChannelInfo->interrupted = 1;
				  // notify disconnected event
				  rmcChannelNotifyStatus(rmcChannelInfo, 0);
			  }
			  break;
		  }

		  RmcFileTransferContext *rmcFileTransferCtx = rmcChannelInfo->rmcTransferFileContext;
		  if (rmcFileTransferCtx != NULL) {
			  int downloaded = rmcFileTransferCtx->fileBuffDownloaded;
			  int blockDownloaded = rmcFileTransferCtx->blockDownloaded;
			  int blockExpected = rmcFileTransferCtx->blockExpected;
//			  log_debug("RMC file transfer timer thread is running: downloaded %d, prev_downloaded %d, blockDownloaded %d, blockExpected %d",
//					  downloaded, rmcFileTransferCtx->fileBuffPrevDownloaded, blockDownloaded, blockExpected);

			  if (downloaded > rmcFileTransferCtx->fileBuffPrevDownloaded) {
				  // Checking first data received
				  if (rmcFileTransferCtx->fileBuffPrevDownloaded == 0) {
					  if (rmcChannelInfo->isValid == 0) {
						  log_info("RMC file transfer received first data...");
						  rmcChannelInfo->isValid = 1;
						  rmcChannelNotifyStatus(rmcChannelInfo, 1);
					  }
				  }

				  // download new data, reset the timestamp
				  rmcChannelInfo->lastReceived = time_now;
				  rmcFileTransferCtx->fileBuffPrevDownloaded = downloaded;
			  }

			  if (blockExpected > 0 && blockExpected == blockDownloaded) {
				  log_info("RMC file transfer completed");
				  rmcFileTransferCtx->fileTransferCompleted = 1;
				  break;
			  }
		  } else {
			  log_debug("RMC file transfer timer thread is initializing...");
		  }


		  /*
		   * Need to split sleep time into smaller sleep
		   */
		  int count = 10;
		  while (count-- > 0 && rmcChannelInfo->interrupted == 0)
		  {
			  usleep(100000);
		  }
	  }
  }

#if ANDROID_BUILD
  log_info("Dettach file transfer timer thread from VM...");
  dettachVmFromThread();
#endif
}

/**
 * Polling and reading data from file transfer buffer.
 */
static void* rmcChannelFileTransferDataTask(void *ptr) {
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach VM to file transfer data thread failed");
    return;
  }
#endif

  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
  if (rmcChannelInfo != NULL)
  {
	  int completed = 0;
	  while (rmcChannelInfo->interrupted == 0) {
		  RmcFileTransferContext *rmcFileTransferCtx = rmcChannelInfo->rmcTransferFileContext;
		  if (rmcFileTransferCtx != NULL) {
			  int consumed = rmcFileTransferCtx->fileBuffConsumed;
			  int downloaded = rmcFileTransferCtx->fileBuffDownloaded;
			  int blockDownloaded = rmcFileTransferCtx->blockDownloaded;
			  int blockExpected = rmcFileTransferCtx->blockExpected;
//			  log_debug("RMC file transfer timer thread is running: consumed %d, downloaded %d, prev_downloaded %d, blockDownloaded %d, blockExpected %d",
//					  consumed, downloaded, rmcFileTransferCtx->fileBuffPrevDownloaded, blockDownloaded, blockExpected);
			  if (consumed + FLV_PREV_TAG_BYTE_COUNT < downloaded) {
				  /*
				   * Do it for play back and download in parallel
				   * Parse FLV tag from buffer and pass it to player
				   */
				  int tag_size;
				  do {
					  tag_size = -1;
					  // skip FLV header
					  if (consumed == 0) {
						  int headerSize = flv_get_header_size(rmcFileTransferCtx->fileBuff, rmcFileTransferCtx->fileBuffDownloaded);
						  if (headerSize > 0) {
							  consumed += headerSize;
						  }
					  }

					  // PREVIOUS TAG SIZE (4) + FLV_TAG
					  consumed += FLV_PREV_TAG_BYTE_COUNT;
					  int tag_size = flv_get_next_tag_size(rmcFileTransferCtx->fileBuff + consumed, downloaded - consumed);
//					  log_debug("Tag size %d", tag_size);
					  if (tag_size > 0) {
//						  int remainingTagSize = FLVPacketParse(rmcFileTransferCtx->fileBuff + consumed, tag_size, p2pCtx);
						  // Pass data to player via callback
						  if (rmcChannelInfo->rmcCallback != NULL) {
							  ((MediaFrameDataHdl)rmcChannelInfo->rmcCallback)(rmcFileTransferCtx->fileBuff + consumed, tag_size, 29, -1, rmcChannelInfo->rmcPrivData);
						  }
						  consumed += tag_size;
					  } else {
						  consumed -= FLV_PREV_TAG_BYTE_COUNT;
					  }

					  if (consumed > rmcFileTransferCtx->fileBuffConsumed) {
						  rmcFileTransferCtx->fileBuffConsumed = consumed;
					  }
				  } while (tag_size > 0 && rmcChannelInfo->interrupted == 0);
			  } else {
				  usleep(20000);
			  }

			  if (blockExpected > 0 && blockExpected == blockDownloaded &&
					  consumed + FLV_PREV_TAG_BYTE_COUNT == downloaded) {
				  log_info("All FLV data have been extracted, stop file transfer data thread");
				  completed = 1;
			  }
		  } else {
			  log_debug("RMC file transfer data thread is initializing...");
		  }

		  if (completed == 1) {
			  log_info("RMC file transfer data task completed");
			  break;
		  }
	  }
  }

#if ANDROID_BUILD
  log_info("Dettach file transfer data thread from VM...");
  dettachVmFromThread();
#endif
}

static void rmcChannelReceiver(void* data, uint32_t size, eMediaSubType type, uint32_t ts, void* priv)
{
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) priv;
  if (rmcChannelInfo != NULL)
  {
//    log_debug("Receive media type %d, size %d, ts %ld from channel %d\n", type, size, ts, rmcChannelInfo->sockFd);
    if (rmcChannelInfo->interrupted == 0)
    {
      if (rmcChannelInfo->isValid == 0) {
        rmcChannelInfo->isValid = 1;
        // notify channel connected event
        if (rmcChannelInfo->notifyRmcChannelStatusMid != NULL) {
          rmcChannelNotifyStatus(rmcChannelInfo, 1);
        }
      }

      struct timeval pTime;
      gettimeofday(&pTime, NULL);
      rmcChannelInfo->lastReceived = pTime.tv_sec*1000000.0 + pTime.tv_usec;

      if (type == eMediaSubTypeVideoJPEG) {
        rmcChannelUpdateJpegImg(rmcChannelInfo, data, size);
      } else if (type == 24) {
        rmcChannelUpdateTalkbackData(rmcChannelInfo, data, size);
      } else {
        if (rmcChannelInfo->rmcCallback != NULL) {
          ((MediaFrameDataHdl)rmcChannelInfo->rmcCallback)(data, size, type, ts, rmcChannelInfo->rmcPrivData);
        }
      }
    }
  } else {
    log_error("rmcChannelReceiver, rmc channel info is NULL\n");
  }
}


static void* rmcChannelWorkerThread(void *ptr) {
#if ANDROID_BUILD
  if(attachVmToThread () != JNI_OK)
  {
    log_error("Attach VM to rmc video thread failed");
    return;
  } else {
	  log_info("Attached VM to rmc video thread done");
  }
#endif
  RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
  if (rmcChannelInfo == NULL) {
    log_error("rmc worker, rmcChannelInfo is NULL\n");
  } else {
    log_info("Starting RMC channel thread");
    if (rmcChannelInfo->interrupted == 0) {
      RMCRXSideRecv(rmcChannelInfo->rmcSess, rmcChannelReceiver);
    } else {
      log_info("Starting RMC channel thread failed, rmcSess is interrupted");
    }
  }

#if ANDROID_BUILD
  log_info("Dettach thread rmc video thread from VM...\n");
  dettachVmFromThread();
#endif
}

static void* rmcChannelFileTransferTask(void *ptr) {
	log_info("Start RMC channel main file transfer task");
	RmcChannelInfo *rmcChannelInfo = (RmcChannelInfo *) ptr;
	if (rmcChannelInfo->interrupted == 0) {
		RMCRXSideRecv(rmcChannelInfo->rmcSess, NULL);
		log_info("RMC channel main file transfer task complete");
	} else {
		log_error("Start RMC channel main file transfer task failed, rmcSess is interrupted");
	}
}

int rmcChannelInitSession(RmcChannelInfo *rmcChannelInfo)
{
  int ret = -1;
  if (rmcChannelInfo == NULL) {
    log_info("rmcChannelInitSession, rmcChannelInfo %is NULL\n");
    return ret;
  }

  JNIEnv *env = getJNIEnv();
  if (rmcChannelInfo->interrupted == 0)
  {
    log_info("Initializing rmc channel, library version %s, type %d\n", RMCVersionGet(NULL), rmcChannelInfo->rmcChannelType);
    switch (rmcChannelInfo->rmcChannelType) {
    case RMC_CHANNEL_TYPE_LOCAL:
      ret = rmcChannelConnectToDestAddr(rmcChannelInfo);
      if (ret < 0) {
        log_error("Connect to local addr failed, ret %d -> try connect via relay", ret);
      }
      break;
    case RMC_CHANNEL_TYPE_REMOTE:
      ret = rmcChannelConnectToDestAddr(rmcChannelInfo);
      if (ret < 0) {
        log_error("Connect to remote addr failed, ret %d", ret);
      } else {
        rmcChannelInfo->isConnected = 1;
//        ret = rmcChannelRemoteHandshake(rmcChannelInfo);
      }

      if (rmcChannelInfo != NULL && rmcChannelInfo->interrupted == 0) {
        if (ret < 0) {
          // Remote handshake failed, notify this event
          if (rmcChannelInfo->rmcChannelObj != NULL && rmcChannelInfo->notifyRmcChannelHandshakeFailedMid != NULL) {
            (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->notifyRmcChannelHandshakeFailedMid);
          }

//          rmcChannelInfo->rmcChannelType = RMC_CHANNEL_TYPE_RELAY;
//          if (rmcChannelInfo->rmcChannelObj != NULL) {
//            (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->updateRmcChannelTypeMid, rmcChannelInfo->rmcChannelType);
//          }
//          ret = rmcChannelConnectToServerAddr(rmcChannelInfo);
//          if (ret < 0) {
//            log_error("Connect to server addr failed, ret %d", ret);
//          } else {
//            // Update rmc channel type to Java code here
//            log_debug("Connect to server addr success, ret %d -> update rmc channel type to Java code\n", ret);
//          }
        } else {
//          log_debug("Remote handshake success, ret %d -> update rmc channel type to Java code\n", ret);
//          if (rmcChannelInfo->rmcChannelObj != NULL) {
//            (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->updateRmcChannelTypeMid, rmcChannelInfo->rmcChannelType);
//          }
        }
      } else {
        log_debug("rmcChannel is interrupted, exit");
      }
      break;
    case RMC_CHANNEL_TYPE_RELAY:
      ret = rmcChannelConnectToServerAddr(rmcChannelInfo);
      if (ret < 0) {
        log_error("Connect to server addr failed, ret %d", ret);
      }
      break;
    }

    if (rmcChannelInfo != NULL) {
      rmcChannelInfo->isConnected = ret;
    }
  } else {
    log_error("startHandshaking: rmc channel is NULL\n");
  }

  if (rmcChannelInfo != NULL && rmcChannelInfo->interrupted == 0) {
    if (ret >= 0) {
      if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_RELAY)
      {
        rmcChannelInfo->rmcSess = RMCAllocUseExternalSocket(rmcChannelInfo->preAllocMem,
            rmcChannelInfo->serverIp, rmcChannelInfo->serverPort,
            rmcChannelInfo->sockFd);
      } else {
        rmcChannelInfo->rmcSess = RMCAllocUseExternalSocket(rmcChannelInfo->preAllocMem,
            rmcChannelInfo->destIp, rmcChannelInfo->destPort,
            rmcChannelInfo->sockFd);
      }

      if (rmcChannelInfo->rmcSess != NULL)
      {
        RMCEncryptionSet(rmcChannelInfo->rmcSess, 1, (uint8_t *)rmcChannelInfo->aesKey);
        RMCPrivateDataSet(rmcChannelInfo->rmcSess, (void *) rmcChannelInfo);

        if (rmcChannelInfo->isFrameFilteringEnabled) {
          RMCRXSideGoodFrameThresholdSet(rmcChannelInfo->rmcSess, 100);
        } else {
          RMCRXSideGoodFrameThresholdSet(rmcChannelInfo->rmcSess, 90);
        }

        if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_LOCAL)
        {
          RMCRXSideTimeoutSet(rmcChannelInfo->rmcSess, 500, 800);
        }
        else if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_REMOTE)
        {
//          RMCRXSideTimeoutSet(rmcChannelInfo->rmcSess, 3000, 4000);
          RMCRXSideTimeoutSet(rmcChannelInfo->rmcSess, 700, 1000);
        }
        else
        {
        	RMCRXSideTimeoutSet(rmcChannelInfo->rmcSess, 700, 1000);
        }

        /*
         * 20161222 HOANG AA-2290
         * For Hubble camera (use old P2P flow), just sends access stream in PS mode
         * If app sends access stream in PL/PR mode, P2P command is always failed.
         */
        if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_RELAY) {
        	log_info("Sending stream access request...\n");
        	rmcChannelSendRelayCmd(rmcChannelInfo, eMediaSubTypeCommandAccessStream);
        	log_info("Sending stream access request...DONE\n");

        	//init timer and retry send rmcChannelSendRelayCmd
        	if (rmcChannelInfo->rmcSessionType == RMC_SESSION_TYPE_FILE_TRANSFER) {
				int status = pthread_create(&rmcChannelInfo->rmcChannelAccessStreamTimerThread, NULL, rmcAccessStreamTimerTask, (void *)rmcChannelInfo);
				if (status < 0)
				{
					log_error("RmcChannel, start access stream timer thread failed, ret %d\n", status);
				}
        	}

        } else {
        	if (rmcChannelInfo->alwaysSendAccessStream == 1) {
        		log_info("Do direct punching...\n");
        		rmcChannelDirectPunching(rmcChannelInfo);
        		log_info("Do direct punching...DONE\n");
        	}
        }

        // Init file transfer context if needed
        if (rmcChannelInfo->rmcSessionType == RMC_SESSION_TYPE_FILE_TRANSFER) {
        	rmcChannelFileTransferInit(rmcChannelInfo);
        }
      }
      else
      {
        log_error("startHandshaking, rmcChannelInfo->rmcSess is NULL\n");
      }
    }
  }


  return ret;
}

int rmcChannelStartSession(RmcChannelInfo *rmcChannelInfo)
{
  int status = -1;
  log_info("RmcChannel, starting RMC main session thread\n");
  if (rmcChannelInfo != NULL) {
    pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
    if (rmcChannelInfo->interrupted == 0) {
    	struct timeval pTime;
    	gettimeofday(&pTime, NULL);
    	rmcChannelInfo->lastReceived = pTime.tv_sec*1000000.0 + pTime.tv_usec;
    	if (rmcChannelInfo->rmcSessionType == RMC_SESSION_TYPE_LIVE_STREAMING) {
    		status = pthread_create(&rmcChannelInfo->rmcChannelVideoThread, NULL, rmcChannelWorkerThread, (void *)rmcChannelInfo);
    		if (status < 0)
    		{
    			log_error("RmcChannel, start rmc video thread failed, ret %d\n", status);
    		}
    		log_info("RmcChannel, starting rmc video thread...DONE\n");

    		log_info("RmcChannel, starting timer thread\n");
    		status = pthread_create(&rmcChannelInfo->rmcChannelTimerThread, NULL, rmcTimerTask, (void *)rmcChannelInfo);
    		if (status < 0)
    		{
    			log_error("RmcChannel, start video timer thread failed, ret %d\n", status);
    		}
    		log_info("RmcChannel, starting timer thread...DONE\n");
    	} else {
    		status = pthread_create(&rmcChannelInfo->rmcChannelVideoThread, NULL, rmcChannelFileTransferTask, (void *)rmcChannelInfo);
    		if (status < 0)
    		{
    			log_error("RmcChannel, start rmc file transfer thread failed, ret %d\n", status);
    		}
    		log_info("RmcChannel, start rmc file transfer thread...DONE\n");

    		log_info("RmcChannel, starting file transfer timer thread\n");
    		status = pthread_create(&rmcChannelInfo->rmcChannelTimerThread, NULL, rmcChannelFileTransferTimerTask, (void *)rmcChannelInfo);
    		if (status < 0)
    		{
    			log_error("RmcChannel, start file transfer timer thread failed, ret %d\n", status);
    		}
    		log_info("RmcChannel, start file transfer timer thread...DONE\n");

    		log_info("RmcChannel, starting file transfer data thread\n");
    		status = pthread_create(&rmcChannelInfo->rmcChannelFileTransferThread, NULL, rmcChannelFileTransferDataTask, (void *)rmcChannelInfo);
    		if (status < 0)
    		{
    			log_error("RmcChannel, start file transfer data thread failed, ret %d\n", status);
    		}
    		log_info("RmcChannel, start file transfer data thread...DONE\n");
    	}
    } else {
      log_error("RmcChannel, start rmc session failed, rmc channel interrupted\n");
    }
    pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
  } else {
    log_error("RmcChannel, start RMC session failed, rmc channel is null\n");
  }
}


int rmcChannelStopSession(RmcChannelInfo *rmcChannelInfo)
{
	log_info("RMC channel, stop session...\n");
	if (rmcChannelInfo != NULL) {
		rmcChannelInfo->interrupted = 1;
		pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);

		if (rmcChannelInfo->terminated == 0) {
			rmcChannelInfo->terminated = 1;
			if (rmcChannelInfo->rmcSess != NULL) {
				log_info("RMC channel, cleaning up media session...\n");
				if (rmcChannelInfo->rmcChannelType == RMC_CHANNEL_TYPE_RELAY)
				{
					log_info("RMC channel, send close relay session cmd\n");
					rmcChannelSendRelayCmd(rmcChannelInfo, eMediaSubTypeCommandCloseStream);
				}

				RMCCleanup(rmcChannelInfo->rmcSess);
				log_info("RMC channel, cleaning up media session...DONE\n");
			}

			if (rmcChannelInfo->rmcChannelVideoThread != NULL)
			{
				log_info("RMC channel, waiting video thread stopped...\n");
				if (pthread_join(rmcChannelInfo->rmcChannelVideoThread, NULL) != 0)
				{
					log_error("RMC channel, cannot stop video thread\n");
				}
				log_info("RMC channel, video thread stopped\n");
			}

			if (rmcChannelInfo->rmcChannelTimerThread != NULL)
			{
				log_info("RMC channel, waiting video timer thread stopped...\n");
				if (pthread_join(rmcChannelInfo->rmcChannelTimerThread, NULL) != 0)
				{
					log_error("RMC channel, cannot stop video timer thread\n");
				}
				log_info("RMC channel, video timer thread stopped\n");
			}

			if (rmcChannelInfo->rmcChannelAccessStreamTimerThread != NULL)
			{
				log_info("RMC channel, waiting access stream timer thread stopped...\n");
				if (pthread_join(rmcChannelInfo->rmcChannelAccessStreamTimerThread, NULL) != 0)
				{
					log_error("RMC channel, cannot stop access stream timer thread\n");
				}
				log_info("RMC channel, access stream timer thread stopped\n");
			}

			if (rmcChannelInfo->rmcSessionType == RMC_SESSION_TYPE_FILE_TRANSFER) {
				if (rmcChannelInfo->rmcChannelFileTransferThread != NULL) {
					log_info("RMC channel, waiting file transfer data thread stopped...\n");
					if (pthread_join(rmcChannelInfo->rmcChannelFileTransferThread, NULL) != 0)
					{
						log_error("RMC channel, cannot stop file transfer data thread\n");
					}
					log_info("RMC channel, file transfer data thread stopped\n");
				}
			}
		} else {
			log_debug("RMC channel terminated, don't need to cleanup anymore");
		}

		pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
	} else {
		log_info("rmcChannelStopSession, rmc channel is NULL\n");
	}
}

//int rmcChannelSendTalkbackData(RmcChannelInfo *rmcChannelInfo, void *frame, uint32_t framesize, uint32_t timestamp)
//{
//  int ret = -1;
//  if (rmcChannelInfo != NULL) {
//    if (rmcChannelInfo->rmcSess != NULL) {
//      ret = RMCRxSideAudioSend(rmcChannelInfo->rmcSess, frame, framesize, timestamp);
//    } else {
//      log_error("send talkback rmc session is NULL\n");
//    }
//  } else {
//    log_error("send talkback rmc channel info is NULL\n");
//  }
//  return ret;
//}

int rmcChannelSendTalkbackData(RmcChannelInfo *rmcChannelInfo, void *pcmData, int pcmDataOffset, int pcmDataLength)
{
  int ret = -1;
  if (rmcChannelInfo != NULL) {
    pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
    if (rmcChannelInfo->interrupted == 0) {
      if (rmcChannelInfo->rmcSess != NULL) {
        RmcTalkbackContext *talkbackContex = rmcChannelInfo->rmcTalkbackContext;
        int out_size;
        g722_enc_encode(&talkbackContex->g722EncContext,
            (short *)pcmData, pcmDataLength/2, talkbackContex->outBuf, &out_size);
        ret = RMCRxSideAudioSend(rmcChannelInfo->rmcSess, talkbackContex->outBuf, out_size, talkbackContex->currentTimestamp++);
        log_debug("(%s:%d) send talkback rmc session, size %d, timestamp %ld\n", __FILE__, __LINE__, out_size, talkbackContex->currentTimestamp);
      } else {
        log_error("(%s:%d) send talkback rmc session is NULL\n", __FILE__, __LINE__);
      }
    } else {
      log_error("(%s:%d) send talkback error, rmc session interrupted\n", __FILE__, __LINE__);
    }
    pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
  } else {
    log_error("(%s:%d) send talkback rmc channel info is NULL\n",  __FILE__, __LINE__);
  }
  return ret;
}


int rmcChannelSendPcmTalkbackData(RmcChannelInfo *rmcChannelInfo, void *pcmData, int pcmDataOffset, int pcmDataLength)
{
    int ret = -1;
    if (rmcChannelInfo != NULL) {
        pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
        if (rmcChannelInfo->interrupted == 0) {
            if (rmcChannelInfo->rmcSess != NULL) {
                RmcTalkbackContext *talkbackContex = rmcChannelInfo->rmcTalkbackContext;
//                int out_size;
//                g722_enc_encode(&talkbackContex->g722EncContext,
//                        (short *)pcmData, pcmDataLength/2, talkbackContex->outBuf, &out_size);
//                ret = RMCRxSideAudioSend(rmcChannelInfo->rmcSess, talkbackContex->outBuf, out_size, talkbackContex->currentTimestamp++);
                ret = RMCRxSideAudioSend(rmcChannelInfo->rmcSess, pcmData, pcmDataLength, talkbackContex->currentTimestamp++);
                log_debug("(%s:%d) send talkback data, size %d, timestamp %ld, ret %d\n", __FILE__, __LINE__, pcmDataLength, talkbackContex->currentTimestamp, ret);
            } else {
                log_error("(%s:%d) send talkback rmc session is NULL\n", __FILE__, __LINE__);
            }
        } else {
            log_error("(%s:%d) send talkback error, rmc session interrupted\n", __FILE__, __LINE__);
        }
        pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
    } else {
        log_error("(%s:%d) send talkback rmc channel info is NULL\n",  __FILE__, __LINE__);
    }
    return ret;
}


int rmcChannelSendCmd(RmcChannelInfo *rmcChannelInfo, const char *request, char **response)
{
  log_info("rmcChannelSendCmd, send command: %s\n", request);

  if (rmcChannelInfo == NULL)
  {
    *response = NULL;
    log_error("send command err, rmc channel is NULL\n");
  }
  else
  {
    tCmdResponse *res = NULL;

    if (rmcChannelInfo->interrupted == 1)
    {
      *response = NULL;
      log_info("send command err, RMC client has stopped\n");
    }
    else
    {
      if (rmcChannelInfo->rmcSess == NULL)
      {
        *response = NULL;
        log_error("send command err, MediaSession is NULL\n");
      }
      else
      {
        tCmdResponseBuffer req;
        req.data = (void *)request;
        req.size = strlen(request);
        int commandID = -1;
        pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
        if (rmcChannelInfo->rmcSess != NULL) {
            commandID = RMCRxSideCmdReqSend(rmcChannelInfo->rmcSess, &req);
        } else {
        	log_error("Send cmd req failed, rmcSess is NULL\n");
        }
        pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
        if(commandID >= 0)
        {
          struct timeval pTime;
          double time_now = -1;
          gettimeofday(&pTime, NULL);
          time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
          // Default timeout for remote p2p command is 10s,
          // Local command will be sent via http, so don't care here
          double expired_time = time_now + MAX_COMMAND_TIMEOUT;
          int isSucceeded = 0;
          while(time_now < expired_time && rmcChannelInfo->interrupted == 0)
          {
            int interrupted = 0;
        	pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
        	if (rmcChannelInfo->rmcSess != NULL) {
                res = RMCRxSideCmdResGet(rmcChannelInfo->rmcSess, commandID);
        	} else {
        		log_error("Get cmd req failed, rmcSess is NULL\n");
        		interrupted = 1;
        	}
        	pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);

            if (interrupted == 1) {
                break;
            }

            if(res != NULL && res->status == eMediaErrorOK)
            {
              isSucceeded = 1;
              break;
            }
            else
            {
              usleep(20000);
              // Update time now
              gettimeofday(&pTime, NULL);
              time_now = pTime.tv_sec*1000000.0 + pTime.tv_usec;
            }
          }

          if (isSucceeded == 1)
          {
            log_info("rmcChannel send cmd res: %s, length %d\n", (char*)res->CmdResponse.data, res->CmdResponse.size);
            if (res->CmdResponse.size > 0) {
              *response = (char *) malloc(res->CmdResponse.size + 1);
              memset(*response, 0, res->CmdResponse.size + 1);
              strncpy(*response, (char*)res->CmdResponse.data, res->CmdResponse.size);
            } else {
              log_error("Send p2p command failed, response is empty\n");
              *response = NULL;
            }
          }
          else
          {
            log_error("Send p2p command failed timeout\n");
            *response = NULL;
          }
        } else {
          log_error("Send p2p command failed, command id %d\n", commandID);
          *response = NULL;
        }
      } //else cmd_idx != -1
    } //else isRunning == true
  }
  return 0;
}

int rmcChannelNotifyStatus(RmcChannelInfo *rmcChannelInfo, int status)
{
  JNIEnv *env = getJNIEnv();
  if (env == NULL) {
    log_error("JNIEnv is NULL\n");
    return -1;
  }

  if (rmcChannelInfo->rmcChannelObj != NULL && rmcChannelInfo->notifyRmcChannelStatusMid != NULL) {
    // notify channel status
    (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->notifyRmcChannelStatusMid, status);
  }
  return 0;
}

int rmcChannelUpdateJpegImg(RmcChannelInfo *rmcChannelInfo, void *jpegData, int jpegDataLength)
{
  int ret = -1;
  JNIEnv *env = getJNIEnv();
  if (env == NULL) {
    log_error("JNIEnv is NULL\n");
    return ret;
  }

  if (jpegDataLength > 0) {
	  if (rmcChannelInfo->rmcChannelObj != NULL && rmcChannelInfo->updateJpegImgMid != NULL) {
		  // notify channel status
		  ret = 0;
		  jbyteArray jbytes = NULL;
		  jbytes = (*env)->NewByteArray(env, jpegDataLength);
		  if (jbytes != NULL) {
			  (*env)->SetByteArrayRegion(env, jbytes, 0, jpegDataLength, (jbyte*) jpegData);
		  }
		  (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->updateJpegImgMid, jbytes, jpegDataLength);
		  (*env)->DeleteLocalRef(env, jbytes);
	  } else {
		  log_error("rmcChannelUpdateJpegImg error, callback is NULL\n");
	  }
  } else {
	  log_error("rmcChannelUpdateJpegImg invalid jpegDataLength %d\n", jpegDataLength);
  }
  return ret;
}

int rmcChannelUpdateTalkbackData(RmcChannelInfo *rmcChannelInfo, void *talkbackData, int talkbackDataLength)
{
  int ret = -1;
  JNIEnv *env = getJNIEnv();
  if (env == NULL) {
    log_error("JNIEnv is NULL\n");
    return ret;
  }

  if (rmcChannelInfo->rmcChannelObj != NULL && rmcChannelInfo->updateTalkbackDataMid != NULL) {
    // notify channel status
    ret = 0;
    jbyteArray jbytes = NULL;
    jbytes = (*env)->NewByteArray(env, talkbackDataLength);
    if (jbytes != NULL) {
      (*env)->SetByteArrayRegion(env, jbytes, 0, talkbackDataLength, (jbyte*) talkbackData);
    }
    (*env)->CallVoidMethod(env, rmcChannelInfo->rmcChannelObj, rmcChannelInfo->updateTalkbackDataMid, jbytes, talkbackDataLength);
    (*env)->DeleteLocalRef(env, jbytes);
  } else {
    log_error("rmcChannelUpdateTalkbackData error, callback is NULL\n");
  }
  return ret;
}

int rmcChannelGetBandwidth(RmcChannelInfo *rmcChannelInfo) {
	int bandwidth = 0;
	if (rmcChannelInfo != NULL) {
		pthread_mutex_lock(&rmcChannelInfo->rmcChanelLock);
		if (rmcChannelInfo->interrupted == 0) {
			bandwidth = RMCRxSideBwGet(rmcChannelInfo->rmcSess);
		} else {
			log_error("Get RMC bandwidth failed, RMC channel interrupted");
		}
		pthread_mutex_unlock(&rmcChannelInfo->rmcChanelLock);
	}
	return bandwidth;
}

//const char* rmcChannelGetTypeName(RmcChannelInfo *rmcChannelInfo)
//{
//  char *rmcChannelTypeName = NULL;
//  if (rmcChannelInfo == NULL) {
//    rmcChannelTypeName = "N/A";
//  } else {
//    switch (rmcChannelInfo->rmcChannelType) {
//    case RMC_CHANNEL_TYPE_LOCAL:
//      rmcChannelTypeName = "RMC local";
//      break;
//    case RMC_CHANNEL_TYPE_REMOTE:
//      rmcChannelTypeName = "RMC remote";
//      break;
//    case RMC_CHANNEL_TYPE_RELAY:
//      rmcChannelTypeName = "RMC relay";
//      break;
//    default:
//      rmcChannelTypeName = "N/A";
//      break;
//    }
//  }
//  return rmcChannelTypeName;
//}
