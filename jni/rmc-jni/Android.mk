LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)
#LOCAL_MODULE    := leonlog
#LOCAL_SRC_FILES := lib/libleonlog.a
#LOCAL_PRELINK_MODULE := true
#include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := rmc_client
LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS -DANDROID_BUILD -D_RECEIVER_ -D_NXCOMM_P2P_PLUS_UPNP_ -DPJ_IS_BIG_ENDIAN=0 -DPJ_IS_LITTLE_ENDIAN=1
LOCAL_CFLAGS += -DBUILD_WITH_RMC
LOCAL_CPPFLAGS := -DANDROID_BUILD=1 -D_RECEIVER_ -D__STDC_CONSTANT_MACROS -D_NXCOMM_P2P_PLUS_UPNP_ -DPJ_IS_BIG_ENDIAN=0 -DPJ_IS_LITTLE_ENDIAN=1
LOCAL_CPPFLAGS += -DBUILD_WITH_RMC

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include \
										$(LOCAL_PATH)/../include \
										$(LOCAL_PATH)/../include/mediaplayer \
										$(LOCAL_PATH)/../include/rmc_inc
LOCAL_SRC_FILES := com_nxcomm_jstun_android_RmcChannel.cpp \
					rmc_channel.c \
					g722_enc.c \
					g722_dec.c \
					flv_parser.c \
				  ../zjni/onLoad.cpp

LOCAL_LDLIBS := -lz -lm -llog -landroid
LOCAL_SHARED_LIBRARIES := libp2p librmc
#LOCAL_STATIC_LIBRARIES := leonlog
include $(BUILD_SHARED_LIBRARY)