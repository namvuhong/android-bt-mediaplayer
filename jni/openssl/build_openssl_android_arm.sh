
if ! [ -f ../set_env_arm.sh ]; then
echo "file ../set_env_arm.sh is not found, create one"
exit 1
fi
. ../set_env_arm.sh 

if [ "$NDK" = "" ]; then
echo NDK variable not set, exiting
echo "Use: export NDK=/your/path/to/android-ndk"
exit 1
fi


function build_openssl
{

    CROSS_COMPILE=$PREBUILT/bin/$CROSS_COMPILE_PREFIX-
    CFLAGS=" -O3 -fpic -DANDROID -DHAVE_SYS_UIO_H=1  -fasm -Wno-psabi -fno-short-enums  -fno-strict-aliasing -finline-limit=300 $OPTIMIZE_CFLAGS "
#CFLAGS=$OPTIMIZE_CFLAGS 
    export CPPFLAGS="$CFLAGS"
    export CFLAGS="$CFLAGS"
    export CXXFLAGS="$CFLAGS"
    export CXX="${CROSS_COMPILE}g++ --sysroot=$PLATFORM"
    export AS="${CROSS_COMPILE}gcc --sysroot=$PLATFORM"
    export CC="${CROSS_COMPILE}gcc --sysroot=$PLATFORM"
    export NM="${CROSS_COMPILE}nm"
    export STRIP="${CROSS_COMPILE}strip"
    export RANLIB="${CROSS_COMPILE}ranlib"
    export AR="${CROSS_COMPILE}ar"
    export LDFLAGS="$NOEXECSTACK_FLAG  -Wl,-rpath-link=$PLATFORM/usr/lib -L$PLATFORM/usr/lib -nostdlib -lc -lm -ldl -llog"

    echo "configuring ..... $LDFLAGS"
    ./Configure --prefix=$(pwd)/$PREFIX shared $TARGET_PLATFORM \
    $ADDITIONAL_CONFIGURE_FLAG

    rm -rf */lib */*/lib
    make clean
    make -j4
    make install
}

#CFLAGS=" -I$ARM_INC -fpic -DANDROID -fpic -mthumb-interwork -ffunction-sections -funwind-tables -fstack-protector -fno-short-enums -D__ARM_ARCH_5__ -D__ARM_ARCH_5T__ -D__ARM_ARCH_5E__ -D__ARM_ARCH_5TE__  -Wno-psabi -march=armv5te -mtune=xscale -msoft-float -mthumb -Os -fomit-frame-pointer -fno-strict-aliasing -finline-limit=64 -DANDROID  -Wa,--noexecstack -MMD -MP "

function openssl_armv7 
{
#armv7-a
TARGET_PLATFORM=android-armv7
CPU=armv7-a
OPTIMIZE_CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=$CPU -mtune=cortex-a8"
PREFIX=./android/$CPU 
ADDITIONAL_CONFIGURE_FLAG=

echo "Build openssl for $CPU, target platform $TARGET_PLATFORM "
build_openssl
}


function openssl_arm
{

#arm 
TARGET_PLATFORM=android
CPU=armeabi
OPTIMIZE_CFLAGS=""
PREFIX=./android/$CPU 
ADDITIONAL_CONFIGURE_FLAG=

echo "Build openssl for $CPU, target platform $TARGET_PLATFORM "
build_openssl
}

function openssl_x86
{

#arm 
TARGET_PLATFORM=android-x86
CPU=x86
OPTIMIZE_CFLAGS=""
PREFIX=./android/$CPU
ADDITIONAL_CONFIGURE_FLAG=

echo "Build openssl for $CPU, target platform $TARGET_PLATFORM "
build_openssl
}


#openssl_armv7
#openssl_x86
openssl_arm


