#ifndef __FLV_PARSER_H__
#define __FLV_PARSER_H__

#include <unistd.h>
#ifdef HAVE_SYS_TYPES_H
#  include <sys/types.h> /* off_t */
#endif

/* FLV tag */
#define FLV_TAG_TYPE_AUDIO  ((uint8)0x08)
#define FLV_TAG_TYPE_VIDEO  ((uint8)0x09)
#define FLV_TAG_TYPE_META_DATA   ((uint8)0x12)

#define FLV_PREV_TAG_BYTE_COUNT 4
#define FLV_TAG_HEADER_BYTE_COUNT 11

typedef struct FlvHeader {
	uint8_t signature[3]; /* always "FLV" */
    uint8_t version; /* should be 1 */
    uint8_t flags;
    uint32_t offset; /* always 9 */
} FlvHeader;

typedef struct FlvTag {
	uint8_t type;
	int size;
	int timestamp;
	uint8_t timestamp_extended;
	int stream_id;
	uint8_t *data;
} FlvTag;

int flv_get_header_size(uint8_t *flvBuf, uint32_t flvBufSize);
int flv_get_next_tag_size(uint8_t *flvBuf, uint32_t flvBufSize);

#endif /* __FLV_PARSER_H__ */
