/*
 * Copyright (C) 2015 NxComm Pte. Ltd.
 *
 * This unpublished material is proprietary to NxComm.
 * All rights reserved. The methods and
 * techniques described herein are considered trade secrets
 * and/or confidential. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of NxComm.
 */

/* FILE   : cvlog.h
 * AUTHOR : hoang
 * DATE   : Mar 31, 2015
 * DESC   : Define log methods for all native libs.
 */

#ifndef __MY_LOG_H__
#define __MY_LOG_H__

#ifdef ANDROID_BUILD

#include <android/log.h>
#define TAG "rmc-native"
#ifdef LOGGING_ENABLED
#define log_verbose(...) __android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__)
#define log_debug(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
#define log_info(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
#define log_warning(...) __android_log_print(ANDROID_LOG_WARN, TAG, __VA_ARGS__)
#define log_error(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define log_fatal(...) __android_log_print(ANDROID_LOG_FATAL, TAG, __VA_ARGS__)
#define log_silent(...) __android_log_print(ANDROID_LOG_SILENT, TAG, __VA_ARGS__)

// #define printf(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
#else
#define log_verbose(...) do {} while(0)
#define log_debug(...) do {} while(0)
#define log_info(...) do {} while(0)
#define log_warning(...) do {} while(0)
#define log_error(...) do {} while(0)
#define log_fatal(...) do {} while(0)
#define log_silent(...) do {} while(0)
#endif

#else

#include <stdio.h>
#define log_verbose(...) printf(__VA_ARGS__)
#define log_debug(...) printf(__VA_ARGS__)
#define log_info(...) printf(__VA_ARGS__)
#define log_warning(...) printf(__VA_ARGS__)
#define log_error(...) printf(__VA_ARGS__)
#define log_fatal(...) printf(__VA_ARGS__)
#define log_silent(...) printf(__VA_ARGS__)

#endif

#endif /* __MY_LOG_H__ */
