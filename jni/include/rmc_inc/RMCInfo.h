/*
* Copyright (C) 2014 NxComm Pte. Ltd.
*
* This unpublished material is proprietary to NxComm.
* All rights reserved. The methods and
* techniques described herein are considered trade secrets
* and/or confidential. Reproduction or distribution, in whole
* or in part, is forbidden except by express written permission
* of NxComm.
*/

/* FILE   : RMCInfo.h
 * AUTHOR : leon
 * DATE   : Mar 3, 2015
 * DESC   :
 */

#ifndef RELIABLE_CHANNEL_INC_RMCINFO_H_
#define RELIABLE_CHANNEL_INC_RMCINFO_H_

typedef enum eMediaErrorCode
    {
    eMediaErrorObsolete             = -16,
    eMediaErrorOpenFile,
    eMediaErrorOutOfRange,
    eMediaErrorTimeout,
    eMediaErrorNotSent,
    eMediaErrorWaitingForResponse,
    eMediaErrorTerminated           = -2,
    eMediaErrorFailed               = -1,
    eMediaErrorOK                   = 0,
    } eMediaErrorCode;

typedef enum eMediaType
    {
    eMediaTypeNull = 0,
    eMediaTypeVideo,
    eMediaTypeAudio,
    eMediaTypeCommand,
    eMediaTypeFile,                 //No more supported
    eMediaTypeACK,
    eMediaTypeTalkback,
    eMediaTypeUserDefine,
    } eMediaType;

typedef enum eMediaSubType
    {
    eMediaSubTypeNULL               = eMediaTypeNull * 10,
    eMediaSubTypeVideoIFrame        = eMediaTypeVideo * 10,
    eMediaSubTypeVideoBFrame,
    eMediaSubTypeVideoPFrame,
    eMediaSubTypeVideoJPEG,
    eMediaSubTypeAudioAlaw          = eMediaTypeAudio * 10,
    eMediaSubTypeAudioPCM,
    eMediaSubTypeAudioADPCM,
    eMediaSubTypeAudioG722,
    eMediaSubTypeCommandRequest     = eMediaTypeCommand * 10,
    eMediaSubTypeCommandResponse,
    eMediaSubTypeCommandOpenStream,
    eMediaSubTypeCommandStopStream,
    eMediaSubTypeCommandAccessStream,
    eMediaSubTypeCommandCloseStream,
    eMediaSubTypeFileV1             = eMediaTypeFile * 10,
    eMediaSubTypeFileV2,
    eMediaSubTypeMediaACK           = eMediaTypeACK * 10,
    eMediaSubTypeMediaFileACK,
    eMediaSubTypeTalkback           = eMediaTypeTalkback * 10,
    eMediaSubTypeUserDefine         = eMediaTypeUserDefine * 10,
    } eMediaSubType;

/* This section contains return data structure */
typedef struct tCmdResponseBuffer
    {
    uint16_t        size;           //real command/response size
    void           *data;           //command/response data pointer
    } tCmdResponseBuffer;

typedef struct tCmdResponse
    {
    int                     status;                                     // Status
    tCmdResponseBuffer      CmdResponse;                                // Command response data
    } tCmdResponse;

/*
 * PURPOSE : Handle media data
 * INPUT   : [data] - Pointer to data
 *           [size] - Size of data
 *           [type] - Media type
 *           [ts]   - Time stamp of this data
 *           [priv] - Private data pointer
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: This is a callback function that user should implement
 *           by himself
 */
typedef void
(*MediaFrameDataHdl)(void* data, uint32_t size, eMediaSubType type, uint32_t ts, void* priv);

/*
 * PURPOSE : Handle when channel is timeout
 * INPUT   : [channel] - Pointer to channel
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: This is a callback function that user should implement
 *           by himself
 */
typedef void
(*RMCTimeOutHdl)(void* channel);

/*
 * PURPOSE : Report bandwidth
 * INPUT   : [name]     - Stream name or client name
 *           [cli_bw]   - Bandwidth at client side
 *           [input_bw] - Bandwidth at camera side, measure at sensor side
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: This is a callback function to report bandwidth.
 */
typedef void
(*RMCClientBwCb)(char* name, int cli_bw, int input_bw);

#endif /* RELIABLE_CHANNEL_INC_RMCINFO_H_ */
