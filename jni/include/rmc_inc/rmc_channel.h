/*
 * Copyright (C) 2014 NxComm Pte. Ltd.
 *
 * This unpublished material is proprietary to NxComm.
 * All rights reserved. The methods and
 * techniques described herein are considered trade secrets
 * and/or confidential. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of NxComm.
 */

/* FILE   : com_cvision_stun_StunClient.cpp
 * AUTHOR : hoang
 * DATE   : Mar 24, 2015
 * DESC   : This is JNI part for StunClient Java class.
 */

#ifndef __RMC_CHANNEL_H__
#define __RMC_CHANNEL_H__

#include <jni.h>
#include <stdlib.h>
#include <pthread.h>
#include "rmc_inc/RMC.h"
#include "mediaplayer/g722_enc.h"
#include "mediaplayer/g722_dec.h"

#define RMC_CHANNEL_TYPE_LOCAL 0
#define RMC_CHANNEL_TYPE_REMOTE 1
#define RMC_CHANNEL_TYPE_RELAY 2

typedef enum RmcSessionType {
	RMC_SESSION_TYPE_LIVE_STREAMING = 0,
	RMC_SESSION_TYPE_FILE_TRANSFER
} RmcSessionType;

typedef void (*RmcBandwidthCallback)(void *privData, uint32_t rmcBandwidth);

#define RMC_1_0 10
#define RMC_2_0 20

#define TALKBACK_DATA_LENGTH_MAX 4*1024
#define TRANSFER_FILE_LENGTH_MAX 50*1024*1024

typedef struct RmcTalkbackContext {
	g722_enc_t g722EncContext;
	g722_dec_t g722DecContext;
	uint8_t pcmBuf[TALKBACK_DATA_LENGTH_MAX];
	uint8_t outBuf[TALKBACK_DATA_LENGTH_MAX];
	uint32_t currentTimestamp;
	int16_t decBuf[TALKBACK_DATA_LENGTH_MAX];
} RmcTalkbackContext;

typedef struct RmcFileTransferContext {
	uint8_t fileBuff[TRANSFER_FILE_LENGTH_MAX];
	uint32_t fileBuffLength;
	uint32_t fileBuffDownloaded;
	uint32_t fileBuffPrevDownloaded;
	uint32_t fileBuffConsumed;
	uint32_t blockDownloaded;
	uint32_t blockExpected;
	uint8_t fileTransferCompleted;
} RmcFileTransferContext;

typedef struct RmcChannelInfo {
	char *destIp;
	int destPort;
	char *serverIp;
	int serverPort;
	int sockFd;
	int rmcVersion;
	int rmcChannelType;
	int rmcSessionType;
	char *randomNumber;
	char *cameraMac;
	char *aesKey;
	void *preAllocMem;
	void *rmcSess;
	void *rmcCallback;
	void *rmcBandwidthCallback;
	void *rmcPrivData;
	int isConnected;
	int isValid;
	int terminated;
	int interrupted;
	int isFrameFilteringEnabled;
	int alwaysSendAccessStream;
	double lastReceived;
	jobject rmcChannelObj;
	jmethodID notifyRmcChannelStatusMid;
	jmethodID notifyRmcChannelHandshakeFailedMid;
	jmethodID updateRmcChannelTypeMid;
	jmethodID updateJpegImgMid;
	jmethodID updateTalkbackDataMid;
	pthread_mutex_t sendCmdLock;
	pthread_t rmcChannelVideoThread;
	pthread_t rmcChannelTimerThread;
	pthread_t rmcChannelFileTransferThread;
	pthread_t rmcChannelAccessStreamTimerThread;
	pthread_mutex_t rmcChanelLock;
	RmcTalkbackContext *rmcTalkbackContext;
	RmcFileTransferContext *rmcTransferFileContext;
} RmcChannelInfo;

RmcChannelInfo* rmcChannelInfoAlloc();

/**
 * Init file transfer context and set it to RMC session.
 * Note: only call this method once RMC session allocated.
 */
int rmcChannelFileTransferInit(RmcChannelInfo *rmcChannelInfo);

void rmcChannelInfoFree(RmcChannelInfo *rmcChannelInfo);
void rmcChannelInfoRegisterCallback(RmcChannelInfo *rmcChannelInfo, void *privData, MediaFrameDataHdl callback, RmcBandwidthCallback bandwidthCallback);
void rmcChannelInfoUnregisterCallback(RmcChannelInfo *rmcChannelInfo);
void rmcChannelSendRelayCmd(RmcChannelInfo *rmcChannelInfo, eMediaSubType cmdType);
void rmcChannelSendChangeModeCmd(RmcChannelInfo *rmcChannelInfo, const char *request);
void rmcChannelDumpInfo(RmcChannelInfo *rmcChannelInfo);
int rmcChannelConnectToDestAddr(RmcChannelInfo *rmcChannelInfo);
int rmcChannelConnectToServerAddr(RmcChannelInfo *rmcChannelInfo);

int rmcChannelRemoteHandshake(RmcChannelInfo *rmcChannelInfo);

/**
 * Try to connect and handshake if needed for rmc channel
 */
int rmcChannelInitSession(RmcChannelInfo *rmcChannelInfo);
int rmcChannelStartSession(RmcChannelInfo *rmcChannelInfo);
int rmcChannelStopSession(RmcChannelInfo *rmcChannelInfo);
//int rmcChannelSendTalkbackData(RmcChannelInfo *rmcChannelInfo, void *frame, uint32_t framesize, uint32_t timestamp);
int rmcChannelSendTalkbackData(RmcChannelInfo *rmcChannelInfo, void *pcmData, int pcmDataOffset, int pcmDataLength);

/**
 * Return command ID.
 */
int rmcChannelSendCmd(RmcChannelInfo *rmcChannelInfo, const char *request, char **response);

/**
 * Send 8 access stream pkt for punching.
 */
void rmcChannelDirectPunching(RmcChannelInfo *rmcChannelInfo);

int rmcChannelNotifyStatus(RmcChannelInfo *rmcChannelInfo, int status);
int rmcChannelUpdateJpegImg(RmcChannelInfo *rmcChannelInfo, void *jpegData, int jpegDataLength);
int rmcChannelUpdateTalkbackData(RmcChannelInfo *rmcChannelInfo, void *talkbackData, int talkbackDataLength);
int rmcChannelSendPcmTalkbackData(RmcChannelInfo *rmcChannelInfo, void *pcmData, int pcmDataOffset, int pcmDataLength);
int rmcChannelGetBandwidth(RmcChannelInfo *rmcChannelInfo);

//int rmcChannelRelayHandshake(RmcChannelInfo *rmcChannelInfo);
//void rmcChannelGetTypeName(RmcChannelInfo *rmcChannelInfo);

#endif /* __RMC_CHANNEL_H__ */
