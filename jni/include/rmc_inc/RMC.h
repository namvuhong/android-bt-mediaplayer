/*
* Copyright (C) 2014 NxComm Pte. Ltd.
*
* This unpublished material is proprietary to NxComm.
* All rights reserved. The methods and
* techniques described herein are considered trade secrets
* and/or confidential. Reproduction or distribution, in whole
* or in part, is forbidden except by express written permission
* of NxComm.
*/

/* FILE   : RMC.h
 * AUTHOR : leon
 * DATE   : Mar 3, 2015
 * DESC   :
 */

#ifndef _RMC_H_
#define _RMC_H_

#include <stdint.h>
#include "RMCInfo.h"

/*
 * PURPOSE : Get RMC library version
 * INPUT   : None
 * OUTPUT  : [ver] - Library version
 * RETURN  : Pointer to library version string
 * DESCRIPT: None
 */
char*
RMCVersionGet(char* ver);

/*
 * PURPOSE : Allocate reliable media channel
 * INPUT   : [memAddr]      - Memory location. If NULL, will allocate a new one
 *           [clientIP]     - Client IP Address
 *           [clienPort]    - Client Port
 *           [localPort]    - Local port to bind. If set to zero, let system choose
 * OUTPUT  : None
 * RETURN  : Pointer to channel
 * DESCRIPT: Make sure that the memory big enough. Should use RMCSizeGet function to check
 */
void*
RMCAlloc(void* memAddr, char* clientIP, int32_t clientPort, int32_t localPort);

/*
 * PURPOSE : Allocate reliable media channel local socket
 * INPUT   : [localPort]    - Local port should bind to. Set to 0 auto choose
 * OUTPUT  : [localPort]    - Local port this socket bind to
 *           [localIP]      - Local IP this socket bind to. Can be null
 * RETURN  : Socket
 * DESCRIPT: None
 */
int
RMCLocalSocketCreate(int* localPort, char* localIP);

/*
 * PURPOSE : Create remote socket that binding on IM port
 * INPUT   : [localPort]     - Local port. Set 0 for system auto choose
 * OUTPUT  : [localPort]     - Local port that this socket bind on
 *           [publicAddress] - Public IP that this socket bind on
 *           [publicPort]    - Public port that this socket bind on
 *           [isPortFw]      - 1 if port forwarding OK and have only 1 NAT network
 * RETURN  : Negative if failed. Otherwise socket value
 * DESCRIPT: None
 */
int
RMCRemoteSocketUPNPCreate(int *localPort, char* publicAddress, int* publicPort, int *isPortFw);

/*
 * PURPOSE : Create remote socket that binding on IM port
 * INPUT   : [localPort]     - Local port. Set 0 for system auto choose
 * OUTPUT  : [localPort]     - Local port that this socket bind on
 *           [publicAddress] - Public IP that this socket bind on
 *           [publicPort]    - Public port that this socket bind on
 * RETURN  : Negative if failed. Otherwise socket value
 * DESCRIPT: None
 */
int
RMCRemoteSocketCreate(int *localPort, char* publicAddress, int* publicPort);

/*
 * PURPOSE : Send keep alive packet to maintain connection
 * INPUT   : [socketVal]    - Socket value
 * RETURN  : None
 * DESCRIPT: This function should be called periodically
 *           to make sure router reserve this mapping port rule
 */
void
RMCRemoteSocketKeepAlive(int socketVal);

/*
 * PURPOSE : Allocate reliable media channel with a socket already exist
 * INPUT   : [memAddr]      - Memory location. If NULL, will allocate a new one
 *           [clientIP]     - Client IP Address
 *           [clienPort]    - Client Port
 *           [socketFd]     - UDP socket for send data
 * OUTPUT  : None
 * RETURN  : Pointer to channel
 * DESCRIPT: Make sure that the memory big enough. Should use RMCSizeGet function to check
 */
void*
RMCAllocUseExternalSocket(void* memAddr, char* clientIP, int32_t clientPort, int32_t socketFd);

/*
 * PURPOSE : Set channel remind name
 * INPUT   : [channel]  - Pointer to channel
 *           [name]     - Name
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: Set channel name for identify
 */
void
RMCNameSet(void* channel, char* name);

/*
 * PURPOSE : Get channel remind name
 * INPUT   : [channel]  - Pointer to channel
 * OUTPUT  : [name]     - Name
 * RETURN  : Remind name
 * DESCRIPT: Output can be null
 */
char*
RMCNameGet(void* channel, char* name);

/*
 * PURPOSE : Cleanup reliable media channel
 * INPUT   : [channel]  - Pointer to channel
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: This function will stop all thread related
 *           User must free the pointer after call this function
 */
void
RMCCleanup(void* channel);

/*
 * PURPOSE : Set callback handler when reliable media channel timeout
 * INPUT   : [channel]  - Pointer to channel
 *           [hdl]      - Callback function
 *           [priv]     - User private data. Can be NULL
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: None
 */
void
RMCTimeOutHdlSet(void* channel, RMCTimeOutHdl hdl, void* priv);

/*
 * PURPOSE : Set private data to channel
 * INPUT   : [channel] - Pointer to channel
 *           [priv]    - Private pointer
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: None
 */
void
RMCPrivateDataSet(void* channel, void* priv);

/*
 * PURPOSE : Set encrypt setting
 * INPUT   : [channel]  - Pointer to channel
 *           [enable]   - Enable encrypt/decrypt
 *           [encKey]   - Key used for encrypt/decrypt
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: If enable is 0, then don't care encKey
 */
void
RMCEncryptionSet(void* channel, uint8_t isEnable, uint8_t* encKey);

/*
 * PURPOSE : Send stream command
 * INPUT   : [channel]          - Pointer to channel
 *           [type]             - Command data value
 *           [uniqueString1]    - Unique string for verify
 *           [uniqueString2]    - Unique string for verify
 * OUTPUT  : None
 * RETURN  : 0 if success
 * DESCRIPT: None
 */
int
RMCRelayRequestSend(void* channel, eMediaSubType type, void* uniqueString1, void* uniqueString2);
int
RMCRelayRequestSend2(void* channel, eMediaSubType type, void* uniqueString1, void* uniqueString2, long timeOfDay);

/*
 * PURPOSE : Get the current miliseconds time stamp (from creating time) of RMC channel
 * INPUT   : [channel] - Pointer to channel
 * OUTPUT  : None
 * RETURN  : Time stamp at miliseconds
 * DESCRIPT: None
 */
uint32_t
RMCMsTimeGet(void* channel);

/*
 * PURPOSE : Get the size of RMC data
 * INPUT   : None
 * OUTPUT  : None
 * RETURN  : Size in bytes of RMC
 * DESCRIPT: None
 */
uint32_t
RMCSizeGet(void);

/*
 * PURPOSE : Get socket value of this channel
 * INPUT   : [channel] - Pointer to channel
 * OUTPUT  : None
 * RETURN  : Socket value
 * DESCRIPT: None
 */
int
RMCSocketValueGet(void* channel);

#ifdef _TRANSMITTER_
/**************************************************************************************
 *                 THOSE BELOW API ONLY AVAILABLE FOR TX SIDE
 **************************************************************************************/

/*
 * PURPOSE : Send media frame using channel
 * INPUT   : [channel]      - Pointer to channel
 *           [frame]        - Pointer to frame data
 *           [frameSize]    - Size of this frame
 *           [frameType]    - Sub type of this frame
 *           [frameTs]      - Time stamp of this frame
 * OUTPUT  : None
 * RETURN  : If negative, return error code
 *           If positive, belong to session type
 *              + Command      : return command ID
 *              + Media/Others : number of bytes sent
 * DESCRIPT: This function will start all thread if needed
 */
eMediaErrorCode
RMCTxSideSend(void* channel, void* frame, int frameSize,
              eMediaSubType frameSubType, uint32_t frameTs);

/*
 * PURPOSE : Register a handler for command at transmit side
 * INPUT   : [channel]  - Pointer to channel
 *           [hdl]      - Callback function
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: None
 */
void
RMCTxSideCmdHdlRegister(void* channel, MediaFrameDataHdl hdl);

/*
 * PURPOSE : Register a handler for audio frame at transmit side
 * INPUT   : [channel]  - Pointer to channel
 *           [hdl]      - Callback function
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: None
 */
void
RMCTxSideAudioHdlRegister(void* channel, MediaFrameDataHdl hdl);

/*
 * PURPOSE : Register a handler for bandwidth report
 * INPUT   : [channel]  - Pointer to channel
 *           [hdl]      - Callback function
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: None
 */
void
RMCTxSideBandwidthReportRegister(void* channel, RMCClientBwCb cb);

/*
 * PURPOSE : Set sender filename
 * INPUT : [channel] - Pointer to channel
 *         [path] - Full path to filename
 *         [file_packet_burst_num] - Number of packets sent per burst, it set to <=0, value will be 15
 * RETURN : Error code, 0 if OK, <0 if error
 * Note: Can be extended to Receiver to save file
 */
int
RMCFileFullPathSet(void* channel, char* filepath, int file_packet_burst_num);


#endif

#ifdef _RECEIVER_
/**************************************************************************************
 *                 THOSE BELOW API ONLY AVAILABLE FOR RX SIDE
 **************************************************************************************/

/*
 * PURPOSE : Set range in miliseconds that one block data will be in buffer
 * INPUT   : [channel]   - Pointer to channel
 *           [minTime]   - Time in miliseconds
 *           [maxTime]   - Time in miliseconds
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: Time should be 300ms for minimum delay and 800ms for best quality
 */
void
RMCRXSideTimeoutSet(void* channel, uint32_t minTime, uint32_t maxTime);

/*
 * PURPOSE : Set percent of total packets of a frame to be accepted and processed
 * INPUT   : [channel]  - Pointer to channel
 *           [percent]  - Percent of total packet
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: If a frame have 100 packet and percent is set to 90 then we will accept
 *           we got >= 90 packets
 */
void
RMCRXSideGoodFrameThresholdSet(void* channel, uint32_t percent);

/*
 * PURPOSE : Receive media frame using reliable media channel
 * INPUT   : [channel] - Pointer to channel
 *           [hdl]     - Function to handle received data
 * OUTPUT  : None
 * RETURN  : None
 * DESCRIPT: This function will start all thread if needed. NOTE that this
 *           function is a BLOCKING call. And callback function should handle
 *           data as fast as possible or else, data can be lost at network socket
 */
void
RMCRXSideRecv(void* channel, MediaFrameDataHdl hdl);

/*
 * PURPOSE : Send command from RX side
 * INPUT   : [channel] - Pointer to channel
 *           [command] - Pointer to command structure
 * OUTPUT  : None
 * RETURN  : negative for error code
 *           positive for PID
 * DESCRIPT: This function is to send 1 command from receiver side. User needs to
 *           collect the return and store the PID to further check the command response.
 */
eMediaErrorCode
RMCRxSideCmdReqSend(void* channel, tCmdResponseBuffer* command);

/*
 * PURPOSE : To get response of a command ID
 * INPUT   : [channel]  - Pointer to channel
 *           [cid]      - Command ID
 * OUTPUT  : None
 * RETURN  : Pointer to command response
 * DESCRIPT: None
 */
tCmdResponse*
RMCRxSideCmdResGet(void* channel, uint32_t cid);

/*
 * PURPOSE : Send audio data from RX side
 * INPUT   : [channel]   - Pointer to channel
 *           [frame]     - Pointer to audio frame
 *           [framesize] - Audio frame size
 *           [timestamp] - Time stamp of data
 * OUTPUT  : None
 * RETURN  : 0 if success
 * DESCRIPT: This function is to send 1 audio frame from receiver side.
 */
int
RMCRxSideAudioSend(void* channel, void *frame, uint32_t framesize, uint32_t timestamp);

/*
 * PURPOSE : Get currently bw
 * INPUT   : [channel]   - Pointer to channel
 * OUTPUT  : None
 * RETURN  : Bytes receive till last time called
 * DESCRIPT: This will reset the counter
 */
uint32_t
RMCRxSideBwGet(void* channel);

/*
* PURPOSE : Set receiver file memory
* INPUT : [channel] - Pointer to channel
* OUTPUT : [filebuf] - Pointer to file buffer
*          [filebuflen] - File buffer length
*          [fileblklen] - File block length
*          [filenowblklen] - File buffer valid block length
*          [valid_file_len] - Valid file length, set to 0 to init buffer
* RETURN : Error code, 0 if OK
* Note: if *fileblklen=*filenowblklen, file sending completed
*/
int
RMCFileMemSet(void* channel, uint8_t* filebuf, uint32_t* filebuflen, uint32_t* fileblklen, uint32_t* filenowblklen, uint32_t* valid_file_len);

#endif

#endif /* _RMC_H_ */
