/* //device/libs/android_runtime/com_media_ffmpeg_FFMpegPlayer.cpp
**
** Copyright 2007, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#define TAG "FFMpegPlayer-JNI"
#define MAX_TALKBACK_DATA_LENGTH 4 * 1024

#include <unistd.h>
#include <android/log.h>
#include <android/bitmap.h>
#include "jniUtils.h"
#include "methods.h"
#include "output.h"
#include "mylog.h"

#include <mediaplayer.h>

struct fields_t {
    jfieldID    context;
    jmethodID   post_event;
    jmethodID	get_next_clip;

};
static fields_t fields;

/* Global thingy */
static MediaPlayer * mediaPlayer;
static int8_t talkbackData[MAX_TALKBACK_DATA_LENGTH];

static const char* const kClassPathName = "com/media/ffmpeg/FFMpegPlayer";
const char* STR_MEDIA_PLAYBACK_COMPLETE = "complete";
const char* STR_MEDIA_PLAYBACK_IN_PROGRESS = "in_progress";

// ----------------------------------------------------------------------------
// ref-counted object for callbacks
class JNIFFmpegMediaPlayerListener: public MediaPlayerListener
{
public:
    JNIFFmpegMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz);
    ~JNIFFmpegMediaPlayerListener();
    void notify(int msg, int ext1, int ext2);
    int getNextClip(char**);
private:
    JNIFFmpegMediaPlayerListener();
    jclass      mClass;     // Reference to MediaPlayer class
    jobject     mObject;    // Weak ref to MediaPlayer Java object to call on
};

JNIFFmpegMediaPlayerListener::JNIFFmpegMediaPlayerListener(JNIEnv* env, jobject thiz, jobject weak_thiz)
{
    // Hold onto the MediaPlayer class for use in calling the static method
    // that posts events to the application thread.
    jclass clazz = env->GetObjectClass(thiz);
    if (clazz == NULL) {
        jniThrowException(env, "java/lang/Exception", kClassPathName);
        return;
    }
    mClass = (jclass)env->NewGlobalRef(clazz);

#if 0
    // We use a weak reference so the MediaPlayer object can be garbage collected.
    // The reference is only used as a proxy for callbacks.
    mObject  = env->NewGlobalRef(weak_thiz);
#else
    mObject = (jobject) env->NewGlobalRef(thiz);
#endif
}

JNIFFmpegMediaPlayerListener::~JNIFFmpegMediaPlayerListener()
{
    // remove global references
    JNIEnv *env = getJNIEnv();
    env->DeleteGlobalRef(mObject);
    env->DeleteGlobalRef(mClass);
}

void JNIFFmpegMediaPlayerListener::notify(int msg, int ext1, int ext2)
{
    JNIEnv *env = getJNIEnv();
    //env->CallStaticVoidMethod(mClass, fields.post_event, mObject, msg, ext1, ext2, 0);
    env->CallVoidMethod(mObject, fields.post_event, mObject, msg, ext1, ext2, 0);
}

int JNIFFmpegMediaPlayerListener::getNextClip(char** url)
{
	int result = MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
	JNIEnv *env = getJNIEnv();

	jstring ret = (jstring) env->CallObjectMethod(mObject, fields.get_next_clip);
	const char *url_ret = env->GetStringUTFChars(ret, NULL);
	if (url_ret == NULL)
	{
		log_info("Get next clip: NULL\n");
		env->DeleteLocalRef(ret);
		return MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
	}

	log_info("Get next clip: %s\n", url_ret);
	int ret_code = strcmp(url_ret, STR_MEDIA_PLAYBACK_COMPLETE);
	if (ret_code == 0)
	{
		env->DeleteLocalRef(ret);
		return MEDIA_PLAYBACK_STATUS_COMPLETE;
	}

	ret_code = strcmp(url_ret, STR_MEDIA_PLAYBACK_IN_PROGRESS);
	if (ret_code == 0)
	{
		env->DeleteLocalRef(ret);
		return MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
	}

	*url = (char *) malloc(strlen(url_ret) + 1);
	if (*url != NULL)
	{
		strcpy(*url, url_ret);
		result = MEDIA_PLAYBACK_STATUS_STARTED;
	}
	else
	{
		result = MEDIA_PLAYBACK_STATUS_IN_PROGRESS;
	}

	env->ReleaseStringUTFChars(ret, url_ret);
	env->DeleteLocalRef(ret);

	return result;
}

// ----------------------------------------------------------------------------

static MediaPlayer* getMediaPlayer(JNIEnv* env, jobject thiz)
{
#if 0
    return (MediaPlayer*)env->GetIntField(thiz, fields.context);
#else
    return mediaPlayer;
#endif
}

static MediaPlayer* setMediaPlayer(JNIEnv* env, jobject thiz, MediaPlayer* player)
{
    MediaPlayer* old = (MediaPlayer*)env->GetIntField(thiz, fields.context);
    if (old != NULL) {
		log_info("freeing old mediaplayer object");
		free(old);
	}
    env->SetIntField(thiz, fields.context, (int)player);
    return old;
}

// If exception is NULL and opStatus is not OK, this method sends an error
// event to the client application; otherwise, if exception is not NULL and
// opStatus is not OK, this method throws the given exception to the client
// application.
static void process_media_player_call(JNIEnv *env, jobject thiz, status_t opStatus, const char* exception, const char *message)
{
    if (exception == NULL) {  // Don't throw exception. Instead, send an event.
		/*
        if (opStatus != (status_t) OK) {
            sp<MediaPlayer> mp = getMediaPlayer(env, thiz);
            if (mp != 0) mp->notify(MEDIA_ERROR, opStatus, 0);
        }
		*/
    } else {  // Throw exception!
        if ( opStatus == (status_t) INVALID_OPERATION ) {
            jniThrowException(env, "java/lang/IllegalStateException", "Native line 01" );
        } else if ( opStatus != (status_t) OK ) {
            if (strlen(message) > 230) {
               // if the message is too long, don't bother displaying the status code
               jniThrowException( env, exception, message);
            } else {
               char msg[256];
                // append the status code to the message
               sprintf(msg, "%s: status=0x%X", message, opStatus);
               jniThrowException( env, exception, msg);
            }
        }
    }
}

static void
    com_media_ffmpeg_FFMpegPlayer_setDataSourceAndHeaders(
        JNIEnv *env, jobject thiz, jstring path, jobject headers) {

#if 0
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", "player is null");
        return;
    }
#else

    if (mediaPlayer == NULL ) {

        jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
        return;
    }

    MediaPlayer * mp = mediaPlayer;


#endif
    if (path == NULL) {
        jniThrowException(env, "java/lang/IllegalArgumentException", "path is null");
        return;
    }

    const char *pathStr = env->GetStringUTFChars(path, NULL);
    if (pathStr == NULL) {  // Out of memory
        jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
        return;
    }

    log_info("setDataSource: path %s", pathStr);
    status_t opStatus = mp->setDataSource(pathStr);

    log_info("setDataSource: opstatus: %d", opStatus );

    // Make sure that local ref is released before a potential exception
    env->ReleaseStringUTFChars(path, pathStr);

    process_media_player_call(
            env, thiz, opStatus, "java/io/IOException",
            "setDataSource failed." );
}

static void
com_media_ffmpeg_FFMpegPlayer_setDataSource(JNIEnv *env, jobject thiz, jstring path)
{
	com_media_ffmpeg_FFMpegPlayer_setDataSourceAndHeaders(env, thiz, path, 0);
}

static void
com_media_ffmpeg_FFMpegPlayer_setVideoSurface(JNIEnv *env, jobject thiz, jobject jsurface)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
      jniThrowException(env, "java/lang/IllegalStateException", NULL);
      return;
    }
    if (jsurface == NULL ) {
      jniThrowException(env, "java/lang/IllegalStateException", NULL);
      return;
    }
	process_media_player_call( env, thiz, mp->setVideoSurface(env, thiz, jsurface),
							  "java/io/IOException", "Set video surface failed.");
}

static void
com_media_ffmpeg_FFMpegPlayer_prepare(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->prepare(env, thiz), "java/io/IOException", "Prepare failed." );
}

static void
com_media_ffmpeg_FFMpegPlayer_start(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->start(), NULL, NULL );
}

static void
com_media_ffmpeg_FFMpegPlayer_stop(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->stop(), NULL, NULL );
}

static void
com_media_ffmpeg_FFMpegPlayer_pause(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->pause(), NULL, NULL );
}

static jboolean
com_media_ffmpeg_FFMpegPlayer_isPlaying(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return false;
    }
    const jboolean is_playing = mp->isPlaying();
    return is_playing;
}

static jboolean
com_media_ffmpeg_FFMpegPlayer_isRecording(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return false;
    }
    const jboolean is_recording = mp->isRecording();
    return is_recording;
}

static void
com_media_ffmpeg_FFMpegPlayer_seekTo(JNIEnv *env, jobject thiz, int msec)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->seekTo(msec), NULL, NULL );
}

static int
com_media_ffmpeg_FFMpegPlayer_getVideoWidth(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return 0;
    }
    int w;
    if (0 != mp->getVideoWidth(&w)) {
        w = 0;
    }
    return w;
}

static int
com_media_ffmpeg_FFMpegPlayer_getVideoHeight(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return 0;
    }
    int h;
    if (0 != mp->getVideoHeight(&h)) {
        h = 0;
    }
    return h;
}


static int
com_media_ffmpeg_FFMpegPlayer_getCurrentPosition(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return 0;
    }
    int msec;
    process_media_player_call( env, thiz, mp->getCurrentPosition(&msec), NULL, NULL );
    return msec;
}

static int
com_media_ffmpeg_FFMpegPlayer_getCurrentStreamPosition(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return 0;
    }
    int msec;
    process_media_player_call( env, thiz, mp->getCurrentStreamPosition(&msec), NULL, NULL );
    return msec;
}

static int
com_media_ffmpeg_FFMpegPlayer_getDuration(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return 0;
    }
    int msec;
    process_media_player_call( env, thiz, mp->getDuration(&msec), NULL, NULL );
    return msec;
}

static void
com_media_ffmpeg_FFMpegPlayer_setDuration(JNIEnv *env, jobject thiz, jint duration)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->setDuration(duration), NULL, NULL );
}

static void
com_media_ffmpeg_FFMpegPlayer_reset(JNIEnv *env, jobject thiz)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->reset(), NULL, NULL );
}

static void
com_media_ffmpeg_FFMpegPlayer_setAudioStreamType(JNIEnv *env, jobject thiz, int streamtype)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->setAudioStreamType(streamtype) , NULL, NULL );
}

static void com_media_ffmpeg_FFMpegPlayer_setPlayOptions (JNIEnv * env, jobject thiz, jint opts)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return;
    }
    process_media_player_call( env, thiz, mp->setPlayOption(opts) , NULL, NULL );

}



// ----------------------------------------------------------------------------

static void
com_media_ffmpeg_FFMpegPlayer_native_init(JNIEnv *env)
{
	log_info("native_init");
    jclass clazz;
    clazz = env->FindClass("com/media/ffmpeg/FFMpegPlayer");
    if (clazz == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find android/media/MediaPlayer");
        return;
    }

    fields.get_next_clip = env->GetMethodID(clazz, "getNextUrl", "()Ljava/lang/String;");
    if (fields.get_next_clip == NULL) {
    	log_info("Can't find FFMpegMediaPlayer.getNextUrl");
    	jniThrowException(env, "java/lang/RuntimeException", "Can't find FFMpegMediaPlayer.getNextUrl");
    	return;
    }

    fields.context = env->GetFieldID(clazz, "mNativeContext", "I");
    if (fields.context == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find MediaPlayer.mNativeContext");
        return;
    }

#if 0
    fields.post_event = env->GetStaticMethodID(clazz, "postEventFromNative",
                                                   "(Ljava/lang/Object;IIILjava/lang/Object;)V");
    if (fields.post_event == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find FFMpegMediaPlayer.postEventFromNative");
        return;
    }
#else
    fields.post_event = env->GetMethodID(clazz, "postEventFromNative",
                                                   "(Ljava/lang/Object;IIILjava/lang/Object;)V");
    if (fields.post_event == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Can't find FFMpegMediaPlayer.postEventFromNative");
        return;
    }
#endif



}

static void
com_media_ffmpeg_FFMpegPlayer_native_setup(JNIEnv *env, jobject thiz, jobject weak_this,
		jboolean forPlayback, jboolean forSharedCam)
{
	log_info("native_setup");

	int k = 0;
	while (mediaPlayer != NULL && k++ < 20)
	{
		usleep(500000);
	}

	if (mediaPlayer != NULL)
	{
		/* Maybe wrong somewhere, try to delete it. */
		__android_log_print(ANDROID_LOG_ERROR, TAG,
				"Maybe wrong somewhere, try to release old player first.\n");
		mediaPlayer->suspend();
		mediaPlayer->stop();
		delete mediaPlayer;
		mediaPlayer = NULL;
	}

    mediaPlayer = new MediaPlayer(forPlayback, forSharedCam);
    //Create a global media player here
    if(mediaPlayer == NULL)
    {
        jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
        return;
    }
    // create new listener and give it to MediaPlayer
    JNIFFmpegMediaPlayerListener* listener = new JNIFFmpegMediaPlayerListener(env, thiz, weak_this);
    if(listener == NULL)
    {
    	jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
    	return;
    }
    mediaPlayer->setListener(listener);
    mediaPlayer->setAudioBufferCallBack(env, thiz);


#if 0
    MediaPlayer* mp = new MediaPlayer();
    if (mp == NULL) {
        jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
        return;
    }
    // create new listener and give it to MediaPlayer
    JNIFFmpegMediaPlayerListener* listener = new JNIFFmpegMediaPlayerListener(env, thiz, weak_this);
    mp->setListener(listener);

    // Stow our new C++ MediaPlayer in an opaque field in the Java object.
    setMediaPlayer(env, thiz, mp);
#endif
}

static void
com_media_ffmpeg_FFMpegPlayer_release(JNIEnv *env, jobject thiz)
{
	/*
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp != NULL) {
        // this prevents native callbacks after the object is released
        mp->setListener(0);
        mp->disconnect();
    }
	*/
	if (mediaPlayer != NULL)
	{
		delete mediaPlayer;
		mediaPlayer = NULL;
		env->SetIntField(thiz, fields.context, (int)NULL);
	}
	else
	{
		log_info("Media Player null, not need to release...\n");
	}
}

static void
com_media_ffmpeg_FFMpegPlayer_native_finalize(JNIEnv *env, jobject thiz)
{
    log_info("native_finalize");
    com_media_ffmpeg_FFMpegPlayer_release(env, thiz);
}

static jint
com_media_ffmpeg_FFMpegPlayer_native_suspend_resume(
        JNIEnv *env, jobject thiz, jboolean isSuspend) {
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp == NULL ) {
        jniThrowException(env, "java/lang/IllegalStateException", NULL);
        return UNKNOWN_ERROR;
    }

    return isSuspend ? mp->suspend() : mp->resume();
}

static jbyteArray
com_media_ffmpeg_FFMpegPlayer_native_getSnapShot(JNIEnv *env, jobject thiz)
{
	int len;
	void* buffer;
	int ret;
	jbyteArray bytes = NULL;
	ret = mediaPlayer->getSnapShot(&buffer, &len);
	if (ret == 0)
	{
		bytes = env->NewByteArray(len);
		if (bytes != NULL) {
			env->SetByteArrayRegion(bytes, 0, len, (jbyte*) buffer);
		}
	}

	return bytes;
}
static void
com_media_ffmpeg_FFMpegPlayer_native_startRecord(JNIEnv * env, jobject thiz, jstring fileName)
{
    if (fileName == NULL)
    {
        jniThrowException(env, "java/lang/IllegalArgumentException", "fileName is NULL");
        return;
    }

    const char * pathStr   = env->GetStringUTFChars(fileName , NULL);
    if (pathStr == NULL) {  // Out of memory
        jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
        return;
    }

    log_info("startRecording: path %s", pathStr);
    status_t opStatus = mediaPlayer->startRecord(pathStr);


    // Make sure that local ref is released before a potential exception
    env->ReleaseStringUTFChars(fileName, pathStr);

    process_media_player_call(
            env, thiz, opStatus, "java/io/IOException",
            "startRecord  failed." );
    return;
}


static void
com_media_ffmpeg_FFMpegPlayer_native_stopRecord(JNIEnv *env, jobject thiz)
{

    log_info("stopRecording");

    status_t opStatus = mediaPlayer->stopRecord();

    return;
}

static void
com_media_ffmpeg_FFMpegPlayer_native_updateAudioClk(JNIEnv *env, jobject thiz, jdouble pts)
{

    //log_info("Update audio clock from PCMPlayer: %f", pts);

    status_t opStatus;
    if (mediaPlayer != NULL)
    {
    	opStatus = mediaPlayer->updateAudioClk(pts);
    }

    return;
}

static void
com_media_ffmpeg_FFMpegPlayer_setEncryptionKey(JNIEnv *env, jobject thiz, jstring key)
{
	if (mediaPlayer == NULL ) {

		jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	if (key == NULL) {
		jniThrowException(env, "java/lang/IllegalArgumentException", "key is null");
		return;
	}

	const char *pathStr = env->GetStringUTFChars(key, NULL);
	if (pathStr == NULL) {  // Out of memory
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return;
	}

	mp->setEncryptionKey(pathStr);

	// Make sure that local ref is released before a potential exception
	env->ReleaseStringUTFChars(key, pathStr);
}

static void
com_media_ffmpeg_FFMpegPlayer_setEncryptionIv(JNIEnv *env, jobject thiz, jstring iv)
{
	if (mediaPlayer == NULL ) {

		jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	if (iv == NULL) {
		jniThrowException(env, "java/lang/IllegalArgumentException", "iv is null");
		return;
	}

	const char *pathStr = env->GetStringUTFChars(iv, NULL);
	if (pathStr == NULL) {  // Out of memory
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return;
	}

	mp->setEncryptionIv(pathStr);

	// Make sure that local ref is released before a potential exception
	env->ReleaseStringUTFChars(iv, pathStr);
}

static void
com_media_ffmpeg_FFMpegPlayer_setEncryptionEnable(JNIEnv *env, jobject thiz, jboolean encrypt_enabled)
{
	if (mediaPlayer == NULL ) {

		jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	mp->setEncryptionEnable(encrypt_enabled);
}

static void
com_media_ffmpeg_FFMpegPlayer_setAudioEnabled(JNIEnv *env, jobject thiz, jboolean isEnabled)
{
	if (mediaPlayer == NULL ) {

		jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	mp->setAudioEnabled(isEnabled);
}

static void
com_media_ffmpeg_FFMpegPlayer_setBufferSize(JNIEnv *env, jobject thiz, jint size_in_kb)
{
	if (mediaPlayer == NULL ) {

		jniThrowException(env, "java/lang/IllegalArgumentException", "player is null");
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	mp->setBufferSize(size_in_kb);
}

// ----------------------------------------------------------------------------

static jint
com_media_ffmpeg_FFMpegPlayer_decryptFile(JNIEnv *env, jobject thiz, jstring filePath, jstring key, jstring iv)
{
	if (filePath == NULL)
	{
		jniThrowException(env, "java/lang/IllegalArgumentException", "path is null");
		return -1;
	}

	if (key == NULL)
	{
		jniThrowException(env, "java/lang/IllegalArgumentException", "key is null");
		return -1;
	}

	if (iv == NULL)
	{
		jniThrowException(env, "java/lang/IllegalArgumentException", "iv is null");
		return -1;
	}

	const char *pathStr = env->GetStringUTFChars(filePath, NULL);
	if (pathStr == NULL) {  // Out of memory
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return -1;
	}

	const char *keyStr = env->GetStringUTFChars(key, NULL);
	if (pathStr == NULL) {  // Out of memory
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return -1;
	}

	const char *ivStr = env->GetStringUTFChars(iv, NULL);
	if (pathStr == NULL) {  // Out of memory
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return -1;
	}
	log_info("decrypte file %s\n", pathStr);
	log_info("decrypte file key %s and iv %s\n", keyStr, ivStr);

	EncryptionContext* enc_ctx = new EncryptionContext();
	enc_ctx->prepare(keyStr, ivStr);
	enc_ctx->decryptFile(pathStr);
	delete enc_ctx;
	// Make sure that local ref is released before a potential exception
	env->ReleaseStringUTFChars(filePath, pathStr);
	env->ReleaseStringUTFChars(key, keyStr);
	env->ReleaseStringUTFChars(iv, ivStr);

	return 0;
}


static void
com_media_ffmpeg_FFMpegPlayer_checkAndFlushAllBuffers(JNIEnv *env, jobject thiz)
{
	if (mediaPlayer == NULL ) {
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	mp->checkAndFlushAllBuffers();
}

static void
com_media_ffmpeg_FFMpegPlayer_flushAllBuffers(JNIEnv *env, jobject thiz)
{
	if (mediaPlayer == NULL ) {
		return;
	}

	MediaPlayer * mp = mediaPlayer;
	mp->flushAllBuffers();
}

static void
com_media_ffmpeg_FFMpegPlayer_setP2PInfo(JNIEnv *env, jobject thiz,
		jobjectArray jp2pClients)
{
	if (mediaPlayer == NULL ) {
		log_error("Set p2p info, media player is NULL\n");
		return;
	}

	MediaPlayer * mp = mediaPlayer;

	jsize size = env->GetArrayLength(jp2pClients);
	log_error("Set p2p info, p2p clients %d\n", size);
	mp->setP2pSessionCount(size);
	if (size > 0)
	{
		/* Get some method ID of P2pClient class */
		jclass clazz = env->FindClass("com/nxcomm/jstun_android/P2pClient");
		jmethodID getRmcChannelPtrMid;
		getRmcChannelPtrMid = env->GetMethodID(clazz, "getRmcChannelPtr", "()J");

#if 0
		jmethodID getDestIp_mid, getDestPort_mid, getRandomNumber_mid, getEncKey_mid, getMac_mid, getP2pMode_mid, getSockFd_mid;
		getDestIp_mid = env->GetMethodID(clazz, "getDestIp", "()Ljava/lang/String;");
		getDestPort_mid = env->GetMethodID(clazz, "getDestPort", "()I");
		getRandomNumber_mid = env->GetMethodID(clazz, "getRandomNumber", "()Ljava/lang/String;");
		getEncKey_mid = env->GetMethodID(clazz, "getEncKey", "()Ljava/lang/String;");
		getMac_mid = env->GetMethodID(clazz, "getMac", "()Ljava/lang/String;");
		getP2pMode_mid = env->GetMethodID(clazz, "getP2pMode", "()I");
		getSockFd_mid = env->GetMethodID(clazz, "getSockFd", "()I");
#endif

//		jstring jdestIp, jrandomNumber, jencKey, jmac;

//		ConnectionInfo **connInfo = (ConnectionInfo **) malloc(size * sizeof(ConnectionInfo *));
		long *rmcChannelInfos = (long *) malloc(size * sizeof(long));
		if (rmcChannelInfos != NULL)
		{
			for (int i=0; i<size; i++)
			{
				jobject p2pClient = env->GetObjectArrayElement(jp2pClients, i);
				long rmcChannelInfo = env->CallLongMethod(p2pClient, getRmcChannelPtrMid);
				rmcChannelInfos[i] = rmcChannelInfo;

#if 0
				/* Get value of p2p client from Java object. */
				jdestIp = (jstring) env->CallObjectMethod(p2pClient, getDestIp_mid);
				const char *destIp = env->GetStringUTFChars(jdestIp, NULL);
				//log_error("destIp %s\n", destIp);
				connInfo[i]->src_ip = (char *) malloc(strlen(destIp) + 1);
				strcpy(connInfo[i]->src_ip, destIp);
				env->ReleaseStringUTFChars(jdestIp, destIp);

				jrandomNumber = (jstring) env->CallObjectMethod(p2pClient, getRandomNumber_mid);
				const char *randomNumber = env->GetStringUTFChars(jrandomNumber, NULL);
				//log_error("randomNumber %s\n", randomNumber);
				connInfo[i]->random_number = (char *) malloc(strlen(randomNumber) + 1);
				strcpy(connInfo[i]->random_number, randomNumber);
				env->ReleaseStringUTFChars(jrandomNumber, randomNumber);

				jencKey = (jstring) env->CallObjectMethod(p2pClient, getEncKey_mid);
				const char *encKey = env->GetStringUTFChars(jencKey, NULL);
				//log_error("encKey %s\n", encKey);
				connInfo[i]->enc_key = (char *) malloc(strlen(encKey) + 1);
				strcpy(connInfo[i]->enc_key, encKey);
				env->ReleaseStringUTFChars(jencKey, encKey);


				jmac = (jstring) env->CallObjectMethod(p2pClient, getMac_mid);
				const char *mac = env->GetStringUTFChars(jmac, NULL);
				//log_error("mac %s\n", mac);
				connInfo[i]->mac = (char *) malloc(strlen(mac) + 1);
				strcpy(connInfo[i]->mac, mac);
				env->ReleaseStringUTFChars(jmac, mac);


				connInfo[i]->port = env->CallIntMethod(p2pClient, getDestPort_mid);
				connInfo[i]->p2p_mode = (P2pMode) env->CallIntMethod(p2pClient, getP2pMode_mid);
				connInfo[i]->sock_fd = env->CallIntMethod(p2pClient, getSockFd_mid);
				//log_error("port %d, p2p_mode %d, sock_fd %d\n",
				//		connInfo[i]->port, connInfo[i]->p2p_mode, connInfo[i]->sock_fd);

				env->DeleteLocalRef(jdestIp);
				env->DeleteLocalRef(jrandomNumber);
				env->DeleteLocalRef(jencKey);
				env->DeleteLocalRef(jmac);
#endif

				env->DeleteLocalRef(p2pClient);
			}

		}

		mp->setRmcChannelInfo((void *)rmcChannelInfos[0]);

		env->DeleteLocalRef(clazz);
	} else {
	  log_error("Set p2p info, p2p clients are empty\n");
	}
}


static void
com_media_ffmpeg_FFMpegPlayer_setUIDs(JNIEnv *env, jobject thiz, jobjectArray UIDs, jint numId)
{
	jstring string[4];
	char **cstring;
	cstring = (char **) malloc(numId * sizeof(char *));

	for (int i=0; i<numId; i++)
	{
		string[i] = (jstring) env->GetObjectArrayElement(UIDs, i);
		cstring[i] = (char *) env->GetStringUTFChars(string[i], 0);
	}

	MediaPlayer * mp = mediaPlayer;
	if (mp == NULL)
	{
		log_error("set UDIDs, media player is NULL\n");
	}
	else
	{
		mp->setUIDs(cstring, numId);
	}

	//release resources
	for (int i=0; i<numId; i++)
	{
		env->ReleaseStringUTFChars(string[i], cstring[i]);
	}
	free(cstring);
}

static jstring
com_media_ffmpeg_FFMpegPlayer_sendCommand(JNIEnv *env, jobject thiz, jstring jrequest)
{
	const char *request = env->GetStringUTFChars(jrequest, NULL);
	if (request == NULL)
	{
		jniThrowException(env, "java/lang/RuntimeException", "Out of memory");
		return NULL;
	}

	char *response = NULL;
	MediaPlayer * mp = mediaPlayer;
	if (mp == NULL)
	{
		log_error("sendCommand, media player is NULL\n");
		return NULL;
	}

	mp->sendCommand(request, &response);
	log_info("sendCommand res: %s\n", response);
	env->ReleaseStringUTFChars(jrequest, request);
	jstring jresponse = env->NewStringUTF(response);

	return jresponse;
}


static void
com_media_ffmpeg_FFMpegPlayer_sendTalkbackData(JNIEnv *env, jobject thiz, jbyteArray jtalkbackData, jint joffset, jint jlength)
{
	//jbyte *talkbackData = env->GetByteArrayElements(jtalkbackData, NULL);
	memset(talkbackData, 0, sizeof(talkbackData));
	env->GetByteArrayRegion(jtalkbackData, joffset, jlength, talkbackData);
	if (talkbackData != NULL)
	{
		MediaPlayer * mp = mediaPlayer;
		if (mp == NULL)
		{
			log_error("sendTalkbackData, media player is NULL\n");
		}
		else
		{
			mp->sendTalkbackData((uint8_t *)talkbackData, 0, jlength);
		}
	}
	else
	{
		log_error("sendTalkbackData, invalid talkback data: NULL\n");
	}
}

static jboolean
com_media_ffmpeg_FFMpegPlayer_isP2pConnected(JNIEnv *env, jobject thiz)
{
	bool is_connected = false;
	MediaPlayer *mp = mediaPlayer;
	if (mp != NULL)
	{
		is_connected = mp->isP2pConnected();
	}
	return is_connected;
}

static void
com_media_ffmpeg_FFMpegPlayer_native_setRecordModeEnabled(JNIEnv *env, jobject thiz, jboolean isEnabled)
{
  if (mediaPlayer == NULL ) {
    jniThrowException(env, "java/lang/IllegalArgumentException", "MediaPlayer object is not initialized");
    return;
  }

  MediaPlayer * mp = mediaPlayer;
  mp->setRecordModeEnabled(isEnabled);
}

static void
com_media_ffmpeg_FFMpegPlayer_native_setP2pPlayByTimestampEnabled(JNIEnv *env, jobject thiz, jboolean isEnabled)
{
  if (mediaPlayer == NULL ) {

    jniThrowException(env, "java/lang/IllegalStateException", "player is null");
    return;
  }

  MediaPlayer * mp = mediaPlayer;
  mp->setP2pPlayByTimestampEnabled(isEnabled);
}

static void
com_media_ffmpeg_FFMpegPlayer_setBackgroundModeEnabled(JNIEnv *env, jobject thiz, jboolean isEnabled, jobject jsurface)
{
  if (mediaPlayer == NULL ) {
    jniThrowException(env, "java/lang/IllegalStateException", "MediaPlayer object is not initialized");
    return;
  }

  MediaPlayer * mp = mediaPlayer;
  if (isEnabled == false) {
    mp->resetVideoSurface(env, thiz, jsurface);
  }
  mp->setBackgroundModeEnabled(isEnabled);
}

static void
com_media_ffmpeg_FFMpegPlayer_native_setProbeSize(JNIEnv *env, jobject thiz, jlong probeSizeInKB)
{
    MediaPlayer* mp = getMediaPlayer(env, thiz);
    if (mp != NULL) {
    	mp->setProbeSize(probeSizeInKB);
    } else {
    	log_error("Set probe size failed, player is NULL\n");
    }
}

static JNINativeMethod gMethods[] = {
	{"flushAllBuffers",         "()V",                  		(void *)com_media_ffmpeg_FFMpegPlayer_flushAllBuffers},
	{"checkAndFlushAllBuffers",         "()V",                  (void *)com_media_ffmpeg_FFMpegPlayer_checkAndFlushAllBuffers},
	{"setBufferSize", 		"(I)V",                     		(void*) com_media_ffmpeg_FFMpegPlayer_setBufferSize},
	{"setAudioEnabled", 	"(Z)V",                     		(void*) com_media_ffmpeg_FFMpegPlayer_setAudioEnabled},
	{"setEncryptionEnable", "(Z)V",                     		(void*) com_media_ffmpeg_FFMpegPlayer_setEncryptionEnable},
	{"setEncryptionIv",    "(Ljava/lang/String;)V",            	(void *)com_media_ffmpeg_FFMpegPlayer_setEncryptionIv},
	{"setEncryptionKey",    "(Ljava/lang/String;)V",            (void *)com_media_ffmpeg_FFMpegPlayer_setEncryptionKey},
    {"setDataSource",       "(Ljava/lang/String;)V",            (void *)com_media_ffmpeg_FFMpegPlayer_setDataSource},
    {"_setVideoSurface",    "(Landroid/view/Surface;)V",        (void *)com_media_ffmpeg_FFMpegPlayer_setVideoSurface},
    {"_prepare",             "()V",                             (void *)com_media_ffmpeg_FFMpegPlayer_prepare},
    {"_start",              "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_start},
    {"_stop",               "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_stop},
    {"getVideoWidth",       "()I",                              (void *)com_media_ffmpeg_FFMpegPlayer_getVideoWidth},
    {"getVideoHeight",      "()I",                              (void *)com_media_ffmpeg_FFMpegPlayer_getVideoHeight},
    {"seekTo",              "(I)V",                             (void *)com_media_ffmpeg_FFMpegPlayer_seekTo},
    {"_pause",              "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_pause},
    {"isPlaying",           "()Z",                              (void *)com_media_ffmpeg_FFMpegPlayer_isPlaying},
    {"isRecording",           "()Z",                            (void *)com_media_ffmpeg_FFMpegPlayer_isRecording},
    {"getCurrentPosition",  "()I",                              (void *)com_media_ffmpeg_FFMpegPlayer_getCurrentPosition},
    {"getCurrentStreamPosition",  "()I",                        (void *)com_media_ffmpeg_FFMpegPlayer_getCurrentStreamPosition},
    {"getDuration",         "()I",                              (void *)com_media_ffmpeg_FFMpegPlayer_getDuration},
    {"setDuration",         "(I)V",                             (void *)com_media_ffmpeg_FFMpegPlayer_setDuration},
    {"_release",            "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_release},
    {"_reset",              "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_reset},
    {"setAudioStreamType",  "(I)V",                             (void *)com_media_ffmpeg_FFMpegPlayer_setAudioStreamType},
    {"native_init",         "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_native_init},
    {"native_setup",        "(Ljava/lang/Object;ZZ)V",          (void *)com_media_ffmpeg_FFMpegPlayer_native_setup},
    {"native_finalize",     "()V",                              (void *)com_media_ffmpeg_FFMpegPlayer_native_finalize},
    {"native_suspend_resume", "(Z)I",                           (void *)com_media_ffmpeg_FFMpegPlayer_native_suspend_resume},
    {"setPlayOption", "(I)V",                                  (void*) com_media_ffmpeg_FFMpegPlayer_setPlayOptions},
    {"native_getSnapShot", 		"()[B", 							(void*) com_media_ffmpeg_FFMpegPlayer_native_getSnapShot},
    {"native_startRecord", "(Ljava/lang/String;)V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_startRecord},
    {"native_stopRecord", "()V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_stopRecord},
    {"native_updateAudioClk", "(D)V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_updateAudioClk},
	{"decryptFile", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I", (void*) com_media_ffmpeg_FFMpegPlayer_decryptFile},
	{"setP2PInfo",    "([Lcom/nxcomm/jstun_android/P2pClient;)V", (void *)com_media_ffmpeg_FFMpegPlayer_setP2PInfo},
	{"setUIDs",    "([Ljava/lang/String;I)V",            	(void *)com_media_ffmpeg_FFMpegPlayer_setUIDs},
	{"sendCommand",    "(Ljava/lang/String;)Ljava/lang/String;",	(void *)com_media_ffmpeg_FFMpegPlayer_sendCommand},
	{"sendTalkbackData",    "([BII)V",	(void *)com_media_ffmpeg_FFMpegPlayer_sendTalkbackData},
	{"isP2pConnected",    "()Z",	(void *)com_media_ffmpeg_FFMpegPlayer_isP2pConnected},
	{"setRecordModeEnabled", "(Z)V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_setRecordModeEnabled},
	{"setP2pPlayByTimestampEnabled", "(Z)V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_setP2pPlayByTimestampEnabled},
	{"setBackgroundModeEnabled", "(ZLandroid/view/Surface;)V",             (void*) com_media_ffmpeg_FFMpegPlayer_setBackgroundModeEnabled},
//	{"native_setProbeSize", "(J)V",             (void*) com_media_ffmpeg_FFMpegPlayer_native_setProbeSize}
};

int register_android_media_FFMpegPlayerAndroid(JNIEnv *env) {
	return jniRegisterNativeMethods(env, kClassPathName, gMethods, sizeof(gMethods) / sizeof(gMethods[0]));
}
