LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

IN_NDK := true

LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS -DANDROID_BUILD

WITH_CONVERTOR := false
WITH_PLAYER := true
USE_FFMPEG_3_0 := true

ifeq ($(WITH_PLAYER),true)
LOCAL_CFLAGS += -DBUILD_WITH_PLAYER
endif

ifeq ($(WITH_CONVERTOR),true)
LOCAL_CFLAGS += -DBUILD_WITH_CONVERTOR
endif

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../libmediaplayer \
    $(LOCAL_PATH)/../include \
    $(LOCAL_PATH)/../include/rmc_inc
ifeq ($(USE_FFMPEG_3_0),true)
LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../ffmpeg-3.0.2
else
LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../ffmpeg
endif

LOCAL_SRC_FILES := \
    onLoad.cpp 

#    I just need the player -- all these are not use 
#    com_media_ffmpeg_FFMpegAVFrame.cpp \
#    com_media_ffmpeg_FFMpegAVInputFormat.c \
#    com_media_ffmpeg_FFMpegAVRational.c \
#    com_media_ffmpeg_FFMpegAVFormatContext.c \
#    com_media_ffmpeg_FFMpegAVCodecContext.cpp \
#    com_media_ffmpeg_FFMpegUtils.cpp

ifeq ($(WITH_CONVERTOR),true)
LOCAL_SRC_FILES += \
    com_media_ffmpeg_FFMpeg.c \
    ../libffmpeg/cmdutils.c
endif
		
ifeq ($(WITH_PLAYER),true)
LOCAL_SRC_FILES += \
    com_media_ffmpeg_FFMpegPlayer.cpp
#com_media_ffmpeg_android_FFMpegPlayerAndroid.cpp
endif

ifeq ($(IN_NDK),true)	
LOCAL_LDLIBS := -llog -landroid -lOpenSLES 
#LOCAL_LDLIBS := -llog -landroid -lOpenSLES -lOpenMAXAL
else
LOCAL_PRELINK_MODULE := false
LOCAL_SHARED_LIBRARIES := liblog
endif

LOCAL_SHARED_LIBRARIES :=  #libjniaudio libjnivideo
LOCAL_STATIC_LIBRARIES := libmediaplayer libavcodec libavformat libavutil libpostproc libswscale 

LOCAL_MODULE := libffmpeg_jni

include $(BUILD_SHARED_LIBRARY)
