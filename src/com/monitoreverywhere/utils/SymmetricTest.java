/*
 * Included By Ankita
 * 
 * */

package com.monitoreverywhere.utils;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.PrivilegedAction;

import android.util.Log;
//import bms.stream.Viewer;
//import bms.utils.HTTPConnection;
//import bms.utils.PropertiesReader;

import com.barchart.udt.ExceptionUDT;
import com.barchart.udt.SocketUDT;
import com.barchart.udt.TypeUDT;

public class SymmetricTest
{

//	private ExceptionReporter m_reporter = null;
//	private CamInfo cam_info = null;
//	public SymmetricTest(ExceptionReporter m_reporter, CamInfo cam_info)
//	{
//		this.cam_info = cam_info;
//		this.m_reporter = m_reporter;
//	}
	public SymmetricTest()
	{
//		this.cam_info = new CamInfo();
	}
//	private final static  String SERVER_IP1 =PropertiesReader.readValueOf("server1_ip");
//    private final static  String SERVER_IP2 =PropertiesReader.readValueOf("server2_ip");
//    private final static String NAT_DELIMiTER =PropertiesReader.readValueOf("delimiter");
//    private final static  int SERVER_PORT1 =Integer.parseInt(PropertiesReader.readValueOf("server1_port"));
//    private final static  int SERVER_PORT2 =Integer.parseInt(PropertiesReader.readValueOf("server2_port"));
//    private final static  short retryLimit=Short.parseShort(PropertiesReader.readValueOf("nat_retry"));
//    private final static  short socketTimeout=Short.parseShort(PropertiesReader.readValueOf("socket-timeout"));
    
    //for test
    private final static  String SERVER_IP1 = "nat1.monitoreverywhere.com";
    private final static  String SERVER_IP2 = "nat2.monitoreverywhere.com";
    private final static String NAT_DELIMiTER = "::";
    private final static  int SERVER_PORT1 = 9999;
    private final static  int SERVER_PORT2 = 9999;
    private final static  short retryLimit= 3;
    private final static  short socketTimeout= 5000;
	
	public SocketUDT sock =null; 
	private int localPort =0;
    public String doNATTest(final String SERVER_IP,final int SERVER_PORT)
    {
        short responseLength =30;
        byte[] responseByte = new byte[responseLength];
        String responseSring = null;
        for(short j=0;j<retryLimit;j++)
        try 
        {
            //clears byte array
            for(short k=0;k<responseLength;k++)
                responseByte[k]=0;
            sock = new SocketUDT(TypeUDT.STREAM);
            sock.setSoTimeout(socketTimeout);
            try
            {    
                sock.bind(new InetSocketAddress(localPort));
//                log.info("bfre connect");
                sock.connect(new InetSocketAddress(SERVER_IP,SERVER_PORT));
//                log.info("aftr connect");
            }
            catch(AccessControlException ex)
            {
                final int localPrivilagedPort = localPort;
                AccessController.doPrivileged(new PrivilegedAction<String>( )
                {
                    @Override
                    public String run( )
                    {
                        try 
                        {
                            sock.bind(new InetSocketAddress(localPrivilagedPort));
                            sock.connect(new InetSocketAddress(SERVER_IP,SERVER_PORT));
                        }
                        catch (ExceptionUDT ex1) 
                        {
//                            log.error("Error:{}",ex1);
                        }
                        catch(Exception ex)
                        {
//                            log.error("Error:{}",ex);
                        }
                        return null;
                    }
                });
            }
            catch(Exception ex)
            {
//            	log.error("Unknown Exception:",ex);
            }
            sock.send("NATTEST".getBytes());
            sock.receive(responseByte);
            responseSring = new String(responseByte,"UTF-8");
            localPort = sock.getLocalInetPort();
            break;
        } catch (ExceptionUDT ex) 
        {
//            log.error("Error:{}",ex);
        } catch (UnsupportedEncodingException ex) {
//            log.error("Error:{}",ex);
        }
        finally
        {
            try {
                if(sock!=null)
                sock.close();
            } catch (ExceptionUDT ex) {
//                log.error("Error in close{}",ex);
            }
        }
        return responseSring;
    }
    
//    public CamInfo getStreamingCoordinate()
//    {
//        String responseServer1 = doNATTest( SERVER_IP1, SERVER_PORT1);
//        log.info("NAT1 result:"+responseServer1);
//        String responseServer2 = doNATTest( SERVER_IP2, SERVER_PORT2);
//        log.info("NAT2 result:"+responseServer2);
//        cam_info.setInternal_browser_port(localPort);
//        if(responseServer1!=null && responseServer2!=null)
//        {
//            log.info("NATTest OK");
//        }
//        else if(responseServer2!=null)
//        {
//        	if(responseServer1==null)
//            {
//                log.warn("Server1 test failed, backup startegy");
//                responseServer1 = responseServer2;
//            }
//        }
//        else if(responseServer1!=null)
//        {
//        	if(responseServer2==null)
//            {
//                log.warn("Server2 test failed, backup startegy");
//                responseServer2 = responseServer1;
//            }
//        }
//        else
//        {
//        	log.warn("Server1  and Server2 test failed");
//            cam_info.setIs_relay_status(true);
//            return cam_info;
//        }
//        try
//        {
//        	String[] iP_Port1 = responseServer1.split(NAT_DELIMiTER);
//        	String[] iP_Port2 = responseServer2.split(NAT_DELIMiTER);
//        	if(iP_Port1[2].contentEquals(iP_Port2[2]) && iP_Port1[1].contentEquals(iP_Port2[1]))
//        	{
//        		log.info("non Symmetric ");
//        		cam_info.setBorwserExternalIP(iP_Port1[1]);
//        		cam_info.setBrowserExternalPort(Integer.parseInt(iP_Port1[2]));
//        		short i=0;
//        		while(i++<PublicDefine.MAX_HTTP_REQUEST)
//        		{
//        			if(get_cam_info())
//        				break;
//        			else if(i==PublicDefine.MAX_HTTP_REQUEST)
//        			{
//        				cam_info.setIs_relay_status(true);
//                    	return cam_info;
//        			}
//        				
//        		}
//        	}
//        	else
//        	{
//        		log.info("Symmetric");
//        		cam_info.setIs_relay_status(true);
//        	}
//        	 
//        }
//        catch(ArrayIndexOutOfBoundsException ex)
//        {
//        	log.error("error in NAT response",ex);
//        	cam_info.setIs_relay_status(true);
//        	return cam_info;
//        }
//        catch(Exception ex)
//        {
//        	log.error("Error in cam cordinate:",ex);
//        	cam_info.setIs_relay_status(true);
//        	return cam_info;
//        }
//        return cam_info;
//    }
	
	
	


//	private boolean get_cam_info()
//	{
//
//		HTTPConnection http = new HTTPConnection();
//		StringBuffer query = new StringBuffer();
//		query = query
//				.append("https://")
//				.append(Viewer.SERVERURL)
//				.append("/phoneservice?action=command&command=view_cam_udp&macAddr=");
//		query = query.append(cam_info.getMacAddress());
//		query = query.append("&clientIP=");
//		query = query.append(cam_info.getExternal_borwser_IP().trim());
//		query = query.append("&clientport=");
//		query = query.append(cam_info.getExternal_browser_port());
//		query = query.append("&channelID=");
//		query = query.append(cam_info.getChannelID());
//
//		log.info("URL :" + query);
//
//		String response = http.send_http_query(query.toString(),
//				Viewer.credentials, true, 0, 0);
//		int response_code = http.getResponsecode();
//		if (response_code == 200)
//		{
//			String[] tempstr = null;
//			
//			try
//			{
//				tempstr = response.split("<br>");
//				int len = tempstr.length;
//				String[] temp = null;
//				for (int i = 0; i < len; i++)
//				{
//					temp = tempstr[i].trim().split("=");
//					switch (i)
//					{
//					case 0:
//						cam_info.setCamera_IP(temp[1].trim().substring(1));
//						break;
//					case 1:
//						cam_info.setCamera_port(Integer.parseInt(temp[1].trim()));
//						break;
//					case 2:
//						cam_info.setSession_key(temp[1].trim());
//						break;
//
//					}
//
//				}
//
//				log.info("****************DETAILS***FROM****BMS***SERVER**************************");
//				log.info("CamerIP				:" + cam_info.getCamera_IP());
//				log.info("CameraPort			:" + cam_info.getCamera_port());
//				log.info("Sessionkey			:" + cam_info.getSession_key());
//				log.info("Channelid			:" + cam_info.getChannelID());
//
//				log.info("Browser Ip			:" + cam_info.getExternal_borwser_IP());
//
//				log.info("************************************************************************");
//
//			} catch (ArrayIndexOutOfBoundsException ex)
//			{
//				log.warn("ArrayIndexOutOfBoundsException-Output :",ex);
//				cam_info.setCamera_IP("0");
//				cam_info.setSession_key(String.format("%0128X", 0));
//				cam_info.setCamera_port(0);
//				return false;
//
//			}
//			catch(Exception ex)
//			{
//				log.warn("exception in getting view_cam query:",ex);
//				cam_info.setCamera_IP("0");
//				cam_info.setSession_key(String.format("%0128X", 0));
//				cam_info.setCamera_port(0);
//				return false;
//			}
//
//		} else if (response_code != -1)
//		{
//			log.warn("camera busy or not available:{}",response_code);
//			cam_info.setCamera_IP("0");
//			cam_info.setSession_key(String.format("%0128X", 0));
//			cam_info.setCamera_port(0);
//			Viewer.error_code = response_code; // response is not in valid
//												// format
//			m_reporter.openErrorCodes();
//		} else
//		{
//			return false;
//
//		}
//		return true;
//
//	}

	public static void main(String args[])
	{
		SymmetricTest sym_test = new SymmetricTest();
		try {
			String server_ip1 = InetAddress.getByName(SERVER_IP1).getHostAddress();
			Log.d("mbp", "server1: "+server_ip1);
			String server_ip2 = InetAddress.getByName(SERVER_IP2).getHostAddress();
			Log.d("mbp", "server2: "+server_ip2);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		Log.d("mbp", "@@@ Test SERVER1: "+sym_test.doNATTest(SERVER_IP1, SERVER_PORT1));
		Log.d("mbp", "@@@ Test SERVER2: "+sym_test.doNATTest(SERVER_IP2, SERVER_PORT2));
	}

}
