package com.blinkhd;

import com.msc3.CamChannel;
import com.msc3.DirectionDispatcher;
import com.msc3.DirectionTouchListener;
import com.msc3.DirectionTouchListener_bb;
import com.msc3.PublicDefine;
import com.nxcomm.blinkhd.ui.LeftSideMenuItemListener;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class DirectionFragment extends Fragment {
	
	private DirectionTouchListener_bb joystickListener;
	private DirectionDispatcher device_comm;
	private Thread device_comm_thrd;
	
	private LeftSideMenuItemListener leftSideMenuListener = null;
	private CamChannel selected_channel = null;
	private String device_ip = null;
	private int device_port = -1;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.direction_fragment, container, false);
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		setupDirectionPad();
	}
	
	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
		device_ip = selected_channel.getCamProfile().
				get_inetAddress().getHostAddress();
		device_port = selected_channel.getCamProfile().get_port();
	}
	
	private void setupDirectionPad() {
		// TODO Auto-generated method stub
		SharedPreferences settings = getActivity().getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		RelativeLayout directionLayout = (RelativeLayout) 
				getView().findViewById(R.id.panLayout);
		ImageView direction_pad = (ImageView) 
				getView().findViewById(R.id.imgPan);
		if (selected_channel.getCamProfile().isInLocal()) {
			if (device_ip != null && device_port != -1) {
				device_comm = new DirectionDispatcher("http://"
						+ device_ip + ":" + device_port);
			}
		} else {
			String saved_token = settings.getString(
					PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			if (saved_token != null) {
				device_comm = new DirectionDispatcher(saved_token,
						selected_channel.getCamProfile().getRegistrationId());
			}
		}


		joystickListener = new DirectionTouchListener_bb(getActivity(), new Handler(
				new Handler.Callback() {

					@Override
					public boolean handleMessage(Message msg) {
						switch (msg.what) {
						case DirectionTouchListener.MSG_JOYSTICK_IS_BEING_USED:
							// showJoystickOnly();
							//cancelFullscreenTimer();
							break;
						case DirectionTouchListener.MSG_JOYSTICK_IS_BEING_MOVED:
							//cancelFullscreenTimer();
							break;
						case DirectionTouchListener.MSG_JOYSTICK_IS_NOT_BEING_USED:
							//tryToGoToFullScreen();
							break;
						default:
							break;
						}

						return false;
					}

				}), direction_pad, directionLayout, device_comm);

		// joystickListener.setOrientation(shouldRotateBitmap);
		direction_pad.setOnTouchListener(joystickListener);

		device_comm_thrd = new Thread(device_comm, "DirectionHTTP");
		device_comm_thrd.start();
		if (joystickListener != null) {
			joystickListener.setDirectionDispatcher(device_comm);
		}
	}
	
	@Override
	public void onStop()
	{
	    // TODO Auto-generated method stub
	    super.onStop();
	    stopDirectionPadThread();
	}
	
	private void stopDirectionPadThread()
	{
		if (device_comm_thrd != null) {
			device_comm.terminate();
			boolean retry = true;
			while (retry) {
				try {
					device_comm_thrd.join(2000);
					retry = false;
				} catch (InterruptedException e) {
				}

			}
			device_comm_thrd = null;
		}
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof LeftSideMenuItemListener)
		{
			leftSideMenuListener = (LeftSideMenuItemListener) activity;
		}
		else
		{
			throw new ClassCastException(activity.toString() +
					" must implement LeftSideMenuItemListener.");
		}
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		leftSideMenuListener = null;
	}
}
