package com.blinkhd.MEAsyncTask;

import com.msc3.CamProfile;
import com.nxcomm.meapi.device.CameraInfo;

public interface LoadCameraListTaskListener 
{
	
	public  abstract void onPostExecute(CamProfile[] cameraProfile);
	public  abstract void onPreExecute();
}
