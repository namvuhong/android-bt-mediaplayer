package com.blinkhd.MEAsyncTask;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import android.os.AsyncTask;
import android.util.Log;

import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.device.CamListResponse;
import com.nxcomm.meapi.device.CameraInfo;
import com.nxcomm.meapi.device.DeviceFirmware;
import com.nxcomm.meapi.device.DeviceLocation;

public class LoadCameraListTask extends AsyncTask<String,Void,CamProfile[]>
{
	private LoadCameraListTaskListener listener;
	private int errorCode;
	public LoadCameraListTask(LoadCameraListTaskListener listener)
	{
		this.listener = listener;
		this.errorCode = 0;
	}
	public int getErrorCode()
	{
		return this.errorCode;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		this.errorCode = 0;
		this.listener.onPreExecute();
	}
	@Override
	protected void onPostExecute(CamProfile[] result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if( !this.isCancelled() )
		{
			this.listener.onPostExecute(result);
		}
	}

	@Override
	protected CamProfile[] doInBackground(String... params) {
		// TODO Auto-generated method stub
		CamProfile[] camProfile = null;
		CameraInfo[] cameraInfo = null;
		if(params.length > 0)
		{
			String bmsAccessToken = params[0];
			
			CamListResponse camListResponse;
			try {
				camListResponse = com.nxcomm.meapi.Device.getOwnCamList(bmsAccessToken);
				
				if (camListResponse.getStatus() == 200) 
				{
					cameraInfo = camListResponse.getCamList();
				}
			} catch (SocketTimeoutException e) {
				// TODO Auto-generated catch block
				this.errorCode = TaskError.SOCKET_TIMEOUT;
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				this.errorCode = TaskError.MALFORM_URL_FORMAT;
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				this.errorCode = TaskError.IO_EXCEPTION;
				e.printStackTrace();
			}
			
			try {
				camProfile = parse_cam_list_temp_new2(cameraInfo);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return camProfile;
	}
	
	private CamProfile[] parse_cam_list_temp_new2(CameraInfo[] caminfos) throws IOException, NumberFormatException
	{
		if (caminfos == null)
		{
			return null;
		}
		
		int total_cam = caminfos.length > 4 ? 4 : caminfos.length;
		CamProfile[] online_list = new CamProfile[total_cam];
		
		for (int i=0; i<total_cam; i++)
		{
			//get camera name
			String camName = caminfos[i].getName();
			
			//get mac address
			String MAC_addr = caminfos[i].getRegistration_id();
			if (MAC_addr.length() != 12)
			{
				//STH is wrong, set to default, to use the local mac address
				MAC_addr = "00:00:00:00:00:00";
			}
			else 
			{
				MAC_addr = PublicDefine.add_colon_to_mac(MAC_addr);
			}
			
			//get fw version
			DeviceFirmware camFwVersion_obj = caminfos[i].getDevice_firmware();
			String camFwVersion = null;
			if (camFwVersion_obj != null)
			{
				camFwVersion = camFwVersion_obj.getVersion();
			}
			
			//get model id
			int camModel = -1;
			camModel = caminfos[i].getDevice_model_id();
			
			int camId = -1;
			camId = caminfos[i].getId();
			
			//get is_available value
			boolean isAvailable = false;
			isAvailable = caminfos[i].isIs_available();
			
			InetAddress remote_addr = null;
			int remoteRtspPort = -1;
			int remoteRtpVideoPort = -1;
			int remoteRtpAudioPort = -1;
			int remoteTalkBackPort = -1;
			InetAddress local_addr = null;
			int localRtspPort = -1;
			int localRtpVideoPort = -1;
			int localRtpAudioPort = -1;
			int localTalkBackPort = -1;
			
			// dev_loc now always null
			DeviceLocation dev_loc = caminfos[i].getDevice_location();
			if (dev_loc != null)
			{
				//get remote addr
				String remote_ip = dev_loc.getRemoteIP();
				if (remote_ip != null &&
						!remote_ip.equalsIgnoreCase("null") &&
						!remote_ip.equalsIgnoreCase(""))
				{
					//remote_addr = InetAddress.getByName(remote_ip);
				}
				
				//get remote RTSP port
				String remoteRtspPort_str = dev_loc.getRemotePort1();
				if (remoteRtspPort_str != null)
				{
					remoteRtspPort = Integer.parseInt(remoteRtspPort_str);
				}
				
				//get remote RTP Video port
				String remoteRtpVideoPort_str = dev_loc.getRemotePort2();
				if (remoteRtpVideoPort_str != null)
				{
					remoteRtpVideoPort = Integer.parseInt(remoteRtpVideoPort_str);
				}
				
				//get remote RTP Audio port
				String remoteRtpAudioPort_str = dev_loc.getRemotePort3();
				if (remoteRtpAudioPort_str != null)
				{
					remoteRtpAudioPort = Integer.parseInt(remoteRtpAudioPort_str);
				}
				
				//get remote TalkBack port
				String remoteTalkBackPort_str = dev_loc.getRemotePort4();
				if (remoteTalkBackPort_str != null)
				{
					remoteTalkBackPort = Integer.parseInt(remoteTalkBackPort_str);
				}
				
				//get local addr
				String local_ip = dev_loc.getLocalIP();
				if (local_ip != null && 
						!local_ip.equalsIgnoreCase("null") &&
						!local_ip.equalsIgnoreCase(""))
				{
					local_addr = InetAddress.getByName(local_ip);
				}
				
				//get local RTSP port
				String localRtspPort_str = dev_loc.getLocalPort1();
				if (localRtspPort_str != null)
				{
					localRtspPort = Integer.parseInt(localRtspPort_str);
				}
				
				//get local RTP Video port
				String localRtpVideoPort_str = dev_loc.getLocalPort2();
				if (localRtpVideoPort_str != null)
				{
					localRtpAudioPort = Integer.parseInt(localRtpVideoPort_str);
				}
				
				//get local RTP Audio port
				String localRtpAudioPort_str = dev_loc.getLocalPort3();
				if (localRtpAudioPort_str != null)
				{
					localRtpAudioPort = Integer.parseInt(localRtpAudioPort_str);
				}
				
				//get local TalkBack port
				String localTalkBackPort_str = dev_loc.getLocalPort4();
				if (localTalkBackPort_str != null)
				{
					localTalkBackPort = Integer.parseInt(localTalkBackPort_str);
				}
			}
		
			//get local addr
			String local_ip = caminfos[i].getLocal_ip();
			if (local_ip != null && 
					!local_ip.equalsIgnoreCase("null") &&
					!local_ip.equalsIgnoreCase(""))
			{
				local_addr = InetAddress.getByName(local_ip);
			}
		
			online_list[i] = new CamProfile(local_addr, MAC_addr);
			online_list[i].setName(camName);
			online_list[i].setFirmwareVersion(camFwVersion);
			online_list[i].setModelId(camModel);
			online_list[i].setCamId(camId);
			online_list[i].setReachableInRemote(isAvailable);
			
			//set camera remote info
			//online_list[i].setRemote_addr(remote_addr);
			online_list[i].setRemoteRtspPort(remoteRtspPort);
			online_list[i].setRemoteRtpVideoPort(remoteRtpVideoPort);
			online_list[i].setRemoteRtpAudioPort(remoteRtpAudioPort);
			online_list[i].setRemoteTalkBackPort(remoteTalkBackPort);
			
			
			online_list[i].setUpgrading(online_list[i].setUpgrading(caminfos[i].getFirmware_status()==1?true:false));
		}
		
		return online_list;
	}

}
