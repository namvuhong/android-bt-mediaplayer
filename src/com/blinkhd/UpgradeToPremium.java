package com.blinkhd;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class UpgradeToPremium
{	
	public static void openUpgradePage(Context context)
	{
		String url = "http://hubble.in/plans/";
		
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(url));
		context.startActivity(intent);
	}
}