package com.blinkhd;

import java.util.Hashtable;

public class CameraFeature 
{
	
	/* 
	 * Device Model Name	MBP36N	MBP836	FOCUS66	FOCUS96	MBP83	 
       Device Model ID		0036	0836	0066	0096	0083 
	 */
	private static Hashtable<String, String> hdCameraModels; 
	private static Hashtable<String, String> sharedCameraModels; 
	static 
	{
		sharedCameraModels= new Hashtable<String, String>(); 
	
		sharedCameraModels.put("0036", "MBP36N");
		sharedCameraModels.put("0043", "MBP43N");
		
		hdCameraModels  = new Hashtable<String, String>(); 
		hdCameraModels.put("0836", "MBP836");
		hdCameraModels.put("0066", "FOCUS66");
		hdCameraModels.put("0096", "FOCUS96");
		hdCameraModels.put("0083", "MBP83");
		
	}
	
	
	
	public static boolean shouldEnableFullFeature(String deviceId)
	{
		if (sharedCameraModels.contains(deviceId))
		{
			return false; 
			
		}
		
		return true; 
	}
	
	

}
