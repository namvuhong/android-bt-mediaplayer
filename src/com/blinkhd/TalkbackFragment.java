package com.blinkhd;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.msc3.CamChannel;
import com.msc3.PCMRecorder;
import com.msc3.PublicDefine;
import com.msc3.TalkBackTouchListener;
import com.nxcomm.blinkhd.ui.CameraDetailSettingActivity;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CreateTalkbackSessionResponse;
import com.nxcomm.meapi.device.CreateTalkbackSessionResponseData;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TalkbackFragment extends Fragment implements Callback
{
	private static final String TAG = "mbp";
	private CamChannel	   selected_channel;
	private String	       device_ip;
	private int	           device_port;
	private int	           device_audio_in_port;
	private PCMRecorder	   pcmRecorder;
	private String	       http_pass;
	private boolean	       talkback_enabled;

	private String	       session_key	     = null;
	private String	       stream_id	     = null;
	private String	       relay_server_ip	 = null;
	private int	           relay_server_port	= -1;

	private Activity	   activity	         = null;
	private ProgressDialog	progressDialog	 = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.talkback_fragment, container, false);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		setupTalkback();
	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
		if (relay_server_ip != null && relay_server_port != -1)
		{
			relay_server_ip = null;
			relay_server_port = -1;
			stop_talkback_session(session_key, stream_id);
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = activity;
	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		super.onDetach();
		activity = null;
	}

	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
		device_ip = selected_channel.getCamProfile().get_inetAddress()
		        .getHostAddress();
		device_port = selected_channel.getCamProfile().get_port();
		device_audio_in_port = PublicDefine.DEFAULT_AUDIO_PORT;
		http_pass = String.format("%s:%s", PublicDefine.DEFAULT_BASIC_AUTH_USR,
		        PublicDefine.DEFAULT_CAM_PWD);

		pcmRecorder = new PCMRecorder(device_ip, device_port, http_pass,
		        device_audio_in_port, new Handler(TalkbackFragment.this));
	}

	public static final int	MSG_LONG_TOUCH	         = 0xDEADBEEF;
	public static final int	MSG_LONG_TOUCH_START	 = 0xDEADBEEE;
	public static final int MSG_AUDIO_STREAM_HANDSHAKE_FAILED = 0xDEADBEED;

	public static final int	MSG_LONG_TOUCH_RELEASED	 = 0xCAFEBEEF;
	public static final int	MSG_SHORT_TOUCH_RELEASED = 0xCAFEBEED;
	public static final int	MSG_PCM_RECORDER_ERR	 = 0xDEADDEAD;

	private void setupTalkback()
	{

		TalkBackTouchListener tb = new TalkBackTouchListener(new Handler(this));
		ImageView btn = (ImageView) activity.findViewById(R.id.imgTalkback);
		if (btn != null)
		{
			btn.setVisibility(View.VISIBLE);
			btn.setOnTouchListener(tb);
		}
	}

	private boolean setTalkBackEnabledWithoutStreaming(boolean enabled)
	{
		if (enabled == true)
		{
			getActivity().runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					/* enable talkback */
					if (selected_channel != null
							&& selected_channel.getCamProfile() != null)
					{
						pcmRecorder.setLocalMode(selected_channel
								.getCamProfile().isInLocal());
					}
					talkback_enabled = pcmRecorder.startRecording();

					if (!talkback_enabled)
					{

						Spanned msg = Html.fromHtml("<big>"
								+ getResources().getString(
										R.string.EntryActivity_ptt_2)
										+ "</big>");
						Toast.makeText(getActivity(), msg,
								Toast.LENGTH_LONG).show();
					}
				}
			});

			// Dont fade
			//cancelFullscreenTimer();
		}
		else
		{
			/* disable talkback */
			if (pcmRecorder != null) {
				pcmRecorder.stopRecording();
				pcmRecorder.releaseRecorder();
			}
			talkback_enabled = false;
			//tryToGoToFullScreen();
		}

		return talkback_enabled;
	}

	private boolean setTalkBackEnabled(boolean enabled)
	{
		if (enabled == true)
		{
			/*
			 * Disable audio - But dont touch the UI stuf onSpeaker(null);
			 */
			getActivity().runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					/* enable talkback */
					if (selected_channel != null
							&& selected_channel.getCamProfile() != null)
					{
						pcmRecorder.setLocalMode(selected_channel
								.getCamProfile().isInLocal());
					}
					talkback_enabled = pcmRecorder.startRecording();

					if (!talkback_enabled)
					{

						Spanned msg = Html.fromHtml("<big>"
								+ getResources().getString(
										R.string.EntryActivity_ptt_2)
										+ "</big>");
						Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG)
						.show();
					}
					else
					{
						pcmRecorder.startStreaming();
					}

				}
			});
			// Dont fade
			// cancelFullscreenTimer();
		}
		else
		{
			/* disable talkback */
			if (pcmRecorder != null) {
				pcmRecorder.stopRecording();
				pcmRecorder.stopStreaming();
			}
			talkback_enabled = false;
			//tryToGoToFullScreen();
		}

		return talkback_enabled;
	}

	@Override
	public boolean handleMessage(Message msg)
	{
		// TODO Auto-generated method stub
		switch (msg.what)
		{
		case MSG_AUDIO_STREAM_HANDSHAKE_FAILED:
			relay_server_ip = null;
			relay_server_port = -1;
			if (pcmRecorder != null)
			{
				pcmRecorder.setRelayAddr(relay_server_ip);
				pcmRecorder.setRelayPort(relay_server_port);
				pcmRecorder.setSessionKey(session_key);
				pcmRecorder.setStreamId(stream_id);
			}
			stop_talkback_session(session_key, stream_id);
			break;
		case MSG_SHORT_TOUCH_RELEASED:
			if (getView() != null)
			{
				getView().post(new Runnable()
				{

					@Override
					public void run() {
						ImageView btn = (ImageView) getView()
								.findViewById(R.id.imgTalkback);
						if (btn != null)
						{
							btn.setImageDrawable((getResources()
									.getDrawable(R.drawable.camera_action_mic)));
						}
						TextView txtTalkback = (TextView) getView()
								.findViewById(R.id.textTalkback);
						if (txtTalkback != null)
						{
							txtTalkback.setText(getResources().getString(R.string.hold_to_talk));
							txtTalkback.setVisibility(View.VISIBLE);
						}
					}
				});
			}
			
			//unmute speaker
			if (getActivity() != null)
			{
				AudioManager amanager = (AudioManager)
						getActivity().getSystemService(Context.AUDIO_SERVICE);
				amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
			}
			
			setTalkBackEnabledWithoutStreaming(false);

			break;
		case MSG_LONG_TOUCH_RELEASED:
			String phoneModel = android.os.Build.MODEL;
			if (getView() != null)
			{
				getView().post(new Runnable()
				{

					@Override
					public void run()
					{
						ImageView btn = (ImageView) getView().findViewById(
						        R.id.imgTalkback);
						if (btn != null)
						{
							btn.setImageDrawable((getResources()
									.getDrawable(R.drawable.camera_action_mic)));
						}
						TextView txtTalkback = (TextView) getView()
						        .findViewById(R.id.textTalkback);
						if (txtTalkback != null)
						{
							txtTalkback.setText(getResources().getString(
							        R.string.hold_to_talk));
							txtTalkback.setVisibility(View.VISIBLE);
						}

						if (progressDialog != null
						        && progressDialog.isShowing())
						{
							progressDialog.dismiss();
							progressDialog = null;
						}
					}
				});
			}

			if (!phoneModel.equals(PublicDefine.PHONE_MBP1k)
			        && !phoneModel.equals(PublicDefine.PHONE_MBP2k))
			{
				Log.d(TAG, "Turn off talkback - Unlock screen orientation");
				if (getActivity() != null)
				{
					getActivity().setRequestedOrientation(
					        ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				}
			}
			
			//unmute speaker
			if (getActivity() != null)
			{
				AudioManager amanager = (AudioManager)
						getActivity().getSystemService(Context.AUDIO_SERVICE);
				amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
			}
			
			setTalkBackEnabled(false);
			// if (selected_channel != null && selected_channel.getCamProfile()
			// != null &&
			// !selected_channel.getCamProfile().isInLocal())
			// {
			// stop_talkback_session(session_key, stream_id);
			// }

			break;
		case MSG_LONG_TOUCH_START:
			if (getView() != null)
			{
				getView().post(new Runnable()
				{

					public void run()
					{
						String phoneModel = android.os.Build.MODEL;
						if (!phoneModel.equals(PublicDefine.PHONE_MBP1k) && 
								!phoneModel.equals(PublicDefine.PHONE_MBP2k))
						{
							Log.d("mbp", "Turn on talkback - Lock screen orientation");
							int currOrient = getCurrentOrientation();
							if (getActivity() != null)
							{
								getActivity().setRequestedOrientation(currOrient);
							}
						}
						
						//mute speaker
						if (getActivity() != null)
						{
							AudioManager amanager = (AudioManager)
									getActivity().getSystemService(Context.AUDIO_SERVICE);
							amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
						}
						
						ImageView btn = (ImageView) getView().findViewById(
						        R.id.imgTalkback);
						if (btn != null)
						{
							btn.setImageDrawable((getResources()
							        .getDrawable(R.drawable.camera_action_mic_pressed)));
						}
						
						TextView txtTalkback = (TextView) getView()
						        .findViewById(R.id.textTalkback);
						if (txtTalkback != null)
						{
							txtTalkback.setVisibility(View.INVISIBLE);
						}
					}
				});
			}
			setTalkBackEnabledWithoutStreaming(true);

			break;
		case MSG_LONG_TOUCH:
			phoneModel = android.os.Build.MODEL;
			if (getActivity() != null)
			{
				if (selected_channel != null
						&& selected_channel.getCamProfile() != null
						&& !selected_channel.getCamProfile().isInLocal()
						&& (relay_server_ip == null || relay_server_port == -1))
				{
					// progressDialog = ProgressDialog.show(
					// getActivity(), null,
					// getActivity().getApplicationContext().getResources().getString(
					// R.string.remove_camera_notification), true, false);
					// start setup remote talkback first time
					SharedPreferences settings = getActivity()
							.getSharedPreferences(PublicDefine.PREFS_NAME,
									0);
					String saved_token = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
					create_talkback_session(saved_token, selected_channel
							.getCamProfile().getRegistrationId());
				}
				else
				{
					pcmRecorder.startStreaming();
				}
			}
			break;
		case MSG_PCM_RECORDER_ERR:// talkback problem from PCMRecorder
			if (talkback_enabled == true)
			{
				setTalkBackEnabled(false);
				if (getActivity() != null)
				{
					getActivity().runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(
							        getActivity(),
							        getResources().getString(
							                R.string.EntryActivity_ptt_1),
							        Toast.LENGTH_LONG).show();

						}
					});
				}
			}

			break;

		}

		return false;
	}

	private int getCurrentOrientation()
	{
		int ret = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
		int rotation = getActivity().getWindowManager().getDefaultDisplay()
		        .getRotation();
		int orientation = getResources().getConfiguration().orientation;

		if ((orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT)
		{
			if (rotation == Surface.ROTATION_0
			        || rotation == Surface.ROTATION_270)
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
			}
			else
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
			}
		}
		else if ((orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (rotation == Surface.ROTATION_0
			        || rotation == Surface.ROTATION_90)
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
			}
			else
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
			}
		}

		return ret;
	}

	private void create_talkback_session(String saved_token, String regId)
	{
		// do create_talkback_session task
		CreateTalkBackSessionTask createTalkBackTask = new CreateTalkBackSessionTask();
		createTalkBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
		        saved_token, regId);
	}

	private void start_talkback_session(String session_key, String stream_id)
	{
		// do start_talkback_session task
		StartTalkBackSessionTask startTalkBackTask = new StartTalkBackSessionTask();
		startTalkBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
		        session_key, stream_id);
	}

	private void create_talkback_session_success(String session_key,
	        String stream_id)
	{
		this.session_key = session_key;
		this.stream_id = stream_id;
		start_talkback_session(session_key, stream_id);
	}

	private void create_talkback_session_failed(String error_desc)
	{
		Log.d(TAG, "create talkback session failed, reason: " + error_desc);
	}

	private void start_talkback_session_success(String relay_server_ip,
	        int relay_server_port)
	{
		// set relay ip & port for PCMRecorder
		Log.d(TAG, "start relay talkback success, relay_ip: "
		        + relay_server_ip + ", relay_port: " + relay_server_port);
		this.relay_server_ip = relay_server_ip;
		this.relay_server_port = relay_server_port;
		if (pcmRecorder != null)
		{
			pcmRecorder.setRelayAddr(relay_server_ip);
			pcmRecorder.setRelayPort(relay_server_port);
			pcmRecorder.setSessionKey(session_key);
			pcmRecorder.setStreamId(stream_id);
			pcmRecorder.startStreaming();
		}
	}

	private void start_talkback_session_failed(String error_desc)
	{
		Log.d(TAG, "start talkback session failed, reason: " + error_desc);
	}

	class CreateTalkBackSessionTask extends AsyncTask<String, String, Integer>
	{
		private static final int	SUCCESS		= 1;
		private static final int	FAILED		= 2;

		private String		     session_key	= null;
		private String		     stream_id		= null;

		private String		     accessToken	= null;
		private String		     regId		 = null;

		private String		     error_desc		= null;

		@Override
		protected Integer doInBackground(String... params)
		{
			// TODO Auto-generated method stub
			accessToken = params[0];
			regId = params[1];
			int ret = -1;

			try
			{
				CreateTalkbackSessionResponse createTalkBackRes = Device
				        .createTalkbackSession(accessToken, regId);
				if (createTalkBackRes != null)
				{
					if (createTalkBackRes.getStatus() == 200)
					{
						CreateTalkbackSessionResponseData createTalkBackResData = 
								createTalkBackRes.getData();
						if (createTalkBackResData != null)
						{
							ret = SUCCESS;
							session_key = createTalkBackResData.getSession_key();
							stream_id = createTalkBackResData.getStream_id();
						}
					}
					else
					{
						error_desc = createTalkBackRes.getMessage();
					}
				}
			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new Integer(ret);
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			// TODO Auto-generated method stub
			if (result.intValue() == SUCCESS)
			{
				create_talkback_session_success(session_key, stream_id);
			}
			else
			{
				create_talkback_session_failed(error_desc);
			}

		}

	}

	class StartTalkBackSessionTask extends AsyncTask<String, String, Integer>
	{
		private static final int	SUCCESS		   = 1;
		private static final int	FAILED		   = 2;

		private String		     relay_server_ip	= null;
		private int		         relay_server_port	= -1;

		private String		     session_key		= null;
		private String		     stream_id		   = null;

		private String		     error_desc		   = null;
		private Gson		     gson		       = new Gson();

		@Override
		protected Integer doInBackground(String... params)
		{
			session_key = params[0];
			stream_id = params[1];

			URL url = null;
			HttpURLConnection conn = null;
			DataInputStream inputStream = null;
			String response = null;
			String usr = "";
			String pwd = "";
			String usr_pass = String.format("%s:%s", usr, pwd);
			int ret = -1;

			String http_cmd = PublicDefine.TALKBACK_SERVER
			        + PublicDefine.START_TALKBACK_CMD
			        + PublicDefine.TALKBACK_PARAM_1 + session_key
			        + PublicDefine.TALKBACK_PARAM_2 + stream_id;
			Log.d(TAG, "Start talkback cmd: " + http_cmd);
			try
			{
				url = new URL(http_cmd);
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setRequestMethod("POST");
				conn.addRequestProperty("Content-Type",
				        "application/x-www-form-urlencoded");
				conn.setConnectTimeout(10000);
				conn.setReadTimeout(10000);

				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK)
				{
					inputStream = new DataInputStream(new BufferedInputStream(
					        conn.getInputStream(), 4 * 1024));
					String response_str = inputStream.readLine();
					StartTalkBackResponse startTbRes = null;
					try
					{
						startTbRes = gson.fromJson(response_str,
						        StartTalkBackResponse.class);
						if (startTbRes != null)
						{
							if (startTbRes.getStatus() == 200)
							{
								ret = SUCCESS;
								relay_server_ip = startTbRes.getRelay_server_ip();
								relay_server_port = startTbRes
										.getRelay_server_port();
							}
							else
							{
								error_desc = startTbRes.getMessage();
							}
						}
					}
					catch (JsonSyntaxException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new Integer(ret);
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			// TODO Auto-generated method stub
			if (result.intValue() == SUCCESS)
			{
				start_talkback_session_success(relay_server_ip,
				        relay_server_port);
			}
			else
			{
				start_talkback_session_failed(error_desc);
			}
		}

		private class StartTalkBackResponse
		{
			private int		status;
			private String	message;
			private String	device_response;
			private int		device_response_code;
			private String	relay_server_ip;
			private int		relay_server_port;

			public int getStatus()
			{
				return status;
			}

			public String getMessage()
			{
				return message;
			}

			public String getDevice_response()
			{
				return device_response;
			}

			public int getDevice_response_code()
			{
				return device_response_code;
			}

			public String getRelay_server_ip()
			{
				return relay_server_ip;
			}

			public int getRelay_server_port()
			{
				return relay_server_port;
			}

		}

	}

	private void stop_talkback_session(String session_key, String stream_id)
	{
		StopTalkBackSessionTask stopTalkBackTask = new StopTalkBackSessionTask();
		stopTalkBackTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
		        session_key, stream_id);
	}

	private void stop_talkback_session_success()
	{
		// Log.d(TAG, "stop relay talkback success");
	}

	private void stop_talkback_session_failed(String error_desc)
	{
		Log.d(TAG, "stop relay talkback failed, reason: " + error_desc);
	}

	class StopTalkBackSessionTask extends AsyncTask<String, String, Integer>
	{
		private static final int	SUCCESS		= 1;
		private static final int	FAILED		= 2;

		private String		     session_key	= null;
		private String		     stream_id		= null;

		private String		     error_desc		= null;
		private Gson		     gson		 = new Gson();

		@Override
		protected Integer doInBackground(String... params)
		{
			session_key = params[0];
			stream_id = params[1];

			URL url = null;
			HttpURLConnection conn = null;
			DataInputStream inputStream = null;
			String response = null;
			String usr = "";
			String pwd = "";
			String usr_pass = String.format("%s:%s", usr, pwd);
			int ret = -1;

			String http_cmd = PublicDefine.TALKBACK_SERVER
			        + PublicDefine.STOP_TALKBACK_CMD
			        + PublicDefine.TALKBACK_PARAM_1 + session_key
			        + PublicDefine.TALKBACK_PARAM_2 + stream_id;
			Log.d(TAG, "Stop talkback cmd: " + http_cmd);
			try
			{
				url = new URL(http_cmd);
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setRequestMethod("POST");
				conn.addRequestProperty("Content-Type",
				        "application/x-www-form-urlencoded");
				// conn.addRequestProperty("Authorization", "Basic " +
				// Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn.setConnectTimeout(10000);
				conn.setReadTimeout(10000);
				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK)
				{
					ret = SUCCESS;
					inputStream = new DataInputStream(new BufferedInputStream(
					        conn.getInputStream(), 4 * 1024));
				}
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return new Integer(ret);
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			// TODO Auto-generated method stub
			if (result.intValue() == SUCCESS)
			{
				stop_talkback_session_success();
			}
			else
			{
				stop_talkback_session_failed(error_desc);
			}
		}
	}

}
