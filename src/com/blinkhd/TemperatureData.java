package com.blinkhd;

import java.util.Date;
import java.util.HashMap;

import com.google.gson.Gson;

public class TemperatureData
{
	
	private static final float		INVALID_TEMP	= -273;
	
	private Date					date;
	private HashMap<Integer, Float>	tempInfo;
	private static int[]			morningTime		= { 0, 1, 2, 3, 4, 5, 6, 7,
			8, 9, 10, 11							};
	private static int[]			afternoonTime	= { 12, 13, 14, 15, 16, 17,
			18										};
	private static int[]			eveningTime		= { 17, 18, 19, 20, 21, 22,
			23										};
	
	public TemperatureData(Date date,HashMap<Integer, Float> tempInfo)
	{
		this.date = date;
		this.tempInfo = tempInfo;
	}
	/**
	 * Get morning average temperature
	 * 
	 * @return average temperature from 0:00 am to 11:59 am
	 */
	public float getMorningAvgTemp()
	{
		return this.getAvgTemp(morningTime);
	}
	
	/**
	 * Get afternoon average temperature
	 * 
	 * @return average temperature from 12:00 pm to 06:00 pm
	 */
	public float getAfternoonAvgTemp()
	{
		return this.getAvgTemp(afternoonTime);
	}
	
	/**
	 * Get evening average temperature
	 * 
	 * @return average temperature from 06:00 pm to 11:59 pm
	 */
	public float getEveningAvgTemp()
	{
		return this.getAvgTemp(eveningTime);
	}
	
	/**
	 * Get average temperature in period
	 * 
	 * @param startHour
	 *            start hour
	 * @param endHour
	 *            end hour
	 * @return average temperature between start hour and end hour.
	 */
	private float getAvgTemp(int[] hours)
	{
		int numberOfHourRecorded = 0;
		float totalTemp = 0.0f;
		float result = INVALID_TEMP;
		for (int hour : hours)
		{
			Float temp = this.tempInfo.get(hour);
			if (temp != null)
			{
				numberOfHourRecorded++;
				totalTemp += temp.floatValue();
			}
		}
		
		if(numberOfHourRecorded > 0)
		{
			result = totalTemp / numberOfHourRecorded;
		}
		
		return result;
		
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	public static void main(String[] args)
	{
		
		String obj = "{"
				+ "tempInfo: "
				+ "{'0':25.0,'1':23.0,'2':24.0,'3':26.0,'4':27.0,'13':28.0,'14':29.0,'15':30.0,'22':29.0,'23':30.0}"
				+ "}";
		Gson gson = new Gson();
		TemperatureData tempData = gson.fromJson(obj , TemperatureData.class);
		
		System.out.println("Morning temp "+ tempData.getMorningAvgTemp());
		System.out.println("Afternoon temp "+ tempData.getAfternoonAvgTemp());
		System.out.println("Evening temp "+ tempData.getEveningAvgTemp());
		
		
		System.out.println(gson.toJson(tempData));
		/*
		HashMap<Integer, Float> map = new HashMap<Integer, Float>();
		
		map.put(0, 25.0f);
		map.put(1, 23.0f);
		map.put(2, 24.0f);
		map.put(3, 26.0f);
		map.put(4, 27.0f);
		map.put(5, 28.0f);
		map.put(6, 29.0f);
		map.put(7, 30.0f);
		map.put(8, 29.0f);
		map.put(9, 30.0f);
		
		Gson gson = new Gson();
		
		Object a = map.get(2);
		if (a == null)
		{
			
		}
		else
		{
			System.out.println(" a= " + (float) a);
		}
		
		System.out.println(gson.toJson(map));
		*/
	}
}
