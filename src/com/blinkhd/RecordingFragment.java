package com.blinkhd;

import com.blinkhd.playback.LiveFragment;
import com.msc3.ButtonTouchListener;
import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RecordingFragment extends Fragment {
	
	private Activity activity;
	
	private CamChannel selected_channel;
	private String device_ip;
	private int device_port;
	private String http_pass;
	private boolean recOrSnap = false;
	private boolean isRecording = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.recording_fragment, container, false);
	}
	
	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
		device_ip = selected_channel.getCamProfile()
				.get_inetAddress().getHostAddress();
		device_port = selected_channel.getCamProfile().get_port();
		http_pass = String.format("%s:%s", PublicDefine.DEFAULT_BASIC_AUTH_USR,
				PublicDefine.DEFAULT_CAM_PWD);
	}
	
	/**
	 * @return true: recording mode, false: snapshot mode
	 */
	public boolean getRecSnapMode()
	{
		return recOrSnap;
	}
	
	public boolean isRecording()
	{
		return isRecording;
	}
	
	public void setRecSnapMode(boolean recOrSnap)
	{
		this.recOrSnap = recOrSnap;
	}
	
	public void setRecording(boolean isRecording)
	{
		this.isRecording = isRecording;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = activity;
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		setupRecording();
	}
	
	/**
	 * @param time time in seconds
	 */
	public void updateRecordingTime(int time)
	{
		if (getView() != null)
		{
			int hours = time / 3600;
			int minutes = (time % 3600) / 60;
			int seconds = time % 60;
			String time_str = String.format("%02d:%02d:%02d", hours, minutes, seconds);
			final TextView txtRec = (TextView) getView().findViewById(R.id.textRecording);
			final TextView txtRecSmall = (TextView) 
					getView().findViewById(R.id.textRecordingSmall);
			
			if (txtRec != null && txtRecSmall != null)
			{
				txtRec.setText(time_str);
				txtRecSmall.setText(time_str);
			}
			
		}
	}
	
	
	private void setupRecording()
	{
		final ImageView imgSnap = (ImageView) getView().findViewById(R.id.imgSnap);
		final ImageView imgRec = (ImageView) getView().findViewById(R.id.imgRecording);
		imgRec.setOnTouchListener(
				new ButtonTouchListener(
						activity.getResources().getDrawable(R.drawable.camera_action_photo),
						activity.getResources().getDrawable(R.drawable.camera_action_photo_pressed)));
		final TextView txtRec = (TextView) getView().findViewById(R.id.textRecording);
		final TextView txtRecSmall = (TextView) 
				getView().findViewById(R.id.textRecordingSmall);
		
		if (isRecording == true)
		{
			imgSnap.setVisibility(View.INVISIBLE);
			txtRecSmall.setVisibility(View.VISIBLE);
		}
		
		imgSnap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (recOrSnap == true)
				{
					//switch to snap mode
					recOrSnap = false;
					imgRec.setOnTouchListener(
							new ButtonTouchListener(
									activity.getResources().getDrawable(R.drawable.camera_action_photo),
									activity.getResources().getDrawable(R.drawable.camera_action_photo_pressed)));
					imgRec.setImageResource(R.drawable.camera_action_photo);
					imgSnap.setImageResource(R.drawable.camera_action_video_s);
					txtRec.setVisibility(View.INVISIBLE);
					
					if (isRecording == true)
					{
						imgSnap.setVisibility(View.INVISIBLE);
						txtRecSmall.setVisibility(View.VISIBLE);
					}
					
				}
				else
				{
					//switch to record mode
					if (CamProfile.shouldEnableMic(
							selected_channel.getCamProfile().getModelId()))
					{
						recOrSnap = true;
						imgRec.setOnTouchListener(null);
						imgRec.setImageResource(R.drawable.camera_action_video);
						imgSnap.setImageResource(R.drawable.camera_action_pic_s);
						txtRec.setText(getResources().getString(R.string.record_video));
						txtRec.setTextColor(getResources().getColor(R.color.textColor));
						txtRec.setVisibility(View.VISIBLE);
					}
					
				}
			}
		});
		
		txtRecSmall.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				recOrSnap = true;
				imgRec.setOnTouchListener(null);
				imgRec.setImageResource(R.drawable.camera_action_video_stop);
				imgSnap.setImageResource(R.drawable.camera_action_pic_s);
				txtRec.setTextColor(getResources().getColor(R.color.timeColor));
				txtRec.setVisibility(View.VISIBLE);
				
				//show snap icon
				imgSnap.setVisibility(View.VISIBLE);
				txtRecSmall.setVisibility(View.INVISIBLE);
			}
		});
		
		imgRec.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (recOrSnap == true)
				{
					//recording is OFF, Turn it ON 
					if (isRecording == false)
					{
						isRecording = true;
						imgRec.setImageResource(R.drawable.camera_action_video_stop);
						if (txtRec != null && txtRecSmall != null)
						{
							txtRec.setText("00:00:00");
							txtRec.setTextColor(getResources().getColor(R.color.timeColor));
							txtRecSmall.setText("00:00:00");
						}
					}
					else // Recording ON, turn if OFF 
					{
						isRecording = false;
						imgRec.setImageResource(R.drawable.camera_action_video);
						if (txtRec != null)
						{
							txtRec.setText(getResources().getString(R.string.record_video));
							txtRec.setTextColor(getResources().getColor(R.color.textColor));
						}
						
						String text = activity.getResources().getString(R.string.saved_video);
						int duration = Toast.LENGTH_SHORT;

						Toast toast = Toast.makeText(activity, text, duration);
						toast.setGravity(Gravity.BOTTOM, 0, 0);
						toast.show();
					}
					
					((HomeScreenActivity) activity).setRecord(isRecording); 
					
				}
				else
				{
					//snapshot mode
					((HomeScreenActivity) activity).onSnap();
				}
			}
		});
		
		if (selected_channel != null && selected_channel.getCamProfile() != null &&
				selected_channel.getCamProfile().isSharedCam())
		{
			if (imgSnap != null)
			{
				imgSnap.setVisibility(View.INVISIBLE);
			}
		}
	}
	


	
}
