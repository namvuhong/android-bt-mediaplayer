package com.blinkhd.gcm;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.VoxMessage;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.nxcomm.meapi.App;
import com.nxcomm.meapi.app.GetAppsResponse;
import com.nxcomm.meapi.app.NotificationInfo;
import com.nxcomm.meapi.app.NotificationSettings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;



/**
 * @author phung
 * 
 */
public class GetDisabledAlertsTask extends AsyncTask<CamProfile, String, CamProfile> {

	private static final String TOTAL_DISABLED_ALERTS = "Total_disabled_alerts="; 
	private static final String ALERT = "alert=";
	
	private Context mContext;
	private String userToken;
	private IGetAlertsCallBack callback;
	private int appId;

	public GetDisabledAlertsTask(Context mContext,IGetAlertsCallBack cb, String userToken) {
		
		this.mContext = mContext;
		this.userToken = userToken; 
		//Can be NULL 
		callback = cb;
	}


	@Override
	protected CamProfile doInBackground(CamProfile... params) {

		CamProfile cp = params[0];
		
		/* Special case: MBP2k, 1k */
		String phonemodel = android.os.Build.MODEL;
		if  (phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			return cp;
		}
		
		
		// Set all status to enable 
		cp.setSoundAlertEnabled(true); 
		cp.setTempHiAlertEnabled(true); 
		cp.setTempLoAlertEnabled(true);
		cp.setMotionAlertEnabled(true);
		
		Log.d("mbp", "Query alert settings");

//		String http_cmd = PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART + 
//				PublicDefine.GET_DISABLED_NOTIFICATIONS_U_CMD + 
//				PublicDefine.GET_DISABLED_NOTIFICATIONS_U_PARAM_1+ this.user_email +
//				PublicDefine.GET_DISABLED_NOTIFICATIONS_U_PARAM_2+ macAddress ;
		
		SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		appId = (int) settings.getLong(PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID, -1);
		NotificationSettings[] notifSettings = null;
		try {
			GetAppsResponse getAppRes = App.getApps(userToken);
			if (getAppRes != null && getAppRes.getStatus() == HttpURLConnection.HTTP_OK
					&& getAppRes.getData() != null)
			{
				NotificationInfo[] notifInfos = getAppRes.getData();
				for (NotificationInfo notifInfo : notifInfos)
				{
					if (notifInfo.getId() == appId)
					{
						notifInfo.getDeviceAppNotificationSettings();
						break;
					}
				}
				
				if (notifSettings != null && notifSettings.length > 0)
				{
					for (NotificationSettings notifSetting : notifSettings)
					{
						if (notifSetting != null)
						{
							int alertType = notifSetting.getAlert();
							switch(alertType)
							{
							case VoxMessage.ALERT_TYPE_SOUND:
								cp.setSoundAlertEnabled(notifSetting.isIs_enabled());
								break;
							case VoxMessage.ALERT_TYPE_TEMP_HI:
								cp.setTempHiAlertEnabled(notifSetting.isIs_enabled());
								break;
							case VoxMessage.ALERT_TYPE_TEMP_LO:
								cp.setTempLoAlertEnabled(notifSetting.isIs_enabled());
								break;
							case VoxMessage.ALERT_TYPE_MOTION_ON:
								cp.setMotionAlertEnabled(notifSetting.isIs_enabled());
								break;
							}
						}
					}
				} //if (notifSettings != null && notifSettings.length > 0)
				
			}//if (getAppRes != null &&...)
		} catch (SocketTimeoutException e1) {
			e1.printStackTrace();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return cp;
	}
	
	protected void onCancelled ()
	{
	}

	/* UI thread */
	protected void onPostExecute(CamProfile cp )
	{
		if (callback != null)
		{
			callback.onSuccess();
		}
	}
	
	
	 public interface IGetAlertsCallBack
	 {
	    public void onSuccess();
	    public void onError(); 
	 }
}

 



