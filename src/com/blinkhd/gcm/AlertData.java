package com.blinkhd.gcm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Iterator;

import android.os.Environment;
import android.util.Log;

import com.msc3.CamProfile;
import com.msc3.VoxMessage;

public class AlertData {

	
	public static final String ALERT_FILE = "mbp_alert.dat";
	private static AlertData alertData = null; 
	
	
	private ArrayList<VoxMessage> unClearedAlert;
	private boolean isLoaded; 
	
	public static boolean addNewAlert( VoxMessage newMsg,File externalFileDir)
	{
		if (alertData == null)
		{
			alertData = new AlertData(); 
		}
		

		if (!alertData.isLoaded())
		{
			alertData.restoreAlert(externalFileDir);
		}
		
		
		alertData.addAlert(newMsg);
		return alertData.saveAlert(externalFileDir);
	}
	
	public static void clearAlertForCamera( String cameraMac,File externalFileDir)
	{
		if (alertData == null)
		{
			alertData = new AlertData(); 
		}
		if (!alertData.isLoaded())
		{
			alertData.restoreAlert(externalFileDir);
		}
		alertData._clearAlertForCamera(cameraMac);
		
		alertData.saveAlert(externalFileDir);
	}
	
	
	/**
	 * @param cameraMac
	 * @param externalFileDir
	 * @return a mask of all alert
	 */
	public static int getAlertForCamera( String cameraMac,File externalFileDir)
	{
		if (alertData == null)
		{
			alertData = new AlertData(); 
		}
		
		if (!alertData.isLoaded())
		{
			alertData.restoreAlert(externalFileDir);
		}
		
		return alertData._getCameraAlert(cameraMac);
	}
	
	
	public static ArrayList<VoxMessage> getAllAlert(File externalFileDir)
	{
		if (alertData.isLoaded())
		{
			return alertData._getAllAlert(); 
		}
		else //reload it first
		{
			alertData.restoreAlert(externalFileDir);
			return alertData._getAllAlert(); 
		}
	}
	
	
	/**
	 * Remove all alerts that're not belong to the camera in the list
	 * 
	 * if cameras = NULL -- REMOVE ALL
	 * if camera is EMPTY --- DONT REMOVE ANY 
	 * 
	 * @param cameras
	 */
	public static void purgeAlertsNotFromCameras(ArrayList<String> cameras, File externalFileDir)
	{
		if (alertData == null)
		{
			alertData = new AlertData(); 
		}
		
		if (!alertData.isLoaded())
		{
			alertData.restoreAlert(externalFileDir);
		}
		
		ArrayList<VoxMessage> allAlerts = alertData._getAllAlert();
		
		if(allAlerts.size() > 0)
		{
			
			if (cameras == null)
			{
				Log.d("mbp", "Clear all un-read alert"); 
				allAlerts.clear(); 
			}
			else if (cameras.isEmpty())
			{
				Log.d("mbp", "Dont remove any un-read alert");
				
			}
			else
			{
			

				Iterator<VoxMessage> iter2 = allAlerts.iterator();
				while (iter2.hasNext())
				{
					VoxMessage vm = (VoxMessage) iter2.next();
					boolean found = false; 

					Iterator<String> iter = cameras.iterator(); 
					while (iter.hasNext())
					{
						if (iter.next().equalsIgnoreCase(vm.getTrigger_mac()))
						{
							found = true; 
							break; 
						}
					}
					if (found == false )
					{
						Log.d("mbp", "Delete alert from: " + vm.getTrigger_name() + "--" + vm.getTrigger_mac());
						iter2.remove(); 
					}

				}
			}
			
			
		}
	}
	
	private ArrayList<VoxMessage> _getAllAlert() 
	{
		return unClearedAlert;
	}

	public AlertData()
	{
		isLoaded  = false; 
		unClearedAlert = new ArrayList<VoxMessage>(); 
	}

	
	public boolean isLoaded() {
		return isLoaded;
	}

	public void setLoaded(boolean isLoaded) {
		this.isLoaded = isLoaded;
	}

	public void addAlert(VoxMessage newMsg)
	{
		if (unClearedAlert != null)
		{
			unClearedAlert.add(newMsg);
		}
	}
	
	
	/**
	 * TO BE called after restored data 
	 * 
	 * @param cameraMac
	 * @return 0 - No alert for camera found OR a mask of alerts
	 *          	VoxMessage.VOX_TYPE_UNKNOWN = 0;
	 				           VOX_TYPE_VOICE = 1;
	 				           VOX_TYPE_TEMP_HI = 2;
					           VOX_TYPE_TEMP_LO = 4;
	 *         
	 */
	public int  _getCameraAlert(String cameraMac)
	{
		int alert_mask = 0; 
		if (unClearedAlert != null)
		{
			VoxMessage vm;
			
			
			Iterator<VoxMessage> iter = unClearedAlert.iterator();
			while(iter.hasNext())
			{
				vm = iter.next();
				if (vm.getTrigger_mac().equalsIgnoreCase(cameraMac))
				{
					alert_mask |= vm.getVoxType();
				}
			}
		}
		return alert_mask ;
	}
	
	
	
	
	/**
	 * 
	 * Called in camera list when user open a camera -
	 * @param cameraMac
	 */
	public void _clearAlertForCamera(String cameraMac)
	{
		if (unClearedAlert != null)
		{
			VoxMessage vm; 
			Iterator<VoxMessage> iter = unClearedAlert.iterator();
			while(iter.hasNext())
			{
				vm = iter.next();
				if (vm.getTrigger_mac().equalsIgnoreCase(cameraMac))
				{
					//remove it
					iter.remove();
				}
			}
		}
	}
	
	
	public boolean saveAlert(File externalFileDir)
	{
		/* Check if the External Storage is available and writeable */
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.e("mbp", "External Storage is mounted as READONLY!");
			return false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			return false;
		}

		/* create a data file in the external dir */
		File file = new File(externalFileDir,ALERT_FILE );
		if (file.exists())
		{
			//Log.e("mbp", "File exist, remove it");
			/* remove the old file */
			file.delete();
		}
		
		
		try {
			OutputStream os = new FileOutputStream(file);
			ObjectOutputStream obj_out = new ObjectOutputStream(os);

			int size = unClearedAlert.size(); 
			obj_out.writeInt(size);
			for (int i = 0 ; i < size; i++)
			{
				obj_out.writeObject(unClearedAlert.get(i));
			}

			obj_out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			if (file.exists())
			{
				/* remove the incomplete file */
				file.delete();
			}
			e.printStackTrace();
		}
		
		return true; 
	}
	
	public boolean restoreAlert(File externalFileDir)
	{
		/* Check if the External Storage is available and writeable */
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.e("mbp", "External Storage is mounted as READONLY!");
			return false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			return false;
		}

		/* create a data file in the external dir */
		File file = new File(externalFileDir,ALERT_FILE );
		if (!file.exists())
		{
			return false; //* file does not exists 
		}

		
		
		try 
		{


			InputStream is = new FileInputStream(file);
			ObjectInputStream obj_in = new ObjectInputStream(is);

			//Read size
			int size = obj_in.readInt();
			this.unClearedAlert = new ArrayList<VoxMessage>();
			for (int i =0; i< size; i++)
			{
				this.unClearedAlert.add((VoxMessage)obj_in.readObject());
			}

			obj_in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			//XXX
			if (file.exists())
				file.delete();
			e.printStackTrace();
		} catch (IOException e) {
			if (file.exists())
				file.delete();
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if (file.exists())
				file.delete();
			e.printStackTrace();
		}

		
		isLoaded = true; 
		return true; 
	}
	
	
	
	
	
}
