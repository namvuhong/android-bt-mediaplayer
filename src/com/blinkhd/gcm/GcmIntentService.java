package com.blinkhd.gcm;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings.Secure;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.msc3.ServerMessage;
import com.msc3.VoxActivity;
import com.msc3.VoxMessage;
import com.nxcomm.meapi.App;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.app.RegisterAppResponse;
import com.nxcomm.meapi.app.RegisterNotificationResponse;
import com.nxcomm.meapi.device.GetTimelineEventsResponse;

/**
 * @author phung
 * 
 */
public class GcmIntentService extends IntentService
{

	public static final String	TAG	                = "mbp";
	private long	           lastTimeRead	        = 0;
	private static String	   android_id;
	private String	           latest_EventTimeCode	= "";

	public GcmIntentService(String name)
	{
		super(name);
	}

	public GcmIntentService()
	{
		super("GcmIntentService-Service");
	}

	private static PowerManager.WakeLock	sWakeLock	= null;
	private static final Object	         LOCK	      = GcmIntentService.class;

	public static void runIntentInService(Context context, Intent intent)
	{
		android_id = Secure.getString(context.getContentResolver(),
		        Secure.ANDROID_ID);
		Log.d("mbp", " Start Registering with GCM 03");
		synchronized (LOCK)
		{
			if (sWakeLock == null)
			{
				PowerManager pm = (PowerManager) context
				        .getSystemService(Context.POWER_SERVICE);
				sWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				        "my_wakelock");
			}

			if (sWakeLock != null)
			{
				sWakeLock.acquire();
			}
		}

		intent.setClassName(context, GcmIntentService.class.getName());
		context.startService(intent);
	}

	@Override
	public final void onHandleIntent(Intent intent)
	{
		try
		{

			String action = intent.getAction();
			if (action.equals("com.google.android.c2dm.intent.REGISTRATION"))
			{
				handleRegistration(intent);
			}
			else if (action.equals("com.google.android.c2dm.intent.RECEIVE"))
			{
				handleMessage(intent);
			}
		}
		finally
		{
			synchronized (LOCK)
			{
				if (sWakeLock != null && sWakeLock.isHeld())
				{
					sWakeLock.release();
				}
			}
		}
	}

	private void handleRegistration(Intent intent)
	{
		String registrationId = intent.getStringExtra("registration_id");
		String error = intent.getStringExtra("error");
		String unregistered = intent.getStringExtra("unregistered");
		String server_url = "http://www.monitoreverywhere.com/gcm-demo/register";

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String oldRegId = settings.getString(
		        PublicDefine.PREFS_PUSH_NOTIFICATION_ID, null);

		String saved_token = settings.getString(
		        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		// registration succeeded
		if (registrationId != null)
		{

			{
				// new regId
				SharedPreferences.Editor editor = settings.edit();
				editor.putString(PublicDefine.PREFS_PUSH_NOTIFICATION_ID,
				        registrationId);
				editor.commit();

				Log.d("mbp", "Start BMS Registration " + registrationId);
				// old query
				// int res = registerWithBMS(user_email,user_pwd,
				// registrationId);
				// new query
				SharedPreferences set = getSharedPreferences(
				        PublicDefine.PREFS_NAME, 0);
				RegisterAppResponse registerAppResponse = null;
				RegisterNotificationResponse registerNotificationResponse = null;
				try
				{
					PackageInfo pInfo = null;
					try
					{
						pInfo = getPackageManager().getPackageInfo(
						        getPackageName(), 0);
					}
					catch (NameNotFoundException e)
					{
						e.printStackTrace();
					}

					String software_version = null;
					if (pInfo != null)
					{
						software_version = pInfo.versionName;
					}
					else
					{
						software_version = "00.00";
					}

					registerAppResponse = App.registerApp(saved_token,
					        android_id, android.os.Build.MODEL,
					        software_version);

					if (registerAppResponse != null)
					{
						if (registerAppResponse.getStatus() == HttpsURLConnection.HTTP_OK)
						{
							registerNotificationResponse = App
							        .registerNotification(saved_token,
							                registerAppResponse.getData()
							                        .getId(), "gcm",
							                registrationId);

							// new regId
							SharedPreferences.Editor edit = settings.edit();
							editor.putLong(
							        PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID,
							        registerAppResponse.getData().getId());
							editor.commit();

						}
						else
						{
							// USE notification to show users
							Log.d(TAG, "Cannot register app via Me-Api Json.");
							VoxMessage news = VoxMessage.buildMessage("0",
							        "00:00:00:00:00:00",
							        registerAppResponse.getMessage(), "0",
							        "Camera App",
							        "http://api.simplimonitor.com");
							generateSeverNotification(this, news);

						}
					} // if (registerAppResponse != null)

				}
				catch (SocketTimeoutException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (MalformedURLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

		// unregistration succeeded
		if (unregistered != null)
		{
			// get old registration ID from shared preferences
			// notify 3rd-party server about the unregistered ID

			if (oldRegId != null)
			{
				SharedPreferences.Editor editor = settings.edit();
				editor.remove(PublicDefine.PREFS_PUSH_NOTIFICATION_ID);
				editor.commit();

			}

		}

		// last operation (registration or unregistration) returned an error;
		if (error != null)
		{
			if ("SERVICE_NOT_AVAILABLE".equals(error))
			{
				// optionally retry using exponential back-off
				// (see Advanced Topics)
			}
			else
			{
				// Unrecoverable error, log it
				Log.i(TAG, "Received error: " + error);
			}
		}
	}

	private void popupNotify()
	{

	}

	/**
	 * Server intent should have the following key pairs ("alert", "1"); //1 =
	 * vox, 2 = temp hi, 3 = temp lo ("mac", "112233445566"); ("val","na");
	 * ("time","1234567");
	 * 
	 * @param intent
	 */

	public static final String	ALERT_TYPE	= "alert";
	public static final String	DEV_MAC	   = "mac";
	public static final String	ALERT_VAL	= "val";
	public static final String	ALERT_TIME	= "time";
	public static final String	CAM_NAME	= "cameraname";
	public static final String	FTP_URL	   = "ftp_url";

	private void handleMessage(Intent intent)
	{
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// Clear and push again later
		notificationManager.cancel(PUSH_IDs[0]);

		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		if (saved_token == null)
		{
			//user has logged out --> ignore notification
			Log.d(TAG, "Use has logged out, ignore the notification message");
			return;
		}
		
		String alertType = intent.getExtras().getString(ALERT_TYPE);
		String macAddr = intent.getExtras().getString(DEV_MAC);
		String alertVal = intent.getExtras().getString(ALERT_VAL);
		String alertTime = intent.getExtras().getString(ALERT_TIME);
		String cameraName = intent.getExtras().getString(CAM_NAME);
		String ftp_url = intent.getExtras().getString(FTP_URL);

		Log.d(TAG, "HANDLE PUSH NOTIFICATION MESSAGE");
		for (String key : intent.getExtras().keySet())
		{
			Object value = intent.getExtras().get(key);
			Log.d(TAG, String.format("%s %s (%s)", key, value.toString(), value
			        .getClass().getName()));
		}

		if ((macAddr == null) || (alertType == null) || (alertTime == null)
		        || (cameraName == null))
		{
			Log.d("mbp", "Extra data is null ");
			return;
		}
		SharedPreferences m = getSharedPreferences(PublicDefine.PREFS_NAME, 0);

		boolean isDonotDiturbEnable = m.getBoolean(
		        PublicDefine.PREFS_IS_DO_NOT_DISTURB_ENABLE, false);
		long timeWhenDonotDisturbExpired = m.getLong(
		        PublicDefine.PREFS_TIME_DO_NOT_DISTURB_EXPIRED, 0);

		if (isDonotDiturbEnable
		        && System.currentTimeMillis() < timeWhenDonotDisturbExpired)
		{
			Log.d(TAG,
			        "RECEIVED PUSH NOTIFICATION, BUT DONOT DISTURD is ENABLE and time still not expired, so do not disturb user.");
			return;
		}
		Log.d(TAG,
		        "Do not disturb not enable or time expired, continue build notification");

		{

			alertTime = String.format("%d", System.currentTimeMillis());
			VoxMessage newMsg = VoxMessage.buildMessage(alertType, macAddr,
			        alertVal, alertTime, cameraName, ftp_url);

			if (newMsg != null)
			{
				if (alertType.startsWith("0"))
				{
					Log.d("mbp", "Alert type 0 >>>>>>>>>>> " + newMsg);
					generateSeverNotification(this, newMsg);
				}
				else
				{

					/*
					 * 20130826: DO IT LATER --
					 * 
					 * Log.d("mbp", "clear all alert for mac:"
					 * +newMsg.getTrigger_mac() );
					 * AlertData.clearAlertForCamera(newMsg.getTrigger_mac(),
					 * getExternalFilesDir(null));
					 * 
					 * //Store this mesg boolean status =
					 * AlertData.addNewAlert(newMsg, getExternalFilesDir(null));
					 * 
					 * ArrayList<VoxMessage> messages =
					 * AlertData.getAllAlert(getExternalFilesDir(null));
					 * Log.d("mbp", "added new vox msg: "+ newMsg + " backlog:" +
					 * messages.size());
					 * 
					 * generateNotifications(this, messages);
					 */

					Notification notif = build_notification_with_sound(this,
							newMsg, null);

					if (notif != null)
					{
						Log.d("mbp", "11 Notify once >>>>>>>>>>> " + newMsg);
						notificationManager.notify(PUSH_IDs[0], notif);
					}

				}
			}

		}

	}

	private ServiceConnection	notifyConn	= new ServiceConnection()
	                                       {

		                                       public void onServiceConnected(
		                                               ComponentName className,
		                                               IBinder service)
		                                       {
			                                       Log.d("mbp",
			                                               "Notify service connected");
		                                       }

		                                       public void onServiceDisconnected(
		                                               ComponentName className)
		                                       {
			                                       Log.d("mbp",
			                                               "Notify service disconnected");
		                                       }
	                                       };

	public static boolean isNotifyServiceRunning(Context c)
	{
		ActivityManager manager = (ActivityManager) c
		        .getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
		        .getRunningServices(Integer.MAX_VALUE))
		{
			if ("com.msc3.gcm.NotifyService".equals(service.service
			        .getClassName()))
			{
				return true;
			}
		}
		return false;
	}

	private static final int	PUSH_VOX_ID_1	= 2;
	private static final int	PUSH_VOX_ID_2	= PUSH_VOX_ID_1 + 1;
	private static final int	PUSH_VOX_ID_3	= PUSH_VOX_ID_1 + 2;
	private static final int	PUSH_VOX_ID_4	= PUSH_VOX_ID_1 + 3;

	private static final int	PUSH_VOX_ID_5	= PUSH_VOX_ID_1 + 4;

	public static final int[]	PUSH_IDs	  = { PUSH_VOX_ID_1, PUSH_VOX_ID_2,
	        PUSH_VOX_ID_3, PUSH_VOX_ID_4	  };

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private void generateSeverNotification(Context context, VoxMessage newVox)
	{
		int icon = R.drawable.cam_icon;
		long when = System.currentTimeMillis();

		NotificationManager notificationManager = (NotificationManager) context
		        .getSystemService(Context.NOTIFICATION_SERVICE);

		Notification notification = new Notification(icon,
		        ((ServerMessage) newVox).getServer_msg(), when);
		String title = context.getString(R.string.app_name);

		String myUrl = ((ServerMessage) newVox).getMsg_url();
		if (myUrl == null)
		{
			myUrl = "http://api.simplimonitor.com";
		}
		Log.d("mbp", "Vox url: " + myUrl);
		Intent notificationIntent = new Intent(Intent.ACTION_VIEW,
		        Uri.parse(myUrl));

		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,
		        newVox.getTrigger_mac());
		PendingIntent intent = PendingIntent.getActivity(context, 0,
		        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(context, title,
		        ((ServerMessage) newVox).getServer_msg(), intent);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_ALL;
		// notification.deleteIntent =null ;
		notificationManager.notify(PUSH_VOX_ID_5, notification);
	}

	private Notification build_notification(Context context, VoxMessage newVox,
	        Notification old_notif)
	{

		int icon = R.drawable.cam_icon;
		long when = newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name();
		Notification notification = null;

		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,
		        newVox.getTrigger_mac());
		notificationIntent.putExtra(VoxActivity.TRIGGER_TYPE,
		        newVox.getVoxType());
		// sonnguyen_20130813
		if (newVox.getVoxType() == VoxMessage.VOX_TYPE_MOTION_ON)
		{
			notificationIntent.putExtra(VoxActivity.TRIGGER_EVENT_CODE_EXTRA,
			        newVox.getEventTimeCode());
		}

		Log.d("mbp",
		        "Build Notif: " + newVox.getVoxType() + " for: "
		                + newVox.getTrigger_mac());

		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		        | Intent.FLAG_ACTIVITY_SINGLE_TOP
		        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
		        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		if (old_notif == null)
		{
			// This is the first message
			//

			notification = new Notification(icon, title, when);

			notification.setLatestEventInfo(context, title, newVox.toString(),
			        intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS;

		}
		else
		// just update the content of the notification
		{

			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(),
			        intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS;

		}
		return notification;
	}

	private Notification build_notification_with_sound(Context context,
	        VoxMessage newVox, Notification old_notif)
	{

		int icon = R.drawable.cam_icon;
		long when = newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name();

		Notification notification = null;
		android.support.v4.app.NotificationCompat.Builder builderNotif = null;

		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,
		        newVox.getTrigger_mac());
		notificationIntent.putExtra(VoxActivity.TRIGGER_TYPE,
		        newVox.getVoxType());
		// sonnguyen_20130813
		if (newVox.getVoxType() == VoxMessage.VOX_TYPE_MOTION_ON)
		{
			notificationIntent.putExtra(VoxActivity.TRIGGER_EVENT_CODE_EXTRA,
			        newVox.getEventTimeCode());
		}

		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		        | Intent.FLAG_ACTIVITY_SINGLE_TOP
		        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
		        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Log.d("mbp", "Build Notif With sound:: " + newVox.getVoxType()
		        + " for: " + newVox.getTrigger_mac());
		if (old_notif == null)
		{
			// This is the first message
			Log.d("mbp", "This is the Notification message");
			// Notification no = new android.app
			try
			{
				android.R.dimen dimen = new android.R.dimen();
				int image_width = getResources().getDimensionPixelSize(
				        android.R.dimen.notification_large_icon_width);
				int image_height = getResources().getDimensionPixelSize(
				        android.R.dimen.notification_large_icon_height);
				// int image_width = this.getApplicationContext().get
				// //this.getResources().getInteger(android.R.dimen.notification_large_icon_width);
				// = 24;//
				// this.getResources().getInteger(android.R.dimen.notification_large_icon_height);
				SharedPreferences sharePrefs = getSharedPreferences(
				        PublicDefine.PREFS_NAME, 0);
				String apiKey = sharePrefs.getString(
				        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
				Log.d("mbp", "VOX TYPE " + newVox.getVoxType());
				if (newVox.getVoxType() == VoxMessage.VOX_TYPE_MOTION_ON)
				{

					String mac = newVox.getTrigger_mac();

					GetTimelineEventsResponse res = Device.getTimelineEvents(
					        apiKey, mac, null, newVox.getEventTimeCode(),
					        VoxMessage.ALERT_TYPE_MOTION_ON + "", 1, -1);

					String bitmapURL = null;
					Bitmap bitmap = null;

					if (res != null && res.getStatus() == 200)
					{
						if (res.getEvents().length > 0
						        && res.getEvents()[0].getData().length > 0)
						{
							bitmapURL = res.getEvents()[0].getData()[0]
							        .getImage();
						}
					}

					builderNotif = new NotificationCompat.Builder(this);
					builderNotif.setAutoCancel(true);
					builderNotif.setSmallIcon(R.drawable.header_logo);
					builderNotif.setTicker(newVox.toString() + " on " + title);

					if (bitmapURL != null)
					{
						bitmap = getBitmapFromURL(bitmapURL);
						if (image_width < 0)
						{
							image_width = 24;
							image_height = 24;
						}

					}
					else
					{
						System.out.println("IMAGE IS NULL");
					}

					builderNotif.setContentTitle(title);
					builderNotif.setContentText(newVox.toString());

					// builderNotif.addAction(R.drawable.play, "View Event",
					// intent);
					// builderNotif.addAction(R.drawable.play, "View Live",
					// intent);

					builderNotif.setLargeIcon(bitmap);
					builderNotif.setContentIntent(intent);

					NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
					bigPicStyle.bigPicture(bitmap);
					bigPicStyle.setBigContentTitle(title);
					bigPicStyle.setSummaryText(newVox.toString());

					bigPicStyle.setBuilder(builderNotif);

					builderNotif.setStyle(bigPicStyle);

					notification = builderNotif.build();
				}
				// other notification type
				else
				{
					builderNotif = new NotificationCompat.Builder(this);
					builderNotif.setAutoCancel(true);
					builderNotif.setSmallIcon(icon);
					builderNotif.setContentIntent(intent);
					builderNotif.setTicker(newVox.toString() + " on " + title);
					builderNotif.setContentTitle(title);
					builderNotif.setSmallIcon(R.drawable.header_logo);
					builderNotif.setContentText(newVox.toString());
					notification = builderNotif.build();

				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				System.out.println("ERROR WHEN BUILDING NOTIFICATION"
				        + ex.toString());
			}

			// notification.setLatestEventInfo(context, title,
			// newVox.toString(), intent);

			// notification.flags |= Notification.FLAG_AUTO_CANCEL;
			// notification.defaults = Notification.DEFAULT_ALL;

		}
		else
		// just update the content of the notification
		{

			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(),
			        intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;

		}
		return notification;
	}

	/**
	 * Show a group of notification For different Macs there is one icons
	 * 
	 * @param context
	 * @param messages
	 */
	private void generateNotifications(Context context,
	        ArrayList<VoxMessage> messages)
	{
		NotificationManager notificationManager = (NotificationManager) context
		        .getSystemService(Context.NOTIFICATION_SERVICE);

		HashMap<String, Notification> notification_map = new HashMap<String, Notification>();

		Iterator<VoxMessage> iter = messages.iterator();
		String mac;
		VoxMessage mesg;
		boolean found;
		Notification notif;

		iter = messages.iterator();
		while (iter.hasNext())
		{
			mesg = iter.next();

			notif = notification_map.get(mesg.getTrigger_mac());

			/*
			 * 20130702: hoang: only the last notification has sound
			 */
			if (iter.hasNext())
			{
				notif = build_notification(context, mesg, notif);
			}
			else
			{
				notif = build_notification_with_sound(context, mesg, notif);
			}

			notification_map.put(mesg.getTrigger_mac(), notif);

		}

		// NOTIFY all ..
		Set<String> devices = notification_map.keySet();
		Iterator<String> iter2 = devices.iterator();
		int i = 0;
		int current_id;
		while (iter2.hasNext() && i < GcmIntentService.PUSH_IDs.length)
		{
			current_id = PUSH_IDs[i];
			notif = notification_map.get(iter2.next());
			notificationManager.notify(current_id, notif);

			i++;
		}
	}

	public Bitmap getBitmapFromURL(String strURL)
	{
		try
		{
			URL url = new URL(strURL);
			HttpURLConnection connection = (HttpURLConnection) url
			        .openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}

}