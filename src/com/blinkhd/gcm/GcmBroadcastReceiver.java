package com.blinkhd.gcm;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class GcmBroadcastReceiver extends BroadcastReceiver {


	@Override
	public final void onReceive(Context context, Intent intent) {
		GcmIntentService.runIntentInService(context, intent);
		setResult(Activity.RESULT_OK, null, null);
	}

}
