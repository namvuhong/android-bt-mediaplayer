package com.blinkhd.playback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ClipInfo 
{
	public static String getFileName(String filePath)
	{
		int idx = filePath.replaceAll("\\\\", "/").lastIndexOf("/");
		return idx >= 0 ? filePath.substring(idx + 1) : filePath;
	}
	private String filePath;
	private String label;
	private String mac;
	private Date dateTime;
	private int splitIndex;
	private int eventType;
	private String eventTimeCode;
	
	public String getMac() {
		return mac;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public int getSplitIndex() {
		return splitIndex;
	}

	public String getFilePath() {
		return filePath;
	}
	public String getLabel() {
		return label;
	}
	public ClipInfo(String filePath) throws ParseException, StringIndexOutOfBoundsException
	{
		this.filePath = getFileName(filePath);
		ParseData();
	}
	//file name:  0123456789AB_01_20130803111213400_000050000_000010000_00006.flv
	public void ParseData() throws ParseException, StringIndexOutOfBoundsException
	{
			
		
			//get mac
			mac = filePath.substring(0,12);
			//get event type
			String eventTypeStr = filePath.substring(13,15);
			eventType = Integer.parseInt(eventTypeStr);
			//get date
			String dateStr = filePath.substring(16,33);
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			format.setTimeZone(TimeZone.getTimeZone("GMT+0"));
			dateTime = format.parse(dateStr);
			
			//contruct event time code
			eventTimeCode = eventTypeStr + "_" + dateStr;
			/*
			//get event start offset
			String startOffsetStr = filePath.substring(34,43);
			startOffset = Integer.parseInt(startOffsetStr); 
			
			//get duration in miliseconds
			String durationStr = filePath.substring(44,53);
			duration = Integer.parseInt(durationStr);
			//get split index
			
			String splitIndexStr = filePath.substring(34,39);
			splitIndex = Integer.parseInt(splitIndexStr);
			*/
	}
	public int getEventType()
	{
		return eventType;
	}
	public String getEventTimeCode() {
		return eventTimeCode;
	}
	public void setEventTimeCode(String eventTimeCode) {
		this.eventTimeCode = eventTimeCode;
	}
}
