package com.blinkhd.playback;

import java.io.IOException;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.registration.UserAccount;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;

public class CameraUnavailableFragment extends Fragment
{
	private Activity activity = null;
	
	@Override
	public void onAttach(Activity activity)
	{
	    // TODO Auto-generated method stub
	    super.onAttach(activity);
	    this.activity = activity;
	}
	
	@Override
	public void onDetach()
	{
	    // TODO Auto-generated method stub
	    super.onDetach();
	    activity = null;
	}
	
	@Override
	public void onStop() {
		super.onStop();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		RelativeLayout v = (RelativeLayout) inflater.inflate(R.layout.live_unavailable_fragment, container, false);


		if (unavailableCameraMac != null)
		{
//			TextView txtUnavailable = (TextView) 
//					v.findViewById(R.id.textView1);
//			String unavailable = String.format(
//					getResources().getString(R.string.camera_is_offline_since_),
//					cp.getLastUpdate());
//			txtUnavailable.setText(unavailable);
			
			final ImageButton refresh = (ImageButton) v.findViewById(R.id.refreshButton);

			refresh.setEnabled(true); 
			refresh.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) 
				{
					if (activity != null)
					{
						((HomeScreenActivity)activity).showRotatingIcon();
					}
					//Refresh this camera by ..
					refresh.setEnabled(false);
					refresh.setVisibility(View.INVISIBLE);
					SharedPreferences settings = getActivity().getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);
					String apikey = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

					RetreiveCameraStatusTask task = new RetreiveCameraStatusTask(getActivity());
					task.execute(apikey, unavailableCameraMac); 
					

				}
				
			});
		}
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		//		recalcDefaultScreenSize();
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	private String unavailableCameraMac;
	private CamProfile cp;

	public void setCameraMac(String mac)
	{
		this.unavailableCameraMac = mac; 
	}
	
	public void setCamProfile(CamProfile cp)
	{
		this.cp = cp;
	}

	public void onAttached(Activity act)
	{
		super.onAttach(act); 


		
	}


	public void checkStatus(CamProfile cp) 
	{
		if (cp != null)
		{
			if (cp.isReachableInRemote())
			{

				Log.d("mbp","camera status has changed to AVAILABLE >>>");
				
				
				HomeScreenActivity act =(HomeScreenActivity) getActivity(); 
				
				if (act != null)
				{
					act.scanAndViewCamera(); 
				}
				
			}
			else
			{
				//Do nothing
				Log.d("mbp","camera status is UNAVAILABLE >>>");
				if (activity != null)
				{
					((HomeScreenActivity)activity).hideRotatingIcon();
				}
				
				final ImageButton refresh = (ImageButton) getView().
						findViewById(R.id.refreshButton);
				refresh.setVisibility(View.VISIBLE);
			}
		}

		ImageButton refresh = (ImageButton) getActivity().findViewById(R.id.refreshButton);
		refresh.setEnabled(true); 
	}


	private 
	class RetreiveCameraStatusTask extends AsyncTask<String, Void, CamProfile>
	{
		private Context mContext; 
		private Exception				exception;

		public RetreiveCameraStatusTask(Context mContext)
		{
			this.mContext = mContext; 
		}

		private CamProfile[] restored_profiles; 


		/* restore functions */
		private boolean restore_session_data()
		{

			SetupData savedData = new SetupData();
			try
			{
				if (savedData.restore_session_data(mContext.getExternalFilesDir(null)))
				{
					restored_profiles = savedData.get_CamProfiles();
					return true;
				}
			}
			catch (StorageException e)
			{
				e.printStackTrace();
			}
			return false;

		}

		protected CamProfile doInBackground(String... args)
		{


			String apiKey = args[0]; 
			String cameraMac = null; 
			if (args.length >=2)
			{
				cameraMac = args[1]; 
			}
			UserAccount online_user;
			try {
				online_user = new UserAccount(apiKey, mContext.getExternalFilesDir(null), null, mContext );

				//Blocking call 
				online_user.sync_user_data();


			} catch (StorageException e1) {
				Log.d("mbp", e1.getLocalizedMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			CamProfile camera = null ; 
			SparseArray<CamProfile> cameras = new SparseArray<CamProfile>();


			if (restore_session_data() == true)
			{

				if (restored_profiles != null && restored_profiles.length > 0)
				{
					for (int i = 0; i < restored_profiles.length; i++)
					{
						if(restored_profiles[i].getRegistrationId().equalsIgnoreCase(cameraMac))
						{
							camera = restored_profiles[i];
							break;
						}

					}

				}
			}

			return camera;
		}

		@Override
		protected void onPostExecute(CamProfile result)
		{
			super.onPostExecute(result);

			checkStatus(result); 
		}

	}
}





