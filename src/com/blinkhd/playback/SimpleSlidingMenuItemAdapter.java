package com.blinkhd.playback;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkhd.R;
import com.google.gson.Gson;

public class SimpleSlidingMenuItemAdapter extends
		ArrayAdapter<SimpleSlidingMenuItem>
{
	
	public SimpleSlidingMenuItemAdapter(Context context)
	{
		super(context, 0);
	}
	
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.listview_menusliding_row, null);
		
			SimpleSlidingMenuItem menuItem = getItem(position);
			Gson gson = new Gson();
			System.out.println("MENU ITEM " + gson.toJson(menuItem));
			// set menu icon if have
			ImageView imgMenuIcon = (ImageView) convertView
					.findViewById(R.id.menuIcon);
			if (menuItem.getIconRes() != 0)
			{
				imgMenuIcon.setImageResource(menuItem.getIconRes());
			}
			
			// set menu text
			if (menuItem.getMenuText() != null)
			{
				TextView tvMenuText = (TextView) convertView
						.findViewById(R.id.menuText);
				tvMenuText.setText(menuItem.getMenuText());
			}
			
			// set menu header text
			if (menuItem.isHaveHeaderText())
			{
				TextView tvMenuHeader = (TextView) convertView
						.findViewById(R.id.menuHeader);
				tvMenuHeader.setText(menuItem.getHeaderText());
				tvMenuHeader.setVisibility(View.VISIBLE);
			}
			
			// set menu description
			TextView tvMenuDesc = (TextView) convertView
					.findViewById(R.id.menuDesc);
			if (menuItem.getMenuDesc() != null)
			{
				tvMenuDesc.setText(menuItem.getMenuDesc());
			}
			else
			{
				tvMenuDesc.setVisibility(View.GONE);
			}
		}
		return convertView;
	}
	
}