package com.blinkhd.playback;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.msc3.VoxActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nxcomm.meapi.device.Event;
import com.nxcomm.meapi.device.GeneralData;
import com.nxcomm.meapi.device.TimelineEvent;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;

public class PlaybackManagerActivity extends Activity implements
		OnItemClickListener, LoadPlaylistListener {

	private static final String TAG = "PLAYBACKMANAGER";


	private ListView listView;
	private List<TimelineEvent> arrayOfList;
	private DisplayImageOptions options;

	private TimelineEvent[] events;
	private String mac;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);


		// get intent data

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mac = extras.getString(VoxActivity.TRIGGER_MAC_EXTRA);
			//eventTimeCode = extras
			//		.getString(VoxActivity.TRIGGER_EVENT_CODE_EXTRA);
			// eventTimeCode = "04_20130803111213400";
		} else {
			Log.d(TAG, "Extra is null.");
		}

		
		loadingEvents();
		
		
	}
	
	
	protected void onStart()
	{
		super.onStart();
		
		
	}
	
	private void loadingEvents()
	{
		setContentView(R.layout.activity_playback_manager);

	
		// get all control back from XML files
		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(this);
		
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		// GetRecordedVideosResponse res = Device.getRecordedVideos(saved_token,
		// mac, eventTimeCode);

		if (Utils.isNetworkAvailable(PlaybackManagerActivity.this)) {
			LoadPlaylistTask loadPlaylistTask = new LoadPlaylistTask(this, true);
			loadPlaylistTask.execute(saved_token, mac, "04", "0");
		}
		else 
		{
			//TODO: show some dialog ..
		}
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		/*
		 * Item item = arrayOfList.get(position); Intent intent = new
		 * Intent(EventManagerActivity.this, DetailActivity.class);
		 * intent.putExtra("url", item.getLink()); intent.putExtra("title",
		 * item.getTitle()); intent.putExtra("desc", item.getDesc());
		 * startActivity(intent);
		 */
		Intent intent = new Intent(PlaybackManagerActivity.this,
				FFMpegPlaybackActivity.class);
		TimelineEvent event = arrayOfList.get(position);

		String streamUrl = null;
		GeneralData[] playlist = event.getData();
		if (playlist != null && playlist.length > 0) 
		{
			streamUrl = playlist[0].getFile();
			if (streamUrl != null) 
			{	
				ClipInfo info = null;
				try 
				{
					info = new ClipInfo(streamUrl);
					
					Log.d("mbp", "Snapshot url: " + streamUrl);
					intent.putExtra(FFMpegPlaybackActivity.STR_DEVICE_MAC, mac);
					
					if (info != null) 
					{
						intent.putExtra(FFMpegPlaybackActivity.STR_EVENT_CODE,
								info.getEventTimeCode());
					}

					startActivity(intent);
				} 
				catch (ParseException e)
				{
					e.printStackTrace();
					showDialog(DIALOG_NO_CLIP); 
				}
				catch(StringIndexOutOfBoundsException ex)
				{
					ex.printStackTrace();
					showDialog(DIALOG_NO_CLIP); 
				}
			
			} 
			else 
			{
				Log.d(TAG, "Playlist file field empty.");
			}
		}

	}

	
	private static final int DIALOG_NO_CLIP = 1; 
	
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		ProgressDialog dialog;
		Spanned msg;
		switch (id) {
		
		case DIALOG_NO_CLIP:
			builder = new AlertDialog.Builder(this);

			msg = Html.fromHtml("<big>"+"There is no clip for this event."+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface diag, int which) {
					diag.cancel();
				}
			})
			.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface diag) {
				}
			});

			alert = builder.create();



			return alert;
		default:
			return null;
		}
	}
	
	
	public void setAdapterToListview() 
	{
		NewsRowAdapter objAdapter = new NewsRowAdapter(
				PlaybackManagerActivity.this, R.layout.row, arrayOfList);
		
		listView.setAdapter(objAdapter);
	}


	@Override
	public void onRemoteCallSucceeded(TimelineEvent latest, TimelineEvent[] allEvents) 
	{
		// TODO Auto-generated method stub
		
		if (allEvents != null)
		{
			this.events = allEvents;
			
			this.arrayOfList = new ArrayList<TimelineEvent>();
			
			ProgressBar pbar = (ProgressBar) findViewById(R.id.prgPreview);
			pbar.setVisibility(View.INVISIBLE); 
			
			for (int i = 0; i < events.length; i++)
			{
				this.arrayOfList.add(events[i]);
			}	
			if(this.events.length > 0)
			{
				setAdapterToListview();
			}
		} 
		else
		{
			//No events
		}
	}

	//TODO: @Son use Diaglog Fragment to replace showDialog
	
	@Override
	public void onRemoteCallFailed(int errorCode) 
	{
		switch (errorCode)
		{
		case LoadPlaylistTask.IO_EXCEPTION:
		case LoadPlaylistTask.MALFORMED_URL_EXCEPTION:
		case LoadPlaylistTask.SOCKET_TIMEOUT_EXCEPTION:
		{
			Spanned msg = Html.fromHtml(getString(R.string.timeout_while_connecting_to_server_retry_));
			AlertDialog.Builder builder = new AlertDialog.Builder(
					this);
			builder.setMessage(msg)
			.setCancelable(true)
			.setNegativeButton(getResources().getString(R.string.Cancel), 
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();
							
						}
			    }	)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					//RELOAD..
					loadingEvents();

				}
			});
			try 
			{
				builder.create().show();
			}
			catch(Exception e)
			{
			}
			
			break;
		}
		default:
			break;
		}
	}
}
