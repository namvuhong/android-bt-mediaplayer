package com.blinkhd.playback;

import java.io.File;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.blinkhd.R;
import com.blinkhd.MEAsyncTask.LoadCameraListTaskListener;
import com.media.ffmpeg.FFMpeg;
import com.media.ffmpeg.FFMpegException;
import com.media.ffmpeg.android.FFMpegMovieViewAndroid;
import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.FadeOutAnimationAndGoneListener;
import com.msc3.LeftSideMenuImageAdapter;
import com.msc3.PublicDefine;
import com.msc3.ScreenTimeOutRunnable;
import com.msc3.Util;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;
import com.nxcomm.blinkhd.ui.CameraDetailSettingActivity;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.blinkhd.ui.ILiveFragmentCallback;
import com.nxcomm.blinkhd.ui.LeftSideMenuItemListener;

import cz.havlena.ffmpeg.ui.FFMpegMessageBox;

//import com.blinkhd.MainActivity;

public class LiveFragment extends Fragment implements
        LoadCameraListTaskListener
{
	private static final String	     TAG	                 = "mbp";
	private String	                 mac;
	private int	                     default_width, default_height;
	private int	                     default_screen_width,
	        default_screen_height;
	private boolean	                 isConnectingForTheFirstTime;
	private float	                 ratio;
	private FFMpegMovieViewAndroid	 mMovieView;
	private ImageView	             imgViewOfflineCam;
	private AsyncTask	             initVideoView;
	private ProgressBar	             prgConnectCamera;
	private TextView	             txtConnectCamera;

	private ProgressDialog	         prgDialog;
	private String	                 deviceIp;
	private int	                     devicePort;
	private String	                 filePath	             = null;
	private CamChannel	             selected_channel	     = null;
	private ILiveFragmentCallback	 liveFragmentListener	 = null;

	private boolean	                 viewRtspStun	         = false;
	private LeftSideMenuItemListener	leftSideMenuListener	= null;
	private boolean	                 shouldEnableMic	     = false;
	private boolean	                 shouldTurnOnPanTilt	 = false;
	private boolean	                 shouldTurnOnMelody	     = false;

	public interface IGotoFullScreenHandler
	{
		public void onGotoFullScreen();

	}

	private IGotoFullScreenHandler	onGotoFullScreen;

	public void stopStreaming()
	{
		if (getActivity() != null)
		{
			((HomeScreenActivity) getActivity()).stopAllThread();
		}

		if (this.mMovieView != null && !mMovieView.isReleasingPlayer())
		{
			this.mMovieView.release();
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		stopStreaming();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();

		if (mMovieView != null && !mMovieView.isReleasingPlayer())
		{
			this.mMovieView.release();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			this.getActivity().getActionBar().hide();
		}
		else
		{
			this.getActivity().getActionBar().show();
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

		TextView txtCamName = null;
		if (getView() != null)
		{
			txtCamName = (TextView) getView().findViewById(R.id.txtCamName);
		}

		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			this.getActivity().getActionBar().hide();

			getActivity().getWindow().addFlags(
			        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
			getActivity().getWindow().addFlags(
			        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

			if (txtCamName != null)
			{

				TypedValue tv = new TypedValue();
				int actionBarHeightInPixel = 0;

				if (getActivity().getTheme().resolveAttribute(
				        android.R.attr.actionBarSize, tv, true))
				{
					actionBarHeightInPixel = TypedValue
					        .complexToDimensionPixelSize(tv.data,
					                getResources().getDisplayMetrics());
				}

				txtCamName.setPadding(0, actionBarHeightInPixel, 0, 0);
			}

		}
		else
		{
			getActivity().getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);

			getActivity().getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
			getActivity().getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);

			this.getActivity().getActionBar().show();
			if (txtCamName != null)
			{
				txtCamName.setPadding(0, 10, 0, 0);
			}
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		RelativeLayout v = (RelativeLayout) inflater.inflate(
		        R.layout.live_fragment, container, false);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		// recalcDefaultScreenSize();
	}

	@Override
	public void onPostExecute(CamProfile[] camProfiles)
	{

	}

	public void setIsConnectingViaStun(boolean isStun)
	{
		viewRtspStun = isStun;
	}

	public void setStreamUrl(String url)
	{
		filePath = url;

		setupFFMpegPlayer(false);
	}

	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
	}

	@Override
	public void onPreExecute()
	{
	}

	private void queryRunningOS()
	{
		Thread worker = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				if (selected_channel != null)
				{
					String http_pass = String.format("%s:%s",
					        PublicDefine.DEFAULT_BASIC_AUTH_USR,
					        PublicDefine.DEFAULT_CAM_PWD);
					String res = null;
					String cmd = PublicDefine.GET_RUNNING_OS;
					if (selected_channel.getCamProfile().isInLocal())
					{
						final String device_address_port = selected_channel
						        .getCamProfile().get_inetAddress()
						        .getHostAddress()
						        + ":"
						        + selected_channel.getCamProfile().get_port();
						String http_addr = null;
						http_addr = String.format("%1$s%2$s%3$s%4$s",
						        "http://", device_address_port,
						        PublicDefine.HTTP_CMD_PART, cmd);
						Log.d("mbp", "get running OS cmd: " + http_addr);
						res = HTTPRequestSendRecv
						        .sendRequest_block_for_response(http_addr,
						                PublicDefine.DEFAULT_BASIC_AUTH_USR,
						                http_pass);
					}
					else
					{
						cmd = PublicDefine.BM_HTTP_CMD_PART + cmd;
						Log.d("mbp", "get running OS cmd: " + cmd);
						SharedPreferences settings = getActivity()
						        .getSharedPreferences(PublicDefine.PREFS_NAME,
						                0);
						String saved_token = settings.getString(
						        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
						if (saved_token != null)
						{
							res = UDTRequestSendRecv.sendRequest_via_stun2(
							        saved_token, selected_channel
							                .getCamProfile()
							                .getRegistrationId(), cmd);
						}
					}

					Log.d("mbp", "get running OS res: " + res);
					if (res != null
					        && res.startsWith(PublicDefine.GET_RUNNING_OS))
					{
						res = res.substring(PublicDefine.GET_RUNNING_OS
						        .length() + 2);
						if (res.equalsIgnoreCase("-1"))
						{
							res = null;
						}
						else
						{
							if (res.equalsIgnoreCase("MACOS"))
							{
								shouldTurnOnPanTilt = false;
								shouldTurnOnMelody = false;
							}
							else
							{
								if (selected_channel
								        .getCamProfile()
								        .getModelId()
								        .equalsIgnoreCase(
								                CamProfile.MODEL_ID_MBP33N))
								{
									shouldTurnOnPanTilt = false;
								}
								else
								{
									shouldTurnOnPanTilt = true;
								}

								shouldTurnOnMelody = true;
							}

						}

					}

					if (getActivity() != null)
					{
						getActivity().runOnUiThread(new Runnable()
						{

							@Override
							public void run()
							{
								// TODO Auto-generated method stub
								setupSideMenu();
							}
						});

					}
				} // if (selected_channel != null)
			}
		});
		worker.start();
	}

	private void setupFFMpegPlayer(boolean show_dialog)
	{

		if (getView() != null)
		{
			mMovieView = (FFMpegMovieViewAndroid) getView().findViewById(
			        R.id.imageVideo);

			TextView txtCamName = (TextView) getView().findViewById(
			        R.id.txtCamName);
			if (txtCamName != null)
			{
				txtCamName.setText(selected_channel.getCamProfile().getName());
			}

			if (mMovieView.isShown())
			{
				if (liveFragmentListener != null)
				{
					liveFragmentListener.setupScaleDetector();
					liveFragmentListener.setupOnTouchEvent();
				}

				shouldEnableMic = CamProfile.shouldEnableMic(selected_channel
				        .getCamProfile().getModelId());

				if (selected_channel != null
				        && selected_channel.getCamProfile() != null
				        && (selected_channel.getCamProfile().getModelId()
				                .equalsIgnoreCase(CamProfile.MODEL_ID_MBP36N)
				                || selected_channel
				                        .getCamProfile()
				                        .getModelId()
				                        .equalsIgnoreCase(
				                                CamProfile.MODEL_ID_MBP41N) || selected_channel
				                .getCamProfile().getModelId()
				                .equalsIgnoreCase(CamProfile.MODEL_ID_MBP33N)))
				{
					queryRunningOS();
				}
				else
				{
					shouldTurnOnPanTilt = CamProfile
					        .shouldEnablePanTilt(selected_channel
					                .getCamProfile().getModelId());
					shouldTurnOnMelody = true;
					setupSideMenu();
				}

				tryToGoToFullScreen();

				try
				{

					FFMpeg ffmpeg = new FFMpeg();
					// mMovieView = ffmpeg.getMovieView(this);
					String snapFileName = Util.getRecordDirectory()
					        + File.separator
					        + CameraDetailSettingActivity
					                .getSnapshotNameHashed(selected_channel
					                        .getCamProfile()
					                        .getRegistrationId()) + ".jpg";
					mMovieView.setSnapshotPath(snapFileName);
					if (selected_channel.getCamProfile().getModelId()
					        .equalsIgnoreCase(CamProfile.MODEL_ID_MBP36N))
					{
						// shared cam
						mMovieView.initVideoView(new Handler(
						        (HomeScreenActivity) getActivity()), false,
						        true);
					}
					else
					{
						// camera hd
						mMovieView.initVideoView(new Handler(
						        (HomeScreenActivity) getActivity()), false,
						        false);
					}
					// else don't need to do anything .. default is UDP
					mMovieView.setVideoPath(filePath);

				}
				catch (FFMpegException e)
				{
					Log.d(TAG,
					        "Error when initializing ffmpeg: " + e.getMessage());
					FFMpegMessageBox.show(getActivity(), e);
					// finish();
				}
			} // if (mMovieView.isShown())
			else
			{
				stopStreaming();
			}

		} // end if (getView() != null)
	}

	@Override
	public void onResume()
	{
		super.onResume();

	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		// setupFFMpegPlayer(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub

		// if (item.getItemId() == R.id.btnEalier) {
		// MainActivity mainActivity = (MainActivity) this.getActivity();
		// mainActivity.goEalier(mac);
		// }

		return super.onOptionsItemSelected(item);
	}

	private LeftSideMenuImageAdapter	leftSideMenuAdpt;

	/***
	 * Split UI and Video Viewing
	 */
	/* keep them on */
	public void showSideMenusAndStatus()
	{
		/* cancel any timeout running */
		cancelFullscreenTimer();

		// Show status bar

		// getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		RelativeLayout leftSideMenuContainer = (RelativeLayout) getActivity()
		        .findViewById(R.id.left_side_menu_container);
		if(leftSideMenuContainer!=null)
		{
			leftSideMenuContainer.setVisibility(View.VISIBLE);
		}
		
		RelativeLayout leftSideMenu = (RelativeLayout) getActivity()
		        .findViewById(R.id.left_side_menu);
		if (leftSideMenu != null)
		{
			leftSideMenu.clearAnimation();
			leftSideMenu.setVisibility(View.VISIBLE);

			GridView gridview = (GridView) leftSideMenu
			        .findViewById(R.id.slide_content);
			gridview.invalidateViews();

		}

		if (getView() != null)
		{
			TextView txtCamName = (TextView) getView().findViewById(
			        R.id.txtCamName);
			if (txtCamName != null)
			{
				txtCamName.setText(selected_channel.getCamProfile().getName());
				txtCamName.setVisibility(View.VISIBLE);

				if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				{
					TypedValue tv = new TypedValue();
					int actionBarHeightInPixel = 0;

					if (getActivity().getTheme().resolveAttribute(
					        android.R.attr.actionBarSize, tv, true))
					{
						actionBarHeightInPixel = TypedValue
						        .complexToDimensionPixelSize(tv.data,
						                getResources().getDisplayMetrics());
					}

					txtCamName.setPadding(0, actionBarHeightInPixel, 0, 0);
				}
				else if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				{
					txtCamName.setPadding(0, 10, 0, 0);
				}
			}
		}

		if (getActivity() != null && getActivity().getActionBar() != null)
		{
			getActivity().getActionBar().show();
		}

	}

	public void tryToGoToFullScreen()
	{

		cancelFullscreenTimer();

		/* Start a 10sec remoteVideoTimer and made menus disappear */
		timeOut = new ScreenTimeOutRunnable(getActivity(), new Runnable()
		{

			@Override
			public void run()
			{

				goToFullScreen();
			}
		});
		_timeOut = new Thread(timeOut, "Screen Timeout");
		_timeOut.start();

	}

	private boolean	              isFullScreen;
	private ScreenTimeOutRunnable	timeOut;
	private Thread	              _timeOut;

	public void goToFullScreen()
	{

		isFullScreen = true;

		if (getView() != null)
		{
			RelativeLayout leftSideMenuContainer = (RelativeLayout) getActivity()
			        .findViewById(R.id.left_side_menu_container);
			if(leftSideMenuContainer!=null)
			{
				fade_out_view(leftSideMenuContainer, 1000);
			}
			

			RelativeLayout leftSideMenu = (RelativeLayout) getView()
			        .findViewById(R.id.left_side_menu);
			if (leftSideMenu != null && leftSideMenu.isShown())
			{
				fade_out_view(leftSideMenu, 1000);
			}

			if (liveFragmentListener != null)
			{
				liveFragmentListener.goToFullScreen();
			}

			if (leftSideMenuAdpt != null)
			{
				leftSideMenuAdpt.clearAllItemSettings();
			}

			TextView txtCamName = (TextView) getView().findViewById(
			        R.id.txtCamName);
			if (txtCamName != null && txtCamName.isShown())
			{
				fade_out_view(txtCamName, 1000);
			}

			if (getActivity() != null && getActivity().getActionBar() != null)
			{
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				{
					getActivity().getActionBar().hide();
				}
				else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				{
					getActivity().getActionBar().show();
				}
			}
			// Hide status bar
			// getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

			if (onGotoFullScreen != null)
			{
				onGotoFullScreen.onGotoFullScreen();
			}
		}
	}

	public boolean isFullScreen()
	{
		return isFullScreen;
	}

	public void setFullScreen(boolean is_full_screen)
	{
		this.isFullScreen = is_full_screen;
	}

	private void fade_out_view(View v, int duration_ms)
	{
		Animation myFadeAnimation = AnimationUtils.loadAnimation(getActivity(),
		        R.anim.fadeout);
		myFadeAnimation.setDuration(duration_ms);
		myFadeAnimation
		        .setAnimationListener(new FadeOutAnimationAndGoneListener(v));
		v.startAnimation(myFadeAnimation);
	}

	private void cancelFullscreenTimer()
	{

		/* cancel any timeout running */
		if (timeOut != null)
		{
			timeOut.setCancel(true);
			_timeOut.interrupt();
			try
			{
				_timeOut.join(1000);
			}
			catch (InterruptedException e)
			{
			}
			timeOut = null;
		}
	}

	private int getNumberOfColumns()
	{
		int result = 2;

		if (shouldEnableMic)
		{
			result++;
		}

		if (shouldTurnOnPanTilt)
		{
			result++;
		}

		if (shouldTurnOnMelody)
		{
			result++;
		}

		return result;
	}

	private void setupPan(int position)
	{
		leftSideMenuAdpt.clearOtherSettings(position);
		leftSideMenuAdpt.toggleItem(position);
		leftSideMenuListener.onPan(leftSideMenuAdpt.isShownEnabled(position));
	}

	private void setupMic(int position)
	{
		leftSideMenuAdpt.clearOtherSettings(position);
		leftSideMenuAdpt.toggleItem(position);
		leftSideMenuListener.onMic(leftSideMenuAdpt.isShownEnabled(position));
	}

	private void setupRecord(int position)
	{
		leftSideMenuAdpt.clearOtherSettings(position);
		leftSideMenuAdpt.toggleItem(position);
		leftSideMenuListener
		        .onRecord(leftSideMenuAdpt.isShownEnabled(position));
	}

	private void setupMelody(int position)
	{
		leftSideMenuAdpt.clearOtherSettings(position);
		leftSideMenuAdpt.toggleItem(position);
		leftSideMenuListener
		        .onMelody(leftSideMenuAdpt.isShownEnabled(position));
	}

	private void setupTemperature(int position)
	{
		leftSideMenuAdpt.clearOtherSettings(position);
		leftSideMenuAdpt.toggleItem(position);
		leftSideMenuListener.onTemperature(leftSideMenuAdpt
		        .isShownEnabled(position));
	}

	private void setupSideMenu()
	{

		if (getActivity() != null)
		{
			final RelativeLayout leftSideMenu = (RelativeLayout) getActivity()
			        .findViewById(R.id.left_side_menu);

			/* build the grid base on given size */
			GridView gridview = (GridView) leftSideMenu
			        .findViewById(R.id.slide_content);

			DisplayMetrics metrics = getActivity().getResources()
			        .getDisplayMetrics();
			float scale = metrics.density;

			Resources r = getResources();
			// float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
			// 200, r.getDisplayMetrics());
			int menu_width;
			if (metrics.widthPixels < metrics.heightPixels)
			{
				menu_width = (int) (metrics.widthPixels * 0.75);
			}
			else
			{
				menu_width = (int) (metrics.heightPixels * 0.75);
			}

			// adjust layout for share cam
			// RelativeLayout.LayoutParams layout = new
			// RelativeLayout.LayoutParams(
			// (int) px, LayoutParams.WRAP_CONTENT);
			RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams(
			        menu_width, LayoutParams.WRAP_CONTENT);
			// layout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			layout.addRule(RelativeLayout.CENTER_HORIZONTAL);

			float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
			        24, r.getDisplayMetrics());
			layout.setMargins(0, (int) (px / 4), 0, (int) px);

			leftSideMenu.setLayoutParams(layout);

			gridview.setAdapter(null);
			
//			if (selected_channel != null && selected_channel.getCamProfile() != null &&
//					!selected_channel.getCamProfile().isInLocal())
//			{
//				//disable talkback in remote
//				shouldEnableMic = false;
//			}
			
			if (leftSideMenuAdpt == null)
			{
				leftSideMenuAdpt = new LeftSideMenuImageAdapter(getActivity()
				        .getApplicationContext(), shouldEnableMic,
				        shouldTurnOnPanTilt, shouldTurnOnMelody);
			}

			int numCols = getNumberOfColumns();
			gridview.setNumColumns(numCols);
			gridview.setColumnWidth(menu_width / numCols);

			gridview.setAdapter(null);
			if (leftSideMenuAdpt == null)
			{
				leftSideMenuAdpt = new LeftSideMenuImageAdapter(getActivity(),
				        shouldEnableMic, shouldTurnOnPanTilt,
				        shouldTurnOnMelody);
			}

			gridview.setAdapter(leftSideMenuAdpt);
			gridview.invalidate();

			gridview.setOnTouchListener(new OnTouchListener()
			{

				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					switch (event.getAction() & MotionEvent.ACTION_MASK)
					{

					case MotionEvent.ACTION_DOWN:
						showSideMenusAndStatus();
						break;
					case MotionEvent.ACTION_UP:
						for (int i = 0; i < ((GridView) v).getChildCount(); i++)
						{
							View img = ((GridView) v).getChildAt(i);
							img.dispatchTouchEvent(MotionEvent.obtain(0,
							        System.currentTimeMillis(),
							        MotionEvent.ACTION_UP, 0, 0, 0));
						}

						tryToGoToFullScreen();
						break;
					default:

						break;
					}

					/* return false means we don't handle the event */
					return false;
				}
			});

			// ... and the handlers
			gridview.setOnItemClickListener(new OnItemClickListener()
			{
				public void onItemClick(AdapterView<?> parent, final View v,
				        int position, long id)
				{

					if (shouldEnableMic == true && shouldTurnOnPanTilt == true
					        && shouldTurnOnMelody == true)
					{
						switch (position)
						{
						case 0:
							setupPan(position);
							break;
						case 1:
							setupMic(position);
							break;
						case 2:
							setupRecord(position);
							break;
						case 3:
							setupMelody(position);
							break;
						case 4:
							setupTemperature(position);
							break;
						default:
							break;
						}
					}
					else if (shouldEnableMic == true
					        && shouldTurnOnPanTilt == false
					        && shouldTurnOnMelody == true)
					{
						// Focus 66
						switch (position)
						{
						case 0:
							setupMic(position);
							break;
						case 1:
							setupRecord(position);
							break;
						case 2:
							setupMelody(position);
							break;
						case 3:
							setupTemperature(position);
							break;
						default:
							break;
						}
					}
					else if (shouldEnableMic == false
					        && shouldTurnOnPanTilt == true
					        && shouldTurnOnMelody == true)
					{
						// Share cam 36/41 Windows
						switch (position)
						{
						case 0:
							setupPan(position);
							break;
						case 1:
							setupRecord(position);
							break;
						case 2:
							setupMelody(position);
							break;
						case 3:
							setupTemperature(position);
							break;
						default:
							break;
						}
					}
					else if (shouldEnableMic == false
					        && shouldTurnOnPanTilt == false
					        && shouldTurnOnMelody == true)
					{
						// Share cam 33 Windows
						switch (position)
						{
						case 0:
							setupRecord(position);
							break;
						case 1:
							setupMelody(position);
							break;
						case 2:
							setupTemperature(position);
							break;
						default:
							break;
						}
					}
					else if (shouldEnableMic == false
					        && shouldTurnOnPanTilt == false
					        && shouldTurnOnMelody == false)
					{
						// Share cam 33
						switch (position)
						{
						case 0:
							setupRecord(position);
							break;
						case 1:
							setupTemperature(position);
							break;
						default:
							break;
						}
					}

					RelativeLayout leftMenu = (RelativeLayout) getView()
					        .findViewById(R.id.left_side_menu);
					if (leftMenu != null)
					{
						GridView gridview = (GridView) leftMenu
						        .findViewById(R.id.slide_content);
						gridview.invalidateViews();

					}
				}
			});
		}
	}

	public LeftSideMenuImageAdapter getLeftSideMenuAdapter()
	{
		return leftSideMenuAdpt;
	}

	public void setMac(String mac)
	{
		// TODO Auto-generated method stub
		this.mac = mac;

	}

	public void grayOut()
	{
		Canvas c = null;
		Paint paint = new Paint();
		paint.setAlpha(50);
		SurfaceHolder _surfaceHolder = mMovieView.getHolder();
		if (_surfaceHolder == null)
		{
			Log.w("mbp", "_surfaceHolder is NULL");
			return;
		}

		try
		{
			c = _surfaceHolder.lockCanvas(null);
			if (c == null)
			{
				return;
			}

			synchronized (_surfaceHolder)
			{
				// c.drawBitmap(background,0,0,null);
				{

					c.drawColor(0x88aaaaaa, Mode.SRC_OVER);
				}
			}
		}
		finally
		{
			// do this in a finally so that if an exception is thrown
			// during the above, we don't leave the Surface in an
			// inconsistent state
			if (c != null)
			{
				_surfaceHolder.unlockCanvasAndPost(c);
			}
		}

	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		if (activity instanceof LeftSideMenuItemListener)
		{
			leftSideMenuListener = (LeftSideMenuItemListener) activity;
		}
		else
		{
			throw new ClassCastException(activity.toString()
			        + " must implement LeftSideMenuItemListener.");
		}

		liveFragmentListener = (ILiveFragmentCallback) activity;
	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		super.onDetach();
		leftSideMenuListener = null;
		liveFragmentListener = null;
	}

	public IGotoFullScreenHandler getOnGotoFullScreen()
	{
		return onGotoFullScreen;
	}

	public void setOnGotoFullScreen(IGotoFullScreenHandler onGotoFullScreen)
	{
		this.onGotoFullScreen = onGotoFullScreen;
	}

}
