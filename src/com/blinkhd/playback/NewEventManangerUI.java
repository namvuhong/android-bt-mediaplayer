package com.blinkhd.playback;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.List;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.msc3.VoxActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nxcomm.meapi.User;
import com.nxcomm.meapi.device.Event;
import com.nxcomm.meapi.user.UserInformation;

public class NewEventManangerUI extends FragmentActivity  {
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	private static final String TAG = "NewUI";
	private ListView listView;
	private List<Event> arrayOfList;
	private DisplayImageOptions options;

	private Event[] events;
	private String mac;
	
	public String getMac()
	{
		return mac;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// set the content view

		// get intent data

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mac = extras.getString(VoxActivity.TRIGGER_MAC_EXTRA);
			// eventTimeCode = extras
			// .getString(VoxActivity.TRIGGER_EVENT_CODE_EXTRA);
			// eventTimeCode = "04_20130803111213400";
		} else {
			Log.d(TAG, "Extra is null.");
		}

		try {
			setContentView(R.layout.activity_new_event_mananger_ui);
			setupSlidingMenu();
			goEalier();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// create sliding menu item

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		// Pass the event to ActionBarDrawerToggle, if it returns
		// true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		if (id == R.id.btnLive) {
			goLive();
		} else if (id == R.id.btnEalier) {
			goEalier();
		}
		// Handle your other action bar items...

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.actionbar_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	protected void onStart() {
		super.onStart();

	}

	private void goEalier() {
		
		EalierFragment newFragment = new EalierFragment();
		//newFragment.setMac(this.mac);
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();
				

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.fragmentHolder, newFragment, "EALIER_FRAG");
		// transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();
		
		//loadingEvents();
	}
	private void setupSlidingMenu()
	{
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.app_name, /* "open drawer" description */
		R.string.app_name /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
			}

			/** Called when a drawer has settled in a completely open state. */
			@SuppressLint("NewApi")
			@TargetApi(Build.VERSION_CODES.HONEYCOMB)
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
			}
		};
		
		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
/*
		try {
			ListView listView = (ListView) findViewById(R.id.listViewMenuItems);

			SimpleSlidingMenuItemAdapter adapter = new SimpleSlidingMenuItemAdapter(
					this);
			for (int i = 0; i < 8; i++) {
				adapter.add(new SimpleSlidingMenuItem("Sample Menu Item",
						R.drawable.camera_online));
			}
			if (listView == null) {
				System.out.println("ListView Menu Item is NULL");
			}
			listView.setAdapter(adapter);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
*/
		
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		displayUserInfo(saved_token);
		
		
	}
	private void displayUserInfo(String userToken) {

		UserInformation info;
		try {
			info = User.getUserInfo(userToken);
			// get user name and user email to display
			if (info.getStatus() == 200) {
				TextView tvUserName = (TextView) findViewById(
						R.id.txtUsername);
				if (tvUserName != null)
					tvUserName.setText("Hi, " + info.getName());

				TextView tvUserEmail = (TextView) findViewById(
						R.id.txtEmail);
				if (tvUserEmail != null)
					tvUserEmail.setText(info.getEmail());
			}
			// end get user name and user email to display
		} catch (SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void goLive() {
		
		LiveFragment newFragment = new LiveFragment();
		FragmentTransaction transaction = this.getFragmentManager().beginTransaction();

		// Replace whatever is in the fragment_container view with this
		// fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.fragmentHolder, newFragment);
		// transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
		
		
	}

	


}
