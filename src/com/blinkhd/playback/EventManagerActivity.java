package com.blinkhd.playback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.msc3.VoxActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageLoadingProgressListener;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.meapi.device.TimelineEvent;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;

public class EventManagerActivity extends Activity
{

	private static final String	TAG	                                 = "mbp";
	public static final long	CHECK_LATEST_CLIP_AVAILABLE_INTERVAL	= 5 * 1000;
	private Timer	            enableViewRecordButton;

	private Button	            btnViewRecord;
	private Button	            btnViewCamera;
	private Button	            btnCameraSetting;
	private Button	            btnIgnore;
	
	private LinearLayout		viewRecordGrp;

	private TimelineEvent	    latest;
	private String	            regId, eventTimeCode;
	private ImageView	        imgView;
	private Animation	        anim	                             = null;
	private ImageView	        loader;

	private LoadPlaylistTask	loadPlaylistTask;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// get intent data

		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			regId = extras.getString(VoxActivity.TRIGGER_MAC_EXTRA);
			eventTimeCode = extras
			        .getString(VoxActivity.TRIGGER_EVENT_CODE_EXTRA);
			// eventTimeCode = "04_20130803111213400";

		}
		else
		{
			Log.d(TAG, "Extra is null.");
		}
		/* 20131217: phung: Test EventManager */
		// eventTimeCode="04_20131216001522000";
		// mac= "44334C7E0CAB";
		//
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		// SharedPreferences.Editor editor = settings.edit();
		// editor.putString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
		// "XU6JaBuARg9sAWXCxRBL");
		// editor.commit();

		// String serverUrl = settings.getString(
		// PublicDefine.PREFS_SAVED_SERVER_URL, "http://api.hubble.in/v1");
		// PublicDefines.SERVER_URL = serverUrl;

		/** Test EventManager **/

		setContentView(R.layout.activity_event_manager);

		loader = (ImageView) findViewById(R.id.imageLoader);
		loader.setBackgroundResource(R.drawable.loader_anim1);
		anim = AnimationUtils.loadAnimation(this, R.anim.loader_anim);
		anim.start();

		loadPlaylistTask = null;

		loadingEvents();

	}

	protected void onStart()
	{
		super.onStart();

	}

	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();

		if (enableViewRecordButton != null)
		{
			enableViewRecordButton.cancel();
			enableViewRecordButton = null;
		}
	}

	protected void onDestroy()
	{
		super.onDestroy();

		if (enableViewRecordButton != null)
		{
			enableViewRecordButton.cancel();
			enableViewRecordButton = null;
		}
	}

	private void loadingEvents()
	{

		imgView = (ImageView) this.findViewById(R.id.imageViewAction);

		enableViewRecordButton = new Timer();

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		final String saved_token = settings.getString(
		        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		btnIgnore = (Button) findViewById(R.id.ignorebtn);
		btnIgnore.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				finish();

			}
		});

		viewRecordGrp = (LinearLayout) findViewById(R.id.btngrp1);
		btnViewRecord = (Button) findViewById(R.id.playbtn);

		btnViewCamera = (Button) findViewById(R.id.goToCamerabtn);
		btnViewCamera.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// cancel query events timertask
				if (enableViewRecordButton != null)
				{
					enableViewRecordButton.cancel();
					enableViewRecordButton = null;
				}
				SharedPreferences settings = getSharedPreferences(
				        PublicDefine.PREFS_NAME, 0);
				Editor editor = settings.edit();
				editor.putString(
				        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
				        regId);
				editor.commit();
				Intent i = new Intent(EventManagerActivity.this,
				        HomeScreenActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		});

		btnCameraSetting = (Button) findViewById(R.id.settingsbtn);
		btnCameraSetting.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO: Go to camera setting

				// For now test Upgrade
				RelativeLayout freemiumGrp = (RelativeLayout) findViewById(R.id.freemium_grp);
				RelativeLayout notifcationGrp = (RelativeLayout) findViewById(R.id.notification_grp);
				if (freemiumGrp != null)
				{

					freemiumGrp.setVisibility(View.VISIBLE);
					notifcationGrp.setVisibility(View.GONE);
				}
			}
		});

		String time_format = "";
		String alert_type = null, alert_timeCode = null;

		// check if event timecode is in the form xx_[timecode]
		if (eventTimeCode.contains("_"))
		{
			String[] tokens = eventTimeCode.split("_");
			if (tokens.length == 2)
			{
				alert_type = tokens[0];
				alert_timeCode = tokens[1];
				SimpleDateFormat format1 = new SimpleDateFormat(
				        "yyyyMMddHHmmss");
				Date eventDate = null;
				try
				{
					eventDate = format1.parse(alert_timeCode);

					format1 = new SimpleDateFormat("HH:mm aaa");
					format1.setTimeZone(TimeZone.getDefault());

				}
				catch (ParseException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				time_format = format1.format(eventDate);

			}

			TextView time = (TextView) findViewById(R.id.msgTime);
			time.setText(time_format);
			time.setTextSize(14);

		}

		if (Utils.isNetworkAvailable(EventManagerActivity.this))
		{

			Log.d("mbp", "LOAD LATEST EVENT: " + eventTimeCode);
			// 1st Load ONE latest event only..
			loadPlaylistTask = new LoadPlaylistTask(new LoadPlaylistListener()
			{

				@Override
				public void onRemoteCallSucceeded(TimelineEvent latest,
				        TimelineEvent[] allEvents)
				{
					if (latest == null)
					{
						loader.clearAnimation();
						loader.setVisibility(View.INVISIBLE);
					}
					else
					{

						EventManagerActivity.this.latest = latest;
						updateMainSnapshot();

						if (latest.getData() != null
						        && latest.getData()[0] != null
						        && latest.getData()[0].getFile() != null
						        && !latest.getData()[0].getFile().isEmpty())
						{
							Log.d("mbp", "Query clips success");
							viewRecordGrp.setVisibility(View.VISIBLE);
							btnViewRecord.setOnClickListener(new View.OnClickListener()
							{
								public void onClick(View v)
								{
									if (EventManagerActivity.this.latest != null)
									{
										// Perform action on click
										Intent intent = new Intent(
										        EventManagerActivity.this,
										        FFMpegPlaybackActivity.class);
										intent.putExtra(
										        FFMpegPlaybackActivity.STR_DEVICE_MAC,
										        regId);
										intent.putExtra(
										        FFMpegPlaybackActivity.STR_EVENT_CODE,
										        eventTimeCode);

										startActivity(intent);

									}
								}
							});
						}
						else
						{
							if (enableViewRecordButton != null)
							{
								enableViewRecordButton.schedule(
										new EnableViewRecordButtonTimerTask(this),
										CHECK_LATEST_CLIP_AVAILABLE_INTERVAL);
							}

						}
					}

				}

				@Override
				public void onRemoteCallFailed(int errorCode)
				{
					switch (errorCode)
					{
					case LoadPlaylistTask.IO_EXCEPTION:
					case LoadPlaylistTask.MALFORMED_URL_EXCEPTION:
					case LoadPlaylistTask.SOCKET_TIMEOUT_EXCEPTION:
					{
						Spanned msg = Html.fromHtml(getString(R.string.timeout_while_connecting_to_server_retry_));
						AlertDialog.Builder builder = new AlertDialog.Builder(
						        EventManagerActivity.this);
						builder.setMessage(msg)
						        .setCancelable(true)
						        .setNegativeButton(
						                getResources().getString(
						                        R.string.Cancel),
						                new DialogInterface.OnClickListener()
						                {
							                public void onClick(
							                        DialogInterface dialog,
							                        int which)
							                {
								                dialog.dismiss();
								                finish();
							                }
						                })
						        .setPositiveButton(
						                getResources().getString(R.string.OK),
						                new DialogInterface.OnClickListener()
						                {
							                public void onClick(
							                        DialogInterface dialog,
							                        int which)
							                {
								                dialog.dismiss();

								                // RELOAD..
								                loadingEvents();

							                }
						                });

						try
						{
							builder.create().show();
						}
						catch (Exception e)
						{

						}
						break;
					}
					default:
						break;
					}
				}

			}, false);

			loadPlaylistTask.execute(saved_token, regId, eventTimeCode);

		}
		else
		// No Network
		{
			// TODO: show some dialog ..
		}
	}

	private static final int	DIALOG_NO_CLIP	= 1;

	protected Dialog onCreateDialog(int id)
	{
		AlertDialog.Builder builder;
		AlertDialog alert;
		ProgressDialog dialog;
		Spanned msg;
		switch (id)
		{

		case DIALOG_NO_CLIP:
			builder = new AlertDialog.Builder(this);

			msg = Html.fromHtml("<big>" + "There is no clip for this event."
			        + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();

			return alert;
		default:
			return null;
		}
	}

	private void updateMainSnapshot()
	{

		/*
		 * options = new DisplayImageOptions.Builder().cacheInMemory()
		 * .cacheOnDisc().build();
		 * 
		 * imageLoader = ImageLoader.getInstance();
		 * imageLoader.init(ImageLoaderConfiguration
		 * .createDefault(EventManagerActivity.this));
		 */

		if (latest != null && latest.getData() != null
		        && latest.getData().length > 0)
		{
			Log.d("mbp", "Snapshot url: " + latest.getData()[0].getImage());
			ImageLoader.getInstance().displayImage(
			        latest.getData()[0].getImage(), imgView,
			        new ImageLoadingListener()
			        {

				        @Override
				        public void onLoadingStarted(String arg0, View arg1)
				        {

				        }

				        @Override
				        public void onLoadingFailed(String arg0, View arg1,
				                FailReason arg2)
				        {
					        loader.clearAnimation();
					        loader.setVisibility(View.INVISIBLE);

				        }

				        @Override
				        public void onLoadingComplete(String arg0, View arg1,
				                Bitmap arg2)
				        {
					        loader.clearAnimation();
					        loader.setVisibility(View.INVISIBLE);
				        }

				        @Override
				        public void onLoadingCancelled(String arg0, View arg1)
				        {

				        }
			        });

		}
		else
		{
			Log.d("mbp", "Can't load snap shot");
			loader.clearAnimation();
			loader.setVisibility(View.INVISIBLE);
		}
	}

	// inner class: EnableViewRecordButton
	class EnableViewRecordButtonTimerTask extends TimerTask
	{
		private LoadPlaylistListener	mListener;

		EnableViewRecordButtonTimerTask(LoadPlaylistListener listener)
		{
			if (listener == null)
			{
				throw new IllegalArgumentException("listener can't be NULL");
			}
			mListener = listener;
		}

		public void run()
		{
			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

			LoadPlaylistTask task = new LoadPlaylistTask(mListener, false);
			task.execute(saved_token, regId, eventTimeCode);

			Log.d(TAG, "EnableViewRecordButtonTimerTask is running.");
		}
	}

}
