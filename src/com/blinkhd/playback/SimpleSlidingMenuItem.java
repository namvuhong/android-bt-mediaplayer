package com.blinkhd.playback;


public class SimpleSlidingMenuItem {
	private String menuText;
	private String menuDesc;
	private int iconRes;
	private boolean haveHeaderText;
	private String headerText;
	public SimpleSlidingMenuItem(String menuText,String menuDesc, int iconRes) {
		this.setMenuText(menuText);
		this.setMenuDesc(menuDesc);
		this.setIconRes(iconRes);
		this.haveHeaderText = false;
	}
	public SimpleSlidingMenuItem(String menuText,String menuDesc, int iconRes, boolean haveHeaderText) {
		this.setMenuText(menuText);
		this.setMenuDesc(menuDesc);
		this.setIconRes(iconRes);
		this.haveHeaderText = haveHeaderText;
	}
	public void setHeaderText(String text)
	{
		this.headerText = text;
	}
	public String getHeaderText()
	{
		return this.headerText;
	}
	public boolean isHaveHeaderText()
	{
		return this.haveHeaderText;
	}
	public void setHaveHeaderText(boolean value)
	{
		this.haveHeaderText = value;
	}
	public String getMenuText() {
		return menuText;
	}
	public void setMenuText(String menuText) {
		this.menuText = menuText;
	}
	public String getMenuDesc() {
		return menuDesc;
	}
	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}
	public int getIconRes() {
		return iconRes;
	}
	public void setIconRes(int iconRes) {
		this.iconRes = iconRes;
	}
	
}

