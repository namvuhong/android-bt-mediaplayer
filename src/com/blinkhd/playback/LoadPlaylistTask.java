package com.blinkhd.playback;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Arrays;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.Event;
import com.nxcomm.meapi.device.GetRecordedVideosResponse;
import com.nxcomm.meapi.device.GetTimelineEventsResponse;
import com.nxcomm.meapi.device.TimelineEvent;

//My AsyncTask start...

public class LoadPlaylistTask extends AsyncTask<String, Void, Void> {

	private static String TAG = "LoadPlaylistTask";
	public static final int NO_ERROR = 0;
	public static final int SOCKET_TIMEOUT_EXCEPTION = 1;
	public static final int IO_EXCEPTION = 2;
	public static final int MALFORMED_URL_EXCEPTION = 3;


	ProgressDialog pDialog;
	private LoadPlaylistListener listener;
	private TimelineEvent latest;
	private TimelineEvent[] allEvents;
	private boolean getAllEvents;
	private int errorCode;

	public LoadPlaylistTask(LoadPlaylistListener listener, boolean getAllEvents) {
		this.listener = listener;
		this.getAllEvents = getAllEvents;
		this.errorCode = NO_ERROR;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		/*
		 * pDialog = new ProgressDialog(EventManagerActivity.this);
		 * pDialog.setMessage("Loading..."); pDialog.show();
		 */
	}
	
	/* on UI Thread - when cancelled don't invoke callback. */
	protected void onCancelled ()
	{
		//Do nothing on Cancelled
	}

	@Override
	protected Void doInBackground(String... params) {
		
		String saved_token = params[0];
		String mac = params[1];
		String before_start_time = null;
		String eventCode = params[2];
		String alerts = null;
		int page = -1;
		int size = -1;
		
		//String eventCode = params[2];
		
		
		TimelineEvent[] timelineEvents = null;
		try {
			//query for one event only 
			if (this.getAllEvents == false)
			{
				com.nxcomm.meapi.PublicDefines.setHttpTimeout(30*1000); 
				GetTimelineEventsResponse eventRes = 
						Device.getTimelineEvents(saved_token, mac, 
								before_start_time, eventCode, alerts, page, size);
				if (eventRes != null && 
						eventRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					timelineEvents = eventRes.getEvents();
				}

				if (timelineEvents != null && timelineEvents.length > 0)
				{
					latest = timelineEvents[0];
				} else {
					Log.d(TAG, "Latest event is null");
				}
			}
			else // Get all recorded playlist
			{
				
				com.nxcomm.meapi.PublicDefines.setHttpTimeout(30*1000); 
				GetTimelineEventsResponse eventRes = 
						Device.getTimelineEvents(saved_token, mac);
				if (eventRes != null && 
						eventRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					timelineEvents = eventRes.getEvents();
				}
				allEvents = timelineEvents;
				
			}
		} catch (SocketTimeoutException e) {
			
			 errorCode = SOCKET_TIMEOUT_EXCEPTION;
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			 errorCode = MALFORMED_URL_EXCEPTION;
			e.printStackTrace();
		} catch (IOException e) {
			
			// TODO Auto-generated catch block
			errorCode = IO_EXCEPTION;
			e.printStackTrace();
		}
		catch (Exception ex)
		{
			errorCode = IO_EXCEPTION;
			ex.printStackTrace();
		}

		if (isCancelled())
		{
			return null;
		}
				
		
		
		return null;
	}

	@Override
	protected void onCancelled(Void result) {
		// TODO Auto-generated method stub
		super.onCancelled(result);
		Log.d("mbp", "LoadPlaylistTask is cancelled.");
	}

	@Override
	protected void onPostExecute(Void result) {
	
		if(this.isCancelled())
		{
			Log.d("mbp", "LoadPlaylistTask is cancelled.");
		}
		else
		{
			super.onPostExecute(result);
			if(errorCode == NO_ERROR)
			{
				Gson gson = new Gson();
				if(this.listener != null)
				{
					this.listener.onRemoteCallSucceeded(latest, allEvents);
				}
			}
			else
			{
				if(this.listener != null)
				{
					this.listener.onRemoteCallFailed(errorCode);
				}
			}
		}
		/*
		 * if (null != pDialog && pDialog.isShowing()) { pDialog.dismiss(); }
		 * 
		 * if (null == arrayOfList || arrayOfList.size() == 0) {
		 * showToast("No data found from web!!!");
		 * EventManagerActivity.this.finish(); } else {
		 * 
		 * // check data... /* for (int i = 0; i < arrayOfList.size(); i++) {
		 * Item item = arrayOfList.get(i); System.out.println(item.getId());
		 * System.out.println(item.getTitle());
		 * System.out.println(item.getDesc());
		 * System.out.println(item.getPubdate());
		 * System.out.println(item.getLink()); }
		 */

		/*
		 * setAdapterToListview();
		 * 
		 * }
		 */

	}
}
