package com.blinkhd.playback;

import com.nxcomm.meapi.device.Event;
import com.nxcomm.meapi.device.TimelineEvent;

public interface LoadPlaylistListener 
{
	  public void onRemoteCallSucceeded(TimelineEvent latest,TimelineEvent[] allEvents);
	  public void onRemoteCallFailed(int errorCode);
}
