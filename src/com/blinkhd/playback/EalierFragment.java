package com.blinkhd.playback;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nxcomm.meapi.device.Event;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;

public class EalierFragment extends Fragment implements OnItemClickListener ,
		LoadPlaylistListener {
	private ListView listView;
	private List<Event> arrayOfList;
	private DisplayImageOptions options;
	private LoadPlaylistTask loadPlaylistTask ;
	private Event[] events;
	private String mac;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// Retain this fragment across configuration changes.
		setRetainInstance(true);
		this.setHasOptionsMenu(true);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		// setRetainInstance(true);
		if (savedInstanceState == null) 
		{
			loadingEvents();
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		
		View view =  inflater.inflate(R.layout.ealier_fragment, container, false);
//		SwipeDetector swipeDetector = new SwipeDetector();
//		view.setOnTouchListener(swipeDetector);
//		if (swipeDetector.getAction() == Action.RL) 
//		{
//			 MainActivity mainActivity = (MainActivity) this.getActivity();
//			 mainActivity.goLive(mac);
//		}
		return view;
	}
	
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		System.out.println("Ealier fragment is on Destroy.");
		if(loadPlaylistTask != null)
			loadPlaylistTask.cancel(true);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		System.out.println("Ealier fragment is on Detach.");
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		System.out.println("Ealier fragment is on Stop.");
	}

	private void loadingEvents() {

		listView = (ListView) getView().findViewById(R.id.listViewAllEvents);
		listView.setOnItemClickListener(this);

		SharedPreferences settings = this.getActivity().getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		
		// GetRecordedVideosResponse res = Device.getRecordedVideos(saved_token,
		// mac, eventTimeCode);

		if (Utils.isNetworkAvailable(getActivity())) {
			loadPlaylistTask = new LoadPlaylistTask(this, true);
			loadPlaylistTask.execute(saved_token, this.mac, "04", "0");
		} else {
			// TODO: show some dialog ..
		}

	}

	

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		/*
		 * Item item = arrayOfList.get(position); Intent intent = new
		 * Intent(EventManagerActivity.this, DetailActivity.class);
		 * intent.putExtra("url", item.getLink()); intent.putExtra("title",
		 * item.getTitle()); intent.putExtra("desc", item.getDesc());
		 * startActivity(intent);
		 */
		Intent intent = new Intent(this.getActivity(),
				FFMpegPlaybackActivity.class);
		Event event = arrayOfList.get(position);

		String streamUrl = null;
		if (event.getPlaylist().length > 0) {
			streamUrl = event.getPlaylist()[0].getFilePath();
			if (streamUrl != null) {
				ClipInfo info = null;
				try {
					info = new ClipInfo(streamUrl);

					Log.d("mbp", "Snapshot url: " + streamUrl);
					intent.putExtra(FFMpegPlaybackActivity.STR_DEVICE_MAC,
							mac);

					if (info != null) {
						intent.putExtra(FFMpegPlaybackActivity.STR_EVENT_CODE,
								info.getEventTimeCode());
					}

					startActivity(intent);
				} catch (ParseException e) {
					e.printStackTrace();
					this.getActivity().showDialog(DIALOG_NO_CLIP);
				} catch (StringIndexOutOfBoundsException ex) 
				{
					ex.printStackTrace();
					this.getActivity().showDialog(DIALOG_NO_CLIP);
				}

			} else {
				Log.d("mpb", "Playlist file field empty.");
			}
		}

	}

	private static final int DIALOG_NO_CLIP = 1;

	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		ProgressDialog dialog;
		Spanned msg;
		switch (id) {

		case DIALOG_NO_CLIP:
			builder = new AlertDialog.Builder(this.getActivity());

			msg = Html.fromHtml("<big>" + "There is no clip for this event."
					+ "</big>");
			builder.setMessage(msg)
					.setCancelable(true)
					.setPositiveButton(getResources().getString(R.string.OK),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface diag,
										int which) {
									diag.cancel();
								}
							}).setOnCancelListener(new OnCancelListener() {
						@Override
						public void onCancel(DialogInterface diag) {
						}
					});

			alert = builder.create();

			return alert;
		default:
			return null;
		}
	}

	public void setAdapterToListview() {
		EventRowAdapter objAdapter = new EventRowAdapter(this.getActivity(),
				R.layout.events_row, arrayOfList);

		listView.setAdapter(objAdapter);
	}

	@Override
	public void onRemoteCallSucceeded(Event latest, Event[] allEvents) {
		// TODO Auto-generated method stub
		
		if (allEvents != null) {
			this.events = allEvents;

			this.arrayOfList = new ArrayList<Event>();

			ProgressBar pbar = (ProgressBar) getView().findViewById(
					R.id.prgPreview);
			pbar.setVisibility(View.INVISIBLE);

			for (int i = 0; i < events.length; i++) {
				this.arrayOfList.add(events[i]);
			}
			if (this.events.length > 0) {
				setAdapterToListview();
			}
		} else {
			// No events
		}

	}

	// TODO: @Son use Diaglog Fragment to replace showDialog

	@Override
	public void onRemoteCallFailed(int errorCode) {
		switch (errorCode) {
		case LoadPlaylistTask.IO_EXCEPTION:
		case LoadPlaylistTask.MALFORMED_URL_EXCEPTION:
		case LoadPlaylistTask.SOCKET_TIMEOUT_EXCEPTION: {
			Spanned msg = Html
					.fromHtml(getString(R.string.timeout_while_connecting_to_server_retry_));
			AlertDialog.Builder builder = new AlertDialog.Builder(
					this.getActivity());
			builder.setMessage(msg)
					.setCancelable(true)
					.setNegativeButton(
							getResources().getString(R.string.Cancel),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();

								}
							})
					.setPositiveButton(getResources().getString(R.string.OK),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();

									// RELOAD..
									loadingEvents();

								}
							});
			try {
				builder.create().show();
			} catch (Exception e) {
			}

			break;
		}
		default:
			break;
		}
	}
	@Override
	public void onCreateOptionsMenu(
	      Menu menu, MenuInflater inflater) {
	   inflater.inflate(R.menu.ealier_fragment_action_bar_button, menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
//		if(item.getItemId() == R.id.btnLive)
//		{
//			MainActivity mainActivity = (MainActivity)this.getActivity();
//			mainActivity.goLive(mac);
//		}
		
		return super.onOptionsItemSelected(item);
	}

	public void setMac(String value)
	{
		this.mac = value;
	}
}