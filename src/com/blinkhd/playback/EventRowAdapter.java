package com.blinkhd.playback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blinkhd.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nxcomm.meapi.device.Event;

public class EventRowAdapter extends ArrayAdapter<Event>
{
	private Activity	activity;
	private List<Event>	items;
	private Event	    objBean;
	private int	        row;

	public EventRowAdapter(Activity act, int resource, List<Event> arrayOfList)
	{
		super(act, resource, arrayOfList);
		this.activity = act;
		this.row = resource;
		this.items = arrayOfList;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		ViewHolder holder;
		if (view == null)
		{
			LayoutInflater inflater = (LayoutInflater) activity
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(row, null);

			holder = new ViewHolder();
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}

		if ((items == null) || ((position + 1) > items.size()))
			return view;

		objBean = items.get(position);

		holder.tvTitle = (TextView) view.findViewById(R.id.txtActionName);
		holder.tvTime = (TextView) view.findViewById(R.id.txtTime);
		holder.tvDate = (TextView) view.findViewById(R.id.txtDate);

		holder.imageViewAction = (ImageView) view
		        .findViewById(R.id.imageViewAction);
		holder.imageViewSnapShot = (ImageView) view
		        .findViewById(R.id.imageViewSnapShot);
		holder.pbar = (ProgressBar) view.findViewById(R.id.pbar);

		if (holder.tvTitle != null
		        && null != objBean.getPlaylist()[0].getTitle()
		        && objBean.getPlaylist()[0].getTitle().trim().length() > 0)
		{
			holder.tvTitle.setText(Html.fromHtml(objBean.getPlaylist()[0]
			        .getTitle()));
		}
		else
		{
			holder.tvTitle.setText(R.string.motion_detected);
		}
		ClipInfo clip_inf = null;
		try
		{
			clip_inf = new ClipInfo(objBean.getPlaylist()[0].getSnapshotURL());
			Date dateTime = clip_inf.getDateTime();

			TimeZone tz = TimeZone.getDefault();

			SimpleDateFormat destFormat = new SimpleDateFormat("HH:mm:ss");
			destFormat.setTimeZone(tz);
			String result = destFormat.format(dateTime);

			SimpleDateFormat dateFormat = new SimpleDateFormat(
			        "EE, dd-MMM-yyyy");
			dateFormat.setTimeZone(tz);
			String date = dateFormat.format(dateTime);

			holder.tvTime.setText(result);
			holder.tvDate.setText(date);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// holder.tvTime.setText("Invalid Date");
		}

		if (holder.imageViewSnapShot != null)
		{

			if (null != objBean.getPlaylist()[0].getSnapshotURL()
			        && objBean.getPlaylist()[0].getSnapshotURL().trim()
			                .length() > 0)
			{
				final ProgressBar pbar = holder.pbar;

				ImageLoader.getInstance().displayImage(
				        objBean.getPlaylist()[0].getSnapshotURL(),
				        holder.imageViewSnapShot);

			}
			else
			{
				holder.imageViewSnapShot.setImageResource(R.drawable.profile);
			}
		}

		return view;
	}

	public class ViewHolder
	{
		public TextView		tvTitle, tvTime, tvDate;
		private ImageView	imageViewAction, imageViewSnapShot;
		private ProgressBar	pbar;
	}
}
