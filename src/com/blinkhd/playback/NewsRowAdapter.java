package com.blinkhd.playback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blinkhd.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nxcomm.meapi.device.TimelineEvent;

public class NewsRowAdapter extends ArrayAdapter<TimelineEvent>
{

	private Activity	        activity;
	private List<TimelineEvent>	items;
	private TimelineEvent	    objBean;
	private int	                row;

	public NewsRowAdapter(Activity act, int resource,
	        List<TimelineEvent> arrayOfList)
	{
		super(act, resource, arrayOfList);
		this.activity = act;
		this.row = resource;
		this.items = arrayOfList;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		ViewHolder holder;
		if (view == null)
		{
			LayoutInflater inflater = (LayoutInflater) activity
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(row, null);

			holder = new ViewHolder();
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}

		if ((items == null) || ((position + 1) > items.size()))
			return view;

		objBean = items.get(position);

		holder.tvTitle = (TextView) view.findViewById(R.id.tvtitle);
		holder.tvDesc = (TextView) view.findViewById(R.id.tvdesc);
		holder.tvDate = (TextView) view.findViewById(R.id.tvdate);
		holder.imgView = (ImageView) view.findViewById(R.id.image);
		holder.pbar = (ProgressBar) view.findViewById(R.id.pbar);

		if (holder.tvTitle != null && null != objBean.getData()[0].getTitle()
		        && objBean.getData()[0].getTitle().trim().length() > 0)
		{
			holder.tvTitle.setText(Html.fromHtml(objBean.getData()[0]
			        .getTitle()));
		}
		else
		{
			holder.tvTitle.setText(R.string.motion_detected);
		}

		// 20130826: phung : hide description field
		holder.tvDesc.setVisibility(View.INVISIBLE);

		ClipInfo clip_inf = null;
		try
		{
			clip_inf = new ClipInfo(objBean.getData()[0].getImage());
			Date dateTime = clip_inf.getDateTime();
			TimeZone tz = TimeZone.getDefault();
			SimpleDateFormat destFormat = new SimpleDateFormat(
			        "yyyy-MM-dd HH:mm:ss");
			destFormat.setTimeZone(tz);
			String result = destFormat.format(dateTime);
			holder.tvDate.setText(result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			holder.tvDate.setText("Invalid Date");
		}

		if (holder.imgView != null)
		{
			if (null != objBean.getData()[0].getImage()
			        && objBean.getData()[0].getImage().trim().length() > 0)
			{
				final ProgressBar pbar = holder.pbar;

				ImageLoader.getInstance().displayImage(
				        objBean.getData()[0].getImage(), holder.imgView,
				        new ImageLoadingListener()
				        {

					        @Override
					        public void onLoadingCancelled(String arg0,
					                View arg1)
					        {
						        // TODO Auto-generated method stub

					        }

					        @Override
					        public void onLoadingComplete(String arg0,
					                View arg1, Bitmap arg2)
					        {
						        pbar.setVisibility(View.INVISIBLE);
					        }

					        @Override
					        public void onLoadingFailed(String arg0, View arg1,
					                FailReason arg2)
					        {
						        pbar.setVisibility(View.INVISIBLE);
					        }

					        @Override
					        public void onLoadingStarted(String arg0, View arg1)
					        {
						        pbar.setVisibility(View.VISIBLE);
					        }
				        });

			}
			else
			{
				holder.imgView.setImageResource(R.drawable.profile);
			}
		}

		return view;
	}

	public class ViewHolder
	{
		public TextView		tvTitle, tvDesc, tvDate;
		private ImageView	imgView;
		private ProgressBar	pbar;
	}

}