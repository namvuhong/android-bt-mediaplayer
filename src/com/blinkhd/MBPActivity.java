package com.blinkhd;

import java.util.Locale;

import com.msc3.CameraConnectivityDetector;
import com.msc3.CameraDetectorService;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.jstun_android.RtspStunBridgeService;
import com.nxcomm.meapi.PublicDefines;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MBPActivity extends Activity   {

	public static final String GOOGLE_API_PROJECT_ID =  "720741930269";
	public static final String FFMPEG_STREAMER_APP_PACKAGE = "cz.havlena.ffmpeg.ui";
	
	private static final int DIALOG_STORAGE_UNAVAILABLE = 1;

	/* set by phone software */
	private static final String bool_LauchFirstTime ="LauchFirstTime";
	
	public static final int bool_InfraModeInitialSetup = 1;
	public static final int bool_InfraMode = 2;
	
	public static final String bool_VoxDirectMode="VoxDirectMode";
	public static final String bool_VoxInfraMode="VoxInfraMode";
	public static final String string_VoxDeviceAddr="VoxDeviceAddr";

	private AnimationDrawable anim = null; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.bb_is_first_screen);
		
		ImageView loader = (ImageView) findViewById(R.id.imageLoader);
		loader.setBackgroundResource(R.drawable.loader_anim1);
		anim = (AnimationDrawable) loader.getBackground();
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings.getString(PublicDefine.PREFS_SAVED_SERVER_URL,
				"https://api.hubble.in/v1");
		PublicDefines.SERVER_URL = serverUrl;

//		Intent serviceIntent = new Intent(this.getApplicationContext(),RtspStunBridgeService.class);
//		if (!isPjnathServiceRunning(this))
//		{
//			Log.i("MBP", "Start new pjnat service"); 
//			startService(serviceIntent);
//		}
//
//		String phonemodel = android.os.Build.MODEL;
//		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) ||
//				phonemodel.equals(PublicDefine.PHONE_MBP1k))
//		{
//			if (isCamDetectServiceRunning(this))
//			{
//				Log.i("MBP", "Stop Cam Detector");
//				Intent i = new Intent(this,CameraDetectorService.class);
//				stopService(i);
//			}
//
//			/*20121204: phung: clear all disconnect alerts */ 
//			NotificationManager notificationManager = (NotificationManager)
//					getSystemService(Context.NOTIFICATION_SERVICE);
//
//			for (int i =0; i< CameraConnectivityDetector.notificationIds.length; i++)
//			{
//				notificationManager.cancel(CameraConnectivityDetector.notificationIds[i]);
//			}
//		}

	}

	public static boolean isPjnathServiceRunning(Context c) {
		ActivityManager manager = (ActivityManager) c.getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.nxcomm.jstun_android.RtspStunBridgeService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		boolean isNotFirstTime = settings.getBoolean(PublicDefine.PREFS_APP_LAUNCHED_NOT_FIRST_TIME, false);
		
		// runs the first time
		if (isNotFirstTime == false)//if ( goToRouterLogin)
		{
			Log.d("mbp", "FIRST TIME Run: Set some default settings");
			//20121203: set default alert while in call & recurring to ON
			SharedPreferences.Editor editor = settings.edit(); 
			editor.putBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_IS_RECURRING, true); 
			editor.putBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL, true); 
			editor.putBoolean(PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_ALERT, true);

			/* 20130111: hoang: issue 1034
			 * set default temperature unit based on language setting
			 */

			String language = Locale.getDefault().getDisplayName();
			if (language.equalsIgnoreCase(Locale.US.getDisplayName()))
			{
				editor.putInt(PublicDefine.PREFS_TEMPERATURE_UNIT, PublicDefine.TEMPERATURE_UNIT_DEG_F);
			}
			else
			{
				editor.putInt(PublicDefine.PREFS_TEMPERATURE_UNIT, PublicDefine.TEMPERATURE_UNIT_DEG_C);
			}
			editor.commit();

			RelativeLayout root = (RelativeLayout) findViewById(R.id.root_view);
			if (root != null)
			{
				if (someExtraChecks() == -1) // currently this will never run, someExtraChecks returns 0
				{
					Dialog alert;
					String _msg = "This camera is not registered. Setup camera failed.";

					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					Spanned msg = Html.fromHtml("<big>" + _msg  + "</big>");
					builder.setMessage(msg)
					.setCancelable(false)
					.setPositiveButton(getResources().getString(R.string.OK),
							new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();
							finish();

						}
					});
					alert = builder.create();
					alert.show();
				}
				else // this always runs
				{
					root.post(new Runnable() {

						@Override
						public void run() {
//							Intent intent = new Intent(MBPActivity.this, FirstTimeActivity.class);
//							intent.putExtra(FirstTimeActivity.bool_InfraModeInitialSetup,true);
//							startActivity(intent);
//							finish();
							
							proceed(bool_InfraModeInitialSetup);
						}
					});
				}
			}
		}
		else // runs any time that is not the first time
		{
			Log.d("mbp", "NOT FIRST TIME ");

			PackageInfo pinfo;
			String versionName = "0.0";
			try {
				pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				versionName = pinfo.versionName;
			}
			catch (NameNotFoundException e) {
				e.printStackTrace();
			}

			TextView version = (TextView) findViewById(R.id.versionText);
			if (version != null)
				version.setText(getResources().getString(R.string.Version) + versionName);

			Handler mHandler = new Handler();
			MyRunnable goToMain = null;
			goToMain = new MyRunnable(this); 
			mHandler.post(goToMain);
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			if (!isCamDetectServiceRunning(this))
			{
				Log.i("MBP", "Start Cam Detector");
				Intent i = new Intent(this,CameraDetectorService.class);
				startService(i);
			}
		}
	}
	
	@Override
	public void onWindowFocusChanged (boolean hasFocus)
	{
		if (hasFocus == true && anim != null)
		{
			//opt2
			anim.start();
		}
		else if (hasFocus == false && anim!= null)
		{
			//opt2
			//anim.cancel();
			//anim.stop(); 
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
		super.onConfigurationChanged(newConfig);
		
		Log.i("MBP", "onConfigurationChanged- newconf: "+ newConfig);
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		Spanned msg;
		switch (id) {
		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					
					MBPActivity.this.finish();
				}
			});

			alert = builder.create();
			return alert;
		}
		return super.onCreateDialog(id);
	}
	
	protected void onNewIntent (Intent intent)
	{
		this.setIntent(intent);
	}
	
	private class MyRunnable implements Runnable
	{
		private MBPActivity saved_context;
		public MyRunnable (MBPActivity c )
		{
			saved_context = c;
		}

		public void run()
		{	
			if (someExtraChecks() == -1)
			{
				Dialog alert;
				String _msg = "This camera is not registered. Setup camera failed.";

				AlertDialog.Builder builder = new AlertDialog.Builder(saved_context);
				Spanned msg = Html.fromHtml("<big>" + _msg  + "</big>");
				builder.setMessage(msg)
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.OK),
						new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,
							int which) {
						dialog.dismiss();
						saved_context.finish();
					}
				});
				alert = builder.create();
				alert.show();
			}
			else
			{
//				Intent intent = new Intent(saved_context, FirstTimeActivity.class);
//				intent.putExtra(FirstTimeActivity.bool_InfraMode,true);
//				saved_context.startActivity(intent);
//				saved_context.finish();
				
				proceed(bool_InfraMode);
			}
		}
	}

	private void proceed(int action) 
	{
		Bundle extra = getIntent().getExtras();
		
		/* We may be waken up by some other intent */
		if (action == bool_InfraMode)
		{
			proceedDirectly();
			
			return;
		} 
		else if (action == bool_InfraModeInitialSetup)
		{
			proceedWithInitialSetup();

			return;
		}
		else if (extra != null)
		{
			boolean goDirectOrInfra = extra.getBoolean(bool_VoxInfraMode);
			if (goDirectOrInfra)
			{
				proceedWithVoxInfraMode();
			}
		}
	}
	
	private void proceedDirectly()
	{
		Log.i("MBPActivity", "In proceedDirectly");
		
		Intent entry = new Intent(MBPActivity.this, LoginOrRegistrationActivity.class);
		entry.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		MBPActivity.this.startActivity(entry);
		
		finish();
		overridePendingTransition(0, 0);
	}
	
	private void proceedWithInitialSetup()
	{
		Log.i("MBPActivity", "In proceedWithInitialSetup");
		
		//20131218:phung: change flow, initial just show Login screen , if user does not have any account
		//    they will need to click "Create Account"				
//		final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
//		dialog.setContentView(R.layout.bb_server_url_new_name);
//		dialog.setCancelable(true);
//
//		//setup connect button
//		Button connect = (Button) dialog.findViewById(R.id.change_btn);
//		connect.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				EditText text = (EditText) dialog.findViewById(R.id.text_new_name);
//				if (text != null)
//				{
//					String serverUrl = "https://" + text.getText().toString().trim() + "/v1";
//					if (!serverUrl.isEmpty())
//					{
//						Log.i("MBP", "New server URL: " + serverUrl);
//						PublicDefines.SERVER_URL = serverUrl;
//						SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
//						Editor editor = settings.edit();
//						editor.putString(PublicDefine.PREFS_SAVED_SERVER_URL, serverUrl);
//						editor.commit();
//					}
//				}
//				dialog.cancel();
//			}
//		});
//
//
//		Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
//		cancel.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				dialog.cancel();
//			}
//		});
//
//		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//
//			@Override
//			public void onCancel(DialogInterface dialog) 
//			{
//				//GO to first time setup 
//				Intent entry = new Intent(MBPActivity.this, LoginOrRegistrationActivity.class);
//				entry.putExtra(LoginOrRegistrationActivity.bool_createUserAccount, true); 
//				entry.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//				MBPActivity.this.startActivity(entry);
//				
//				finish();
//				overridePendingTransition(0, 0);
//			}
//		});
//		
//		dialog.show();
		
		Intent entry = new Intent(MBPActivity.this, LoginOrRegistrationActivity.class);
		entry.putExtra(LoginOrRegistrationActivity.bool_createUserAccount, true); 
		entry.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		MBPActivity.this.startActivity(entry);
		
		finish();
		overridePendingTransition(0, 0);
	}
	
	/*20120913: phung: vox here will just show the camera list 
	 *  extra string_VoxDeviceAddr is useless for now. 
	 *  
	 *  NEW: If there is no OFFLine data here we launch LoginOrRegistrationActivity instead
	 *  
	 */
	private void proceedWithVoxInfraMode()
	{
		Log.i("MBPActivity", "In proceedWithVoxInfraMode");
//		Bundle extra = getIntent().getExtras();
		
//		String voxDevice = extra.getString(string_VoxDeviceAddr);

		//Try to restore data
		try 
		{
			if (new SetupData().restore_session_data(getExternalFilesDir(null)))
			{
				Intent entry_intent = new Intent(MBPActivity.this, HomeScreenActivity.class);
				entry_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(entry_intent);
				
				finish();
				overridePendingTransition(0, 0);
			}
			else
			{
				Log.i("MBP", "Can't restore offline file  -> Login page instead");
				Intent entry = new Intent(MBPActivity.this,LoginOrRegistrationActivity.class);
				entry.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				MBPActivity.this.startActivity(entry);
				
				finish();
				overridePendingTransition(0, 0);
			}
		} 
		catch (StorageException e) 
		{
			Log.d("mbp", e.getLocalizedMessage());
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
			return;
		}
	}
	
	public static boolean isCamDetectServiceRunning(Context c) {
		ActivityManager manager = (ActivityManager) c.getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.msc3.CameraDetectorService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	private int someExtraChecks()
	{
		return 0;
	}
	// TEST 
	/*
	private static final char[] _CNAME = {0x43, 0x48, 0x49, 0x4E, 0x41 };
	private static final char[] _CCODE = {0x43, 0x4E };
	private static final String _CODE_HTTP_CHECK = 
			"http://api.ipinfodb.com/v3/ip-city/?key=d32404a074fea8426a5ebf8237e7e1603bca43ec63349c20da0e4a07bad79797";

	private int someExtraChecks()
	{
		//1.Check if ip is from a specific country

		String code = String.valueOf(_CCODE);
		String name = String.valueOf(_CNAME);

		//Log.d("mbp", "Check code "+ code + " name: " + name);

		 sample response format:
			"statusCode" : "OK",
			"statusMessage" : "",
			"ipAddress" : "74.125.45.100",
			"countryCode" : "US",
			"countryName" : "UNITED STATES",
			"regionName" : "CALIFORNIA",
			"cityName" : "MOUNTAIN VIEW",
			"zipCode" : "94043",
			"latitude" : "37.3956",
			"longitude" : "-122.076",
			"timeZone" : "-08:00"

		String http_cmd = _CODE_HTTP_CHECK;
		int countryCode_idx = 3;
		int countryName_idx = 4;


		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;
		String response = null;
		int respondeCode = -1;
		int ret = 0;
		boolean isCountryMatched = false; 


		try {
			url = new URL(http_cmd );
			conn = (HttpURLConnection)url.openConnection();

			conn.setConnectTimeout(PublicDefine.BM_SERVER_CONNECTION_TIMEOUT);
			conn.setReadTimeout(PublicDefine.BM_SERVER_READ_TIMEOUT);


			respondeCode = conn.getResponseCode();

			if (respondeCode == HttpURLConnection.HTTP_OK)
			{
				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream(),4*1024));

				response = inputStream.readLine();

				//Log.d("mbp", "Check code cam response: " + response);

				String [] tokens = response.split(";");
				if (tokens.length == 11)
				{
					if (tokens[countryCode_idx].equalsIgnoreCase(code) &&
							tokens[countryName_idx].equalsIgnoreCase(name) )
					{
						isCountryMatched= true;
					}
				}



			}


		} 
		catch (IOException e) {
			e.printStackTrace();
			ret = 0;
			isCountryMatched = false;

		}


		//2. Check if  mac-address is registered
		boolean shouldFailed = false; 
		if (isCountryMatched == true)
		{
			http_cmd =  PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART + "maccheck"+ 
					"&mac=" + "112233445566" ;
			//Log.d("mbp", "mac registerd cmd: "+http_cmd);

			HttpsURLConnection conn1 = null;

			String usr_pass =  String.format("%s:%s", "triphung@gmail.com", "qwe");
			try {
				url = new URL(http_cmd );
				conn1 = (HttpsURLConnection)url.openConnection();

				conn1.addRequestProperty("Authorization", "Basic " +
						Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn1.setConnectTimeout(PublicDefine.BM_SERVER_CONNECTION_TIMEOUT);
				conn1.setReadTimeout(PublicDefine.BM_SERVER_READ_TIMEOUT);


				respondeCode = conn.getResponseCode();
				if (respondeCode != HttpURLConnection.HTTP_OK)
				{
					shouldFailed = true; 
				}


			} 
			catch (IOException e) {
				e.printStackTrace();
				shouldFailed = true;
			}

		}



		if (shouldFailed)
		{

			ret = -1;

		}
		return ret;
	}

	 */



}  

