package com.blinkhd;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.msc3.CamChannel;
import com.msc3.PublicDefine;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;
import com.nxcomm.blinkhd.ui.customview.TemperatureMode;
import com.nxcomm.blinkhd.ui.customview.TemperatureView;

public class TemperatureFragment extends Fragment {
	
	private CamChannel selected_channel = null;
	private String http_pass = String.format("%s:%s", PublicDefine.DEFAULT_BASIC_AUTH_USR,
			PublicDefine.DEFAULT_CAM_PWD);
	
	private float temp_value_celcius = -1;
	private Activity activity = null;
	private Timer queryTempTimer = null;
	
	@Override
	public void onAttach(Activity activity)
	{
	    // TODO Auto-generated method stub
	    super.onAttach(activity);
	    this.activity = activity;
	}
	
	@Override
	public void onDetach()
	{
	    // TODO Auto-generated method stub
	    super.onDetach();
	    activity = null;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)

	{
		// TODO Auto-generated method stub

		View v = inflater.inflate(R.layout.temperature_fragment, container,
		        false);

		TemperatureView tempView = (TemperatureView) v
		        .findViewById(R.id.temperatureView1);
		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			tempView.setMainTextSize(50);
			tempView.setTextColor(getResources().getColor(R.color.white));
		}
		else
		{
			tempView.setMainTextSize(133);
			tempView.setTextColor(getResources().getColor(R.color.main_blue));
		}
		tempView.setVisibility(View.INVISIBLE);

		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		queryTempTimer = new Timer();
		queryTempTimer.schedule(new QueryTemperatureTask(), 0, 10000);
	}
	
	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
	}
	
	@Override
	public void onStart()
	{
	    // TODO Auto-generated method stub
	    super.onStart();
	}
	
	private void queryTemperature()
	{
		Runnable runn = new Runnable()
		{
			
			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if (selected_channel != null)
				{
					String res = null;
					String cmd = PublicDefine.GET_TEMP_VALUE;
					if (selected_channel.getCamProfile().isInLocal())
					{
						final String device_address_port = 
								selected_channel.getCamProfile().get_inetAddress().getHostAddress() +
								":" + selected_channel.getCamProfile().get_port();
						String http_addr = null;
						http_addr = String
								.format("%1$s%2$s%3$s%4$s", "http://",
										device_address_port,
										PublicDefine.HTTP_CMD_PART,
										cmd);
						Log.d("mbp", "get temp cmd: " + http_addr);
						res = HTTPRequestSendRecv
								.sendRequest_block_for_response(
										http_addr,
										PublicDefine.DEFAULT_BASIC_AUTH_USR,
										http_pass);
					}
					else
					{
						cmd = PublicDefine.BM_HTTP_CMD_PART + cmd;
						Log.d("mbp", "get temp cmd: " + cmd);
						SharedPreferences settings = 
								getActivity().getSharedPreferences(
										PublicDefine.PREFS_NAME, 0);
						String saved_token = settings
								.getString(
										PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
										null);
						if (saved_token != null) {
							res = UDTRequestSendRecv.sendRequest_via_stun2(
									saved_token, selected_channel
									.getCamProfile().getRegistrationId(),
									cmd);
						}
					}
					
					if (res != null && res.startsWith(PublicDefine.GET_TEMP_VALUE))
					{
						res = res.substring(PublicDefine.GET_TEMP_VALUE.length() + 2);
						if (res.equalsIgnoreCase("-1"))
						{
							res = null;
						}
						else
						{
							try
                            {
	                            temp_value_celcius = Float.valueOf(res);
                            }
                            catch (NumberFormatException e)
                            {
	                            e.printStackTrace();
                            }
							
							if (temp_value_celcius != -1)
							{
								if (activity != null)
								{
									activity.runOnUiThread(new Runnable()
									{
										
										@Override
										public void run()
										{
											// TODO Auto-generated method stub
											updateTemperature();
										}
									});
									
								}
							}
						}
						
					} //if (res != null && res.startsWith(PublicDefine.GET_TEMP_VALUE))
				} //if (selected_channel != null)
			}
		};
		Thread worker = new Thread(runn);
		worker.start();
	}
	
	private void updateTemperature()
	{
		if (activity != null)
		{
			String unit = null;
			int temp_converted = -1;
			SharedPreferences settings = activity.getSharedPreferences(
					PublicDefine.PREFS_NAME, 0);
			int saved_temp_unit = settings.getInt(PublicDefine.PREFS_TEMPERATURE_UNIT,
					PublicDefine.TEMPERATURE_UNIT_DEG_C);
			
			TemperatureView txtTemp = (TemperatureView) 
					activity.findViewById(R.id.temperatureView1);
			if (txtTemp != null)
			{
				txtTemp.setTemperature(temp_value_celcius);
				txtTemp.setVisibility(View.VISIBLE);
				if (saved_temp_unit == PublicDefine.TEMPERATURE_UNIT_DEG_C)
				{
					txtTemp.setMode(TemperatureMode.C);
				}
				else
				{
					txtTemp.setMode(TemperatureMode.F);
				}
			}
			
		}
	}
	
	private float convertToDegF(float temp_in_c)
	{
		return temp_in_c * 9f / 5f + 32;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);

		TemperatureView tempView = (TemperatureView) getView().findViewById(
		        R.id.temperatureView1);
		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			tempView.setMainTextSize(50);
			tempView.setTextColor(getResources().getColor(R.color.white));
		}
		else
		{
			tempView.setMainTextSize(133);
			tempView.setTextColor(getResources().getColor(R.color.main_blue));
		}

	}
	
	@Override
	public void onStop()
	{
	    // TODO Auto-generated method stub
	    super.onStop();
	    if (queryTempTimer != null)
	    {
	    	queryTempTimer.cancel();
	    }
	}
	
	@Override
	public void onDestroy()
	{
	    // TODO Auto-generated method stub
	    super.onDestroy();
	}
	
	private class QueryTemperatureTask extends TimerTask
	{

		@Override
        public void run()
        {
	        // TODO Auto-generated method stub
	        queryTemperature();
        }
		
	}
}
