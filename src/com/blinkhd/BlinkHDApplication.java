package com.blinkhd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;


import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;

import com.msc3.Util;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nxcomm.cached.CameraEventOpenHelper;

public class BlinkHDApplication extends Application
{

	// public static final String logFilePath = "/sdcard/hubble/logs/log.log";
	private final String	    logFileDir	      = Util.getRootRecordingDirectory()
	                                                      + File.separator
	                                                      + "logs";
	private final String	    logFileName	      = "log.log";
	private final String	    logFilePath	      = logFileDir + File.separator
	                                                      + logFileName;
	public static final boolean	sendMailWhenCrash	= true;
	
	public final static String	LINE_SEPARATOR	  = System.getProperty("line.separator");

	// private CameraEventOpenHelper timelineCache;

	@Override
	public void onCreate()
	{

		// TODO Auto-generated method stub
		super.onCreate();

		Log.i("mbp",
		        "============================================ APP CREATED ==============================================");
		// init font
		FontManager.init(getApplicationContext());
		// setTimelineCache(new CameraEventOpenHelper(getApplicationContext()));

		/*
		 * try { String name = "super-log.txt"; FileOutputStream f =
		 * openFileOutput(name, 0); f.write("Hello World".getBytes());
		 * f.close();
		 * 
		 * FileInputStream inputStream = openFileInput(name); byte[] buffer =
		 * new byte[20]; inputStream.read(buffer); inputStream.close();
		 * 
		 * Log.d("mbp","Read from file: "+ new String(buffer));
		 * 
		 * } catch (Exception e2) { // TODO Auto-generated catch block
		 * e2.printStackTrace(); }
		 */

		// init image loader
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
		        .cacheOnDisc(true).cacheInMemory(true).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
		        getApplicationContext())
		        .defaultDisplayImageOptions(defaultOptions)
		        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
		        .memoryCacheSize(2 * 1024 * 1024)
		        .discCache(new UnlimitedDiscCache(new File("hubble")))
		        .discCacheFileCount(100).writeDebugLogs().build();

		ImageLoader.getInstance().init(config);

		if (sendMailWhenCrash)
		{
			Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler()
			{

				@Override
				public void uncaughtException(Thread thread, Throwable ex)
				{
					ex.printStackTrace();
					Log.i("mbp", "Uncaught exception: " + ex.toString());
					try
					{
						final StringBuilder log = new StringBuilder();
						String logcat_cmd = "logcat -d -v time *:s mbp:V FFMpegFileExplorer:v FFMpegPlayerActivity:v "
						        + "FFMpegPlaybackActivity:v mbp.update:V System.err:V ActivityManager:I AndroidRuntime:I "
						        + "FFMpegPlayer-JNI:v FFMpegMediaPlayer-native:v FFMpeg:v ffmpeg_onLoad:v "
						        + "FFMpegMovieViewAndroid:v libffmpeg:v FFMpegMediaPlayer-java:v FFMpegAudioDecoder:v "
						        + "FFMpegVideoDecoder:v FFMpegIDecoder:v System.out:v AudioTrack:v native-Pjnath:v "
						        + "stun_client:v DEBUG:v";
						Process process;

						writeLogAndroidDeviceInfo();
						
						process = Runtime.getRuntime().exec(logcat_cmd);

						BufferedReader bufferedReader = new BufferedReader(
						        new InputStreamReader(process.getInputStream()));

						String line;
						int lineOfLog = 0;

						while ((line = bufferedReader.readLine()) != null)
						{
							log.append(line);
							log.append(LINE_SEPARATOR);
							lineOfLog++;
						}

						log.append(getDebugInfo());

					
						storeOnSD(log);

					}
					catch (IOException e1)
					{
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					Intent errorActivity = new Intent(
					        "com.blinkhd.ReportErrorActivity");
					errorActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					PendingIntent pendingIntent = PendingIntent.getActivity(
					        getApplicationContext(), 22, errorActivity, 0);
					try
					{
						pendingIntent.send();

					}
					catch (CanceledException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.exit(0);
				}
			});
		}

		boolean logToFile = false;
		Log.d("mbp", "APP created.");

		if (logToFile)
		{
			try
			{
				Log.d("mbp", "Start INTERNAL LOGCAT COLLECTOR....");
				String logcat_cmd = "logcat -f /sdcard/hubble/hubble.log -r 200";
				Process process = Runtime.getRuntime().exec(logcat_cmd);
			}
			catch (IOException e)
			{
				Log.e("mbp", "INTERNAL LOGCAT COLLECTOR start failed.", e);//$NON-NLS-1$
			}
		}

	}

	private void  writeLogAndroidDeviceInfo()
	{
		try
		{
			Class<?> c = Build.class;
			for (Field f : c.getDeclaredFields())
			{

				Class<?> t = f.getType();
				if (t == String.class)
				{
					Log.i("mbp", f.getName() + ": " + (String) f.get(null));
				}

			}

			Class<?> version = Build.VERSION.class;

			for (Field f : version.getDeclaredFields())
			{

				Class<?> t = f.getType();
				if (t == String.class)
				{
					Log.i("mbp", f.getName() + ": " + (String) f.get(null));
				}
				else if (t == Integer.class)
				{
					Log.i("mbp", f.getName() + ": " + f.getInt(null));
				}
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}

		DisplayMetrics displaymetrics = new DisplayMetrics();
		Object windowManagerObj = this.getSystemService(Context.WINDOW_SERVICE);

		if (windowManagerObj instanceof WindowManager)
		{
			WindowManager windowManager = (WindowManager) windowManagerObj;

			windowManager.getDefaultDisplay().getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;
			Log.i("mbp", "Screen DPI: " + displaymetrics.densityDpi);
			Log.i("mbp", "Screen resolution " + width + " x " + height
			        + " pixels.");
			Log.i("mbp", "Screen resolution " + width / displaymetrics.density
			        + " x " + height / displaymetrics.density + " dpi.");
		}

	}

	public String getDebugInfo()
	{
		PackageInfo pInfo;
		try
		{

			String fullInfo = "=======================DEBUG INFORMATION=======================\n";

			Runtime rt = Runtime.getRuntime();
			long maxMemory = rt.maxMemory();

			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String appInfo = pInfo.packageName + " version code "
			        + pInfo.versionCode + " version name " + pInfo.versionName;
			fullInfo += "Application information: " + appInfo + "\n";
			fullInfo += "Device name: " + getDeviceName() + "\n";
			fullInfo += "CPU Information: \n" + getCpuInfo() + "\n";
			fullInfo += "Memory Information: \n" + getMemoryInfo() + "\n";
			fullInfo += "Max heap size for me: " + (maxMemory / 1024)
			        + " Kbytes.\n";

			fullInfo += "=======================END DEBUG INFORMATION=======================\n";

			return fullInfo;

		}
		catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}

		return "";
	}

	public static String getDeviceName()
	{
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer))
		{
			return capitalize(model);
		}
		else
		{
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s)
	{
		if (s == null || s.length() == 0)
		{
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first))
		{
			return s;
		}
		else
		{
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static String getCpuInfo()
	{
		try
		{
			Process proc = Runtime.getRuntime().exec("cat /proc/cpuinfo");
			InputStream is = proc.getInputStream();

			return getStringFromInputStream(is);
		}
		catch (IOException e)
		{
			Log.e("mbp", "------ getCpuInfo " + e.getMessage());
		}

		return null;
	}

	public static String getMemoryInfo()
	{
		try
		{
			Process proc = Runtime.getRuntime().exec("cat /proc/meminfo");
			InputStream is = proc.getInputStream();
			return getStringFromInputStream(is);
		}
		catch (IOException e)
		{
			Log.e("mbp", "------ getMemoryInfo " + e.getMessage());
		}
		return null;
	}

	private static String getStringFromInputStream(InputStream is)
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = null;

		try
		{
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
				sb.append("\n");
			}
		}
		catch (IOException e)
		{
			Log.e("mbp", "------ getStringFromInputStream " + e.getMessage());
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					Log.e("mbp",
					        "------ getStringFromInputStream " + e.getMessage());
				}
			}
		}

		return sb.toString();
	}

	private void storeOnSD(StringBuilder log)
	{
		File dir = new File(logFileDir);
		if (!dir.exists())
		{
			dir.mkdirs();
		}

		/* create a data file in the external dir */
		File file = new File(logFilePath);
		if (file.exists())
		{
			Log.e("mbp", "File exist, remove it");
			/* remove the old file */
			file.delete();
		}

		try
		{
			OutputStream os = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(os);

			pw.write(log.toString());

			pw.close();
			os.close();

		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{

			if (file.exists())
			{
				/* remove the incomplete file */
				Log.e("mbp", "Remove imcompleted file.");
				file.delete();
			}
			e.printStackTrace();
		}
	}

	@Override
	public void onTerminate()
	{
		// TODO Auto-generated method stub
		super.onTerminate();
		Log.d("mbp", "APP destroy.");
	}

	

	private int	               i	= 0;
	private TestAsyncTaskInApp	task;

	public void startAsysnTask()
	{
		if (task == null)
		{

			task = new TestAsyncTaskInApp();
			task.execute();
		}

	}

	class TestAsyncTaskInApp extends AsyncTask<Void, Void, String>
	{

		@Override
		protected String doInBackground(Void... params)
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "Hello World " + i++;

		}

		@Override
		protected void onPostExecute(String result)
		{
			// TODO Auto-generated method stub
			Log.i("mbp", "Async task result: " + result);
			super.onPostExecute(result);
		}
	}

}
