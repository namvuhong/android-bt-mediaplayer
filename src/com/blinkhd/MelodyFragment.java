package com.blinkhd;

import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MelodyFragment extends Fragment {
	
	private CamChannel selected_channel = null;
	private String device_ip = null;
	private int device_port = -1;
	private String http_pass;
	private int currentMelodyIndx = -1;
	private BaseAdapter melodyAdapter = null;
	
	private Activity activity = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.melody_fragment, container, false);
	}
	
	@Override
	public void onAttach(Activity activity)
	{
	    // TODO Auto-generated method stub
	    super.onAttach(activity);
	    this.activity = activity;
	}
	
	@Override
	public void onDetach()
	{
	    // TODO Auto-generated method stub
	    super.onDetach();
	    activity = null;
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		http_pass = String.format("%s:%s", PublicDefine.DEFAULT_BASIC_AUTH_USR,
				PublicDefine.DEFAULT_CAM_PWD);
		//setupMelody();
		queryMelodyStatus();
	}

	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
		if (selected_channel != null)
		{
			device_ip = selected_channel.getCamProfile().
					get_inetAddress().getHostAddress();
			device_port = selected_channel.getCamProfile().get_port();
		}
	}
	
	private void sendMelodyCmd(final int position)
	{
		Thread worker = new Thread() 
		{
			public void run() {
				boolean send_via_udt = false;
				if (selected_channel != null) {
					if (selected_channel.getCamProfile().isInLocal() == false)
					{
						send_via_udt = true;
					}
				}

				// TODO: please change this.. very dirty way of doing
				// things

				if (send_via_udt == true)
				{
					/*int localPort = 0;
					if (bm_session_auth != null) {
						localPort = bm_session_auth.getLocalPort();
					}*/

					if (position + 1 == 0) 
					{
						String request = PublicDefine.SET_MELODY_OFF;
						request = PublicDefine.BM_HTTP_CMD_PART + request;
						Log.d("mbp", "set melody cmd: " + request);
						SharedPreferences settings = 
								getActivity().getSharedPreferences(
										PublicDefine.PREFS_NAME, 0);
						String saved_token = settings
								.getString(
										PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
										null);
						if (saved_token != null) {
							UDTRequestSendRecv.sendRequest_via_stun2(
									saved_token, selected_channel
									.getCamProfile().getRegistrationId(),
									request);
						}
					} 
					else 
					{
						String request = String.format("%1$s%2$s",
								"melody", position + 1);
						request = PublicDefine.BM_HTTP_CMD_PART + request;
						Log.d("mbp", "set melody cmd: " + request);
						SharedPreferences settings = 
								getActivity().getSharedPreferences(
										PublicDefine.PREFS_NAME, 0);
						String saved_token = settings
								.getString(
										PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
										null);
						if (saved_token != null) {
							UDTRequestSendRecv.sendRequest_via_stun2(
									saved_token, selected_channel
									.getCamProfile().getRegistrationId(),
									request);
						}

					}

				} //if (send_via_udt == true)
				else 
				{
					if (device_ip == null) {
						return;
					}

					final String device_address_port = device_ip + ":"
							+ device_port;
					String http_addr = null;
					http_addr = String
							.format("%1$s%2$s%3$s%4$d", "http://",
									device_address_port,
									"/?action=command&command=melody",
									position + 1);
					if (position + 1 == 0) 
					{
						String http_addr_1 = String.format(
								"%1$s%2$s%3$s%4$s", "http://",
								device_address_port,
								PublicDefine.HTTP_CMD_PART,
								PublicDefine.SET_MELODY_OFF);
						Log.d("mbp", "set melody cmd: " + http_addr_1);
						HTTPRequestSendRecv
						.sendRequest_block_for_response(
								http_addr_1,
								PublicDefine.DEFAULT_BASIC_AUTH_USR,
								http_pass);
					} 
					else 
					{
						Log.d("mbp", "set melody cmd: " + http_addr);
						HTTPRequestSendRecv
						.sendRequest_block_for_response(
								http_addr,
								PublicDefine.DEFAULT_BASIC_AUTH_USR,
								http_pass);
					}

				}

				if (getActivity() != null)
				{
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							//updateMelodyIcon(position);
						}

					});
				}

			}
		};

		worker.start();
	}
	
	public void clearOtherItems(int melodyIdx)
	{
		
		for (int i=0; i<melodyAdapter.getCount(); i++)
		{
			if (i != melodyIdx && status[i] == true)
			{
				status[i] = false;
				final ListView melodies = (ListView) getView().findViewById(R.id.melodyList);
				RelativeLayout itemView = (RelativeLayout) 
						melodies.getChildAt(i);
				//final Typeface regTf = Typeface.createFromAsset(activity.getAssets(), 
				//		"fonts/ProximaNova-Regular.otf");
				final ImageView ind = (ImageView) 
						itemView.findViewById(R.id.imgMelody);
				final TextView txt = (TextView) itemView.findViewById(R.id.melodyItem);
				ind.setBackgroundResource(R.drawable.camera_action_play);
				txt.setTypeface(FontManager.fontRegular);
			}
		}
	}
	
	private void queryMelodyStatus()
	{
		Thread worker = new Thread() 
		{
			public void run() {
				boolean send_via_udt = false;
				if (selected_channel != null) {
					if (selected_channel.getCamProfile().isInLocal() == false)
					{
						send_via_udt = true;
					}
				}

				// TODO: please change this.. very dirty way of doing
				// things

				String melody_response = null;
				if (send_via_udt == true)
				{
					/*int localPort = 0;
						if (bm_session_auth != null) {
							localPort = bm_session_auth.getLocalPort();
						}*/

					String request = PublicDefine.GET_MELODY_VALUE;
					request = PublicDefine.BM_HTTP_CMD_PART + request;
					Log.d("mbp", "get melody cmd: " + request);
					if (getActivity() != null)
					{
						SharedPreferences settings = 
								getActivity().getSharedPreferences(
										PublicDefine.PREFS_NAME, 0);
						String saved_token = settings
								.getString(
										PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
										null);
						if (saved_token != null) {
							melody_response = UDTRequestSendRecv.sendRequest_via_stun2(
									saved_token, selected_channel
									.getCamProfile().getRegistrationId(),
									request);
						}
					}

				} //if (send_via_udt == true)
				else 
				{
					if (device_ip == null) {
						return;
					}

					final String device_address_port = device_ip + ":"
							+ device_port;
					String http_addr = null;
					http_addr = String
							.format("%1$s%2$s%3$s%4$s", "http://",
									device_address_port,
									"/?action=command&command=",
									PublicDefine.GET_MELODY_VALUE);
					Log.d("mbp", "get melody cmd: " + http_addr);
					melody_response = HTTPRequestSendRecv
					.sendRequest_block_for_response(
							http_addr,
							PublicDefine.DEFAULT_BASIC_AUTH_USR,
							http_pass);

				}

				Log.d("mbp", "get melody res: " + melody_response);
				if (melody_response != null &&
						melody_response.startsWith(PublicDefine.GET_MELODY_VALUE))
				{
					melody_response = melody_response.substring(
							PublicDefine.GET_MELODY_VALUE.length() + 2);
					try
                    {
	                    currentMelodyIndx = Integer.parseInt(melody_response);
	                    if (currentMelodyIndx >= 0 && currentMelodyIndx <= 6)
	                    {
	                    	currentMelodyIndx--;
	                    }
	                    
                    }
                    catch (NumberFormatException e)
                    {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
                    }
				}
				
				if (activity != null)
                {
                	activity.runOnUiThread(new Runnable()
					{
						
						@Override
						public void run()
						{
							// TODO Auto-generated method stub
							setupMelody();
						}
					});
                }
			}
		};

		worker.start();
	}
	
	
	private boolean[] status;
	private void setupMelody() 
	{
		// TODO Auto-generated method stub
		final ListView melodies = (ListView) getView().findViewById(R.id.melodyList);
		
		if ( (getResources().getConfiguration().orientation & 
				Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			melodies.setBackgroundResource(R.drawable.background);
			melodies.getBackground().setAlpha((int) (0.6 * 255)); //60% transparency
		}
		
		if (melodies == null) {
			return;
		}

		melodies.setSelection(currentMelodyIndx);
		melodyAdapter = new BaseAdapter() {

			private final String[] melodyItems = getMelodyItems();

			private String[] getMelodyItems()
			{
				String[] items = getResources().getStringArray(
						R.array.CameraMenuActivity_melody_items_2);

				if (selected_channel.getCamProfile().getModelId().equalsIgnoreCase(
						CamProfile.MODEL_ID_MBP36N))
				{
					items = getResources().getStringArray(
							R.array.CameraMenuActivity_melody_items_3);
				}

				if (items != null)
				{
					status = new boolean[items.length];
					for (int i=0; i<items.length; i++)
					{
						status[i] = false;
					}
					
					if (currentMelodyIndx >= 0 && currentMelodyIndx < items.length)
					{
						status[currentMelodyIndx] = true;
					}
				}
				
				return items;
			}

			
			
			public View getView(int position, View convertView, ViewGroup parent) {
				RelativeLayout itemView;
				if (convertView == null) { // if it's not recycled, initialize
					// some
					// attributes
					LayoutInflater inflater = (LayoutInflater) 
							getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					itemView = (RelativeLayout) inflater.inflate(
							R.layout.bb_melody_list_item, null);

				} else {
					itemView = (RelativeLayout) convertView;

				}
				
				final int melodyIdx = position;
				final ImageView ind = (ImageView) itemView.findViewById(R.id.imgMelody);
				final TextView txt = (TextView) itemView.findViewById(R.id.melodyItem);
				txt.setText((String) getItem(position));
				txt.setTypeface(FontManager.fontRegular);
				if (ind != null) {
					if (position == currentMelodyIndx) {
						ind.setBackgroundResource(R.drawable.camera_action_pause);
					} else {
						ind.setBackgroundResource(R.drawable.camera_action_play);
					}
					
					ind.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							clearOtherItems(melodyIdx);
							if (status[melodyIdx] == false)
							{
								txt.setTypeface(FontManager.fontBold);
								status[melodyIdx] = true;
								sendMelodyCmd(melodyIdx);
								ind.setBackgroundResource(R.drawable.camera_action_pause);
							}
							else
							{
								txt.setTypeface(FontManager.fontRegular);
								status[melodyIdx] = false;
								sendMelodyCmd(-1);
								ind.setBackgroundResource(R.drawable.camera_action_play);
							}

							melodies.invalidate();
						}
					});
				}

				if (position == currentMelodyIndx) {
					txt.setTypeface(FontManager.fontBold);
				} else {
					txt.setTypeface(FontManager.fontRegular);
				}

				return itemView;
			}

			public long getItemId(int position) {
				return position;
			}

			public Object getItem(int position) {
				return melodyItems[position];
			}

			public int getCount() {
				return melodyItems.length;
			}

		};

		melodies.setAdapter(melodyAdapter);
		melodies.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				clearOtherItems(position);
				final ListView melodies = (ListView) getView().findViewById(R.id.melodyList);
				
				RelativeLayout itemView = (RelativeLayout) 
						melodies.getChildAt(position);
				final TextView txt = (TextView) itemView.findViewById(R.id.melodyItem);
				final ImageView ind = (ImageView) 
						itemView.findViewById(R.id.imgMelody);
				if (status[position] == false)
				{
					txt.setTypeface(FontManager.fontBold);
					status[position] = true;
					sendMelodyCmd(position);
					ind.setBackgroundResource(R.drawable.camera_action_pause);
				}
				else
				{
					txt.setTypeface(FontManager.fontRegular);
					status[position] = false;
					sendMelodyCmd(-1);
					ind.setBackgroundResource(R.drawable.camera_action_play);
				}

				melodies.invalidate();
			}

		});
		
		return;
	}
}
