package com.blinkhd;

import android.content.Context;
import android.graphics.Typeface;

public class FontManager
{

	public static Typeface	fontBlack, fontBold, fontBoldIt, fontExtraBold,
	        fontLight, fontLightIt, fontRegular, fontRegularItalic,
	        fontSemiBold, fontSemiBoldItalic;

	public static void init(Context ctx)
	{
		// Init all fonts:
		fontBlack = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Black.otf");
		fontBold = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Bold.otf");
		fontBoldIt = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-BoldIt.otf");
		fontExtraBold = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Extrabold.otf");
		fontLight = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Light.otf");
		fontLightIt = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-LightItalic.otf");
		fontRegular = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Regular.otf");
		fontRegularItalic = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-RegularItalic.otf");
		fontSemiBold = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-Semibold.otf");
		fontSemiBoldItalic = Typeface.createFromAsset(ctx.getAssets(),
		        "fonts/ProximaNova-SemiboldItalic.otf");

		// completed init all fonts
	}
}
