package com.blinkhd;

import java.io.File;

import com.msc3.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class ReportErrorActivity extends Activity
{
	private final String logFilePath = 
			Util.getRootRecordingDirectory() + File.separator + "logs" + 
					File.separator + "log.log";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_error_activity);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Add the buttons
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{

				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[] {
				        "androidcrashreport@cvisionhk.com" });
				// intent.putExtra(Intent.EXTRA_CC, new String[] {
				// "triphung@nxcomm.com", "son.nguyen@nxcomm.com" });
				intent.putExtra(Intent.EXTRA_SUBJECT,
						"Hubble Home Android App Report");
				intent.putExtra(Intent.EXTRA_TEXT,
						"[Put your description here]");

				Uri uri = Uri
				        .fromFile(new File(logFilePath));

				intent.putExtra(Intent.EXTRA_STREAM, uri);
				startActivity(Intent.createChooser(intent, "Send email..."));

				System.exit(0);
			}
		});
		builder.setNegativeButton("No", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				System.exit(0);
			}
		});
		// Set other dialog properties
		builder.setMessage("Hubble Home crashed. Do you want send crash report to developer?");
		// Create the AlertDialog
		AlertDialog dialog = builder.create();
		dialog.show();
		dialog.setCanceledOnTouchOutside(false);

	}

}
