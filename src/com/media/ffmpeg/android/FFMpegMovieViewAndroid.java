package com.media.ffmpeg.android;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.Toast;

import com.blinkhd.R;
import com.media.ffmpeg.FFMpegPlayer;
import com.msc3.FadeOutAnimationAndGoneListener;
import com.msc3.Streamer;
import com.msc3.Util;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;
import cz.havlena.ffmpeg.ui.IPlaylistUpdater;

public class FFMpegMovieViewAndroid extends SurfaceView implements VideoControllerView.MediaPlayerControl {
	private static final String 	TAG = "FFMpegMovieViewAndroid"; 

	private FFMpegPlayer			mPlayer;
	private MediaController			mMediaController;
	private VideoControllerView controller;

	private Context mContext;
	private String filePath;
	private String snapPath = null;
	private Thread initializing_thrd = null;
	private Handler mHandler;
	private boolean inPlayBackMode = false; 
	private boolean isInitilizing = false;
	private boolean shouldInterrupt = false;
	private Timer fadeOutTimer = null;

	public FFMpegMovieViewAndroid(Context context) {
		super(context);
		mContext = context;
		SurfaceHolder surfHolder = getHolder();
		surfHolder.addCallback(mSHCallback);
	}

	public FFMpegMovieViewAndroid(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		SurfaceHolder surfHolder = getHolder();
		surfHolder.addCallback(mSHCallback);
	}

	public FFMpegMovieViewAndroid(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		SurfaceHolder surfHolder = getHolder();
		surfHolder.addCallback(mSHCallback);
	}

	public void setSnapshotPath(String path)
	{
		snapPath = path;
	}

	public void getSnapShot(int snap_width, int snap_height, boolean shouldShowDialog)
	{
		if (mPlayer != null)
		{
			byte[] picture = null;
			picture = mPlayer.native_getSnapShot();
			Bitmap bitmap = Bitmap.createBitmap(
					snap_width, snap_height, Bitmap.Config.RGB_565);
			ByteBuffer buffer = ByteBuffer.wrap(picture);
			bitmap.copyPixelsFromBuffer(buffer);

			final String fileName = Util.getSnapshotFileName();
			BufferedOutputStream bos = null;

			try {
				if (shouldShowDialog == true)
				{
					bos = new BufferedOutputStream(new FileOutputStream(
							fileName));
				}
				else
				{
					Log.d("mbp", "snappath: " + snapPath);
					bos = new BufferedOutputStream(new FileOutputStream(
							snapPath));
				}
				bitmap.compress(CompressFormat.JPEG, 100, bos);
				bos.flush();
				bos.close();

			} catch (IOException e) {
				// todo: error handling
			}

			if (shouldShowDialog)
			{
//				Spanned msg = Html.fromHtml("<big>"
//						+ getResources().getString(
//								R.string.EntryActivity_rec_2)
//								+ Util.getShortFileName(fileName) + "</big>");
//				AlertDialog snapshotFileNameDialog = new AlertDialog.Builder(
//						mContext)
//				.setMessage(msg)
//				.setNeutralButton(android.R.string.ok, null)
//				.create();
//
//				snapshotFileNameDialog.show();
				
				String text = mContext.getResources().getString(R.string.saved_photo);
				int duration = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(mContext, text, duration);
				toast.setGravity(Gravity.BOTTOM, 0, 0);
				toast.show();
			}
		}
	}

	public  void initVideoView(Handler mHandler, boolean forPlayBack, boolean forSharedCam) {
		mPlayer = new FFMpegPlayer(mHandler, forPlayBack, forSharedCam);
		inPlayBackMode = forPlayBack; 
		this.mHandler = mHandler;
	}


	public IPlaylistUpdater getFFMpegPlayer()
	{
		return mPlayer;
	}


	public void setFFMpegPlayerOptions(int mode)
	{
		mPlayer.setPlayOption(mode); 
	}

	private void attachMediaController() {

		controller = new VideoControllerView(mContext);


		controller.setMediaPlayer(this);
		View anchorView = this.getParent() instanceof View ?
				(View)this.getParent() : this;
				controller.setAnchorView((ViewGroup) anchorView);
				
	}


	/**
	 * @param filePath
	 */
	public void setVideoPath(String filePath) 
	{
		Log.d(TAG, "Set video path: " + filePath);
		this.filePath = filePath;

		Thread worker = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				int retries = 3;
				do
				{
					//Moves here from onSurfaceCreated 
					if (getHolder() != null && getHolder().getSurface().isValid() && isShown())
					{
						initializeVideo(getHolder());
						break;
					}
					else
					{
						Log.d(TAG, "Surface is not valid yet, waiting...");
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				while (--retries > 0);
			}
		});
		worker.start();

	}


	/**
	 * initialize player
	 * 
	 * prerequisite: filePath, mPlayer, SurfaceHolder 
	 */
	private void initializeVideo(SurfaceHolder surfHolder) 
	{

		try {

			//if player is releasing, not need to initialize video anymore
			if (mPlayer != null && shouldInterrupt == false)
			{
				//ORDER is important -- Set display before prepare()!!!
				mPlayer.setDisplay(surfHolder);
				initializing_thrd = new Thread(new Runnable() {

					@Override
					public void run() {

						boolean hasInitialized = false;
						int retries = 3;

						while (retries > 0 && !shouldInterrupt)
						{
							synchronized (FFMpegMovieViewAndroid.this)
							{
								if (mPlayer == null)
								{
									return;
								}

								isInitilizing = true;
								try {

									mPlayer.setDataSource(filePath);
									mPlayer.prepare();

									isInitilizing = false;
									hasInitialized = true;
									break;
								} catch (IllegalStateException e) {
									Log.e(TAG, "Couldn't prepare player: " + e.getMessage());
								} catch (IOException e) {
									Log.e(TAG, "IO Exception Couldn't prepare player: " + e.getMessage());
								}
								isInitilizing = false;

								try {
									Thread.sleep(3000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}

								Log.e(TAG, "Initializing player failed...Retries: " + retries);
								retries--;
							}
						}

						if (!shouldInterrupt)
						{
							if (hasInitialized)
							{
								//Initialize successfully
								startVideo();
							}
							else
							{
								mPlayer = null;
								//Initialize failed
								failedToStartVideo();
							}
						}
						else
						{
							shouldInterrupt = false;
						}
					}

				});
				initializing_thrd.start();
			}

		} catch (IllegalStateException e) {
			Log.e(TAG, "Couldn't prepare player: " + e.getMessage());

		} catch (IOException e) {
			Log.e(TAG, "IO Exception Couldn't prepare player: " + e.getMessage());
		}
	}

	private void startVideo()
	{
		if (inPlayBackMode == true)
		{
			((Activity)mContext).runOnUiThread(new Runnable() {

				@Override
				public void run() {
					attachMediaController();
				}
			});
		}

		//could be stopping now.. 

		synchronized (this) 
		{
			mPlayer.start();
		}

	}

	private void failedToStartVideo()
	{
		mHandler.dispatchMessage(Message.obtain(mHandler, Streamer.MSG_CAMERA_IS_NOT_AVAILABLE));
	}

	public boolean isReleasingPlayer()
	{
		return shouldInterrupt;
	}

	public void release() 
	{

		if ( mPlayer != null ) // && mPlayer.isPlaying())
		{
			Log.d(TAG, "releasing ...");
			shouldInterrupt = true;
			mPlayer.suspend();
			synchronized (this) 
			{

				try {
					//					if (inPlayBackMode == false && mPlayer.getVideoWidth() != 0 &&
					//							mPlayer.getVideoHeight() != 0)
					//					{
					//						getSnapShot(mPlayer.getVideoWidth(), mPlayer.getVideoHeight(), false);
					//					}
					mPlayer.stop();
				} catch (IllegalStateException e) {
					e.printStackTrace();
				}

				//wait for setDataSource finished, avoid crash
				while (isInitilizing)
				{
					Log.d(TAG,"waiting for release......");
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
				}

				mPlayer.release();
				mPlayer = null;

			}
			shouldInterrupt = false;

			Log.d(TAG, "player released");
		}
	}
	
	public boolean onTouchEvent(android.view.MotionEvent event) {
//		if(mMediaController != null && !mMediaController.isShowing()) {
//			mMediaController.show(3000);
//		}
		
		if (inPlayBackMode == true)
		{
			final ImageView imgFullClose = (ImageView) ((Activity)mContext)
					.findViewById(R.id.imgCloseFull);
			final LinearLayout layoutOpt = (LinearLayout) ((Activity)mContext)
					.findViewById(R.id.layoutOption);
			final TextView txtDone = (TextView) ((Activity)mContext)
					.findViewById(R.id.txtPlaybackDone);
			final int orientation = mContext.getResources().getConfiguration()
					.orientation;
			
			if (controller != null && !controller.isShowing())
			{
				controller.show(3000);
				if (orientation == Configuration.ORIENTATION_LANDSCAPE)
				{
					imgFullClose.setVisibility(View.VISIBLE);
				}
				else
				{
					txtDone.setVisibility(View.VISIBLE);
				}
				layoutOpt.setVisibility(View.VISIBLE);
				
				fadeOutTimer = new Timer();
				fadeOutTimer.schedule(new TimerTask()
				{
					
					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						post(new Runnable()
						{
							
							@Override
							public void run()
							{
								// TODO Auto-generated method stub
								if (orientation == Configuration.ORIENTATION_LANDSCAPE)
								{
									imgFullClose.setVisibility(View.INVISIBLE);
								}
								else
								{
									txtDone.setVisibility(View.INVISIBLE);
								}
								layoutOpt.setVisibility(View.INVISIBLE);
							}
						});
						
					}
				}, 3000);
			}
			else
			{
				if (fadeOutTimer != null)
				{
					fadeOutTimer.cancel();
					fadeOutTimer = null;
				}
				controller.hide();
				if (orientation == Configuration.ORIENTATION_LANDSCAPE)
				{
					imgFullClose.setVisibility(View.INVISIBLE);
				}
				else
				{
					txtDone.setVisibility(View.INVISIBLE);
				}
				layoutOpt.setVisibility(View.INVISIBLE);
			}

		}
		
		return false;
	}

	SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback() {
		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			Log.d(TAG, "Surface changed ...f:" + format + " w:" + w + " h:" + h );
		}

		public void surfaceCreated(SurfaceHolder holder) {
			Log.d(TAG, "Surface created...");
		}

		public void surfaceDestroyed(SurfaceHolder holder)
		{
			Log.d(TAG, "Surface destroyed...");
			
			try {
				release();
			} catch (Exception e1) {
			}

			boolean retry = true;
			if (initializing_thrd != null && initializing_thrd.isAlive())
			{
				shouldInterrupt = true;
				while (retry)
				{
					try {
						initializing_thrd.join(2000);
						retry = false;
					} catch (InterruptedException e) {
					}
				}
				initializing_thrd = null;
			}

			if(mMediaController!= null && mMediaController.isShowing()) 
			{
				mMediaController.hide();
			}
			
			((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		}
	};



	MediaPlayerControl mMediaPlayerControl = new MediaPlayerControl() {

		public void start() {
			mPlayer.resume();
		}

		public void seekTo(int pos) {
			//Log.d(TAG, "want seek to");
		}

		public void pause() {
			mPlayer.pause();
		}

		public boolean isPlaying() {
			return mPlayer.isPlaying();
		}

		public int getDuration() {
			return mPlayer.getDuration();
		}

		public int getCurrentPosition() {
			return mPlayer.getCurrentPosition();
		}

		public int getBufferPercentage() {
			//Log.d(TAG, "want buffer percentage");
			return 0;
		}

		@Override
		public boolean canPause() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean canSeekBackward() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean canSeekForward() {
			// TODO Auto-generated method stub
			return false;
		}
	};







	@Override
	public void start() {
		mPlayer.resume();
	}

	@Override
	public void pause() {
		mPlayer.pause(); 
	}

	@Override
	public int getDuration() {
		int result;
		if (mPlayer != null)
		{
			result = mPlayer.getDuration();
		}
		else
		{
			result = 0;
		}
		return result;
	}

	@Override
	public int getCurrentPosition() {
		int result;
		if (mPlayer != null)
		{
			result = mPlayer.getCurrentPosition();
		}
		else
		{
			result = 0;
		}
		return result;
	}

	@Override
	public void seekTo(int pos) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isPlaying() {
		boolean result;
		if (mPlayer != null)
		{
			result = mPlayer.isPlaying();
		}
		else
		{
			result = false;
		}
		return result;
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public boolean canPause() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean canSeekBackward() {
		return false;
	}

	@Override
	public boolean canSeekForward() {
		return false;
	}

	@Override
	public boolean isFullScreen() {

		return  (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE); 

	}

	@Override
	public void toggleFullScreen() {

		Log.d("mbp", "Toggle Fullscreen"); 

		if (isFullScreen())
		{
			((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		else
		{
			((Activity) mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
	}

	/**
	 * Start/Stop recording 
	 * 
	 * if recording is already started - dont start again 
	 *      
	 * The recording will stop automatically if the MovieView is destroyed / hidenn/ remove from view
	 * 
	 * @param isEnable - true _Start recording
	 *                 - false stop recording
	 *                 
	 */
	public void startRecord(boolean isEnable)
	{


		if (mPlayer != null)
		{
			if (isEnable)
			{
				mPlayer.startRecording(Util.getRecordFileName() );
			}
			else
			{
				mPlayer.stopRecord();
			}
		}

	}

	public boolean isRecording()
	{
		if (mPlayer != null)
		{
			return mPlayer.isRecording();
		}

		return false;
	}
}
