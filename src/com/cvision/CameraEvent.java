package com.cvision;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public class CameraEvent //implements Comparator<CameraEvent>
{
	private String id;
	private String time_code; 
	private String description;
	private String time_zone; 
	private String snaps_url; 
	private String clip_url;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTime_code() {
		return time_code;
	}
	public void setTime_code(String time_code) {
		this.time_code = time_code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getSnaps_url() {
		return snaps_url;
	}
	public void setSnaps_url(String snaps_url) {
		this.snaps_url = snaps_url;
	}
	public String getClip_url() {
		return clip_url;
	}
	public void setClip_url(String clip_url) {
		this.clip_url = clip_url;
	}

	public String toString()
	{
		return id + ":" + time_code + ":" + description ; 
	}

	private static Calendar clearTimes(Calendar c) {
	    c.set(Calendar.HOUR_OF_DAY,0);
	    c.set(Calendar.MINUTE,0);
	    c.set(Calendar.SECOND,0);
	    c.set(Calendar.MILLISECOND,0);
	    return c;
	}

	public static String convertSimpleDayFormat(Date val) 
	{
	    Calendar today=Calendar.getInstance();
	    today=clearTimes(today);

	    Calendar yesterday=Calendar.getInstance();
	    yesterday.add(Calendar.DAY_OF_YEAR,-1);
	    yesterday=clearTimes(yesterday);

	    Calendar last7days=Calendar.getInstance();
	    last7days.add(Calendar.DAY_OF_YEAR,-7);
	    last7days=clearTimes(last7days);

	    Calendar last30days=Calendar.getInstance();
	    last30days.add(Calendar.DAY_OF_YEAR,-30);
	    last30days=clearTimes(last30days);


	    
	    if(val.after(today.getTime()))
	    {
	            return "Today";  
	    }
	    else if(val.after(yesterday.getTime()))
	    {
	        return "Yesterday";
	    } 
	    else if(val.after(last7days.getTime()))
	    {
	        return "Last 7 days";
	    } 
	    else if(val.after(last30days.getTime()))
	    {
	        return "Last 30 days";
	    }
	    else
	    {
	        return "More Than 30 days";
	    }
	}

	//lhs is before rhs -> 1
	public static int compare(CameraEvent lhs, CameraEvent rhs)
	{
		if (lhs.time_code.equalsIgnoreCase(rhs.time_code))
		{
			return 0; 
		}
		else 
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			format.setTimeZone(TimeZone.getTimeZone("GMT+0"));
			Date lhs_dateTime, rhs_dateTime;
			try {
				lhs_dateTime = format.parse(lhs.time_code);
				
				rhs_dateTime = format.parse(rhs.time_code);
				
				
				
				return (lhs_dateTime.before(rhs_dateTime))? 1: -1;
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				
				return -1000; 
			}
			 
		}
		
	}
}
