package com.cvision.eventtimeliner;

import android.app.Activity;
import android.os.Bundle;

import com.blinkhd.R;


public class EventManagerFragmentActivity extends Activity {


	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_activity_event_manager);

	}

	protected void onStart()
	{
		super.onStart();
	}


	protected void onStop()
	{
		super.onStop(); 
		finish(); 
	}

	protected void onDestroy()
	{
		super.onDestroy(); 
	}

}
