package com.cvision.eventtimeliner;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class CvnDialogFragment extends DialogFragment 
{
	int mNum;
	private int numButtons;

	private CvnDialogFragmentCb cb; 

	public static CvnDialogFragment newInstance(int title) 
	{
		CvnDialogFragment frag = new CvnDialogFragment();
		Bundle args = new Bundle();
		args.putInt("title", title);
		frag.setArguments(args);
		frag.setNumButton(2);
		return frag;
	}
	
	public static CvnDialogFragment newInstance(int title, int numButn)
	{
		CvnDialogFragment frag = new CvnDialogFragment();
		Bundle args = new Bundle();
		args.putInt("title", title);
		frag.setArguments(args);
		frag.setNumButton(numButn);
		return frag;
	}
	
	private void setNumButton(int num)
	{
		numButtons = num;
	}
	
	private int getNumButton()
	{
		return numButtons;
	}
	
	public void setOnClickListener(CvnDialogFragmentCb _cb)
	{
		cb = _cb; 
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int title = getArguments().getInt("title");

		AlertDialog.Builder builder = null;
		if (getNumButton() == 2)
		{
			builder = new AlertDialog.Builder(getActivity())
			//.setIcon(R.drawable.alert_dialog_icon)
			.setTitle(title)
			.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (cb != null)
						cb.doPositiveClick(CvnDialogFragment.this);
				}
			}
					)
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							if (cb != null)
								cb.doNegativeClick(CvnDialogFragment.this);
						}
					}
							);
		}
		else if (getNumButton() == 1)
		{
			builder = new AlertDialog.Builder(getActivity())
			//.setIcon(R.drawable.alert_dialog_icon)
			.setTitle(title)
			.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if (cb != null)
						cb.doPositiveClick(CvnDialogFragment.this);
				}
			}
					);
		}
		
		return builder.create();
	}
}



