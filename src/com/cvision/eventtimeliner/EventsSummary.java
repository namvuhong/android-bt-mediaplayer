package com.cvision.eventtimeliner;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.GetTimelineEventsResponse;
import com.nxcomm.meapi.device.TimelineEvent;

public class EventsSummary {
	
	private Context mContext;
	private ArrayList<TimelineEvent> eventsList = new ArrayList<TimelineEvent>();
	
	private String timelineStatus = null;
	private String saved_token;
	private String regId;
	private String camName;
	private long currentTime;
	private long past20minutes;
	private int page = 0;
	private String before_start_time = null;
	private boolean shouldContinueQuery = true;
	private boolean isAnalyzing = false;
	private IEventSummaryCallback updater;
	
	private int vox = 0;
	private int motion = 0;
	
	public EventsSummary(Context context, String saved_token, String regId, String camName, 
			IEventSummaryCallback cb) {
		// TODO Auto-generated constructor stub
		mContext = context;
		this.saved_token = saved_token;
		this.regId = regId;
		this.camName = camName;
		updater = cb;
	}
	
	public void analyze()
	{
		isAnalyzing = true;
		currentTime = System.currentTimeMillis();
		past20minutes = currentTime - 20 * 60 * 1000;
		Date dateTime = new Date(currentTime);
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		destFormat.setTimeZone(tz);
		before_start_time = destFormat.format(dateTime);
		doQueryTask();
		
	}
	
	public boolean isAnalyzing()
	{
		return isAnalyzing;
	}
	
	private void doQueryTask()
	{
		page++;
		//Log.d("mbp", "doQueryTask: page: " + page);
		QueryEventsTask queryTask = new QueryEventsTask();
		queryTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 
				saved_token, regId, before_start_time, String.valueOf(page));
	}
	
	private void doAnalyzeTask()
	{
		//Log.d("mbp", "doAnalyzeTask: motion: "+ motion + ", vox: " +vox);
		if (motion == 0 && vox == 0)
		{
			timelineStatus = mContext.getResources().getString(
					R.string.all_is_calm);
		}
		else if (vox < 4)
		{
			if (motion == 0)
			{
				if (vox == 1)
				{
					timelineStatus = mContext.getResources().getString(
							R.string.there_has_been_a_little_noise);
				}
				else // 2 <= vox <= 3
				{
					timelineStatus = mContext.getResources().getString(
							R.string.there_has_been_some_noise);
				}
			}
			else if (motion == 1)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_little_movement);
			}
			else if (motion == 2 || motion == 3)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_some_movement);
			}
			else if (motion >= 4)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_lot_of_movement);
			}
		}
		else if (vox >= 4)
		{
			if (motion == 0)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_lot_of_noise);
			}
			else if (motion == 1)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_lot_of_noise_and_little_movement);
			}
			else if (motion == 2 || motion == 3)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_lot_of_noise_and_some_movement);
			}
			else if (motion >= 4)
			{
				timelineStatus = mContext.getResources().getString(
						R.string.there_has_been_a_lot_of_noise_movement);
			}
		}
				
		isAnalyzing = false;
		timelineStatus = PublicDefine.capitalizeFirstCharacter(timelineStatus) + 
				" at " + camName;
		updater.updateAnalyzeResult(timelineStatus);
	}
	
	public String getAnalyzeResult()
	{
		return timelineStatus;
	}
	
	private void updateEventsList(TimelineEvent[] events)
	{
		if (events != null && events.length > 0)
		{
			long eventTimestamp = 0;
			for (TimelineEvent event : events)
			{
				if (event != null && event.getTime_stamp() != null)
				{
					eventTimestamp = event.getTime_stamp().getTime();
					if (eventTimestamp < past20minutes)
					{
						shouldContinueQuery = false;
						break;
					}
					else
					{
						if (event.getAlert() == 4)
						{
							motion++;
						}
						else if (event.getAlert() == 1)
						{
							vox++;
						}
						eventsList.add(event);
					}
					
				}
				
			} //end for (TimelineEvent event : events)
			
		} //if (events != null && events.length > 0)
		else
		{
			shouldContinueQuery = false;
		}
		
		if (shouldContinueQuery == false)
		{
			doAnalyzeTask();
		}
		else
		{
			doQueryTask();
		}
	}
	
	private class QueryEventsTask extends AsyncTask<String, Void, TimelineEvent[]>
	{
		private String saved_token;
		private String regId;
		private String before_start_time;
		private String event_code;
		private String alerts;
		private int page;
		private int offset;
		private int size;
		
		
		
		@Override
		protected TimelineEvent[] doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			saved_token = params[0];
			regId = params[1];
			before_start_time = params[2];
			before_start_time = Uri.encode(before_start_time);
			event_code = null;
			alerts = null;
			page = Integer.parseInt(params[3]);
			size = 30;
			
			TimelineEvent[] timelineEvents = null;
			try {
				GetTimelineEventsResponse eventRes = 
						Device.getTimelineEvents(saved_token, regId, before_start_time, 
								event_code, alerts, page, size);
				if (eventRes != null && 
						eventRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					timelineEvents = eventRes.getEvents();
				}

			} catch (SocketTimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return timelineEvents;
		}
		
		@Override
		protected void onPostExecute(TimelineEvent[] result) {
			// TODO Auto-generated method stub
			updateEventsList(result);
		}
		
	}
	
}
