package com.cvision.eventtimeliner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.TimeZone;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.cvision.CameraEvent;
import com.msc3.PublicDefine;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nxcomm.meapi.device.TimelineEvent;

public class CameraEventAdapter extends BaseAdapter
{

	private Context	                  mContext;
	private int	                      row;
	private LinkedList<TimelineEvent>	mEvents;
	private int[]	                  numOfRowInSecion;
	private DisplayImageOptions	      options;
	private TimelineEvent	          objBean;
	private AnimationDrawable	      anim	= null;
	private int	                      clockMode;
	private Typeface	              lightTf;
	private Typeface	              regTf;

	public CameraEventAdapter(Context c, int resource, TimelineEvent[] res)
	{
		mContext = c;
		lightTf = FontManager.fontLight;// Typeface.createFromAsset(mContext.getAssets(),
		// "fonts/ProximaNova-Light.otf");
		regTf = FontManager.fontRegular;// Typeface.createFromAsset(mContext.getAssets(),
		// "fonts/ProximaNova-Regular.otf");
		row = resource;
		mEvents = new LinkedList<TimelineEvent>();

		if (res != null)
		{
			for (TimelineEvent te : res)
			{
				mEvents.addLast(te);
			}
			notifyDataSetChanged();
		}

		SharedPreferences settings = mContext.getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		clockMode = settings.getInt(PublicDefine.PREFS_CLOCK_MODE,
		        PublicDefine.CLOCK_MODE_12H);
	}

	public void insertNewEvents(TimelineEvent[] res)
	{
		if (mEvents.size() == 0)
		{
			for (TimelineEvent te : res)
			{
				mEvents.addLast(te);
			}
			notifyDataSetChanged();
			return;
		}

		TimelineEvent te;
		int length = res.length;
		te = res[length - 1];

		// this is the oldest event in the list
		if (te != null)
		{
			// if //ce isNewer than the first event - prepend it
			if (compare(te, mEvents.getFirst()) > 0)
			{
				prependNewerEvent(res);

			}
			else
			{
				Log.d("mbp", "Don't insert up front");
			}
		}

		te = res[0];
		// this is the oldest event in the list
		if (te != null)
		{
			// if //ce is Older than the last event - append it
			if (compare(te, mEvents.getLast()) < 0)
			{
				appendOlderEvent(res);
			}
			else
			{
				Log.d("mbp", "Don't insert at the back");
			}
		}

	}

	public static int compare(TimelineEvent lhs, TimelineEvent rhs)
	{
		return lhs.getTime_stamp().compareTo(rhs.getTime_stamp());
	}

	public static String convertSimpleDayFormat(Date val)
	{
		Calendar today = Calendar.getInstance();
		today = clearTimes(today);

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DAY_OF_YEAR, -1);
		yesterday = clearTimes(yesterday);

		Calendar last7days = Calendar.getInstance();
		last7days.add(Calendar.DAY_OF_YEAR, -7);
		last7days = clearTimes(last7days);

		Calendar last30days = Calendar.getInstance();
		last30days.add(Calendar.DAY_OF_YEAR, -30);
		last30days = clearTimes(last30days);

		if (val.after(today.getTime()))
		{
			return "Today";
		}
		else if (val.after(yesterday.getTime()))
		{
			return "Yesterday";
		}
		else if (val.after(last7days.getTime()))
		{
			return "Last 7 days";
		}
		else if (val.after(last30days.getTime()))
		{
			return "Last 30 days";
		}
		else
		{
			return "More Than 30 days";
		}
	}

	private static Calendar clearTimes(Calendar c)
	{
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	/* Used to update the list */
	public void prependNewerEvent(TimelineEvent[] res)
	{
		TimelineEvent te = null;
		for (int i = res.length - 1; i >= 0; i--)
		{
			te = (TimelineEvent) res[i];
			mEvents.addFirst(te);
		}
		notifyDataSetChanged();
	}

	public void appendOlderEvent(TimelineEvent[] res)
	{
		// priority queue.. order is maintained - just add them in
		for (TimelineEvent ce : res)
		{
			mEvents.addLast(ce);
		}
		notifyDataSetChanged();
	}

	private boolean didEventHappenBeyondYesterday(CameraEvent ce)
	{
		return false;
	}

	private boolean didEventHappenYesterday(TimelineEvent te)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date_time;

		date_time = te.getTime_stamp();
		if (convertSimpleDayFormat(date_time).equalsIgnoreCase("Yesterday"))
		{
			return true;
		}

		return false;
	}

	private boolean doesEventHappenToday(TimelineEvent te)
	{
		Date current = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(current);
		clearTimes(c);
		Date today = c.getTime();
		Date date_time = te.getTime_stamp();
		if (date_time.after(today))
		{
			return true;
		}

		return false;
	}

	private boolean didEventHappenThisWeek(TimelineEvent te)
	{
		Date current = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(current);
		int i = c.getFirstDayOfWeek();
		c.set(Calendar.DAY_OF_WEEK, i);
		c = clearTimes(c);
		Date firstDay = c.getTime();
		Date date_time = te.getTime_stamp();
		if (date_time.after(firstDay))
		{
			return true;
		}

		return false;
	}

	private boolean didEventHappenBeforeThisWeek(TimelineEvent te)
	{
		Date current = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(current);
		int i = c.getFirstDayOfWeek();
		c.set(Calendar.DAY_OF_WEEK, i);
		c = clearTimes(c);
		Date firstDay = c.getTime();
		Date date_time = te.getTime_stamp();
		if (date_time.before(firstDay))
		{
			return true;
		}

		return false;
	}

	@Override
	public boolean isEnabled(int position)
	{
		TimelineEvent te = mEvents.get(position);

		if (te.getData() != null && te.getData().length > 0)
		{
			return true;
		}

		return false;
	}

	@Override
	public int getCount()
	{
		return mEvents.size();
	}

	@Override
	public Object getItem(int position)
	{
		if (position < mEvents.size())
		{
			return mEvents.get(position);
		}
		else
		{
			return null;
		}
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView; // =null
		ViewHolder holder;
		LayoutInflater inflater = (LayoutInflater) mContext
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (view == null)
		{
			if (position == 0)
			{
				view = inflater.inflate(R.layout.first_row2, null);
			}
			else
			{
				view = inflater.inflate(this.row, null);
			}

			holder = new ViewHolder();
			view.setTag(holder);
		}
		else
		{
			// holder = new ViewHolder();
			// view.setTag(holder);
			if (position == 0)
			{
				if (view.findViewById(R.id.watermark1) == null)
				{
					// Log.d("mbp", "Reuse  for: "+ position);
					holder = (ViewHolder) view.getTag();
				}
				else
				{
					view = inflater.inflate(R.layout.first_row2, null);
					holder = new ViewHolder();
					view.setTag(holder);
				}
			}
			else
			// position != 0
			{
				if (view.findViewById(R.id.watermark1) != null)
				{
					// Log.d("mbp", "Reuse  for: "+ position);
					holder = (ViewHolder) view.getTag();
				}
				else
				{
					view = inflater.inflate(this.row, null);
					holder = new ViewHolder();
					view.setTag(holder);
				}
			}
		}

		if ((mEvents == null) || ((position + 1) > mEvents.size()))
		{
			Log.d("mbp",
			        "Should not behere: " + position + " / " + mEvents.size());
			return null;
		}

		objBean = mEvents.get(position);

		// holder.tvTitle = (TextView) view.findViewById(R.id.tvtitle);
		holder.tvDesc = (TextView) view.findViewById(R.id.tvdesc);
		holder.tvDate = (TextView) view.findViewById(R.id.tvdate);
		holder.imgView = (ImageView) view.findViewById(R.id.snapshot);
		holder.pbar = (ImageView) view
		        .findViewById(R.id.snapshot_loading_progess);
		holder.imgViewGrp = (RelativeLayout) view
		        .findViewById(R.id.snapshotgrp);
		holder.imgView.setVisibility(View.INVISIBLE);
		holder.watermark = (ImageView) view.findViewById(R.id.watermark);
		holder.watermark1 = (ImageView) view.findViewById(R.id.watermark1);

		if (holder.watermark != null)
		{
			holder.watermark.setVisibility(View.VISIBLE);
		}
		if (holder.watermark1 != null)
		{
			holder.watermark1.setVisibility(View.VISIBLE);
		}

		holder.tvDate.setTypeface(lightTf);
		if (position == 0)
		{
			holder.tvDesc.setTypeface(lightTf);
		}
		else
		{
			holder.tvDesc.setTypeface(regTf);
		}

		if (null != objBean.getAlert_name())
		{
			if (position != 0)
			{
				holder.tvDesc.setText(Html.fromHtml(PublicDefine
				        .capitalizeFirstCharacter(objBean.getAlert_name())));
			}
			else
			{
				holder.tvDesc.setText(Html.fromHtml(objBean.getAlert_name()));
			}
		}
		else
		{
			holder.tvDesc.setText(mContext.getResources().getString(
			        R.string.no_description));
		}

		// 20130826: phung : hide description field
		holder.tvDesc.setVisibility(View.VISIBLE);

		// String when = "dd/MM/yyyy HH:mm aaa";
		String when;
		if (clockMode == PublicDefine.CLOCK_MODE_12H)
		{
			when = "h:mm a";
		}
		else
		{
			when = "H:mm";
		}
		String day = "";

		if (doesEventHappenToday(objBean))
		{
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				when = "h:mm a";
			}
			else
			{
				when = "H:mm";
			}
			// day = "Today ";
			day = "";
		}
		else if (didEventHappenYesterday(objBean))
		{
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				when = "h:mm a";
			}
			else
			{
				when = "H:mm";
			}
			day = "Yesterday ";
		}
		else if (didEventHappenThisWeek(objBean))
		{
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				when = "EEE, h:mm a";
			}
			else
			{
				when = "EEE, H:mm";
			}
			day = "";
		}
		else if (didEventHappenBeforeThisWeek(objBean))
		{
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				when = "MMM d, h:mm a";
			}
			else
			{
				when = "MMM d, H:mm";
			}
			day = "";
		}

		try
		{
			Date dateTime = objBean.getTime_stamp(); // Date dateTime =
			                                         // format.parse(objBean.getValue());
			TimeZone tz = TimeZone.getDefault();

			SimpleDateFormat destFormat = new SimpleDateFormat(when);
			destFormat.setTimeZone(tz);
			String result = destFormat.format(dateTime);
			holder.tvDate.setText(day + result);

			holder.tvDate.setTextColor(Color.DKGRAY);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			holder.tvDate.setText("Invalid Date");
		}

		if (holder.imgView != null)
		{
			if (null != objBean.getData() && objBean.getData().length > 0)
			{
				final ImageView pbar = holder.pbar;

				final View imgView = holder.imgView;

				holder.imgViewGrp.setVisibility(View.VISIBLE);

				ImageLoader.getInstance().displayImage(
				        objBean.getData()[0].getImage(), holder.imgView,
				        options, new ImageLoadingListener()
				        {

					        @Override
					        public void onLoadingCancelled(String arg0,
					                View arg1)
					        {
						        // TODO Auto-generated method stub

					        }

					        @Override
					        public void onLoadingComplete(String arg0,
					                View arg1, Bitmap arg2)
					        {
						        pbar.clearAnimation();
						        pbar.setVisibility(View.INVISIBLE);
						        imgView.setVisibility(View.VISIBLE);

					        }

					        @Override
					        public void onLoadingFailed(String arg0, View arg1,
					                FailReason arg2)
					        {
						        pbar.clearAnimation();
						        pbar.setVisibility(View.INVISIBLE);
						        imgView.setVisibility(View.INVISIBLE);
					        }

					        @Override
					        public void onLoadingStarted(String arg0, View arg1)
					        {
						        pbar.setBackgroundResource(R.drawable.loader_anim1);
						        pbar.setVisibility(View.VISIBLE);
						        anim = (AnimationDrawable) pbar.getBackground();
						        anim.start();
						        imgView.setVisibility(View.INVISIBLE);
					        }

				        });

				// 20130306: hoang: hide watermark if there is no clip
				if (objBean.getData()[0].getFile() == null
				        || objBean.getData()[0].getFile().isEmpty())
				{
					if (holder.watermark != null && holder.watermark1 != null)
					{
						holder.watermark.setVisibility(View.INVISIBLE);
						holder.watermark1.setVisibility(View.INVISIBLE);
					}
				}
			}
			else
			{
				holder.imgViewGrp.setVisibility(View.GONE);
				holder.imgView.setVisibility(View.INVISIBLE);
				holder.pbar.clearAnimation();
				holder.pbar.setVisibility(View.INVISIBLE);
			}
		}

		return view;
	}

	public class ViewHolder
	{
		public TextView		   tvDesc, tvDate;
		public ImageView		watermark, watermark1;
		private ImageView		imgView;
		private ImageView		pbar;
		private RelativeLayout	imgViewGrp;
	}

}
