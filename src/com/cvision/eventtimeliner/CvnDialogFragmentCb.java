package com.cvision.eventtimeliner;

import android.app.DialogFragment;


public interface CvnDialogFragmentCb 
{
	public void doPositiveClick(DialogFragment fragment) ;
	public void doNegativeClick(DialogFragment fragment) ;
}