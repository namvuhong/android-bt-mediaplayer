package com.cvision.eventtimeliner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;

import com.blinkhd.R;
import com.google.gson.Gson;
import com.media.ffmpeg.android.FFMpegMovieViewAndroid;
import com.msc3.CamChannel;
import com.msc3.PublicDefine;
import com.nxcomm.cached.CameraEventOpenHelper;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.HttpResponse;
import com.nxcomm.meapi.PublicDefines;
import com.nxcomm.meapi.device.GetTimelineEventsResponse;
import com.nxcomm.meapi.device.TimelineEvent;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;
import eu.erikw.PullToRefreshListView;
import eu.erikw.PullToRefreshListView.OnRefreshListener;

public class EventManagerFragment extends Fragment implements
        OnItemClickListener, CvnDialogFragmentCb, IEventSummaryCallback
{

	public static final long	  CHECK_LATEST_CLIP_AVAILABLE_INTERVAL	= 30 * 1000;

	private PullToRefreshListView	listView;
	private CameraEventAdapter	  objAdapter;
	private Activity	          activity	                           = null;
	private CamChannel	          selected_channel	                   = null;

	private EventsSummary	      eventsSum	                           = null;

	private AnimationDrawable	  anim	                               = null;
	private static final Gson	  gson	                               = new Gson();
	private DownloadEventsTask	  dl	                               = null;

	private String	              url1	                               = "http://nxcomm-office.no-ip.info/release/events/event_template3.txt";
	private String	              url2	                               = "http://nxcomm-office.no-ip.info/release/events/event_template4.txt";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// setContentView(R.layout.activity_event_manager);

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		View fragmentView = inflater.inflate(R.layout.event_manager, container,
		        false);

		listView = (PullToRefreshListView) fragmentView
		        .findViewById(R.id.listView2);
		listView.setVisibility(View.INVISIBLE);
		listView.setAdapter(objAdapter);
		listView.setOnItemClickListener(this);
		listView.setDividerHeight(0);

		// MANDATORY: Set the onRefreshListener on the list. You could also use
		// listView.setOnRefreshListener(this); and let this Activity
		// implement OnRefreshListener.
		listView.setOnRefreshListener(new OnRefreshListener()
		{

			@Override
			public void onRefresh()
			{
				// Your code to refresh the list contents goes here

				Log.d("mbp", "onRefresh listView");
				loadEvents();
				// Make sure you call listView.onRefreshComplete()
				// when the loading is done. This can be done from here or any
				// other place, like on a broadcast receive from your loading
				// service or the onPostExecute of your AsyncTask.
			}
		});

		SharedPreferences settings = activity.getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		if (selected_channel != null
		        && selected_channel.getCamProfile() != null)
		{
			if (selected_channel.getCamProfile().isInLocal()
			        || selected_channel.getCamProfile().isReachableInRemote())
			{
				String saved_token = settings.getString(
				        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
				String regId = selected_channel.getCamProfile()
				        .getRegistrationId();
				eventsSum = new EventsSummary(activity, saved_token, regId,
				        selected_channel.getCamProfile().getName(), this);
				new Thread(new Runnable()
				{

					@Override
					public void run()
					{
						eventsSum.analyze();
					}
				}).start();
			}
			else
			{
				String unavailable_str = getResources().getString(
				        R.string.camera_appears_to_be_offline_last_online_at_);
				unavailable_str = PublicDefine
				        .capitalizeFirstCharacter(unavailable_str);
				updateAnalyzeResult(unavailable_str);
			}
		}

		return fragmentView;
	}

	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		this.activity = activity;
		objAdapter = new CameraEventAdapter(activity, R.layout.row2, null);

		// objAdapter2 = new CameraEventAdapter2(activity, R.layout.row, null);
	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		super.onDetach();
		activity = null;
	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	private int	page	= 0;

	private void loadEvents()
	{
		if (activity != null)
		{
			dl = new DownloadEventsTask();

			SharedPreferences settings = activity.getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			String regId = null;
			if (selected_channel != null
			        && selected_channel.getCamProfile() != null)
			{
				regId = selected_channel.getCamProfile().getRegistrationId();
			}

			Date dateTime = new Date(currentTime);
			TimeZone tz = TimeZone.getTimeZone("UTC");
			SimpleDateFormat destFormat = new SimpleDateFormat(
			        "yyyy-MM-dd HH:mm:ss");
			destFormat.setTimeZone(tz);
			String before_start_time = destFormat.format(dateTime);
			page++;
			dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, saved_token,
			        regId, before_start_time, String.valueOf(page));
		}
	}

	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
		if (isInLayout())
		{
			SharedPreferences settings = activity.getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			if (selected_channel != null
			        && selected_channel.getCamProfile() != null)
			{
				if (selected_channel.getCamProfile().isInLocal()
				        || selected_channel.getCamProfile()
				                .isReachableInRemote())
				{
					String saved_token = settings.getString(
					        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
					String regId = selected_channel.getCamProfile()
					        .getRegistrationId();
					eventsSum = new EventsSummary(activity, saved_token, regId,
					        selected_channel.getCamProfile().getName(), this);
					new Thread(new Runnable()
					{

						@Override
						public void run()
						{
							eventsSum.analyze();
						}
					}).start();
				}
				else
				{
					String unavailable_str = getResources()
					        .getString(
					                R.string.camera_appears_to_be_offline_last_online_at_);
					unavailable_str = PublicDefine
					        .capitalizeFirstCharacter(unavailable_str);
					updateAnalyzeResult(unavailable_str);
				}
			}
		}
	}

	public void onStart()
	{
		super.onStart();
		currentTime = System.currentTimeMillis();
		yesterdayTime = currentTime - 24 * 60 * 60 * 1000;
		page = 0;
		loadEvents();
	}

	public void onStop()
	{
		super.onStop();

		if (dl != null)
		{
			dl.cancel(true);
		}
	}

	public void onDestroy()
	{
		super.onDestroy();

		if (dl != null)
		{
			dl.cancel(true);
		}
	}

	/* Called from postExecute() */
	private void loadingEvents(TimelineEvent[] res)
	{
		if (res == null || res.length == 0)
		{
			Log.d("mbp", "Query events response: null");
			page--;
			shouldContinueQuery = false;
			// return;
		}
		else
		{
			// check last events
			TimelineEvent lastEvent = res[res.length - 1];
			if (lastEvent.getTime_stamp().getTime() < yesterdayTime)
			{
				shouldContinueQuery = false;
			}
		}

		if (res != null && res.length > 0)
		{
			objAdapter.insertNewEvents(res);
		}
		// objAdapter2.insertNewEvents(res);

		// UI thread here so call it simply like this
		listView.setVisibility(View.VISIBLE);
		listView.onRefreshComplete();

		// continue to load events last 24h from now
		if (shouldContinueQuery == true)
		{
			loadEvents();
		}

		// final Button premiumButton = (Button)
		// listView.getHeaderContainer().findViewById(R.id.button2);
		//
		// premiumButton.postDelayed(new Runnable() {
		//
		// @Override
		// public void run()
		// {
		// premiumButton.setVisibility(View.VISIBLE);
		// }
		//
		// }, 2000);
	}

	private long	currentTime	        = 0;
	private long	yesterdayTime	    = 0;
	private boolean	shouldContinueQuery	= true;

	private class DownloadEventsTask extends
	        AsyncTask<String, Void, TimelineEvent[]>
	{
		private String	saved_token;
		private String	regId;
		private String	before_start_time;
		private String	event_code;
		private String	alerts;
		private int		page;
		private int		offset;
		private int		size;

		protected TimelineEvent[] doInBackground(String... params)
		{
			HttpResponse httpRes;
			saved_token = params[0];
			regId = params[1];
			before_start_time = params[2];
			
			page = Integer.parseInt(params[3]);

			event_code = null;
			alerts = null;
			size = 50;

			TimelineEvent[] timelineEvents = null;
			try
			{

				CameraEventOpenHelper helper = CameraEventOpenHelper
				        .getInstance(getActivity());

				SharedPreferences sharePrefs = getActivity()
				        .getSharedPreferences(PublicDefine.PREFS_NAME, 0);

				String userName = sharePrefs.getString(
				        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

				timelineEvents = helper.getTimelineEventFromDB(userName, regId,
				        before_start_time, event_code, alerts, page, size);
				
				//Log.i("mbp",String.format("Get timeline before start time %s page %d and size %d.",before_start_time,page,size));
				
				if (timelineEvents == null)
				{
					
					before_start_time = Uri.encode(before_start_time);
					
					//Log.i("mbp",String.format("Get time line from db is null"));
					
					GetTimelineEventsResponse eventRes = Device
					        .getTimelineEvents(saved_token, regId,
					                before_start_time, event_code, alerts,
					                page, size);
					if (eventRes != null
					        && eventRes.getStatus() == HttpURLConnection.HTTP_OK)
					{
						timelineEvents = eventRes.getEvents();
						helper.insertEvent(userName, regId, timelineEvents);
					}
				}
				
				else
				{
					//Log.i("mbp","Get time line from db is not null.");
				}

			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return timelineEvents;
		}

		protected void onPostExecute(TimelineEvent[] result)
		{

			loadingEvents(result);

		}

	}

	public HttpResponse getHttpResponse(String url_)
	        throws MalformedURLException, SocketTimeoutException, IOException
	{
		URL url = null;
		HttpURLConnection conn = null;
		BufferedReader reader = null;

		StringBuilder response = new StringBuilder();
		int responseCode = -1;

		url = new URL(url_);
		System.out.println("Sending " + url_);
		conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("Content-Type", "application/json");
		conn.setConnectTimeout(15000);
		conn.setReadTimeout(PublicDefines.getHttpTimeout());

		try
		{
			responseCode = conn.getResponseCode();

			System.out.println("Response code: " + responseCode);

			if (responseCode > 0 && responseCode < 400)
			{
				reader = new BufferedReader(new InputStreamReader(
				        new BufferedInputStream(conn.getInputStream(),
				                20 * 1024)));

				String str;
				while ((str = reader.readLine()) != null)
				{
					response.append(str);
				}

				System.out.println("Execute succeed result: " + response);
			}
			else
			{

				reader = new BufferedReader(new InputStreamReader(
				        new BufferedInputStream(conn.getErrorStream())));
				response.append(reader.readLine());
				System.out.println("Execute failed result: " + response);
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}

		PublicDefines.setHttpTimeout(20000);

		return new HttpResponse(responseCode, response.toString());
	}

	private static final int	DIALOG_NO_CLIP	= 1;

	void showDialog(int dialogId)
	{
		switch (dialogId)
		{
		case DIALOG_NO_CLIP:

			CvnDialogFragment newFragment = CvnDialogFragment
			        .newInstance(R.string.there_is_no_clip_for_this_event_);
			newFragment.setOnClickListener(this);
			newFragment.show(getActivity().getFragmentManager(), "dialog");

		default:
		}
	}

	@Override
	public void doPositiveClick(DialogFragment fragment)
	{
		// TODO Auto-generated method stub
		fragment.dismiss();
	}

	@Override
	public void doNegativeClick(DialogFragment fragment)
	{
		// TODO Auto-generated method stub
		fragment.dismiss();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
	        long arg3)
	{
		// TODO Auto-generated method stub
		String streamUrl = null;

		TimelineEvent te = (TimelineEvent) objAdapter.getItem(position);

		if (te != null && te.getData() != null && te.getData().length > 0)
		{
			Intent intent = new Intent(this.getActivity(),
			        FFMpegPlaybackActivity.class);

			streamUrl = te.getData()[0].getFile();
			Log.d("mbp", "play file:" + streamUrl);
			if (streamUrl != null && !streamUrl.isEmpty())
			{
				if (activity != null)
				{
					FrameLayout liveLayout = (FrameLayout) activity
					        .findViewById(R.id.fragmentHolder1);
					if (liveLayout != null)
					{
						FFMpegMovieViewAndroid mMovieView = (FFMpegMovieViewAndroid) liveLayout
						        .findViewById(R.id.imageVideo);
						if (mMovieView != null && mMovieView.isPlaying())
						{
							mMovieView.release();
						}
					}
				}

				String regId = selected_channel.getCamProfile()
				        .getRegistrationId();
				String eventCode = getEventCodeFromUrl(te.getData()[0]
				        .getImage());

				intent.putExtra(FFMpegPlaybackActivity.STR_EVENT_CODE,
				        eventCode);
				intent.putExtra(FFMpegPlaybackActivity.STR_DEVICE_MAC, regId);
				startActivity(intent);
			}
		}
	}

	private String getEventCodeFromUrl(String url)
	{
		String result = null;

		if (url != null)
		{
			int endIdx = url.indexOf(".jpg");
			int startIdx = endIdx - 33;

			try
			{
				result = url.substring(startIdx, endIdx);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		return result;
	}

	@Override
	public void updateAnalyzeResult(String timelineStatus)
	{
		// TODO Auto-generated method stub
		if (activity != null)
		{
			Log.d("mbp", "Timeline analyze update, status: " + timelineStatus);
			String timelineSummary;
			if (timelineStatus != null)
			{
				timelineSummary = timelineStatus;
			}
			else
			{
				timelineSummary = PublicDefine
				        .capitalizeFirstCharacter(getResources().getString(
				                R.string.all_is_calm))
				        + " at " + selected_channel.getCamProfile().getName();
			}

			TimelineEvent statusEvent = new TimelineEvent();
			statusEvent.setAlert_name(timelineSummary);
			Date current = new Date();
			statusEvent.setTime_stamp(current);
			statusEvent.setAlert(-1);
			TimeZone tz = TimeZone.getDefault();
			SimpleDateFormat destFormat = new SimpleDateFormat(
			        "yyyyMMddHHmmssSSS");
			destFormat.setTimeZone(tz);
			String eventCode = destFormat.format(current);
			statusEvent.setValue(eventCode);
			objAdapter.insertNewEvents(new TimelineEvent[] { statusEvent });
			listView.setVisibility(View.VISIBLE);
			listView.onRefreshComplete();
		}
	}

}
