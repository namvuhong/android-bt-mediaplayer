package com.cvision.eventtimeliner;

public interface IEventSummaryCallback {
	public void updateAnalyzeResult(String timelineStatus);
}
