package com.cvision;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CameraEvents implements Type
{
	private String latestId; 
	private String oldesId; 
	private CameraEvent [] events; 
	
	
	

	public  CameraEvent []  getEvents()
	{
		return events; 
	}
	public String toString()
	{
		StringBuilder string_  = new StringBuilder(); 
		for (CameraEvent ce : events)
		{
			string_.append(ce.toString() + " \n" ); 
		}
		
		return string_.toString(); 
	}
	public List<CameraEvent> getListEvents() 
	{
		
		ArrayList<CameraEvent> list = new ArrayList<CameraEvent>(); 
		for (CameraEvent ce: events)
		{
			list.add(ce); 
		}
		
		
		
		return list;
	}
}
