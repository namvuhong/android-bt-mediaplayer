/**
 * 
 */
package com.msc3;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CreateSessionKeyResponse;
import com.nxcomm.meapi.device.CreateSessionKeyResponse.CreateSessionKeyResponseData;
import com.nxcomm.meapi.device.CreateSessionKeyResponse2;
import com.nxcomm.meapi.device.CreateSessionKeyResponse2.CreateSessionKeyResponseData2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * @author hoang
 *
 */
public class ViewRemoteCamViaWowzaRequestTask extends ViewRemoteCamRequestTask {
	
	private String regId;
	private String userToken;
	private String clientType;
	
	protected Context mContext;
	
	private int server_status_code;
	private int server_error_code;
	
	/**
	 * @param mContext
	 * @param mHandler
	 */
	public ViewRemoteCamViaWowzaRequestTask(Handler mHandler, Context mContext) {
		super(mHandler, mContext);
		
		this.mContext = mContext;
		
		server_error_code = 404;
		server_status_code = 404; 
	}

	private static final String RTMP_URL = "rtmp://wowza.api.simplimonitor.com:1935";
	
	private static final String LOCAL_RTMP_URL = "rtmp://nxcomm-office.no-ip.info:1935";
	

	@Override
	protected BabyMonitorAuthentication doInBackground(String... params) {
		
		BabyMonitorAuthentication bm_auth = null;
		
		regId = params[0];
		userToken = params[1];
		clientType = params[2];
		
		Log.d("mbp", "View cam upnp request");

		int retry = 1;
		do
		{
			try {

				CreateSessionKeyResponse2 session_info = 
						Device.getSessionKey2(userToken, regId, clientType);

				if (session_info != null && (session_info.getStatus() == HttpURLConnection.HTTP_OK))
				{
					if (session_info.isSucceed())
					{
						CreateSessionKeyResponseData2 session_data = session_info.getData();
						if (session_data != null)
						{
							String streamUrl = session_data.getUrl();
							
							if (streamUrl != null)
							{
								
								SharedPreferences settings = mContext.getSharedPreferences(
										PublicDefine.PREFS_NAME, 0);
								boolean shouldUseLocalServer = settings.getBoolean(
										PublicDefine.PREFS_SHOULD_USE_LOCAL_WOWZA_SERVER, false);
								
								/*20131029:phung: hack stream url */ 
								if (shouldUseLocalServer == true)
								{
									if (streamUrl.startsWith(RTMP_URL))
									{
										streamUrl = streamUrl.replace(RTMP_URL, LOCAL_RTMP_URL);
									}
								}
								
								Log.d("mbp", "View cam streamUrl: " + streamUrl);
								
								bm_auth = new BabyMonitorRelayAuthentication(null, "80", null, regId,
										null, streamUrl, 80, null, null);
								
								
								break;
							}
						}
					}
					else
					{
						server_status_code = session_info.getStatus();
						server_error_code = session_info.getCode();
					}
				}

			} catch (SocketTimeoutException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
			}
			
		} while (--retry > 0);
		
		return bm_auth;
	}
	
	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(BabyMonitorAuthentication result) {
		if (mHandler != null)
		{
			Message msg;
			if (result != null)
			{
				msg = Message.obtain(mHandler, MSG_VIEW_CAM_SUCCESS, result);
			}
			else
			{
				msg = Message.obtain(mHandler, MSG_VIEW_CAM_FALIED, server_status_code, server_error_code);
			}
			mHandler.dispatchMessage(msg);
		}
	}

}
