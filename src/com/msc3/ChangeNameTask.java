package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.JsonResponse;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class ChangeNameTask extends AsyncTask<String, String, Integer> {

	private String _error_desc;

	private static final int UPDATE_SUCCESS = 0x1;
	private static final int UPDATE_FAILED_SERVER_UNREACHABLE = 0x11;
	private static final int UPDATE_FAILED_WITH_DESC = 0x12;
	
	private Context mContext; 
	private IChangeNameCallBack mCallBack; 
	
	public ChangeNameTask(Context c, IChangeNameCallBack cb)
	{
		mContext = c; 
		mCallBack = cb ; 
	}

	@Override
	protected Integer doInBackground(String... params) {

		String usrToken = params[0];
		String encode_name = params[1];
		
		String regId = params[2];

		int ret = -1;
		try {
			JsonResponse jRes = Device.changeCameraName(usrToken, regId, encode_name);
			if (jRes != null && jRes.isSucceed())
			{
				if (jRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					ret = UPDATE_SUCCESS;
				}
				else
				{
					ret = UPDATE_FAILED_WITH_DESC;
					Log.d("mbp", "Update camName _error_desc: " + jRes.getStatus());
					_error_desc = jRes.getMessage();
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			//Connection Timeout - Server unreachable ??? 
			ret = UPDATE_FAILED_SERVER_UNREACHABLE;
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
			ret = UPDATE_FAILED_SERVER_UNREACHABLE;
		}
		catch (IOException e) {
			e.printStackTrace();
			//Connection Timeout - Server unreachable ??? 
			ret = UPDATE_FAILED_SERVER_UNREACHABLE;
		}

		return new Integer(ret);
	}

	/* UI thread */
	protected void onPostExecute(Integer result)
	{
		if (result.intValue() == UPDATE_SUCCESS)
		{
			mCallBack.update_cam_success();
		}
		else
		{
			mCallBack.update_cam_failed();
		}
	}
}
