package com.msc3;


import com.blinkhd.R;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

public class ClearNotificationActivity extends Activity  {

	private PowerManager.WakeLock wl;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vox_main);

		String phonemodel = android.os.Build.MODEL;
		// register if it's not iHome Phone
		if(!phonemodel.equals(PublicDefine.PHONE_MBP2k) && !phonemodel.equals(PublicDefine.PHONE_MBP1k)) 
		{	
			//Should not be here 
			return; 
		}
		else
		{
			//Clear the recurring sound now!!
//			if (MBP2K_NotifyService.isServiceRunning(this))
//			{
//				Log.d("mbp", "Sound is playing... kill it now since where are cleared");
//				Intent recurringAlertService = new Intent(this, MBP2K_NotifyService.class);
//				stopService(recurringAlertService);
//			}
			
		}

		Log.d("mbp", "ClearNotificationActivity: ON Create return ");
	}

	protected void onStart() {
		super.onStart();


		finish();
		return; 
	}

	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
	}

	protected void onNewIntent (Intent intent)
	{
	}

	protected void onPause() {
		super.onPause();

		
	}


	protected void onResume()
	{
		super.onResume();
	}

	protected void onStop() {
		super.onStop();
	}

	protected void onDestroy()
	{
		super.onDestroy();
		Log.d("mbp","VOX act onDestroy");

	}

}
