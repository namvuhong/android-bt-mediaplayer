package com.msc3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

import com.blinkhd.TalkbackFragment;

import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;

public class AudioOutStreamer implements Runnable
{
	private static final String TAG = "mbp";
	public static final int	UNMUTE_CAMERA_AUDIO_FAILED	= 0xde000007;

	private Socket	        audioOutSock;
	private String	        addr;
	private String	        http_pass;
	private int	            cmd_port;
	private int	            audio_port;
	private PCMRecorder	    recorder;
	private boolean	        streaming;
	private boolean	        hasSetDeviceAudioOn;
	private boolean	        isInLocal	               = false;
	private String	        session_key	               = null;
	private String	        stream_id	               = null;

	private Handler	        mHandler;

	/* @ip should be in the form x.y.z.t */
	public AudioOutStreamer(PCMRecorder recorder, String ip, int cmd_port,
	        String http_pass, int audio_port, Handler errorCallback)
	{
		/* keep it for future reference */
		addr = ip;
		this.cmd_port = cmd_port;
		this.audio_port = audio_port;
		audioOutSock = null;
		this.recorder = recorder;
		streaming = false;
		hasSetDeviceAudioOn = false;

		this.http_pass = http_pass;
		if (http_pass == null)
			this.http_pass = "";

		mHandler = errorCallback;
	}

	public String getAddr()
	{
		return addr;
	}

	public void setAddr(String addr)
	{
		this.addr = addr;
	}

	public int getAudioPort()
	{
		return audio_port;
	}

	public void setAudioPort(int audio_port)
	{
		this.audio_port = audio_port;
	}

	public void setLocalMode(boolean isInLocal)
	{
		this.isInLocal = isInLocal;
	}

	public boolean isLocalMode()
	{
		return isInLocal;
	}

	public void setSessionKey(String session_key)
	{
		this.session_key = session_key;
	}

	public void setStreamId(String stream_id)
	{
		this.stream_id = stream_id;
	}

	public void startStreaming()
	{
		streaming = true;
	}

	public void stopStreaming()
	{
		streaming = false;
	}

	public void run()
	{
		// run_udp();
		run_tcp();
	}

	private int tryConnect()
	{
		URL url = null;
		HttpURLConnection conn = null;
		int responseCode = -1;

		String usr_pass = String.format("%s:%s",
		        PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);

		try
		{

			if (isInLocal == false)
			{
				// NOT SUPPORTED yet
				if (doHandShake() == false)
				{
					Log.d(TAG, "doHandshake failed");
					mHandler.sendMessage(Message.obtain(mHandler, 
							TalkbackFragment.MSG_AUDIO_STREAM_HANDSHAKE_FAILED));
					return -1;
				}
			}
			else
			{
				Log.d(TAG, "AudioOn cmd: "+
						"http://"+ addr+":" +cmd_port+ PublicDefine.HTTP_CMD_PART+
						PublicDefine.SET_DEVICE_AUDIO_ON);
				url = new URL("http://" + addr + ":" + cmd_port
				        + PublicDefine.HTTP_CMD_PART
				        + PublicDefine.SET_DEVICE_AUDIO_ON);
				conn = (HttpURLConnection) url.openConnection();
				conn.addRequestProperty(
				        "Authorization",
				        "Basic "
				                + Base64.encodeToString(
				                        usr_pass.getBytes("UTF-8"),
				                        Base64.NO_WRAP));
				conn.connect();
				conn.getContentType();
				responseCode = conn.getResponseCode();
				// Log.d(TAG,"responseCode:"+ responseCode);
				audioOutSock = new Socket();
				audioOutSock.connect(new InetSocketAddress(addr, audio_port),
				        10000);

				hasSetDeviceAudioOn = true;
			}

		}
		catch (IOException e)
		{
			e.printStackTrace();
			return -1;
		}

		return 0;
	}

	public void run_tcp()
	{
		// OutputStream outStream= null;
		DataOutputStream outStream = null;
		int data_len = 4 * 1024;
		byte[] data_out = new byte[data_len];
		int actual_read = -1;

		do
		{
			/* make sure we have some data before opening the connection */
			actual_read = recorder.readFromAudioBuffer(data_out, data_len);
			try
			{
				Thread.sleep(125);// 125
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		while (actual_read == -1);

		if (tryConnect() != -1)
		{
			try
			{
				// outStream = new
				// BufferedOutputStream(audioOutSock.getOutputStream());
				outStream = new DataOutputStream(audioOutSock.getOutputStream());
				if (actual_read != -1)
					outStream.write(data_out, 0, actual_read);
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			while (streaming)
			{

				try
				{
					actual_read = recorder.readFromAudioBuffer(data_out,
					        data_len);
					if (actual_read != -1)
					{
						outStream.write(data_out, 0, actual_read);
					}
				}
				catch (SocketException e1)
				{
					e1.printStackTrace();
					streaming = false;
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}

				try
				{
					Thread.sleep(125);// 125
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			if (streaming == false) // stopped by releasing PTT button
			{
				/* try to push all the recorded data out */
				Log.d(TAG, "Pushing out last bit of audio");
				do
				{
					try
					{
						actual_read = recorder.readFromAudioBuffer(data_out,
						        data_len);
						if (actual_read != -1)
						{
							outStream.write(data_out, 0, actual_read);
						}
					}
					catch (SocketException e1)
					{
						e1.printStackTrace();
					}
					catch (IOException e1)
					{
						e1.printStackTrace();
					}
					// } while (actual_read != -1);
				}
				while (actual_read != -1);
				
				Log.d(TAG, "Finish flushing audio hardware buffer");
				recorder.releaseRecorder();
			}

			try
            {
	            Thread.sleep(1000);
            }
            catch (InterruptedException e2)
            {
	            // TODO Auto-generated catch block
	            e2.printStackTrace();
            }
			
			// Can't disable talkback, camera audio may be muted, please
			// activate talkback again

			try
			{
				outStream.flush();
				outStream.close();

			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			if (audioOutSock != null)
			{
				try
				{
					audioOutSock.close();
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}

			/* send audio_out0 cmd */
			HttpURLConnection conn;
			int responseCode = -1;
			String usr_pass = String.format("%s:%s",
			        PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);

			if (isInLocal == false)
			{
				// NOT SUPPORTED YET
			}
			else
			{

				try
				{
					URL url = new URL("http://" + addr + ":" + cmd_port
					        + PublicDefine.HTTP_CMD_PART
					        + PublicDefine.SET_DEVICE_AUDIO_OFF);
					Log.d(TAG, "AudioOff cmd: "+
							"http://"+ addr+":" +cmd_port+
							PublicDefine.HTTP_CMD_PART+
							PublicDefine.SET_DEVICE_AUDIO_OFF);
					conn = (HttpURLConnection) url.openConnection();
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(5000);
					conn.addRequestProperty(
					        "Authorization",
					        "Basic "
					                + Base64.encodeToString(
					                        usr_pass.getBytes("UTF-8"),
					                        Base64.NO_WRAP));
					conn.connect();
					conn.getContentType();
					responseCode = conn.getResponseCode();
					// Log.d(TAG,"responseCode:"+ responseCode);

					hasSetDeviceAudioOn = false;
				}
				catch (SocketTimeoutException ste)
				{
					ste.printStackTrace();
					// Timeout ;
					Log.d(TAG, "Socket Timeout -- send error now ");
					mHandler.dispatchMessage(Message.obtain(mHandler,
					        UNMUTE_CAMERA_AUDIO_FAILED));
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

			}

		}
		else
		{
			if (hasSetDeviceAudioOn)
			{
				HttpURLConnection conn;
				int responseCode = -1;
				String usr_pass = String.format("%s:%s",
				        PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);
				try
				{
					URL url = new URL("http://" + addr + ":" + cmd_port
					        + PublicDefine.HTTP_CMD_PART
					        + PublicDefine.SET_DEVICE_AUDIO_OFF);
					Log.d(TAG, "AudioOff cmd: "+
							"http://"+ addr+":" +cmd_port+
							PublicDefine.HTTP_CMD_PART+
							PublicDefine.SET_DEVICE_AUDIO_OFF);
					conn = (HttpURLConnection) url.openConnection();
					conn.setConnectTimeout(5000);
					conn.setReadTimeout(5000);
					conn.addRequestProperty(
					        "Authorization",
					        "Basic "
					                + Base64.encodeToString(
					                        usr_pass.getBytes("UTF-8"),
					                        Base64.NO_WRAP));
					conn.connect();
					conn.getContentType();
					responseCode = conn.getResponseCode();
					// Log.d(TAG,"responseCode:"+ responseCode);

					hasSetDeviceAudioOn = false;
				}
				catch (SocketTimeoutException ste)
				{
					ste.printStackTrace();
					// Timeout ;
					Log.d(TAG, "Socket Timeout -- send error now ");
					mHandler.dispatchMessage(Message.obtain(mHandler,
					        UNMUTE_CAMERA_AUDIO_FAILED));

				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}

			recorder.stopRecording();
			/* try to push all the recorded data out */
			Log.d(TAG, "Pushing out last bit of audio");
			do
			{
				actual_read = recorder.readFromAudioBuffer(data_out, data_len);
			}
			while (actual_read != -1);

			Log.d(TAG, "Finish flushing audio hardware buffer");
			recorder.releaseRecorder();
		}

	}

	public boolean doHandShake()
	{
		boolean result = false;
		byte[] handShakeSuccessResponse = { 01, 07, 00, 00, 00, 00, 00 };
		int timeout = 10 * 1000;
		byte ty = 1;
		byte lengt = 79;
		byte[] header = new byte[3];
		int handshakestrlen;

		byte[] sendData = new byte[79];

		header[0] = ty;
		header[1] = lengt;
		header[2] = 0;
		String handshakeRequest = stream_id + session_key;

		audioOutSock = new Socket();
		try
		{
			SocketAddress serverAddr = new InetSocketAddress(
			        InetAddress.getByName(addr), audio_port);

			audioOutSock.connect(serverAddr, timeout);

			BufferedOutputStream out = new BufferedOutputStream(
			        audioOutSock.getOutputStream());
			BufferedInputStream in = new BufferedInputStream(
			        audioOutSock.getInputStream());
			System.arraycopy(header, 0, sendData, 0, 3);
			System.arraycopy(handshakeRequest.getBytes(), 0, sendData, 3, 76);
			out.write(sendData, 0, sendData.length);
			out.flush();

			byte[] data = new byte[7];
			String handShakeResponse = null;
			do
			{
				handshakestrlen = in.read(data);
				try
				{
					handShakeResponse = new String(data, 0, handshakestrlen,
					        "UTF-8");
				}
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			while (in.available() != 0);

			if (Arrays.equals(handShakeSuccessResponse, data))
			{
				result = true;
			}

		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}

		return result;
	}

	/*** use UDP ***/
	public void run_udp()
	{
		int data_len = 2 * 1024;
		byte[] data_out = new byte[data_len];
		int actual_read = -1;
		DatagramPacket packet = null;
		/* Create new UDP-Socket */
		DatagramSocket socket = null;

		/*
		 * Test addr = "192.168.2.102"; port = 51110;
		 */
		URLConnection conn;
		URL url;
		try
		{
			url = new URL("http://" + addr + PublicDefine.HTTP_CMD_PART
			        + PublicDefine.SET_DEVICE_AUDIO_OFF);
			conn = url.openConnection();
			conn.getContentType();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		try
		{
			socket = new DatagramSocket();
		}
		catch (SocketException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		do
		{

			/* make sure we have some data before opening the connection */
			actual_read = recorder.readFromAudioBuffer(data_out, data_len);
			try
			{
				Thread.sleep(125);// 125
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		while (actual_read == -1);

		if (socket != null)
		{
			try
			{
				if (actual_read != -1)
				{
					/*
					 * Create UDP-packet with data & destination(url+port)
					 */
					packet = new DatagramPacket(data_out, actual_read,
					        InetAddress.getByName(addr), cmd_port);

					/* Send out the packet */
					socket.send(packet);
				}
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			while (streaming)
			{

				try
				{
					actual_read = recorder.readFromAudioBuffer(data_out,
					        data_len);
					if (actual_read != -1)
					{
						packet.setData(data_out, 0, actual_read);
						/* Send out the packet */
						socket.send(packet);
					}
				}
				catch (SocketException e1)
				{
					e1.printStackTrace();
					streaming = false;
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}

				try
				{
					Thread.sleep(125);// 125
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			/* send audio_out0 cmd */
			try
			{
				url = new URL("http://" + addr + PublicDefine.HTTP_CMD_PART
				        + PublicDefine.SET_DEVICE_AUDIO_ON);
				conn = url.openConnection();
				conn.getContentType();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			socket.close();
		}

	}

}
