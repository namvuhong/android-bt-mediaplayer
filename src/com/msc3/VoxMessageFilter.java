package com.msc3;

import java.util.ArrayList;

import android.util.Log;

public class VoxMessageFilter {

	private static final int DEFAULT_CLEANUP_SLEEP_TIME = 10000;//10sec
	private ArrayList<VoxMessage> voxList;
	private CleanUpRunnable cleanup ;
	private Thread _cleanup ; 
	/***
	 * Singleton Pattern: make sure there's only one instance of such filter 
	 */
	private static VoxMessageFilter instance = new VoxMessageFilter();
	public static synchronized VoxMessageFilter getInstance() {
		return instance;
	}

	private VoxMessageFilter()
	{
		voxList = new ArrayList<VoxMessage>();
	}


	public void insertVoxMessage(VoxMessage vmsg)
	{
		if(containVoxMessage(vmsg) == null)
		{
			voxList.add(vmsg);
			
			if (_cleanup == null || !_cleanup.isAlive())
			{
				cleanup = new CleanUpRunnable(DEFAULT_CLEANUP_SLEEP_TIME);
				_cleanup = new Thread( cleanup, "VOXMSGFilter");
				_cleanup.start();
			}

		}
	}


	public VoxMessage containVoxMessage(VoxMessage newVm)
	{
		VoxMessage ret_vm = null;
		for (VoxMessage vm :voxList)
		{
			if (vm.getTrigger_mac().equalsIgnoreCase(newVm.getTrigger_mac())
					&& (vm.getVoxType() == newVm.getVoxType())
				)
			{
				ret_vm = vm ;
				break; 
			}
		}
		return ret_vm;
	}

	/**
	 * @param vmsg
	 * @return TRUE if vmsg was found and deleted
	 *         FALSE otherwise
	 */
	public boolean deleteVoxMessage(VoxMessage vmsg)
	{
		VoxMessage vm = null;
		boolean found = false;
		for (int i = 0 ; i< voxList.size(); i++)
		{
			vm = voxList.get(i);
			if (vm.getTrigger_mac().equalsIgnoreCase(vmsg.getTrigger_mac()))
			{
				voxList.remove(i);
				found = true;
				break; 
			}
		}
		return found; 
	}

	/**
	 * @param incoming
	 * @return FALSE if incoming message should be ignored because the same entry exist
	 *         TRUE if incoming message should be passed to VOX
	 */
	public boolean checkIncomingVoxMesage(VoxMessage incoming)
	{
		VoxMessage vmsg = containVoxMessage(incoming);
		long now; 

		if (vmsg != null)
		{
			now = System.currentTimeMillis();
			if ( now <= vmsg.getExpired_time() )
			{
				//vm is a REPEAT vox message - ignore it .
				
				return false; 
			}
			else
			{
				//vm is a valid NEW vox message - remove and update with this vm because of ip
				deleteVoxMessage(vmsg);
				insertVoxMessage(incoming);
				return true;
			}
		}
		else //vm is a valid NEW vox message 
		{
			insertVoxMessage(incoming);
			return true;
		}
	}

	public long purgeOldVoxMessages()
	{
		long now = System.currentTimeMillis();
		long latestExpiredTime = now;
		boolean found; 
		while (!voxList.isEmpty())
		{
			found = false; 
			//scan and remove 1 vox msg at a time - to make sure all the indices are handled correctly
			for (VoxMessage vm : voxList)
			{
				if (now > vm.getExpired_time())
				{
					voxList.remove(vm);
					found = true; 
					
					break; //from for loop 
				}
			}


			/* we can no longer find any entry with expiredtime */
			if (found == false)
			{
				break; //from while loop 
			}
		}

		//Once reach here - either list is empty or no more expired entry
		//if voxList isEmpty() -> stop the thread
		//  else return next wake up time

		if (!voxList.isEmpty())
		{
			for (VoxMessage vm : voxList)
			{
				if (latestExpiredTime < vm.getExpired_time())
				{
					latestExpiredTime = vm.getExpired_time();
				}
			}

		}
		else
		{
			
			latestExpiredTime = 0;
		}

		return latestExpiredTime; //list is empty
	}

	class CleanUpRunnable implements Runnable
	{
		private long next_wakeup; 

		CleanUpRunnable (long when)
		{
			next_wakeup = when;
		}

		@Override
		public void run() {
			long now,sleep_duration;
			do 
			{
				now = System.currentTimeMillis();
				if ( next_wakeup > (now +1))
				{
					sleep_duration = next_wakeup - System.currentTimeMillis();
				}
				else
				{
					//default sleep time to 1 sec
					sleep_duration =1000;
				}

				try {
					Thread.sleep(sleep_duration);
				} catch (InterruptedException e) {
				}

				next_wakeup = purgeOldVoxMessages();
			}
			while (next_wakeup != 0 );
			
		}
	}


}



