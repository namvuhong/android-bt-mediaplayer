/**
 * 
 */
package com.msc3.comm;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;

import android.net.Uri;
import android.util.Log;

import com.barchart.udt.ExceptionUDT;
import com.barchart.udt.SocketUDT;
import com.barchart.udt.TypeUDT;
import com.msc3.BabyMonitorRelayAuthentication;
import com.msc3.PublicDefine;

/**
 * @author NxComm
 *
 */
public class RelayRequestSendRecv extends UDTRequestSendRecv {

	public RelayRequestSendRecv(String device_ip, int device_port,
			BabyMonitorRelayAuthentication bm) {
		device_addr = new InetSocketAddress(device_ip,
				device_port);
		localPort = bm.getLocalPort(); 
		this.bm = bm; 
	}
	
	
	/************* Asyn Taks methods *****************************/

	/* background thread: post the type of request here 
	 * for e.g.: "action=command&command=brightness_plus"
	 * */

	protected String doInBackground(String... urls)  {
		String request = null; 
		String response = null;
		byte[] request_bytes = null; 

		if ( device_addr ==null)
		{
			Log.d("mbp","device_addr: = null");
			return null; 
		}
		try {
			udtSock = new SocketUDT(TypeUDT.STREAM);
			udtSock.setSoTimeout(5000);
			udtSock.connect(device_addr);
			//Log.d("mbp","num of req: "+ urls.length);

			for (int i =0; i < urls.length; i++)
			{
				request = urls[i];
				request_bytes = request.getBytes(); 
				udtSock.send(request_bytes); 

				InputStream is = udtSock.getUDTInputStream();
				if (is == null)
				{
					return null;
				}

				_inputStream = new DataInputStream(new BufferedInputStream(is));
				response = _inputStream.readLine();
			}

		} catch (ExceptionUDT e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return response;
	}

	/* on UI Thread */
	protected void onProgressUpdate(Integer... progress) {
	}


	/* on UI Thread */
	protected void onPostExecute(String result) {
		Log.d("mbp","UDT:response: >"+result+"<");
		//TODO: parse result
	}

	/************* end of Asyn Taks methods *****************************/
	
	/************* Instance methods ******************************/

	/**
	 * sendCommand - instance method, use the current socket to to send -
	 * - at any time, there can only be one caller to this method - 
	 * @param urls[0] : request
	 *         
	 * optional - urls[1] : basic authentication user name 
	 *            urls[2] : basic authentication pass wrd 
	 *                   
	 * @return - String - reponse (could be null ) 
	 */
	public synchronized String sendCommand(String... urls) 
	{
		String request = null; 
		String response = null;
		byte[] request_bytes = null; 

		String usr = "";
		String pwd = "";
		if (urls.length == 3)
		{
			usr = urls[1];
			pwd = urls[2];
		}
			
		request = urls[0];
			
		
		if ( device_addr ==null)
		{
			Log.d("mbp","device_addr: = null");
			return null; 
		}
		//Log.d("mbp","sendCommand: "+  request);
		try {
			udtSock = new SocketUDT(TypeUDT.STREAM);
			//udtSock.setSoTimeout(5000);
			udtSock.bind(new InetSocketAddress(localPort));
			udtSock.connect(device_addr);
			
			request_bytes = request.getBytes(); 
			udtSock.send(request_bytes); 

			InputStream is = udtSock.getUDTInputStream();
			if (is == null)
			{
				return null;
			}

			_inputStream = new DataInputStream(new BufferedInputStream(is));
			response = _inputStream.readLine();

			udtSock.close();
		} catch (ExceptionUDT e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


		return response;
	}

	
	public synchronized String sendCommandViaServer(String... urls) 
	{
		String request = null; 
		String response = null;
		
		String command  = null, mac = null,http_userName = null , http_userPass = null; 
		String channelId = null; 
		request = urls[0];

		mac = ((BabyMonitorRelayAuthentication)bm).getDeviceMac(); 
		channelId = ((BabyMonitorRelayAuthentication)bm).getChannelID(); 
		
		command = request; 
		http_userName = ((BabyMonitorRelayAuthentication)bm).getUser();
		http_userPass= ((BabyMonitorRelayAuthentication)bm).getPass();
		
		
		//20121120: new way to send command
		command = "action=command&command=" + command;
		// URL-Encoded
		command =  Uri.encode(command);


		String http_addr =String.format("%1$s%2$s%3$s%4$s%5$s",
				PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART,
				PublicDefine.SEND_CTRL_CMD,
				PublicDefine.SEND_CTRL_PARAM_1 + mac,
				PublicDefine.SEND_CTRL_PARAM_2 + channelId,
				PublicDefine.SEND_CTRL_PARAM_3 + command
				) ;

		response = HTTPRequestSendRecv.sendRequest_block_for_response_1(http_addr, http_userName,http_userPass, "3000");


		return response;
	}



	/************* end of Instance methods ******************************/
	
}
