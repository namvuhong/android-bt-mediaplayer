package com.msc3.comm;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.msc3.BabyMonitorAuthentication;
import com.msc3.BabyMonitorUdtAuthentication;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.SendCommandResponse;
import com.nxcomm.meapi.device.SendCommandResponse.DeviceCmdResponse;
import com.nxcomm.meapi.device.SendCommandResponse.SendCommandResponseData;

public class UDTRequestSendRecv extends AsyncTask<String, Integer, String> {

	//protected  SocketUDT udtSock ;
	protected InetSocketAddress device_addr; 
	protected int localPort; 
	protected DataInputStream _inputStream;
	protected BabyMonitorAuthentication bm; 


	/**
	 * 
	 */
	public UDTRequestSendRecv() {
	}
	
	public  UDTRequestSendRecv(String device_ip, int device_port, BabyMonitorUdtAuthentication bm) 
	{
		
		device_addr = new InetSocketAddress(device_ip,
				device_port);
		this.localPort = bm.getLocalPort(); 
		this.bm = bm; 
		
		
	}

	/************ STATIC METHODS ***************************/


	/**
	 * sendRequest_block_for_response
	 * 
	 * @param urls[0] : device _ip 
	 *        urls[1] : port 
	 *        urls[2] : request 
	 *        urls[3] : localPort
	 *         
	 * optional - urls[4] :  relayToken  
	 *            
	 *                   
	 * @return
	 */
	public static String sendRequest_block_for_response(String... urls)
	{
		String request = null; 
		String response = null;
		
//		DataInputStream inputStream = null;
//		byte[] request_bytes = null; 
//		SocketUDT udtSock ;
//		InetSocketAddress device_addr; 
//		
//		String device_ip = urls[0]; 
//		int device_port = -1; 
//		try 
//		{
//			device_port = Integer.parseInt(urls[1]);
//		}
//		catch (NumberFormatException nfe)
//		{
//			nfe.printStackTrace(); 
//			return null; 
//		}
//		String idtoken = "";
//		if (urls.length == 5)
//		{
//			idtoken = urls[4];
//			
//		}
//		
//		int localPort = Integer.parseInt(urls[3]);
//		
//		/* check for RELAY server */ 
//		if (device_ip.equalsIgnoreCase(PublicDefine.RELAY_SERVER))
//		{
//			//OMG .. 
//			request = "##"+urls[2] +":::" + idtoken;
//		}
//		else
//		{
//			request = urls[2];
//		}
//		
//		
//
//		device_addr = new InetSocketAddress(device_ip,
//				device_port);
//
//		try {
//			udtSock = new SocketUDT(TypeUDT.STREAM);
//			
//			//bind to the local port before connecting to camera 
//			if ( localPort > 0)
//			{
//				udtSock.setSoTimeout(5000);
//				udtSock.bind(new InetSocketAddress(localPort));
//			}
//			else
//			{
//				Log.d("mbp"," dont bind .. to any local port"); 
//			}
//			
//			udtSock.connect(device_addr);
//
//			
//			request_bytes = request.getBytes(); 
//			udtSock.send(request_bytes); 
//
//			InputStream is = udtSock.getUDTInputStream();
//			if (is == null)
//			{
//				return null;
//			}
//
//			inputStream = new DataInputStream(new BufferedInputStream(is));
//			response = inputStream.readLine();
//		
//			
//			udtSock.close();
//		} 
//		catch (ExceptionUDT e) 
//		{
//			e.printStackTrace();
//		} 
//		catch (IllegalArgumentException e) 
//		{
//			e.printStackTrace();
//		} 
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}

		return response;
	}


	//https://www.monitoreverywhere.com/BMS173/phoneservice?action=command&command=send_control_command
	//         &macaddress=000EA3070AC9&channelid=123456789012&query=value_contract
	
	
	/**
	 * sendRequest_block_for_response
	 * 
	 * @param urls[0] : mac 
	 *        urls[1] : channelId 
	 *        urls[2] : camera query (command part only, eg: get_skp_volume) 
	 *        urls[3] : user
	 *      - urls[4] : pass  
	 *            
	 *                   
	 * @return
	 */
	public static String sendRequest_via_stun(String... urls)
	{

		String command  = null, mac = null,http_userName = null , http_userPass = null; 
		String channelId = null; 
		
		mac = urls[0]; 
		channelId = urls[1]; 
		command = urls[2]; 
		http_userName = urls[3];
		http_userPass= urls[4];
		
		//20121120: new way to send command
		command = "action=command&command=" + command;
		// URL-Encoded
		command =  Uri.encode(command);
		if (mac.contains(":"))
		{
			mac = PublicDefine.strip_colon_from_mac(mac);
		}
		
		String http_addr =String.format("%1$s%2$s%3$s%4$s%5$s",
				PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART,
				PublicDefine.SEND_CTRL_CMD,
				PublicDefine.SEND_CTRL_PARAM_1 + mac,
				PublicDefine.SEND_CTRL_PARAM_2 + channelId,
				PublicDefine.SEND_CTRL_PARAM_3 + command
				) ;

		
		String response = HTTPRequestSendRecv.sendRequest_block_for_response_1(http_addr, http_userName,http_userPass);
		
		return response;
	}
	
	
	
	public static String sendRequest_via_stun2(String... urls)
	{
		String userToken = null;
		String regId = null;
		String command  = null;

		userToken = urls[0];
		regId = urls[1]; 
		command = urls[2];

		// URL-Encoded
		//command =  Uri.encode(command);

		String response = null;

		try {
			SendCommandResponse send_cmd_res = Device.sendCommand(
					userToken, regId, command);
			if (send_cmd_res != null)
			{
				int status_code = send_cmd_res.getStatus();
				if (status_code == HttpURLConnection.HTTP_OK)
				{
					SendCommandResponseData res_data = send_cmd_res.getSendCommandResponseData();
					if (res_data != null)
					{
						DeviceCmdResponse dev_cmd_res = res_data.getDevice_response();
						if (dev_cmd_res != null)
						{
							response = dev_cmd_res.getBody();
						}
					}
				}
			}
		} catch (SocketTimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return response;
	}
	
	
	/**
	 * Use for send some stun cmd: close_session, close_relay_session, close_relay_session2...
	 * @param urls mac, channelId, command, userName, userPass
	 * @return
	 */
	public static String send_stun_command(String... urls)
	{

		String command  = null, mac = null, http_userName = null, http_userPass = null; 
		String channelId = null; 
		
		mac = urls[0]; 
		channelId = urls[1]; 
		command = urls[2]; 
		http_userName = urls[3];
		http_userPass= urls[4];

		command = "action=" + command;
		// URL-Encoded
		command =  Uri.encode(command);
		if (mac.contains(":"))
		{
			mac = PublicDefine.strip_colon_from_mac(mac);
		}
		
		String http_addr =String.format("%1$s%2$s%3$s%4$s%5$s",
				PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART,
				PublicDefine.SEND_CTRL_CMD,
				PublicDefine.SEND_CTRL_PARAM_1 + mac,
				PublicDefine.SEND_CTRL_PARAM_2 + channelId,
				PublicDefine.SEND_CTRL_PARAM_3 + command
				) ;
		
		
		Log.d("mbp", "stun cmd: " + http_addr); 
		
		String response = HTTPRequestSendRecv.sendRequest_block_for_response_1(http_addr, http_userName,http_userPass);
		
		return response;
	}
	
	
	/**
	 * @param urls
	 * @return string version (1x_xxx or 0x_xxx) or null
	 */
	public static String getFirmwareVersionViaStun(String... urls)
	{
		String command  = null, mac = null,http_userName = null , http_userPass = null; 
		String channelId = null; 
		
		mac = urls[0]; 
		channelId = urls[1]; 
		command = urls[2]; 
		http_userName = urls[3];
		http_userPass= urls[4];
		
		//20121120: new way to send command
		command = "action=command&command=" + command;
		// URL-Encoded
		command =  Uri.encode(command);
		if (mac.contains(":"))
		{
			mac = PublicDefine.strip_colon_from_mac(mac);
		}
		
		String http_addr =String.format("%1$s%2$s%3$s%4$s%5$s",
				PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART,
				PublicDefine.SEND_CTRL_CMD,
				PublicDefine.SEND_CTRL_PARAM_1 + mac,
				PublicDefine.SEND_CTRL_PARAM_2 + channelId,
				PublicDefine.SEND_CTRL_PARAM_3 + command
				) ;

		
		String response = HTTPRequestSendRecv.sendRequest_block_for_response_1(http_addr, http_userName,http_userPass);
		final String str_version = "get_version: ";
		String result = null;
		
		if (response != null)
		{
			if (response.startsWith(str_version))
			{
				result = response.substring(str_version.length());
			}
		}
		
		return result;
	}
	

	/************ end of  STATIC METHODS ***************************/




	/************* Asyn Taks methods *****************************/

	/* background thread: post the type of request here 
	 * for e.g.: "action=command&command=brightness_plus"
	 * */
	
	protected String doInBackground(String... urls)  {
		String request = null; 
		String response = null;
		byte[] request_bytes = null; 

		if ( device_addr ==null)
		{
			Log.d("mbp","device_addr: = null");
			return null; 
		}
		
		/*
		try {
			udtSock = new SocketUDT(TypeUDT.STREAM);
			udtSock.setSoTimeout(5000);
			udtSock.connect(device_addr);
			//Log.d("mbp","num of req: "+ urls.length);

			for (int i =0; i < urls.length; i++)
			{
				request = urls[i];
				request_bytes = request.getBytes(); 
				udtSock.send(request_bytes); 

				InputStream is = udtSock.getUDTInputStream();
				if (is == null)
				{
					return null;
				}

				_inputStream = new DataInputStream(new BufferedInputStream(is));
				response = _inputStream.readLine();
			}

		} catch (ExceptionUDT e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/

		return response;
	}

	/* on UI Thread */
	protected void onProgressUpdate(Integer... progress) {
	}


	/* on UI Thread */
	protected void onPostExecute(String result) {
		Log.d("mbp","UDT:response: >"+result+"<");
		//TODO: parse result
	}

	/************* end of Asyn Taks methods *****************************/


	/************* Instance methods ******************************/

	/**
	 * sendCommand - instance method, use the current socket to to send -
	 * - at any time, there can only be one caller to this method - 
	 * @param urls[0] : request
	 *         
	 * optional - urls[1] : basic authentication user name 
	 *            urls[2] : basic authentication pass wrd 
	 *                   
	 * @return - String - reponse (could be null ) 
	 */
	public synchronized String sendCommand(String... urls) 
	{
		String request = null; 
		String response = null;
		byte[] request_bytes = null; 

		String usr = "";
		String pwd = "";
		if (urls.length == 3)
		{
			usr = urls[1];
			pwd = urls[2];
		}

		/* check for RELAY server */ 
		if (this.bm.getIP().equalsIgnoreCase(PublicDefine.RELAY_SERVER))
		{
			//OMG .. 
			Log.d("mbp","relayToken is: " + ((BabyMonitorUdtAuthentication)bm).getRelayToken());
			
			request = "##"+urls[0] +":::" + ((BabyMonitorUdtAuthentication)bm).getRelayToken();
		}
		else
		{
			request = urls[0];
		}
		
		
		
		if ( device_addr ==null)
		{
			Log.d("mbp","device_addr: = null");
			return null; 
		}
		
		
		/*
		//Log.d("mbp","sendCommand: "+  request);
		try {
			udtSock = new SocketUDT(TypeUDT.STREAM);
			//udtSock.setSoTimeout(5000);
			udtSock.bind(new InetSocketAddress(localPort));
			udtSock.connect(device_addr);
			
			request_bytes = request.getBytes(); 
			udtSock.send(request_bytes); 

			InputStream is = udtSock.getUDTInputStream();
			if (is == null)
			{
				return null;
			}

			_inputStream = new DataInputStream(new BufferedInputStream(is));
			response = _inputStream.readLine();

			udtSock.close();
		} catch (ExceptionUDT e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		*/

		return response;
	}

	
	public synchronized String sendCommandViaServer(String... urls) 
	{
		String request = null; 
		String response = null;
		
		String command  = null, mac = null,http_userName = null , http_userPass = null; 
		String channelId = null; 
		request = urls[0];

		mac = ((BabyMonitorUdtAuthentication)bm).getDeviceMac(); 
		channelId = ((BabyMonitorUdtAuthentication)bm).getChannelID(); 
		
		command = request; 
		http_userName = ((BabyMonitorUdtAuthentication)bm).getUser();
		http_userPass= ((BabyMonitorUdtAuthentication)bm).getPass();
		
		
		//20121120: new way to send command
		command = "action=command&command=" + command;
		// URL-Encoded
		command =  Uri.encode(command);


		String http_addr =String.format("%1$s%2$s%3$s%4$s%5$s",
				PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART,
				PublicDefine.SEND_CTRL_CMD,
				PublicDefine.SEND_CTRL_PARAM_1 + mac,
				PublicDefine.SEND_CTRL_PARAM_2 + channelId,
				PublicDefine.SEND_CTRL_PARAM_3 + command
				) ;

		response = HTTPRequestSendRecv.sendRequest_block_for_response_1(http_addr, http_userName,http_userPass, "3000");

		return response;
	}



	/************* end of Instance methods ******************************/


}
