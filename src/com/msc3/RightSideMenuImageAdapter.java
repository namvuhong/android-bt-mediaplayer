package com.msc3;


import com.blinkhd.R;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

public class RightSideMenuImageAdapter extends BaseAdapter {
    private Context mContext;
    private boolean  enableTalkback, enableMulti, enableMenu;
    
    /* TO BE UPDATED if the arrays below are modified */
    static public final int pos_menu = 0;
    static public final int pos_multi = 2;
    static public final int pos_mic = 4;

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.smallicon3_1, R.drawable.side_separator, 
            R.drawable.smallicon1_1, R.drawable.side_separator, 
            R.drawable.bb_vs_mike_on,           
    };
    
    
 // references to our images
    private Integer[] mThumbIds_disabled = {
            R.drawable.smallicon3_1_d,R.drawable.side_separator, 
            R.drawable.smallicon1_1_d,R.drawable.side_separator,
            R.drawable.bb_vs_mike_off, 
    };
    
    final float scale ;
    
    public RightSideMenuImageAdapter(Context c) {
        mContext = c;
        enableTalkback = enableMulti = true;
        enableMenu = true;
        scale = mContext.getResources().getDisplayMetrics().density;
    }
    
    public void toggleMulti(boolean enabled)
    {
    	enableMulti = enabled;
    	
    }
    
    public void toggleTalkback(boolean enabled)
    {
    	enableTalkback = enabled;
    }
    public void toggleMenu(boolean enabled)
    {
    	enableMenu= enabled;
    }
    
    public int getCount() {
        return mThumbIds.length ;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    
    
    
    public boolean areAllItemsEnabled()
    {
    	return false;
    }
    
    @Override
    public boolean isEnabled(int position)
    {
    	switch (position)
    	{
    	case RightSideMenuImageAdapter.pos_multi:
    		return enableMulti;
    	case RightSideMenuImageAdapter.pos_mic: 
    		return enableTalkback;
    	case RightSideMenuImageAdapter.pos_menu:
    		return true;
    	default:
    		return false;
    	}
    }
    

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        int width, height,padding;
        

        if ( (position % 2) == 1 )//ODD  position is separator
        {
    		int width_dp =108;//dp
			padding = 0;
			height = (int) (3 * scale + 0.5f);;
			width =(int) (width_dp * scale + 0.5f);

        	if (convertView == null) {  // if it's not recycled, initialize some attributes
        		imageView = new ImageView(mContext);

        		imageView.setLayoutParams(new GridView.LayoutParams(width,height));
        		imageView.setScaleType(ImageView.ScaleType.CENTER);
        		imageView.setPadding(padding,padding, padding, padding);


        	} else {
        		imageView = (ImageView) convertView;
        		
        		imageView.setLayoutParams(new GridView.LayoutParams(width,height));
        		imageView.setScaleType(ImageView.ScaleType.CENTER);
        		imageView.setPadding(padding,padding, padding, padding);

        	}
        	imageView.setImageResource(R.drawable.side_separator);

        }
        else /* normal items */ 
        {

        	/* 3 icons visible */
			//int height_dp = 84;//dp
			int padding_dp = 10;//dp 
			padding = (int) (padding_dp* scale + 0.5f);
			//Convert to system pixel -- this value should be different from screen to screen 
//			height =(int) (height_dp * scale + 0.5f);
//			width = height;
        	

        	if (convertView == null) {  // if it's not recycled, initialize some attributes
        		imageView = new ImageView(mContext);

        		//imageView.setLayoutParams(new GridView.LayoutParams(width,height));
        		imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        		imageView.setPadding(padding,padding, padding, padding);


        	} else {
        		imageView = (ImageView) convertView;
//        		imageView.setLayoutParams(new GridView.LayoutParams(width,height));
        		imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        		imageView.setPadding(padding,padding, padding, padding);
        	}

        	imageView.setImageResource(mThumbIds[position]);
        	imageView.setOnTouchListener(
        			new ButtonTouchListener(mContext.getResources().getDrawable(mThumbIds[position]),
        					mContext.getResources().getDrawable(mThumbIds_disabled[position])));


        	if ( !isEnabled(position))
        	{
        		imageView.setImageResource(mThumbIds_disabled[position]);
        		imageView.setOnTouchListener(null);
        	}

        	
        	if ( (position == RightSideMenuImageAdapter.pos_mic) && enableTalkback)
        	{
        		/* sth not right here ..but hack it */
        		TalkBackTouchListener tb = new TalkBackTouchListener(new Handler((Callback) mContext));

        		ButtonTouchListener bl = new ButtonTouchListener(mContext.getResources().getDrawable(mThumbIds[position]),
    					mContext.getResources().getDrawable(mThumbIds_disabled[position]));
        		bl.registerOnTouchListener(tb);
        		imageView.setOnTouchListener(bl);
        	}
        	
        }

        return imageView;
    }

   
}

