package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.http.protocol.HTTP;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.UDTRequestSendRecv;
import com.msc3.registration.LoginOrRegistrationActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;



/**
 * @author phung
 * 
 * @param  
 *         @[0]: portal Username
 *         @[1]: portal Password
 *         @[2]: camera mac address (with colon, i.e. 11:22:33:44:55:66)
 * 
 * 
 *  result: returned by postExcecute() 
 *  
 *   is a DUMMY authentication object. 
 *   This object only contains the device mac address and channelID other info are not usable. 
 * 		--> Check UDTVideoStreamer.java for detail
 * 
 * 
 */
public class ViewRemoteCamUdtRequestTask extends ViewRemoteCamRequestTask {

	
	private String macAddress;
	
	private String channelID; 
	private String enc_key; 
	private String usrName, usrPass; 
	private SSLContext ssl_context ;
	
	private static final String CHANNEL_ID = "ChannelID:";
	private static final int CHANNEL_ID_LEN = 12; 
	
	// test symmetric NAT server & port
	private final static  String SERVER_IP1 = "nat1.monitoreverywhere.com";
	private final static  String SERVER_IP2 = "nat2.monitoreverywhere.com";
	private final static  String NAT_DELIMITER = "::";
	private final static  int SERVER_PORT1 = 9999;
	private final static  int SERVER_PORT2 = 9999;
	
	//private Handler mHandler;
	private int server_err_code; 
	private boolean switch_to_relay;
	
	private Tracker tracker;
	private EasyTracker easyTracker;
	
	//retryUntilSuccess

	public ViewRemoteCamUdtRequestTask(Handler h, Context mContext) {
		super(h,mContext);
		mHandler = h;
		server_err_code = 404; 
		switch_to_relay = false;
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(mContext);
		tracker = easyTracker.getTracker();
	}
	
	

	@Override
	protected BabyMonitorAuthentication doInBackground(String... params) {

		usrName = params[0];
		usrPass = params[1];
		macAddress = params[2];
		macAddress= PublicDefine.strip_colon_from_mac(macAddress).toUpperCase();
		
		BabyMonitorUdtAuthentication auth = null;

		do 
		{
			// check symmetric NAT 
			if (isSymmetricNAT())
			{
				switch_to_relay = true;
				break;
			}
			
			//Step 1 : check if camera is ready
			int retry = 12; 
			do 
			{
				server_err_code = isCamReady();
				if ( server_err_code == HttpURLConnection.HTTP_OK)
				{
					break; 
				}
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (isCancelled())
				{
					Log.d("mbp", " ViewRemoteCamUdtRequestTask Task is cancelled " );
					break;
				}
			}
			while (retry -->0);
			
			
			if ( server_err_code != HttpURLConnection.HTTP_OK)
			{
				auth = null; 
			}
			else
			{

				 
				//Step 2: get channel id
				server_err_code = getChannelID();
				
				if (isCancelled())
				{
					Log.d("mbp", " ViewRemoteCamUdtRequestTask Task is cancelled " );
					break;
				}
				
				if (server_err_code != HttpURLConnection.HTTP_OK)
				{
					//do nothing 
					auth = null;
				}
				else
				{


					/*20120522: NOTES  - check UDTVideoStreamer.java for more details.
					 * 
					 *  Return a DUMMY babyMonitorUDTauth objec here. 
					 *  The only valid information is : Channel ID  & Mac address. 
					 *  
					 *  Inside Streamer, the actual query process will take place 
					 *    Query UDT server to get the session key, ip, port 
					 *    Query camera at the updated ip
					 *    Callback to EntryActivity to update camera ip,port for other usage , eg. melody, joystick
					 *    
					 * 
					 */
					auth = new BabyMonitorUdtAuthentication("127.0.0.1", "80", "ss_key","",80,macAddress);
					auth.setChannelID(channelID);
					//store encryption key 
					auth.setEncKey(enc_key); 
					auth.setUserPass(this.usrName, this.usrPass);

				}
			}


			

			//Step 3: test the connection 
			//Log.d("mbp", "DBG ---- connect to camera here -------------");
			//testConnectionToCamera(auth); 
			//Log.d("mbp", "//////////////SS CLOSED ////////////////////////");

			if (auth != null)
			{
				break; 
			}

		}
		while (retryUntilSuccess && !isCancelled());

		return auth;
	}

	/* UI thread */
	protected void onPostExecute(BabyMonitorAuthentication result)
	{
		Message m; 
		if (switch_to_relay == true)
		{
			mHandler.dispatchMessage(Message.obtain(mHandler,
					ViewRemoteCamRequestTask.MSG_VIEW_CAM_SWITCH_TO_RELAY, mHandler));
		}
		else
		{
			if (result != null)
			{
				m = Message.obtain(mHandler, 
						ViewRemoteCamRequestTask.MSG_VIEW_CAM_SUCCESS, result);
			}
			else
			{
				tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY, "View Remote Cam Request Failed",
						"STUN Cam Info Access Failed", null);
				m = Message.obtain(mHandler,
						ViewRemoteCamRequestTask.MSG_VIEW_CAM_FALIED,
						server_err_code, server_err_code);
			}
			mHandler.dispatchMessage(m);
		}
	}
	
	/**
	 * 
	 * Connect to HTTPs to get a channel id -- weird.. 
	 * 
	 * @return
	 */
	private int  getChannelID()
	{
		/*String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.GET_CHANNEL_ID_CMD;*/
		
		//Get security 
		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.GET_SECURITY_INFO + 
				PublicDefine.GET_SECURITY_INFO_PARAM_1 + usrName +
				PublicDefine.GET_SECURITY_INFO_PARAM_2 + usrPass;
		
		Log.d("mbp","getsec http" );//+ http_cmd);

		
		
		URL url = null;
		HttpsURLConnection conn = null;
		DataInputStream inputStream = null;
		String response = null;
		int respondeCode = -1;

		String usr_pass =  String.format("%s:%s", usrName, usrPass);

		try {
			url = new URL(http_cmd );
			conn = (HttpsURLConnection)url.openConnection();

			if (ssl_context!=null)
			{
				conn.setSSLSocketFactory(ssl_context.getSocketFactory());
			}

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(10000);


			respondeCode = conn.getResponseCode();
			Log.d("mbp","getsec http respondeCode: " + respondeCode);
			
			
			
			
			if (respondeCode == HttpURLConnection.HTTP_OK )
			{

				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream(),4*1024));
				response = inputStream.readLine();
				
				if (response != null && response.startsWith(CHANNEL_ID))
				{
					String response_part = response.substring(CHANNEL_ID.length(),CHANNEL_ID.length() + CHANNEL_ID_LEN);

					channelID = response_part;

					int sk_start = response.indexOf("Secret_key:") + "Secret_key:".length();
					enc_key = response.substring(sk_start);
					
				}
			}
			

		} catch (MalformedURLException e) {
			e.printStackTrace();
			respondeCode = -1; 
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
			se.printStackTrace();
			respondeCode = -1; 
		}
		catch (IOException e) {
			e.printStackTrace();
			
			respondeCode = -1; 
		}
		return respondeCode;
	}
	
	
	/* use this  for UDT camera ONLY 
	 */
	private int  isCamReady()
	{
		
//		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
//				PublicDefine.IS_CAM_AVAIL + 
//				PublicDefine.IS_CAM_AVIL_PARAM_1 +  macAddress;
		/*
		 * 20130521: hoang: change to is_cam_available_onload2
		 */
		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.IS_CAM_AVAILABLE_ONLOAD2_CMD + 
				PublicDefine.IS_CAM_AVAILABLE_ONLOAD2_PARAM_1 +  macAddress;

		//Log.d("mbp","REMOVE B4 RELEASE: getsec http: " + http_cmd);

		
		
		URL url = null;
		HttpsURLConnection conn = null;
		DataInputStream inputStream = null;
		String response = null;
		int respondeCode = -1;

		String usr_pass =  String.format("%s:%s", usrName, usrPass);

		try {
			url = new URL(http_cmd );
			conn = (HttpsURLConnection)url.openConnection();

			if (ssl_context!=null)
			{
				conn.setSSLSocketFactory(ssl_context.getSocketFactory());
			}

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(20000);
			respondeCode = conn.getResponseCode();
			Log.d("mbp","iSCamReady respondeCode: " + respondeCode);
			

		} catch (MalformedURLException e) {
			e.printStackTrace();
			respondeCode = -1; 
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
			se.printStackTrace();
			respondeCode = -1; 
		}
		catch (IOException e) {
			e.printStackTrace();
			
			respondeCode = -1; 
		}
		return respondeCode;
	}
	
	
	/**
	 * @return true if phone is in symmetric NAT
	 * @return false otherwise
	 */
	private boolean isSymmetricNAT() {
		boolean result = true;
		/*
		SymmetricTest sym_test = new SymmetricTest();
		String response_str1 = sym_test.doNATTest(SERVER_IP1, SERVER_PORT1);
//		Log.d("mbp", "### response server 1: " + response_str1);
		String response_str2 = sym_test.doNATTest(SERVER_IP2, SERVER_PORT2);
//		Log.d("mbp", "### response server 2: " + response_str2);

		if( (response_str1 != null) && (response_str2 != null) )
		{
			String[] response_str_arr1 = response_str1.split(NAT_DELIMITER);
			String[] response_str_arr2 = response_str2.split(NAT_DELIMITER);
			if ( (response_str_arr1 != null) && (response_str_arr1.length == 4) &&
					(response_str_arr2 != null) && (response_str_arr2.length == 4) )
			{
				if (response_str_arr1[1].contentEquals(response_str_arr2[1])
						&& response_str_arr1[2].contentEquals(response_str_arr2[2]))
				{
					result = false;
				}
			}
		}
		*/
		return result;
	}

}




