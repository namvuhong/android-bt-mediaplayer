package com.msc3.registration;

import com.blinkhd.FontManager;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class RegularCustomFontEditText extends EditText
{

	public RegularCustomFontEditText(Context context)
	{
		super(context);
		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontEditText(Context context, AttributeSet attrs,
	        int defStyle)
	{
		super(context, attrs, defStyle);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

}
