package com.msc3.registration;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import javax.net.ssl.SSLContext;

import com.msc3.BabyMonitorRelayAuthentication;
import com.msc3.CamChannel;
import com.msc3.CamProfile;

import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.blinkhd.MBPActivity;
import com.blinkhd.R;
import com.blinkhd.SupportModel;
import com.msc3.StorageException;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CreateSessionKeyResponse2;
import com.nxcomm.meapi.device.CreateSessionKeyResponse2.CreateSessionKeyResponseData2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.WindowManager.BadTokenException;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/* ASSUM USER logged in is done in the registration screen 
 *  Once inside this activity, it means user has tried to log in 
 *  
 *  It could failed due to no internet connection.
 *  
 *  Requirement user/pass and bool_isLoggedIn has to be set
 */
public class UserLoginActivity extends Activity {

	private static final int DIALOG_CONTACTING_BMS_SERVER = 1;
	private static final int DIALOG_CONTACTING_BMS_SERVER_FAILED = 2;
	private static final int DIALOG_CONTACTING_BMS_SERVER_FAILED_DOT = 3;
	private static final int DIALOG_STORAGE_UNAVAILABLE = 4;

	public static final String bool_isLoggedIn= "isLoggedIn";

	public static final String STR_USER_TOKEN = "userToken";
	
	//MBP_2k,1k ONLY: Set when user have added one camera successfully 
	public static final String str_NewCamMac = "string_newCamMac";
	private SSLContext ssl_context;
	private String usrToken;
	private AnimationDrawable anim ;
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* added to force url*/
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings.getString(PublicDefine.PREFS_SAVED_SERVER_URL,
				"https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;
		

		String phoneModel = android.os.Build.MODEL;
		// register if it's not iHome Phone
		if(SupportModel.doesPhoneModelSupportGCM(phoneModel)) 
		{	
			registerWithGCM(); 
		}
	}

	protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
	}

	protected void onStart() {

		super.onStart();


		/* read the keystore and create the ssl context for HTTPS connections*/
		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(this);

		/*20120208:issue 95 go to connect to server directly*/
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		boolean shouldAutoConnect = settings.getBoolean(PublicDefine.PREFS_AUTO_SHOW_CAM_LIST, false);
		/* if we have to "connectToserver" now, there is no need to layout 
		 * 
		 * Otherwise, do the layout*/

		/*20120228: another great change from some bull head .. 
		 * - Force go to camlist all the time*/ 
		if (shouldAutoConnect == false)
		{
			shouldAutoConnect = true;
			SharedPreferences.Editor editor = settings.edit();

			editor.putBoolean(PublicDefine.PREFS_AUTO_SHOW_CAM_LIST, shouldAutoConnect );
			editor.commit();

		}

//		setContentView(R.layout.bb_is_waiting_screen);
		Log.i("SETUP", "B");
		setContentView(R.layout.bb_is_first_screen);
		
		ImageView loader = (ImageView) findViewById(R.id.imageLoader);
		loader.setBackgroundResource(R.drawable.loader_anim1);
		anim= (AnimationDrawable) loader.getBackground();
		
		Bundle extra = getIntent().getExtras();
		if (extra != null)
		{
			boolean isLoggedIn = extra.getBoolean(bool_isLoggedIn);
			if (isLoggedIn)
			{
				usrToken = extra.getString(STR_USER_TOKEN);
				
				
				Runnable worker= new Runnable() {

					@Override
					public void run() {
						UserAccount online_user;
						try {
							online_user = new UserAccount(usrToken,
									getExternalFilesDir(null), ssl_context, UserLoginActivity.this );
						} catch (StorageException e1) {
							Log.d("mbp", e1.getLocalizedMessage());
							showDialog(DIALOG_STORAGE_UNAVAILABLE);
							return;
						}
						//Blocking call 
						try {
							online_user.sync_user_data();
						}
						catch (FunnyException fe)
						{
							UserLoginActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									try
									{
										showDialog(DIALOG_CONTACTING_BMS_SERVER_FAILED_DOT);
									}
									catch(IllegalArgumentException ie)
									{

									}
									catch (BadTokenException be)
									{

									}
								}
							});
							return; 
						}
						catch (IOException e) {

							Log.d("mbp","catched:  " + e.getLocalizedMessage());

							UserLoginActivity.this.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									try
									{
										showDialog(DIALOG_CONTACTING_BMS_SERVER_FAILED);
									}
									catch(IllegalArgumentException ie)
									{

									}
									catch (BadTokenException be)
									{

									}
								}
							});


							return; 

						}
						
						
						UserLoginActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								try
								{
									dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
								}
								catch(IllegalArgumentException ie)
								{

								}
								catch (BadTokenException be)
								{

								}
								

								/*20120208:issue 95 go to connect to server directly*/
								SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
								boolean shouldAutoConnect = settings.getBoolean(PublicDefine.PREFS_AUTO_SHOW_CAM_LIST, false);
								if (shouldAutoConnect == true)
								{
									SharedPreferences.Editor editor = settings.edit();
									editor.remove(PublicDefine.PREFS_AUTO_SHOW_CAM_LIST);
									editor.commit();
									
									Intent entry = new Intent(UserLoginActivity.this, HomeScreenActivity.class);
									entry.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									UserLoginActivity.this.startActivity(entry);
									UserLoginActivity.this.finish();
								}
							}
						});
					}
				};

				new Thread(worker,"Worker Thread").start();

				//showDialog(DIALOG_CONTACTING_BMS_SERVER);
			}
			else
			{
				/* 20120317 - Offline mode supported 
				Log.d("mbp", " ENTER OFFLINE MODE --");

				Intent entry = new Intent(UserLoginActivity.this, DashBoardActivity.class);
				entry.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				UserLoginActivity.this.startActivity(entry);
				UserLoginActivity.this.finish();*/
			}
		}
	}

	@Override
	public void onWindowFocusChanged (boolean hasFocus)
	{
		if (hasFocus == true && anim != null)
		{
			//opt2
			anim.start();
		}
		else if (hasFocus == false && anim!= null)
		{
			//opt2
			//anim.cancel();
			//anim.stop(); 
		}
	}
	
	protected Dialog onCreateDialog(int id)
	{
		ProgressDialog dialog;
		AlertDialog.Builder builder;
		AlertDialog alert;
		switch (id)
		{
		case DIALOG_CONTACTING_BMS_SERVER:
			dialog = new ProgressDialog(this);
			Spanned msg = Html.fromHtml("<big>"+getResources().getString(R.string.UserLoginActivity_updating)+"</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;
		case DIALOG_CONTACTING_BMS_SERVER_FAILED:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.there_are_some_problems_contacting_the_server_please_try_again_later_)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent initialSetup = new Intent(UserLoginActivity.this, FirstTimeActivity.class);
					initialSetup.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(initialSetup);
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_CONTACTING_BMS_SERVER_FAILED_DOT:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.there_are_some_problems_contacting_the_server_please_try_again_later_)+".</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent initialSetup = new Intent(UserLoginActivity.this, FirstTimeActivity.class);
					initialSetup.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(initialSetup);
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					Intent homeScreen = new Intent(
							UserLoginActivity.this,
							FirstTimeActivity.class);
					homeScreen
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeScreen);
					UserLoginActivity.this.finish();
				}
			});

			alert = builder.create();
			return alert;
		default:
			break;
		}
		return null;
	}
	
	private void registerWithGCM()
	{
		Log.d("mbp", " Start Registering with GCM RESET the flag :" + MBPActivity.GOOGLE_API_PROJECT_ID);
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_PUSH_REG_STATUS, false);
		editor.commit(); 
		
		
		/**** register with GCM *****/
		
		Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
		// sets the app name in the intent
		registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0));
		
		/* 
		 * The Android application that is registering to receive messages. The Android application 
		 * is identified by the package name from the manifest. This ensures that the messages are 
		 * targeted to the correct Android application.
		 */
		registrationIntent.putExtra("sender",MBPActivity.GOOGLE_API_PROJECT_ID);
		startService(registrationIntent);
		///*********** END  *****/
	}
	

}
