package com.msc3.registration;

import com.blinkhd.R;
import java.util.ArrayList;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.msc3.NameAndSecurity;

public class AccessPointAdapter extends BaseAdapter
{

	private Context mContext;
	private  ArrayList<NameAndSecurity>ap_list;
	private int selectedPosition = -1;

	
//	private int wifi_icons[] =
//		{
//			R.drawable.wifi_0_r_icon, R.drawable.wifi_1_r_icon,
//			R.drawable.wifi_2_r_icon,R.drawable.wifi_3_r_icon,
//			R.drawable.wifi_full_r_icon
//		};

	public AccessPointAdapter(Context c, ArrayList<NameAndSecurity>ap_list) {
		mContext = c;
		this.ap_list = ap_list;
	}


	@Override
	public int getCount() {
		if ( ap_list != null)
			//return ap_list.size() + 1;// extra item for "Add wifi network" option
			return ap_list.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int arg0) {
		if (arg0 >=0 && arg0 <ap_list.size())
			return ap_list.get(arg0);
		else
			return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	public void setSelectedPositision(int position)
	{
		selectedPosition = position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		LinearLayout itemView = null;
		ImageView imageView = null;
		NameAndSecurity ap = null;

		/* Stat setting ups the view */
		if (convertView == null) {  // if it's not recycled, initialize some attributes
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService
					(Context.LAYOUT_INFLATER_SERVICE);
			itemView = (LinearLayout) inflater.inflate(R.layout.bb_access_point_list_item_w_rssid, null);

		} else {
			itemView = (LinearLayout) convertView;
		}
		
		ImageView checked = (ImageView) itemView.findViewById(R.id.imgChecked);
		if (position == selectedPosition)
		{
			checked.setVisibility(View.VISIBLE);
		}
		else
		{
			checked.setVisibility(View.INVISIBLE);
		}
		
//		if (position == ap_list.size()) 
//		{
//			//Add wifi network
//			
//			itemView.setVisibility(View.VISIBLE);
//
//
//			TextView text = (TextView) itemView.findViewById(R.id.AccessPointItem);
//			if (text != null)
//			{
//				text.setText(mContext.getString(R.string.add_wi_fi_network));
//			}
//
//			
//			
//			imageView = (ImageView) itemView.findViewById(R.id.imageSS);
//			imageView.setVisibility(View.INVISIBLE); 
//
//		}
//		else
		{

			ap = ap_list.get(position);
			if (ap == null)
			{
				itemView.setClickable(false);
				itemView.setVisibility(View.INVISIBLE);
				return itemView;
			}


			itemView.setVisibility(View.VISIBLE);


			TextView text = (TextView) itemView.findViewById(R.id.AccessPointItem);
			if (text != null)
			{
				text.setText(ap.toString());
			}

//			imageView = (ImageView) itemView.findViewById(R.id.imageSS);
//			int cur_level =WifiManager.calculateSignalLevel( ap.getLevel(), 5);
//			//Update the current WIFI level
//			imageView.setImageResource(wifi_icons[cur_level]);

		}

		return itemView;
	}

}