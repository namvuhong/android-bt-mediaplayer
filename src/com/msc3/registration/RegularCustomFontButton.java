package com.msc3.registration;

import com.blinkhd.FontManager;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class RegularCustomFontButton extends Button
{

	public RegularCustomFontButton(Context context)
	{
		super(context);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontButton(Context context, AttributeSet attrs,
	        int defStyle)
	{
		super(context, attrs, defStyle);
		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

}
