package com.msc3.registration;

import com.blinkhd.FontManager;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RegularCustomFontTextView extends TextView
{
	public RegularCustomFontTextView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontTextView(Context context, AttributeSet attrs,
	        int defStyle)
	{
		super(context, attrs, defStyle);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontTextView(Context context)
	{
		super(context);

		if (isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}
}
