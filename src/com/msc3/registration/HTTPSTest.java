package com.msc3.registration;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.util.Log;

public class HTTPSTest {

	/**
	 * Trust every server - dont check for any certificate
	 */
	private  void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
				Log.d("mbp","checkClientTrusted()auth: "+ authType );
				Log.d("mbp","checkClientTrusted()chain: "+ chain.toString() );
			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {

				Log.d("mbp","checkServerTrusted()auth: "+ authType );
				Log.d("mbp","checkServerTrusted()chain: "+ chain.toString() );
			}
		} };

		
		
		
		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void test_https (URL url) throws IOException
	{
		HttpURLConnection http = null;

		if (url.getProtocol().toLowerCase().equals("https")) {
			//trustAllHosts();
			HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
			//https.setHostnameVerifier(DO_NOT_VERIFY);
			
			https.setHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			http = https;
		} else {
			http = (HttpURLConnection) url.openConnection();
		}

		http.connect();
		
		Log.d("mbp",this.getClass().getName() + " aaresponse: " + http.getResponseCode());


	}



}

