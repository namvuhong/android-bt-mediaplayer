package com.msc3.registration;


import com.blinkhd.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class TermOfUseActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.bb_is_term_of_use_screen);

		TextView textTitle = (TextView) findViewById(R.id.textTitle);
		textTitle.setText(getResources().getString(R.string.terms_and_conditions));


		WebView terms = (WebView) findViewById(R.id.webView1);
		if (terms != null)
		{
			terms.loadUrl("file:///android_asset/terms_of_use.html");
		}

		setResult(RESULT_CANCELED, getIntent());

		Button iAgree = (Button) findViewById(R.id.button1);
		iAgree.getBackground().setColorFilter(0xff8eca00, android.graphics.PorterDuff.Mode.MULTIPLY);
		iAgree.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				setResult(RESULT_OK, getIntent());
				finish();
			}

		});
		

		// button Cancel
		Button btC = (Button) findViewById(R.id.buttonCancel);
		btC.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//Go back to login screen
				setResult(RESULT_CANCELED, getIntent());
				finish();
			}
		});
	}
	
}
