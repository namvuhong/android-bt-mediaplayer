package com.msc3.registration;

import com.blinkhd.R;
import com.blinkhd.gcm.GcmIntentService;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.msc3.CameraConnectivityDetector;
import com.msc3.CameraDetectorService;
import com.msc3.CameraMenuActivity;
import com.msc3.ChangeNameTask;
import com.msc3.LogCollectorTask;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.VoiceActivationService;
import com.msc3.update.CheckAppVersion;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.jstun_android.RtspStunBridgeService;
import com.nxcomm.meapi.PublicDefines;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class FirstTimeActivity extends Activity {

	public static final String bool_DirectModeShowInfoAndConnect = "directModeShowInfoAndConnect";
	/* first time setup will set this flag */
	public static final String bool_InfraModeInitialSetup = "InfraModeInitialSetup";

	public static final String bool_InfraMode = "InfraMode";
	public static final String bool_ExitToIdleScreen = "CloseAppAndReturn";

	public static final String bool_VoxDirectMode="VoxDirectMode";
	public static final String bool_VoxInfraMode="VoxInfraMode";
	public static final String string_VoxDeviceAddr="VoxDeviceAddr";
	
	private static final int DIALOG_STORAGE_UNAVAILABLE = 1;

	CheckAppVersion checkVersiontask ; 

	Handler.Callback versionCB = new  Handler.Callback ()
	{
		public boolean handleMessage(Message arg0)
		{
			//Don't need to do anything special here.. the dialog is popep up by CheckAppVersion task
			switch (arg0.what)
			{
			case CheckAppVersion.NO_UPDATE:
				// Check bundle  & call different functions... 
				proceed(); 
				break;
			case CheckAppVersion.NEW_APP_AVAILABLE:
				
				//.. User click Yes.. -> app will be closed anyway. So don't do anything here.. 
				break;
			default:
				break; 
			}

			//set it to null: check only once everytime startup 
			checkVersiontask = null; 
			return false;
		}

	};

	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		checkVersiontask = null;
		/* query server for versions- everytime activity is started 
		//--- CheckAppversion--- callback --- proceed() --...
		checkVersiontask  = new CheckAppVersion(this,new Handler(versionCB ) );
		checkVersiontask.execute("now");*/

		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) ||
				phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			if (isCamDetectServiceRunning(this))
			{
				Log.d("mbp", "Stop Cam Detector");
				Intent i = new Intent(this,CameraDetectorService.class);
				stopService(i);
			}

			/*20121204: phung: clear all disconnect alerts */ 
			NotificationManager notificationManager = (NotificationManager)
					getSystemService(Context.NOTIFICATION_SERVICE);

			for (int i =0; i< CameraConnectivityDetector.notificationIds.length; i++)
			{
				notificationManager.cancel(CameraConnectivityDetector.notificationIds[i]);
			}
		}
	}

	/* 20130305: hoang:
	 * @see android.app.Activity#onCreateDialog(int)
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		Spanned msg;
		switch (id) {
		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					
					FirstTimeActivity.this.finish();
				}
			});

			alert = builder.create();
			return alert;
		}
		return super.onCreateDialog(id);
	}
	
	protected void onNewIntent (Intent intent)
	{
		this.setIntent(intent);

	}

	protected void onPause() {
		super.onPause();
	}


	protected void onStop() {
		
		super.onStop();
	}
	protected void onRestart() {
		super.onRestart();
	}


	protected void onResume()
	{
		super.onResume();
	}
	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		//Do nothing
		Log.d("mbp", "onConfigurationChanged- newconf: "+ newConfig);
	}

	protected void onStart() {
		super.onStart();
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings.getString(PublicDefine.PREFS_SAVED_SERVER_URL,
				"https://api.hubble.in/v1");
		PublicDefines.SERVER_URL = serverUrl;
		
		setContentView(R.layout.bb_is_waiting_screen);
		Log.d("mbp", "First Time on Start "); 
		if (checkVersiontask == null)
		{
			Log.d("mbp", "proceed 1... "); 
			proceed();
		}
	}
	
	private void proceed() 
	{
		/* We may be waken up by some other intent */
		Bundle extra = getIntent().getExtras();
		if (extra != null)
		{
			boolean goDirectOrInfra = extra.getBoolean(bool_DirectModeShowInfoAndConnect);

			goDirectOrInfra = extra.getBoolean(bool_InfraMode);
			if (goDirectOrInfra)
			{
				getIntent().removeExtra(bool_InfraMode);
				Intent entry = new Intent(FirstTimeActivity.this,LoginOrRegistrationActivity.class);
				FirstTimeActivity.this.startActivity(entry);
				return;
			}

			goDirectOrInfra = extra.getBoolean(bool_InfraModeInitialSetup);
			if (goDirectOrInfra)
			{

				//20131218:phung: change flow, initial just show Login screen , if user does not have any account
				//    they will need to click "Create Account"
				getIntent().removeExtra(bool_InfraModeInitialSetup);

				
//				Intent entry = new Intent(FirstTimeActivity.this, LoginOrRegistrationActivity.class);
//				entry.putExtra(LoginOrRegistrationActivity.bool_createUserAccount, true); 
//				FirstTimeActivity.this.startActivity(entry);
				
				final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
				dialog.setContentView(R.layout.bb_server_url_new_name);
				dialog.setCancelable(true);


				//setup connect button
				Button connect = (Button) dialog.findViewById(R.id.change_btn);
				connect.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						EditText text = (EditText) dialog.findViewById(R.id.text_new_name);
						if (text != null)
						{
							String serverUrl = "https://" + text.getText().toString().trim() + "/v1";
							if (!serverUrl.isEmpty())
							{
								Log.d("mbp", "New server URL: " + serverUrl);
								PublicDefines.SERVER_URL = serverUrl;
								SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
								Editor editor = settings.edit();
								editor.putString(PublicDefine.PREFS_SAVED_SERVER_URL, serverUrl);
								editor.commit();
							}
						}
						dialog.cancel();
					}
				});


				Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});

				dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						//GO to first time setup 
						Intent entry = new Intent(FirstTimeActivity.this, LoginOrRegistrationActivity.class);
						entry.putExtra(LoginOrRegistrationActivity.bool_createUserAccount, true); 
						FirstTimeActivity.this.startActivity(entry);
					}
				});
				dialog.show();

				return;
			}

			boolean exit = extra.getBoolean(bool_ExitToIdleScreen);
			if (exit)
			{
				finish();
				return;
			}

			/*20120913: phung: vox here will just show the camera list 
			 *  extra string_VoxDeviceAddr is useless for now. 
			 *  
			 *  NEW: If there is no OFFLine data here we launch LoginOrRegistrationActivity instead
			 *  
			 */
			boolean voxInfra = extra.getBoolean(bool_VoxInfraMode);
			if (voxInfra)
			{
				getIntent().removeExtra(bool_VoxInfraMode);
				String voxDevice = extra.getString(string_VoxDeviceAddr);
				getIntent().removeExtra(string_VoxDeviceAddr);
				
				SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				Editor editor = settings.edit();
				editor.putString(
				        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING, voxDevice);
				editor.commit();

				//Try to restore data
				try {
					if (new SetupData().restore_session_data(getExternalFilesDir(null)))
					{
						Intent entry_intent = new Intent(FirstTimeActivity.this, HomeScreenActivity.class);
						//entry_intent.putExtra(EntryActivity.string_voxDeviceAddr, voxDevice);
						entry_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(entry_intent);
					}
					else
					{
						Log.d("mbp", "Can't restore offline file  -> Login page instead");
						Intent entry = new Intent(FirstTimeActivity.this,LoginOrRegistrationActivity.class);
						FirstTimeActivity.this.startActivity(entry);
					}
				} catch (StorageException e) {
					Log.d("mbp", e.getLocalizedMessage());
					showDialog(DIALOG_STORAGE_UNAVAILABLE);
					return;

				}


			}



		}
		else
		{
			//main exit point
			Log.d("mbp", "FirstTimeAct: NOthing to do .. exit");
			finish(); 
		}
		

	}
	protected void onDestroy()
	{
		super.onDestroy();


		/*20120726: issue 450 - stop vox if app is close */ 

		/*20121116: issue 714 - Stop vox only if user logout 
		 * 
		if (LoginOrRegistrationActivity.isVOXServiceRunning(FirstTimeActivity.this))
		{
			Log.d("mbp", "ARGG Stop service when app close");
			Intent i = new Intent(this,VoiceActivationService.class);
			stopService(i);
		}

		 */
		
		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) ||
				phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			if (!isCamDetectServiceRunning(this))
			{
				Log.d("mbp", "Start Cam Detector");
				Intent i = new Intent(this,CameraDetectorService.class);
				startService(i);
			}
		}


	}
	
	public static boolean isCamDetectServiceRunning(Context c) {
		ActivityManager manager = (ActivityManager) c.getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.msc3.CameraDetectorService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}


}

