package com.msc3.registration;

import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.blinkhd.R;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.AutoConfigureCameras;
import com.msc3.CamConfiguration;
import com.msc3.CamProfile;
import com.msc3.CameraPassword;
import com.msc3.ChangeNameTask;
import com.msc3.ConnectToNetwork;
import com.msc3.FadeOutAnimationAndGoneListener;
import com.msc3.IChangeNameCallBack;
import com.msc3.IWifiScanUpdater;
import com.msc3.NameAndSecurity;
import com.msc3.PublicDefine;
import com.msc3.StorageException;
import com.msc3.VerifyNetworkKeyTask;
import com.msc3.WifiScan;
import com.msc3.comm.HTTPRequestSendRecv;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.jstun_android.RtspStunBridgeService;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;
import com.nxcomm.meapi.device.AddNewDeviceResponse;

/**
 * @author phung
 * 
 */
public class SingleCamConfigureActivity extends Activity implements
        IWifiScanUpdater, Handler.Callback, IChangeNameCallBack
{

	public static final String	str_userName	                             = "usr";
	public static final String	str_userPass	                             = "pwd";
	public static final String	str_userToken	                             = "token";

	public static final String	ACTION_TURN_WIFI_NOTIF_ON_OFF	             = "com.msc3.SingleCamConfigureActivity.ACTION_TURN_OFF_WIFI_NOTIF";
	public static final String	WIFI_NOTIF_BOOL_VALUE	                     = "wifi_notif_bool_value";
	public static final boolean	TURN_OFF	                                 = false;
	public static final boolean	TURN_ON	                                     = true;

	public static final String	GA_ADD_CAMERA_CATEGORY	                     = "Add Camera";

	private static final int	SCAN_FOR_CAMERA	                             = 0x100;
	private static final int	SCAN_FOR_ALL	                             = 0x101;

	private static final int	CONNECTING_DIALOG	                         = 0;
	private static final int	ASK_WEP_KEY_DIALOG	                         = 1;
	private static final int	ASK_WPA_KEY_DIALOG	                         = 2;
	private static final int	ROUTER_CONNECTION_FAILED	                 = 3;
	private static final int	VERIFY_KEY_DIALOG	                         = 4;
	private static final int	CONTACTING_BMS_SERVER_DIALOG	             = 5;
	private static final int	CONFIG_CAMERA_FAILED_DIALOG	                 = 6;
	private static final int	CONNECT_TO_CAMERA_NETWORK_FAILED_DIALOG	     = 7;
	private static final int	ADD_TO_CAMERA_NETWORK_FAILED_DIALOG	         = 8;
	private static final int	VERIFY_CAM_PASS_DIALOG	                     = 9;
	private static final int	CONNECTING_TO_HOME_DIALOG	                 = 10;
	private static final int	DIALOG_FAILED_TO_CONNECT_TO_CAM_NW_RESCAN	 = 11;
	private static final int	DIALOG_STORAGE_UNAVAILABLE	                 = 12;
	private static final int	ROUTER_CONNECTION_FAILED_RETRY	             = 13;
	private static final int	DIALOG_RENAMING_CAMERA	                     = 14;
	private static final int	DIALOG_FAILED_TO_FIND_CAM	                 = 15;
	private static final int	DIALOG_ADD_WIFI_NETWORK	                     = 16;
	private static final int	DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY	= 17;
	private static final int	DIALOG_CAMERA_NAME_IS_INVALID	             = 18;
	private static final int	DIALOG_SEARCHING_CAMERA	                     = 19;
	private static final int	DIALOG_SEARCHING_NETWORK	                 = 20;

	private int	                wifi_scan_purpose;

	/* Transient data - Only valid during the Configuration process */
	private WifiConfiguration	selectedWifiCon;

	private String	            user_token;
	private CamConfiguration	configuration;

	private boolean	            isOnFirstPage	                             = false;

	private String	            saved_ssid;

	private CamProfile	        cam_profile;

	private String	            cam_codec;

//	private Tracker	            tracker;
//	private EasyTracker	        easyTracker;

	/******/

	private SSLContext	        ssl_context;
	private boolean	            g_should_relogin;

	private AnimationDrawable anim = null;
	private AutoConfigureCameras config_task = null;

	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

//		easyTracker = EasyTracker.getInstance();
//		easyTracker.setContext(SingleCamConfigureActivity.this);
//		tracker = easyTracker.getTracker();
		
		
		ConnectivityManager connManager  = (ConnectivityManager)getSystemService(
				Context.CONNECTIVITY_SERVICE);
		Method dataMtd = null;
        try
        {
	        dataMtd = ConnectivityManager.class.getDeclaredMethod(
	        		"setMobileDataEnabled", boolean.class);
	        dataMtd.setAccessible(true);
			dataMtd.invoke(connManager, false);
			
			NetworkInfo mobileInfo = connManager.getNetworkInfo(
					ConnectivityManager.TYPE_MOBILE);
        }
        catch (NoSuchMethodException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (InvocationTargetException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		

		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(this);

		Bundle extra = getIntent().getExtras();
		if (extra != null)
		{
			// user_name = extra.getString(str_userName);
			// user_pass = extra.getString(str_userPass);
			user_token = extra.getString(str_userToken);
			extra.remove(str_userToken);
			// extra.remove(str_userName);
			// extra.remove(str_userPass);

		}

		//Intent i = new Intent(this.getApplicationContext(), RtspStunBridgeService.class);
		//stopService(i);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setup_camera_scan();
		g_should_relogin = true;
	}

	protected void onDestroy()
	{
		super.onDestroy();
		/*
		 * 20130415: hoang: turn on wifi connectivity checking while adding
		 * camera successfully.
		 */
		Intent i = new Intent();
		i.setAction(ACTION_TURN_WIFI_NOTIF_ON_OFF);
		i.putExtra(WIFI_NOTIF_BOOL_VALUE, TURN_ON);
		sendBroadcast(i);

		/*
		 * 20130201: hoang: issue 1260 release wakelock
		 */
		if ((wl != null) && (wl.isHeld()))
		{
			wl.release();
			wl = null;
			Log.d("mbp", "SingleCamConfigureAct onDestroy - release WakeLock");
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
		
		if (config_task != null && 
				config_task.getStatus() == AsyncTask.Status.RUNNING)
		{
			config_task.cancel(true);
		}

		stopAutoRefresh();
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		check_and_reconnect_to_home();
		
		ConnectivityManager connManager  = (ConnectivityManager)getSystemService(
				Context.CONNECTIVITY_SERVICE);
		Method dataMtd = null;
        try
        {
	        dataMtd = ConnectivityManager.class.getDeclaredMethod(
	        		"setMobileDataEnabled", boolean.class);
	        dataMtd.setAccessible(true);
			dataMtd.invoke(connManager, true);
			
			NetworkInfo mobileInfo = connManager.getNetworkInfo(
					ConnectivityManager.TYPE_MOBILE);
        }
        catch (NoSuchMethodException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (IllegalAccessException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
        catch (InvocationTargetException e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		
		//Intent it = new Intent(this.getApplicationContext(), RtspStunBridgeService.class);
		//startService(it);

		if (g_should_relogin == true)
		{
			Intent intent = new Intent(SingleCamConfigureActivity.this,
			        HomeScreenActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}

	}

	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

		if (isOnFirstPage == true)
		{
			setup_camera_scan();
		}

	}

	protected Dialog onCreateDialog(int id)
	{
		final Dialog dialog;
		switch (id)
		{
		// WifiScan_1
		case DIALOG_SEARCHING_NETWORK:
			final Dialog scan_dialog = new Dialog(this,
			        R.style.CustomAlertDialogStyle);
			scan_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			Log.i("SingleCamConfigureActivity", "O");
			scan_dialog.setContentView(R.layout.dialog_wait_for_connecting);
			ImageView loader = (ImageView) scan_dialog
			        .findViewById(R.id.imgLoader);
			if (loader != null)
			{
				loader.setBackgroundResource(R.drawable.loader_anim1);
				anim = (AnimationDrawable) loader.getBackground();
				anim.start();
			}

			TextView txtWaiting = (TextView) scan_dialog
			        .findViewById(R.id.txtLoaderDesc);
			txtWaiting.setText(R.string.WifiScan_1);
			scan_dialog.setCancelable(true);
			scan_dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					// TODO Auto-generated method stub
					ImageView loader = (ImageView) scan_dialog
					        .findViewById(R.id.imgLoader);
					if (loader != null)
					{
						loader.clearAnimation();
					}
				}
			});

			return scan_dialog;

		case DIALOG_SEARCHING_CAMERA:

			final Dialog searching_dialog = new Dialog(this,
			        R.style.CustomAlertDialogStyle);
			searching_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			Log.i("SingleCamConfigureActivity", "P");
			searching_dialog
			        .setContentView(R.layout.dialog_wait_for_connecting);
			loader = (ImageView) searching_dialog.findViewById(R.id.imgLoader);
			if (loader != null)
			{
				loader.setBackgroundResource(R.drawable.loader_anim1);
				anim = (AnimationDrawable) loader.getBackground();
				anim.start();
			}

			txtWaiting = (TextView) searching_dialog
			        .findViewById(R.id.txtLoaderDesc);
			txtWaiting.setText(R.string.ManualDirectConnectActivity_search_cam);
			searching_dialog.setCancelable(true);
			searching_dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					// TODO Auto-generated method stub
					ImageView loader = (ImageView) searching_dialog
					        .findViewById(R.id.imgLoader);
					if (loader != null)
					{
						loader.clearAnimation();
					}

					stopAutoRefresh();
				}
			});
			searching_dialog.setOnDismissListener(new OnDismissListener()
			{

				@Override
				public void onDismiss(DialogInterface dialog)
				{
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			return searching_dialog;

		case CONNECTING_DIALOG:
			dialog = new ProgressDialog(this);
			Spanned msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.SingleCamConfigureActivity_connecting)
			        + "</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog) dialog).setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{

				}
			});

			((AlertDialog) dialog).setButton(
			        getResources().getString(R.string.Cancel),
			        new DialogInterface.OnClickListener()
			        {

				        @Override
				        public void onClick(DialogInterface dialog, int which)
				        {
					        dialog.cancel();
				        }
			        });

			return dialog;
		case VERIFY_KEY_DIALOG:
			final Dialog wait_dialog = new Dialog(this,
			        R.style.CustomAlertDialogStyle);
			wait_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			Log.i("SingleCamConfigureActivity", "Q");
			wait_dialog.setContentView(R.layout.dialog_wait_for_connecting);
			loader = (ImageView) wait_dialog.findViewById(R.id.imgLoader);
			if (loader != null)
			{
				loader.setBackgroundResource(R.drawable.loader_anim1);
				anim = (AnimationDrawable) loader.getBackground();
				anim.start();
			}

			txtWaiting = (TextView) wait_dialog
			        .findViewById(R.id.txtLoaderDesc);
			wait_dialog.setCancelable(true);
			wait_dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					// TODO Auto-generated method stub
					ImageView loader = (ImageView) wait_dialog
					        .findViewById(R.id.imgLoader);
					if (loader != null)
					{
						loader.clearAnimation();
					}
				}
			});

			return wait_dialog;

		case CONTACTING_BMS_SERVER_DIALOG:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.SingleCamConfigureActivity_conn_bms)
			        + "</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog) dialog).setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
				}
			});

			((AlertDialog) dialog).setButton(
			        getResources().getString(R.string.Cancel),
			        new DialogInterface.OnClickListener()
			        {

				        @Override
				        public void onClick(DialogInterface dialog, int which)
				        {
					        dialog.cancel();
				        }
			        });

			return dialog;

		case ASK_WEP_KEY_DIALOG:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			Log.i("SingleCamConfigureActivity", "R");
			dialog.setContentView(R.layout.bb_is_router_key);
			dialog.setCancelable(true);

			String text = null;
			if (this.selectedWifiCon != null)
			{
				// Obmit the quote
				text = this.selectedWifiCon.SSID.substring(1,
				        this.selectedWifiCon.SSID.length() - 1);
			}
			else
			{
				Log.e("mbp", ">>selectedWifiCon is NULL@@");
			}

			final TextView ssid_text = (TextView) dialog.findViewById(R.id.t0);
			ssid_text.setText(text);

			/*
			 * 20120724: dont enable wep_options LinearLayout wep_opts =
			 * (LinearLayout) dialog.findViewById(R.id.wep_options); if
			 * (wep_opts != null ) { wep_opts.setVisibility(View.VISIBLE); }
			 * 
			 * Spinner spinner = (Spinner)
			 * dialog.findViewById(R.id.wep_opt_index);
			 * ArrayAdapter<CharSequence> adapter =
			 * ArrayAdapter.createFromResource( this, R.array.key_index,
			 * android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource
			 * (android.R.layout.simple_spinner_dropdown_item);
			 * spinner.setAdapter(adapter); spinner.setSelection(0);
			 * 
			 * spinner = (Spinner) dialog.findViewById(R.id.wep_opt_method);
			 * adapter = ArrayAdapter.createFromResource( this,
			 * R.array.auth_method, android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource(android.R.layout.
			 * simple_spinner_dropdown_item); spinner.setAdapter(adapter);
			 * spinner.setSelection(0);
			 */

			EditText pwd_text = (EditText) dialog.findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
				        KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
						        .getInstance());

					}
					return false;
				}

			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;
					// String key_index = null;
					// Sring auth_method = null;

					EditText text = (EditText) dialog
					        .findViewById(R.id.text_key);
					if (text == null)
					{
						return;
					}

					pass_string = text.getText().toString();
					// text.setText(null);

					Spinner spinner = (Spinner) dialog
					        .findViewById(R.id.wep_opt_index);
					if (spinner != null)
					{
						// key_index = (String)spinner.getSelectedItem();
						spinner = (Spinner) dialog
						        .findViewById(R.id.wep_opt_method);
						// auth_method = (String) spinner.getSelectedItem();
					}

					/*
					 * Construct a WifiConf object here This is just a temp
					 * object to store data, it should not be used as an actual
					 * Wificonfiguration
					 */
					WifiConfiguration wep_conf = new WifiConfiguration();
					wep_conf.SSID = ssid_text.getText().toString(); // Un-quoted

					/*
					 * if (auth_method.equals("Open")) {
					 * wep_conf.allowedAuthAlgorithms
					 * .set(WifiConfiguration.AuthAlgorithm.OPEN); } else if
					 * (auth_method.equals("Shared")) {
					 * wep_conf.allowedAuthAlgorithms
					 * .set(WifiConfiguration.AuthAlgorithm.SHARED); }
					 * wep_conf.wepTxKeyIndex = Integer.parseInt(key_index)-1;
					 */

					// hardcode - auth OPEN
					wep_conf.allowedAuthAlgorithms
					        .set(WifiConfiguration.AuthAlgorithm.OPEN);
					// hardcode - index 1
					wep_conf.wepTxKeyIndex = 0;

					wep_conf.allowedKeyManagement
					        .set(WifiConfiguration.KeyMgmt.NONE);
					wep_conf.allowedGroupCiphers
					        .set(WifiConfiguration.GroupCipher.WEP40);

					wep_conf.wepKeys[wep_conf.wepTxKeyIndex] = pass_string;

					start_configure_camera(wep_conf);

					dialog.cancel();

				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});

			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					SingleCamConfigureActivity.this
					        .removeDialog(ASK_WEP_KEY_DIALOG);
				}
			});

			return dialog;
		}
		case ASK_WPA_KEY_DIALOG:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			Log.i("SingleCamConfigureActivity", "S");
			dialog.setContentView(R.layout.bb_is_router_key);
			dialog.setCancelable(true);

			LinearLayout wep_opts = (LinearLayout) findViewById(R.id.wep_options);
			if (wep_opts != null)
			{
				wep_opts.setVisibility(View.GONE);
			}

			String text = null;
			if (this.selectedWifiCon != null)
			{
				// Obmit the quote
				text = this.selectedWifiCon.SSID.substring(1,
				        this.selectedWifiCon.SSID.length() - 1);
			}
			else
			{
				Log.e("mbp", ">>selectedWifiCon is NULL@@");
			}

			final TextView ssid_text = (TextView) dialog.findViewById(R.id.t0);
			ssid_text.setText(text);

			EditText pwd_text = (EditText) dialog.findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
				        KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
						        .getInstance());

					}
					return false;
				}

			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;

					EditText text = (EditText) dialog
					        .findViewById(R.id.text_key);
					if (text == null)
					{
						return;
					}
					// Log.d("mbp", "WPA KEY: "+text.getText());
					pass_string = text.getText().toString();
					// text.setText(null);

					/* Construct a WifiConf object here */
					WifiConfiguration wpa_conf = new WifiConfiguration();
					wpa_conf.SSID = ssid_text.getText().toString(); // Un-quoted
					wpa_conf.allowedAuthAlgorithms
					        .set(WifiConfiguration.AuthAlgorithm.OPEN);
					wpa_conf.allowedKeyManagement
					        .set(WifiConfiguration.KeyMgmt.WPA_PSK);
					wpa_conf.preSharedKey = pass_string;

					start_configure_camera(wpa_conf);

					dialog.cancel();
				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});
			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					SingleCamConfigureActivity.this
					        .removeDialog(ASK_WPA_KEY_DIALOG);
				}
			});

			return dialog;
		}

		case ROUTER_CONNECTION_FAILED:
		{

			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.SingleCamConfigureActivity_wifi_key_err)
			        + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
				                }
			                });
			return builder.create();
		}

		case CONFIG_CAMERA_FAILED_DIALOG:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.SingleCamConfigureActivity_conf_cam_failed_1)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                WifiScan ws = new WifiScan(
					                        SingleCamConfigureActivity.this,
					                        SingleCamConfigureActivity.this);
					                wifi_scan_purpose = SCAN_FOR_CAMERA;
					                Spanned msg = Html
					                        .fromHtml("<big>"
					                                + getResources()
					                                        .getString(
					                                                R.string.ManualDirectConnectActivity_search_cam)
					                                + "</big>");
					                ws.setDialog_message(msg);
					                ws.execute("Scan now");
				                }
			                });
			return builder.create();
		}
		case CONNECT_TO_CAMERA_NETWORK_FAILED_DIALOG:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.SingleCamConfigureActivity_conn_cam_failed_1)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
				                }
			                });
			return builder.create();
		}
		case ADD_TO_CAMERA_NETWORK_FAILED_DIALOG:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.SingleCamConfigureActivity_conn_cam_failed_2)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                startActivity(new Intent(
					                        Settings.ACTION_WIFI_SETTINGS));
				                }
			                })
			        .setNegativeButton(
			                getResources().getString(R.string.Cancel),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.cancel();
				                }
			                });
			return builder.create();
		}
		case VERIFY_CAM_PASS_DIALOG:
		{
			dialog = new ProgressDialog(this);
			msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.SingleCamConfigureActivity_verifying_cam_pass)
			                + "</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog) dialog).setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener()
			{

				public void onCancel(DialogInterface dialog)
				{
				}
			});

			((AlertDialog) dialog).setButton(
			        getResources().getString(R.string.Cancel),
			        new DialogInterface.OnClickListener()
			        {

				        @Override
				        public void onClick(DialogInterface dialog, int which)
				        {
					        dialog.cancel();
				        }
			        });

			return dialog;
		}
		case CONNECTING_TO_HOME_DIALOG:
		{
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.SingleCamConfigureActivity_connecting)
			        + "</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog) dialog).setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener()
			{

				public void onCancel(DialogInterface dialog)
				{
				}
			});

			((AlertDialog) dialog).setButton(
			        getResources().getString(R.string.Cancel),
			        new DialogInterface.OnClickListener()
			        {

				        @Override
				        public void onClick(DialogInterface dialog, int which)
				        {
					        dialog.cancel();
				        }
			        });

			return dialog;
		}
		case DIALOG_FAILED_TO_CONNECT_TO_CAM_NW_RESCAN:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html
			        .fromHtml("<big>"
			                + getString(R.string.failed_to_connect_to_camera_network_press_ok_to_rescan)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(false)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                WifiScan ws = new WifiScan(
					                        SingleCamConfigureActivity.this,
					                        SingleCamConfigureActivity.this);
					                wifi_scan_purpose = SCAN_FOR_CAMERA;
					                showDialog(DIALOG_SEARCHING_CAMERA);
					                ws.setDialog_message(getResources()
					                        .getString(
					                                R.string.ManualDirectConnectActivity_search_cam));
					                ws.setSilence(true);
					                ws.execute("Scan now");
				                }
			                });
			return builder.create();
		}
		case DIALOG_STORAGE_UNAVAILABLE:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html
			        .fromHtml("<big>"
			                + getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();

					                Intent homeScreen = new Intent(
					                        SingleCamConfigureActivity.this,
					                        FirstTimeActivity.class);
					                homeScreen
					                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					                startActivity(homeScreen);
					                SingleCamConfigureActivity.this.finish();
				                }
			                });

			return builder.create();

		}

		case ROUTER_CONNECTION_FAILED_RETRY:
		{

			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.SingleCamConfigureActivity_wifi_key_err)
			        + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(
			                getResources().getString(R.string.retry),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                Log.d("mbp", "Retry -- using same key ");
					                // rescann -- without erasing data -- will
									// try with the same wifi info again ..
					                WifiScan ws = new WifiScan(
					                        SingleCamConfigureActivity.this,
					                        SingleCamConfigureActivity.this);
					                wifi_scan_purpose = SCAN_FOR_ALL;
					                ws.execute("Scan now");
				                }
			                })
			        .setPositiveButton(
			                getResources().getString(
			                        R.string.change_network_key),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                Log.d("mbp", "Change Key : - clear info ");
					                // clear wifi info -
					                clear_default_wifi_info();

					                // Popup asking for key.. not rescann -
									// maybe future-
					                WifiScan ws = new WifiScan(
					                        SingleCamConfigureActivity.this,
					                        SingleCamConfigureActivity.this);
					                wifi_scan_purpose = SCAN_FOR_ALL;
					                ws.execute("Scan now");

				                }
			                });
			return builder.create();
		}

		case DIALOG_RENAMING_CAMERA:
		{
			// dialog = new ProgressDialog(this);
			// msg =
			// Html.fromHtml("<big>"+getString(R.string.renaming_your_camera_please_wait_)+"</big>");
			// ((ProgressDialog) dialog).setMessage(msg);
			// ((ProgressDialog) dialog).setIndeterminate(true);
			// return dialog;
			final Dialog rename_dialog = new Dialog(this,
			        R.style.CustomAlertDialogStyle);
			rename_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			Log.i("SingleCamConfigureActivity", "T");
			rename_dialog.setContentView(R.layout.dialog_wait_for_connecting);
			loader = (ImageView) rename_dialog.findViewById(R.id.imgLoader);
			if (loader != null)
			{
				loader.setBackgroundResource(R.drawable.loader_anim1);
				anim = (AnimationDrawable) loader.getBackground();
				anim.start();
			}

			txtWaiting = (TextView) rename_dialog
			        .findViewById(R.id.txtLoaderDesc);
			txtWaiting.setText(R.string.renaming_your_camera_please_wait_);
			rename_dialog.setCancelable(true);
			rename_dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					// TODO Auto-generated method stub
					ImageView loader = (ImageView) rename_dialog
					        .findViewById(R.id.imgLoader);
					if (loader != null)
					{
						loader.clearAnimation();
					}
				}
			});

			return rename_dialog;
		}
		case DIALOG_FAILED_TO_FIND_CAM:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
			        SingleCamConfigureActivity.this);
			msg = Html
			        .fromHtml("<big>"
			                + getString(R.string.could_not_find_any_cameras_nearby_please_check_that_your_camera_has_been_switched_on_and_try_again)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();

					                // Go back to camera list
					                update_cam_success();

				                }
			                });

			return builder.create();
		}
		case DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.camera_name_length_is_out_of_boundary)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_CAMERA_NAME_IS_INVALID:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(R.string.camera_name_is_invalid)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_ADD_WIFI_NETWORK:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			Log.i("SingleCamConfigureActivity", "U");
			dialog.setContentView(R.layout.bb_is_other_router);
			dialog.setCancelable(true);

			final EditText pwd_text = (EditText) dialog
			        .findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
				        KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
						        .getInstance());

					}
					return false;
				}

			});

			Spinner spinner = (Spinner) dialog.findViewById(R.id.sec_type);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter
			        .createFromResource(this, R.array.sec_type,
			                android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(adapter);
			spinner.setSelection(0);
			spinner.setOnItemSelectedListener(new OnItemSelectedListener()
			{

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
				        int arg2, long arg3)
				{

					Log.d("mbp", "Selected item is: " + arg2);

					LinearLayout ll = (LinearLayout) dialog
					        .findViewById(R.id.linearLayout_pwd);
					if (ll != null)
					{
						if (arg2 != 0)
						{
							ll.setVisibility(View.VISIBLE);
						}
						else
						{
							ll.setVisibility(View.GONE);
						}
					}
					else
					{
						Log.d("mbp", "ll is nULL");
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0)
				{

				}

			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;
					String ssid_string = null;

					EditText ssid_ = (EditText) dialog
					        .findViewById(R.id.text_ssid);
					if (ssid_ == null)
					{
						return;
					}

					ssid_string = ssid_.getText().toString();

					EditText text = (EditText) dialog
					        .findViewById(R.id.text_key);
					if (text == null)
					{
						return;
					}

					// Log.d("mbp", "WPA KEY: "+text.getText());
					pass_string = text.getText().toString();
					// text.setText(null);

					/* Construct a WifiConf object here */
					WifiConfiguration _conf = new WifiConfiguration();
					_conf.SSID = "\"" + ssid_string + "\""; // Un-quoted
					_conf.hiddenSSID = true; //

					Spinner secType = (Spinner) dialog
					        .findViewById(R.id.sec_type);
					switch (secType.getSelectedItemPosition())
					{
					case 0: // OPEN
						_conf.allowedKeyManagement.set(KeyMgmt.NONE);
						break;
					case 1: // WEP
						_conf.allowedAuthAlgorithms
						        .set(WifiConfiguration.AuthAlgorithm.OPEN);
						_conf.wepTxKeyIndex = 0;
						_conf.allowedKeyManagement
						        .set(WifiConfiguration.KeyMgmt.NONE);
						_conf.allowedGroupCiphers
						        .set(WifiConfiguration.GroupCipher.WEP40);
						_conf.wepKeys[_conf.wepTxKeyIndex] = pass_string;

						break;
					case 2: // WPA2
						_conf.allowedAuthAlgorithms
						        .set(WifiConfiguration.AuthAlgorithm.OPEN);
						_conf.allowedKeyManagement
						        .set(WifiConfiguration.KeyMgmt.WPA_PSK);
						_conf.preSharedKey = pass_string;

						break;
					default:// UNKNOWN
						break;
					}

					selectedWifiCon = _conf;

					start_configure_camera(_conf);

					dialog.cancel();
				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});
			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					SingleCamConfigureActivity.this
					        .removeDialog(DIALOG_ADD_WIFI_NETWORK);
				}
			});

			return dialog;
		}
		default:
			break;
		}

		return null;
	}

	/*
	 * Important note: conf.SSID = un-quoted ssid
	 * 
	 * Currently Connect to BM network
	 */

	private void start_configure_camera(WifiConfiguration conf)
	{

		String ssid = conf.SSID;
		if (ssid.indexOf("\"") == 0
		        && ssid.lastIndexOf("\"") == ssid.length() - 1)
		{
			ssid = ssid.substring(1, ssid.lastIndexOf('"'));
		}

		// 3 cases: WPA, WEP, OPEN
		// Kick start a background task

		if ((conf.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK))
		        || (conf.allowedKeyManagement
		                .get(WifiConfiguration.KeyMgmt.WPA_EAP)))
		{

			// Log.d("mbp", "presharedkey: " + conf.preSharedKey);
			// conf.preSharedKey is still in normal text here.

			configuration = new CamConfiguration(ssid, "WPA/WPA2",
			        conf.preSharedKey, null, null, null, null, null, null,
			        cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass(), cam_profile.getName(), "4");
			configuration
			        .setWifiConf(SingleCamConfigureActivity.this.selectedWifiCon);

			Vector<String> dev_list = new Vector<String>(1);
			dev_list.addElement(cam_profile.getRegistrationId());
			configuration.setDeviceList(dev_list);

			/*
			 * Start a 3 consecutive Tasks - VerifyNetworkTask -> AddCameraTask
			 * --> AutoConfigureCameras
			 */

			VerifyNetworkKeyTask verify = new VerifyNetworkKeyTask(
			        SingleCamConfigureActivity.this, new Handler(
			                SingleCamConfigureActivity.this));
			verify.execute(configuration);

			try
            {
	            showDialog(VERIFY_KEY_DIALOG);
            }
            catch (Exception e)
            {
            }

		}
		else if (conf.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.NONE)
		        && (conf.allowedGroupCiphers
		                .get(WifiConfiguration.GroupCipher.WEP104) || conf.allowedGroupCiphers
		                .get(WifiConfiguration.GroupCipher.WEP40)))
		{
			// WEP

			String key_index = String.format("%d", conf.wepTxKeyIndex + 1);
			String auth_mode = conf.allowedAuthAlgorithms
			        .get(WifiConfiguration.AuthAlgorithm.OPEN) ? "Open"
			        : "Shared";

			configuration = new CamConfiguration(ssid, "WEP",
			        conf.wepKeys[conf.wepTxKeyIndex], key_index, auth_mode,
			        null, null, null, null, cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass(),
			        cam_profile.getName(), "4");
			configuration
			        .setWifiConf(SingleCamConfigureActivity.this.selectedWifiCon);

			Vector<String> dev_list = new Vector<String>(1);
			dev_list.addElement(cam_profile.getRegistrationId());
			configuration.setDeviceList(dev_list);

			/*
			 * Start a 3 consecutive Tasks - VerifyNetworkTask -> AddCameraTask
			 * --> AutoConfigureCameras
			 */

			VerifyNetworkKeyTask verify = new VerifyNetworkKeyTask(
			        SingleCamConfigureActivity.this, new Handler(
			                SingleCamConfigureActivity.this));
			verify.execute(configuration);

			try
            {
	            showDialog(VERIFY_KEY_DIALOG);
            }
            catch (Exception e)
            {
            }
			// Kick start a background task
			// config_task = new
			// AutoConfigureCameras(SingleCamConfigureActivity.this,
			// new Handler(SingleCamConfigureActivity.this));
			// config_task.execute(configuration);
		}
		else
		{
			// OPEN
			configuration = new CamConfiguration(ssid, "OPEN", "", null, null,
			        null, null, null, null, cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass(),
			        cam_profile.getName(), "4");
			configuration
			        .setWifiConf(SingleCamConfigureActivity.this.selectedWifiCon);

			Vector<String> dev_list = new Vector<String>(1);
			dev_list.addElement(cam_profile.getRegistrationId());
			configuration.setDeviceList(dev_list);

			VerifyNetworkKeyTask verify = new VerifyNetworkKeyTask(
			        SingleCamConfigureActivity.this, new Handler(
			                SingleCamConfigureActivity.this));
			verify.execute(configuration);

			try
            {
	            showDialog(VERIFY_KEY_DIALOG);
            }
            catch (Exception e)
            {
            }
			// //Since it's OPEN sec, no need to verify
			// config_task = new
			// AutoConfigureCameras(SingleCamConfigureActivity.this,
			// new Handler(SingleCamConfigureActivity.this));
			// config_task.execute(configuration);
		}

	}

	@Override
	public void scanWasCanceled()
	{
		// Do nothing
	}

	@Override
	public void updateWifiScanResult(final List<ScanResult> result)
	{
		switch (wifi_scan_purpose)
		{
		case SCAN_FOR_CAMERA:
			if (result != null)
			{
				runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						camera_selection(result);
					}
				});

			}
			break;
		case SCAN_FOR_ALL:
			if (result != null)
			{
				runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						router_selection(result);
					}
				});

			}
			break;
		default:
			break;
		}

	}

	private void check_and_reconnect_to_home()
	{
		WifiManager wm = (WifiManager ) getSystemService(Context.WIFI_SERVICE);
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String homeSSID = settings.getString(PublicDefine.PREFS_SAVED_WIFI_SSID, null);
		List<WifiConfiguration> wcs = wm.getConfiguredNetworks();
		for (WifiConfiguration wc : wcs)
		{
			if ((wc == null) || (wc.SSID == null))
				continue;
			if (wc.SSID.equalsIgnoreCase(convertToQuotedString(homeSSID)))
			{
				wm.enableNetwork(wc.networkId, true);
				//wm.reconnect();
			}
		}

	}

	private static String convertToQuotedString(String string)
	{
		return "\"" + string + "\"";
	}

	private void fade_outin_view(View v, int duration_ms)
	{
		Animation myFadeAnimation = AnimationUtils.loadAnimation(this,
		        R.anim.fadeout_in);
		myFadeAnimation.setDuration(duration_ms);
		myFadeAnimation
		        .setAnimationListener(new FadeOutAnimationAndGoneListener(v));
		v.startAnimation(myFadeAnimation);
	}

	/* First Page shown when enter */
	private void setup_camera_scan()
	{
		Display display = getWindowManager().getDefaultDisplay();
		int width_in_pixel = display.getWidth();
		int height_in_pixel = display.getHeight();
		int dpi = getResources().getDisplayMetrics().densityDpi;
		int dp_in_width = 160 * (width_in_pixel / dpi);
		int dp_in_height = 160 * (height_in_pixel / dpi);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			Log.i("SingleCamConfigureActivity", "B");
			setContentView(R.layout.bb_is_add_cam_landscape);
		}
		else
		{
			Log.i("SingleCamConfigureActivity", "D");
			setContentView(R.layout.bb_is_add_cam_potrait);
		}

		ImageView eye = (ImageView) findViewById(R.id.imageViewSnapShot);

		fade_outin_view(eye, 500);

		// TextView title = (TextView)findViewById(R.id.textTitle);
		// title.setText(R.string.a_quick_checklist_before_you_proceed);

		isOnFirstPage = true;

		WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);

		if (wm.getConnectionInfo() != null
		        && wm.getConnectionInfo().getSSID() != null)
		{

			saved_ssid = wm.getConnectionInfo().getSSID();
			saved_ssid = '\"' + saved_ssid + '\"';
		}

		Button next = (Button) findViewById(R.id.connectWifi);
		// next.getBackground().setColorFilter(0xff7fae00,
		// android.graphics.PorterDuff.Mode.MULTIPLY);
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				isOnFirstPage = false;
				displayPairInstructions();
			}
		});

		/*
		 * next.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * isOnFirstPage = false;
		 * 
		 * //WifiScan ws = new
		 * WifiScan(SingleCamConfigureActivity.this,SingleCamConfigureActivity
		 * .this); //wifi_scan_purpose = SCAN_FOR_CAMERA;
		 * //ws.setDialog_message(
		 * getResources().getString(R.string.ManualDirectConnectActivity_search_cam
		 * )); //ws.setSilence(true); //ws.execute("Scan now");
		 * camera_selection(null); //force layout first
		 * 
		 * startAutoRefresh(); } });
		 */

	}

	private void displayPairInstructions()
	{
		setContentView(R.layout.bb_is_add_camera_pair_instruction);
		Button btnSearchForCamera = (Button) findViewById(R.id.search_for_camera);
		btnSearchForCamera.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				camera_selection(null); // force layout first

				startAutoRefresh();

			}
		});
	}

	/**
	 * To run on UI thread
	 * 
	 * @param ns
	 */
	private void on_router_item_selected(NameAndSecurity ns)
	{

		String ssid = ns.name;
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String saved_wifi_nw = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_SSID, null);
		String saved_wifi_sec = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_SEC, null);
		Boolean saved_wifi_hidden_ssid = settings.getBoolean(
		        PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID, false);
		String saved_wifi_pass = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_PWD, null);

		if (ns.name.equals(saved_wifi_nw))
		{
			Log.d("mbp", "skip enter wifi password, rebuild new WifiConf ");

			/*
			 * {"OPEN", "WEP", "WPA", "WPA2"}
			 */
			// BUILD a wifi configuration
			// WEP
			if (saved_wifi_sec.equalsIgnoreCase("WEP"))
			{
				String auth_method = settings.getString(
				        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH, "Open");
				String key_index = settings.getString(
				        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX, "1");
				/*
				 * Construct a WifiConf object here This is just a temp object
				 * to store data, it should not be used as an actual
				 * Wificonfiguration
				 */
				WifiConfiguration wep_conf = new WifiConfiguration();
				wep_conf.SSID = '\"' + saved_wifi_nw + '\"';
				wep_conf.hiddenSSID = saved_wifi_hidden_ssid;
				if (auth_method.equals("Open"))
				{
					wep_conf.allowedAuthAlgorithms
					        .set(WifiConfiguration.AuthAlgorithm.OPEN);
				}
				else if (auth_method.equals("Shared"))
				{
					wep_conf.allowedAuthAlgorithms
					        .set(WifiConfiguration.AuthAlgorithm.SHARED);
				}

				wep_conf.allowedKeyManagement
				        .set(WifiConfiguration.KeyMgmt.NONE);
				wep_conf.allowedGroupCiphers
				        .set(WifiConfiguration.GroupCipher.WEP40);
				wep_conf.wepTxKeyIndex = Integer.parseInt(key_index) - 1;
				wep_conf.wepKeys[wep_conf.wepTxKeyIndex] = saved_wifi_pass;
				setSelectedSSID(saved_wifi_nw, saved_wifi_sec, wep_conf);
			}
			// WPA/WPA2
			else if (saved_wifi_sec.startsWith("WPA"))
			{
				/* Construct a WifiConf object here */
				WifiConfiguration wpa_conf = new WifiConfiguration();
				wpa_conf.SSID = '\"' + saved_wifi_nw + '\"';
				wpa_conf.hiddenSSID = saved_wifi_hidden_ssid;
				wpa_conf.allowedAuthAlgorithms
				        .set(WifiConfiguration.AuthAlgorithm.OPEN);
				wpa_conf.allowedKeyManagement
				        .set(WifiConfiguration.KeyMgmt.WPA_PSK);
				wpa_conf.preSharedKey = saved_wifi_pass;
				setSelectedSSID(saved_wifi_nw, saved_wifi_sec, wpa_conf);

			}
			// OPEN
			else
			{
				WifiConfiguration open_conf = new WifiConfiguration();
				open_conf.SSID = '\"' + saved_wifi_nw + '\"';
				open_conf.hiddenSSID = saved_wifi_hidden_ssid;
				open_conf.allowedKeyManagement.set(KeyMgmt.NONE);
				setSelectedSSID(saved_wifi_nw, saved_wifi_sec, open_conf);
			}
			autoConnectToOldWifi(saved_wifi_nw, saved_wifi_sec);
		}
		else
		{
			WifiManager w = (WifiManager) SingleCamConfigureActivity.this
			        .getSystemService(Context.WIFI_SERVICE);

			List<WifiConfiguration> wcs = w.getConfiguredNetworks();
			String checkSSID = '\"' + ssid + '\"';

			boolean foundExisting = false;

			WifiConfiguration selectedConfiguration;
			selectedConfiguration = null;

			for (WifiConfiguration wc : wcs)
			{
				if ((wc == null) || (wc.SSID == null))
					continue;
				if (wc.SSID.equals(checkSSID))
				{

					if (wc.allowedKeyManagement.equals(ns.getKeyManagement()))
					{
						foundExisting = true;
						selectedConfiguration = wc;
					}
					else
					{
						w.removeNetwork(wc.networkId);
					}
				}
			}

			if (!foundExisting)
			{

				WifiConfiguration newWC = new WifiConfiguration();
				newWC.hiddenSSID = false;
				newWC.SSID = checkSSID;
				newWC.status = WifiConfiguration.Status.ENABLED;
				// the following is the settings
				// that found to be working for ai-ball
				newWC.hiddenSSID = false;
				newWC.allowedAuthAlgorithms = ns.getAuthAlgorithm();
				newWC.allowedGroupCiphers = ns.getGroupCiphers();
				newWC.allowedKeyManagement = ns.getKeyManagement();

				newWC.allowedPairwiseCiphers = ns.getPairWiseCiphers();
				newWC.allowedProtocols = ns.getProtocols();

				int res = w.addNetwork(newWC);
				/*
				 * Log.d("mbp",this.getClass().getName() + " add new ssid:"+
				 * checkSSID+ " id:" + res + " cap: " +ns.security );
				 */

				// DONT REMOVE THIS
				Log.d("mbp", " save: " + w.saveConfiguration());

				selectedConfiguration = newWC;
			}

			/*
			 * Store in some temporary location and wait until user enter the
			 * key
			 */
			setSelectedSSID(ns.name, ns.security, selectedConfiguration);

			/*
			 * Popup a dialog asking for key if sec is WEP or WPA Else go direct
			 * to Configure Camera
			 */
			if (ns.security.equals("WEP"))
			{
				SingleCamConfigureActivity.this.showDialog(ASK_WEP_KEY_DIALOG);

			}
			else if (ns.security.startsWith("WPA"))
			{
				SingleCamConfigureActivity.this.showDialog(ASK_WPA_KEY_DIALOG);
			}
			else
			// Should be OPEN Sec
			{

				WifiConfiguration open_conf = new WifiConfiguration();
				open_conf.SSID = ssid;// NO - Quote
				open_conf.allowedKeyManagement.set(KeyMgmt.NONE);

				// Directly start configure
				start_configure_camera(open_conf);
			}
		}

	}

	/* Currently Connect to BM network */
	private void router_selection(List<ScanResult> results)
	{
		try
		{
			dismissDialog(DIALOG_SEARCHING_NETWORK);
		}
		catch (Exception e)
		{
		}

		ImageView imgConn = (ImageView) findViewById(R.id.imgConnecting);
		if (imgConn != null)
		{
			imgConn.clearAnimation();
		}
		/*
		 * CHECK if there is a default WIFI SSID & password saved in the
		 * settings- if there is skip scanning
		 */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);

		/* NO Quote */
		String saved_wifi_nw = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_SSID, null);
		String saved_wifi_sec = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_SEC, null);
		String saved_wifi_pass = settings.getString(
		        PublicDefine.PREFS_SAVED_WIFI_PWD, null);
		Boolean saved_wifi_hidden_ssid = settings.getBoolean(
		        PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID, false);

		/*
		 * Log.d("mbp", "REMOVE WHEN RELEASE -- Stored wifi: " + saved_wifi_nw +
		 * " sec:"+ saved_wifi_sec + " :key:"+ saved_wifi_pass );
		 */

		if ((saved_wifi_nw == null)
		        || (saved_wifi_sec == null)
		        || (!saved_wifi_sec.equalsIgnoreCase("OPEN") && (saved_wifi_pass == null))
		        || (results == null))
		{
			/*
			 * 20120217: in case we have ssid but don't have password. proceed
			 * to asking the key right away.
			 * 
			 * provide a way back to choose the network ..
			 */
			if (saved_wifi_nw != null)
			{
				NameAndSecurity _ns = null;
				for (ScanResult result : results)
				{
					if (result.SSID != null
					        && result.SSID.equals(saved_wifi_nw))
					{
						_ns = new NameAndSecurity(result.SSID,
						        result.capabilities, result.BSSID);
						_ns.setHideMac(true);
						break;
					}
				}

				if (_ns != null)
				{
					// Found in scanResult
					runOnUiThread(new Runnable()
					{
						private NameAndSecurity	ns;

						public Runnable setData(NameAndSecurity ns)
						{
							this.ns = ns;
							return this;
						}

						@Override
						public void run()
						{
							Log.d("mbp", "Found SSID only, ask for key now");
							on_router_item_selected(ns);
						}
					}.setData(_ns));
				}

			}
			else
			/* saved_wifi_nw == null */
			{
				// Continue to setup the wifi list
				router_selection_with_layout(results);
			} /* saved_wifi_nw == null */

		}
		else
		/* if there is some default wifi information */
		{
			// try to connect to the wifi with pass
			String checkSSID = '\"' + saved_wifi_nw + '\"';

			WifiManager w = (WifiManager) SingleCamConfigureActivity.this
			        .getSystemService(Context.WIFI_SERVICE);
			Log.d("mbp", "in the old wifi before: "
			        + w.getConnectionInfo().getSSID());
			List<WifiConfiguration> wcs = w.getConfiguredNetworks();

			boolean foundExisting = false;

			WifiConfiguration selectedConfiguration;
			selectedConfiguration = null;

			for (WifiConfiguration wc : wcs)
			{
				if ((wc == null) || (wc.SSID == null))
					continue;
				if (wc.SSID.equals(checkSSID))
				{
					foundExisting = true;
					selectedConfiguration = wc;
				}
			}

			boolean inTheOldWifi = false;
			if (saved_ssid.equals(checkSSID))
			{
				inTheOldWifi = true;
			}
			else
			{
			}

			/*
			 * 20130301: hoang: issue 1307 handle hidden SSID
			 */
			if (saved_wifi_hidden_ssid == false)
			{
				if (!foundExisting || !inTheOldWifi)
				{

					// //Go back to display the wifi list ----
					// clear_default_wifi_info();
					//
					// //force a rescan
					// WifiScan ws = new
					// WifiScan(SingleCamConfigureActivity.this,SingleCamConfigureActivity.this);
					// wifi_scan_purpose = SCAN_FOR_ALL;
					// ws.execute("Scan now");

					// Log.d("mbp","Can't find the ssid" + checkSSID+
					// " but Dont clear wifi_inf here..");

					// show the wifi list - instead
					router_selection_with_layout(results);
					return;
				}
				else
				{

					setSelectedSSID(saved_wifi_nw, saved_wifi_sec,
					        selectedConfiguration);
					autoConnectToOldWifi(saved_wifi_nw, saved_wifi_sec);

				}
			}
			else
			// hidden SSID
			{
				if (!inTheOldWifi)
				{
					router_selection_with_layout(results);
					return;
				}
				else
				{
					// BUILD a wifi configuration
					// WEP
					if (saved_wifi_sec.equalsIgnoreCase("WEP"))
					{
						String auth_method = settings.getString(
						        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH,
						        "Open");
						String key_index = settings.getString(
						        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX, "1");
						/*
						 * Construct a WifiConf object here This is just a temp
						 * object to store data, it should not be used as an
						 * actual Wificonfiguration
						 */
						WifiConfiguration wep_conf = new WifiConfiguration();
						wep_conf.SSID = '\"' + saved_wifi_nw + '\"';
						wep_conf.hiddenSSID = saved_wifi_hidden_ssid;
						if (auth_method.equals("Open"))
						{
							wep_conf.allowedAuthAlgorithms
							        .set(WifiConfiguration.AuthAlgorithm.OPEN);
						}
						else if (auth_method.equals("Shared"))
						{
							wep_conf.allowedAuthAlgorithms
							        .set(WifiConfiguration.AuthAlgorithm.SHARED);
						}

						wep_conf.allowedKeyManagement
						        .set(WifiConfiguration.KeyMgmt.NONE);
						wep_conf.allowedGroupCiphers
						        .set(WifiConfiguration.GroupCipher.WEP40);
						wep_conf.wepTxKeyIndex = Integer.parseInt(key_index) - 1;
						wep_conf.wepKeys[wep_conf.wepTxKeyIndex] = saved_wifi_pass;
						setSelectedSSID(saved_wifi_nw, saved_wifi_sec, wep_conf);
					}
					// WPA/WPA2
					else if (saved_wifi_sec.startsWith("WPA"))
					{
						/* Construct a WifiConf object here */
						WifiConfiguration wpa_conf = new WifiConfiguration();
						wpa_conf.SSID = '\"' + saved_wifi_nw + '\"';
						wpa_conf.hiddenSSID = saved_wifi_hidden_ssid;
						wpa_conf.allowedAuthAlgorithms
						        .set(WifiConfiguration.AuthAlgorithm.OPEN);
						wpa_conf.allowedKeyManagement
						        .set(WifiConfiguration.KeyMgmt.WPA_PSK);
						wpa_conf.preSharedKey = saved_wifi_pass;
						setSelectedSSID(saved_wifi_nw, saved_wifi_sec, wpa_conf);

					}
					// OPEN
					else
					{
						WifiConfiguration open_conf = new WifiConfiguration();
						open_conf.SSID = '\"' + saved_wifi_nw + '\"';
						open_conf.hiddenSSID = saved_wifi_hidden_ssid;
						open_conf.allowedKeyManagement.set(KeyMgmt.NONE);
						setSelectedSSID(saved_wifi_nw, saved_wifi_sec,
						        open_conf);
					}

					autoConnectToOldWifi(saved_wifi_nw, saved_wifi_sec);

				}
			}

		}

		return;

	}

	private void autoConnectToOldWifi(String saved_wifi_nw,
	        String saved_wifi_sec)
	{
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);

		// WEP
		if (saved_wifi_sec.equalsIgnoreCase("WEP"))
		{
			String auth_method = settings.getString(
			        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH, "Open");
			String key_index = settings.getString(
			        PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX, "1");
			String pass_string = settings.getString(
			        PublicDefine.PREFS_SAVED_WIFI_PWD, null);
			Boolean saved_wifi_hidden_ssid = settings.getBoolean(
			        PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID, false);
			/*
			 * Construct a WifiConf object here This is just a temp object to
			 * store data, it should not be used as an actual Wificonfiguration
			 */
			WifiConfiguration wep_conf = new WifiConfiguration();
			wep_conf.SSID = saved_wifi_nw; // Un-quoted
			wep_conf.hiddenSSID = saved_wifi_hidden_ssid;
			if (auth_method.equals("Open"))
			{
				wep_conf.allowedAuthAlgorithms
				        .set(WifiConfiguration.AuthAlgorithm.OPEN);
			}
			else if (auth_method.equals("Shared"))
			{
				wep_conf.allowedAuthAlgorithms
				        .set(WifiConfiguration.AuthAlgorithm.SHARED);
			}
			wep_conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			wep_conf.allowedGroupCiphers
			        .set(WifiConfiguration.GroupCipher.WEP40);
			wep_conf.wepTxKeyIndex = Integer.parseInt(key_index) - 1;
			wep_conf.wepKeys[wep_conf.wepTxKeyIndex] = pass_string;
			start_configure_camera(wep_conf);
		}
		// WPA/WPA2
		else if (saved_wifi_sec.startsWith("WPA"))
		{
			String pass_string = settings.getString(
			        PublicDefine.PREFS_SAVED_WIFI_PWD, null);
			Boolean saved_wifi_hidden_ssid = settings.getBoolean(
			        PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID, false);
			/* Construct a WifiConf object here */
			WifiConfiguration wpa_conf = new WifiConfiguration();
			wpa_conf.SSID = saved_wifi_nw; // Un-quoted
			wpa_conf.hiddenSSID = saved_wifi_hidden_ssid;
			wpa_conf.allowedAuthAlgorithms
			        .set(WifiConfiguration.AuthAlgorithm.OPEN);
			wpa_conf.allowedKeyManagement
			        .set(WifiConfiguration.KeyMgmt.WPA_PSK);
			wpa_conf.preSharedKey = pass_string;
			start_configure_camera(wpa_conf);

		}
		// OPEN
		else
		{
			Boolean saved_wifi_hidden_ssid = settings.getBoolean(
			        PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID, false);
			WifiConfiguration open_conf = new WifiConfiguration();
			open_conf.SSID = saved_wifi_nw;// NO - Quote
			open_conf.hiddenSSID = saved_wifi_hidden_ssid;
			open_conf.allowedKeyManagement.set(KeyMgmt.NONE);

			// Directly start configure
			start_configure_camera(open_conf);

		}
	}

	/**
	 * Called when : 1- No saved wifi ssid/ key - popup a list 2- has saved wifi
	 * ssid/ key but does not find the wifi after a scann - popup a list
	 * 
	 * @param results
	 */
	private int	wifi_selected_position;

	private void router_selection_with_layout(List<ScanResult> results)
	{
		// Continue to setup the wifi list
		Log.i("SingleCamConfigureActivity", "E");
		setContentView(R.layout.bb_is_wifi_selection);

		RelativeLayout addNetwork = (RelativeLayout) findViewById(R.id.addNetworks);
		addNetwork.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				try
                {
	                showDialog(DIALOG_ADD_WIFI_NETWORK);
                }
                catch (Exception e)
                {
                }
			}
		});

		RelativeLayout scanNetwork = (RelativeLayout) findViewById(R.id.wifiSearchRoot);
		scanNetwork.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				try
                {
	                showDialog(DIALOG_SEARCHING_NETWORK);
                }
                catch (Exception e)
                {
                }
				
				WifiScan ws = new WifiScan(SingleCamConfigureActivity.this,
				        SingleCamConfigureActivity.this);
				ws.setSilence(true);
				wifi_scan_purpose = SCAN_FOR_ALL;
				ws.execute("Scan now");
			}
		});

		if (results == null)
			return;

		ArrayList<NameAndSecurity> ap_list;
		final ListView wifi_list = (ListView) findViewById(R.id.wifi_list);
		ap_list = new ArrayList<NameAndSecurity>();
		for (ScanResult result : results)
		{
			/* 20120220:filter those camera network out of this list */
			if (!result.SSID.startsWith(PublicDefine.DEFAULT_CAM_NAME))
			{
				NameAndSecurity _ns = new NameAndSecurity(result.SSID,
				        result.capabilities, result.BSSID);
				_ns.setLevel(result.level);
				_ns.setHideMac(true);
				ap_list.add(_ns);
			}
		}

		final AccessPointAdapter ap_adapter = new AccessPointAdapter(this,
		        ap_list);

		wifi_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		wifi_list.setAdapter(ap_adapter);
		wifi_selected_position = -1;
		wifi_list.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
			        int position, long id)
			{

				// last element is not a NameAndSec object
				wifi_selected_position = position;
				int startPosition = wifi_list.getFirstVisiblePosition();
				int endPosition = wifi_list.getLastVisiblePosition();
				ap_adapter.setSelectedPositision(wifi_selected_position);
				for (int i = 0; i <= endPosition - startPosition; i++)
				{
					if (wifi_list.getChildAt(i) != null)
					{
						ImageView checked = (ImageView) parent.getChildAt(i)
						        .findViewById(R.id.imgChecked);
						if (i + startPosition == position)
						{
							checked.setVisibility(View.VISIBLE);
						}
						else
						{
							checked.setVisibility(View.INVISIBLE);
						}
					}
				}

			}
		});

		Button connect = (Button) findViewById(R.id.buttonConnect);
		// connect.getBackground().setColorFilter(0xff8eca00,
		// android.graphics.PorterDuff.Mode.MULTIPLY);
		connect.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (wifi_selected_position != -1)
				{
					NameAndSecurity ns = (NameAndSecurity) wifi_list
					        .getItemAtPosition(wifi_selected_position);
					on_router_item_selected(ns);
				}
			}
		});

	}

	private static final int	CAMER_SEARCH_TIMEOUT	= 3 * 60 * 1000; // 3
																		 // mins
	private long	         cameraSearchTimeout;
	private boolean	         startCameraSearchTimer	 = false;
	protected WakeLock	     wl = null;

	/*
	 * 20121015: phung: issue 488 - implement 3 min timeout if can't find any
	 * camera
	 */
	private void checkFailedToFindCamera(
	        ArrayAdapter<?> accessPointsArrayAdapter)
	{

		if (accessPointsArrayAdapter.isEmpty())// there is no camera in this
											   // list
		{
			if (startCameraSearchTimer == false)
			{
				startCameraSearchTimer = true;
				cameraSearchTimeout = System.currentTimeMillis()
				        + CAMER_SEARCH_TIMEOUT;
			}
		}
		else
		{
			startCameraSearchTimer = false;
			cameraSearchTimeout = 0;

		}

		if (startCameraSearchTimer)
		{

			if (System.currentTimeMillis() >= cameraSearchTimeout)
			{
				// Stop autoRefresh
				stopAutoRefresh();
				// popup -- Scan time-out
				// showDialog(DIALOG_FAILED_TO_FIND_CAM);
			}
		}
	}

	private int	selected_position;

	private void camera_selection(final List<ScanResult> results)
	{
		Log.i("SingleCamConfigureActivity", "F");
		setContentView(R.layout.bb_is_camera_selection_form);
		Button connect = (Button) findViewById(R.id.btn_connect);
		// connect.getBackground().setColorFilter(0xff8eca00,
		// android.graphics.PorterDuff.Mode.MULTIPLY);

		// setup Search again
		RelativeLayout camSearch = (RelativeLayout) findViewById(R.id.cameraSearchRoot);
		if (camSearch != null)
		{
			camSearch.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					startAutoRefresh();
				}
			});
		}
		
		if (results == null)
			return;

		final ListView wifi_list = (ListView) findViewById(R.id.cam_list);
		ArrayList<NameAndSecurity> accessPointList = new ArrayList<NameAndSecurity>();
		for (ScanResult result : results)
		{
			if (result.SSID != null
			        && result.SSID.startsWith(PublicDefine.DEFAULT_SSID_HD))
			{
				NameAndSecurity _ns = new NameAndSecurity(result.SSID,
				        result.capabilities, result.BSSID);
				_ns.setShowSecurity(false);
				_ns.setHideMac(true);
				accessPointList.add(_ns);
			}
		}

		if (accessPointList.size() > 0)
		{
			try
			{
				dismissDialog(DIALOG_SEARCHING_CAMERA);
			}
			catch (Exception e1)
			{
			}
		}
		
		final CameraAccessPointListAdapter adapter = new CameraAccessPointListAdapter(
		        this, accessPointList);

		// checkFailedToFindCamera(accessPointsArrayAdapter);

		wifi_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		wifi_list.setAdapter(adapter);
		selected_position = -1;
		if (wifi_list != null)
		{
			wifi_list.setOnItemClickListener(new OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
				        int position, long id)
				{
					// TODO Auto-generated method stub
					selected_position = position;
					int startPosition = wifi_list.getFirstVisiblePosition();
					int endPosition = wifi_list.getLastVisiblePosition();
					adapter.setSelectedPositision(selected_position);
					for (int i = 0; i <= endPosition - startPosition; i++)
					{
						if (wifi_list.getChildAt(i) != null)
						{
							ImageView checked = (ImageView) parent
							        .getChildAt(i)
							        .findViewById(R.id.imgChecked);
							if (i + startPosition == position)
							{
								checked.setVisibility(View.VISIBLE);
							}
							else
							{
								checked.setVisibility(View.INVISIBLE);
							}
						}
					}
				}
			});

			// if there is only 1 camera, automatically selected it
			if (accessPointList.size() == 1)
			{
				NameAndSecurity ns = accessPointList.get(0);
				Log.d("mbp", "Go to add camera: " + ns.name);
				wifi_list.performItemClick(wifi_list.getChildAt(0), 0, 0);
			}
		}

		if (connect != null)
		{

			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					// TODO Auto-generated method stub
					if (selected_position != -1)
					{
						Log.i("SingleCamConfigureActivity", "G");
						setContentView(R.layout.connecting_to_camera);
						ImageView imgConn = (ImageView) findViewById(R.id.imgConnecting);
						if (imgConn != null)
						{
							// start waiting animation
							imgConn.setVisibility(View.VISIBLE);
							imgConn.setImageResource(R.drawable.wifi_connecting_anim);
							anim = (AnimationDrawable) imgConn.getDrawable();
							anim.start();
						}

						TextView txtConn = (TextView) findViewById(R.id.txtConnecting);
						txtConn.setText(R.string.connecting_to_camera_wifi);
						TextView txtDesc = (TextView) findViewById(R.id.txtDesc);
						txtDesc.setText(R.string.this_may_take_up_to_a_minute);

						/*
						 * 20130415: hoang: turn off wifi connectivity checking
						 * while adding camera.
						 */
						Intent intent = new Intent();
						intent.setAction(ACTION_TURN_WIFI_NOTIF_ON_OFF);
						intent.putExtra(WIFI_NOTIF_BOOL_VALUE, TURN_OFF);
						sendBroadcast(intent);

						/*
						 * 20130201: hoang: issue 1260 force turn on screen
						 * while adding camera
						 */
						Log.d("mbp", "Acquire WakeLock for adding camera.");
						PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
						Log.d("mbp", "Turn screen on for a bit");
						wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
						        "TURN ON Because of error");
						wl.setReferenceCounted(false);
						wl.acquire();
						
						if (getWindow() != null)
						{
							getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
						}

						NameAndSecurity ns = ((NameAndSecurity) wifi_list
						        .getItemAtPosition(selected_position));

						String ssid = ns.name;
						String checkSSID = '\"' + ssid + '\"';
						String checkBSSID = ns.BSSID;

						WifiManager w = (WifiManager) SingleCamConfigureActivity.this
						        .getSystemService(Context.WIFI_SERVICE);
						List<WifiConfiguration> wcs = w.getConfiguredNetworks();
						boolean foundExisting = false;

						for (WifiConfiguration wc : wcs)
						{
							if ((wc == null) || (wc.SSID == null))
								continue;
							if (wc.SSID.equals(checkSSID))
							{

								if (wc.BSSID == null
								        || !wc.BSSID
								                .equalsIgnoreCase(checkBSSID)
								        || !wc.allowedKeyManagement.equals(ns
								                .getKeyManagement()))
								{
									w.removeNetwork(wc.networkId);
								}
								else
								{
									foundExisting = true;
								}

							}
						}

						if (!foundExisting)
						{
							WifiConfiguration newWC = new WifiConfiguration();
							newWC.hiddenSSID = false;
							newWC.SSID = checkSSID;
							newWC.BSSID = checkBSSID;
							newWC.status = WifiConfiguration.Status.ENABLED;
							// the following is the settings
							// that found to be working for ai-ball
							newWC.hiddenSSID = false;
							newWC.allowedAuthAlgorithms = ns.getAuthAlgorithm();
							newWC.allowedGroupCiphers = ns.getGroupCiphers();
							newWC.allowedKeyManagement = ns.getKeyManagement();
							if (ns.security.equalsIgnoreCase("WPA"))
							{
								newWC.preSharedKey = convertToQuotedString(PublicDefine.DEFAULT_WPA_PRESHAREDKEY);
							}
							newWC.allowedPairwiseCiphers = ns
							        .getPairWiseCiphers();
							newWC.allowedProtocols = ns.getProtocols();

							int res = w.addNetwork(newWC);

							// DONT REMOVE THIS
							Log.d("mbp", " save: " + w.saveConfiguration());

							// Search again to make sure it's there
							wcs = w.getConfiguredNetworks();
							foundExisting = false;
							for (WifiConfiguration wc : wcs)
							{
								if ((wc == null) || (wc.SSID == null))
									continue;
								if (wc.SSID.equals(checkSSID))
								{
									foundExisting = true;
								}
							}
							if (foundExisting)
							{
								Log.d("mbp", "added network successfully.");
							}
							else
							{
								// SingleCamConfigureActivity.this.showDialog(CONNECT_TO_CAMERA_NETWORK_FAILED_DIALOG);
								connectToCameraFailed(checkSSID);
							}

						}

						InetAddress default_inet_address = null;
						String default_cam_name = ns.name;
						String gatewayIp = getGatewayIp(SingleCamConfigureActivity.this);
						try
						{
							default_inet_address = InetAddress
							        .getByName(gatewayIp);
						}
						catch (UnknownHostException e)
						{
							Log.d("mbp", "exception: "
							        + this.getClass().getName());
							e.printStackTrace();
						}
						cam_profile = new CamProfile(default_inet_address,
						        checkBSSID);
						cam_profile.setName(default_cam_name);

						w = (WifiManager) SingleCamConfigureActivity.this
						        .getSystemService(Context.WIFI_SERVICE);
						wcs = w.getConfiguredNetworks();
						ConnectToNetwork connect_task = new ConnectToNetwork(
						        SingleCamConfigureActivity.this, new Handler(
						                SingleCamConfigureActivity.this));
						// ignore BSSID because some phones cannot get BSSID
						connect_task.setSilence(true);
						connect_task.setIgnoreBSSID(true);
						wcs = w.getConfiguredNetworks();
						boolean found = false;
						WifiConfiguration this_conf = null;
						for (WifiConfiguration wc : wcs)
						{
							if ((wc.SSID != null) && wc.SSID.equals(checkSSID))
							{
								// Log.d("mbp", "found:  " + wc.networkId + ":"
								// + wc.SSID+
								// " @ " + wc.BSSID + " presharedkey: " +
								// wc.preSharedKey);
								this_conf = wc;
								found = true;
								break;
							}
						}

						if (found)
						{
							// This task will make sure the app is connected to
							// the camera.
							// At the end it will send MSG_CONNECT_TO_NW_DONE
							// To verify cam password call camera_setup() when
							// done

							connect_task.execute(this_conf);
						}
						else
						{
							// showDialog(ADD_TO_CAMERA_NETWORK_FAILED_DIALOG);
							connectToCameraFailed(checkSSID);
						}
					} // if (selected_position != -1)
				}
			});

			// if there is only 1 camera, automatically selected it
			if (accessPointList.size() == 1)
			{
				connect.performClick();
			}
		}

		return;
	}

	private void connectToCameraFailed(String cameraSSID)
	{
		Log.i("SingleCamConfigureActivity", "H");
		setContentView(R.layout.connect_to_camera_failed);
		TextView txtFailed = (TextView) findViewById(R.id.txtDesc);
		String desc_str = String
		        .format(getString(R.string.please_check_and_reenter_the_password_or_choose_another_network),
		                cameraSSID);
		txtFailed.setText(desc_str);

		Button connect = (Button) findViewById(R.id.btn_connect);
		connect.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				startAutoRefresh();
			}
		});
	}

	/**
	 * 
	 * Start autoRefresh camera list if it's not started - Called on UI thread
	 */
	private void startAutoRefresh()
	{
		try
		{
			showDialog(DIALOG_SEARCHING_CAMERA);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (refresh_cam_list != null && refresh_cam_list.isAlive())
		{
			// do nothing
		}
		else
		// = null OR is NOT alive -- > create new;
		{
			Log.d("mbp", "startAutoRefresh() now");
			_camListRefresher = new CamListRefresher();
			refresh_cam_list = new Thread(_camListRefresher, "Refresh Cam List");
			refresh_cam_list.start();
		}

	}

	/**
	 * Stop if not stopped
	 */
	private void stopAutoRefresh()
	{
		if (refresh_cam_list != null && refresh_cam_list.isAlive())
		{
			Log.d("mbp", "stopAutoRefresh() now");

			_camListRefresher.stop();

			refresh_cam_list.interrupt();
			// try {
			// refresh_cam_list.join(1000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }

		}

		// try
		// {
		// dismissDialog(DIALOG_SEARCHING_CAMERA);
		// }
		// catch (Exception e)
		// {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	private Thread	         refresh_cam_list;
	private CamListRefresher	_camListRefresher;

	private class CamListRefresher implements Runnable
	{

		private boolean		isRunning;
		private WifiScan	ws;

		CamListRefresher()
		{
			isRunning = true;
			ws = null;
		}

		public void run()
		{
			while (isRunning)
			{

				try
				{
					Thread.sleep(3000);
				}
				catch (InterruptedException e)
				{
					if (isRunning == false)
					{
						break;
					}
				}
				ws = new WifiScan(SingleCamConfigureActivity.this,
				        SingleCamConfigureActivity.this);
				wifi_scan_purpose = SCAN_FOR_CAMERA;
				ws.setSilence(true);
				ws.execute("Scan now");

				// wait here until we get the result.. processing the result is
				// done in updateWifiScanResult()
				// here we just wait .. to avoid overlapping 2 scans.
				try
				{
					ws.get();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}

		public void stop()
		{
			isRunning = false;
			if (ws != null)
			{
				ws.cancel(true);
			}
		}
	}

	/**
	 * camera_setup_first_try
	 * 
	 * Try to set password automatically - if there is a saved password , use it
	 * - else use 000000
	 * 
	 * AFter that, go to camera_verify_pwd() to verify , if pass -> go on else
	 * -> go to camera_setup_name_and_password() - to ask user to fill in
	 * manually.
	 * 
	 * Note: runOnUIThread
	 */
	private void camera_setup_first_try()
	{

		WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if ((w.getConnectionInfo() == null)
		        || (w.getConnectionInfo().getBSSID() == null))
		{
			Log.d("mbp",
			        "w.getConnectionInfo() == null || w.getConnectionInfo().getBSSID() == null "
			                + " in camera_password");
			return;
		}

		/*
		 * 20120322: for all cases, start with default password first if false,
		 * try to check the password store if false , popup -wrong password if
		 * true , going thru all the way else , popup -new password
		 */
		String camPassString = "000000";

		cam_profile.setBasicAuth_usr(PublicDefine.DEFAULT_BASIC_AUTH_USR);
		cam_profile.setBasicAuth_pass(camPassString);

		Runnable worker = new Runnable()
		{
			public void run()
			{
				camera_verify_pwd();

				// SingleCamConfigureActivity.this.runOnUiThread(new Runnable()
				// {
				// @Override
				// public void run() {
				// dismissDialog(VERIFY_CAM_PASS_DIALOG);
				// }
				// });
			}
		};
		// showDialog(VERIFY_CAM_PASS_DIALOG);
		new Thread(worker, "Verify PWD").start();

	}

	/**
	 * camera_setup_name_and_password Show Camera setup screen ask for Camera
	 * name, Camera Password "Next" -- connect to the camera & test the password
	 * "Cancel" -- go back to the camera list
	 * 
	 * By now, iHome phone is connected to the camera network
	 */
	private void camera_setup_name_and_password()
	{
		WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if ((w.getConnectionInfo() == null)
		        || (w.getConnectionInfo().getBSSID() == null))
		{
			Log.d("mbp",
			        "w.getConnectionInfo() == null || w.getConnectionInfo().getBSSID() == null "
			                + " in camera_password");
			return;
		}
		String checkBSSID = w.getConnectionInfo().getBSSID();

		Log.i("SingleCamConfigureActivity", "I");
		setContentView(R.layout.bb_setup_camera_setup);

		final EditText cam_name = (EditText) findViewById(R.id.cam_name);
		cam_name.setText(cam_profile.getName());
		final EditText cam_pass = (EditText) findViewById(R.id.cam_pass);

		/* 20120209: issue 109 - save camera password */
		String camPassString = null;
		try
		{
			camPassString = CameraPassword.getPasswordforCam(
			        getExternalFilesDir(null), checkBSSID);
		}
		catch (StorageException e)
		{
			Log.d("mbp", e.getLocalizedMessage());
			try
            {
	            showDialog(DIALOG_STORAGE_UNAVAILABLE);
            }
            catch (Exception e1)
            {
            }
			return;
		}
		if (camPassString != null)
		{
			cam_pass.setText(camPassString);
			// Transform this password to ****
			cam_pass.setTransformationMethod(PasswordTransformationMethod
			        .getInstance());

		}

		cam_pass.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
			        KeyEvent event)
			{
				if (actionId == EditorInfo.IME_ACTION_DONE)
				{
					v.setTransformationMethod(PasswordTransformationMethod
					        .getInstance());
				}

				return false;
			}
		});

		Button next = (Button) findViewById(R.id.buttonNext);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				cam_profile.setName(cam_name.getText().toString());

				String cam_auth_pass_str = cam_pass.getText().toString();
				if ((cam_auth_pass_str == null)
				        || (cam_auth_pass_str.length() != 6)
				        || cam_auth_pass_str
				                .equalsIgnoreCase(PublicDefine.DEFAULT_CAM_PWD))
				{

					// dismissDialog(VERIFY_CAM_PASS_DIALOG);

					AlertDialog.Builder builder = new AlertDialog.Builder(
					        SingleCamConfigureActivity.this);
					builder.setMessage(
					        getResources()
					                .getString(
					                        R.string.SingleCamConfigureActivity_pwd_cam_2))
					        .setCancelable(true)
					        .setPositiveButton(
					                getResources().getString(R.string.OK),
					                new DialogInterface.OnClickListener()
					                {
						                public void onClick(
						                        DialogInterface dialog,
						                        int which)
						                {
							                dialog.dismiss();
						                }
					                });
					builder.create().show();
					return;

				}

				cam_profile
				        .setBasicAuth_usr(PublicDefine.DEFAULT_BASIC_AUTH_USR);
				cam_profile.setBasicAuth_pass(cam_auth_pass_str);

				Runnable worker = new Runnable()
				{
					public void run()
					{
						camera_verify_pwd();
						SingleCamConfigureActivity.this
						        .runOnUiThread(new Runnable()
						        {

							        @Override
							        public void run()
							        {
								        try
                                        {
	                                        dismissDialog(VERIFY_CAM_PASS_DIALOG);
                                        }
                                        catch (Exception e)
                                        {
                                        }
							        }
						        });
					}
				};
				
				try
                {
	                showDialog(VERIFY_CAM_PASS_DIALOG);
                }
                catch (Exception e)
                {
                }
				new Thread(worker, "Verify PWD").start();

			}
		});

		Button cancel = (Button) findViewById(R.id.buttonCancel);
		cancel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// * go back to the first page
				setup_camera_scan();
			}
		});
	}

	/**
	 * IMPORTANT: HAVE to run this on UI THREAD
	 * 
	 * @param cp
	 */
	private void camera_change_pwd(final CamProfile cp)
	{
		final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
		Log.i("SingleCamConfigureActivity", "J");
		dialog.setContentView(R.layout.bb_camera_change_pwd_form);
		dialog.setCancelable(true);

		final EditText pwd_text = (EditText) dialog.findViewById(R.id.text_key);
		pwd_text.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
			        KeyEvent event)
			{
				if (actionId == EditorInfo.IME_ACTION_DONE)
				{
					v.setTransformationMethod(PasswordTransformationMethod
					        .getInstance());

				}
				return false;
			}

		});

		// setup connect button
		Button connect = (Button) dialog.findViewById(R.id.pwd_change_btn);
		connect.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final Thread worker_ = new Thread()
				{
					public void run()
					{
						String pwd = pwd_text.getText().toString();
						if (pwd == null
						        || (pwd.length() < 3)
						        || (pwd.length() > 12)
						        || pwd.equalsIgnoreCase(PublicDefine.DEFAULT_CAM_PWD))
						{
							SingleCamConfigureActivity.this
							        .runOnUiThread(new Runnable()
							        {

								        @Override
								        public void run()
								        {

									        try
                                            {
	                                            dismissDialog(VERIFY_CAM_PASS_DIALOG);
                                            }
                                            catch (Exception e)
                                            {
                                            }

									        AlertDialog.Builder builder = new AlertDialog.Builder(
									                SingleCamConfigureActivity.this);
									        builder.setMessage(
									                getResources()
									                        .getString(
									                                R.string.SingleCamConfigureActivity_pwd_cam_2))
									                .setCancelable(true)
									                .setPositiveButton(
									                        getResources()
									                                .getString(
									                                        R.string.OK),
									                        new DialogInterface.OnClickListener()
									                        {
										                        public void onClick(
										                                DialogInterface dialog,
										                                int which)
										                        {
											                        dialog.dismiss();
											                        camera_change_pwd(cp);
										                        }
									                        });
									        builder.create().show();
								        }
							        });

							return;

						}

						String http_addr = null;
						String gatewayIp = SingleCamConfigureActivity
						        .getGatewayIp(SingleCamConfigureActivity.this);
						String device_address_port = gatewayIp + ":" + 80;
						http_addr = String
						        .format("%1$s%2$s%3$s%4$s",
						                device_address_port,
						                PublicDefine.HTTP_CMD_PART,
						                PublicDefine.BASIC_AUTH_USR_PWD_CHANGE
						                        + PublicDefine.BASIC_AUTH_USR_PWD_CHANGE_PARAM_1,
						                cp.getBasicAuth_usr() + ":" + pwd);

						String res = HTTPRequestSendRecv
						        .sendRequest_block_for_response(http_addr,
						                cp.getBasicAuth_usr(),
						                cp.getBasicAuth_pass());
						cp.setBasicAuth_pass(pwd);

						// Try to verify new password ..
						camera_verify_pwd();
						try
                        {
	                        dismissDialog(VERIFY_CAM_PASS_DIALOG);
                        }
                        catch (Exception e)
                        {
                        }

					}
				};

				try
                {
	                showDialog(VERIFY_CAM_PASS_DIALOG);
                }
                catch (Exception e)
                {
                }
				worker_.start();
				dialog.cancel();

			}
		});

		dialog.show();

	}

	private String getModelFromSsid(String cam_ssid)
	{
		String model = null;

		if (cam_ssid != null
		        && cam_ssid.startsWith(PublicDefine.DEFAULT_SSID_HD))
		{
			int startIdx = PublicDefine.DEFAULT_SSID_HD.length();
			int endIdx = startIdx + 3;
			model = cam_ssid.substring(startIdx, endIdx);
		}

		return model;
	}

	public static String getGatewayIp(Context mContext)
	{
		String gatewayIp = null;
		WifiManager w = (WifiManager) mContext
		        .getSystemService(Context.WIFI_SERVICE);
		gatewayIp = ((w.getDhcpInfo().serverAddress) & 0xFF) + "."
		        + ((w.getDhcpInfo().serverAddress >> 8) & 0xFF) + "."
		        + ((w.getDhcpInfo().serverAddress >> 16) & 0xFF) + "."
		        + ((w.getDhcpInfo().serverAddress >> 24) & 0xFF);
		return gatewayIp;
	}

	/**
	 * camera_verify_pwd precondition: Camerasetup page is shown DONT RUN THIS
	 * ON UI THREAD
	 */
	private void camera_verify_pwd()
	{
		String gatewayIp = getGatewayIp(this);
		String regId = null;
		try
		{
			regId = HTTPRequestSendRecv.getUdid(gatewayIp,
			        PublicDefine.DEFAULT_DEVICE_PORT,
			        cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass());
		}
		catch (ConnectException e2)
		{
			e2.printStackTrace();
		}

		if (regId != null && regId.length() == 26)
		{
			cam_profile.setRegistrationId(regId);
			String mac_address = CamProfile.getMacFromRegId(regId);
			String modelId = CamProfile.getModelIdFromRegId(regId);
			cam_profile.set_MAC(PublicDefine.add_colon_to_mac(mac_address));
			cam_profile.setModelId(modelId);
		}

		String fw_version = null;
		try
		{
			fw_version = HTTPRequestSendRecv.getFirmwareVersion(gatewayIp,
			        PublicDefine.DEFAULT_DEVICE_PORT,
			        cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass());
		}
		catch (SocketException e1)
		{
			e1.printStackTrace();
		}
		cam_profile.setFirmwareVersion(fw_version);

		/*
		 * 20130517: hoang: use get_codecs_support to test usr, pass
		 */
		String codec_str = null;
		try
		{
			codec_str = HTTPRequestSendRecv.getCodecsSupport(gatewayIp,
			        PublicDefine.DEFAULT_DEVICE_PORT,
			        cam_profile.getBasicAuth_usr(),
			        cam_profile.getBasicAuth_pass());
		}
		catch (SocketException e)
		{
			Log.d("mbp", "Connection error while authenticating");
			runOnUiThread(new Runnable()
			{
				public void run()
				{
					try
                    {
	                    showDialog(DIALOG_FAILED_TO_CONNECT_TO_CAM_NW_RESCAN);
                    }
                    catch (Exception e)
                    {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
                    }
				}
			});
		}
		/*
		 * 20130517: hoang: save camera codecs to use in add cam command default
		 * is MJPEG
		 */
		if (codec_str != null)
		{
			int codec_int_value = -1;
			try
            {
	            codec_int_value = Integer.parseInt(codec_str);
            }
            catch (NumberFormatException e)
            {
	            e.printStackTrace();
            }
			switch (codec_int_value) {
			case -1:
			case 0:
			case 1:
				cam_profile.setCodec(CamProfile.CODEC_MJPEG);
				break;

			case 2:
			case 3:
				cam_profile.setCodec(CamProfile.CODEC_H264);
				break;

			default:
				cam_profile.setCodec(CamProfile.CODEC_MJPEG);
				break;
			}
		}

		if (regId != null)
		{

			SingleCamConfigureActivity.this.runOnUiThread(new Runnable()
			{
				public void run()
				{
					// GO to scan for all network and select router
					WifiScan ws1 = new WifiScan(
					        SingleCamConfigureActivity.this,
					        SingleCamConfigureActivity.this);
					ws1.setSilence(true);
					wifi_scan_purpose = SCAN_FOR_ALL;
					ws1.execute("Scan now");
				}
			});

		}
		else
		{
			SingleCamConfigureActivity.this.runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					// TODO Auto-generated method stub
					connectToCameraFailed(cam_profile.getName());
				}
			});
		}

	}

	private void camera_config_completed()
	{
		// setContentView(R.layout. aa_setup_router_connect_complete_form);
		Log.i("SingleCamConfigureActivity", "K");
		setContentView(R.layout.bb_is_add_cam_end_screen);

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		Editor editor = settings.edit();
		editor.putString(
		        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
		        cam_profile.getRegistrationId());
		editor.commit();

		this.configuration = null;// clear the temp configuration

		TextView currentName = (TextView) findViewById(R.id.renameCam);
		if (currentName != null && cam_profile != null)
		{
			currentName.setText(cam_profile.getName());
		}

		Button finish_btn = (Button) findViewById(R.id.connect_btn);
		// finish_btn.getBackground().setColorFilter(0xff8eca00,
		// android.graphics.PorterDuff.Mode.MULTIPLY);
		finish_btn.setOnClickListener(new OnClickListener()
		{
			// go to Setup Finish screen
			@Override
			public void onClick(View v)
			{

				final EditText newName = (EditText) findViewById(R.id.renameCam);
				String newCameraName = newName.getText().toString().trim();

				/*
				 * 20140211: hoang: issue 330 length of camera name must be
				 * between 4 to 31 characters.
				 */
				if ((newCameraName.length() < 4)
				        || (newCameraName.length() > 31))
				{
					try
                    {
	                    showDialog(DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY);
                    }
                    catch (Exception e)
                    {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
                    }
					newName.setText("");
				}
				else if (LoginOrRegistrationActivity.validate(
						LoginOrRegistrationActivity.CAMERA_NAME,
				        newCameraName) == false)
				{
					try
                    {
	                    showDialog(DIALOG_CAMERA_NAME_IS_INVALID);
                    }
                    catch (Exception e)
                    {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
                    }
					newName.setText("");
				}
				else
				{
					/*
					 * 20130201: hoang: issue 1260 release wakelock
					 */
					if ((wl != null) && (wl.isHeld()))
					{
						wl.release();
						wl = null;
						Log.d("mbp", "Add cam success - release WakeLock");
					}
					if (getWindow() != null)
					{
						getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					}

					/* save user name & password for future automatic login */
					SharedPreferences settings = getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
					        user_token);

					/* 20120208:issue 95 : show the camera list after this */
					editor.putBoolean(PublicDefine.PREFS_AUTO_SHOW_CAM_LIST,
					        true);

					editor.commit();

					// TextView newName =
					// (TextView)findViewById(R.id.renameCam);
					String _newName = newName.getText().toString().trim();

					if (cam_profile != null
					        && !_newName.equals(cam_profile.getName()))
					{
						// Start a rename task
						ChangeNameTask rename = new ChangeNameTask(
						        SingleCamConfigureActivity.this,
						        SingleCamConfigureActivity.this);
						rename.execute(user_token, _newName,
						        cam_profile.getRegistrationId());
						try
                        {
	                        showDialog(DIALOG_RENAMING_CAMERA);
                        }
                        catch (Exception e)
                        {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
                        }
					}
					else
					{
						// assume name changed success - same effect
						update_cam_success();
					}

				}
			} // end of onClick
		});

	}

	@Override
	public void update_cam_success()
	{
		Log.d("mbp", "Change Name success");
		/* Go to User Login - this will automatically transfer to next activity */
		Intent entry = new Intent(SingleCamConfigureActivity.this,
		        UserLoginActivity.class);
		entry.putExtra(UserLoginActivity.bool_isLoggedIn, true);
		entry.putExtra(UserLoginActivity.STR_USER_TOKEN, user_token);
		entry.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		startActivity(entry);

		// 1 of 3 places - disable first time wizard
		// others are: - when user chooses login on create account page
		// - when user chooses to skip
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_APP_LAUNCHED_NOT_FIRST_TIME, true);
		editor.commit();

		g_should_relogin = false;
		SingleCamConfigureActivity.this.finish();
	}

	@Override
	public void update_cam_failed()
	{

		Log.d("mbp", "Change Name failed");

		// What todo??-- for now skip silencely ..
		Intent entry = new Intent(SingleCamConfigureActivity.this,
		        UserLoginActivity.class);
		entry.putExtra(UserLoginActivity.bool_isLoggedIn, true);
		entry.putExtra(UserLoginActivity.STR_USER_TOKEN, user_token);
		entry.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		g_should_relogin = false;
		startActivity(entry);
		SingleCamConfigureActivity.this.finish();
	}

	/* CONDITION: have to be connected to internet */
	private void camera_config_failed(int errCode)
	{
		Log.d("mbp", String.format("Add camera error code: %x", errCode));
		if (errCode != SingleCamConfigureActivity.ADD_CAM_FAILED_WITH_DESC)
		{
			RemCameraTask remove = new RemCameraTask();
			remove.execute(cam_profile.getRegistrationId(), user_token);
		}

		String strMessage = null;
		switch (errCode)
		{
		case AutoConfigureCameras.START_SCAN_FAILED:
			strMessage = getString(R.string.start_scan_failed);
			break;
		case AutoConfigureCameras.CONNECT_TO_CAMERA_FAILED:
			strMessage = getString(R.string.connect_to_camera_failed);
			break;
		case AutoConfigureCameras.SEND_DATA_TO_CAMERA_FAILED:
			strMessage = getString(R.string.send_data_to_camera_failed);
			break;
		case AutoConfigureCameras.CONNECT_TO_HOME_WIFI_FAILED:
			strMessage = getString(R.string.connect_to_homw_wifi_failed);
			break;
		case AutoConfigureCameras.SCAN_CAMERA_FAILED:
			strMessage = getString(R.string.scan_camera_failed);
			break;
		case AutoConfigureCameras.CAMERA_DOES_NOT_HAVE_SSID:
			strMessage = getString(R.string.camera_cannot_locate_router_please_move_camera_close_to_the_router_switch_off_and_on_the_camera_and_try_to_add_again_);
			break;
		case SingleCamConfigureActivity.ADD_CAM_FAILED_WITH_DESC:
			strMessage = "This camera is not registered. Setup camera failed";
			break;
		default:
			strMessage = getString(R.string.SingleCamConfigureActivity_conf_cam_failed_1);
			break;
		}

		try
		{
			dismissDialog(VERIFY_KEY_DIALOG);
		}
		catch (Exception e)
		{
		}

		// this.showDialogMessage(strMessage, errCode);
		Log.i("SingleCamConfigureActivity", "L");
		setContentView(R.layout.configure_camera_failed);
		TextView txtFailed = (TextView) findViewById(R.id.txtDesc);
		String desc_str = String.format("%s.(%x)", strMessage, errCode);
		txtFailed.setText(desc_str);

		Button connect = (Button) findViewById(R.id.btn_connect);
		connect.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				// WifiScan ws = new
				// WifiScan(SingleCamConfigureActivity.this,SingleCamConfigureActivity.this);
				// wifi_scan_purpose = SCAN_FOR_CAMERA;
				// Spanned msg =
				// Html.fromHtml("<big>"+getResources().getString(R.string.ManualDirectConnectActivity_search_cam)+"</big>");
				// ws.setDialog_message(msg);
				// ws.execute("Scan now");
				startAutoRefresh();
			}
		});

		/*
		 * 20130201: hoang: issue 1260 release wakelock
		 */
		if ((wl != null) && (wl.isHeld()))
		{
			wl.release();
			wl = null;
			Log.d("mbp", "Camera config failed - release WakeLock");
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	private void clear_default_wifi_info()
	{
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);

		SharedPreferences.Editor editor = settings.edit();
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_PWD);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SSID);
		editor.commit();
	}

	private synchronized void store_default_wifi_info()
	{
		clear_default_wifi_info();

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		if (configuration != null)
		{

			String ssid_no_quote = configuration.ssid();// no quote

			if (ssid_no_quote.indexOf("\"") == 0
			        && ssid_no_quote.lastIndexOf("\"") == ssid_no_quote
			                .length() - 1)
			{
				ssid_no_quote = ssid_no_quote.substring(1,
				        ssid_no_quote.lastIndexOf('"'));
			}

			String current_security_type = configuration.security_type();
			String pwd_no_quote = null;

			editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC,
			        current_security_type);
			editor.putString(PublicDefine.PREFS_SAVED_WIFI_SSID, ssid_no_quote);
			/*
			 * 20130228: hoang: issue 1307 store hidden SSID info to use later
			 */
			editor.putBoolean(PublicDefine.PREFS_SAVED_WIFI_HIDDEN_SSID,
			        (configuration.wc() == null) ? false
			                : configuration.wc().hiddenSSID);

			// Log.d("mbp","ssid_no_quote: " + ssid_no_quote);

			if (current_security_type.startsWith("WPA"))
			{
				pwd_no_quote = configuration.pass_string();
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_PWD,
				        pwd_no_quote);
			}
			else if (current_security_type.startsWith("WEP"))
			{
				pwd_no_quote = configuration.pass_string();
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX,
				        configuration.key_index());
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_PWD,
				        pwd_no_quote);
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH,
				        configuration.auth_method());

				/*
				 * Log.d("mbp","pwd_no_quote WEP: " + pwd_no_quote + " idex:" +
				 * configuration.key_index()+ " auth:"
				 * +configuration.auth_method());
				 */
			}

		}
		editor.commit();

	}

	@Override
	public boolean handleMessage(Message arg0)
	{

		switch (arg0.what)
		{
		case ConnectToNetwork.MSG_CONNECT_TO_NW_DONE:

			// Go to the next Screen - first try to bypass the camera password
			// screen
			camera_setup_first_try();
			break;
		case ConnectToNetwork.MSG_CONNECT_TO_NW_FAILED:
			// Connect to camera network failed OR connect to wifi network
			// failed
			final String checkSSID = (String) arg0.obj;
			Log.d("mbp", "Connect to network " + checkSSID + " failed");
			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					try
					{
						// showDialog(CONNECT_TO_CAMERA_NETWORK_FAILED_DIALOG);
						connectToCameraFailed(checkSSID);
					}
					catch (Exception e)
					{

					}

				}
			});

			// re-scan for camera
			// WifiScan ws = new
			// WifiScan(SingleCamConfigureActivity.this,SingleCamConfigureActivity.this);
			// wifi_scan_purpose = SCAN_FOR_CAMERA;
			// ws.setDialog_message(getResources().getString(R.string.ManualDirectConnectActivity_search_cam));
			// ws.execute("Scan now");

			break;
		case AutoConfigureCameras.MSG_AUTO_CONF_SUCCESS:
			camera_config_completed();
			break;
		case AutoConfigureCameras.MSG_AUTO_CONF_FAILED:
			camera_config_failed(arg0.arg1);
			// SHOULD NOT happen because we config camera one by one now
			break;

		case VerifyNetworkKeyTask.MSG_NETWORK_KEY_VERIFY_FAILED:
			Log.d("mbp", "KEY_VERIFY_FAILED - popup sth here ..");
			try
			{
				this.dismissDialog(VERIFY_KEY_DIALOG);
			}
			catch (Exception e)
			{

			}

			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					Log.i("SingleCamConfigureActivity", "M");
					setContentView(R.layout.connect_to_camera_failed);
					TextView txtFailed = (TextView) findViewById(R.id.txtDesc);
					String desc_str = String
					        .format(getString(R.string.SingleCamConfigureActivity_wifi_key_err));
					txtFailed.setText(desc_str);

					Button connect = (Button) findViewById(R.id.btn_connect);
					connect.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							// TODO Auto-generated method stub
							try
                            {
	                            showDialog(DIALOG_SEARCHING_NETWORK);
                            }
                            catch (Exception e)
                            {
                            }
							WifiScan ws = new WifiScan(
							        SingleCamConfigureActivity.this,
							        SingleCamConfigureActivity.this);
							ws.setSilence(true);
							wifi_scan_purpose = SCAN_FOR_ALL;
							ws.execute("Scan now");
						}
					});
				}
			});

			break;
		case VerifyNetworkKeyTask.MSG_NETWORK_KEY_VERIFY_PASSED:
			try
			{
				this.dismissDialog(VERIFY_KEY_DIALOG);
			}
			catch (Exception e)
			{
			}
			Log.d("mbp", "KEY_VERIFY_PASSED");

			Log.i("SingleCamConfigureActivity", "N");
			setContentView(R.layout.connecting_to_wifi);
			ImageView imgConn = (ImageView) findViewById(R.id.imgConnecting);
			if (imgConn != null)
			{
				// start waiting animation
				imgConn.setVisibility(View.VISIBLE);
				imgConn.setImageResource(R.drawable.wifi_connecting_anim);
				anim = (AnimationDrawable) imgConn.getDrawable();
				anim.start();
			}

			TextView txtConn = (TextView) findViewById(R.id.txtConnecting);
			txtConn.setText(R.string.connecting_camera_to_wifi);
			TextView txtDesc = (TextView) findViewById(R.id.txtDesc);
			String desc_str = String
			        .format(getResources()
			                .getString(
			                        R.string.please_wait_for_a_couple_of_minutes_while_camera_connects_to_your_network),
			                cam_profile.getName());
			txtDesc.setText(desc_str);

			store_default_wifi_info();
			/* NEXT: add camera to user online account */
			AddCameraTask try_addCam = new AddCameraTask(cam_profile);
			String deviceAccessibilityMode = "upnp";
			int offset = TimeZone.getDefault().getRawOffset();
			String timeZone = String.format("%s%02d.%02d", offset >= 0 ? "+" : "-", 
					Math.abs(offset) / 3600000,
			        (Math.abs(offset) / 60000) % 60);
			String subscriptionType = "tier1";

			try_addCam.execute(user_token, cam_profile.getName(),
			        cam_profile.getRegistrationId(), cam_profile.getModelId(),
			        deviceAccessibilityMode, cam_profile.getFirmwareVersion(),
			        timeZone);

			// this.showDialog(CONTACTING_BMS_SERVER_DIALOG);

			break;
		default:
			break;

		}
		return false;
	}

	private static final int	ADD_CAM_SUCCESS	           = 0x1;
	private static final int	ADD_CAM_FAILED_UNKNOWN	   = 0x2;
	private static final int	ADD_CAM_FAILED_WITH_DESC	= 0x3;
	private static final int	ADD_CAM_FAILED_SERVER_DOWN	= 0x11;

	private void add_cam_success(String master_key)
	{
		// this.dismissDialog(CONTACTING_BMS_SERVER_DIALOG);

		/*
		 * 20130806: hoang: not need to set masterkey in blinkhd
		 */
		// LASTLY: Kick start a background task
		configuration.setMasterKey(master_key);
		configuration.setCamProfiles(new CamProfile[] {cam_profile});
		config_task = new AutoConfigureCameras(SingleCamConfigureActivity.this, 
				new Handler(SingleCamConfigureActivity.this));
		config_task.execute(this.configuration);

	}

	private void add_cam_failed(String error_desc)
	{
		try
		{
			dismissDialog(VERIFY_KEY_DIALOG);
		}
		catch (Exception e)
		{
		}
		
		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);

		String message = String.format(
		        getResources().getString(
		                R.string.SingleCamConfigureActivity_add_cam_failed_1),
		        error_desc);

		builder.setMessage(message)
		        .setCancelable(true)
		        .setPositiveButton(
		                getResources().getString(
		                        R.string.SingleCamConfigureActivity_retry),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();

				                /* Retry this task */
				                AddCameraTask try_addCam = new AddCameraTask(
				                        cam_profile);
				                String deviceAccessibilityMode = "upnp";
				                int offset = TimeZone.getDefault()
				                        .getRawOffset();
				                String timeZone = String.format("%s%02d.%02d",
				                        offset >= 0 ? "+" : "-",
				                        Math.abs(offset) / 3600000,
				                        (Math.abs(offset) / 60000) % 60);
				                String subscriptionType = "tier1";

				                try_addCam.execute(user_token,
				                        cam_profile.getName(),
				                        cam_profile.getRegistrationId(),
				                        cam_profile.getModelId(),
				                        deviceAccessibilityMode,
				                        cam_profile.getFirmwareVersion(),
				                        timeZone);
				                try
                                {
	                                showDialog(VERIFY_KEY_DIALOG);
                                }
                                catch (Exception e)
                                {
                                }
			                }
		                })
		        .setNegativeButton(
		                getResources().getString(
		                        R.string.SingleCamConfigureActivity_exit),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {

				                /*
								 * 20130201: hoang: issue 1260 release wakelock
								 */
				                if ((wl != null) && (wl.isHeld()))
				                {
					                wl.release();
					                wl = null;
				                }
				                if (getWindow() != null)
								{
									getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
								}

				                SingleCamConfigureActivity.this.finish();
				                diag.cancel();
			                }
		                });

		alert = builder.create();
		
		try
        {
	        alert.show();
        }
        catch (Exception e)
        {
        }

	}

	private void showDialogMessage(String _msg, int errCode)
	{
		Dialog alert;

		_msg = String.format("%s.(%x)", _msg, errCode);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		Spanned msg = Html.fromHtml("<big>" + _msg + "</big>");
		builder.setMessage(msg)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface dialog,
			                        int which)
			                {
				                dialog.dismiss();
				                WifiScan ws = new WifiScan(
				                        SingleCamConfigureActivity.this,
				                        SingleCamConfigureActivity.this);
				                wifi_scan_purpose = SCAN_FOR_CAMERA;
				                Spanned msg = Html
				                        .fromHtml("<big>"
				                                + getResources()
				                                        .getString(
				                                                R.string.ManualDirectConnectActivity_search_cam)
				                                + "</big>");
				                ws.setDialog_message(msg);
				                ws.execute("Scan now");
			                }
		                });
		alert = builder.create();
		alert.show();
	}

	private void setSelectedSSID(String newSSID, String securityType,
	        WifiConfiguration w)
	{
		String security_type;
		synchronized (this)
		{
			if (securityType.startsWith("WPA"))
			{
				security_type = "WPA/WPA2";
			}
			else
			{
				security_type = securityType;
			}
			/* update the current_ssid with the seleted SSID */
			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();

			editor.putString(PublicDefine.PREFS_CURRENT_SSID, newSSID);
			editor.putString(PublicDefine.PREFS_CURRENT_NW_SEC, security_type);

			// Commit the edits!
			editor.commit();

			selectedWifiCon = w;
		}
	}

	private static final char[]	_CNAME	         = { 0x43, 0x48, 0x49, 0x4E,
	        0x41	                             };
	private static final char[]	_CCODE	         = { 0x43, 0x4E };
	private static final String	_CODE_HTTP_CHECK	= "http://api.ipinfodb.com/v3/ip-city/?key=d32404a074fea8426a5ebf8237e7e1603bca43ec63349c20da0e4a07bad79797";

	class AddCameraTask extends AsyncTask<String, String, Integer>
	{

		private String		usrToken;
		private String		deviceName;
		private String		registerId;
		private String		deviceModelId;
		private String		deviceAccessibilityMode;
		private String		deviceFwVersion;
		private String		timeZone;
		private String		subscriptionType;
		private String		_master_key;

		private String		_error_desc;
		private CamProfile	cp;

		/**
		 * @param cp
		 */
		public AddCameraTask(CamProfile cp)
		{
			this.cp = cp;
		}

		@Override
		protected Integer doInBackground(String... params)
		{

			URL url = null;
			HttpsURLConnection conn = null;
			DataInputStream inputStream = null;
			String response = null;
			int respondeCode = -1;
			int ret = -1;

			usrToken = params[0];
			Log.d("mbp", "access token: " + usrToken);
			deviceName = params[1];
			Log.d("mbp", "deviceName: " + deviceName);
			registerId = params[2]; // mac address
			Log.d("mbp", "registration id: " + registerId);
			deviceModelId = params[3];
			Log.d("mbp", "deviceModelId: " + deviceModelId);
			deviceAccessibilityMode = params[4];
			Log.d("mbp", "deviceAccessibilityMode: " + deviceAccessibilityMode);
			deviceFwVersion = params[5];
			Log.d("mbp", "deviceFwVersion: " + deviceFwVersion);
			timeZone = params[6];
			Log.d("mbp", "timeZone: " + timeZone);

			// String http_cmd = PublicDefine.BM_SERVER +
			// PublicDefine.BM_HTTP_CMD_PART + PublicDefine.ADD_CAM_CMD+
			// PublicDefine.ADD_CAM_PARAM_1 + usrName +
			// PublicDefine.ADD_CAM_PARAM_2 + usrPass +
			// PublicDefine.ADD_CAM_PARAM_3 + macAddress +
			// PublicDefine.ADD_CAM_PARAM_4 + camName+
			// PublicDefine.ADD_CAM_PARAM_5 + usrPass.length() +
			// PublicDefine.ADD_CAM_PARAM_6 + camCodec;

			Log.d("mbp", "Add camera " + deviceName + " to account.");
			try
			{

				// AddNewDeviceResponse reg_res =
				// Device.registerDevice2(user_token, deviceName, registerId,
				// deviceModelId,
				// deviceAccessibilityMode, deviceFwVersion, timeZone,
				// subscriptionType);
				AddNewDeviceResponse reg_res = Device.registerDevice(
				        user_token, deviceName, registerId,
				        deviceAccessibilityMode, deviceFwVersion, timeZone);

				if (reg_res != null)
				{
					if (reg_res.getStatus() == HttpURLConnection.HTTP_OK)
					{
						ret = ADD_CAM_SUCCESS;
						if (reg_res.getResponseData() != null)
						{
							_master_key = reg_res.getResponseData()
							        .getAuth_token();
						}
					}
					else
					{
//						tracker.sendEvent(GA_ADD_CAMERA_CATEGORY,
//						        "Get Masterkey Failed", "Masterkey ErrorCode "
//						                + reg_res.getStatus(), null);
						ret = ADD_CAM_FAILED_WITH_DESC;
						Log.d("mbp",
						        "Add camera res code: " + reg_res.getStatus());
						_error_desc = reg_res.getMessage();
					}
				}
				else
				{
					ret = ADD_CAM_FAILED_UNKNOWN;
					_error_desc = PublicDefine.get_error_description(
					        SingleCamConfigureActivity.this, 699);
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
//				tracker.sendEvent(GA_ADD_CAMERA_CATEGORY,
//				        "Get Masterkey Failed", "MalformedURLException", null);
				ret = ADD_CAM_FAILED_SERVER_DOWN;

			}
			catch (SocketTimeoutException se)
			{
				// Connection Timeout - Server unreachable ???
				se.printStackTrace();
//				tracker.sendEvent(GA_ADD_CAMERA_CATEGORY,
//				        "Get Masterkey Failed", "SocketTimeoutException", null);
				ret = ADD_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
//				tracker.sendEvent(GA_ADD_CAMERA_CATEGORY,
//				        "Get Masterkey Failed", "IOException", null);
				ret = ADD_CAM_FAILED_SERVER_DOWN;
			}

			return Integer.valueOf(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (result.intValue() == ADD_CAM_SUCCESS)
			{
				SingleCamConfigureActivity.this.add_cam_success(_master_key);
			}
			else
			{

				if (result.intValue() == ADD_CAM_FAILED_SERVER_DOWN)
				{
					_error_desc = getResources().getString(
					        R.string.server_connection_timeout);
				}

				SingleCamConfigureActivity.this.add_cam_failed(_error_desc);
			}
		}
	}

	class RemCameraTask extends AsyncTask<String, String, Integer>
	{

		private String	usrToken;
		private String	regId;

		@Override
		protected Integer doInBackground(String... params)
		{

			regId = params[0];
			usrToken = params[1];

			// String http_cmd = PublicDefine.BM_SERVER +
			// PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DEL_CAM_CMD+
			// PublicDefine.DEL_CAM_PARAM_1 + usrName +
			// PublicDefine.DEL_CAM_PARAM_2 + macAddress;
			int ret = -1;
			try
			{
				SimpleJsonResponse del_res = Device.delete(usrToken, regId);
				if (del_res != null && del_res.isSucceed())
				{
					int responseCode = del_res.getStatus();
					if (responseCode == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = ADD_CAM_FAILED_UNKNOWN;
					}
					else if (responseCode == HttpURLConnection.HTTP_OK)
					{
						ret = ADD_CAM_SUCCESS;
					}
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
			catch (SocketTimeoutException se)
			{
				// Connection Timeout - Server unreachable ???
				ret = ADD_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
		}
	}

}
