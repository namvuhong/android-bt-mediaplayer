package com.msc3.registration;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.blinkhd.gcm.GcmIntentService;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.CameraMenuActivity;
import com.msc3.ConnectToNetworkActivity;
import com.msc3.LogCollectorTask;
import com.msc3.PublicDefine;
import com.msc3.registration.ScrollOnKeyboardShowRelativeLayout.OnSoftKeyboardListener;
import com.nxcomm.blinkhd.ui.SettingsActivity;
import com.nxcomm.meapi.App;
import com.nxcomm.meapi.PublicDefines;
import com.nxcomm.meapi.SimpleJsonResponse;
import com.nxcomm.meapi.User;
import com.nxcomm.meapi.app.RegisterAppResponse;
import com.nxcomm.meapi.user.LoginResponse2;
import com.nxcomm.meapi.user.UserInformation;

public class LoginOrRegistrationActivity extends Activity
{

	public static final String	bool_shouldNotAutoLogin	                    = "shouldNotAutoLogin";
	public static final String	bool_createUserAccount	                    = "createUserAccount";
	public static final String	str_UserId	                                = "str_userid";
	public static final String	str_UserEmail	                            = "str_userEmail";
	public static final String	str_UserPass	                            = "str_userPass";
	public static final String	str_UserConfirmPass	                        = "str_userConfirmPass";

	public static final String	GA_LOGIN_CATEGORY	                        = "Login";

	private static final int	DIALOG_PASSWD_DOES_NOT_MATCH	            = 1;
	private static final int	DIALOG_CONTACTING_BMS_SERVER	            = 2;
	private static final int	DIALOG_LOGIN_BMS_SERVER	                    = 3;
	private static final int	DIALOG_REGISTER_BMS_SERVER	                = 4;
	private static final int	DIALOG_PASSWD_TOO_SHORT	                    = 5;
	private static final int	DIALOG_PASSWD_TOO_LONG	                    = 6;
	private static final int	DIALOG_USER_TOO_SHORT	                    = 7;
	private static final int	DIALOG_CONNECTING_TO_CAMERA_IN_DIRECT_MODE	= 8;
	private static final int	DIALOG_WRONG_EMAIL_FORMAT	                = 9;
	private static final int	DIALOG_USER_REG_SUCCESS	                    = 10;
	private static final int	DIALOG_UNCHECK_CONFIRM_THE_TERM	            = 11;
	private static final int	DIALOG_ABOUT	                            = 12;
	private static final int	DIALOG_USER_ID_IS_INVALID	                = 13;
	private static final int	DIALOG_USER_LENGTH_OUT_OF_RANGE	            = 14;
	private SSLContext	       ssl_context;

	public static final int		USER_ID	                                    = 1;
	public static final int		EMAIL	                                    = 2;
	public static final int		CAMERA_NAME									= 3;
	

	private static final int	REQUEST_CONNECTIVITY_CHECK_FOR_AUTO_LOGIN	= 0x111;
	private static final int	REQUEST_CONNECTIVITY_CHECK_FOR_LOGIN	    = 0x112;
	private static final int	REQUEST_CONNECTIVITY_CHECK_FOR_REGISTRATION	= 0x113;
	private static final int	READ_TERMS_OF_USE	                        = 0x114;

	private boolean	           checked_for_connectivity;
	private LoginTask	       try_login;
	private boolean	           createUserAccountOnSetupFirstTime;

//	private Tracker	           tracker;
	//private EasyTracker	       easyTracker;

	private AnimationDrawable	anim	                                    = null;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// Force portrait on Phone
		if (!getResources().getBoolean(R.bool.isTablet))
		{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

//		easyTracker = EasyTracker.getInstance();
//		easyTracker.setContext(LoginOrRegistrationActivity.this);
//		tracker = easyTracker.getTracker();

		try_login = null;
		createUserAccountOnSetupFirstTime = false;
		checked_for_connectivity = false;

		/* read the keystore and create the ssl context for HTTPS connections */
		ssl_context = prepare_SSL_context(this);

		// MBP2k/1k only
		mBP_clearNotification();

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings
		        .getString(PublicDefine.PREFS_SAVED_SERVER_URL,
		                "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;
	}

	protected void onStart()
	{

		super.onStart();

		// Log.d("mbp","ON START Is called after onActivityResult() REMEMBER PLeASEEEEEE dumb ass ");

		// Check to see if we should go on & check for connectivity
		if (checked_for_connectivity == true)
		{
			Log.d("mbp",
			        "ON START Is called after onActivityResult() REMEMBER PLeASEEEEEE, idiot!! ");
			// True means we are here AFTER ConnectToNetworkActivity is closed ,
			// so dont do anything
			// coz we have called propper functions in onAcitivityResult()
			return;
		}
		// else Go on , since this is the FIRST TIME..

		// setContentView(R.layout.bb_is_waiting_screen);
		setContentView(R.layout.bb_is_first_screen);

		ImageView loader = (ImageView) findViewById(R.id.imageLoader);
		loader.setBackgroundResource(R.drawable.loader_anim1);
		anim = (AnimationDrawable) loader.getBackground();

		// Check to see if we are asked to NOT login automatically
		Bundle extra = getIntent().getExtras();
		if (extra != null)
		{
			createUserAccountOnSetupFirstTime = extra.getBoolean(
			        bool_createUserAccount, false);
		}

		// Dont login -- go direct to create user account
		if (createUserAccountOnSetupFirstTime)
		{
			Log.d("mbp", "create user account now ");

			user_registration();
		}
		else
		{

			boolean shouldNotAutoLogin = false;
			if (extra != null)
			{
				shouldNotAutoLogin = extra.getBoolean(bool_shouldNotAutoLogin,
				        false);
			}

			/* get user name & password for future automatic login */
			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			/* 1 if there is saved urs/passwd .. go to auto-login */
			if ((shouldNotAutoLogin == false) && (saved_token != null))
			{
				Intent entry = new Intent(LoginOrRegistrationActivity.this,
				        ConnectToNetworkActivity.class);
				entry.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				LoginOrRegistrationActivity.this.startActivityForResult(entry,
				        REQUEST_CONNECTIVITY_CHECK_FOR_AUTO_LOGIN);

				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(
				        PublicDefine.PREFS_APP_LAUNCHED_NOT_FIRST_TIME, true);
				editor.commit();

			}
			/* 2. else .. go to first time setup */
			else
			{
				// show login screen
				LoginOrRegistrationActivity.this.user_login_or_registration();
			}
		}
	}

	protected void onDestroy()
	{
		super.onDestroy();

		if (try_login != null)
		{
			try_login.cancel(true);
		}
	}

	protected void onResume()
	{
		super.onResume();

		if (findViewById(R.id.root_view).getTag() != null
		        && findViewById(R.id.root_view).getTag().equals("scrollable"))
		{
			((ScrollOnKeyboardShowRelativeLayout) findViewById(R.id.root_view))
			        .setOnSoftKeyboardListener(new OnSoftKeyboardListener()
			        {
				        @Override
				        public void onShown()
				        {

					        TextView anchor = (TextView) findViewById(R.id.textCreateAccount);
					        final int height = anchor.getBottom();

					        final ScrollView scroller = (ScrollView) findViewById(R.id.loginScroll);
					        scroller.scrollTo(0, height);

					        scroller.post(new Runnable()
					        {
						        public void run()
						        {
							        scroller.scrollTo(0, height);
						        }
					        });
				        }

				        @Override
				        public void onHidden()
				        {
					        // currently do nothing on hide
				        }
			        });
		}
	}

	protected void onNewIntent(Intent intent)
	{
		this.setIntent(intent);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if (hasFocus == true && anim != null)
		{
			// opt2
			anim.start();
		}
		else if (hasFocus == false && anim != null)
		{
			// opt2
			// anim.cancel();
			// anim.stop();
		}
	}

	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.about_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch (item.getItemId())
		{
		case R.id.about_item:
			showDialog(DIALOG_ABOUT);
			// TextView version = (TextView) findViewById(R.id.versionText);
			// if (version != null)
			// version.setText(getResources().getString(R.string.Version) +
			// versionName);
			return true;

		case R.id.get_log_item:
			LogCollectorTask logC = new LogCollectorTask(this,
			        getExternalFilesDir(null));
			logC.execute(new ArrayList<String>());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		checked_for_connectivity = true;

		if (requestCode == REQUEST_CONNECTIVITY_CHECK_FOR_AUTO_LOGIN)
		{
			/* get user name & password for future automatic login */
			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			// String saved_usr =
			// settings.getString(PublicDefine.PREFS_SAVED_PORTAL_USR, null);
			// String saved_pwd=
			// settings.getString(PublicDefine.PREFS_SAVED_PORTAL_PWD, null);

			String temp_usr = settings.getString(
			        PublicDefine.PREFS_TEMP_PORTAL_USR, null);
			// String temp_pwd=
			// settings.getString(PublicDefine.PREFS_TEMP_PORTAL_PWD, null);
			String temp_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

			if ((resultCode != Activity.RESULT_OK) || (temp_token == null))
			{
				/* Sth is wrong stay in user_login screen */
				LoginOrRegistrationActivity.this.user_login_or_registration();
			}
			else
			{
				LoginOrRegistrationActivity.this.user_login_success(temp_usr,
				        temp_token);
			}
		}
		else if (requestCode == REQUEST_CONNECTIVITY_CHECK_FOR_LOGIN)
		{

			/* get user name & password for future automatic login */
			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String temp_usr = settings.getString(
			        PublicDefine.PREFS_TEMP_PORTAL_USR, null);
			String temp_pwd = settings.getString(
			        PublicDefine.PREFS_TEMP_PORTAL_PWD, null);

			/*
			 * 20120315- Dont Clear temp data - leave it to pre-fill the login
			 * form later SharedPreferences.Editor editor = settings.edit();
			 * editor.remove(PublicDefine.PREFS_TEMP_PORTAL_USR);
			 * editor.remove(PublicDefine.PREFS_TEMP_PORTAL_PWD);
			 * editor.commit();
			 */

			if ((resultCode != Activity.RESULT_OK) || (temp_usr == null)
			        || (temp_pwd == null))
			{
				/* Sth is wrong stay in user_login screen */
				LoginOrRegistrationActivity.this.user_login_or_registration();
			}
			else
			{
				LoginOrRegistrationActivity.this.user_logging_in(temp_usr,
				        temp_pwd);
			}
		}
		else if (requestCode == REQUEST_CONNECTIVITY_CHECK_FOR_REGISTRATION)
		{
			// NEXT page
			if (resultCode != Activity.RESULT_OK)
			{

			}
			else
			{
				LoginOrRegistrationActivity.this.user_registration();
			}
		}
		else if (requestCode == READ_TERMS_OF_USE)
		{
			user_registration();

			if (data != null)
			{
				Bundle userData = data.getExtras();
				String userId = null;
				String userEmail = null;
				String userPass = null;
				String userConfirmPass = null;
				if (userData != null)
				{
					userId = userData.getString(str_UserId);
					userEmail = userData.getString(str_UserEmail);
					userPass = userData.getString(str_UserPass);
					userConfirmPass = userData.getString(str_UserConfirmPass);

					reload_user_data(userId, userEmail, userPass,
					        userConfirmPass);

					if (resultCode == Activity.RESULT_OK)
					{
						register_user(userId, userEmail, userPass);
					}
					else if (resultCode == Activity.RESULT_CANCELED)
					{
						// Don't do anything... reload user info
					}

				}
				else
				{

				}
			}

		}

	}

	protected Dialog onCreateDialog(int id)
	{
		ProgressDialog dialog;
		switch (id)
		{
		case DIALOG_CONTACTING_BMS_SERVER:
			dialog = new ProgressDialog(this);
			Spanned msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_conn_bms)
			        + "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			// Dialog test = new Dialog(getApplicationContext(),
			// R.style.myDialogTheme);
			// test.setContentView(R.layout.dialog_connecting);

			return dialog;

		case DIALOG_LOGIN_BMS_SERVER:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_login_1)
			        + "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;

		case DIALOG_REGISTER_BMS_SERVER:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_register_1)
			        + "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;

		case DIALOG_PASSWD_DOES_NOT_MATCH:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			builder = new AlertDialog.Builder(this);

			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_passwd_1)
			        + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();

			return alert;
		}
		case DIALOG_PASSWD_TOO_SHORT:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_passwd_2)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_PASSWD_TOO_LONG:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_passwd_3)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_USER_TOO_SHORT:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_user_1)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_USER_LENGTH_OUT_OF_RANGE:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.user_length_is_out_of_range) + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_USER_ID_IS_INVALID:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"
			        + getResources().getString(R.string.user_id_is_invalid)
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_CONNECTING_TO_CAMERA_IN_DIRECT_MODE:
		{
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
			        + getResources().getString(
			                R.string.LoginOrRegistrationActivity_conn_camera)
			        + "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;
		}

		case DIALOG_WRONG_EMAIL_FORMAT:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html
			        .fromHtml("<big>"
			                + getString(R.string.invalid_email_address_valid_email_address_is_in_the_format_someone_somedomain_sth_please_re_enter)
			                + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.dismiss();
				                }
			                });
			alert = builder.create();
			return alert;
		}
		case DIALOG_UNCHECK_CONFIRM_THE_TERM:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.LoginOrRegistrationActivity_confirm_the_term)
			                + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                }).setOnCancelListener(new OnCancelListener()
			        {
				        @Override
				        public void onCancel(DialogInterface diag)
				        {
				        }
			        });

			alert = builder.create();
			return alert;
		}
		case DIALOG_USER_REG_SUCCESS:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(
			        R.string.LoginOrRegistrationActivity_user_reg_3));
			alert = builder.create();
			return alert;
		}
		case DIALOG_ABOUT:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			PackageInfo pinfo;
			String versionName = "0.0";
			try
			{
				pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				versionName = pinfo.versionName;
			}
			catch (NameNotFoundException e)
			{
				e.printStackTrace();
			}

			msg = Html.fromHtml("<big>"
			        + getResources().getString(R.string.Version) + versionName
			        + "</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface diag,
				                        int which)
				                {
					                diag.cancel();
				                }
			                });

			alert = builder.create();
			return alert;
		}
		default:
			break;

		}
		return null;
	}

	public static boolean isVOXServiceRunning(Context c)
	{
		ActivityManager manager = (ActivityManager) c
		        .getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
		        .getRunningServices(Integer.MAX_VALUE))
		{
			if ("com.msc3.VoiceActivationService".equals(service.service
			        .getClassName()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * For MBP2k/1K only - clear any pending notifications/ Including the
	 * recurring alert sound
	 */
	private void mBP_clearNotification()
	{
		String phonemodel = android.os.Build.MODEL;
		// For MBPxxx phones
		if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
		        || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			// //Clear the recurring sound now!!
			// if (MBP2K_NotifyService.isServiceRunning(this))
			// {
			// Intent recurringAlertService = new Intent(this,
			// MBP2K_NotifyService.class);
			// stopService(recurringAlertService);
			// }

			/*
			 * 20121119: phung: NON_MBP phones.. Simply clear notifications and
			 * Return
			 */
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

			for (int i = 0; i < GcmIntentService.PUSH_IDs.length; i++)
			{
				notificationManager.cancel(i);
			}
		}
	}

	private void user_login_or_registration()
	{
//		tracker.sendView("Login Screen");
		setContentView(R.layout.bb_is_login_screen);

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings
		        .getString(PublicDefine.PREFS_SAVED_SERVER_URL,
		                "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;

		// 1 of 3 places - when user chooses login on create account page Or
		// when user chooses to skip
		// others are: - disable first time wizard

		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_APP_LAUNCHED_NOT_FIRST_TIME, true);

		String temp_usr = settings.getString(
		        PublicDefine.PREFS_TEMP_PORTAL_USR, null);
		final String temp_pwd = settings.getString(
		        PublicDefine.PREFS_TEMP_PORTAL_PWD, null);

		editor.commit();

		final EditText usrNameField = (EditText) findViewById(R.id.text_userName);
		final EditText usrPwdField = (EditText) findViewById(R.id.text_userPwd);

		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Light.otf");
		Typeface boldTf = FontManager.fontBold;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Bold.otf");
		usrNameField.setTypeface(sboldTf);
		usrPwdField.setTypeface(sboldTf);
		usrPwdField.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
			        KeyEvent event)
			{

				return false;
			}
		});
		usrPwdField.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus == true)
				{
					usrPwdField.setText("");

				}
				else
				{

					if (usrPwdField.getText().length() == 0)
					{
						usrPwdField.setText(temp_pwd);
					}
				}

			}
		});

		/*
		 * if temp_usr is avail.. then use it .. otherwise.. use the saved,
		 * otherwise.. dont pre-fill
		 */
		if (temp_usr != null)
		{
			usrNameField.setText(temp_usr);
		}

		/*
		 * if temp_pwd is avail.. then use it .. otherwise.. use the saved,
		 * otherwise.. dont pre-fill
		 */
		if (temp_pwd != null)
		{
			usrPwdField.setText(temp_pwd);
			// usrPwdField.setTransformationMethod(PasswordTransformationMethod.getInstance());
		}

		Button login = (Button) findViewById(R.id.buttonLogin);
		login.setTypeface(boldTf);
		login.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				String userName = null, passwrd = null;
				userName = usrNameField.getText().toString().trim();
				passwrd = usrPwdField.getText().toString();

				// if (passwrd.length() < 8)
				// {
				// //password does not match
				// LoginOrRegistrationActivity.this.showDialog(DIALOG_PASSWD_TOO_SHORT);
				// }
				// else if (passwrd.length() > 30)
				// {
				// //password does not match
				// LoginOrRegistrationActivity.this.showDialog(DIALOG_PASSWD_TOO_LONG);
				// }
				// else if ( (userName == null) || (userName.length() < 5))
				// {
				// LoginOrRegistrationActivity.this.showDialog(DIALOG_USER_TOO_SHORT);
				// }
				// else
				{

					/*
					 * save TEMP user name & password for later used after
					 * connectivity is checked. TEMP user name & password is not
					 * verified. After verification, this should be empty.
					 */
					SharedPreferences settings = getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString(PublicDefine.PREFS_TEMP_PORTAL_USR,
					        userName);
					editor.putString(PublicDefine.PREFS_TEMP_PORTAL_PWD,
					        passwrd);

					/*
					 * if is checked --> write true, else --> false (i.e. nxt
					 * time login pwd will be cleared)
					 */
					/*
					 * editor.putBoolean(PublicDefine.PREFS_USED_SAVED_USR_PWD,
					 * rememberPwdCheck.isChecked());
					 */
					editor.commit();

					Intent entry = new Intent(LoginOrRegistrationActivity.this,
					        ConnectToNetworkActivity.class);
					LoginOrRegistrationActivity.this.startActivityForResult(
					        entry, REQUEST_CONNECTIVITY_CHECK_FOR_LOGIN);
					// LoginOrRegistrationActivity.this.user_logging_in(
					// userName, passwrd);
				}
			}
		});

		TextView forgetPwdText = (TextView) findViewById(R.id.forgetPwdText);
		forgetPwdText.setTypeface(lightTf);
		forgetPwdText.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				user_reset_password();
			}
		});

		SpannableString createAccountText_ = new SpannableString(getResources()
		        .getString(R.string.create_account));
		createAccountText_.setSpan(new UnderlineSpan(), 0,
		        createAccountText_.length(), 0);
		TextView createAccount = (TextView) findViewById(R.id.textCreateAccount);
		createAccount.setTypeface(sboldTf);
		createAccount.setText(createAccountText_);
		createAccount.setVisibility(View.VISIBLE);
		createAccount.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				user_registration();
			}
		});

	}

	private void user_registration_welcome()
	{
		setContentView(R.layout.bb_is_create_account_or_connect_locally);
		TextView title = (TextView) findViewById(R.id.textTitle);
		title.setText(R.string.create_an_account);
		Button next = (Button) findViewById(R.id.button1);
		next.getBackground().setColorFilter(0xff8eca00,
		        android.graphics.PorterDuff.Mode.MULTIPLY);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				user_registration();
			}
		});

		Button login = (Button) findViewById(R.id.button2);
		login.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				setResult(Activity.RESULT_CANCELED);

				user_login_or_registration();

			}
		});

	}

	private final TextWatcher	createAccountWatcher	= 
			new TextWatcher()
	{

		@Override
		public void onTextChanged(
				CharSequence s,
				int start,
				int before,
				int count)
		{
		}

		@Override
		public void beforeTextChanged(
				CharSequence s,
				int start,
				int count,
				int after)
		{
		}

		@Override
		public void afterTextChanged(
				Editable s)
		{
			validateAndEnableSignupButtonNow();
		}

		/**
		 * Validating every
		 * field input.
		 * 
		 * @return
		 */
		private boolean validateAndEnableSignupButtonNow()
		{

			EditText usrNameField = (EditText) findViewById(R.id.text_userName);
			EditText usrPwdField = (EditText) findViewById(R.id.text_userPwd);
			EditText usrConfirmPwdField = (EditText) findViewById(R.id.text_userConfirmPwd);
			EditText userEmailField = (EditText) findViewById(R.id.text_userEmail);

			String userName = "";
			String passwrd = "";
			String confirmPwd = "";
			String email = "";

			if (usrNameField != null
					&& usrNameField
					.getText()
					.toString() != null)
			{
				userName = usrNameField
						.getText()
						.toString()
						.trim();
			}

			if (usrPwdField != null)
			{
				passwrd = usrPwdField
						.getText()
						.toString();
			}

			if (usrConfirmPwdField != null)
			{
				confirmPwd = usrConfirmPwdField
						.getText()
						.toString();
			}

			if (userEmailField != null
					&& userEmailField
					.getText()
					.toString() != null)
			{
				email = userEmailField
						.getText()
						.toString()
						.trim();
			}

			Button signUp = (Button) findViewById(R.id.buttonSignup);
			if (signUp != null)
			{
				if ((userName
						.length() > 0)
						&& (passwrd
								.length() > 0)
								&& (confirmPwd
										.length() > 0)
										&& (email
												.length() > 0))
				{
					signUp.setEnabled(true);
					return true;
				}
				else
				{
					signUp.setEnabled(false);
				}
			}

			return false;
		}
	};

	private void user_registration()
	{
		setContentView(R.layout.bb_is_create_an_account);
		Log.d("mbp", "User REGISTRATION ");

		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Light.otf");
		Typeface boldTf = FontManager.fontBold;// Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Bold.otf");

		final ImageView imgLogo = (ImageView) findViewById(R.id.login_logo);
		if (imgLogo != null)
		{
			imgLogo.setOnLongClickListener(new OnLongClickListener()
			{

				@Override
				public boolean onLongClick(View v)
				{
					// TODO Auto-generated method stub
					final Dialog dialog = new Dialog(
					        LoginOrRegistrationActivity.this,
					        R.style.myDialogTheme);
					dialog.setContentView(R.layout.bb_server_url_new_name);
					dialog.setCancelable(true);

					// setup connect button
					Button connect = (Button) dialog
					        .findViewById(R.id.change_btn);
					connect.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							EditText text = (EditText) dialog
							        .findViewById(R.id.text_new_name);
							if (text != null
							        && !text.getText().toString().trim()
							                .isEmpty())
							{
								String serverUrl = "https://"
								        + text.getText().toString().trim()
								        + "/v1";
								Log.i("mbp", "New server URL: " + serverUrl);
								PublicDefines.SERVER_URL = serverUrl;
								SharedPreferences settings = getSharedPreferences(
								        PublicDefine.PREFS_NAME, 0);
								Editor editor = settings.edit();
								editor.putString(
								        PublicDefine.PREFS_SAVED_SERVER_URL,
								        serverUrl);
								editor.commit();
							}
							dialog.cancel();
						}
					});

					Button cancel = (Button) dialog
					        .findViewById(R.id.cancel_btn);
					cancel.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							dialog.cancel();
						}
					});

					dialog.setOnCancelListener(new DialogInterface.OnCancelListener()
					{

						@Override
						public void onCancel(DialogInterface dialog)
						{
							// GO to first time setup
							dialog.dismiss();
						}
					});

					dialog.show();
					return false;
				}
			});
		}

		final EditText usrNameField = (EditText) findViewById(R.id.text_userName);
		final EditText usrPwdField = (EditText) findViewById(R.id.text_userPwd);

		usrNameField.setTypeface(sboldTf);
		usrPwdField.setTypeface(sboldTf);

		usrPwdField.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
			        KeyEvent event)
			{
				return false;
			}
		});

		usrPwdField.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{

			}
		});

		final EditText usrConfirmPwdField = (EditText) findViewById(R.id.text_userConfirmPwd);
		usrConfirmPwdField.setTypeface(sboldTf);
		usrConfirmPwdField
		        .setOnEditorActionListener(new OnEditorActionListener()
		        {

			        @Override
			        public boolean onEditorAction(TextView v, int actionId,
			                KeyEvent event)
			        {
				        return false;
			        }
		        });
		usrConfirmPwdField.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{

			}
		});

		final EditText userEmailField = (EditText) findViewById(R.id.text_userEmail);
		userEmailField.setTypeface(sboldTf);

		usrNameField.addTextChangedListener(createAccountWatcher);
		usrPwdField.addTextChangedListener(createAccountWatcher);
		usrConfirmPwdField.addTextChangedListener(createAccountWatcher);
		userEmailField.addTextChangedListener(createAccountWatcher);

		final Button signUp = (Button) findViewById(R.id.buttonSignup);
		signUp.setTypeface(boldTf);
		signUp.setEnabled(false);
		signUp.getBackground().setColorFilter(null);
		signUp.setOnClickListener(null);
		final OnClickListener signUpListener = new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				String userName = null, passwrd = null, confirmPwd = null, email = null;
				userName = usrNameField.getText().toString().trim();
				passwrd = usrPwdField.getText().toString();
				confirmPwd = usrConfirmPwdField.getText().toString();
				email = userEmailField.getText().toString();

				if ((userName.length() < 5) || (userName.length() > 20))
				{
					// username too short
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_USER_LENGTH_OUT_OF_RANGE);
				}
				else if (!validate(USER_ID, userName))
				{
					// username is invalid
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_USER_ID_IS_INVALID);
				}
				else if (passwrd.length() < 8)
				{
					// password too short
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_PASSWD_TOO_SHORT);
				}
				else if (passwrd.length() > 30)
				{
					// password too long
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_PASSWD_TOO_LONG);
				}
				else if (!confirmPwd.equals(passwrd))
				{
					// password does not match
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_PASSWD_DOES_NOT_MATCH);
				}
				else if ((email == null) || (!validate(EMAIL, email)))
				{
					// invalid format email address
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_WRONG_EMAIL_FORMAT);
				}
				else
				{

					// //Force showing Terms of Use all the time
					// Intent i = new Intent(LoginOrRegistrationActivity.this,
					// TermOfUseActivity.class);
					// i.putExtra(str_UserId, userName);
					// i.putExtra(str_UserEmail, email);
					// i.putExtra(str_UserPass, passwrd);
					// startActivityForResult(i, READ_TERMS_OF_USE);

					register_user(userName, email, passwrd);

				}

			}
		};

		final CheckBox agree = (CheckBox) findViewById(R.id.check_agreetheTerm);
		agree.setTypeface(lightTf);
		agree.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
			        boolean isChecked)
			{
				// TODO Auto-generated method stub
				if (isChecked == false)
				{
					signUp.setOnClickListener(null);
				}
				else
				{
					signUp.setOnClickListener(signUpListener);
				}
			}
		});

		SpannableString loginText_ = new SpannableString(getResources()
		        .getString(R.string.already_have_a_account));
		loginText_.setSpan(new UnderlineSpan(), 0, loginText_.length(), 0);
		TextView createAccount = (TextView) findViewById(R.id.textHaveAccount);
		createAccount.setTypeface(sboldTf);
		createAccount.setText(loginText_);
		createAccount.setVisibility(View.VISIBLE);
		createAccount.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				user_login_or_registration();
			}
		});

		SpannableString termOfUse = new SpannableString(getResources()
		        .getString(R.string.term_of_use));
		loginText_.setSpan(new UnderlineSpan(), 0, loginText_.length(), 0);
		TextView termOfUse_ = (TextView) findViewById(R.id.termOfUse);
		termOfUse_.setTypeface(lightTf);
		termOfUse_.setText(termOfUse);
		termOfUse_.setVisibility(View.VISIBLE);
		termOfUse_.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				String passwrd = null, confirmPwd = null, email = null;
				passwrd = usrPwdField.getText().toString();
				confirmPwd = usrConfirmPwdField.getText().toString();
				email = userEmailField.getText().toString();

				// Force showing Terms of Use all the time
				Intent i = new Intent(LoginOrRegistrationActivity.this,
				        TermOfUseActivity.class);
				i.putExtra(str_UserConfirmPass, confirmPwd);
				i.putExtra(str_UserEmail, email);
				i.putExtra(str_UserPass, passwrd);
				startActivityForResult(i, READ_TERMS_OF_USE);
			}
		});

	}

	public static boolean validate(int fieldId, String fieldContent)
	{
		boolean isValid = false;
		switch (fieldId)
		{
		case USER_ID:
			String regularExpression = "^[_a-zA-Z0-9]+$";
			if (fieldContent.matches(regularExpression))
			{
				isValid = true;
			}
			break;

		case EMAIL:
			regularExpression = "^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)+$";
			if (fieldContent.matches(regularExpression))
			{
				isValid = true;
			}
			break;

		/*
		 * 20130306: hoang: issue 1505 validate camera name
		 */
		case CAMERA_NAME:
			regularExpression = "^[a-zA-Z0-9' \\._-]+$";
			if (fieldContent.matches(regularExpression))
			{
				isValid = true;
			}
			break;

		default:
			break;
		}

		return isValid;
	}

	/**
	 * 
	 * Called after user has agreed to Terms of use
	 */
	private void reload_user_data(String userName, String email, String pass,
	        String confirmPass)
	{
		EditText usrNameField = (EditText) findViewById(R.id.text_userName);
		EditText usrConfirmPwdField = (EditText) findViewById(R.id.text_userConfirmPwd);
		EditText usrPwdField = (EditText) findViewById(R.id.text_userPwd);
		EditText userEmailField = (EditText) findViewById(R.id.text_userEmail);

		usrNameField.setText(userName);
		usrPwdField.setText(pass);
		usrConfirmPwdField.setText(confirmPass);
		userEmailField.setText(email);
	}

	private void register_user(String userId, String email, String passwrd)
	{

		this.showDialog(DIALOG_CONTACTING_BMS_SERVER);

		RegisterTask try_register = new RegisterTask();
		/*
		 * End of this task one of these funcs will be called -
		 * user_reg_success() - user_reg_failed() - user_reg_failed_to_connect()
		 */
		try_register.execute(userId, email, passwrd);

	}

	private static final int	USER_REGISTER_SUCCESS	                      = 0x1;
	private static final int	USER_REGISTER_FAILED_USR_ALREADY_REGISTERED	  = 0x2;
	private static final int	USER_REGISTER_FAILED_SERVER_UNDER_MAINTENANCE	= 0x4;
	private static final int	USER_REGISTER_FAILED_SERVER_UNREACHABLE	      = 0x11;
	private static final int	USER_REGISTER_FAILED_WITH_DESC	              = 0x12;

	private void user_reg_success(final String userName,
	        final String userEmail, final String passwrd, final String userToken)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		// PHUNG: //////******** I THINK, Those LINES OF CODE BELOW are NEVER
		// USED *************////

		// 20120520: display a dialog for 2 secs - go to add camera
		showDialog(DIALOG_USER_REG_SUCCESS);

		// Save userName & pass
		/*
		 * save user name & password for future automatic login actually just
		 * save the email
		 */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_USR, userName);
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_PWD, passwrd);
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_ID, userName);
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, userToken);

		editor.putString(PublicDefine.PREFS_TEMP_PORTAL_USR, userName);
		editor.putString(PublicDefine.PREFS_TEMP_PORTAL_PWD, passwrd);
		editor.commit();

		Intent i = new Intent(this, SettingsActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
		finish();
		// Thread worker = new Thread() {
		// public void run()
		// {
		// try {
		// Thread.sleep(2000);
		// } catch (InterruptedException e1) {
		// }
		// runOnUiThread(new Runnable() {
		//
		// @Override
		// public void run() {
		// try
		// {
		// dismissDialog(DIALOG_USER_REG_SUCCESS);
		// }
		// catch (Exception e)
		// {
		// }
		// LoginOrRegistrationActivity.this.user_logging_in(userName, passwrd);
		// }
		// });
		// }
		// };
		// worker.start();

		/*
		 * 20120520: dont need to activate user_activation(userName,passwrd);
		 */
	}

	private void user_reg_failed(String reason)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(reason)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
			                }
		                }).setOnCancelListener(new OnCancelListener()
		        {
			        @Override
			        public void onCancel(DialogInterface diag)
			        {
			        }
		        });

		alert = builder.create();
		alert.show();

	}

	private void user_reg_failed_to_connect()
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_user_reg_2))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.dismiss();
			                }
		                }).setOnCancelListener(new OnCancelListener()
		        {
			        @Override
			        public void onCancel(DialogInterface diag)
			        {
			        }
		        });

		alert = builder.create();
		alert.show();
	}

	private void user_logging_in(String userName, String passwrd)
	{

		try_login = new LoginTask();
		/*
		 * End of this task one of these funcs will be called -
		 * user_login_success() - user_login_failed() -
		 * user_login_failed_to_connect()
		 */
		this.showDialog(DIALOG_CONTACTING_BMS_SERVER);
		try_login.execute(userName, passwrd);

	}

	private void user_login_success(String userName, final String sessionToken)
	{
		Log.d("mbp", "User Login Success...");
		// 20131018: hoang: query app id for use in the future
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{

				String android_id = Secure.getString(getContentResolver(),
				        Secure.ANDROID_ID);
				RegisterAppResponse registerAppRes = null;
				try
				{

					PackageInfo pInfo = null;
					try
					{
						pInfo = getPackageManager().getPackageInfo(
						        getPackageName(), 0);
					}
					catch (NameNotFoundException e)
					{
						e.printStackTrace();
					}

					String software_version = null;
					if (pInfo != null)
					{
						software_version = pInfo.versionName;
					}
					else
					{
						software_version = "00.00";
					}
					Log.d("mbp", "Register app version: " + software_version);

					registerAppRes = App.registerApp(sessionToken, android_id,
					        android.os.Build.MODEL, software_version);
					long appId = -1;
					if (registerAppRes != null
					        && registerAppRes.getStatus() == HttpURLConnection.HTTP_OK)
					{
						appId = registerAppRes.getData().getId();
						if (appId != -1)
						{
							SharedPreferences settings = getSharedPreferences(
							        PublicDefine.PREFS_NAME, 0);
							SharedPreferences.Editor editor = settings.edit();
							editor.putLong(
							        PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID,
							        appId);
							editor.commit();
						}
					}
				}
				catch (SocketTimeoutException e)
				{
					e.printStackTrace();
				}
				catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}).start();

		try
		{
			this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		}
		catch (IllegalArgumentException ie)
		{

		}
		catch (BadTokenException bad)
		{

		}

		Intent entry = new Intent(LoginOrRegistrationActivity.this,
		        UserLoginActivity.class);
		entry.putExtra(UserLoginActivity.bool_isLoggedIn, true);
		entry.putExtra(UserLoginActivity.STR_USER_TOKEN, sessionToken);

		/* save user name & password for future automatic login */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, sessionToken);
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_ID, userName);
		editor.putBoolean(PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
		editor.commit();

		entry.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		LoginOrRegistrationActivity.this.startActivity(entry);

		// 20121106: no need to stay alive on my stack
		LoginOrRegistrationActivity.this.finish();

		// next time we r back here we don't want to relogin-
		checked_for_connectivity = false;
		// Intent goBack = new Intent();
		// goBack.putExtra(bool_shouldNotAutoLogin, true);
		// setIntent(goBack);
	}

	private void user_login_failed()
	{
		try
		{
			this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		}
		catch (IllegalArgumentException ie)
		{

		}
		catch (BadTokenException bad)
		{

		}

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_login_2))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
			                }
		                });
		alert = builder.create();
		alert.show();

	}

	private void user_login_failed_with_desc(String desc)
	{
		try
		{
			this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		}
		catch (IllegalArgumentException ie)
		{

		}
		catch (BadTokenException bad)
		{

		}
		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_login_3)
		                + desc)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
				                // Go back to login screen
				                LoginOrRegistrationActivity.this
				                        .user_login_or_registration();
			                }
		                });

		alert = builder.create();
		alert.show();
	}

	private void user_login_failed_to_connect()
	{
		try
		{
			this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		}
		catch (IllegalArgumentException iae)
		{

		}
		catch (BadTokenException bad)
		{

		}

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.could_not_reach_server_please_try_again))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
				                // Go back to login screen
				                LoginOrRegistrationActivity.this
				                        .user_login_or_registration();
			                }
		                });

		alert = builder.create();
		try
		{
			alert.show();
		}
		catch (BadTokenException bad)
		{
		}

	}

	private static final int	USER_ACTIVATE_SUCCESS	                = 0x1;
	private static final int	USER_ACTIVATE_FAILED_UNKNOWN	        = 0x2;
	private static final int	USER_ACTIVATE_FAILED_SERVER_UNREACHABLE	= 0x11;
	private static final int	USER_ACTIVATE_FAILED_WITH_DESC	        = 0x12;

	private void user_act_success(String usrName, String usrPass)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		user_activation_complete(usrName, usrPass);
	}

	private void user_act_failed(String desc)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_user_act_1)
		                + desc)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
			                }
		                }).setOnCancelListener(new OnCancelListener()
		        {
			        @Override
			        public void onCancel(DialogInterface diag)
			        {
			        }
		        });

		alert = builder.create();
		alert.show();
	}

	private void user_act_failed_to_connect()
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_user_act_2))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.dismiss();
			                }
		                });

		alert = builder.create();
		alert.show();
	}

	private void user_activation_complete(final String usrName,
	        final String usrPass)
	{

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_user_act_3))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.dismiss();
				                // Intent cam_conf = new
				                // Intent(LoginOrRegistrationActivity.this,SingleCamConfigureActivity.class);
				                // cam_conf.putExtra(SingleCamConfigureActivity.str_userName,
				                // usrName);
				                // cam_conf.putExtra(SingleCamConfigureActivity.str_userPass,
				                // usrPass);
				                // LoginOrRegistrationActivity.this.startActivity(cam_conf);
				                // LoginOrRegistrationActivity.this.finish();

				                /* try to do auto login here */
				                LoginOrRegistrationActivity.this
				                        .user_logging_in(usrName, usrPass);

			                }
		                });

		alert = builder.create();
		alert.show();

	}

	private static final int	PWD_RESET_SUCCESS	                      = 0x1;
	private static final int	PWD_RESET_FAILED_SERVER_UNDER_MAINTENANCE	= 0x4;
	private static final int	PWD_RESET_FAILED_SERVER_UNREACHABLE	      = 0x11;
	private static final int	PWD_RESET_FAILED_WITH_DESC	              = 0x12;

	private void user_reset_password()
	{
		setContentView(R.layout.bb_is_forgot_pwd_screen);
		TextView title = (TextView) findViewById(R.id.textTitle);
		title.setText(R.string.baby_monitor);

		final EditText usrEmail = (EditText) findViewById(R.id.text_usrEmail);

		Button cancel = (Button) findViewById(R.id.buttonCancel);
		cancel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// Go back to login screen
				LoginOrRegistrationActivity.this.user_login_or_registration();
			}
		});

		Button next = (Button) findViewById(R.id.buttonResetPwd);
		next.getBackground().setColorFilter(0xff7fae00,
		        android.graphics.PorterDuff.Mode.MULTIPLY);
		next.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				String usr_email = usrEmail.getText().toString();
				if (usr_email == null || usr_email.length() == 0)
				{
					showDialog(DIALOG_USER_TOO_SHORT);
				}
				else
				{
					LoginOrRegistrationActivity.this
					        .showDialog(DIALOG_CONTACTING_BMS_SERVER);
					ResetPwdTask resetpwd = new ResetPwdTask();
					resetpwd.execute(usr_email);
				}

			}
		});
	}

	/*
	 * private void user_reset_password_success() {
	 * this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER); AlertDialog.Builder
	 * builder; AlertDialog alert; builder = new
	 * AlertDialog.Builder(LoginOrRegistrationActivity.this);
	 * builder.setMessage(
	 * getResources().getString(R.string.LoginOrRegistrationActivity_reset_pwd_1
	 * )) .setCancelable(true)
	 * .setPositiveButton(getResources().getString(R.string.OK), new
	 * DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface diag, int which) {
	 * diag.dismiss(); //Go back to login screen
	 * LoginOrRegistrationActivity.this.user_login_or_registration(); } });
	 * 
	 * alert = builder.create(); alert.show(); }
	 */
	private void user_reset_password_success(final String email)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);
		setContentView(R.layout.bb_is_link_to_reset_pwd);

		TextView title = (TextView) findViewById(R.id.textTitle);
		title.setText(R.string.baby_monitor);

		TextView eMail = (TextView) findViewById(R.id.t2);
		if (email != null)
		{
			eMail.setText(email);
		}

		Button btn_ok = (Button) findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// Go back to login screen
				LoginOrRegistrationActivity.this.user_login_or_registration();
			}
		});

	}

	private void user_reset_password_failed_server_down()
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_login_4))
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.dismiss();
			                }
		                });

		alert = builder.create();
		alert.show();
	}

	private void user_reset_password_failed_with_desc(String desc)
	{
		this.dismissDialog(DIALOG_CONTACTING_BMS_SERVER);

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(
		        getResources().getString(
		                R.string.LoginOrRegistrationActivity_user_act_1)
		                + desc)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
			                }
		                }).setOnCancelListener(new OnCancelListener()
		        {
			        @Override
			        public void onCancel(DialogInterface diag)
			        {
			        }
		        });

		alert = builder.create();
		alert.show();

	}

	/**
	 * prepare_SSL_context - read the keystore, and create an SSL context for
	 * used with HTTPSURLCONNECTION
	 * 
	 * @param mContext
	 *            - mainly used to read the keystore from resource
	 * @return SSLContext object or NULL if there is some problems
	 */
	public static SSLContext prepare_SSL_context(Context mContext)
	{
		KeyStore trusted = null;
		SSLContext context = null;
		try
		{
			trusted = KeyStore.getInstance("BKS");
			InputStream in = mContext.getResources().openRawResource(
			        R.raw.bms_key_store);
			try
			{
				trusted.load(in, "smartpanda".toCharArray());
			}
			finally
			{
				in.close();
			}

		}
		catch (Exception e)
		{
			Log.d("mbp", "keystore exception:" + e.getLocalizedMessage());
			return null;
		}

		TrustManagerFactory tmf;
		try
		{
			tmf = TrustManagerFactory.getInstance("X509");
			tmf.init(trusted);

			context = SSLContext.getInstance("TLS");
			context.init(null, tmf.getTrustManagers(), null);

		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		catch (KeyStoreException e)
		{
			e.printStackTrace();
		}
		catch (KeyManagementException e)
		{
			e.printStackTrace();
		}

		// return context;

		// 20120306: to enable real SSL: return NULL here
		// Log.d("mbp","return NULL to force using the official cert");
		return null;
	}

	class LoginTask extends AsyncTask<String, String, Integer>
	{

		private String		     usrName;
		private String		     usrPwd;
		private String		     usrToken;
		private String		     userEmail;
		private String		     _error_desc;

		private static final int	USER_LOGIN_SUCCESS		                   = 0x1;
		private static final int	USER_LOGIN_FAILED_PWD_NOT_CORRECT		   = 0x2;
		private static final int	USER_LOGIN_FAILED_USR_NOT_REGISTERED		= 0x3;
		private static final int	USER_LOGIN_FAILED_SERVER_UNDER_MAINTENANCE	= 0x4;
		private static final int	USER_LOGIN_FAILED_SERVER_UNREACHABLE		= 0x11;
		private static final int	USER_LOGIN_FAILED_WITH_DESC		           = 0x12;

		@Override
		protected Integer doInBackground(String... params)
		{

			usrName = params[0];
			usrPwd = params[1];

			// String http_cmd = PublicDefine.BM_SERVER +
			// PublicDefine.BM_HTTP_CMD_PART + PublicDefine.USR_LOGIN_CMD+
			// PublicDefine.USR_LOGIN_PARAM_1 + usrName +
			// PublicDefine.USR_LOGIN_PARAM_2 + usrPwd +
			// PublicDefine.USR_LOGIN_PARAM_3 + "android" +
			// PublicDefine.USR_LOGIN_PARAM_4 + "iHome" +
			// PublicDefine.USR_LOGIN_PARAM_5 + usrPwd.length();

			usrToken = null;
			int ret = -1;
			int retry = 1;
			do
			{
				try
				{
					com.nxcomm.meapi.PublicDefines.setHttpTimeout(30000);

					LoginResponse2 login_res = User.login2(usrName, usrPwd);
					if (login_res != null)
					{
						if (login_res.getStatus() == HttpURLConnection.HTTP_OK)
						{
							usrToken = login_res.getAuthenticationToken();
							if (usrToken != null)
							{
								ret = USER_LOGIN_SUCCESS;
								break;
							}
							else
							{
//								tracker.sendEvent(GA_LOGIN_CATEGORY,
//								        "Login Task",
//								        "Login Failed - Unhandled Exception",
//								        null);
								ret = USER_LOGIN_FAILED_WITH_DESC;
								_error_desc = PublicDefine
								        .get_error_description(
								                LoginOrRegistrationActivity.this,
								                699);
							}
						}
						else
						{
							ret = USER_LOGIN_FAILED_WITH_DESC;
							_error_desc = login_res.getMessage();
						}
					}
					else
					{
						ret = USER_LOGIN_FAILED_WITH_DESC;
						_error_desc = PublicDefine.get_error_description(
						        LoginOrRegistrationActivity.this, 699);
					}
				}
				catch (SocketTimeoutException e1)
				{
					e1.printStackTrace();
					Log.d("mbp", "retry #: " + (retry - 1)
					        + " Login exception: " + e1.getLocalizedMessage());
					ret = USER_LOGIN_FAILED_SERVER_UNREACHABLE;
				}
				catch (MalformedURLException e1)
				{
					e1.printStackTrace();
					Log.d("mbp", "retry #: " + (retry - 1)
					        + " Login exception: " + e1.getLocalizedMessage());
					ret = USER_LOGIN_FAILED_SERVER_UNREACHABLE;
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
					Log.d("mbp", "retry #: " + (retry - 1)
					        + " Login exception: " + e1.getLocalizedMessage());
					ret = USER_LOGIN_FAILED_SERVER_UNREACHABLE;
				}
				finally
				{
					if (isCancelled())
					{
						Log.d("mbp", "Login cancelled");
						retry = 0;
					}
				}

			}
			while (--retry > 0);

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{

			switch (result.intValue())
			{
			case USER_LOGIN_SUCCESS:
//				tracker.sendEvent(GA_LOGIN_CATEGORY, "Login Task",
//				        "Login Success", null);
				//usrToken = "VfE-JFpHWY6e51pp3nFx";
				user_login_success(usrName, usrToken);
				break;
			case USER_LOGIN_FAILED_PWD_NOT_CORRECT:
				user_login_failed();
				break;
			case USER_LOGIN_FAILED_SERVER_UNREACHABLE:
//				tracker.sendEvent(GA_LOGIN_CATEGORY, "Login Task",
//				        "Login Failed - Server unreachable", null);
				user_login_failed_to_connect();
				break;
			case USER_LOGIN_FAILED_WITH_DESC:
				user_login_failed_with_desc(_error_desc);
				break;

			default:
				break;
			}
		}
	}

	class RegisterTask extends AsyncTask<String, String, Integer>
	{

		private String	usrName;
		private String	usrPwd;
		private String	usrEmail;
		private String	usrToken;
		private String	_error_desc;

		@Override
		protected Integer doInBackground(String... params)
		{

			usrName = params[0];
			usrEmail = params[1];
			usrPwd = params[2];

			Log.d("mbp", "Register user name: " + usrName);

			int ret = -1;
			try
			{
				UserInformation user_info = User.register(usrName, usrEmail,
				        usrPwd);
				if (user_info != null)
				{
					if (user_info.getStatus() == HttpURLConnection.HTTP_OK)
					{
						ret = USER_REGISTER_SUCCESS;
						usrToken = user_info.getAuthenticationToken();
					}
					else
					{
						ret = USER_REGISTER_FAILED_WITH_DESC;
						_error_desc = user_info.getMessage();
					}
				}
				else
				{
					ret = USER_REGISTER_FAILED_WITH_DESC;
					_error_desc = PublicDefine.get_error_description(null, 699);
				}

			}
			catch (SocketTimeoutException e)
			{
				e.printStackTrace();
				ret = USER_REGISTER_FAILED_SERVER_UNREACHABLE;
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				ret = USER_REGISTER_FAILED_SERVER_UNREACHABLE;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			switch (result.intValue())
			{
			case USER_REGISTER_SUCCESS:

				LoginOrRegistrationActivity.this.user_reg_success(usrName,
				        usrEmail, usrPwd, usrToken);
				break;
			case USER_REGISTER_FAILED_WITH_DESC:
				LoginOrRegistrationActivity.this.user_reg_failed(_error_desc);
				break;
			case USER_REGISTER_FAILED_SERVER_UNREACHABLE:
				LoginOrRegistrationActivity.this.user_reg_failed_to_connect();
				break;

			default:
				break;
			}
		}
	}

	class UsrActivationTask extends AsyncTask<String, String, Integer>
	{

		private String	usrName;
		private String	usrPwd;
		private String	activationKey;
		private String	_error_desc;

		@Override
		protected Integer doInBackground(String... params)
		{

			usrName = params[0];
			usrPwd = params[1];
			activationKey = params[2];

			String http_cmd = PublicDefine.BM_SERVER
			        + PublicDefine.BM_HTTP_CMD_PART + PublicDefine.USR_ACT_CMD
			        + PublicDefine.USR_ACT_PARAM_1 + usrName
			        + PublicDefine.USR_ACT_PARAM_2 + activationKey;

			URL url = null;
			HttpsURLConnection conn = null;
			String response = null;
			int respondeCode = -1;
			int ret = -1;
			try
			{
				url = new URL(http_cmd);
				conn = (HttpsURLConnection) url.openConnection();

				if (ssl_context != null)
				{
					conn.setSSLSocketFactory(ssl_context.getSocketFactory());
				}
				conn.setConnectTimeout(PublicDefine.BM_SERVER_CONNECTION_TIMEOUT);
				conn.setReadTimeout(PublicDefine.BM_SERVER_READ_TIMEOUT);

				respondeCode = conn.getResponseCode();
				if (respondeCode == HttpURLConnection.HTTP_OK)
				{

					ret = USER_ACTIVATE_SUCCESS;
				}
				else
				{
					ret = USER_ACTIVATE_FAILED_WITH_DESC;
					_error_desc = PublicDefine.get_error_description(
					        LoginOrRegistrationActivity.this, respondeCode);
				}

			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
			catch (SocketTimeoutException se)
			{
				// Connection Timeout - Server unreachable ???
				ret = USER_ACTIVATE_FAILED_SERVER_UNREACHABLE;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				// Connection Timeout - Server unreachable ???
				ret = USER_ACTIVATE_FAILED_SERVER_UNREACHABLE;
			}
			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			switch (result.intValue())
			{
			case USER_ACTIVATE_SUCCESS:
				user_act_success(usrName, usrPwd);
				break;
			case USER_ACTIVATE_FAILED_WITH_DESC:
				user_act_failed(_error_desc);
				break;
			case USER_ACTIVATE_FAILED_SERVER_UNREACHABLE:
				user_act_failed_to_connect();
				break;

			default:
				break;
			}
		}
	}

	class ResetPwdTask extends AsyncTask<String, String, Integer>
	{

		private String	usrEmail;
		private String	_error_desc;

		@Override
		protected Integer doInBackground(String... params)
		{

			usrEmail = params[0];

			int ret = -1;
			try
			{
				SimpleJsonResponse res = User.forgotPassword(usrEmail);
				if (res != null)
				{
					if (res.getStatus() == HttpURLConnection.HTTP_OK)
					{
						// reset success
						ret = PWD_RESET_SUCCESS;
					}
					else
					{
						ret = PWD_RESET_FAILED_WITH_DESC;
						_error_desc = res.getMessage();
					}
				}
			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = PWD_RESET_FAILED_SERVER_UNREACHABLE;
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = PWD_RESET_FAILED_SERVER_UNREACHABLE;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			switch (result.intValue())
			{
			case PWD_RESET_SUCCESS:
				LoginOrRegistrationActivity.this
				        .user_reset_password_success(usrEmail);
				break;
			case PWD_RESET_FAILED_WITH_DESC:
				LoginOrRegistrationActivity.this
				        .user_reset_password_failed_with_desc(_error_desc);
				break;
			case PWD_RESET_FAILED_SERVER_UNREACHABLE:
				LoginOrRegistrationActivity.this
				        .user_reset_password_failed_server_down();
				break;

			default:
				break;
			}
		}
	}
}
