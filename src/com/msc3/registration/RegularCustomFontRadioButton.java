package com.msc3.registration;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.blinkhd.FontManager;

public class RegularCustomFontRadioButton extends RadioButton
{

	public RegularCustomFontRadioButton(Context context)
	{
		super(context);

		if (!isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}

	}

	public RegularCustomFontRadioButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		if (!isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}

	public RegularCustomFontRadioButton(Context context, AttributeSet attrs,
	        int defStyle)
	{
		super(context, attrs, defStyle);

		if (!isInEditMode())
		{
			this.setTypeface(FontManager.fontRegular);
		}
	}
}
