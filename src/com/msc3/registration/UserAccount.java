package com.msc3.registration;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;

import com.blinkhd.R;
import com.blinkhd.gcm.AlertData;
import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.GetRemoteStreamModeTask;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.Util;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.PublicDefines;
import com.nxcomm.meapi.device.CamListResponse;
import com.nxcomm.meapi.device.CameraInfo;
import com.nxcomm.meapi.device.DeviceFirmware;
import com.nxcomm.meapi.device.DeviceLocation;
import com.nxcomm.meapi.device.GetCameraInfoResponse;

/**
 * @author phung
 *
 */
/**
 * @author phung
 * 
 */
public class UserAccount
{

	private static final String	str_totalCam	           = "Total_Cameras=";
	private static final String	str_camName	               = "Cam = ";
	private static final String	str_camMac	               = " Mac = ";
	private static final String	str_lastComm	           = "last_comm_from_cam = ";
	private static final String	str_timeDiff	           = " time_up_to_request = ";
	private static final String	str_streamMode	           = " streaming_mode = ";
	private static final String	str_ipUpdatedDate	       = " ipUpdatedDate = ";
	private static final String	str_sysDate	               = " sysDate = ";
	private static final String	str_isCameraActive	       = " Is_camera_Active = ";
	private static final String	str_localIp	               = " local_ip = ";
	private static final String	str_cameraIp	           = " camera_ip = ";
	private static final String	str_isAvailable	           = " isAvailable = ";
	private static final String	str_codec	               = " codec = ";
	private static final String	str_cameraFirmwareVersion	= " cameraFirmwareVersion = ";
	private static final String	str_BRTag	               = "<br>";

	private static final int	CAM_LIST_ENTRY_TOKEN	   = 4;	                       // Cam
	                                                                                       // =
	                                                                                       // xx,
	                                                                                       // Mac
	                                                                                       // =xxx,
	                                                                                       // last_com..
	private static final int	MAC_STR_LEN	               = 12;	                       /*
																							 * Mac
																							 * addr
																							 * format
																							 * :
																							 * xx
																							 * :
																							 * yy
																							 * :
																							 * zz
																							 * :
																							 * tt
																							 * :
																							 * aa
																							 * :
																							 * bb
																							 */
	private static final int	REG_ID_LEN	               = 26;

	private static final int	DIALOG_STORAGE_UNAVAILABLE	= 1;
	private static final long	FIVE_MINUTES	           = 5 * 60 * 1000;
	private String	            sessionToken;
	private SetupData	        savedData;
	private SSLContext	        ssl_context;
	private File	            externalFilesDir;
	private Context	            mContext;

	/**
	 * @param sessionToken
	 * @param savedData
	 * @param ssl_context
	 * @param externalFilesDir
	 * @param mContext
	 */
	public UserAccount(String sessionToken, File externalFilesDir,
	        SSLContext ssl_context, Context mContext) throws StorageException
	{

		this.sessionToken = sessionToken;
		this.mContext = mContext;

		this.externalFilesDir = externalFilesDir;
		this.ssl_context = ssl_context;

		savedData = new SetupData();
		/*
		 * Start by restoring data and add on new camera along the way then Save
		 * when camera is configured
		 */
		try
		{
			if (externalFilesDir != null
			        && savedData.restore_session_data(externalFilesDir))
			{

			}
			else
			{
				Log.e("mbp", "Can't restore session data sth is wrong");
			}
		}
		catch (StorageException e)
		{
			throw e;
		}
	}

	/**
	 * @param dialogStorageUnavailable
	 */
	private void showDialog(int dialogId)
	{
		// TODO Auto-generated method stub
		AlertDialog.Builder builder;
		AlertDialog alert;
		Spanned msg;
		switch (dialogId)
		{
		case DIALOG_STORAGE_UNAVAILABLE:
		{
			builder = new AlertDialog.Builder(mContext);
			msg = Html
			        .fromHtml("<big>"
			                + mContext
			                        .getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setPositiveButton(
			                mContext.getResources().getString(R.string.OK),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
				                }
			                });
			alert = builder.create();
			alert.show();
		}
		}

	}

	public void sync_user_data() throws IOException
	{
		CamProfile[] online_list = query_online_cammera_list();

		// register if it's not iHome Phone
		if (online_list != null)
		{

			/* Purging invalid alert if any */
			ArrayList<String> cameraMacs = new ArrayList<String>();
			for (int i = 0; i < online_list.length; i++)
			{
				cameraMacs.add(online_list[i].get_MAC());
			}
			AlertData.purgeAlertsNotFromCameras(cameraMacs,
			        mContext.getExternalFilesDir(null));

		}

		/*
		 * 20130110: hoang: With new query, we don't need to update available
		 * status
		 */
		// if (online_list!= null)
		// {
		// //applicable for both MBP & G-Phone
		// update_remote_available_status_for_cameras(online_list);
		// }

		sync_online_and_offline_data(online_list);
	}

	public boolean isCameraOnline(String regId)
	{
		boolean isOnline = false;
		try
		{
			PublicDefines.setHttpTimeout(30000);
			GetCameraInfoResponse camlist_res = Device.getCameraInfo(
			        sessionToken, regId);
			if (camlist_res != null)
			{
				Log.d("mbp",
				        "Get cam info response code: "
				                + camlist_res.getStatus());
				if (camlist_res.getStatus() == HttpURLConnection.HTTP_OK)
				{
					CameraInfo caminfo = camlist_res.getCameraInfo();
					/* Donot need to check local ip anymore */
					// if (caminfo != null && caminfo.getDevice_location() !=
					// null)
					if (caminfo != null)
					{
						// if (caminfo.isIs_available() &&
						// caminfo.getDevice_location().getLocalIP() != null)
						if (caminfo.isIs_available())
						{
							isOnline = true;
						}
					}

				}
			}

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}

		return isOnline;
	}

	public static void query_camera_snapshots(CamProfile[] online_list,
	        String user_email, String user_pwd, SSLContext ssl_context)
	{
		URL url = null;
		HttpsURLConnection conn = null;
		int resCode = -1;

		String usr_pass = String.format("%s:%s", user_email, user_pwd);

		for (int i = 0; i < online_list.length; i++)
		{

			CamProfile cam = online_list[i];

			try
			{
				url = new URL(PublicDefine.BM_SERVER
				        + PublicDefine.BM_HTTP_CMD_PART
				        + PublicDefine.GET_IMG_CMD
				        + PublicDefine.GET_IMG_PARAM_1
				        + PublicDefine.strip_colon_from_mac(cam.get_MAC()));
				conn = (HttpsURLConnection) url.openConnection();

				if (ssl_context != null)
				{
					conn.setSSLSocketFactory(ssl_context.getSocketFactory());
				}

				conn.addRequestProperty(
				        "Authorization",
				        "Basic "
				                + Base64.encodeToString(
				                        usr_pass.getBytes("UTF-8"),
				                        Base64.NO_WRAP));

				conn.setConnectTimeout(PublicDefine.BM_SERVER_CONNECTION_TIMEOUT);
				conn.setReadTimeout(PublicDefine.BM_SERVER_READ_TIMEOUT);

				resCode = conn.getResponseCode();
				/* make sure the return type is text before using readLine */
				if (resCode == HttpURLConnection.HTTP_OK)
				{
					String contentType = conn.getContentType();

					if (contentType.startsWith("application/download"))
					{
						Bitmap bmp = BitmapFactory.decodeStream(conn
						        .getInputStream());
						if (bmp != null)
						{
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
							byte[] byteArray = stream.toByteArray();
							cam.getShortClip().add(byteArray);
						}
					}
				}
				else
				{
					// Dont care about other cases
					Log.d("mbp", "Failed to get snapshot from server");
				}

			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}

	}

	private static final String	this_url	= "http://98.130.72.88/ms85478/2b4w00";

	private CamProfile[] query_online_cammera_list() throws IOException
	{
		CamProfile[] list = null;
		boolean server_error = false;
		int retry = 1;
		while (retry > 0)
		{

			try
			{
				PublicDefines.setHttpTimeout(60000);
				CamListResponse camlist_res = Device
				        .getOwnCamList(sessionToken);
				if (camlist_res != null)
				{
					Log.d("mbp",
					        "Get cam list response code: "
					                + camlist_res.getStatus());
					if (camlist_res.getStatus() == HttpURLConnection.HTTP_OK)
					{
						CameraInfo[] caminfos = camlist_res.getCamList();
						try
						{

							/*
							 * 20130110: hoang: fix for new get cam list query
							 * "command=cam_list4"
							 */
							// list = parse_cam_list_temp_new(inputStream);
							list = parse_cam_list_temp_new2(caminfos);
						}
						catch (NumberFormatException nfe)
						{
							throw new IOException("Error parsing input stream");
						}

						break;
					}
					else if (camlist_res.getStatus() == HttpURLConnection.HTTP_INTERNAL_ERROR)
					{
						retry--;
						// after last try we throw...
						if (retry == 0)
						{
							server_error = true;
						}
					}
					else
					{
						retry--;
					}
				}
				else
				{
					retry--;
					// after last try we throw...
					if (retry == 0)
					{
						server_error = true;
					}
				}

			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				retry--;
				// Should not happen
			}
			catch (IOException ioe)
			{
				retry--;
				// after last try we throw...
				if (retry == 0)
				{
					Log.d("mbp", "throw big ioe now ");
					throw ioe;
				}
			}

		}

		if (server_error)
		{
			Log.d("mbp", "throw big ioe now ");
			throw new IOException("Server internal error");
		}

		return list;
	}

	private CamProfile[] parse_cam_list_temp_new2(CameraInfo[] caminfos)
	        throws IOException, NumberFormatException
	{
		if (caminfos == null)
		{
			return null;
		}

		int total_cam = caminfos.length > 4 ? 4 : caminfos.length;
		CamProfile[] online_list = new CamProfile[total_cam];

		for (int i = 0; i < total_cam; i++)
		{
			// get camera name
			String camName = caminfos[i].getName();

			// get mac address
			String registrationId = caminfos[i].getRegistration_id();
			if (registrationId == null || registrationId.length() != REG_ID_LEN)
			{
				registrationId = "00000000000000000000000000";
			}

			String MAC_addr = CamProfile.getMacFromRegId(registrationId);
			MAC_addr = PublicDefine.add_colon_to_mac(MAC_addr);

			// get fw version
			// DeviceFirmware camFwVersion_obj =
			// caminfos[i].getDevice_firmware();
			String camFwVersion = null;
			camFwVersion = caminfos[i].getFirmware_version();

			String planId = null;
			planId = caminfos[i].getPlan_id();

			// get model id
			String camModel = null;
			camModel = CamProfile.getModelIdFromRegId(registrationId);

			int camId = -1;
			camId = caminfos[i].getId();

			Date lastDateUpdate = null;
			lastDateUpdate = caminfos[i].getUpdated_at();
			TimeZone tz = TimeZone.getDefault();
			// String dateFormat = "hh:mm a, d'" +
			// Util.getDayOfMonthSuffix(lastDateUpdate.getDate()) +
			// "' MMMM yyyy";
			SharedPreferences settings = mContext.getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			int clockMode = settings.getInt(PublicDefine.PREFS_CLOCK_MODE,
			        PublicDefine.CLOCK_MODE_12H);
			String dateFormat;
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				dateFormat = "h:mm a, MMM d, yyyy";
			}
			else
			{
				dateFormat = "H:mm, MMM d, yyyy";
			}
			SimpleDateFormat destFormat = new SimpleDateFormat(dateFormat);
			destFormat.setTimeZone(tz);
			String lastUpdate = destFormat.format(lastDateUpdate);

			// get is_available value
			boolean isAvailable = false;
			isAvailable = caminfos[i].isIs_available();

			InetAddress remote_addr = null;
			int remoteRtspPort = -1;
			int remoteRtpVideoPort = -1;
			int remoteRtpAudioPort = -1;
			int remoteTalkBackPort = -1;
			InetAddress local_addr = null;
			int localRtspPort = -1;
			int localRtpVideoPort = -1;
			int localRtpAudioPort = -1;
			int localTalkBackPort = -1;

			// dev_loc now always null
			DeviceLocation dev_loc = caminfos[i].getDevice_location();
			if (dev_loc != null)
			{
				// get remote addr
				String remote_ip = dev_loc.getRemoteIP();
				if (remote_ip != null && !remote_ip.equalsIgnoreCase("null")
				        && !remote_ip.equalsIgnoreCase(""))
				{
					// remote_addr = InetAddress.getByName(remote_ip);
				}

				// get remote RTSP port
				String remoteRtspPort_str = dev_loc.getRemotePort1();
				if (remoteRtspPort_str != null)
				{
					remoteRtspPort = Integer.parseInt(remoteRtspPort_str);
				}

				// get remote RTP Video port
				String remoteRtpVideoPort_str = dev_loc.getRemotePort2();
				if (remoteRtpVideoPort_str != null)
				{
					remoteRtpVideoPort = Integer
					        .parseInt(remoteRtpVideoPort_str);
				}

				// get remote RTP Audio port
				String remoteRtpAudioPort_str = dev_loc.getRemotePort3();
				if (remoteRtpAudioPort_str != null)
				{
					remoteRtpAudioPort = Integer
					        .parseInt(remoteRtpAudioPort_str);
				}

				// get remote TalkBack port
				String remoteTalkBackPort_str = dev_loc.getRemotePort4();
				if (remoteTalkBackPort_str != null)
				{
					remoteTalkBackPort = Integer
					        .parseInt(remoteTalkBackPort_str);
				}

				// get local addr
				String local_ip = dev_loc.getLocalIP();
				if (local_ip != null && !local_ip.equalsIgnoreCase("null")
				        && !local_ip.equalsIgnoreCase(""))
				{
					local_addr = InetAddress.getByName(local_ip);
				}

				// get local RTSP port
				String localRtspPort_str = dev_loc.getLocalPort1();
				if (localRtspPort_str != null)
				{
					localRtspPort = Integer.parseInt(localRtspPort_str);
				}

				// get local RTP Video port
				String localRtpVideoPort_str = dev_loc.getLocalPort2();
				if (localRtpVideoPort_str != null)
				{
					localRtpAudioPort = Integer.parseInt(localRtpVideoPort_str);
				}

				// get local RTP Audio port
				String localRtpAudioPort_str = dev_loc.getLocalPort3();
				if (localRtpAudioPort_str != null)
				{
					localRtpAudioPort = Integer.parseInt(localRtpAudioPort_str);
				}

				// get local TalkBack port
				String localTalkBackPort_str = dev_loc.getLocalPort4();
				if (localTalkBackPort_str != null)
				{
					localTalkBackPort = Integer.parseInt(localTalkBackPort_str);
				}
			}

			// get local addr
			String local_ip = caminfos[i].getLocal_ip();
			if (local_ip != null && !local_ip.equalsIgnoreCase("null")
			        && !local_ip.equalsIgnoreCase(""))
			{
				local_addr = InetAddress.getByName(local_ip);
			}

			online_list[i] = new CamProfile(local_addr, MAC_addr);
			online_list[i].setRegistrationId(registrationId);
			online_list[i].setName(camName);
			online_list[i].setFirmwareVersion(camFwVersion);
			online_list[i].setPlanId(planId);
			online_list[i].setModelId(camModel);
			online_list[i].setCamId(camId);
			online_list[i].setReachableInRemote(isAvailable);
			online_list[i].setLastUpdate(lastUpdate);

			// set camera remote info
			// online_list[i].setRemote_addr(remote_addr);
			online_list[i].setRemoteRtspPort(remoteRtspPort);
			online_list[i].setRemoteRtpVideoPort(remoteRtpVideoPort);
			online_list[i].setRemoteRtpAudioPort(remoteRtpAudioPort);
			online_list[i].setRemoteTalkBackPort(remoteTalkBackPort);

			if (caminfos[i].getFirmware_time() != null)
			{
				TimeZone timezone = TimeZone.getTimeZone("UTC");
				Calendar c = Calendar.getInstance(timezone);
				long time_stamp  = c.getTime().getTime()
				        - caminfos[i].getFirmware_time().getTime();
				
				Log.i("mbp","Time left from update: " + (time_stamp/1000) + " seconds.");
				
				if (time_stamp < FIVE_MINUTES)
				{

					if (caminfos[i].getFirmware_status() == 1)
					{
						online_list[i].setUpgrading(true);
					}
					else
					{
						online_list[i].setUpgrading(false);
					}
				}
				else
				{
					online_list[i].setUpgrading(false);
				}

			}
			else
			{
				online_list[i].setUpgrading(false);
			}

		}

		return online_list;
	}

	private CamProfile[] parse_cam_list_temp(InputStream inputStream)
	        throws IOException, NumberFormatException
	{
		String response = null;
		int total_cam = 0;
		CamProfile[] online_list = null;
		response = ((DataInputStream) inputStream).readLine();
		Log.d("mbp", "Get camera list from server");
		if (response.startsWith(str_totalCam))
		{
			total_cam = Integer.parseInt(response.substring(
			        str_totalCam.length(), str_totalCam.length() + 1));
			if (total_cam > 0)
			{
				total_cam = (total_cam > 4) ? 4 : total_cam;

				online_list = new CamProfile[total_cam];

				// /skip "total_cams=x<br>"
				response = response.substring(str_totalCam.length() + 1 + 4);

				String sub_str = null;
				int i = 0;

				int br_tag_index = response.indexOf(str_BRTag);

				while ((br_tag_index > 0) && (i < total_cam))
				{

					sub_str = response.substring(0, br_tag_index);
					StringTokenizer str_tok = new StringTokenizer(sub_str, ",");

					if (str_tok.countTokens() >= CAM_LIST_ENTRY_TOKEN)
					{
						// UPDATE CameraMenuActivity remove camera code as well
						String str_cam_name = str_tok.nextToken();
						String str_cam_mac = str_tok.nextToken();
						String str_last_com = str_tok.nextToken();
						String str_time_diff = null;
						if (str_tok.hasMoreTokens())
						{
							str_time_diff = str_tok.nextToken();
						}

						String local_ip = null;
						// Support local_ip=xx.xx.xx.xx
						if (str_tok.hasMoreTokens())
						{
							local_ip = str_tok.nextToken();
							if (local_ip.startsWith(str_localIp))
							{
								local_ip = local_ip.substring(str_localIp
								        .length());
							}
							else
							{
								local_ip = null;
							}
						}

						str_cam_name = str_cam_name.substring(str_camName
						        .length());
						str_cam_mac = str_cam_mac
						        .substring(str_camMac.length());// Could be
						                                        // empty
						if (str_cam_mac.length() != MAC_STR_LEN)
						{
							// STH is wrong, set to default, to use the local
							// mac address
							str_cam_mac = "00:00:00:00:00:00";
						}
						else
						{
							str_cam_mac = PublicDefine
							        .add_colon_to_mac(str_cam_mac);
						}

						// Store last comm
						if (str_lastComm.length() < str_last_com.length())
						{
							str_last_com = str_last_com.substring(str_lastComm
							        .length());
						}
						else
						{
							str_last_com = "Unknown";
						}

						// Store time diff
						int _time_diff = -1;

						if (str_time_diff != null
						        && str_timeDiff.length() < str_time_diff
						                .length())
						{
							String int_time_diff = str_time_diff
							        .substring(str_timeDiff.length());
							_time_diff = Integer.parseInt(int_time_diff);

						}

						InetAddress local_addr = null;
						if (local_ip != null
						        && !local_ip.equalsIgnoreCase("null")
						        && !local_ip.equalsIgnoreCase(""))
						{
							local_addr = InetAddress.getByName(local_ip);
						}

						online_list[i] = new CamProfile(local_addr, str_cam_mac);
						online_list[i].setName(str_cam_name);
						online_list[i].setLastCommStatus(str_last_com);

						if (_time_diff >= 0)
						{

							online_list[i].setMinutesSinceLastComm(_time_diff);
						}
						else
						{
							online_list[i]
							        .setMinutesSinceLastComm(100 * 24 * 60 /*
																			 * dummy
																			 * value
																			 * -
																			 * 100
																			 * days
																			 */);
						}
					}
					else
					{

					}

					i++;

					// Skip one entry
					response = response.substring(br_tag_index + 4);
					br_tag_index = response.indexOf(str_BRTag);

				}

			}

		}
		else
		{

		}
		return online_list;

	}

	/*
	 * 20130110: hoang: used for the new query like:
	 * https://www.monitoreverywhere
	 * .com/BMS/phoneservice?action=command&command=
	 * camera_list4&email=blink1.demo@gmail.com
	 */
	private CamProfile[] parse_cam_list_temp_new(InputStream inputStream)
	        throws IOException, NumberFormatException
	{
		String response = null;
		int total_cam = 0;
		CamProfile[] online_list = null;
		response = ((DataInputStream) inputStream).readLine();
		Log.d("mbp", "Get camera list from server");
		if (response.startsWith(str_totalCam))
		{
			total_cam = Integer.parseInt(response.substring(
			        str_totalCam.length(), str_totalCam.length() + 1));
			if (total_cam > 0)
			{
				total_cam = (total_cam > 4) ? 4 : total_cam;

				online_list = new CamProfile[total_cam];

				// /skip "total_cams=x<br>"
				response = response.substring(str_totalCam.length() + 1 + 4);

				String sub_str = null;
				int i = 0;

				int br_tag_index = response.indexOf(str_BRTag);

				while ((br_tag_index > 0) && (i < total_cam))
				{

					sub_str = response.substring(0, br_tag_index);
					StringTokenizer str_tok = new StringTokenizer(sub_str, ",");

					if (str_tok.countTokens() >= CAM_LIST_ENTRY_TOKEN)
					{
						// UPDATE CameraMenuActivity remove camera code as well
						String str_cam_name = str_tok.nextToken();
						String str_cam_mac = str_tok.nextToken();
						String str_last_com = str_tok.nextToken();
						String str_time_diff = null;
						if (str_tok.hasMoreTokens())
						{
							str_time_diff = str_tok.nextToken();
						}
						String str_stream_mode = str_tok.nextToken();
						String str_ip_update_date = str_tok.nextToken();
						String str_sysdate = str_tok.nextToken();
						String str_is_camera_active = str_tok.nextToken();
						String local_ip = null;
						// Support local_ip=xx.xx.xx.xx
						if (str_tok.hasMoreTokens())
						{
							local_ip = str_tok.nextToken();
							if (local_ip.startsWith(str_localIp))
							{
								local_ip = local_ip.substring(str_localIp
								        .length());
							}
							else
							{
								local_ip = null;
							}
						}

						String camera_ip = null;
						if (str_tok.hasMoreTokens())
						{
							camera_ip = str_tok.nextToken();
							if (camera_ip.startsWith(str_cameraIp))
							{
								camera_ip = camera_ip.substring(str_cameraIp
								        .length());
							}
							else
							{
								camera_ip = null;
							}
						}

						String str_is_available = null;
						if (str_tok.hasMoreTokens())
						{
							str_is_available = str_tok.nextToken();
							if (str_is_available.startsWith(str_isAvailable))
							{
								str_is_available = str_is_available
								        .substring(str_isAvailable.length());
							}
							else
							{
								str_is_available = null;
							}
						}

						String codec = null;
						if (str_tok.hasMoreTokens())
						{
							codec = str_tok.nextToken();
							if (codec.startsWith(str_codec))
							{
								codec = codec.substring(str_codec.length());
							}
							else
							{
								codec = null;
							}
						}

						String cameraFirmwareVersion = null;
						if (str_tok.hasMoreTokens())
						{
							cameraFirmwareVersion = str_tok.nextToken();
							if (cameraFirmwareVersion
							        .startsWith(str_cameraFirmwareVersion))
							{
								cameraFirmwareVersion = cameraFirmwareVersion
								        .substring(str_cameraFirmwareVersion
								                .length());
							}
							else
							{
								cameraFirmwareVersion = null;
							}
						}

						str_cam_name = str_cam_name.substring(str_camName
						        .length());
						str_cam_mac = str_cam_mac
						        .substring(str_camMac.length());// Could be
						                                        // empty
						if (str_cam_mac.length() != MAC_STR_LEN)
						{
							// STH is wrong, set to default, to use the local
							// mac address
							str_cam_mac = "00:00:00:00:00:00";
						}
						else
						{
							str_cam_mac = PublicDefine
							        .add_colon_to_mac(str_cam_mac);
						}

						// Store last comm
						if (str_lastComm.length() < str_last_com.length())
						{
							str_last_com = str_last_com.substring(str_lastComm
							        .length());
						}
						else
						{
							str_last_com = "Unknown";
						}

						// Store time diff
						int _time_diff = -1;

						if (str_time_diff != null
						        && str_timeDiff.length() < str_time_diff
						                .length())
						{
							String int_time_diff = str_time_diff
							        .substring(str_timeDiff.length());
							_time_diff = Integer.parseInt(int_time_diff);

						}

						InetAddress local_addr = null;
						if (local_ip != null
						        && !local_ip.equalsIgnoreCase("null")
						        && !local_ip.equalsIgnoreCase(""))
						{
							local_addr = InetAddress.getByName(local_ip);
						}

						online_list[i] = new CamProfile(local_addr, str_cam_mac);
						online_list[i].setName(str_cam_name);
						online_list[i].setLastCommStatus(str_last_com);

						if ((str_is_available != null)
						        && (Integer.parseInt(str_is_available) == 1))
						{
							online_list[i].setReachableInRemote(true);
						}
						else
						{
							online_list[i].setReachableInRemote(false);
						}

						online_list[i].setCodec(codec);
						online_list[i]
						        .setFirmwareVersion(cameraFirmwareVersion);

						// if(_time_diff >= 0 )
						// {
						//
						// online_list[i].setMinutesSinceLastComm(_time_diff);
						// }
						// else
						// {
						// online_list[i].setMinutesSinceLastComm(100*24*60 /*
						// dummy value- 100 days*/);
						// }
					}
					else
					{

					}

					i++;

					// Skip one entry
					response = response.substring(br_tag_index + 4);
					br_tag_index = response.indexOf(str_BRTag);

				}

			}

		}
		else
		{

		}
		return online_list;

	}

	/**
	 * Sync online and offline list Offline list may be shrinked or extended
	 * Offline list will be saved after the synchronization
	 * 
	 * @param online_list
	 *            - online list of CamProfile, query from Server
	 * 
	 */

	private void sync_online_and_offline_data(CamProfile[] online_list)
	{

		CamProfile[] offline_list = savedData.get_CamProfiles();

		if (online_list == null)
		{
			// If online_list == null -> user has no cam online --remove all cam
			// offline too
			savedData.set_CamProfiles(new CamProfile[0]);

			CamChannel[] chs = new CamChannel[4];
			for (int i = 0; i < chs.length; i++)
			{
				chs[i] = new CamChannel(i);
			}
			savedData.set_Channels(chs);

			savedData.set_AccessMode(SetupData.ACCESS_VIA_LAN);
			savedData.set_SSID("NA");// will not be used

			savedData.save_session_data(externalFilesDir);

			return;
		}

		String phonemodel = android.os.Build.MODEL;

		if (offline_list == null)
		{

			if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
			        || phonemodel.equals(PublicDefine.PHONE_MBP1k))
			{
				int i, j;
				/* 20121105 : SET DEFAULT Alert settings to TRUE for new cameras */
				for (j = 0; j < online_list.length; j++)
				{
					online_list[j].setSoundAlertEnabled(true);
					online_list[j].setTempHiAlertEnabled(true);
					online_list[j].setTempLoAlertEnabled(true);
					online_list[j].setMotionAlertEnabled(true);
				}

			}

			savedData.set_CamProfiles(online_list);
		}
		else
		{

			if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
			        || phonemodel.equals(PublicDefine.PHONE_MBP1k))
			{
				/* 20121105 :keep old alert status */
				int i, j;
				for (j = 0; j < online_list.length; j++)
				{
					for (i = 0; i < offline_list.length; i++)
					{
						if (offline_list[i].get_MAC().equalsIgnoreCase(
						        online_list[j].get_MAC()))
						{

							online_list[j].setSoundAlertEnabled(offline_list[i]
							        .isSoundAlertEnabled());
							online_list[j]
							        .setTempHiAlertEnabled(offline_list[i]
							                .isTempHiAlertEnabled());
							online_list[j]
							        .setTempLoAlertEnabled(offline_list[i]
							                .isTempLoAlertEnabled());
							break;
						}
					}

				}

				/* 20121105 : SET DEFAULT Alert settings to TRUE for new cameras */
				for (j = 0; j < online_list.length; j++)
				{
					boolean found = false;
					for (i = 0; i < offline_list.length; i++)
					{
						if (offline_list[i].get_MAC().equalsIgnoreCase(
						        online_list[j].get_MAC()))
						{
							found = true;
							break;
						}
					}

					if (found == false)
					{
						online_list[j].setSoundAlertEnabled(true);
						online_list[j].setTempHiAlertEnabled(true);
						online_list[j].setTempLoAlertEnabled(true);
					}

				}

			}

			// SYNC online/offline list
			offline_list = online_list;

			// update the reference .. yes,needed
			savedData.set_CamProfiles(offline_list);
		}

		if ((savedData.get_AccessMode() == -1)
		        || (savedData.get_Channels() == null))
		{
			// Looks like there is no saved data, create one
			savedData.set_AccessMode(SetupData.ACCESS_VIA_LAN);
			savedData.set_SSID("NA");// will not be used

			CamProfile[] cps = savedData.get_CamProfiles();
			CamChannel[] chs = new CamChannel[4];
			for (int i = 0; i < 4; i++)
			{
				chs[i] = new CamChannel(i);

				if ((i < cps.length) && (cps[i] != null))
				{
					cps[i].bind(true);
					chs[i].setCamProfile(cps[i]);
					cps[i].setChannel(chs[i]);
				}
			}
			savedData.set_Channels(chs);

		}
		else
		{

			CamProfile[] cps = savedData.get_CamProfiles();
			CamChannel[] chs = savedData.get_Channels();

			/* Update the channel list */
			for (int i = 0; i < chs.length; i++)
			{
				// reset the channel
				if (chs[i] != null)
				{
					chs[i].reset();
				}
			}
			for (int j = 0; j < cps.length; j++)
			{

				if (cps[j] != null)
				{
					for (int i = 0; i < chs.length; i++)
					{
						if (chs[i] != null)
						{
							if (chs[i].getState() == CamChannel.CONFIGURE_STATUS_NOT_ASSIGNED)
							{
								cps[j].bind(true);
								chs[i].setCamProfile(cps[j]);
								cps[j].setChannel(chs[i]);
								break;
							}
						}
					}
				}
			}
		}

		/* save data for offline used */
		savedData.save_session_data(externalFilesDir);

	}

	/**
	 * update_remote_available_status_for_cameras
	 * 
	 * @param cameras
	 *            NON-NULL array
	 */
	private void update_remote_available_status_for_cameras(CamProfile[] cameras)
	{
		for (int i = 0; i < cameras.length; i++)
		{
			query_remote_availability_status_for_one_camera(cameras[i]);
		}
	}

	/**
	 * query_remote_availability_status_for_one_camera
	 * 
	 * Query server for camera stream mode first. Then query the camera
	 * availability depending on the stream mode. Set the remote status
	 * accordingly.
	 * 
	 * 
	 * IMPORTANT: This is to be called AFTER querying the camera list. Because
	 * the camera reachable status will be overwritten HERE.
	 * 
	 * @param cp
	 *            - camera profile to set
	 */
	private void query_remote_availability_status_for_one_camera(CamProfile cp)
	{

		GetRemoteStreamModeTask getModeTask = new GetRemoteStreamModeTask(null,
		        mContext);
		getModeTask.execute(cp.getRegistrationId(), sessionToken);

		Integer streamMode;
		try
		{
			streamMode = getModeTask.get(10, TimeUnit.SECONDS);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			streamMode = GetRemoteStreamModeTask.STREAM_MODE_UNKNOWN;
		}
		catch (ExecutionException e)
		{
			e.printStackTrace();
			streamMode = GetRemoteStreamModeTask.STREAM_MODE_UNKNOWN;
		}
		catch (TimeoutException e)
		{
			e.printStackTrace();
			streamMode = GetRemoteStreamModeTask.STREAM_MODE_UNKNOWN;
		}

		switch (streamMode.intValue())
		{

		case GetRemoteStreamModeTask.STREAM_MODE_UDT:
			query_availablility_status_for_cam(
			        PublicDefine.IS_CAM_AVAILABLE_ONLOAD_CMD, cp);
			break;
		case GetRemoteStreamModeTask.STREAM_MODE_MANUAL_PORT_FWD:
		case GetRemoteStreamModeTask.STREAM_MODE_UPNP:
			query_availablility_status_for_cam(
			        PublicDefine.IS_CAM_AVAILABLE_UPNP_CMD, cp);
			break;
		// could be some errors while connecting..
		case GetRemoteStreamModeTask.STREAM_MODE_UNKNOWN:
			break;
		default:
			break;
		}

		return;
	}

	/*
	 * Check camera availability IS_CAM_AVAILABLE_ONLOAD_ - udt cameras
	 * IS_CAM_AVAILABLE_UPNP_CMD - upnp cameras
	 * 
	 * response: <Cam_Mac_NO_COLON>: \n<Status> 2 lines.... f*** dumb idiots
	 * <status> could be one of these string: AVAILABLE : camera is up & running
	 * ERROR : timeout in checking or other error (however it takes quite a long
	 * time to show this) BUSY : camera is busy - someone is viewing..
	 * 
	 * eg: 000DA3120583: AVAILABLE
	 */

	private void query_availablility_status_for_cam(String stunORUpnp,
	        CamProfile cp)
	{
		// String macAddress= PublicDefine.strip_colon_from_mac(cp.get_MAC());
		//
		// String http_cmd = PublicDefine.BM_SERVER +
		// PublicDefine.BM_HTTP_CMD_PART +
		// stunORUpnp +
		// PublicDefine.IS_CAM_AVAILABLE_ONLOAD_PARAM_1+ macAddress ;
		//
		//
		// URL url = null;
		// HttpsURLConnection conn = null;
		// DataInputStream inputStream = null;
		// String response = null;
		// int respondeCode = -1;
		//
		// Log.d("mbp", "Query Remote avai: " + http_cmd);
		//
		// String usr_pass = String.format("%s:%s", this.user_email,
		// this.user_pwd);
		//
		// try {
		// url = new URL(http_cmd );
		// conn = (HttpsURLConnection)url.openConnection();
		//
		// conn.addRequestProperty("Authorization", "Basic " +
		// Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
		// conn.setConnectTimeout(PublicDefine.BM_SERVER_CONNECTION_TIMEOUT);
		// conn.setReadTimeout(PublicDefine.BM_SERVER_READ_TIMEOUT);
		//
		// respondeCode = conn.getResponseCode();
		// if (respondeCode == HttpURLConnection.HTTP_OK)
		// {
		// inputStream = new DataInputStream(
		// new BufferedInputStream(conn.getInputStream(),4*1024));
		// byte [] buffer = new byte[30];
		// inputStream.read(buffer);
		//
		// response = new String(buffer, "UTF-8");
		//
		// Log.d("mbp", "Remote avai success1 : " + response);
		//
		//
		// //Parse the list
		// String [] tokens = response.split(":");
		//
		// if (tokens.length ==2)
		// {
		// String cam_status = tokens[1].trim();
		//
		// if (cam_status.startsWith("AVAILABLE") ||
		// cam_status.startsWith("BUSY"))
		// {
		// Log.e("mbp", "Set Remote available for cam: " + cp.get_MAC() );
		// cp.setReachableInRemote(true);
		// }
		// else
		// {
		// cp.setReachableInRemote(false);
		// }
		//
		// }
		// else
		// {
		// Log.e("mbp", "Remote avai Response error");
		// }
		//
		// }
		// else
		// {
		// Log.e("mbp","Remote avai error code: " + respondeCode);
		// }
		//
		//
		// } catch (MalformedURLException e) {
		// e.printStackTrace();
		// //Connection Timeout - Server unreachable ???
		// } catch (SocketTimeoutException se)
		// {
		// /////THIS COULD BE THE CAMERA IS NOT AVAILABLE
		// //Connection Timeout - Server unreachable ???
		// Log.e("mbp", "Remote avai Response exception: " +
		// se.getLocalizedMessage());
		// //Set camera unreachable
		// cp.setReachableInRemote(false);
		//
		// }
		// catch (IOException e) {
		// e.printStackTrace();
		// //Connection Timeout - Server unreachable ???
		// Log.e("mbp", "Remote avai Response exception: " +
		// e.getLocalizedMessage());
		// //Set camera unreachable
		// cp.setReachableInRemote(false);
		// }

		return;
	}

}
