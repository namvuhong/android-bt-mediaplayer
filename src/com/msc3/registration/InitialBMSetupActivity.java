package com.msc3.registration;

import com.msc3.CameraConnectivityDetector;
import com.msc3.CameraDetectorService;
import com.msc3.ConnectToNetworkActivity;
import com.msc3.PublicDefine;
import com.blinkhd.R;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author MC
 *
 * Created based on BabyMonitor function Rev 3.3.doc
 *  This activity is similar to LoginOrRegistrationActivity.java. 
 *  The only different is that, there is only one choice for user to choose that is "Router Mode".
 *  
 *   
 * I don't want to change LoginOrRegistrationActivity any more.. 
 * 
 * 
 * modified based on BabyMonitor_setup rev_1.doc .. may the intelligence god shine on these ppl..
 * 
 *
 */
public class InitialBMSetupActivity extends Activity {
	
	private static final int REQUEST_CONNECTIVITY_FOR_FIRST_TIME = 0x1111; 
	private static final int REQUEST_CREATE_ACCOUNT_FOR_FIRST_TIME = 0x3333; 
	
	private Initial_setup_step current_setup_step;
	
	/*private static final int SETUP_ABORTED = -1; 
	private static final int START = 0;
	private static final int CONNECT_WIFI = 0;
	private static final int TIME_SETTING = 1;
	private static final int CREATE_ACCOUNT = 2;
	private static final int INSTALL_CAMERA = 3;*/
	
	private enum Initial_setup_step {SETUP_ABORTED, START,CONNECT_WIFI,  TIME_SETTING,CREATE_ACCOUNT, INSTALL_CAMERA };
	
	
	private boolean isInstalled = false;
	private String TIME_SETTING_APP_PACKAGE = "com.msc3.TimeSetting";
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		current_setup_step = Initial_setup_step.CONNECT_WIFI ;//connect to WIFI 
	}
	protected void onPause() {
		super.onPause();
	}


	protected void onStop() {
		super.onStop();
		
	}
	protected void onRestart() {
		super.onRestart();
		
	}


	protected void onResume()
	{
		super.onResume();
	}
	protected void onStart() {
		
		Log.d("mbp", "initial setup: onStart() ");
		super.onStart();

		/* 
		 * 20130403: hoang: fix force close when initial setup.
		 * if CameraDetectorService is running, stop it
		 */
		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) ||
				phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			if (FirstTimeActivity.isCamDetectServiceRunning(this))
			{
				Log.d("mbp", "Stop Cam Detector");
				Intent i = new Intent(this, CameraDetectorService.class);
				stopService(i);
			}
		}
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String wifi_SSID = settings.getString(PublicDefine.PREFS_SAVED_WIFI_SSID, null);
		String wifi_pwd = settings.getString(PublicDefine.PREFS_SAVED_WIFI_PWD, null);
		WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);

		if (current_setup_step == Initial_setup_step.SETUP_ABORTED)
		{
			//abort abort abort
			Log.d("mbp", "Setup aborted ... finish()");
			finish(); 
		}
		else if(current_setup_step == Initial_setup_step.TIME_SETTING) // Time Setting step
		{
			if(phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k)) // iHome Phone
			{	
				timesetting(TIME_SETTING_APP_PACKAGE);
			}
			else // not iHome Phone
			{
				current_setup_step = Initial_setup_step.CREATE_ACCOUNT;
			}
		}
		if(current_setup_step == Initial_setup_step.CREATE_ACCOUNT) // Time Setting done, goto Create Account
		{
			connectDoneLayout();
		}
		else if(current_setup_step == Initial_setup_step.INSTALL_CAMERA) // Create Account done, goto final step
		{
			createAccDoneLayout(); ///-> finish()
		}
		else if((current_setup_step == Initial_setup_step.START) &&  
				wifi_SSID != null  &&  wifi_pwd != null  &&
				wm.getConnectionInfo() != null  &&  wm.getConnectionInfo().getSSID() != null  &&
				wifi_SSID.equals(wm.getConnectionInfo().getSSID()) == true)
		{
			connectDoneLayout();
		}
		else 
		{
			dolayout();
		}
	}
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
	}
	
	protected Dialog onCreateDialog(int id)
	{
		return null;
	}
	
	
	private void dolayout()
	{
		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			setContentView(R.layout.bb_is_steps_screen_1);
		}
		else 
		{
			setContentView(R.layout.bb_is_steps_screen);
		}
		
		TextView title = (TextView)findViewById(R.id.textTitle);
		title.setText(R.string.baby_monitor_setup);
		
		Button startSetup = (Button) findViewById(R.id.buttonNext);
		startSetup.getBackground().setColorFilter(0xff7fae00, android.graphics.PorterDuff.Mode.MULTIPLY);
		startSetup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Current WORKING CODE 
				Intent entry = new Intent(InitialBMSetupActivity.this,ConnectToNetworkActivity.class);
				entry.putExtra(ConnectToNetworkActivity.bool_ForceChooseWifi, true);
				startActivityForResult(entry,REQUEST_CONNECTIVITY_FOR_FIRST_TIME);
				
				
				
			}
		});
		
		
		//skip setup Wizard and jump to login page -- 
		Button skip = (Button) findViewById(R.id.buttonSkip);
		skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//finish();
				Log.d("mbp","Skipping");
				Intent entry = new Intent(InitialBMSetupActivity.this,LoginOrRegistrationActivity.class);
				startActivity(entry);
				InitialBMSetupActivity.this.finish();
			}
		});
		
		
	}
	
	private void  createAccDoneLayout()
	{
		//Set up the first screen for iHome Phone
		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k) )
		{
			setContentView(R.layout.bb_is_create_account_end_screen_1);
		}
		else 
		{
			setContentView(R.layout.bb_is_create_account_end_screen);
		}

		TextView title = (TextView)findViewById(R.id.textTitle);
		title.setText(R.string.baby_monitor_setup);	
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		final String saved_usr = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_USR, null);
		final String saved_pwd= settings.getString(PublicDefine.PREFS_SAVED_PORTAL_PWD, null);
		final String saved_id = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_ID, null);
		final String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		
		Button nextStep = (Button) findViewById(R.id.nextStep);
		nextStep.getBackground().setColorFilter(0xff7fae00, android.graphics.PorterDuff.Mode.MULTIPLY);
		nextStep.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				
				Intent cam_conf = new Intent(InitialBMSetupActivity.this,
							SingleCamConfigureActivity.class);
				
				cam_conf.putExtra(SingleCamConfigureActivity.str_userName, saved_usr);
				cam_conf.putExtra(SingleCamConfigureActivity.str_userPass, saved_pwd);
				cam_conf.putExtra(SingleCamConfigureActivity.str_userToken, saved_token);

				startActivity(cam_conf);
				finish();//no going back ..sure??

			}
		});

		TextView usrName = (TextView) findViewById(R.id.t5_2);
		if (usrName != null)
		{
			usrName.setText("\'" + saved_id + "\'");
		}
		TextView usrEmail = (TextView) findViewById(R.id.t5_3);
		if (usrEmail != null)
		{
			usrEmail.setText("with " + saved_usr); 
		}
		
		return; 
	}
	
	private void connectDoneLayout()
	{	
		//Set up screen for iHome phone
		String phonemodel = android.os.Build.MODEL;
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			setContentView(R.layout.bb_is_wifi_end_screen_1);
		}
		else
		{
			setContentView(R.layout.bb_is_wifi_end_screen);
		}
		
		TextView title = (TextView)findViewById(R.id.textTitle);
		title.setText(R.string.baby_monitor_setup);
		
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String ssid_ = settings.getString(PublicDefine.PREFS_SAVED_WIFI_SSID, "Wifi network"); 
		TextView ssid = (TextView)findViewById(R.id.t3_2);
		ssid.setText(ssid_); 
		
		Button startSetup = (Button) findViewById(R.id.nextStep);
		startSetup.getBackground().setColorFilter(0xff7fae00, android.graphics.PorterDuff.Mode.MULTIPLY);
		startSetup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Go on to create account 
				Log.d("mbp", "create account now !!");
				Intent entry = new Intent(InitialBMSetupActivity.this,LoginOrRegistrationActivity.class);
				entry.putExtra(LoginOrRegistrationActivity.bool_createUserAccount, true);
				
				//20120726:  let it control the Steps page
				startActivityForResult(entry, REQUEST_CREATE_ACCOUNT_FOR_FIRST_TIME);
			}
		});
	}
	protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQUEST_CONNECTIVITY_FOR_FIRST_TIME)
		{
			if  (resultCode == Activity.RESULT_OK)
			{
				current_setup_step = Initial_setup_step.TIME_SETTING; // connect to WIFI done
			}
			else
			{
				current_setup_step = Initial_setup_step.START; 
			}
		}
		else if (requestCode == REQUEST_CREATE_ACCOUNT_FOR_FIRST_TIME)
		{
			if  (resultCode == Activity.RESULT_OK)
			{
				current_setup_step = Initial_setup_step.INSTALL_CAMERA;			
			}
			else if (resultCode == Activity.RESULT_CANCELED)
			{
				
				current_setup_step = Initial_setup_step.SETUP_ABORTED; 
			}
		}
		
	}
	
	private static final String TIME_SETTING_INTENT = "com.msc3.custom.intent.action.SET_TIME";
	
	private void timesetting(String app_package)
	{
		PackageManager pm = getPackageManager();
		try
		{
			pm.getPackageInfo(app_package, PackageManager.GET_ACTIVITIES);
			isInstalled = true;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			isInstalled = false;
		}
		if(isInstalled==true)
		{
			Log.d("mbp", "222- App is installed---- ");
			
			//Broadcast to launch
	        Intent i = new Intent();
	        i.setAction(TIME_SETTING_INTENT);
	        sendBroadcast(i);
			
			
			//original code 
			//Intent intent = pm.getLaunchIntentForPackage(app_package);
			//startActivity(intent);
		}
		else
		{
			Log.d("mbp", "App is not installed");
		}
		current_setup_step = Initial_setup_step.CREATE_ACCOUNT;
	}

}
