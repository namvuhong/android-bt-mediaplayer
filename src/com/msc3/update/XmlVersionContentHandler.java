package com.msc3.update;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XmlVersionContentHandler extends DefaultHandler{

	// ===========================================================
	// Fields
	// ===========================================================

	private boolean in_versionName = false;
	private boolean in_versionCode = false;
	private boolean in_downloadableFileName = false;
	
	
	private static final String version_str = "app-version";
	private static final String versionName_str = "versionName";
	private static final String versionCode_int = "versionCode";
	private static final String downloadableFileName_str = "downloadableFileName";

	private ParsedDataSet myParsedExampleDataSet = new ParsedDataSet();

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public ParsedDataSet getParsedData() {
		return this.myParsedExampleDataSet;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	@Override
	public void startDocument() throws SAXException {
		this.myParsedExampleDataSet = new ParsedDataSet();
	}

	@Override
	public void endDocument() throws SAXException {
		// Nothing to do
	}

	/** Gets be called on opening tags like:
	 * <tag>
	 * Can provide attribute(s), when xml was like:
	 * <tag attribute="attributeValue">*/
	@Override
	public void startElement(String namespaceURI, String localName,
			String qName, Attributes atts) throws SAXException 
	{

		if (qName.equalsIgnoreCase(versionName_str))
		{
			in_versionName = true;
		}
		else if (qName.equalsIgnoreCase(versionCode_int))
		{
			in_versionCode = true;
		}
		else if (qName.equalsIgnoreCase(downloadableFileName_str))
		{
			in_downloadableFileName = true;
		}
		
	}

	/** Gets be called on closing tags like:
	 * </tag> */
	@Override
	public void endElement(String namespaceURI, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase(versionName_str))
		{
			in_versionName = false;
		}
		else if (qName.equalsIgnoreCase(versionCode_int))
		{
			in_versionCode = false;
		}
		else if (qName.equalsIgnoreCase(downloadableFileName_str))
		{
			in_downloadableFileName = false;
		}
	}

	/** Gets be called on the following structure:
	 * <tag>characters</tag> */
	@Override
	public void characters(char ch[], int start, int length) {
		if(this.in_versionName){
			myParsedExampleDataSet.setVersionName(new String(ch, start, length));
		}
		if(this.in_versionCode){
			myParsedExampleDataSet.setVersionCode(Integer.parseInt(new String(ch, start, length)));
		}
		if (this.in_downloadableFileName)
		{
			myParsedExampleDataSet.setDownloadableFileName(new String(ch, start, length));
		}
	}
}