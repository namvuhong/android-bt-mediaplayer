package com.msc3.update;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.conn.ConnectTimeoutException;

import com.msc3.PublicDefine;
import com.blinkhd.R;
import com.msc3.comm.HTTPRequestSendRecv;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;

public class CheckVersionFW extends AsyncTask<String, String, String>{
	private static final String MY_DEBUG_TAG 	= "mbp";
	private static final String no_patch		= "0";
	private static final String no_patch1		= "check_fw_upgrade: -1";
	private static final String processing		= "processing";
	private static final String upgrade_timeout	= "upgrade_timeout";
	private static final String progress		= "burning_process :";
	private static final String user 			= "msc2000" ;
	private static final String pass			= "patch2012";
	public static final String CHECK_FW_UPGRADE = "check_fw_upgrade";
	public static final String REQUEST_FW_UPGRADE = "request_fw_upgrade";

	private static final long TIME_OUT = 6*60000;

	public static final int PATCH_AVAILABLE = 0xde000005;
	public static final int UPGRADE_DONE	= 0xde000008;
	public static final int UPGRADE_FAILED	= 0xde000009;

	private Context mContext;
	private Handler mHandler;
	private boolean check_status;
	private String device_ip;
	private String device_version;
	private IpAndVersion parse_object;
	private Message mess;
	private ProgressDialog dialog;
	private String new_version;
	private String device_mac;
	private String portalUser, portalPass;

	public CheckVersionFW (Context c, Handler h, boolean check, 
			String newVersion, String mac) 
	{
		new_version = newVersion;
		check_status = check;
		mContext = c;
		mHandler = h;
		device_mac = mac;

	}


	private static final char[] _CNAME = {0x43, 0x48, 0x49, 0x4E, 0x41 };
	private static final char[] _CCODE = {0x43, 0x4E };
	private static final String _CODE_HTTP_CHECK = 
			"http://api.ipinfodb.com/v3/ip-city/?key=d32404a074fea8426a5ebf8237e7e1603bca43ec63349c20da0e4a07bad79797";


	protected String doInBackground(String... params) {
		String response = null;
		String status = null;
		int percent = -100, sleep_time = 5000; 
		String usr_pass =  String.format("%s:%s", user, pass);
		String process_burning;
		URL url;
		HttpURLConnection conn;
		DataInputStream inputStream;

		//send a query 
		String http_cmd = String.format("http://%s:%s%s%s", 
				params[0],params[1],params[2],params[3]);
		Log.e(MY_DEBUG_TAG, "Check firmware upgrade");
		device_ip = params[0];

		if (params[3].equals(CHECK_FW_UPGRADE))
		{
			//get a response to analyze
			response = HTTPRequestSendRecv.sendRequest_block_for_response(http_cmd, user, pass);
			try {
				Thread.sleep(sleep_time);
			} catch (InterruptedException e) {
			}
			if (response != null)
			{
				status = response.trim();
			}
			else
			{
				status = null; 
			}
		}
		// if this is REQUEST_FW_UPGRADE then...
		else if(params[3].equals(REQUEST_FW_UPGRADE))
		{
			response = HTTPRequestSendRecv.sendRequest_block_for_response(http_cmd, user, pass);

			Log.e(MY_DEBUG_TAG, "respone >>> "+response);
			try {
				Thread.sleep(5*1000);
			} catch (Exception e) 
			{
			}

			status = processing;		
			boolean device_reset = false, start_counting = false; 
			String http_command = "http://"+device_ip+":"+"8080"+"/cgi-bin/fullupgrade";

			long endTime = System.currentTimeMillis() + TIME_OUT;
			boolean continue_upgrade = true;
			while ((device_reset == false) && (continue_upgrade == true))
			{
				try{
					url = new URL(http_command);
					conn = (HttpURLConnection)url.openConnection();
					conn.addRequestProperty("Authorization", "Basic " +
							Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
					conn.setConnectTimeout(15000);
					conn.setReadTimeout(10000);

					inputStream = new DataInputStream(new BufferedInputStream(conn.getInputStream(),4*1024));
					process_burning = inputStream.readLine();
					inputStream.close();

					try 
					{
						Log.e(MY_DEBUG_TAG, "% upgrade: "+Integer.parseInt(process_burning.substring(progress.length())));

						percent =Integer.parseInt(process_burning.substring(progress.length())) ;
						if(process_burning!=null && percent>=0) 
						{
							Log.e(MY_DEBUG_TAG, "process_burning: >>>"+process_burning);
							// show the progress
							String displayMessage = "Upgrading firmware to version " + new_version + ": " + percent + "% complete.";
							publishProgress(displayMessage);
						}
						if(percent>0)
						{
							start_counting = true; 
						}
						if(percent>60)
						{
							sleep_time = 500;
						}
						if (((percent == 100)||(percent==-1)) 
								&& (start_counting  == true))
						{
							device_reset = true; 
							//break;
						}

					}
					catch (NumberFormatException nfe)
					{

					}

					Log.e(MY_DEBUG_TAG,"Start to sleep for 3-5s");

				}
				catch(ConnectTimeoutException cet)
				{
					cet.printStackTrace();
					//break;
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}	
				try {
					Thread.sleep(sleep_time);
				} catch (InterruptedException e) {
				}

				//timeout after 5min
				if (System.currentTimeMillis() > endTime)
				{
					continue_upgrade = false;
					status = upgrade_timeout;
				}
			}

		}
		else
		{
		}


		return status;
	}

	protected void onPreExecute()
	{
		if(check_status==true)
		{
			Spanned msg = Html.fromHtml("<big>"+mContext.getString(R.string.camera_is_upgrading_please_do_not_power_off_)+"</big>");
			Log.e(MY_DEBUG_TAG, "onPreExecute...");
			dialog = new ProgressDialog(mContext);
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.show();
		}
	}	

	protected void onProgressUpdate(String... progress) 
	{
		if(check_status==true) 
		{
			Spanned msg = Html.fromHtml("<big>"+progress[0]+"</big>");
			dialog.setMessage(msg);
		}
	}

	protected void onPostExecute(String result)
	{
		if (dialog != null)
		{
			dialog.dismiss();
		}
		if (result == null)
		{
			Log.e(MY_DEBUG_TAG, "some error while checking version .. skip silencely");
		}
		else if (result.equals(upgrade_timeout))
		{
			Log.e(MY_DEBUG_TAG, "Upgrade timeout...");
			mess = Message.obtain(mHandler, UPGRADE_FAILED);
			mHandler.dispatchMessage(mess);
		}
		else if (result.equals(processing)) 
		{
			Log.e(MY_DEBUG_TAG, "Upgrade processing is complete...");
			//send a message to EntryActivity in order to exit AsyncTask
			mess = Message.obtain(mHandler, UPGRADE_DONE);
			mHandler.dispatchMessage(mess);
		}
		else if (result.equals(no_patch) || result.equals(no_patch1))
		{
		}
		else
		{
			Log.e(MY_DEBUG_TAG, "Found newer version for this FW -- send message ");
			if (device_ip!=null)
			{
				device_version = result; //get version string 
				parse_object = new IpAndVersion(device_ip, device_version); //package all into an object
				mess = Message.obtain(mHandler, CheckVersionFW.PATCH_AVAILABLE, parse_object);
				mHandler.dispatchMessage(mess);
			}
			else
			{
				Log.e(MY_DEBUG_TAG, "device ip is NULL.. skip this for now");
			}
		}
	}
}
