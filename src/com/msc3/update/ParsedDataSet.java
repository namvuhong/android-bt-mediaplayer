package com.msc3.update;

public class ParsedDataSet {
    private String versionName = null;
    private String downloadableFileName = null;
    private int versionCode = 0;

    public String getVersionName() {
            return versionName;
    }
    public void setVersionName(String extractedString) {
            this.versionName = extractedString;
    }

    public int getVersionCode() {
            return versionCode;
    }
    public void setVersionCode(int extractedInt) {
            this.versionCode = extractedInt;
    }
    
    
    public String getDownloadableFileName()
    {
    	return downloadableFileName;
    }
    public void setDownloadableFileName(String extractedString) {
    	downloadableFileName = extractedString;
    }
    
    
    public String toString(){
            return "ExtractedString = " + this.versionName
                            + "nExtractedInt = " + this.versionCode;
    }
}