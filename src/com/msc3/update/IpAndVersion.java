package com.msc3.update;

public class IpAndVersion {
	public String device_ip;
	public String device_version;
	
	public IpAndVersion(String m_device_ip, String m_device_version)
	{
		device_ip = m_device_ip;
		device_version = m_device_version;
	}
}
