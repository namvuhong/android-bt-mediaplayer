package com.msc3.update;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.List;

import com.blinkhd.R;
import com.msc3.WifiScan;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class AppDownloader extends AsyncTask<URL , String, String> {

	
	
	private Context mContext;
	private ProgressDialog dialog;
	public AppDownloader(Context c)
	{
		mContext = c;
	}
	
	/* on UI Thread */
	protected void onPreExecute()
	{
		String msg = mContext.getString(R.string.downloading_please_wait) ; 
		Spanned msg_1 = Html.fromHtml("<big>"+msg+"</big>");
		dialog = new ProgressDialog(mContext);
		dialog.setMessage(msg_1);
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		dialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				//WifiScan.this.cancel(false);
				AppDownloader.this.cancel(true);
				dialog.dismiss();
			}
		});
		dialog.setButton(mContext.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		dialog.show();

	}

	/* on UI Thread */
	protected void onProgressUpdate(String... progress) {

		dialog.setMessage(progress[0]);
	}

	protected void  onCancelled ()
	{
		//onCancelled(null);
		onCancelled();
	}

	protected void onCancelled(List<ScanResult> result)
	{
		if (dialog != null && dialog.isShowing())
		{
			dialog.dismiss();
		}
		Log.d("mbp","onCancel called");

	}

	protected void onPostExecute (final String results)
	{
		if (dialog != null && dialog.isShowing())
		{
			dialog.dismiss();
		}
		
		
		if ( results != null)
		{
			/*20120322: start install directly - coz user has accepted before download starts*/
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(results)), "application/vnd.android.package-archive");
			mContext.startActivity(intent);
		}
		else //result == null 
		{
			AlertDialog.Builder  builder;
			AlertDialog alert;
			String msg = String.format(mContext.getString(R.string.download_software_failed_with_error_s),download_err );
			Spanned _msg= Html.fromHtml("<big>"+msg+"</big>");

			builder = new AlertDialog.Builder(mContext);
			builder.setMessage(_msg)
			.setCancelable(false)
			.setPositiveButton(mContext.getResources().getString(R.string.OK), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();

			alert.show();
		}
	}
	
	
	
	private String APP_INSTALLER = "_mbp_new.apk";
	private String download_err = null;

	@Override
	protected String doInBackground(URL... params) {

		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;

		
		
		/* Check if the External Storage is available and writeable */
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.e("mbp", "External Storage is mounted as READONLY!");
			return null;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			return null;
		}

		
		/* create a data file in the external dir */
		File file = new File(mContext.getExternalCacheDir(),APP_INSTALLER );
		if (file.exists())
		{
			Log.e("mbp", "File exist, remove it");
			/* remove the old file */
			file.delete();
		}

		FileOutputStream os = null;
		try {
			os = new FileOutputStream(file);
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return null;
		}
			
		
		String fileName = null;
		String usr_pass =  String.format("%s:%s", "msc2000", "patch2012");
		byte buff[] = new byte[1024];
		int byte_read = -1;
		try {
			url = params[0];
			conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			if (((HttpURLConnection) conn).getResponseCode() == 401)
			{
				((HttpURLConnection) conn).disconnect();
				return null;
			}
			

			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream()));
			
			while ((byte_read = inputStream.read(buff)) > 0)
			{
				os.write(buff, 0, byte_read);
				
				if (isCancelled())
				{
					//Simply close inputstream to force exception
					inputStream.close();
				}
			} 
			
			os.flush();
			os.close();
			
			
			fileName = file.getAbsolutePath();
			Log.e("mbp", "File download complete! at:" + fileName);
		} 
		catch (SocketTimeoutException se)
		{
			Log.d("mbp","SocketTimeoutException: " + se.getLocalizedMessage());
			download_err =  se.getLocalizedMessage();
			fileName = null;
		}
		catch (ConnectException ce)
		{
			Log.d("mbp","ConnectException: " + ce.getLocalizedMessage());
			download_err =  ce.getLocalizedMessage();
			fileName = null;
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
			download_err =  e.getLocalizedMessage();
			fileName = null;
		} 
		catch (IOException e) {
			e.printStackTrace();
			download_err =  e.getLocalizedMessage();
			fileName = null;
		} 
		
		
		if (isCancelled())
		{
			return null;
		}
		
		return fileName;
	}

}
