package com.msc3.update;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.msc3.PublicDefine;
import com.blinkhd.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager.BadTokenException;
import android.widget.Toast;

import android.app.Service;

public class CheckAppVersion extends AsyncTask<String , String, ParsedDataSet> {

	
	
	public static final int NEW_APP_AVAILABLE = 1; 
	public static final int NO_UPDATE = 2; 
	
	private static final String MY_DEBUG_TAG = "mbp";
	
	private static final String APP_RELEASE_URL = "http://ota.monitoreverywhere.com/ota/App_release/";
	//URL used for updating Google Phone
	private static final String APP_VERSION_URL = "http://ota.monitoreverywhere.com/ota/App_release/app_version.xml";
	//URL used for updating MBP Phone
	private static final String MBP_APP_VERSION_URL = "http://ota.monitoreverywhere.com/ota/App_release/mbp_app_version.xml";
	
	private static final String user  = "msc2000";
	private static final String pass = "patch2012";
	
	private Context mContext;
	private Handler mHandler; 
	
	private boolean reportNoUpdate = false;
	
	public CheckAppVersion(Context c)
	{
		mContext = c;
		mHandler = null; 
	}
	
	public CheckAppVersion(Context c, Handler mHandler)
	{
		mContext = c;
		this.mHandler = mHandler; 
	}
	
	protected void onPreExecute ()
	{

	}

	public void setReportNoUpdate(boolean b)
	{
		reportNoUpdate = b; 
	}

	protected void onPostExecute (ParsedDataSet result)
	{
		PackageInfo pinfo;

		if (result == null)
		{
			Log.d(MY_DEBUG_TAG,"Update Server unreachable" );

			if (mHandler != null)
			{
				Message m; 
				m = Message.obtain(mHandler, NO_UPDATE);
				mHandler.dispatchMessage(m);
			}
			
			return; //Server not contactable
		}
		
		
		if (mContext == null) // could be killed . 
		{
			//simply return here.. next time we can check again
			Log.d(MY_DEBUG_TAG,"mContext is NULL " );
			

			if (mHandler != null)
			{
				Message m; 
				m = Message.obtain(mHandler, NO_UPDATE);
				mHandler.dispatchMessage(m);
			}
			
			
			return; 
		}
		
		try {
			pinfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
			int versionNumber = pinfo.versionCode;
			int versionNo = result.getVersionCode();
			String versionStr  = result.getVersionName();
			
			
			
			
			if (versionNumber >= versionNo)
			{
				
				
				//same version - no update needed
				Log.d(MY_DEBUG_TAG,"No update -" );
				if ( reportNoUpdate == true)
				{
					createDialogNoUpdate().show();
				}
				
			}
			else
			{
				
				
				String filename = result.getDownloadableFileName();
				Log.d(MY_DEBUG_TAG,"new verion: " +versionNo + " - update file: " + filename);
				createDialogDownload(filename,versionStr).show();
				
				return; 
			}
			
			
			
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (BadTokenException badTok)
		{
			/* May thows by dialog.show() if activity is destroyed 
			 * - Simply don't do anything..we are zombies now, R.I.P.
			 * */
			
		}
		
		

		if (mHandler != null)
		{
			Message m; 
			m = Message.obtain(mHandler, NO_UPDATE);
			mHandler.dispatchMessage(m);
		}
		
		
	}

	
	private Dialog createDialogDownload(final String filename,String version ) 
	{
		AlertDialog.Builder  builder;
		AlertDialog alert;
		
		String msg = String.format(mContext.getString(R.string.CheckAppVersion_start_dl_1) , version);
		Spanned _msg= Html.fromHtml("<big>"+msg+"</big>");
		
		builder = new AlertDialog.Builder(mContext);
		builder.setMessage(_msg)
		.setCancelable(false)
		.setPositiveButton(mContext.getResources().getString(R.string.Yes), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				/* Method 1: Start download Task
				 *  This will download to an known location 
				 *  and ask user to install, if the app is running at main screen (EntryActivity), 
				 *  there will be a popup.  
				 */
				//Updating.. 
				if (mHandler != null)
				{
					Message m; 
					m = Message.obtain(mHandler, NEW_APP_AVAILABLE);
					mHandler.dispatchMessage(m);
				}
				
				
				URL app_loc;
				try {
					app_loc = new URL(APP_RELEASE_URL+ "/" +filename);
					AppDownloader app_dl = new AppDownloader(mContext);
					app_dl.execute(app_loc);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				
				
				
				

				/* Another method: Download will be done automatically, and a notification 
				 * will be put on the status bar after download is done. 
				 * User has to click to start installing, which may not be practical because
				 * if the App is run, user will not see the status bar. 
				 */
				/*Intent intent = new Intent(Intent.ACTION_VIEW ,
											Uri.parse("http://" + APP_SERVER_IP_ADDR + ":" + 
									                   APP_SERVER_PORT +  "/" + filename));
				mContext.startActivity(intent);*/
			}
		})
		.setNegativeButton(mContext.getResources().getString(R.string.No), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				//NO UPdate --> Signal The Handler to continue .. 
				if (mHandler != null)
				{
					Message m; 
					m = Message.obtain(mHandler, NO_UPDATE);
					mHandler.dispatchMessage(m);
				}
				
				
				dialog.dismiss();
			}
		});



		alert = builder.create();

		return alert;
		
		
	}
	
	private Dialog createDialogNoUpdate() 
	{
		AlertDialog.Builder  builder;
		AlertDialog alert;
		
		Spanned _msg= Html.fromHtml("<big>"+mContext.getString(R.string.no_update_is_available)+"</big>");
		
		builder = new AlertDialog.Builder(mContext);
		builder.setMessage(_msg)
		.setCancelable(false)
		.setPositiveButton(mContext.getResources().getString(R.string.OK), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});



		alert = builder.create();

		return alert;
		
		
	}

	/** Called when the activity is first created. */
	protected ParsedDataSet doInBackground(String... params) {
		
		String usr_pass =  String.format("%s:%s", user, pass);
		
		ParsedDataSet parsedExampleDataSet = null;
		/* Create a new TextView to display the parsingresult later. */
		try {
			/* Create a URL we want to load some xml-data from. */
			String phoneModel = android.os.Build.MODEL;
			URL url = null;
			if (phoneModel.equals(PublicDefine.PHONE_MBP2k) || phoneModel.equals(PublicDefine.PHONE_MBP1k))
			{
				//MBP phone
				url = new URL(MBP_APP_VERSION_URL);
				Log.d("mbp", "check app version >>> " + MBP_APP_VERSION_URL);
			}
			else
			{
				//Google phone
				url = new URL(APP_VERSION_URL);
				Log.d("mbp", "check app version >>> " + APP_VERSION_URL);
			}
			
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(5000);
			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			DataInputStream inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream(),4*1024));

			
			/* Get a SAXParser from the SAXPArserFactory. */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();

			/* Get the XMLReader of the SAXParser we created. */
			XMLReader xr = sp.getXMLReader();
			/* Create a new ContentHandler and apply it to the XML-Reader*/
			XmlVersionContentHandler myExampleHandler = new XmlVersionContentHandler();
			xr.setContentHandler(myExampleHandler);

			/* Parse the xml-data from our URL. */
			xr.parse(new InputSource(inputStream));
			
			
			/* Parsing has finished. */

			/* Our ExampleHandler now provides the parsed data to us. */
			parsedExampleDataSet = myExampleHandler.getParsedData();
		
		} catch (Exception e) {
			/* Display any Error to the GUI. */
			Log.e(MY_DEBUG_TAG,  e.toString());
			return null;
		}
		return parsedExampleDataSet;
	}



	

	
	
	
}