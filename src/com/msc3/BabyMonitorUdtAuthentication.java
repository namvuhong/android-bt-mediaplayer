package com.msc3;

import java.io.UnsupportedEncodingException;

import android.util.Base64;
import android.util.Log;

import com.monitoreverywhere.utils.SymmetricCipher;

/**
 * @author phung
 */
public class BabyMonitorUdtAuthentication extends BabyMonitorAuthentication {

	private String macAddress; 
	private String channelID; 
	private String encKey;
	private int  udtLocalPort;
	
	//20120621: seriously i don't know what kind of mess is this...
	private String portalUser; 
	private String portalPass; 
	private String relayToken; 
	
	
	public BabyMonitorUdtAuthentication(String ip, String port, String ss_key)
	{
		device_ip = ip;
		device_port = Integer.parseInt(port);
		session_key = ss_key;
		channelID = null;
		macAddress = null;
		stream_url = null;
	}
	
	public BabyMonitorUdtAuthentication(String ip, String port, String ss_key, String chann, 
			int udtPort , String mac)
	{
		device_ip = ip;
		device_port = Integer.parseInt(port);
		session_key = ss_key;
		channelID = chann; 
		udtLocalPort = udtPort;
		macAddress = mac;
		stream_url = null;
	}

	
	public String toString()
	{
		String ret = "http_cam: "+ device_ip + ":" + device_port + "\nLocalPort: " + udtLocalPort; 
		
		return ret;
	}
	public void setEncKey(String sk)
	{
		encKey = sk; 
	}
	public String getDeviceMac()
	{
		return macAddress; 
	}
	public String getChannelID() {
		return channelID;
	}

	public String getUser()
	{
		return this.portalUser;
	}
	
	public String getPass()
	{
		return this.portalPass;
	}
	
	public void setUserPass(String user, String pass)
	{
		this.portalPass = pass; 
		this.portalUser = user; 
	}
	
	public String getUserPass() 
	{
		if (portalPass == null || portalUser == null)
		{
			return null; 
		}
		String usr_pass =  String.format("%s:%s", portalUser, portalPass);
		
		return usr_pass;
		  
	}
	
	public String getBase64EncodeUserPass() throws UnsupportedEncodingException
	{
		String usr_pass =  getUserPass() ;
		if ( usr_pass == null)
			return null; 
		
		return Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP);
		  
	}
	
	
	public byte [] getEncChannelID() throws Exception
	{
		
		return SymmetricCipher.encryptAES(channelID.getBytes(), encKey);
	}
	
	
	public byte [] getEncMac() throws Exception
	{
		return SymmetricCipher.encryptAES(macAddress.getBytes(), encKey);
	}
	
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}

	public String getIP()
	{
		return device_ip;
	}
	public int getPort()
	{
		return device_port;
	}
	
	public String getSSKey()
	{
		return session_key;
	}

	public int getUdtLocalPort() {
		return udtLocalPort;
	}

	public void setUdtLocalPort(int port) 
	{
		udtLocalPort = port; 
	}
	
	public void setRelayToken(String s)
	{
		this.relayToken = s; 
	}

	public String getRelayToken()
	{
		return this.relayToken;
	}
	
	
	@Override
	public int  getLocalPort() {
		return  getUdtLocalPort();
	}

	public byte[] decodeServerMessage(byte[] data, int offset, int len) throws Exception {
		byte [] inDat = new byte[len]; 
		System.arraycopy(data, offset, inDat, 0, len);
		return SymmetricCipher.decryptAES(inDat, encKey);
	}
	
	
	public static String calculateRelayToken(String relaySk, String macAddress, 
			BabyMonitorUdtAuthentication bm_auth)
	{
		///Encrypt - macaddress:enc(email:pass)
		String msg = macAddress+ ":";

		String rawUserPass = bm_auth.getUserPass();
		byte [] encryptedBytes = null; 
		try {
			encryptedBytes =SymmetricCipher.encryptAES(rawUserPass.getBytes("UTF8"), relaySk);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if ( encryptedBytes == null)
		{
			return null; 
		}

		String encodeEncrypted = Base64.encodeToString(encryptedBytes,Base64.NO_WRAP);
		msg = msg.concat(encodeEncrypted);
		
		
		

		return msg; 
	}


	@Override
	public String getStreamUrl() {
		return stream_url;
	}

}
