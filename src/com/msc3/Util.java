package com.msc3;

import java.io.File;
import java.math.BigInteger;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

public class Util {
	//private static final boolean use_tflash = true;

	public static byte[] intToByteArray_MSB(int value) {
		byte[] b = new byte[4];
		for (int i = 0; i < 4; i++) {
			int offset = (b.length - 1 - i) * 8;
			b[i] = (byte) ((value >>> offset) & 0xFF);
		}
		return b;
	}
	public static int byteArrayToInt_MSB(byte[] b, int offset) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i + offset] & 0x000000FF) << shift;
		}
		return value;
	}


	/**
	 * Attemp to convert 8bytes (starting from offset) to a long value 
	 * [MSB] First bytes is becomes the most significant bytes in the final long value 
	 *  
	 * @param b
	 * @param offset
	 * @return
	 */
	public static long byteArrayToLong_MSB(byte[] b, int offset)
	{
		long value = 0;
		for (int i = 0; i < 8; i++) {
			int shift = (8 - 1 - i) * 8;
			value += (b[i + offset] & 0x00000000000000FFL) << shift;
		}
		return value;
	}

	public static long byteArrayToLong_LSB(byte[] b, int offset)
	{
		long value = 0;
		for (int i = 0; i < 8; i++) {
			int shift = i * 8;
			value += (b[i + offset] & 0x00000000000000FFL) << shift;
		}
		return value;
	}

	public static void intToByteArray_LSB(int value,byte[] abyte0)
	{
		abyte0[3] = (byte)(value / 0x1000000);
		value %= 0x1000000;
		abyte0[2] = (byte)(value / 0x10000);
		value %= 0x10000;
		abyte0[1] = (byte)(value / 256);
		value %= 256;
		abyte0[0] = (byte)value;
	}
	public static void shortToByteArray_LSB(short word0, byte abyte0[])
	{
		abyte0[1] = (byte)(word0 >>> 8);
		abyte0[0] = (byte)(word0 & 0xff);
	}
	public static void memSet(byte[] arr, int len,byte value)
	{
		for(int i = 0 ; i < len;i++)
		{
			arr[i] = value;
		}
	}

	public static String getRecordFileName() {
		String directory = getRecordDirectory();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return directory + File.separator + formatter.format(date) + ".flv";
	}

	public static String getSnapshotFileName() {
		String directory = getRecordDirectory();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return directory + File.separator + formatter.format(date) + ".jpg";
	}
	public static String getShortFileName(String filename) {
		String short_name = filename;
		int start = filename.indexOf("/mbp_");
		if (start != -1 )
		{
			short_name = filename.substring(start);
		}
		return short_name;
	}

	public static String getRecordDirectory() {
		String path = Environment.getExternalStorageDirectory().getPath() + File.separator +
				"hubble" + File.separator + "saved";  //mbp_tflash
		if (Environment.getExternalStorageDirectory().getPath() != null && 
				Environment.getExternalStorageDirectory().getPath().contains("external_sd"))
		{
			path = Environment.getExternalStorageDirectory().getPath() + File.separator +
					"hubble_sdcard" + File.separator + "saved"; //mbp_sdcard
		}

		File f = new File(path);
		if(!f.exists()) {
			boolean res = f.mkdirs();
			Log.w("hubble-dir", "create new dir: "+ path + " ->" + res);

		}
		return path;
	}
	
	public static String getRootRecordingDirectory() {
		String path = Environment.getExternalStorageDirectory().getPath() + File.separator +
				"hubble";  //mbp_tflash
		if (Environment.getExternalStorageDirectory().getPath() != null && 
				Environment.getExternalStorageDirectory().getPath().contains("external_sd"))
		{
			path = Environment.getExternalStorageDirectory().getPath() + File.separator +
					"hubble_sdcard"; //mbp_sdcard
		}

		File f = new File(path);
		if(!f.exists()) {
			boolean res = f.mkdirs();
			Log.w("hubble-dir", "create new dir: "+ path + " ->" + res);

		}
		return path;
	}

	private static Character [] hex_chars = {'0', '1','2','3','4','5','6','7','8',
		'9','0','a','b','c','d','e','f','A','B','C','D','E','F'};

	public static boolean isThisAHexString(String str)
	{
		char ch ;
		for (int i =0; i<str.length(); i++)
		{
			ch = str.charAt(i);
			if (Arrays.asList(hex_chars).indexOf(Character.valueOf(ch)) == -1)
			{
				return false;
			}
		}
		return true;
	}

	public static File[] getRecordFiles() {
		File recordDir = new File(getRecordDirectory());
		return recordDir.listFiles();
	}

	/********** THUMBNAIL APIs **********/
	public static String getVideoThumbnailFileName(String from_recordedFilenName)
	{
		int i = 0, j = 0;
		String thumbnail = null;
		i = from_recordedFilenName.lastIndexOf(File.separator);

		j = from_recordedFilenName.lastIndexOf(".avi");
		thumbnail = from_recordedFilenName.substring(i+1, j);
		return getVideoThumbnailDirectory() + File.separator + thumbnail;
	}


	public static String getPhotoThumbnailFileName(String from_recordedFilenName)
	{
		int i = 0;
		String thumbnail = null;
		i = from_recordedFilenName.lastIndexOf(File.separator);
		thumbnail = from_recordedFilenName.substring(i+1);
		return getPhotoThumbnailDirectory() + File.separator + thumbnail;
	}

	public static String getPhotoThumbnailDirectory() {
		String path = Environment.getExternalStorageDirectory().getPath() + File.separator +
				"mbp" + File.separator + ".thumbnails";


		File f = new File(path);
		if(!f.exists()) {
			f.mkdir();
		}
		return path;
	}

	public static String getVideoThumbnailDirectory() {
		String path = Environment.getExternalStorageDirectory().getPath() + File.separator +
				"mbp" + File.separator + ".video_thumbnails";



		File f = new File(path);
		if(!f.exists()) {
			f.mkdirs();
		}
		return path;
	}

	public static String getDayOfMonthSuffix(final int n) 
	{
	    if (n < 1 || n > 31)
	    {
	    	return null;
	    }
	    
	    if (n >= 11 && n <= 13) {
	        return "th";
	    }
	    
	    switch (n % 10) {
	        case 1:  return "st";
	        case 2:  return "nd";
	        case 3:  return "rd";
	        default: return "th";
	    }
	}

	public static String getLogFile()
	{
		String path = getRootRecordingDirectory() + File.separator + "logs" ;

		File f = new File(path);
		if(!f.exists()) {
			f.mkdirs();
		}
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyddMM_HHmmss");
		return path + File.separator + formatter.format(date) + ".txt";
	}

	public static String getPCMFile()
	{
		String path = getRecordDirectory() + File.separator + ".tmp" ;

		File f = new File(path);
		if(!f.exists()) {
			f.mkdirs();
		}
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyddMM_HHmmss");
		return path + File.separator + formatter.format(date) + ".pcm";
	}


	public static void deleteThumbnail(String filename) {
		String path = null;
		if(filename.endsWith(".jpg")) {
			path = getPhotoThumbnailFileName(filename);

		}
		if(filename.endsWith(".avi")) {
			path = getVideoThumbnailFileName(filename);
		}
		if (path != null)
		{
			File f = new File(path);
			if (f.exists())
			{
				Log.d("mbp", " delete: " + path);
				f.delete();
			}
		}
	}
	public static void deleteVideoOrPic(String filename) {
		File f = new File(getRecordDirectory() + File.separator + filename);
		f.delete();
	} 

	/***  For Application data Read and Write ****/
	public static String getApplicationData()
	{
		return null;
	}

	public static long spaceAvailableOnDisk()
	{
		long availableSpace_in_bytes = -1L;
		try {
			StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
					.getPath());
			stat.restat(Environment.getExternalStorageDirectory().getPath());
			availableSpace_in_bytes = (long) stat.getAvailableBlocks() * (long)stat.getBlockSize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return availableSpace_in_bytes; 
	}

	public static String convertHexToIP(String hexIp) throws UnknownHostException {

		int Ip = new BigInteger(hexIp, 16).intValue();
		byte[] result = new byte[4];

		result[0] = (byte) (Ip >> 0);
		result[1] = (byte) (Ip >> 8);
		result[2] = (byte) (Ip >> 16);
		result[3] = (byte) (Ip >> 24);

		int[] arr = new int[4];

		for (int i = 0; i < 4; i++) {

			if (result[i] < 0) {
				arr[i] = result[i] + 256;

			} else {
				arr[i] = result[i];
			}

		}
		System.out.print("\n" + arr[0] + "." + arr[1] + "." + arr[2] + "." + arr[3]);
		String output = arr[0] + "." + arr[1] + "." + arr[2] + "." + arr[3];
		return output;
	}

	static public String formatMillis(long millis) {
		StringBuilder    buf=new StringBuilder(8);
		String           tmp;

		if(millis<0) { buf.append('-'); millis=Math.abs(millis); }
		tmp=("0" + (millis/3600000));          buf.append((tmp.length()>2) ? tmp.substring(1) : tmp);
		buf.append(":");
		tmp=("0" + ((millis%3600000)/60000));  buf.append(tmp.substring(tmp.length()-2));
		buf.append(":");
		tmp=("0" + ((millis%60000)/1000));     buf.append(tmp.substring(tmp.length()-2));
		
//		buf.append(".");
//		tmp=("00" + (millis%1000));            buf.append(tmp.substring(tmp.length()-3));
		
		return buf.toString();
	}
}
