package com.msc3;

import java.io.*;
import java.util.concurrent.Semaphore;

import android.os.Environment;
import android.util.Log;

public class SetupData {

	public static final String SETUP_FILE = "mbp_setup.dat";
	
	public static final int  ACCESS_VIA_INTERNET = 0x02;
	public static final int  ACCESS_VIA_LAN = 0x01;
	
	private int access_mode;
	private String current_ssid_;
	private CamChannel [] channels;
	private CamProfile [] configured_cams;
	
	private static  Semaphore readWriteLock ;
	
	/* Used when LOADing data */
	public SetupData()
	{
		access_mode = -1;
		current_ssid_ = null;
		channels = null;
		configured_cams = null;
		
		//Only one person can read/write at one time
		
		readWriteLock = new Semaphore(1);
	}
	
	/* Used when SAVEing data */
	public SetupData(int a_mode, String home_ssid, CamChannel[] channs, CamProfile[] cps)
	{
		access_mode = a_mode;
		current_ssid_ = home_ssid;
		channels = channs;
		configured_cams = cps;
		
		//Only one person can read/write at one time
		readWriteLock = new Semaphore(1);
	}
	
	
	public void set_AccessMode (int amode)
	{
		this.access_mode = amode;
	}
	
	public int get_AccessMode()
	{
		return this.access_mode;
	}

	public void set_SSID (String ssid)
	{
		this.current_ssid_ = ssid;
	}
	public String get_SSID()
	{
		return this.current_ssid_;
	}
	
	public void set_Channels(CamChannel [] chann_array)
	{
		this.channels = chann_array;
	}
	public CamChannel [] get_Channels()
	{
		return this.channels;
	}
	
	
	public void set_CamProfiles(CamProfile [] cam_profile)
	{
		this.configured_cams = cam_profile;
	}
	public CamProfile [] get_CamProfiles()
	{
		return this.configured_cams;
	}
	
	
	
	private boolean check_data_before_saving()
	{
		if (  (access_mode == -1) ||
		      (current_ssid_ == null) /*||
		      (channels == null) ||
		      (configured_cams == null) */)
			return false;
		else
			return true;
	}
	
	
	
	public void clear_session_data(File externalFileDir)
	{
		
		try {
			readWriteLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.e("mbp", "External Storage is mounted as READONLY!");
			return;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			return;
		}

		/* create a data file in the external dir */
		File file = new File(externalFileDir,SETUP_FILE );
		if (file.exists())
		{
			Log.d("mbp", "Remove offline data");
			/* remove the old file */
			file.delete();
		}
		
		readWriteLock.release();
		
		
		
	}
	/**** Save And Restore data function */
	public boolean save_session_data(File externalFileDir)
	{
		
		
		/* Check if the External Storage is available and writeable */
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.e("mbp", "External Storage is mounted as READONLY!");
			return false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			return false;
		}

		
		try {
			readWriteLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		if (check_data_before_saving() == false)
		{
			Log.e("mbp", "Not enough data to save");
			readWriteLock.release();
			return false;
		}
		
		/* create a data file in the external dir */
		File file = new File(externalFileDir,SETUP_FILE );
		if (file.exists())
		{
			//Log.e("mbp", "File exist, remove it");
			/* remove the old file */
			file.delete();
		}

		
		
		try {
			OutputStream os = new FileOutputStream(file);
			ObjectOutputStream obj_out = new ObjectOutputStream(os);

			/* Write Access mode */
			obj_out.writeInt(access_mode);

			/* Write the Homenetwork SSID */
			obj_out.writeByte(current_ssid_.getBytes().length);
			obj_out.write(current_ssid_.getBytes());

			/* Write out the Channel list */
			obj_out.writeByte(channels.length);
			for (int i = 0; i< channels.length; i++)
			{
				obj_out.writeObject(channels[i]);
			}

			/* Write out the configured_cams */
			obj_out.writeByte(configured_cams.length);
			for (int i = 0 ; i < configured_cams.length; i++)
			{
				obj_out.writeObject(configured_cams[i]);
			}



			obj_out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			if (file.exists())
			{
				/* remove the incomplete file */
				file.delete();
			}
			e.printStackTrace();
		}
		
		readWriteLock.release();
		
		
		return true;
	}


	 
	
	public  boolean restore_session_data(File externalFileDir) throws StorageException
	{

		
		/* Check if the External Storage is available and Readable */
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			Log.w("mbp", "External Storage is mounted as READONLY!");
			throw new StorageException("External Storage is mounted as READONLY!");
			/*since we just want to read , it's ok to proceed*/
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			Log.e("mbp", "External Storage is not ready! (mount/unmount)");
			throw new StorageException("External Storage is not ready! (mount/unmount)");
			//return false;
		}


		try {
			readWriteLock.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		/* Open a data file in the external dir */
		File file = new File(externalFileDir,SETUP_FILE );
		if (!file.exists())
		{
			Log.d("mbp", "Setup file not exist");
			readWriteLock.release();
			
			return false; //* file does not exists 
		}

		try {
			InputStream is = new FileInputStream(file);
			ObjectInputStream obj_in = new ObjectInputStream(is);

			/* Read Access mode - not used */
			this.access_mode = obj_in.readInt();


			int len = obj_in.readByte();
			byte[] home_ssid = new byte[len];
			obj_in.readFully(home_ssid);
			//reload current_ssid - not used 
			current_ssid_ = new String(home_ssid);

			len = obj_in.readByte();
			
			channels = new CamChannel[len];
			for (int i = 0; i< len; i++)
			{
				channels[i] = (CamChannel)obj_in.readObject();
			}

			len = obj_in.readByte();
			configured_cams = new CamProfile[len];
			for (int i = 0; i < len ; i++)
			{
				configured_cams[i]= (CamProfile)obj_in.readObject();
				
			}

			obj_in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			if (file.exists())
				file.delete();
			e.printStackTrace();
		} catch (IOException e) {
			if (file.exists())
				file.delete();
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			if (file.exists())
				file.delete();
			e.printStackTrace();
		}
		
		
		readWriteLock.release();
		
		
		return true;
	}
	
	public boolean hasUpdate(File externalFileDir, long lastTimeRead)
	{
		File file = new File(externalFileDir, SETUP_FILE);
		if (file.exists())
		{
			if (file.lastModified() > lastTimeRead)
			{
				return true;
			}
		}
		
		return false;
	}

}
