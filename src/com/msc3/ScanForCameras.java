package com.msc3;


import com.blinkhd.R;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.InvalidObjectException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.msc3.comm.HTTPRequestSendRecv;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.SlidingDrawer;

public class ScanForCameras extends AsyncTask<CamProfile [], CamProfile, CamProfile[]> {

	private ProgressDialog dialog;
	private Context mContext;
	private Handler mHandler;
	
	public static final int SUCCESS = 1; 
	public static final int REASON_AUTHORIZED_PWD_NEEDED = 2;
	public static final int REASON_NULL_CAM_PROFILE = 3;
	public static final int REASON_NULL_CONTENT_TYPE = 4;
	
	private static final int REQUEST_PORT = 10000;
	private static final int REPLY_PORT = 10001;
	private static final int TCP_REPLY_PORT = 10002;
	
	public static final int SCAN_CAMERA_PROGRESS = 10003;

	private static final String SCAN_REQUEST = "Mot-Cam QUERY   *               ";
	private static final int MAX_CHANNEL_NUM = 4;

	private InetAddress broadcast,own_ip;
	private CamProfile [] reduced_scan_results;

	private ICameraScanner scanner;
	private int status, index ; 

	private boolean silence;
	private boolean shouldGetSnapshot;
	
	public ScanForCameras(Context mContext,ICameraScanner sc )
	{
		this.mContext = mContext;
		this.mHandler = null;

		reduced_scan_results= null;
		scanner = sc;
		silence = false; 
		shouldGetSnapshot =true; 
	}
	
	public ScanForCameras(Context mContext, Handler mHandler, ICameraScanner sc )
	{
		this.mContext = mContext;
		this.mHandler = mHandler;

		reduced_scan_results= null;
		scanner = sc;
		silence = false; 
		shouldGetSnapshot =true; 
	}

	public void setShouldGetSnapshot(boolean b)
	{
		shouldGetSnapshot = b; 
	
	}
	public void setSilence(boolean b)
	{
		silence = b; 
	}

	/* 
	 * (non-Javadoc)
	 *  Description:
	 *  Scan n/wfor "IRabit *"
	 *                Set some timeout
	 *                Listen on a port to get a response
	 *                Once a response is receive -> parse it
	 *                Save it into channel_x (x =1,2,3,4)
	 *                repeat until we have configured Max_Channel (4)
	 *                or when timeout expired.
	 *                - Enable (highlight) those configure channel in UI
	 * return : Array of cameras found on the network
	 *          Null otherwise
	 */

	@Override
	protected CamProfile[] doInBackground(CamProfile []... arg0) {
		try {
			getAddresses();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//new way: broadcast request but expect only one camera reply 
		send_page_request(arg0[0]);
		//send_page_request_new(arg0[0]);

		if (isCancelled())
		{
			//simple return NUll; onPostExecute() will not be called anyway 
			Log.d("mbp", "Cancelled after send_scan_request");
			return null;
		}


		/* reduced_scan_results != null if we found some cams on the n/w */
		if( (reduced_scan_results != null) &&
			(shouldGetSnapshot == true))
		{

			SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);

			/* now we query each cameras for a few frame, use for location
			 * indication 
			 */

			int return_val = 0;

			for (int i = 0; i< reduced_scan_results.length; i++)
			{
				/*20120223: query the camera for the melody status as well 
				 * Future Work: query the whole set of status here and store in camera profile 
				 * ---- 
				updateMelodyIndex(reduced_scan_results[i]);
				 

				if (isCancelled())
				{
					//simple return NUll; onPostExecute() will not be called anyway
					Log.d("mbp", "Cancelled after updateMelodyIndex");
					return null;
				}
				 */
				
				/*20120606: query vox status - if one cam enable vox -> set wakelock flag to true
				 */
				checkVoxStatus(reduced_scan_results[i]);

				if (isCancelled())
				{
					//simple return NUll; onPostExecute() will not be called anyway
					Log.d("mbp", "Cancelled after updateVox");
					return null;
				}
				
			} //end for (int i = 0; i< reduced_scan_results.length; i++)



		}


		return reduced_scan_results;
	}



	private void checkVoxStatus(CamProfile camProfile) {
		if ( camProfile == null)
			return;
		String device_address_port = camProfile.get_inetAddress().getHostAddress() + ":" + camProfile.get_port() ;
		String res = null; 
		String http_addr =String.format("%1$s%2$s%3$s","http://",
				device_address_port,
				PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_STATUS) ;
		res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,  camProfile.getBasicAuth_usr(), camProfile.getBasicAuth_pass());

		if ( (res!= null) && res.startsWith(PublicDefine.VOX_STATUS))
		{
			String str_value = res.substring(PublicDefine.VOX_STATUS.length()
					+ 2 ); 

			int vox_status = Integer.parseInt(str_value);


			if (vox_status == 0)//Disabled
			{
				camProfile.setVoxEnabled(false); 
			}
			else
			{
				//Enabled
				camProfile.setVoxEnabled(true); 
			}

		}
		
	}

	
	/* on UI Thread */
	protected void onPreExecute()
	{
		if (silence == false)
		{
			dialog = new ProgressDialog(mContext);
			Spanned msg = Html.fromHtml("<big>"+mContext.getString(R.string.ScanCam_3)+"</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
	
			dialog.show();
		}
	}

	/* on UI Thread */
	protected void onProgressUpdate(CamProfile... progress) {

//		if (dialog != null)
//		{
//			dialog.setMessage(progress[0]);
//		}
		if (mHandler != null)
		{
			mHandler.dispatchMessage(Message.obtain(
					mHandler, SCAN_CAMERA_PROGRESS, progress[0]));
		}

	}


	/* on UI Thread */
	protected void onPostExecute(CamProfile[] result) {
		scanner.updateScanResult(result, status,index);
		if (dialog != null && dialog.isShowing())
		{
			try 
			{
				dialog.dismiss();
			}
			catch (IllegalArgumentException ie)
			{

			}
		}
	}
	/* on UI Thread - when cancelled don't invoke callback. */
	protected void onCancelled ()
	{
		if (dialog != null && dialog.isShowing())
		{
			try 
			{
				dialog.dismiss();
			}
			catch (Exception ie)
			{

			}
		}
	}


	/**
	 * Send one paging message at a time. 
	 * May take longer.. 
	 * @param data
	 * @param cameras [] array of cameras that need to be paged, other camera should be ignored
	 */
	private  void send_page_request(CamProfile [] cameras)
	{
		DatagramSocket socket = null;
		DatagramPacket req_packet = null, recv_packet = null;
		InetAddress []clientAddr = new InetAddress[1];
		byte [] page_req = null; 
		InetAddress scan_results[] = new InetAddress [MAX_CHANNEL_NUM];
		String scan_results_mac [] = new String[MAX_CHANNEL_NUM];
		int scan_results_port[] = new int[MAX_CHANNEL_NUM];
		//String scan_results_version[]  = new String[MAX_CHANNEL_NUM];
		int next_channel  = 0;
		reduced_scan_results = null;
		byte[] buf ;
		
		int retry_create_sock = 10;
		do 
		{
			try {
				socket = new DatagramSocket(REPLY_PORT);
				socket.setBroadcast(true);
				socket.setSoTimeout(1000); //Timeout in 1 sec
			} catch (SocketException e1) {
				Log.e("mbp","Failed to create socket for Scan request exception: " +
			e1.getLocalizedMessage());
				socket = null;
			}
			finally
			{
				if (socket !=null)
				{
					retry_create_sock = 0; 
				}
			}
			
			if (isCancelled() )
			{
				Log.d("mbp", "Cancelled during create sock");
				if (socket!= null)
					socket.close(); 
				return; 
			}
			
			
		}
		while (--retry_create_sock > 0);

		
		if (socket != null )
		{
			for (int i = 0; i< cameras.length; i++)
			{
				
				//First Optimization ----
				if ( shouldSkipScannning(cameras[i]) == true )
				{
					/* 20130131: hoang: update scan progress to UI
					 */
					CamProfile scanned_camera = new CamProfile(cameras[i].get_inetAddress(),
							cameras[i].get_port(), cameras[i].get_MAC());
					scanned_camera.setInLocal(false);
					publishProgress(scanned_camera);
					
					continue;
				}
				else //shouldSkipScannning(cameras[i]) == false 
				{
					try {
						page_req =  build_page_request(cameras[i].get_MAC());
					} catch (InvalidObjectException e) {
						Log.d("mbp","Invalid mac .. skip ");
						continue;//skip this entry
					}

					//Second Optimization ---- 
					if ( cameras[i].get_inetAddress() != null)
					{
						String http_get_mac_address = 
								"http://"+ cameras[i].get_inetAddress().getHostName() + ":" +
								cameras[i].get_port() +
								PublicDefine.HTTP_CMD_PART +PublicDefine.GET_MAC_ADDRESS;
						String response = 
								HTTPRequestSendRecv.sendRequest_block_for_response(http_get_mac_address);
						String cam_mac_address =  cameras[i].get_MAC().replaceAll(":", "");

						if ( response != null  &&  response.equals(cam_mac_address) )
						{

							scan_results[next_channel] = cameras[i].get_inetAddress();
							// Check UDP Protocol 20110915 doc for the size*
							scan_results_mac[next_channel] = cameras[i].get_MAC();
							
							scan_results_port[next_channel] = cameras[i].get_port();

							next_channel ++;

							/* 20130131: hoang: update scan progress to UI
							 */
							CamProfile scanned_camera = new CamProfile(cameras[i].get_inetAddress(),
									cameras[i].get_port(), cameras[i].get_MAC());
							scanned_camera.setInLocal(true);
							publishProgress(scanned_camera);

							//Log.d("mbp","FOUND cam"+ cameras[i].get_MAC() + " via get_mac_address");


							continue;
						}
						else 
						{
							// go to send udp broadcast request
						}

					}

					
					//Finally... old way -- broadcast --- 
					/** UDP way ------ UDP way - 2 times */
					boolean hasFoundDeviceOnUDP = false; 
					int retry = 1;
					do
					{
						//Log.d("mbp", "Scan:"+ cameras[i].get_MAC() +  " using UDP ");

						try {
							buf = new byte[41];

							req_packet = new DatagramPacket(page_req, page_req.length,
									broadcast, REQUEST_PORT);
							
							recv_packet = new DatagramPacket(buf, buf.length);

							socket.send(req_packet);
							//Receive reply
							socket.receive(recv_packet);
							buf = recv_packet.getData();

							String signature = new String (buf,0,7, "UTF-8");
							if ( signature.equalsIgnoreCase("Mot-Cam"))
							{

								String mac_ =  new String(buf,24,17,"UTF-8");
								mac_ = mac_.toUpperCase();
								if ( mac_.equals(cameras[i].get_MAC()))
								{

									String str_port = new String(buf, 16, 8, "UTF-8");
									str_port = str_port.trim();
									int port_ = Integer.parseInt(str_port);
									InetAddress iAddress = recv_packet.getAddress();

									String version = new String (recv_packet.getData(),9,6, "UTF-8");
									Log.d("mbp", "recv scan-res from: " + iAddress.toString() + " FW: " + version);
									scan_results[next_channel] = iAddress;
									// Check UDP Protocol 20110915 doc for the size*

									scan_results_mac[next_channel] = mac_;
									
									scan_results_port[next_channel] = port_;


									next_channel ++;

									/* 20130131: hoang: update scan progress to UI
									 */

									CamProfile scanned_camera = new CamProfile(iAddress, port_, mac_);

									scanned_camera.setInLocal(true);
									publishProgress(scanned_camera);

									hasFoundDeviceOnUDP = true;
									break; // Stop retrying .. 
								}
							}

						} catch (UnsupportedEncodingException e) 
						{
							Log.d("mbp", " Unsupport encoding?? " );
							e.printStackTrace();
						}
						catch (SocketException e) {
							Log.d("mbp", "Scan "+ cameras[i].get_MAC() +  " using UDP failed w Sockexception: " + e.getLocalizedMessage());

						} 
						catch (IOException e) {
							Log.d("mbp", "Scan "+ cameras[i].get_MAC() +  " using UDP failed w  IOexception: " + e.getLocalizedMessage());
						}

						if (isCancelled())
						{
							//simple return ; onPostExecute() will not be called anyway 
							Log.d("mbp", "Cancelled during send_scan_request 1");
							socket.close(); 

							return; 
						}


					}
					while (retry -- > 0);

					boolean hasFoundDeviceOnTCP = false;
					if (hasFoundDeviceOnUDP == false)
					{
						/*20120511: if UDP failed. use TCP to receive  1 times*/
						retry = 0; 
						do
						{
							buf = new byte[41];
							req_packet = new DatagramPacket(page_req, page_req.length,
									broadcast, REQUEST_PORT);
							try {
								socket.send(req_packet);
							} catch (IOException e1) {
							}
							int res = receiveResponseViaTCP(buf, clientAddr); 

							if (res == -1 )
							{
								Log.d("mbp","scan "+ cameras[i].get_MAC() +  " using TCP failed"); 

							}
							else if (res != buf.length)
							{
								try {
									Log.d("mbp","Receive less than 41 bytes:" + new String (buf,0,res, "UTF-8") );
								} catch (UnsupportedEncodingException e) {
								}
							}

							String signature;
							try {
								signature = new String (buf,0,7, "UTF-8");

								if ( signature.equalsIgnoreCase("Mot-Cam"))
								{

									String mac_ =  new String(buf,24,17,"UTF-8");
									mac_ = mac_.toUpperCase();
									if ( mac_.equals(cameras[i].get_MAC()))
									{

										String str_port = new String(buf, 16, 8, "UTF-8");
										str_port = str_port.trim();
										int port_ = Integer.parseInt(str_port);
										scan_results[next_channel] = clientAddr[0];
										// Check UDP Protocol 20110915 doc for the size/
										scan_results_mac[next_channel] = mac_;
										scan_results_port[next_channel] = port_;
										Log.d("mbp", "recv TCP scan-res from : " + clientAddr[0]  + " mac: " + mac_);	

										next_channel ++;

										/* 20130131: hoang: update scan progress to UI
										 */

										CamProfile scanned_camera = new CamProfile(clientAddr[0], port_, mac_);
										scanned_camera.setInLocal(true);
										publishProgress(scanned_camera);
										
										hasFoundDeviceOnTCP = true;

										break; // Stop retrying .. 
									}
								}
							} catch (UnsupportedEncodingException e) {
							}
						}
						while (retry -- > 0);
					} //if hasFoundDeviceOnUDP == false
					

					/* 20130131: hoang: update scan progress to UI
					 */
					if ( (hasFoundDeviceOnUDP == false) && (hasFoundDeviceOnTCP == false) )
					{
						CamProfile scanned_camera = new CamProfile(cameras[i].get_inetAddress(),
								cameras[i].get_port(), cameras[i].get_MAC());
						scanned_camera.setInLocal(false);
						publishProgress(scanned_camera);
					}

				} // else (shouldSkipScannning(cameras[i]) == false) 
			} // For loop 

			socket.close();
		} // If socket != null

		
		if (next_channel> 0)
		{
			reduced_scan_results = new CamProfile[next_channel];

			for (int i = 0; i<next_channel; i++)
			{
				reduced_scan_results[i] = new CamProfile(scan_results[i],
						scan_results_port[i], scan_results_mac[i]);
			}

		}
	}

	private boolean shouldSkipScannning(CamProfile cp)
	{
		//20120829:phung: remove this condition 
//		if (cp.isInLocal() == false 
//				&& (cp.get_inetAddress() != null) 
//				&& (cp.isReachableInRemote() == false) )
//		{
//			// camera keep alive time from server > 10 min, don't do the local scan
//			Log.d("mbp", cp.getName() + " is unreachable, should skip scannning");
//			return true;
//		}
		
		
		if (cp.get_inetAddress() != null)
		{
			WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
			DhcpInfo dhcp = wifi.getDhcpInfo();
			int int_dhcp_network_addr = (dhcp.ipAddress & dhcp.netmask);

			byte [] array_cam_addr = cp.get_inetAddress().getAddress();

			int int_cam_addr = 0;
			for (int k = 0; k < array_cam_addr.length; k++)
			{
				int_cam_addr = int_cam_addr | ((array_cam_addr[k] & 0xFF) << k * 8);
			}

			int int_cam_network_addr = (int_cam_addr & dhcp.netmask);

			if (int_dhcp_network_addr != int_cam_network_addr)
			{
				// camera is not in the local network, don't do the local scan
				Log.d("mbp", cp.getName() + " is not in local, should skip scannning");
				return true;
			}
		}

		Log.d("mbp", cp.getName() + " is in local, go to scanning");
		return false;
	}


	/**
	 * @param out_DataBuff - data array
	 * 
	 *  return num of bytes read on success 
	 *         -1 on read failed 
	 *  
	 */
	private int  receiveResponseViaTCP(byte [] out_DataBuff, InetAddress [] from) {
		
		ServerSocket srvSock = null; 
		Socket client = null; 
		DataInputStream inputStream = null;
		int byte_read = -1; 
		//create aTCP server on port 
		try {
			srvSock= new ServerSocket(TCP_REPLY_PORT);
			srvSock.setSoTimeout(2000); // accept timeout 2sec 
			//block until there's a client 
			client = srvSock.accept();
			
			if(client != null )
			{
				inputStream = new DataInputStream(
	            		new BufferedInputStream(client.getInputStream(),1024));
				byte_read = inputStream.read(out_DataBuff);
				
				
				from[0] = client.getInetAddress();
			}
			
			//just close it 
			srvSock.close(); 
		} catch (IOException e) {
			Log.d("mbp","Failed to create a server socket");
			if (srvSock != null)
			{
				try {
					srvSock.close();
				} catch (IOException e1) {
				}
			}
			
		}
		return byte_read; 
	}

	private  void send_page_request_new(CamProfile [] cameras)
	{
		DatagramSocket socket = null;
		DatagramPacket req_packet = null;
		byte [] page_req = null; 
		InetAddress scan_results[] = new InetAddress [MAX_CHANNEL_NUM];
		String scan_results_mac [] = new String[MAX_CHANNEL_NUM];
		int next_channel  = 0;
		reduced_scan_results = null;
		try {
			socket = new DatagramSocket(REPLY_PORT+5);
			socket.setBroadcast(true);
			socket.setSoTimeout(6000); //Timeout in 3 sec
		} catch (SocketException e1) {
			Log.e("mbp","Failed to create socket for Scan request exception: " + e1.getLocalizedMessage());
			socket = null;
		}

		if (socket != null )
		{
			for (int i = 0; i< cameras.length; i++)
			{
				try {
					page_req =  build_page_request(cameras[i].get_MAC());
				} catch (InvalidObjectException e) {
					Log.d("mbp","INvalid mac .. skip ");
					continue;//skip this entry
				}


				try {
					
					/* Check UDP Protocol 20110915 doc for the size*/
					req_packet = new DatagramPacket(page_req, page_req.length,
							broadcast, REQUEST_PORT);
					
					int retry = 3;
					do 
					{

						Page_Receiver pr = new Page_Receiver(); 
						Thread _pr = new Thread(pr);
						_pr.start(); //start rcving thread before sending 
						
						socket.send(req_packet);


						_pr.join(6000);
						if (_pr.isAlive())
						{
							Log.d("mbp", "Page rcver thread is still alive, interrupt it now!");
							_pr.interrupt();
						}

						if (pr.get_cam_address() != null)
						{
							scan_results[next_channel] = pr.get_cam_address();
							/* Check UDP Protocol 20110915 doc for the size*/
							scan_results_mac[next_channel] =pr.get_cam_mac();

							next_channel ++;
							break;
						}
						else
						{

						}
					} while (retry -->0);

				} catch (UnsupportedEncodingException e) 
				{
				}
				catch (SocketException e) {
					Log.d("mbp", "Scan msg Sockexception: " + e.getLocalizedMessage());

				} catch (IOException e) {
					Log.d("mbp", "Scan msg IOexception: " + e.getLocalizedMessage());
				} catch (InterruptedException e) {
					Log.d("mbp", "scan msg Interrupted exception: " + e.getLocalizedMessage());
					//e.printStackTrace();
				}


			}

			socket.close();
		}

		if (next_channel> 0)
		{
			reduced_scan_results = new CamProfile[next_channel];

			for (int i = 0; i<next_channel; i++)
			{
				reduced_scan_results[i] = new CamProfile(scan_results[i],scan_results_mac[i]);
			}

		}
	}

	private class Page_Receiver implements Runnable{

		private DatagramSocket socket;
		private InetAddress scan_results;

		private String scan_results_mac  ;
		Page_Receiver ()
		{
			try {
				socket = new DatagramSocket(REPLY_PORT);
				socket.setBroadcast(true);
				socket.setSoTimeout(4000); //Timeout in 3 sec
			} catch (SocketException e1) {
				Log.e("mbp","Failed to create socket for Scan request exception: " + e1.getLocalizedMessage());
				socket = null;
			}
		}
		public String get_cam_mac()
		{
			return scan_results_mac;
		}
		public InetAddress  get_cam_address()
		{
			return scan_results;
		}
		public void run()
		{
			byte [] buf = new byte[41];
			DatagramPacket  recv_packet;
			recv_packet = new DatagramPacket(buf, buf.length);
			//Receive reply
			try {
				socket.receive(recv_packet);
			} catch (IOException e) {
			}

			String signature;
			try {
				signature = new String (recv_packet.getData(),0,7, "UTF-8");
				if ( signature.equalsIgnoreCase("Mot-Cam"))
				{
					Log.d("mbp", "recv scan-res from: " + recv_packet.getAddress().toString());
					scan_results = recv_packet.getAddress();
					/* Check UDP Protocol 20110915 doc for the size*/
					scan_results_mac= new String(recv_packet.getData(),24,17,"UTF-8");

				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			socket.close();
		}
	}


	/** NOT USED --------------
	 * Send 2 broadcast messages and wait for responses.  
	 * 
	 * @param data
	 */
	private  void send_scan_request (byte[] data )
	{
		DatagramSocket socket = null;
		DatagramPacket req_packet = null, recv_packet= null;;
		int next_channel  = 0;
		InetAddress scan_results[] = new InetAddress [MAX_CHANNEL_NUM];
		String scan_results_mac [] = new String[MAX_CHANNEL_NUM];
		reduced_scan_results = null;
		try {
			socket = new DatagramSocket(REPLY_PORT);
			socket.setBroadcast(true);
			req_packet = new DatagramPacket(data, data.length,
					broadcast, REQUEST_PORT);
			socket.send(req_packet);

			//send twice
			socket.send(req_packet);

			/* Check UDP Protocol 20110915 doc for the size*/
			byte[] buf = new byte[41];
			socket.setSoTimeout(3000); //Timeout in 3 sec

			while (next_channel < MAX_CHANNEL_NUM)
			{
				recv_packet = new DatagramPacket(buf, buf.length);
				//Receive reply
				socket.receive(recv_packet);


				boolean duplicate = false;
				for (int i= 0; i < next_channel ; i ++)
				{
					if (scan_results[i].toString().equals(recv_packet.getAddress().toString()))
					{
						duplicate = true;
						break; 
					}

				}

				if ( duplicate != true)
				{

					String signature = new String (recv_packet.getData(),0,7, "UTF-8");
					if ( signature.equalsIgnoreCase("Mot-Cam"))
					{
						//Log.d("mbp", "finish recving: " + recv_packet.getAddress().toString());
						scan_results[next_channel] = recv_packet.getAddress();
						/* Check UDP Protocol 20110915 doc for the size*/
						scan_results_mac[next_channel] = new String(recv_packet.getData(),24,17,"UTF-8");

						next_channel ++;
					}
				}


			}

		}
		catch (SocketException e) {
			e.printStackTrace();
		}
		catch (InterruptedIOException ie)
		{
			Log.e("mbp","interrupted timeout? with " + ie.toString());
		} 
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		finally
		{
			if (socket != null)
			{
				socket.close();
			}

			if (next_channel> 0)
			{
				reduced_scan_results = new CamProfile[next_channel];

				for (int i = 0; i<next_channel; i++)
				{
					reduced_scan_results[i] = new CamProfile(scan_results[i],scan_results_mac[i]);
				}

			}


		}

	}

	private void  getAddresses() throws IOException {
		WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow

		int broadcast_addr = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;


		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast_addr >> k * 8) & 0xFF);

		broadcast =  InetAddress.getByAddress(quads);
		
		//broadcast = InetAddress.getByName("192.168.0.109");
		//Log.d("mbp", "bc: " + broadcast);

		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((dhcp.ipAddress >> k * 8) & 0xFF);

		own_ip = InetAddress.getByAddress(quads);


		return;
	}


	/* �Mot-Cam QUERY   *               192.168.1.7    aa:bb:cc:dd:ee:ff�*/
	private byte[] build_page_request(String device_mac_with_colon) throws InvalidObjectException
	{
		String scan_request = SCAN_REQUEST;
		int blank_chars = 0;

		/* add Own IP */
		String padded_own_ip = own_ip.toString().substring(1);
		blank_chars = 16 - own_ip.toString().length() ;
		if ( blank_chars > 0 )
		{
			for (int i = 0 ; i< blank_chars; i++)
			{
				padded_own_ip +=" ";
			}
		}
		scan_request += padded_own_ip;

		if ( device_mac_with_colon.length() != 17)
		{
			throw new InvalidObjectException("Invalid Device Mac : " +device_mac_with_colon );
		}
		/* add device mac */
		scan_request+= device_mac_with_colon.toLowerCase();

		//Log.d("mbp","page_request : " +scan_request);


		byte[] utf8 = null;
		try {
			utf8 = scan_request.getBytes("UTF-8");

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return utf8;
	}


}
