package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.UDTRequestSendRecv;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

public class RelayVideoStreamer extends VideoStreamer {
	
	private Tracker tracker;
	private EasyTracker easyTracker;
	
	public RelayVideoStreamer(Handler h, Context m, String device_ip, int port) {
		super(h, m, device_ip, port);
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(m);
		tracker = easyTracker.getTracker();
	}
	
	@Override
	protected URLConnection tryConnect() throws IOException {
		URLConnection conn = null;
		URL stream_url = null;
		int retry = 0;

		String usr_pass = String.format("%s:%s", http_usr, http_pass);

		if (isRunning == false) {
			return null;
		}

		try {
			
			String http_cmd = "http://" + PublicDefine.RELAY_SERVER2 +
					"/streamingservice?action=command&command=get_relay_stream" +
					"&channelId=" + ((BabyMonitorRelayAuthentication)bm_auth).getChannelID() +
					"&mac=" + ((BabyMonitorRelayAuthentication)bm_auth).getDeviceMac() +
					"&skey=" + ((BabyMonitorRelayAuthentication)bm_auth).getSSKey();

			stream_url = new URL(http_cmd);
				
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		HttpURLConnection videoConn = null;
		int responseCode = 0;
		if (_inputStream != null) {
			_inputStream.close();
			_inputStream = null;
		}
		// to change to 15.......
		while (retry++ < 15 && isRunning) {
			try {

				videoConn = (HttpURLConnection) stream_url.openConnection();
				videoConn.addRequestProperty(
						"Authorization",
						"Basic "
								+ Base64.encodeToString(
										usr_pass.getBytes("UTF-8"),
										Base64.NO_WRAP));

				videoConn.setConnectTimeout(15000);
				videoConn.setReadTimeout(20000);
				responseCode = videoConn.getResponseCode();
				_inputStream = new DataInputStream(new BufferedInputStream(
						videoConn.getInputStream()));
				break;
			} catch (IOException ex) {
				videoConn = null;
				if (ex instanceof SocketException) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Log.d("mbp", "tryConnect():connect error : ");
				}
				else {
					Log.d("mbp", "tryConnect():connect error : ");
				}

				if (responseCode == 602) {
					// special case remote server has timeout
					throw new IOException(REMOTE_TIMEOUT);
				}
				else if ( (responseCode == 601) || (responseCode == 401) ) 
				{
					session_key_mismatched_code = responseCode;
					throw new IOException(SESSION_KEY_MISMATCHED);
				}
				
				/* 20130218: hoang: issue 1222
				 * exit if in local
				 */
				if (retry == 2)
				{
					throw new IOException(CAMERA_IS_NOT_AVAILABLE);
				}

			}
		}

		// Double check
		if ((isRunning == false) && _inputStream != null) {
			videoConn = null;
			_inputStream.close();
			_inputStream = null;

		}

		return videoConn;
	}
	
	
	@Override
	public void initQueries() {
		String request,res ;
		//// NEW WAY TO SEND COMMAND

		request = PublicDefine.GET_MELODY_VALUE;
		
		res = UDTRequestSendRecv.sendRequest_via_stun(
				((BabyMonitorRelayAuthentication)bm_auth).getRestrationId(),
				((BabyMonitorRelayAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorRelayAuthentication)bm_auth).getUser(),
				((BabyMonitorRelayAuthentication)bm_auth).getPass());
		
		updateMelodyStatus(res);
		
		SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);
		if (img_res == PublicDefine.RESOLUTON_QVGA)
		{
			request = PublicDefine.SET_RESOLUTION_QVGA;
		}
		else
		{
			request = PublicDefine.SET_RESOLUTION_VGA;
		}
		UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorRelayAuthentication)bm_auth).getDeviceMac(),
				((BabyMonitorRelayAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorRelayAuthentication)bm_auth).getUser(),
				((BabyMonitorRelayAuthentication)bm_auth).getPass());
		if (_resolutionUpdater != null) {
			_resolutionUpdater.updateResolution(img_res);
		}
	}
	
	
	@Override
	protected void closeCurrentSession()
	{
		Log.d("mbp", "Closing session---on BMport: "+device_port+ " frmlocalport:"+
				((BabyMonitorRelayAuthentication)bm_auth).getLocalPort());
		
		if (((BabyMonitorRelayAuthentication)bm_auth) == null)
		{
			//forget it
			Log.d("mbp", "bm_auth = null"); 
			return; 
		}

		/* @param urls[0] : mac 
		 *        urls[1] : channelId 
		 *        urls[2] : camera query (command part only, eg: get_skp_volume) 
		 *        urls[3] : user
		 *      - urls[4] : pass  
		 */

		Log.d("mbp", "Closing session--- ");
		String request = PublicDefine.CLOSE_UDT_SESSION;

		String response = UDTRequestSendRecv.sendRequest_via_stun(
				((BabyMonitorRelayAuthentication)bm_auth).getDeviceMac(),
				((BabyMonitorRelayAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorRelayAuthentication)bm_auth).getUser(),
				((BabyMonitorRelayAuthentication)bm_auth).getPass());
	}
	

	@Override
	protected void checkDisconnectReason()
	{
		closeCurrentSession();
		Log.e("mbp", "closeCurrentSession done" );

		super.checkDisconnectReason();
	}
}
