package com.msc3;

import com.blinkhd.R;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PlaybackActivity extends Activity implements IVideoSink, IAudioSink {

	//private ImageView _imageView;
	private boolean _playInProgress;

	private long _totalPausedTime;
	private long _pausedStartTime;

	/* Runnables */
	private AviPlayer _player;
	private PCMPlayer _pcmPlayer;

	private SurfaceHolder _surfaceHolder;
	private VideoSurfaceView _vImageView;

	private Bitmap video_background; 
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player);

		//DBG
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			video_background = BitmapFactory.decodeResource(this.getResources(),
					R.drawable.homepage);
		}
		else 
		{
			video_background = BitmapFactory.decodeResource(this.getResources(),
					R.drawable.homepage_p);
		}
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		video_background = Bitmap.createScaledBitmap(video_background, wm
				.getDefaultDisplay().getWidth(), wm.getDefaultDisplay()
				.getHeight(), false);
		Bundle extras = getIntent().getExtras();

		/**
		 *  There are 2 ways to play a video file 
		 *  - put a key "filename" in extra corresponding to an absolute path 
		 *  - Gallery - way, put a URI in getData()
		 * 
		 */



		if (extras != null) {
			/*--- USING Surface View : check if surface is enabled in layout */ 
			_vImageView = (VideoSurfaceView)findViewById(R.id.playBackArea);
			_surfaceHolder  =_vImageView.get_SurfaceHolder();

			final String filename = extras.getString("filename");
			if (filename != null)
			{
				_player = new AviPlayer(filename);

				_player.addVideoSink(this);
				//_player.addAudioSink(this);

				_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, true);

				ImageButton stop_btn = (ImageButton) findViewById(R.id.playback_stop_btn);


				stop_btn.setOnTouchListener(
						new ButtonTouchListener(getResources().getDrawable(R.drawable.video_stop_btn),
								getResources().getDrawable(R.drawable.video_stop_btn_d)));
				stop_btn.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						/* Stop the playing thread */
						onVideoEnd();					

					}
				});

				ImageButton pause_btn = (ImageButton) findViewById(R.id.playback_pause_btn);

				pause_btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						onVideoPause();
						if (_player.isPaused())
						{
							((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.video_play_btn));
						}
						else
						{
							((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.video_pause_btn));
						}
					}
				});

				_vImageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						RelativeLayout controlBar = (RelativeLayout) findViewById(R.id.playback_control_bar);
						if (controlBar != null) 
						{
							if (controlBar.isShown()) 
							{
								controlBar.setVisibility(View.INVISIBLE);
							}
							else 
							{
								controlBar.setVisibility(View.VISIBLE);
							}
						}
					}
				});

				return;
			}

		}

		if ( getIntent().getData() != null)
		{
			/*--- USING Surface View : check if surface is enabled in layout */ 
			_vImageView = (VideoSurfaceView)findViewById(R.id.playBackArea);
			_surfaceHolder  =_vImageView.get_SurfaceHolder();

			InputStream is;
			try {
				is = getContentResolver().openInputStream( getIntent().getData());
				_player = new AviPlayer(is);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				//this.finish();
				showDialog(DIALOG_FILE_NOT_FOUND);
				return;

			}


			_player.addVideoSink(this);
			_player.addAudioSink(this);

			_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, true);
			ImageButton stop_btn = (ImageButton) findViewById(R.id.playback_stop_btn);
			stop_btn.setOnTouchListener(
					new ButtonTouchListener(getResources().getDrawable(R.drawable.video_stop_btn),
							getResources().getDrawable(R.drawable.video_stop_btn_d)));
			stop_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					/* Stop the playing thread */
					onVideoEnd();					
				}
			});

			ImageButton pause_btn = (ImageButton) findViewById(R.id.playback_pause_btn);
			pause_btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					onVideoPause();
					if (_player.isPaused())
					{
						((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.video_play_btn));
					}
					else
					{
						((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.video_pause_btn));
					}
				}
			});

			_vImageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					RelativeLayout controlBar = (RelativeLayout) findViewById(R.id.playback_control_bar);
					if (controlBar != null) 
					{
						if (controlBar.isShown()) 
						{
							controlBar.setVisibility(View.INVISIBLE);
						}
						else 
						{
							controlBar.setVisibility(View.VISIBLE);
						}
					}
				}
			});
		}

		else
		{
			this.finish();
		}



	}

	/**
	 * @see android.app.Activity#onStart()
	 */
	@Override
	protected void onStart() {
		super.onStart();



		/*--- USING Surface View*/
		_playInProgress = true;
		_totalPausedTime = 0;

		if (_player != null &&_pcmPlayer!= null )
		{
			new Thread(_player).start();
			new Thread(_pcmPlayer).start();
		}

		final TextView recTime = (TextView) findViewById(R.id.textPlayTime);
		if (recTime != null)
		{
			recTime.setVisibility(View.VISIBLE);

			Runnable timer = new Runnable() {

				@Override
				public void run() {
					long startTime = System.currentTimeMillis();
					while (_player!=null && _player.isPlaying())
					{
						long duration = System.currentTimeMillis() - startTime;
						/*Date date=new Date(duration);
						SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
						final String duration_str = df.format(date);*/

						final String duration_str = Util.formatMillis(duration - _totalPausedTime); 



						PlaybackActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								recTime.setText(duration_str);
							}
						});

						while (_player.isPaused())
						{
							try
							{
								Thread.sleep(10);
							}
							catch (Exception ex)
							{}
						}
					}

					PlaybackActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							recTime.setText("");
							recTime.setVisibility(View.INVISIBLE);

						}
					});

				}
			};

			new Thread(timer).start();

		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		Log.d("mbp", this.getClass().getName()+ " onStop");
		/*--- USING Surface View*/
		if(_player != null) {
			_player.stop();
		}

		if (_pcmPlayer != null)
		{
			_pcmPlayer.stop();
		}

	}
	protected void onDestroy()
	{
		super.onDestroy();
		Log.d("mbp", this.getClass().getName()+ " onDestroy");
	}




	@Override
	public void onFrame(byte[] frame, byte[] pcm, int pcm_len) {


		if (frame.length > 0)
		{

			Canvas c = null;
			//Bottleneck is here: decodeByteArray takes too long 
			Bitmap bit = BitmapFactory.decodeByteArray(frame, 0, frame.length);

			if (bit == null)
			{
				return; 
			}


			try {
				c = _surfaceHolder.lockCanvas(null);
				if (c == null) {
					return;
				}


				int left = 0;
				int top  = 0;
				int right = bit.getWidth();
				int bottom = bit.getHeight();
				int destWidth;
				int destHeight;

				float ratio = (float) right / bottom;

				float fh = c.getWidth() / ratio;

				destWidth = c.getWidth();
				destHeight = (int) fh;

				int dst_top = 0;
				if (destHeight > c.getHeight())
				{
					int delta = destHeight - c.getHeight();
					dst_top = -delta / 2;
					destHeight -= delta / 2;
				}
				else
				{
					// expand the height
					destHeight = c.getHeight();
				}


				Rect src = new Rect(left,top,right, bottom);
				Rect dest = new Rect(0,dst_top,destWidth,destHeight);

				synchronized (_surfaceHolder) {
					c.drawBitmap(bit,src ,dest, null);
				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}

			/* pcm, pcm_len is NOT used here */
		}

	}

	@Override
	public void onPCM(byte[] pcmData) {
		if(_pcmPlayer != null) {
			_pcmPlayer.writePCM(pcmData, pcmData.length);
		}
	}

	@Override
	public void onInitError(String errorMessage) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onVideoEnd() {
		_pcmPlayer.stop();
		_vImageView.post(new Runnable() {
			public void run() {
				finish();
			}
		});
	}

	public void onVideoPause()
	{
		if (_player.isPaused())
		{
			_totalPausedTime += System.currentTimeMillis() - _pausedStartTime;
			_player.resume();
		}
		else
		{
			_pausedStartTime = System.currentTimeMillis();
			_player.pause();
		}
	}

	private static final int DIALOG_FILE_NOT_FOUND = 1; 
	protected Dialog onCreateDialog(int id) 
	{
		AlertDialog.Builder builder;
		AlertDialog alert;
		Spanned msg;
		switch (id){
		case DIALOG_FILE_NOT_FOUND:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+ getString(R.string.the_file_you_are_trying_to_play_is_not_found_please_check_) +"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					PlaybackActivity.this.finish();
				}
			});

			alert = builder.create();
			return alert;
		default:
			break;
		}


		return null;
	}



}
