package com.msc3;

import com.blinkhd.R;
import java.util.ArrayList;
import java.util.List;

import com.msc3.registration.AccessPointAdapter;
import com.msc3.registration.SingleCamConfigureActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ConnectToNetworkActivity extends Activity implements
		IWifiScanUpdater, Handler.Callback
{

	public static final String bool_ForceChooseWifi = "ConnectToNetworkActivity_bool_ForceChooseWifi";
	public static final String bool_AddCameraWhenUsing3G = "ConnectToNetworkActivity_bool_AddCameraWhenUsing3G";

	private static final int CONNECTING_DIALOG = 0;
	private static final int ASK_WEP_KEY_DIALOG = 1;
	private static final int ASK_WPA_KEY_DIALOG = 2;
	private static final int CONNECTION_FAILED_DIALOG = 3;
	private static final int CONNECT_THRU_MOBILE_NETWORK = 4;
	private static final int DIALOG_ADD_WIFI_NETWORK = 5;
	private static final int CONNECT_THRU_MOBILE_NETWORK_WITH_OPTION = 6;

	private String selected_SSID;// with quotes

	private boolean should_save_wifi_data;
	private WifiConfiguration newConf_needs_key;
	private String current_security_type;
	private String current_wep_auth_mode; // Dirty way

	private boolean shouldForceEnterWifiPass;
	private boolean addCameraWhenUsing3G;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle extra = getIntent().getExtras();
		shouldForceEnterWifiPass = false;
		addCameraWhenUsing3G = false;
		if (extra != null)
		{
			shouldForceEnterWifiPass = extra.getBoolean(bool_ForceChooseWifi,
					false);
			addCameraWhenUsing3G = extra.getBoolean(bool_AddCameraWhenUsing3G,
					false);
		}

	}

	/**
	 * @param context
	 * @return - True if internet connection is available on 3g False otherwise
	 */
	public static boolean haveInternetViaOtherMedia(Context context)
	{

		if (Build.MODEL != null
				&& (Build.MODEL.equals(PublicDefine.PHONE_MBP2k)
						|| Build.MODEL.equals(PublicDefine.PHONE_MBP1k) || Build.MODEL
							.equals(PublicDefine.PHONE_IHOMEPHONE5)))

		{
			return false;
		}

		ConnectivityManager conMan = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null)
		{
			// mobile
			State mobile = conMan.getNetworkInfo(
					ConnectivityManager.TYPE_MOBILE).getState();
			if (mobile == NetworkInfo.State.CONNECTED
					|| mobile == NetworkInfo.State.CONNECTING)
			{
				// mobile
				return true;
			}
		}
		return false;
	}

	protected void onStart()
	{
		super.onStart();

		if (addCameraWhenUsing3G == false)
		{
			final WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);

			if (shouldForceEnterWifiPass == true)
			{
				/* re-use Vox_main, just an empty and transparent activity */
				setContentView(R.layout.bb_is_wifi_screen);

				TextView title = (TextView) findViewById(R.id.textTitle);
				title.setText(R.string.connect_camera_to_wi_fi);

				Button connect = (Button) findViewById(R.id.connectWifi);
				connect.getBackground().setColorFilter(0xff7fae00,
						android.graphics.PorterDuff.Mode.MULTIPLY);
				connect.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						w.setWifiEnabled(true);
						while ((w.getWifiState() != WifiManager.WIFI_STATE_ENABLED))
						{
							try
							{
								Thread.sleep(1000);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}

						WifiScan ws = new WifiScan(
								ConnectToNetworkActivity.this,
								ConnectToNetworkActivity.this);
						ws.execute("Scan now");
					}
				});

			}
			else
			{

				setContentView(R.layout.bb_is_waiting_screen);

				if ((w.getWifiState() == WifiManager.WIFI_STATE_ENABLED)
						&& (w.getConnectionInfo() != null)
						&& (w.getConnectionInfo().getSSID() != null)
						&& (!w.getConnectionInfo().getSSID()
								.startsWith(PublicDefine.DEFAULT_SSID))
						&& (!w.getConnectionInfo().getSSID()
								.startsWith(PublicDefine.DEFAULT_SSID_HD)))
				{

					// Wifi is up, and we are connecting to some network--
					// simply finish now
					setResult(RESULT_OK);
					finish();
				}
				/* 20120413- Support connecting via other media (non-wifi) */
				else if (haveInternetViaOtherMedia(this))
				{
					// showDialog(CONNECT_THRU_MOBILE_NETWORK);
					/*
					 * 20130121: hoang: issue 1173: add option to skip popup
					 * messages "turn on wifi" when the phone don't connects to
					 * internet by wifi
					 */
					SharedPreferences settings = getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);
					boolean shouldSkip = settings.getBoolean(
							PublicDefine.PREFS_DONT_ASK_ME_AGAIN, false);
					if (shouldSkip == false)
					{
						showDialog(CONNECT_THRU_MOBILE_NETWORK_WITH_OPTION);
					}
					else
					{
						boolean shouldTurnOnWifi = settings.getBoolean(
								PublicDefine.PREFS_SHOULD_TURN_ON_WIFI, false);
						if (shouldTurnOnWifi == false)
						{
							Log.d("mbp", "Don't turn on wifi");
							setResult(RESULT_OK);
							finish();
						}
						else
						{
							Log.d("mbp", "Turn on wifi");
							turnOnWifiAndScann();
						}
					}
				}
				else
				{
					turnOnWifiAndScann();
				}

			}
		}

		else
		{
			turnOnWifiAndScann();
			Intent returnIntent = new Intent();
			ConnectToNetworkActivity.this.setResult(RESULT_OK, returnIntent);
		}
	}

	private void turnOnWifiAndScann()
	{
		/* re-use Vox_main, just an empty and transparent activity */
		setContentView(R.layout.vox_main);

		WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		w.setWifiEnabled(true);
		while ((w.getWifiState() != WifiManager.WIFI_STATE_ENABLED))
		{
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		WifiScan ws = new WifiScan(this, this);
		ws.execute("Scan now");
	}

	protected void onPause()
	{
		super.onPause();

	}

	protected void onResume()
	{
		super.onResume();
	}

	protected void onStop()
	{
		super.onStop();
	}

	protected void onDestroy()
	{
		super.onDestroy();

	}

	private synchronized void store_default_wifi_info()
	{
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_PWD);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX);
		editor.remove(PublicDefine.PREFS_SAVED_WIFI_SSID);
		if (should_save_wifi_data == true)
		{

			String ssid_no_quote = newConf_needs_key.SSID.substring(1,
					newConf_needs_key.SSID.length() - 1);
			String pwd_no_quote = null;

			editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC,
					current_security_type);
			editor.putString(PublicDefine.PREFS_SAVED_WIFI_SSID, ssid_no_quote);

			if (current_security_type.startsWith("WPA"))
			{
				Log.d("mbp", "Store wpa key");
				if (newConf_needs_key.preSharedKey.startsWith("\""))
				{
					pwd_no_quote = newConf_needs_key.preSharedKey.substring(1,
							newConf_needs_key.preSharedKey.length() - 1);
				}
				else
				{
					pwd_no_quote = newConf_needs_key.preSharedKey;
				}
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_PWD,
						pwd_no_quote);

			}
			else if (current_security_type.startsWith("WEP"))
			{
				int key_index = newConf_needs_key.wepTxKeyIndex + 1;

				pwd_no_quote = newConf_needs_key.wepKeys[newConf_needs_key.wepTxKeyIndex];

				if (pwd_no_quote.startsWith("\""))
				{
					// Strip quote away
					pwd_no_quote = pwd_no_quote.substring(1,
							pwd_no_quote.length() - 1);
				}

				editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_IDX,
						String.valueOf(key_index));
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_PWD,
						pwd_no_quote);
				editor.putString(PublicDefine.PREFS_SAVED_WIFI_SEC_WEP_AUTH,
						current_wep_auth_mode);

			}

			should_save_wifi_data = false;
		}
		editor.commit();

	}

	protected Dialog onCreateDialog(int id)
	{
		final Dialog dialog;
		AlertDialog.Builder builder;
		AlertDialog alert = null;
		switch (id)
		{
		case CONNECTION_FAILED_DIALOG:
			builder = new AlertDialog.Builder(this);
			Spanned msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.ConnectToNetworkActivity_conn_failed)
					+ "</big>");
			builder.setMessage(msg)
					.setCancelable(true)
					.setPositiveButton(getResources().getString(R.string.OK),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.cancel();
								}
							}).setOnCancelListener(new OnCancelListener()
					{
						@Override
						public void onCancel(DialogInterface dialog)
						{
						}
					});

			alert = builder.create();
			return alert;
		case CONNECTING_DIALOG:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.ConnectToNetworkActivity_connecting)
					+ "</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog) dialog).setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{

				}
			});

			((AlertDialog) dialog).setButton(
					getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener()
					{

						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							dialog.cancel();
						}
					});

			return dialog;
		case ASK_WEP_KEY_DIALOG:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			dialog.setContentView(R.layout.bb_is_router_key);
			dialog.setCancelable(true);

			String text = null;
			// Obmit the quote
			text = this.selected_SSID.substring(1,
					this.selected_SSID.length() - 1);

			final TextView ssid_text = (TextView) dialog.findViewById(R.id.t0);
			ssid_text.setText(text);

			/*
			 * 20120724: dont enable wep_options LinearLayout wep_opts =
			 * (LinearLayout) dialog.findViewById(R.id.wep_options); if
			 * (wep_opts != null ) { wep_opts.setVisibility(View.VISIBLE); }
			 * 
			 * Spinner spinner = (Spinner)
			 * dialog.findViewById(R.id.wep_opt_index);
			 * ArrayAdapter<CharSequence> adapter =
			 * ArrayAdapter.createFromResource( this, R.array.key_index,
			 * android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource
			 * (android.R.layout.simple_spinner_dropdown_item);
			 * spinner.setAdapter(adapter); spinner.setSelection(0);
			 * 
			 * spinner = (Spinner) dialog.findViewById(R.id.wep_opt_method);
			 * adapter = ArrayAdapter.createFromResource( this,
			 * R.array.auth_method, android.R.layout.simple_spinner_item);
			 * adapter.setDropDownViewResource(android.R.layout.
			 * simple_spinner_dropdown_item); spinner.setAdapter(adapter);
			 * spinner.setSelection(0);
			 */

			final EditText pwd_text = (EditText) dialog
					.findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
								.getInstance());
						pwd_text.clearFocus();
					}
					return false;
				}
			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;
					// String key_index = null;
					// String auth_method = null;

					EditText text = (EditText) dialog
							.findViewById(R.id.text_key);

					if (text == null)
					{
						return;
					}

					pass_string = text.getText().toString();
					// text.setText(null);

					Spinner spinner = (Spinner) dialog
							.findViewById(R.id.wep_opt_index);
					if (spinner != null)
					{
						// key_index = (String)spinner.getSelectedItem();
						spinner = (Spinner) dialog
								.findViewById(R.id.wep_opt_method);
						// auth_method = (String) spinner.getSelectedItem();
						// current_wep_auth_mode = auth_method;

						// 20120724: Hardcode auth to Open
						current_wep_auth_mode = "Open";
					}

					if (newConf_needs_key != null)
					{

						// 20120724: Hardcode index to 1
						newConf_needs_key.wepTxKeyIndex = 0;// Integer.parseInt(key_index)-1;

						/*
						 * 20120203: WEP key in HEX len should be either 26 or
						 * 10 ??? COULD BE WRONG
						 */
						if ((pass_string.length() == 26 || pass_string.length() == 10)
								&& Util.isThisAHexString(pass_string))
						{
							newConf_needs_key.wepKeys[newConf_needs_key.wepTxKeyIndex] = pass_string;
						}
						else
						{
							newConf_needs_key.wepKeys[newConf_needs_key.wepTxKeyIndex] = '\"' + pass_string + '\"';
						}

					}

					start_connect();

					dialog.cancel();
				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});

			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					ConnectToNetworkActivity.this
							.removeDialog(ASK_WEP_KEY_DIALOG);
				}
			});

			return dialog;
		}
		case ASK_WPA_KEY_DIALOG:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			dialog.setContentView(R.layout.bb_is_router_key);
			dialog.setCancelable(true);

			LinearLayout wep_opts = (LinearLayout) findViewById(R.id.wep_options);
			if (wep_opts != null)
			{
				wep_opts.setVisibility(View.GONE);
			}

			String text = null;
			text = this.selected_SSID.substring(1,
					this.selected_SSID.length() - 1);

			final TextView ssid_text = (TextView) dialog.findViewById(R.id.t0);
			ssid_text.setText(text);

			EditText pwd_text = (EditText) dialog.findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
								.getInstance());

					}
					return false;
				}
			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;

					EditText text = (EditText) dialog
							.findViewById(R.id.text_key);
					if (text == null)
					{
						return;
					}

					pass_string = text.getText().toString();
					// text.setText(null);

					// Log.d("mbp","preshare key: "+ pass_string);

					if (newConf_needs_key != null)
					{

						/*
						 * 20120203: WPA key in HEX len should be 64 ??? COULD
						 * BE WRONG
						 */
						if ((pass_string.length() == 64)
								&& Util.isThisAHexString(pass_string))
						{
							newConf_needs_key.preSharedKey = pass_string;
						}
						else
						{
							newConf_needs_key.preSharedKey = '\"' + pass_string + '\"';
						}

					}

					start_connect();
					dialog.cancel();
				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});
			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					ConnectToNetworkActivity.this
							.removeDialog(ASK_WPA_KEY_DIALOG);
				}
			});

			return dialog;
		}
		case CONNECT_THRU_MOBILE_NETWORK:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources()
									.getString(
											R.string.mobile_data_3g_is_enabled_continue_to_connect_may_incur_air_time_charge_do_you_want_to_proceed_)
							+ "</big>");
			builder.setMessage(msg)
					.setCancelable(true)
					.setPositiveButton(
							getResources().getString(R.string.Proceed),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.dismiss();
									setResult(RESULT_OK);
									finish();
								}
							})
					.setNegativeButton(
							getResources().getString(R.string.turn_on_wifi),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.dismiss();

									turnOnWifiAndScann();
								}
							});

			alert = builder.create();
			return alert;

			/*
			 * 20130121: hoang: issue 1173 create dialog with option
			 * "Don't ask me again"
			 */
		case CONNECT_THRU_MOBILE_NETWORK_WITH_OPTION:
			builder = new AlertDialog.Builder(this);
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			LinearLayout layout1 = (LinearLayout) inflater.inflate(
					R.layout.bb_dont_ask_me_again, null);
			final CheckBox dontAskAgain = (CheckBox) layout1
					.findViewById(R.id.skip);
			msg = Html
					.fromHtml("<big>"
							+ getResources()
									.getString(
											R.string.mobile_data_3g_is_enabled_continue_to_connect_may_incur_air_time_charge_do_you_want_to_proceed_)
							+ "</big>");
			builder.setMessage(msg)
					.setView(layout1)
					.setCancelable(true)
					.setPositiveButton(
							getResources().getString(R.string.Proceed),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.dismiss();
									if (dontAskAgain.isChecked())
									{
										SharedPreferences settings = ConnectToNetworkActivity.this
												.getSharedPreferences(
														PublicDefine.PREFS_NAME,
														0);
										SharedPreferences.Editor editor = settings
												.edit();
										editor.putBoolean(
												PublicDefine.PREFS_DONT_ASK_ME_AGAIN,
												true);
										editor.putBoolean(
												PublicDefine.PREFS_SHOULD_TURN_ON_WIFI,
												false);
										editor.commit();
									}
									setResult(RESULT_OK);
									finish();
								}
							})
					.setNegativeButton(
							getResources().getString(R.string.turn_on_wifi),
							new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog,
										int which)
								{
									dialog.dismiss();
									if (dontAskAgain.isChecked())
									{
										SharedPreferences settings = ConnectToNetworkActivity.this
												.getSharedPreferences(
														PublicDefine.PREFS_NAME,
														0);
										SharedPreferences.Editor editor = settings
												.edit();
										editor.putBoolean(
												PublicDefine.PREFS_DONT_ASK_ME_AGAIN,
												true);
										editor.putBoolean(
												PublicDefine.PREFS_SHOULD_TURN_ON_WIFI,
												true);
										editor.commit();
									}
									turnOnWifiAndScann();
								}
							});

			alert = builder.create();
			return alert;

		case DIALOG_ADD_WIFI_NETWORK:
		{
			dialog = new Dialog(this, R.style.myDialogTheme);
			dialog.setContentView(R.layout.bb_is_other_router);
			dialog.setCancelable(true);

			final EditText pwd_text = (EditText) dialog
					.findViewById(R.id.text_key);
			pwd_text.setOnEditorActionListener(new OnEditorActionListener()
			{

				@Override
				public boolean onEditorAction(TextView v, int actionId,
						KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_DONE)
					{
						v.setTransformationMethod(PasswordTransformationMethod
								.getInstance());

					}
					return false;
				}

			});

			Spinner spinner = (Spinner) dialog.findViewById(R.id.sec_type);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter
					.createFromResource(this, R.array.sec_type,
							android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(adapter);
			spinner.setSelection(0);
			spinner.setOnItemSelectedListener(new OnItemSelectedListener()
			{

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3)
				{

					Log.d("mbp", "Selected item is: " + arg2);

					LinearLayout ll = (LinearLayout) dialog
							.findViewById(R.id.linearLayout_pwd);
					if (ll != null)
					{
						if (arg2 != 0)
						{
							ll.setVisibility(View.VISIBLE);
						}
						else
						{
							ll.setVisibility(View.GONE);
						}
					}
					else
					{
						Log.d("mbp", "ll is nULL");
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0)
				{

				}

			});

			// setup connect button
			Button connect = (Button) dialog.findViewById(R.id.connect_btn);
			connect.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					String pass_string = null;
					String ssid_string = null;

					EditText ssid_ = (EditText) dialog
							.findViewById(R.id.text_ssid);
					if (ssid_ == null)
					{
						return;
					}

					ssid_string = ssid_.getText().toString();

					EditText text = (EditText) dialog
							.findViewById(R.id.text_key);
					if (text == null)
					{
						return;
					}

					// Log.d("mbp", "WPA KEY: "+text.getText());
					pass_string = text.getText().toString();
					// text.setText(null);

					/* Construct a WifiConf object here */
					WifiConfiguration _conf = new WifiConfiguration();
					_conf.SSID = "\"" + ssid_string + "\""; // with quoted
					_conf.hiddenSSID = true; //

					Spinner secType = (Spinner) dialog
							.findViewById(R.id.sec_type);
					switch (secType.getSelectedItemPosition())
					{
					case 0: // OPEN
						current_security_type = "OPEN";
						_conf.allowedKeyManagement.set(KeyMgmt.NONE);
						break;
					case 1: // WEP

						current_security_type = "WEP";
						_conf.allowedAuthAlgorithms
								.set(WifiConfiguration.AuthAlgorithm.OPEN);
						_conf.wepTxKeyIndex = 0;
						_conf.allowedKeyManagement
								.set(WifiConfiguration.KeyMgmt.NONE);
						_conf.allowedGroupCiphers
								.set(WifiConfiguration.GroupCipher.WEP40);
						// non HEX key needs double quote
						_conf.wepKeys[_conf.wepTxKeyIndex] = "\"" + pass_string
								+ "\"";

						break;
					case 2: // WPA2
						current_security_type = "WPA";
						_conf.allowedAuthAlgorithms
								.set(WifiConfiguration.AuthAlgorithm.OPEN);
						_conf.allowedKeyManagement
								.set(WifiConfiguration.KeyMgmt.WPA_PSK);
						// non HEX key needs double quote
						_conf.preSharedKey = "\"" + pass_string + "\"";

						break;
					default:// UNKNOWN
						break;
					}

					newConf_needs_key = _conf;

					ConnectToNetworkActivity.this.selected_SSID = _conf.SSID;

					should_save_wifi_data = true;
					start_connect();

					dialog.cancel();
				}
			});

			Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
			cancel.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					dialog.cancel();
				}
			});
			dialog.setOnCancelListener(new OnCancelListener()
			{

				@Override
				public void onCancel(DialogInterface dialog)
				{
					removeDialog(DIALOG_ADD_WIFI_NETWORK);
				}
			});

			return dialog;
		}
		default:
			break;
		}

		return null;
	}

	@Override
	public void scanWasCanceled()
	{
		router_selection(null); // show some ui and return
	}

	@Override
	public void updateWifiScanResult(List<ScanResult> result)
	{
		router_selection(result);
	}

	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

	}

	private void router_selection(List<ScanResult> results)
	{
		setContentView(R.layout.bb_is_wifi_selection);

		TextView title = (TextView) findViewById(R.id.textTitle);
		if (title != null)
		{
			title.setText(R.string.select_your_wifi_network);
		}

		Button rescan = (Button) findViewById(R.id.buttonRescan);
		if (rescan != null)
		{
			rescan.getBackground().setColorFilter(0xff7fae00,
					android.graphics.PorterDuff.Mode.MULTIPLY);
			rescan.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					WifiScan ws = new WifiScan(ConnectToNetworkActivity.this,
							ConnectToNetworkActivity.this);
					ws.execute("Scan now");
				}
			});
		}

		if (results == null)
			return;

		ArrayList<NameAndSecurity> ap_list;

		ListView wifi_list = (ListView) findViewById(R.id.wifi_list);
		if (wifi_list == null)
		{
			return;
		}
		
		ap_list = new ArrayList<NameAndSecurity>(results.size());
		for (ScanResult result : results)
		{
			if (result.SSID != null)
			{
				/* 20120220:filter those camera network out of this list */
				if (!result.SSID.startsWith(PublicDefine.DEFAULT_CAM_NAME))
				{
					NameAndSecurity _ns = new NameAndSecurity(result.SSID,
							result.capabilities, result.BSSID);
					_ns.setLevel(result.level);
					_ns.setHideMac(true);
					ap_list.add(_ns);

				}

			}

		}

		AccessPointAdapter ap_adapter = new AccessPointAdapter(this, ap_list);
		wifi_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		wifi_list.setAdapter(ap_adapter);
		wifi_list.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id)
			{

				// last element is not a NameAndSec object
				if (position == (parent.getCount() - 1))
				{
					// Add wifi network ;

					showDialog(DIALOG_ADD_WIFI_NETWORK);
				}
				else
				{
					NameAndSecurity ns = ((NameAndSecurity) parent
							.getItemAtPosition(position));

					String ssid = ns.name; /* no quote */
					String bssid = ns.BSSID;
					WifiManager w = (WifiManager) ConnectToNetworkActivity.this
							.getSystemService(Context.WIFI_SERVICE);
					ConnectivityManager cm = (ConnectivityManager) ConnectToNetworkActivity.this
							.getSystemService(Context.CONNECTIVITY_SERVICE);

					/*************
					 * What if we are already connected to the chosen network
					 * ??? DBG -- comment for dbg only
					 */
					// / if we need to force user to enter wifi pass -- skipp
					// this check
					if (!shouldForceEnterWifiPass
							&& (w.getWifiState() == WifiManager.WIFI_STATE_ENABLED)
							&& (w.getConnectionInfo() != null)
							&& (w.getConnectionInfo().getSSID() != null)
							&& (w.getConnectionInfo().getSSID().equals(ssid)))
					{
						Log.d("mbp", "We are connecting to it, state:  "
								+ w.getConnectionInfo().getSupplicantState());
						if ((w.getConnectionInfo().getSupplicantState() == SupplicantState.COMPLETED))
						{
							ConnectToNetworkActivity.this.setResult(RESULT_OK);
							ConnectToNetworkActivity.this.finish();
							return;
						}
					}

					List<WifiConfiguration> wcs = w.getConfiguredNetworks();
					String checkSSID = '\"' + ssid + '\"';

					boolean foundExisting = false;

					for (WifiConfiguration wc : wcs)
					{
						if ((wc == null) || (wc.SSID == null))
							continue;
						if (wc.SSID.equals(checkSSID))
						{

							if (!shouldForceEnterWifiPass
									&& (wc.allowedKeyManagement.equals(ns
											.getKeyManagement())))
							{
								newConf_needs_key = wc;
								// USEful when use to debug - network security
								// connection
								// enable this and use android system to connect
								// and read back the configuration
								// Log.d("mbp"," 1 Found wc:"+ wc.networkId+
								// " : " + wc.SSID +
								// " algo: " + wc.allowedAuthAlgorithms +
								// " key: " + wc.allowedKeyManagement +
								// " grp:" + wc.allowedGroupCiphers +
								// " pair: " + wc.allowedPairwiseCiphers +
								// " proto:" + wc.allowedProtocols);
								foundExisting = true;
							}
							else
							{

								w.removeNetwork(wc.networkId);
							}

						}
					}

					should_save_wifi_data = false;

					if (!foundExisting)
					{

						WifiConfiguration newWC = new WifiConfiguration();
						newWC.hiddenSSID = false;
						newWC.SSID = checkSSID;
						newWC.status = WifiConfiguration.Status.ENABLED;
						newWC.hiddenSSID = false;
						newWC.allowedAuthAlgorithms = ns.getAuthAlgorithm();
						newWC.allowedGroupCiphers = ns.getGroupCiphers();
						newWC.allowedKeyManagement = ns.getKeyManagement();
						newWC.allowedPairwiseCiphers = ns.getPairWiseCiphers();
						newWC.allowedProtocols = ns.getProtocols();

						Log.d("mbp", " allowdAuth: "
								+ newWC.allowedAuthAlgorithms + " grpcipher:"
								+ newWC.allowedGroupCiphers + " keymng: "
								+ newWC.allowedKeyManagement + " pairwise:"
								+ newWC.allowedPairwiseCiphers + " proto: "
								+ newWC.allowedProtocols);

						/*
						 * Dont save here, we will save in start_connect() Can't
						 * save here because this may need a key
						 */
						newConf_needs_key = newWC;

						ConnectToNetworkActivity.this.selected_SSID = checkSSID;
						/*
						 * Popup a dialog asking for key if sec is WEP or WPA
						 * Else go direct to Configure Camera
						 */
						current_wep_auth_mode = null;

						should_save_wifi_data = true;

						if (ns.security.equals("WEP"))
						{
							current_security_type = "WEP";
							ConnectToNetworkActivity.this
									.showDialog(ASK_WEP_KEY_DIALOG);

						}
						else if (ns.security.startsWith("WPA"))
						{

							current_security_type = "WPA";
							ConnectToNetworkActivity.this
									.showDialog(ASK_WPA_KEY_DIALOG);
						}
						else
						// Should be OPEN Sec
						{
							current_security_type = "OPEN";
							// Directly start configure
							start_connect();
						}

					}
					else
					{

						/*
						 * if wificonfiguration is already in the list, don't
						 * need to ask for the key again
						 */
						start_connect();
						return;
					}

				}
			}
		});

		return;

	}

	private ConnectToNetwork connect_task;

	private void start_connect()
	{

		synchronized (this)
		{
			if (connect_task != null)
			{
				Log.d("mbp", "Someone else has started .. return ");

				return;
			}
		}

		// TEST 3 cases: WPA, WEP, OPEN
		connect_task = new ConnectToNetwork(ConnectToNetworkActivity.this,
				new Handler(ConnectToNetworkActivity.this));
		connect_task.setIgnoreBSSID(true);

		WifiManager w = (WifiManager) ConnectToNetworkActivity.this
				.getSystemService(Context.WIFI_SERVICE);
		List<WifiConfiguration> wcs;
		wcs = w.getConfiguredNetworks();

		boolean isFound = false;

		for (WifiConfiguration wc : wcs)
		{
			if ((wc.SSID != null) && wc.SSID.equals(newConf_needs_key.SSID))
			{
				connect_task.execute(wc);
				isFound = true;
				break;
			}
		}

		if (isFound == false)
		{
			int res = w.addNetwork(newConf_needs_key);
			Log.d("mbp", this.getClass().getName() + " add new ssid:"
					+ newConf_needs_key.SSID + " id:" + res + "new bssid:"
					+ newConf_needs_key.BSSID);

			// DONT REMOVE THIS
			Log.d("mbp", " save: " + w.saveConfiguration());
			connect_task.execute(newConf_needs_key);
		}

	}

	@Override
	public boolean handleMessage(Message msg)
	{
		switch (msg.what)
		{
		case ConnectToNetwork.MSG_CONNECT_TO_NW_DONE:

			store_default_wifi_info();

			this.setResult(RESULT_OK);
			this.finish();
			break;
		case ConnectToNetwork.MSG_CONNECT_TO_NW_FAILED:
			try
			{
				this.showDialog(CONNECTION_FAILED_DIALOG);
			}
			catch (Exception e)
			{

			}
			break;
		default:

			break;
		}

		// reset it here
		synchronized (this)
		{
			connect_task = null;
		}

		return false;
	}

}
