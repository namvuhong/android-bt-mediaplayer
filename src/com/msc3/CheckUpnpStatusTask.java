package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;



import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;



/**
 * @author phung
 * 
 * params[0] - String ip ;
   params[1] - String port;
 */
public class CheckUpnpStatusTask extends AsyncTask<String, String, Integer> {
	
	private Handler mHander;
	public static final int MSG_UPNP_OK = 1;
	public static final int MSG_UPNP_NOT_OK = 0;
	public static final int MSG_UPNP_CANCEL = -1;
	public static final int MSG_UPNP_RUNNING = 2;
	public static final int MSG_UPNP_GAVE_UP = 10;
	public static final int MSG_UPNP_PORT_CLOSED = 11;
	
	public static final int MSG_MANUAL_FWD = 12;
	
	public CheckUpnpStatusTask(  Handler h ) {
		mHander = h;
		
	}

	/**
	 * @param device_ip
	 * @param device_port
	 * @param user
	 * @param pass
	 * @return -1 Error communication 
	 *          MSG_MANUAL_FWD camera is in manual fwd mode 
	 */
	private int check_UPNP_enabled(String device_ip, String device_port, String user, String pass)
	{
		String usr_pass =  String.format("%s:%s", user, pass);
		
		String http_cmd = "http://" + device_ip + ":" + device_port + 
				PublicDefine.HTTP_CMD_PART + PublicDefine.GET_UPNP_PORT; 

		
		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;
		String contentType = null, response= null;
		
		int upnp_status = -1; // Inprogess

		try {
			url = new URL(http_cmd );
			conn = (HttpURLConnection)url.openConnection();

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));


			conn.setConnectTimeout(2000);
			conn.setReadTimeout(2000);

			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream(),4*1024));
			contentType = conn.getContentType();
			/* make sure the return type is text before using readLine */
			if (contentType!= null &&contentType.equalsIgnoreCase("text/plain"))
			{
				response = inputStream.readLine();

				if (response.startsWith(PublicDefine.GET_UPNP_PORT))
				{
					/* check_upnp: */
					String status_str = response.substring(PublicDefine.GET_UPNP_PORT.length()+2);
					upnp_status = Integer.parseInt(status_str);

				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
		}
		catch (IOException e) {
			e.printStackTrace();
			//STOP CHECK if any socket/connect exception 
			upnp_status = -1;
		}

		if (upnp_status != -1 && upnp_status != 0 )
		{
			upnp_status = MSG_MANUAL_FWD;
		}

		return upnp_status;
	}
	
	/**
	 * 
	 * Run the query only once , for now
	 * @param device_ip
	 * @param device_port
	 * @param user
	 * @param pass
	 * @return
	 */
	private int check_UPNP_status(String device_ip, String device_port, String user, String pass)
	{
		
		int upnp_status  = check_UPNP_enabled(device_ip, device_port, user, pass);
		if (upnp_status == MSG_MANUAL_FWD)
		{
			return upnp_status;
		}
		
		String usr_pass =  String.format("%s:%s", user, pass);
		
		String http_cmd = "http://" + device_ip + ":" + device_port + 
				PublicDefine.HTTP_CMD_PART + PublicDefine.CHECK_UPNP; 

		
		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;
		String contentType = null, response= null;
		
		upnp_status = 2; // Inprogess
		
		do 
		{

			try {
				url = new URL(http_cmd );
				conn = (HttpURLConnection)url.openConnection();

				conn.addRequestProperty("Authorization", "Basic " +
				        Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				
				
				conn.setConnectTimeout(2000);
				conn.setReadTimeout(2000);

				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream(),4*1024));
				contentType = conn.getContentType();
				/* make sure the return type is text before using readLine */
				if (contentType!= null &&contentType.equalsIgnoreCase("text/plain"))
				{
					response = inputStream.readLine();

					Log.d("mbp","check upnp response: " + response);

					if (response.startsWith(PublicDefine.CHECK_UPNP))
					{
						/* check_upnp: */
						String status_str = response.substring(PublicDefine.CHECK_UPNP.length()+2);
						upnp_status = Integer.parseInt(status_str);

					}
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (SocketTimeoutException se)
			{
				//Connection Timeout - Server unreachable ??? 
			}
			catch (IOException e) {
				e.printStackTrace();
				//STOP CHECK if any socket/connect exception 
				upnp_status = -1;
				break;
			}

			
			if (this.isCancelled())
			{
				//Terminate 
				upnp_status = -1;
				break;
			}
			
			
			/*try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			if (this.isCancelled())
			{
				//Terminate 
				upnp_status = -1;
				break;
			}
			

		} while (false) ; //orig: (upnp_status == MSG_UPNP_RUNNING);
		
		
		return upnp_status;
	}
	
	/**
	 * @param device_ip
	 * @param device_port
	 * @param user
	 * @param pass
	 * @return
  		upnp_status = 0 : will need to check back after some time using check_upnp 
                    = 1 : tried too many times and gave up
					
	 */
	private int reset_UPNP(String device_ip, String device_port, String user, String pass)
	{
		String usr_pass =  String.format("%s:%s", user, pass);
		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;
		String contentType = null, response= null;
		
		int upnp_status = 2; // Inprogess
		
		String http_cmd = "http://" + device_ip + ":" + device_port + 
				PublicDefine.HTTP_CMD_PART + PublicDefine.RESET_UPNP; 
		try {
			url = new URL(http_cmd );
			conn = (HttpURLConnection)url.openConnection();

			conn.addRequestProperty("Authorization", "Basic " +
			        Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			
			
			conn.setConnectTimeout(2000);
			conn.setReadTimeout(2000);

			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream(),4*1024));
			contentType = conn.getContentType();
			/* make sure the return type is text before using readLine */
			if (contentType!= null &&contentType.equalsIgnoreCase("text/plain"))
			{
				response = inputStream.readLine();

				Log.d("mbp","reset upnp response: " + response);

				if (response.startsWith(PublicDefine.RESET_UPNP))
				{
					/* check_upnp: */
					String status_str = response.substring(PublicDefine.RESET_UPNP.length()+2);
					upnp_status = Integer.parseInt(status_str);
					
					
				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		
		return upnp_status;
	}
	
	
	
	@Override
	protected Integer doInBackground(String... params) {

		String device_ip = params[0];
		String device_port = params[1];
		String user = params[2];
		String pass = params[3];
		

		if (user == null) user = "";
		if (pass == null) pass = "";
		
		int upnp_status = 2; // Inprogess
		upnp_status = check_UPNP_status(device_ip, device_port,user, pass );
		
		if (this.isCancelled())
		{
			//Terminate 
			upnp_status = -1;
		}
		
		/* 
		 * TODO: enable and TEST
		  upnp_status = 1 or 0 or -999 here 
		*/
		if (upnp_status == -1 || upnp_status == MSG_UPNP_OK || upnp_status == MSG_UPNP_NOT_OK ||
				upnp_status == MSG_UPNP_RUNNING || upnp_status == MSG_MANUAL_FWD )
		{
			return new Integer(upnp_status);
		}
		else if (upnp_status == -999) //PORT CLOSE 
		{  
			Log.d("mbp","PORT CLOSE-- try resetting ");
			upnp_status = reset_UPNP(device_ip, device_port,user, pass );
			if ( upnp_status == 0) //Try again later
			{
				Log.d("mbp","reset return 0 -- check again later ");
				/*try {
						Thread.sleep(10*60*1000);
					} catch (InterruptedException e) {
					}*/

				upnp_status = MSG_UPNP_RUNNING;
			}
			else if ( upnp_status == 1 )// Gave up 
			{
				Log.d("mbp","reset return 1 - gave up ");
				upnp_status = MSG_UPNP_GAVE_UP;
			}
			
		}
		
		if (this.isCancelled())
		{
			//Terminate 
			upnp_status = -1;
		}
		
		
		return new Integer(upnp_status);
	}

	/* UI thread */
	protected void onPostExecute(Integer result)
	{
		/* 
		 * If we are cancelled, silence -- 
		 * else if upnp is not OK -->  popup 
		 */
		// if we are cancelled --- We WONT BE HERE .. onPostExec() wont be called
		if (this.isCancelled() || result.intValue() == -1)
		{
			//Dont do anything here
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_CANCEL));
		}
		else if (result.intValue() == 1) //OK
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_OK));
		}
		else  if (result.intValue() == MSG_UPNP_NOT_OK)  
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_NOT_OK));
		}
		else  if (result.intValue() == MSG_UPNP_RUNNING)  
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_RUNNING));
		}
		else if (result.intValue() == MSG_UPNP_GAVE_UP)
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_GAVE_UP));
		}
		else if (result.intValue() == MSG_MANUAL_FWD)
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_MANUAL_FWD));
		}
		else if (result.intValue() == -999)
		{
			mHander.sendMessage(Message.obtain(mHander, MSG_UPNP_PORT_CLOSED));
		}
		
	}
}




