package com.msc3;

import com.blinkhd.R;
import java.io.IOException;
import java.net.InetAddress;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.util.Log;

public class CameraDetectorService extends Service 
{


	private CameraConnectivityDetector mCamDetector;
	private Thread  mCamDetectorThread;


	public CameraDetectorService() {
		super();

	}
	public CameraDetectorService(String name) {
		super();
	}


	@Override
	public void onCreate() {
		super.onCreate();



		String phonemodel = android.os.Build.MODEL;
		if(!phonemodel.equals(PublicDefine.PHONE_MBP2k) && !phonemodel.equals(PublicDefine.PHONE_MBP1k)) // if it's not iHome Phone
		{	
			//IGNORE local vox 
			Log.d("mbp","NOT iHome - Dont start this service");
			return;
		}


		

		mCamDetector = new CameraConnectivityDetector(this);
		mCamDetectorThread = new Thread(mCamDetector);
		mCamDetectorThread.start();

	}



	@Override
	public void onDestroy() {
		super.onDestroy();


		boolean retry = true;
		if (mCamDetectorThread != null)
		{
			mCamDetector.stop();
			mCamDetectorThread.interrupt();
			while (retry)
			{
				try {
					mCamDetectorThread.join(5000);
					retry = false;
				} catch (InterruptedException e) {
				}
			}
			mCamDetectorThread = null;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (mCamDetectorThread!= null && !mCamDetectorThread.isAlive())
		{
			mCamDetectorThread.start();
		}
		else
		{
		}

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}




	/**
	 * Used by wifi out of range broadcast receiver
	 * @param message
	 */
	public void noficationMessage(String message)
	{

		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

		int icon = R.drawable.cam_icon;
		//CharSequence tickerText = getResources().getString(R.string.vox_service_enabled);
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, message, when);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_ALL;


		Context context = getApplicationContext();      // application Context
		CharSequence contentTitle = getResources().getString(R.string.app_name);
		//CharSequence contentText = getResources().getString(R.string.vox_service_enabled);
		String contentText = message; //"Low Battery";
		Intent intent = new Intent();


		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

		mNotificationManager.notify(11111, notification);


	}

	/**
	 * FROM GcmIntentService.java
	 */
	private Notification build_notification_with_sound(Context context, VoxMessage newVox, Notification old_notif)
	{

		int icon = R.drawable.cam_icon;
		long when =newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name(); 
		Notification notification = null;

		//setup pending intent when being clicked
		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,newVox.getTrigger_mac());
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent =
				PendingIntent.getActivity(context, 0, notificationIntent, 0); 


		//setup pending intent when being CLEARED
		Intent deleteIntent = new Intent(context, ClearNotificationActivity.class);
		// set intent so it does not start a new activity
		deleteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
				Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent deletePIntent =
				PendingIntent.getActivity(context, 0, deleteIntent, 0); 

		if (old_notif == null)
		{
			//This is the first message
			//Log.d("mbp", "This is the Notification message");

			notification = new Notification(icon, title, when);


			notification.setLatestEventInfo(context, title, newVox.toString(), intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;


		}
		else //just update the content of the notification 
		{

			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;

		}

		notification.deleteIntent = deletePIntent;

		return notification; 
	}

	/**
	 * 
	 * Build notification WITHOUT sound, to avoid too many clustered sound alarm. 
	 * @param context
	 * @param newVox
	 * @param old_notif
	 * @return
	 */
	private Notification build_notification(Context context, VoxMessage newVox, Notification old_notif)
	{

		int icon = R.drawable.cam_icon;
		long when =newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name(); 
		Notification notification = null;

		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,newVox.getTrigger_mac());
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
				Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent =
				PendingIntent.getActivity(context, 0, notificationIntent, 0); 


		if (old_notif == null)
		{
			//This is the first message
			//Log.d("mbp", "This is the Notification message");

			notification = new Notification(icon, title, when);


			notification.setLatestEventInfo(context, title, newVox.toString(), intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE ;

		}
		else //just update the content of the notification 
		{

			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE ;

		}
		return notification; 
	}

	/* 20120118: copied from ScanForCamera.java */
	private InetAddress broadcast, gateWay;
	private void  getAddresses() throws IOException {
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow


		int broadcast_addr = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;


		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast_addr >> k * 8) & 0xFF);

		broadcast =  InetAddress.getByAddress(quads);


		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((dhcp.gateway >> k * 8) & 0xFF);

		gateWay = InetAddress.getByAddress(quads);


		return;
	}
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

}
