package com.msc3;

import com.blinkhd.MBPActivity;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.blinkhd.R;

public class AVShadowStreamerController {
	
	private Messenger avsService; 
	final Messenger avsMessenger = new Messenger(new AVSIncomingHandler());
	
	
	private boolean avsIsBound; 
	private Context mContext;
	private Streamer videoStreamer;
	
	private String cameraName; 
	
	public AVShadowStreamerController(Context context, Streamer _streamer)
	{
		avsIsBound = false;
		mContext = context; 
		this.videoStreamer = _streamer;
		
	}
	
	
	
	public Streamer getVideoStreamer() {
		return videoStreamer;
	}



	public void setVideoStreamer(VideoStreamer videoStreamer) {
		this.videoStreamer = videoStreamer;
	}

	public void setEnableStatusBarIcon(boolean enable, String nameShowned)
	{
		if (enable)
		{
			showSOL(nameShowned);
		}
		else
		{
			hideSOL();
		}
		
		
	}
	
	
	private void showSOL(String cameraName)
	{
		this.cameraName  = cameraName; 
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
		
		int icon = R.drawable.mini_app_icon;
		CharSequence tickerText = mContext.getResources().getString(R.string.app_name);
		long when = System.currentTimeMillis();
		
		if (cameraName == null)
		{
			cameraName = "Camera"; //getResources().getString(R.string.app_name);
		}
		
		Notification notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_NO_CLEAR ;
		notification.flags |= Notification.FLAG_ONGOING_EVENT ;
		
		Context context = mContext.getApplicationContext();      // application Context
		CharSequence contentTitle = cameraName;
		CharSequence contentText = mContext.getResources().getString(R.string.vox_service_enabled);
		
		Intent intent = new Intent(mContext, MBPActivity.class);
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, intent, 0);

		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		
		mNotificationManager.notify(100, notification);
		
	}
	
	private void hideSOL()
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(ns);
		
		mNotificationManager.cancel(100);
	}
	

	private static boolean isAVSServiceRunning(Context c) {
		ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) 
		{
			if ("com.msc3.AVShadowStreamerService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	

	private void doAVSBindService() {
		if(avsIsBound==false)
		{
			boolean res = mContext.bindService(new Intent(mContext, AVShadowStreamerService.class), mAVSConnection, Context.BIND_AUTO_CREATE);
			
			avsIsBound = true;
		}
	}
	private void doAVSUnbindService() {
		if (avsIsBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (avsService != null) {
				try {
					Message msg = Message.obtain(null, AVShadowStreamerService.AVS_MSG_UNREGISTER_CLIENT);
					msg.replyTo = avsMessenger;
					avsService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			mContext.unbindService(mAVSConnection);
			avsIsBound = false;
		}
	}
	
	public void stopAVSService()
	{
		if (isAVSServiceRunning(mContext))
		{
			doAVSUnbindService();
			
			Intent i = new Intent(mContext,AVShadowStreamerService.class);
			mContext.stopService(i);
			
			//release this.videoStreamer
			this.videoStreamer = null;
		}
		
		hideSOL();
	}
	/**
	 * Start AVS service if it is not started before. 
	 *  if it is running, restart & rebind to it.
	 * 
	 */
	public void startAVSService()
	{
		if (!isAVSServiceRunning(mContext))
		{
			Intent i = new Intent(mContext,AVShadowStreamerService.class);
			mContext.startService(i);
		}
		doAVSBindService();
	}
	
	/**
	 * Called to update the streamer update 
	 *  Usecase: reconnect after lost link
	 *  
	 *  Use this function so as to keep the icon on the status bar
	 *  (if use stop & start service , the icon on status bar will be gone)
	 * 
	 * @param streamer
	 */
	public void rebindAVSService(Streamer streamer)
	{
		
		if (!isAVSServiceRunning(mContext))
		{
			startAVSService();
		}
		else
		{
			Log.d("mbp", "Re-bind AVS service"); 
			
			this.videoStreamer = streamer;
			
			/*Simply unbind (to kill the service and the pcm player thread)
			 * And  bind (to start new service & the pcm player thread) 
			 */
			doAVSUnbindService();
			doAVSBindService();
		}
	}
	
	
	
	
	private ServiceConnection mAVSConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {
			avsService = new Messenger(service);
			try {
				Message msg = Message.obtain(null, AVShadowStreamerService.AVS_MSG_REGISTER_CLIENT);
				/* pass a messenger to service using this field 
				 * - later service will be able to send sth back to this Activity using this messenger*/
				msg.replyTo = avsMessenger;
				
				/* attach the video streamer obj here */
				msg.obj = videoStreamer;
				
				avsService.send(msg);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			avsService = null;
		}
	};
	
	private class AVSIncomingHandler extends Handler {
		public void handleMessage(Message msg) {
		//Do nothing here
		}
	}
}
