

package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.registration.SingleCamConfigureActivity;
import com.msc3.registration.UserAccount;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.IsAvailableResponse;




import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;

import com.blinkhd.R;
import com.discovery.LocalScanForCameras;
import com.discovery.ScanProfile;

public class AutoConfigureCameras extends AsyncTask<CamConfiguration, String, String> implements ICameraScanner {


	private static final int DEFAULT_SEARCHING_FOR_NEW_NETWORK = 20000;//sec
	private static final int DEFAULT_WAITING_FOR_CONNECTION = 30000;//sec

	public static final int MSG_AUTO_CONF_SUCCESS = 0x1111;
	public static final int MSG_AUTO_CONF_FAILED = 0x2222;

	public static final int START_SCAN_FAILED = 0x2225;
	public static final int CONNECT_TO_CAMERA_FAILED = 0x2226;
	public static final int SEND_DATA_TO_CAMERA_FAILED = 0x2227;
	public static final int CONNECT_TO_HOME_WIFI_FAILED = 0x2228;
	public static final int SCAN_CAMERA_FAILED = 0x2229;
	public static final int CAMERA_DOES_NOT_HAVE_SSID = 0x2230;
	public static final int USB_STORAGE_TURNED_ON = 0x2231;

	private int errorCode;

	//use when does not find ssid in wifi list
	private boolean continueAddCamera;
	private boolean isWaiting;

	private Context mContext; 
	private Handler mHandler;
	private Object wifiLockObject;

	private Object supplicantLockObject;
	private SupplicantState sstate;


	private NetworkInfo mWifiNetworkInfo;
	private int mWifiState;

	//private ProgressDialog dialog;

	private String home_ssid, home_key;
	private String security_type ; 
	private String wep_auth_mode,wep_key_index;
	private String http_usr, http_pass; 

	private WifiConfiguration conf;
	private LocalScanForCameras scan_task; 

	private boolean isCancelable;

//	private Tracker tracker;
//	private EasyTracker easyTracker;

	private CamProfile cam_profile = null;

	public AutoConfigureCameras(Context mContext, Handler h)
	{
		this.mContext = mContext;

		wifiLockObject = new Object();
		supplicantLockObject = new Object();
		sstate = null;
		mWifiNetworkInfo = null;
		mWifiState = -1;
		wep_auth_mode = wep_key_index = null;
		conf = null;
		mHandler = h;
		scan_task = null; 

//		easyTracker = EasyTracker.getInstance();
//		easyTracker.setContext(mContext);
//		tracker = easyTracker.getTracker();
	}



	/* 
	 * (non-Javadoc)
	 * @Param[]: - SSID of cameras in Setup-mode
	 *  		 - Max_num_of_Cam (int string)
	 *           - SSID of Network to be configured 
	 *           - Key  of network to be configured
	 *           - Security type (wpa or wep)
	 *           - WEP method (null if not valid)
	 *           - WEP key (null if not valid)
	 *          
	 *  Description:
	 *  		 - Disconnect fr Original Network
	 *           Search for "MotAndroid" ssid
	 *           Try to connect 
	 *           send over the configuration using config_write
	 *           send over the restart cmd 
	 *           - expect irabot reboot - 
	 *           Repeat until we have found Max_num_of_Cam (4) 
	 *           Or when we hit a time out - 
	 *           - Reconnect back to Original Network
	 */

	protected String doInBackground(CamConfiguration... arg0) {

		isCancelable = false;
		String saved_ssid = arg0[0].device_ssid();
		int max_cams = Integer.parseInt(arg0[0].max_cam()) ;
		Vector<String> failed_devices = arg0[0].getSkippedDeviceList();
		home_ssid = arg0[0].ssid();
		home_key = arg0[0].pass_string();
		security_type = arg0[0].security_type();
		wep_auth_mode = arg0[0].auth_method();
		wep_key_index =  arg0[0].key_index();
		conf = arg0[0].wc();
		cam_profile = arg0[0].getCamProfiles()[0];

		http_usr = arg0[0].getHttpUsr();
		http_pass =arg0[0].getHttpPass();

		String output = "Done";

		WifiReceiver1 br = null;
		List<WifiConfiguration> wcs;
		long start_restart_system = 0;

		IntentFilter i = new IntentFilter();
		br = new WifiReceiver1();

		i.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
		i.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		i.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);




		WifiManager w = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);

		publishProgress(mContext.getString(R.string.AutoConf_5));

		//publishProgress(mContext.getString(R.string.AutoConf_scanning_for_)+setup_ssid + " ...");

		//remove the old configuration if any
		Log.d("mbp", "Removing all Camera-... entries"  );
		wcs = w.getConfiguredNetworks();
		for(WifiConfiguration wc : wcs) {
			if ( (wc == null)|| (wc.SSID == null))
			{

				Log.d("mbp", " Stuck??  size is: " +wcs.size() +
						" wc == null? " + ( wc== null) );

				if (wc != null)
				{
					Log.d("mbp", " Stuck??  wc.ssid == null");
				}
				continue;
			}

			if(wc.SSID.startsWith("\"" + PublicDefine.DEFAULT_SSID) ||
					wc.SSID.startsWith("\"" + PublicDefine.DEFAULT_SSID_HD))
			{
				Log.d("mbp", "remove id: " + wc.networkId + " ssid:" + wc.SSID
						);
				w.removeNetwork(wc.networkId);
			}
		}
		Log.d("mbp", " DONT save configuration.."  );
		//w.saveConfiguration();


		if (w.startScan() == true)
		{


			mContext.registerReceiver(br,i);
			//Scanning started 
			synchronized(this)
			{
				try {
					this.wait(DEFAULT_SEARCHING_FOR_NEW_NETWORK);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			//xxx;
			mContext.unregisterReceiver(br);


			// now we can read the result back 
			List<ScanResult> list = w.getScanResults();

			/* list of network id to configure after adding */
			int [] networkIds = {-1,-1,-1,-1};
			int networkIds_idx = 0;
			//mWifiNetworkInfo= null;
			for (ScanResult scanned_nw :list)
			{

				if (scanned_nw.SSID == null)
				{
					Log.d("mbp", "Scanned SSID = null!"  );
					continue;
				}
				if (scanned_nw.SSID.startsWith(PublicDefine.DEFAULT_SSID) ||
						scanned_nw.SSID.startsWith(PublicDefine.DEFAULT_SSID_HD))
				{
					Log.d("mbp", "scanned ssid: " + scanned_nw.SSID);
					NameAndSecurity _ns = new NameAndSecurity(scanned_nw.SSID,scanned_nw.capabilities,scanned_nw.BSSID);
					_ns.setShowSecurity(false);
					_ns.setHideMac(true);
					String bssid = arg0[0].getDeviceBSSIDList().elementAt(0);
					Log.d("mbp", "scanned_nw.SSID: " + scanned_nw.SSID +
							", saved_ssid: " + saved_ssid);
					if (scanned_nw.SSID.equalsIgnoreCase(saved_ssid))
					{
						//found 1 camera in range, add new configuration
						WifiConfiguration wc = new WifiConfiguration();
						wc.SSID = convertToQuotedString(scanned_nw.SSID);
						saved_ssid = convertToQuotedString(scanned_nw.SSID);
						wc.BSSID = scanned_nw.BSSID;
						wc.hiddenSSID = false; 
						wc.allowedAuthAlgorithms = _ns.getAuthAlgorithm();
						wc.allowedKeyManagement = _ns.getKeyManagement();
						wc.status = WifiConfiguration.Status.ENABLED;
						if (_ns.security.equalsIgnoreCase("WPA"))
						{
							wc.preSharedKey = convertToQuotedString(
									PublicDefine.DEFAULT_WPA_PRESHAREDKEY);
						}
						wc.allowedGroupCiphers = _ns.getGroupCiphers();
						wc.allowedPairwiseCiphers= _ns.getPairWiseCiphers();
						wc.allowedProtocols = _ns.getProtocols();

						Log.d("mbp","add NEW wc:"+ wc.networkId+ " : " + wc.SSID +
								" algo: " + wc.allowedAuthAlgorithms + 
								" key: " + wc.allowedKeyManagement +
								" grp:" + wc.allowedGroupCiphers +
								" pair: " + wc.allowedPairwiseCiphers +
								" proto:" + wc.allowedProtocols + 
								" hidden: " + wc.hiddenSSID +
								" preSharedKey: " + wc.preSharedKey);


						int netid = w.addNetwork(wc);
						Log.d("mbp", "added camera nw with id: " + netid);
						if (networkIds_idx < networkIds.length )
						{
							networkIds_idx++;
						}
					}



				} //if  (scanned_nw.SSID.startsWith(setup_ssid))

			}// for (ScanResult scanned_nw :list)

			Log.d("mbp", "save configuration..2nd"  );

			w.saveConfiguration();
			//No setup network being added 		
			if ( networkIds_idx == 0 )
			{
				Log.d("mbp", "no setup_ssid found: " +PublicDefine.DEFAULT_SSID);
				publishProgress(String.format(mContext.getResources()
						.getString(R.string.AutoConf_can_t_find_1), PublicDefine.DEFAULT_SSID));
				output = "Failed"; 
			}
			else // 
			{

				/* Scan the list of Configured N/W to update the newtork id array 
				 * reason being... below..
				 */
				networkIds_idx = 0;
				/* Note: It is possible for this method to change the network 
				 * IDs of existing networks. You should assume the network IDs can 
				 * be different after calling this method.
				 */
				boolean connecting = false; 


				String regId = arg0[0].getDeviceBSSIDList().elementAt(0);
				wcs = w.getConfiguredNetworks();

				Log.d("mbp","Find regId in phone wifi list: " +  regId);
				for (WifiConfiguration wc: wcs)
				{
					if (wc.SSID != null && wc.SSID.equals(saved_ssid) )
					{
						//start connecting.. now
						Log.d("mbp", "start connect to nw with id: " + wc.networkId);
						w.enableNetwork(wc.networkId, true);
						connecting = true; 
						break;


					}
				}




				if (connecting == true)
				{


					/* We have finished adding all wificonfiguration of devices 
					 * Now for each of the network ID, try to connect and send the configuration
					 * data over
					 * 
					 */



					boolean retry_waiting = true;
					output ="Done";

					do
					{

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}

						mContext.registerReceiver(br,i);

						//how to know when Iam connected ?
						synchronized(wifiLockObject)
						{
							try {
								wifiLockObject.wait(DEFAULT_WAITING_FOR_CONNECTION);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}

						mContext.unregisterReceiver(br);

						int max_try = 30;
						int retries = 0;
						do 
						{				
							if (  (w.getConnectionInfo().getIpAddress() != 0 )&& 
									(w.getDhcpInfo().ipAddress != 0) )
							{
								//We're connected but don't have any IP yet
								Log.d("mbp", "IP:"+ ((w.getDhcpInfo().ipAddress&0xFF000000)>>24) + "."+
										((w.getDhcpInfo().ipAddress&0x00FF0000)>>16) + "."+
										((w.getDhcpInfo().ipAddress&0x0000FF00)>>8) + "."+
										(w.getDhcpInfo().ipAddress&0x000000FF)  );

								Log.d("mbp", "SV:"+ ((w.getDhcpInfo().serverAddress&0xFF000000)>>24) + "."+
										((w.getDhcpInfo().serverAddress&0x00FF0000)>>16) + "."+
										((w.getDhcpInfo().serverAddress&0x0000FF00)>>8) + "."+
										(w.getDhcpInfo().serverAddress&0x000000FF)  );
								break;
							}
							Log.d("mbp", "connected to camera but don't have any IP yet...");
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						while(retries++ < max_try);


						Log.d("mbp","Current ssid: " +  w.getConnectionInfo().getSSID()  + 
								" setup_ssid:" + saved_ssid +
								" setup_regId: " + regId);

						ConnectivityManager conn = (ConnectivityManager) 
								mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
						NetworkInfo wifiInfo = conn.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
						if (wifiInfo == null) //sth is wrong 
						{
							publishProgress(String.format(mContext.getResources()
									.getString(R.string.AutoConf_2),saved_ssid ));
						}
						else
						{
							String cameraRegId = null;
							//try connect 10s
							String gatewayIp = SingleCamConfigureActivity.getGatewayIp(mContext);
							long try_connect_time = System.currentTimeMillis() + 20000;
							do
							{
								try {
									cameraRegId = HTTPRequestSendRecv
											.getUdid(
													gatewayIp,
													PublicDefine.DEFAULT_DEVICE_PORT,
													PublicDefine.DEFAULT_BASIC_AUTH_USR,
													PublicDefine.DEFAULT_CAM_PWD);
									break;

								} catch (ConnectException e1) {
									e1.printStackTrace();
								}
							} while (System.currentTimeMillis() < try_connect_time);

							Log.d("mbp", "get mac result: " + cameraRegId);
							Log.d("mbp", "camera bssid: " + regId);

							if ( 
									/*
									(w.getConnectionInfo().getBSSID() != null)  &&
									(w.getConnectionInfo().getBSSID().equalsIgnoreCase(bssid))
									 */
									cameraRegId != null && cameraRegId.equalsIgnoreCase(regId)
									)
							{

								if (  (w.getConnectionInfo().getIpAddress() == 0 )|| 
										(w.getDhcpInfo().ipAddress == 0) )
								{
									//Dont increase j repeat this step once
									if ( retry_waiting == true)
									{
										retry_waiting = false;//reset it here 
										continue;
									}
								}

								publishProgress(mContext.getString(R.string.AutoConf_5));
								//publishProgress(String.format(mContext.getString(R.string.AutoConf_3),setup_ssid ));

								//TODO
								/* 20130228: hoang: issue 1307
								 * skip warning message when hidden SSID
								 */
								if (!conf.hiddenSSID)
								{
									// CHECK if the HOME wifi is seen by the camera
									QueryCameraWifiListTask getWifiList = new QueryCameraWifiListTask(
											this.mContext, cam_profile.getFirmwareVersion()); 
									getWifiList.executeOnExecutor(THREAD_POOL_EXECUTOR, "dummy");
									ArrayList<CameraWifiEntry> wifiList = null; 

									try {
										wifiList = (ArrayList<CameraWifiEntry>) 
												getWifiList.get(30000, TimeUnit.MILLISECONDS);
									} catch (ExecutionException e) {
										e.printStackTrace();
									} catch (InterruptedException e) {
										e.printStackTrace();
									} catch (TimeoutException e) {
										e.printStackTrace();
									}

									boolean foundSsidInCamera = false; 
									if (wifiList != null)
									{

										if (wifiList.isEmpty())
										{
											Log.d("mbp", "Wifi list is empty");
										}

										//Search for the home ssid in the camera wifi list
										CameraWifiEntry entry = null; 
										Iterator<CameraWifiEntry> ic = wifiList.iterator();
										while (ic.hasNext())
										{
											entry = ic.next(); 
											if (entry.getSsidNoQoute().equals(home_ssid))
											{
												foundSsidInCamera = true; 
												break; 
											}
										}
										if (!foundSsidInCamera)
										{
											Log.d("mbp", "Camera does no contain the SSID " + home_ssid);

											isWaiting = true;
											continueAddCamera = true;
											/* 20130110: hoang: show alert and wait for user select whether continue add camera or not
											 */
											publishProgress(mContext.getString(R.string.camera_does_not_have_ssid_warning));
											while (isWaiting == true)
											{
												try {
													Thread.sleep(500);
												} catch (InterruptedException e) {
													e.printStackTrace();
												}
											}

											if (continueAddCamera == false)
											{
												errorCode = CAMERA_DOES_NOT_HAVE_SSID; 
												output ="Failed";
												break;
											}
										}
										else
										{
											Log.d("mbp", "Found ssid in camra .. continue");
										}


									}
									else 
									{
										// NULL wifilist.. wifi list is timeout
										Log.d("mbp", "get wifi list timeout");
									}
								}
								else
								{
									Log.d("mbp", "Hidden SSID - Not need to query wifi list from camera.");
								}

								// retry up to 5 times
								int k = 0;
								String response; 
								String req = "http://" + gatewayIp + ":80" +
										arg0[0].build_setup_request();

								String device_address_port = gatewayIp + ":" + 80;
								while (k++ < 5   )
								{

									String set_mkey_cmd = String.format("%1$s%2$s%3$s%4$s%5$s%6$s", "http://",
											device_address_port, PublicDefine.HTTP_CMD_PART, 
											PublicDefine.SET_MASTER_KEY, PublicDefine.SET_MASTER_KEY_PARAM_1,
											arg0[0].getMasterKey());
									response = send_request(set_mkey_cmd, 20000);
									Log.d("mbp", "Mkey response: " + response); 
									//set_master_key: 0 
									if(!verify_response(PublicDefine.MASTER_KEY_RESPONSE, response))
									{
										//loop back;
										Log.d("mbp", "Failed to send MKEY: " + response + " retry: " + k); 
										output ="Failed";
										errorCode = SEND_DATA_TO_CAMERA_FAILED;
										continue; 
									}

									int offset = TimeZone.getDefault().getRawOffset();
									String timeZone = String.format("%s%02d.%02d", offset >= 0 ? "+" : "-", 
											Math.abs(offset) / 3600000,
											(Math.abs(offset) / 60000) % 60);
									Log.d("mbp", "Send time zone " + timeZone + " to camera");
									String set_tz_cmd = String.format("%1$s%2$s%3$s%4$s%5$s%6$s", "http://",
											device_address_port, PublicDefine.HTTP_CMD_PART, 
											PublicDefine.SET_TIME_ZONE, PublicDefine.SET_TIME_ZONE_PARAM,
											timeZone);
									response = send_request(set_tz_cmd, 20000);
									Log.d("mbp", "Timezone response: " + response);
									if(!verify_response(PublicDefine.SET_TIME_ZONE_RESPONSE, response))
									{
										//ignore response
									}

									Log.d("mbp", "Send wifi config to camera");
									response = send_request(req, 20000);
									Log.d("mbp", "wifi res response: " + response); 
									if(!verify_response(PublicDefine.WIRELESS_SETUP_SAVE_RESPONSE,response))
									{
										Log.d("mbp", "Failed to send WIRELESS info: " + response + " retry: " + k); 
										output ="Failed";
										errorCode = SEND_DATA_TO_CAMERA_FAILED;
									}
									else
									{
										output = "Done";
										break; 
									}


								}



								if ( output.equalsIgnoreCase("Failed"))
								{
									break;
								}

								String restart_cmd = String.format("%1$s%2$s%3$s%4$s", "http://",
										device_address_port, PublicDefine.HTTP_CMD_PART, PublicDefine.RESTART_DEVICE);
								response = send_request(restart_cmd, 10000);
								Log.d("mbp", "restart--->>: " + response); 
								//"restart_system: 0"  / -1

								if (!verify_response(PublicDefine.RESTART_DEVICE_RESPONSE,response))
								{
									response = send_request(restart_cmd, 10000);
								}

								mWifiNetworkInfo = null;
							} /*  wc.BSSID == w.getConnectionInfo().getBSSID() */
							else // ERROR OUT -- cant' connect to camera network. 
							{
								output ="Failed";
								errorCode = CONNECT_TO_CAMERA_FAILED;

								Log.d("mbp", "cant' connect to camera network.");
								break; 
							}

						} // wifiInfo != null

					} 
					while (false);

				} //if connecting == true
				else
				{
					output = "Failed";
					Log.d("mbp", "Failed to start Find camera network in Phone's wifi");
					errorCode = CONNECT_TO_CAMERA_FAILED;
				}

			} // if networkId > 0 


		}
		else
		{
			output ="Failed";
			Log.d("mbp", "Start scan failed");
			errorCode = CONNECT_TO_CAMERA_FAILED;
		}

		publishProgress(mContext.getString(R.string.AutoConf_5));

		//publishProgress(String.format(mContext.getString(R.string.AutoConf_4) , home_ssid));

		// reconnect back to the original network 

		wcs = w.getConfiguredNetworks();
		for(WifiConfiguration wc : wcs) {
			if ( (wc == null)|| (wc.SSID == null))
				continue;

			if(wc.SSID.equalsIgnoreCase(convertToQuotedString(home_ssid))) {
				w.enableNetwork(wc.networkId, true);
				//w.reconnect();
			}
		}


		mContext.registerReceiver(br, i);

		//how to know when Iam connected ?
		synchronized(wifiLockObject)
		{
			try {
				wifiLockObject.wait(DEFAULT_WAITING_FOR_CONNECTION);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (br != null) {
			mContext.unregisterReceiver(br);
		}


		isCancelable = true;
		publishProgress(mContext.getString(R.string.AutoConf_5));


		// scan for this camera here .. 
		if ( output.equals("Done"))
		{

			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e2)
			{
			}

			//InetAddress dummy;
			ScanProfile[]  cps = null;
			String regId; 
			regId = arg0[0].getDeviceBSSIDList().elementAt(0);
			String mac = CamProfile.getMacFromRegId(regId).toUpperCase();
			mac = PublicDefine.add_colon_to_mac(mac);
			boolean foundConfiguredCam = false; 
			long time_to_finish_scanning =  System.currentTimeMillis() + 4*60*1000;
			long time_to_query_online= System.currentTimeMillis() + 60*1000; 
			do
			{
				Log.d("mbp","start scanning for: " + mac);

				//start scanning now.. for one camera only ; 
				scan_task = new LocalScanForCameras(mContext, this);
				scan_task.setShouldGetSnapshot(false);

				// TODO Auto-generated method stub
				//create a dummy profile 
				CamProfile dummy = new CamProfile(null, mac);
				/*Assume that it is in local Becoz We have just finished configuring it
						  Assume that it is 1 mins since last comm
						    ---NEED to set this to trick ScanForCameras to scan otherwise it will skip this camera  
				 */
				dummy.setInLocal(true);
				dummy.setMinutesSinceLastComm(1);
				scan_task.startScan(new CamProfile[] {dummy});

				//wait for result
				cps = null;
				cps = scan_task.getScanResults();

				if (cps != null && cps.length == 1)
				{
					if (mac.equalsIgnoreCase(cps[0].get_MAC()))
					{
						//foundConfiguredCam = true; 
						Log.d("mbp","Found it in local");
						//break; 
					}

				}

				if (isCancelled())
				{
					break; 
				}

				//don't need to wait anymore
				//if ( System.currentTimeMillis() >  time_to_query_online )
				{
					Log.d("mbp","Start query online");
					//reset to the nxt minute
					time_to_query_online = System.currentTimeMillis() + 60*1000;
					SharedPreferences prefs = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
					String user_token = prefs.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
					UserAccount online_user;
					try {
						online_user = new UserAccount(
								user_token,
								mContext.getExternalFilesDir(null), null, mContext );
					} catch (StorageException e1) {
						Log.d("mbp", e1.getLocalizedMessage());
						output = "Failed";
						errorCode = USB_STORAGE_TURNED_ON;
						break;
					}

					boolean isOnline = online_user.isCameraOnline(regId);
					if (isOnline)
					{
						Log.d("mbp","FOUND the camera online??? ");
						foundConfiguredCam = true; 
						Log.d("mbp","Anyway .. Found it");

						/*
						try {
							online_user.sync_user_data();
						} catch (IOException e) {
							e.printStackTrace();
						} //sync data here
						*/ 

						break;

					}

				}


				if (isCancelled())
				{
					break; 
				}

			}
			//Keep scanning up to 2 min 
			while  (System.currentTimeMillis() < time_to_finish_scanning);

			if (!foundConfiguredCam)
			{
				//Sth is wrong 
				Log.d("mbp","Can't find camera -- ");
				output = "Failed";
				errorCode = SCAN_CAMERA_FAILED; 
			}

			scan_task = null; 
		} // if output.equals("Done"); 


		return output;
	}


	@Override
	public void updateScanResult(ScanProfile[] results, int status, int index) {
	}



	/* on UI Thread */
	protected void onPreExecute()
	{
		//		Spanned msg = Html.fromHtml("<big>"+mContext.getString(R.string.AutoConf_working_)+"</big>");
		//		
		//		dialog = new ProgressDialog(mContext);
		//		dialog.setMessage(msg);
		//		dialog.setIndeterminate(true);
		//		dialog.setCancelable(false);
		//		dialog.show();
	}

	/* on UI Thread */
	protected void onProgressUpdate(String... progress) {

		Spanned msg = Html.fromHtml("<big>"+progress[0]+"</big>");


		// when the message box "... this may take from 2 to 5 minutes ...",
		// add Cancel button to allow user to choose to cancel the operation.
		if (isCancelable == true)
		{
			//			if (dialog != null)
			//			{
			//				dialog.dismiss();
			//			}
			//			dialog = new ProgressDialog(mContext);
			//			dialog.setMessage(msg);
			//			dialog.setIndeterminate(true);
			//			dialog.setCancelable(false);
			//			dialog.setButton(mContext.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
			//
			//				@Override
			//				public void onClick(DialogInterface dialog, int which) {
			//
			//					AutoConfigureCameras.this.cancel(true);
			//				}
			//			});
			//
			//			dialog.show();
		}
		else if (progress[0].equalsIgnoreCase(mContext.getResources().getString(R.string.camera_does_not_have_ssid_warning)))
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			msg = Html.fromHtml("<big>"
					+ mContext.getString(R.string.camera_does_not_have_ssid_warning)
					+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(mContext.getResources().getString(R.string.Yes),new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,int which) {
					dialog.dismiss();
					isWaiting = false;
					continueAddCamera = true;
				}
			})
			.setNegativeButton(
					mContext.getResources().getString(R.string.No),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();
							isWaiting = false;
							continueAddCamera = false;
						}
					});

			AlertDialog alert = builder.create();
			alert.show();
		}
		else
		{
			//dialog.setMessage(msg);
		}




	}



	/* on UI Thread */
	protected void onPostExecute(String result) {

		//		if (dialog != null)
		//		{
		//			dialog.dismiss();
		//		}

		if (result.equalsIgnoreCase("Done"))
		{
//			tracker.sendEvent(SingleCamConfigureActivity.GA_ADD_CAMERA_CATEGORY,
//					"Add Cam Success", "Add Cam Success", null);
			//Toast.makeText(mContext,result, Toast.LENGTH_LONG).show();
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_AUTO_CONF_SUCCESS));
		}
		else 
		{
//			tracker.sendEvent(SingleCamConfigureActivity.GA_ADD_CAMERA_CATEGORY,
//					"Add Cam Failed", "Add Cam Failed - ErrorCode " + errorCode, null);
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_AUTO_CONF_FAILED, errorCode, errorCode));
		}
	}

	protected void onCancelled()
	{
		super.onCancelled();
		//dialog.cancel();

		if (scan_task != null)
		{
			scan_task.stopScan(); 
			scan_task = null;
		}


		mHandler.dispatchMessage(Message.obtain(mHandler, MSG_AUTO_CONF_FAILED));


	}

	private boolean verify_response (String request_command, String response )
	{

		if (  (response != null) &&
				(response.startsWith(request_command)))
		{
			String response_code = response.substring(request_command.length());
			if (Integer.parseInt(response_code) == 0)
			{
				//OK 
				return true;
			}
		}

		return false;

	}
	/* handle url connection */
	private String send_request (String request, int timeout)
	{
		URL url;
		String contentType = null;
		int responseCode = -1;
		HttpURLConnection conn = null;
		DataInputStream _inputStream = null;
		String response_str = null;

		String usr_pass =  String.format("%s:%s", http_usr, http_pass);
		try {
			url = new URL(request);
			/* send the request to device by open a connection*/
			conn =  (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(timeout);
			conn.setReadTimeout(timeout);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}


		try {

			responseCode  = conn.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK)
			{
				_inputStream =new DataInputStream( new BufferedInputStream(conn.getInputStream()));
				contentType = conn.getContentType();
				/* make sure the return type is text before using readLine */
				response_str = _inputStream.readLine();

				//Log.d("mbp","Get response..: " +response_str );
			}


		} catch(Exception ex) {
			//continue;
			ex.printStackTrace();
		}
		finally
		{
			conn.disconnect();
		}

		return response_str;
	}


	protected static String convertToQuotedString(String string) {
		return "\"" + string + "\"";
	}


	private void notifyScanResult() {
		synchronized (this) {
			this.notify();
		}
	}



	private void notifySupplicantState(SupplicantState ss)
	{

		synchronized(supplicantLockObject)
		{
			sstate = ss;
			supplicantLockObject.notify();
		}
	}

	private void notifyWifiState(String bssid) {
		synchronized (wifiLockObject) {

			wifiLockObject.notify();
		}
	} 

	/* For WPA2 authentication:
	 * SupplicantState in case of ERROR
	 *  - (ASSOCIATING)
	 *  - ASSOCIATED 
	 *  - FOUR_WAY_HANDSHAKE 
	 *  - DISCONNECTED 
	 *  - INACTIVE  <---- We will wait for this
	 * SupplicantState in case of SUCCESS
	 *  - (ASSOCIATING)
	 *  - ASSOCIATED 
	 *  - FOUR_WAY_HANDSHAKE 
	 *  - GROUP_HANDSHAKE
	 *  - COMPLETED  <---- We will wait for this
	 */


	private class WifiReceiver1 extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
				notifyScanResult();
			} 
			else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) 
			{
				mWifiNetworkInfo =
						(NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

				Log.d("mbp",this.getClass().getName() + " NETWORK_STATE_CHANGED_ACTION: " + mWifiNetworkInfo.isConnected() );

				if ( (mWifiNetworkInfo != null)&& mWifiNetworkInfo.isConnected() )
				{
					Log.d("mbp", "NOTIFY" );

					notifyWifiState(null);

				}
			}
			else if (action.equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION))
			{
				SupplicantState ss = (SupplicantState)
						intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
				//Log.d("mbp", "SUPPLICANT_STATE_CHANGED_ACTION: " + ss);

				if (ss.equals(SupplicantState.COMPLETED) || 
						ss.equals(SupplicantState.INACTIVE) ||
						ss.equals(SupplicantState.DISCONNECTED))
				{
					notifySupplicantState(ss);
				}

			}
			else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) 
			{
				mWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
						WifiManager.WIFI_STATE_UNKNOWN);

				//notifyWifiState();
			}
			else {
				return;
			}
		}
	}


}
