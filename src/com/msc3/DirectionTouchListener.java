package com.msc3;


import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;


public class DirectionTouchListener implements OnTouchListener
{
	
	private float indicator_center_x, indicator_center_y;
	private float translation_down_limit, translation_up_limit, 
				translation_left_limit, translation_right_limit;
	private View directionPad,directionIndicator;
	private TranslateAnimation animate;
	
	
	private float fr_deltaX , fr_deltaY; 
	private DirectionDispatcher device_comm;
	
	//private Context c;
	private Handler mHandler;

	public static final int DIRECTION_INVALID = -1;
	public static final int DIRECTION_V_NON = 0x01;
	public static final int DIRECTION_UP    = 0x02;
	public static final int DIRECTION_DOWN  = 0x04;
	public static final int DIRECTION_H_NON = 0x10;
	public static final int DIRECTION_LEFT  = 0x20;
	public static final int DIRECTION_RIGHT = 0x40;
	
	public  static final int MSG_JOYSTICK_IS_BEING_USED= 0x11;
	public  static final int MSG_JOYSTICK_IS_BEING_MOVED= 0x13;
	public  static final int MSG_JOYSTICK_IS_NOT_BEING_USED= 0x12;
	
	/* 
	 * These are used to calculated the max_translation_up/down/left/right 
	 */
	public DirectionTouchListener(Handler h, View directionPad, View directionIndicator,
			DirectionDispatcher device_comm)
	{
		this.directionPad = directionPad;
		this.directionIndicator = directionIndicator;
		fr_deltaX = fr_deltaY  = 0;
		this.device_comm = device_comm;
		//c = context;
		mHandler = h;
		
		mIsPortrait = false; 
	}
		
	private boolean mIsPortrait; 
	
	
	public synchronized void setOrientation(boolean isPortrait)
	{
		mIsPortrait = isPortrait;
	}


	/* Return  True if the listener has consumed the event, false otherwise.*/
	public boolean onTouch(View v, MotionEvent event)
	{
		
		switch(event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			
			directionIndicator.clearAnimation();
			
			
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_BEING_USED));
			
			
			directionPad.setVisibility(View.VISIBLE);
			directionIndicator.setVisibility(View.VISIBLE);
			
			indicator_center_x = directionIndicator.getLeft()+ directionIndicator.getWidth()/2;
			indicator_center_y = directionIndicator.getTop() + directionIndicator.getHeight()/2;
			
			translation_down_limit = directionPad.getHeight()/2 - directionIndicator.getHeight()/2;
			translation_up_limit = (-1)*translation_down_limit;
			translation_right_limit = directionPad.getWidth()/2 - directionIndicator.getWidth()/2;
			translation_left_limit = (-1)*translation_right_limit;
			
			validate_and_translate(event.getX(), event.getY());

			break;
		case MotionEvent.ACTION_MOVE:
			validate_and_translate(event.getX(), event.getY());
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_BEING_MOVED));
			break;
		case MotionEvent.ACTION_UP:
			validate_and_translate(indicator_center_x, indicator_center_y);
			fr_deltaX = fr_deltaY  = 0;
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_NOT_BEING_USED));
			break;
		default:
			break;
		}
		return true;
	}


	/* @new_x, new_y: new location to translate the direction indicator to
	 * This function makes sure the translation will not be outof bound 
	 */
	private void validate_and_translate(float new_x, float new_y)
	{
		float deltaX , deltaY; 
		deltaX = new_x -indicator_center_x;
		deltaY = new_y -indicator_center_y;

		/* Check outer boundary */
		if (deltaY > translation_down_limit)
		{
			deltaY = translation_down_limit;
		}
		else if (deltaY <  translation_up_limit)
		{
			deltaY =  translation_up_limit;
		}

		if (deltaX >  translation_right_limit )
		{
			deltaX = translation_right_limit;
		}
		else if  (deltaX <  translation_left_limit )
		{
			deltaX = translation_left_limit;
		}

		
		/* Check inner boundary: define some slack */

		if ( Math.abs(deltaY)< (directionIndicator.getHeight()/2))
		{
			deltaY = 0;
		}

		
		if ( Math.abs(deltaX) < (directionIndicator.getWidth()/2))
		{
			deltaX = 0;
		}

		/* Just take 1 dimension (either updown or leftright 
		 * - To do this, compare the ABS(delta) and take the dominant one 
		 */
		if (Math.abs(deltaX) > Math.abs(deltaY))
		{
			deltaY = 0;
		}
		else
		{
			deltaX = 0;
		}

		/*
		Log.d("MBP-onTouch", 
				"move fr :" +  fr_deltaX+ " "+fr_deltaY +
				" to: " + deltaX + " " + deltaY);
		*/


		device_comm.postDirection(eval_direction(deltaX, deltaY));
		
		animate = new TranslateAnimation(fr_deltaX, deltaX,fr_deltaY,  deltaY);
		
		fr_deltaX = deltaX;
		fr_deltaY = deltaY;
		
		
		animate.setDuration(10);
		animate.setFillEnabled(true);
		animate.setFillAfter(true);
		
		//directionIndicator.clearAnimation();
		directionIndicator.startAnimation(animate);
	}

	/* Find out which direction this location is in relative with the center */
	private int eval_direction(float dx, float dy)
	{
		int direction = 0;
		String direction_str = null;
		
		/* UP DOWN */
		if(dy == 0)
		{
			
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_H_NON ;	
			}
			else
			{
				direction |= DIRECTION_V_NON ;	
			}
		}
		else if (dy > 0)
		{
			
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_RIGHT;
			}
			else
			{
				direction |= DIRECTION_DOWN;	
			}
		}
		else
		{
			
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_LEFT;
			}
			else
			{
				direction |= DIRECTION_UP;	
			}
		}
			
		/* LEFT RIGHT */
		if(dx == 0)
		{
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_V_NON ;	
			}
			else
			{
				direction |= DIRECTION_H_NON ;
			}
				
		}
		else if (dx > 0)
		{
			
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_UP;
			}
			else
			{
				direction |= DIRECTION_RIGHT;	
			}
		}
		else
		{
			
			if (mIsPortrait == true)
			{
				direction |= DIRECTION_DOWN;
			}
			else
			{
				direction |= DIRECTION_LEFT;
			}
			
		}
		
		
		
		
		
		return direction  ;
	}

}
