package com.msc3;


public interface IVOXStatusUpdater {
	
	public void updateVOXIcon(boolean enabled);
}
