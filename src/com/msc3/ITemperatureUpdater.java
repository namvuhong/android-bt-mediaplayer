package com.msc3;

public interface ITemperatureUpdater {
	public void updateTemperature(int level);
}
