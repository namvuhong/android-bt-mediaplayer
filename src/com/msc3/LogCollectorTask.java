package com.msc3;

import com.blinkhd.R;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.blinkhd.R;
/*****
 * 
 * Adapt from
 * http://code.google.com/p/android-log-collector/
 *
 */

public class LogCollectorTask  extends AsyncTask<ArrayList<String>, Void, StringBuilder>
{
	public final static String LINE_SEPARATOR = System.getProperty("line.separator");
	final int MAX_LOG_MESSAGE_LENGTH = 100000;
	public static final String LOG_FILE = "mbp_log.txt";
	File externalFileDir;
	Context mContext; 
	
	public LogCollectorTask(Context c, File f)
	{
		externalFileDir = f;
		mContext = c;
	}
	
    @Override
    protected void onPreExecute(){
    }
    
    @Override
    protected StringBuilder doInBackground(ArrayList<String>... params){
       
    	final StringBuilder log = new StringBuilder();
    	Log.e("mbp", "Log collector started");
//    	String logcat_cmd = "logcat -d -v time *:s  mbp:V  " +
//    			" System.out:v FFMpegMediaPlayer-native:v" + 
//    			" mbp.update:V ALSAModule:s AudioHardwareALSA:s  " +
//    			" InputManagerService:s KeyCharacterMap:s  asset:s  " +
//    			"WeatherEngine:s  BluetoothHeadset:s";
    	String logcat_cmd = "logcat -d -v time *:s mbp:V FFMpegFileExplorer:v FFMpegPlayerActivity:v " +
    			"FFMpegPlaybackActivity:v mbp.update:V System.err:V ActivityManager:I AndroidRuntime:I " +
    			"FFMpegPlayer-JNI:v FFMpegMediaPlayer-native:v FFMpeg:v ffmpeg_onLoad:v " +
    			"FFMpegMovieViewAndroid:v libffmpeg:v FFMpegMediaPlayer-java:v FFMpegAudioDecoder:v " +
    			"FFMpegVideoDecoder:v FFMpegIDecoder:v System.out:v AudioTrack:v native-Pjnath:v " +
    			"stun_client:v DEBUG:v";
    	//String logcat_cmd = "logcat -d ";
        try{
            //ArrayList<String> commandLine = new ArrayList<String>();
            //commandLine.add("logcat");//$NON-NLS-1$
            //commandLine.add("-d");
            //commandLine.add("*:w");
            //commandLine.add("mbp:v");
            
            //ArrayList<String> arguments = ((params != null) && (params.length > 0)) ? params[0] : null;
            //   if (null != arguments){
            //    commandLine.addAll(arguments);
            //}
            
            //Process process = Runtime.getRuntime().exec(commandLine.toArray(new String[0]));
            Process process = Runtime.getRuntime().exec(logcat_cmd);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            String line;
            while ((line = bufferedReader.readLine()) != null){ 
                log.append(line);
                log.append(LINE_SEPARATOR); 
            }
            
        } 
        catch (IOException e){
            Log.e("mbp", "CollectLogTask.doInBackground failed", e);//$NON-NLS-1$
        } 

        return log;
    }

    @Override
    protected void onPostExecute(StringBuilder log){
        if (null != log){
            //truncate if necessary
//            int keepOffset = Math.max(log.length() - MAX_LOG_MESSAGE_LENGTH, 0);
//            if (keepOffset > 0){
//                log.delete(0, keepOffset);
//            }
            
    		
    		
            storeOnSD(log);
    		//sendEmail(log);

        }	
        
    }
    
    private void storeOnSD(StringBuilder log)
    {
    	
    	/* create a data file in the external dir */
		File file = new File(Util.getLogFile() );
		if (file.exists())
		{
			Log.e("mbp", "File exist, remove it");
			/* remove the old file */
			file.delete();
		}
		
		try {
			OutputStream os = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(os);
			pw.write(log.toString());
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			if (file.exists())
			{
				/* remove the incomplete file */
				file.delete();
			}
			e.printStackTrace();
		}
		Log.e("mbp", "save to file:" +file.getAbsolutePath()); 
    	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage("Log saved to file:" +file.getAbsolutePath())
		.setCancelable(true)
		.setPositiveButton(mContext.getString(R.string.OK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface _dialog, int which) {
				_dialog.dismiss();
			}
		});

		builder.create().show();
    }
    
    private void sendEmail(StringBuilder log)
    {
    	Intent i = new Intent(Intent.ACTION_SEND);
    	i.setType("text/plain");
    	i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"triphung@nxcomm.com"});
    	i.putExtra(Intent.EXTRA_SUBJECT, "MSC Log file");
    	i.putExtra(Intent.EXTRA_TEXT   , log.toString());
    	try {
    	    mContext.startActivity(Intent.createChooser(i, "Send mail..."));
    	} catch (android.content.ActivityNotFoundException ex) {
    	    
    	    //if no email then store on Card
    	   
    	}
    }
    
    /*Usage: logcat [options] [filterspecs]
    options include:
      -s              Set default filter to silent.
                      Like specifying filterspec '*:s'
      -f <filename>   Log to file. Default to stdout
      -r [<kbytes>]   Rotate log every kbytes. (16 if unspecified). Requires -f
      -n <count>      Sets max number of rotated logs to <count>, default 4
      -v <format>     Sets the log print format, where <format> is one of:

                      brief process tag thread raw time threadtime long

      -c              clear (flush) the entire log and exit
      -d              dump the log and then exit (don't block)
      -g              get the size of the log's ring buffer and exit
      -b <buffer>     request alternate ring buffer
                      ('main' (default), 'radio', 'events')
      -B              output the log in binary
    filterspecs are a series of
      <tag>[:priority]

    where <tag> is a log component tag (or * for all) and priority is:
      V    Verbose
      D    Debug
      I    Info
      W    Warn
      E    Error
      F    Fatal
      S    Silent (supress all output)

    '*' means '*:d' and <tag> by itself means <tag>:v

    If not specified on the commandline, filterspec is set from ANDROID_LOG_TAGS.
    If no filterspec is found, filter defaults to '*:I'

    If not specified with -v, format is set from ANDROID_PRINTF_LOG
    or defaults to "brief"*/
    
//    void collectAndSendLog(){
//        
//
//        ArrayList<String> list = new ArrayList<String>();
//        
//        if (mFormat != null){
//            list.add("-v");
//            list.add(mFormat);
//        }
//        
//        if (mBuffer != null){
//            list.add("-b");
//            list.add(mBuffer);
//        }
//
//        if (mFilterSpecs != null){
//            for (String filterSpec : mFilterSpecs){
//                list.add(filterSpec);
//            }
//        }
//        
//        mCollectLogTask = (CollectLogTask) new CollectLogTask().execute(list);
//    }
}
