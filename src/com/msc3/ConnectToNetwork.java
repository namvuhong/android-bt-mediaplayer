package com.msc3;

import java.util.List;

import com.msc3.registration.SingleCamConfigureActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.blinkhd.R;
/*
 * Connect to a network given a WifiConfiguration
 */

public class ConnectToNetwork extends AsyncTask<WifiConfiguration, String, Boolean> {

	public static final int MSG_CONNECT_TO_NW_DONE = 0x100;
	public static final int MSG_CONNECT_TO_NW_FAILED = 0x101;
	public static final int MSG_CONNECT_TO_NW_CANCELLED = 0x102; 

	private Dialog dialog = null;
	private Context mContext;
	private Object wifiLockObject;
	private Handler mHandler;
	private String bssid= null, ssid = null;

	NetworkInfo mWifiNetworkInfo;
	private WifiBr br;
	private boolean ignoreBSSID; 
	private boolean silence; 
	private boolean dontRemoveConfigurationIfConnectFailed; 
	public ConnectToNetwork(Context c, Handler h )
	{
		mContext = c;
		wifiLockObject = new Object();
		
		mHandler = h;
		ignoreBSSID = false;
		silence = false;
		dontRemoveConfigurationIfConnectFailed = false;
	}

	public void setIgnoreBSSID(boolean ignore)
	{
		ignoreBSSID = ignore;
	}

	public void setSilence(boolean s)
	{
		silence = s;
		
	}
	public void dontRemoveFailedConnection (boolean r)
	{
		dontRemoveConfigurationIfConnectFailed = r;
	}

	protected void onPreExecute ()
	{

		dialog = new ProgressDialog(mContext);
		Spanned msg= Html.fromHtml("<big>"+mContext.getString(R.string.ConnectToNetworkActivity_connecting)+"</big>");
		((AlertDialog) dialog).setMessage(msg);
		((ProgressDialog) dialog).setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				ConnectToNetwork.this.cancel(true);
			}
		});
		((AlertDialog) dialog).setButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		if (!silence )
		{
			dialog.show();
		}
	}

	protected void onPostExecute (Boolean result)
	{
		if (dialog != null && dialog.isShowing())
		{
			try
			{
				dialog.dismiss();
			}
			catch (IllegalArgumentException iae)
			{
				
			}
		}
		if ( mHandler != null)
		{

			if (result.booleanValue()==true)
			{
				Message m = Message.obtain(mHandler, MSG_CONNECT_TO_NW_DONE);
				mHandler.dispatchMessage(m);
			}
			else
			{
				mHandler.dispatchMessage(Message.obtain(mHandler, MSG_CONNECT_TO_NW_FAILED, ssid));
			}
		}
	}
	
	protected void onCancelled()
	{
		if (dialog != null && dialog.isShowing())
		{
			try
			{
				dialog.dismiss();
			}
			catch (IllegalArgumentException iae)
			{
				
			}
		}
		if ( mHandler != null)
		{

			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_CONNECT_TO_NW_CANCELLED));
		}
	}


	@Override
	protected Boolean doInBackground(WifiConfiguration... params) {

		WifiConfiguration conf= params[0];
		WifiManager w = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE); 

		List<WifiConfiguration> wcs = null; 

		bssid = conf.BSSID;
		ssid = conf.SSID;

		br = new WifiBr();
		IntentFilter i = new IntentFilter();
		i.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		mContext.registerReceiver(br,i);

		int nw_id = -1;
		/*1. try to connect to the corresponding BSSID */

		Log.d("mbp", "Connect to : " + ssid + " - " +  bssid );
		boolean connection_result = false;
		WifiConfiguration wc ;
		int retries = 1;
		do 
		{
			
			wcs = w.getConfiguredNetworks();
			if (wcs != null)
			{
				for(int j = 0 ; j< wcs.size(); j++)
				{
					wc =  wcs.get(j);
					if(  (wc.SSID != null) && 
							(wc.SSID.equalsIgnoreCase( ssid )) )
					{
						if ( ignoreBSSID || 
								((wc.BSSID != null) &&(wc.BSSID.equalsIgnoreCase(bssid))) 
								)
						{
							Log.d("mbp","Found wc:"+ wc.networkId+ " : " + wc.SSID +
									" algo: " + wc.allowedAuthAlgorithms + 
									" key: " + wc.allowedKeyManagement +
									" grp:" + wc.allowedGroupCiphers +
									" pair: " + wc.allowedPairwiseCiphers +
									" proto:" + wc.allowedProtocols + 
									" hidden: " + wc.hiddenSSID +
									" preSharedKey: " + wc.preSharedKey);
							nw_id = wc.networkId;
							break;
						}
					}

				}
			}
	
			
			boolean enabled ;
			enabled = w.enableNetwork(nw_id, true);
			Log.d("mbp", "enableNetwork id: " + nw_id + " : " + enabled );
			
			Log.d("mbp", ">>> current connection ip: " + w.getConnectionInfo().getIpAddress());
			
			if ( ignoreBSSID || 
					(w.getConnectionInfo().getBSSID()== null) ||
					(!w.getConnectionInfo().getBSSID().equalsIgnoreCase(bssid)) ||
					(w.getConnectionInfo().getIpAddress() == 0)
					
					)
			{

				//need to wait for the network to be up
				synchronized (wifiLockObject) {
					try {
						wifiLockObject.wait(30000);

					} catch (InterruptedException e) {
						e.printStackTrace();
						
						
					}
					finally
					{
						if (isCancelled())
						{
							w.disableNetwork(nw_id);
							break;
						}
					}
				}

			}

			ConnectivityManager conn = (ConnectivityManager) 
					mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifiInfo = conn.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifiInfo != null && wifiInfo.isConnected())
			{


				if (  /* some check on the network id */
						(w.getConnectionInfo().getNetworkId() != -1) &&
						( ignoreBSSID ||
						  (w.getConnectionInfo().getBSSID()!= null) &&
					      w.getConnectionInfo().getBSSID().equalsIgnoreCase(bssid) )
						)
				{
					Log.d("mbp", "got the correct BSSID!!>>>>>>> ip: " + w.getDhcpInfo().ipAddress +
							"\n gw ip: " + w.getDhcpInfo().gateway);

					if (  (w.getConnectionInfo().getIpAddress() == 0 )|| 
							(w.getDhcpInfo().ipAddress == 0) )
					{
						//We're connected but don't have any IP yet
						retries++; //give 1 more chance
						continue;
					}
					else
					{
						//Now we're sure that the connection is ready
						connection_result = true;
						break;
					}
				}

			}
			

		} while (retries-- > 0);

		if (  (connection_result == false) && 
				(dontRemoveConfigurationIfConnectFailed == false))
		{
			/* bad connection - remove it */
			w.removeNetwork(nw_id);
			w.saveConfiguration();
		}
			
		
		
		if (br != null) {
			try 
			{
				mContext.unregisterReceiver(br);
			}
			catch (IllegalArgumentException iae)
			{
				//ignore it
			}
		}
		


		return new Boolean(connection_result);
	}



	private class WifiBr extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {


			mWifiNetworkInfo =
					(NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);


			Log.d("mbp", "NW_STATE:" + mWifiNetworkInfo.isConnected()  );
			
			if ( (mWifiNetworkInfo != null)&& mWifiNetworkInfo.isConnected() )
			{

				//String mBssid = intent.getStringExtra(WifiManager.EXTRA_BSSID);
				Log.d("mbp", "NW_STATE:" + mWifiNetworkInfo.isConnected()  );

				synchronized (wifiLockObject) {
					wifiLockObject.notify();
				}
			}				
		}
	}
}
