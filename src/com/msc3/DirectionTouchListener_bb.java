package com.msc3;


import com.blinkhd.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;



public class DirectionTouchListener_bb implements OnTouchListener
{

	private float indicator_center_x, indicator_center_y;
	private float translation_down_limit, translation_up_limit, 
	translation_left_limit, translation_right_limit;
	private ImageView directionPad;
	private ImageView panPoint;
	private ImageView panLine;
	private TranslateAnimation animate;


	private float fr_deltaX , fr_deltaY; 
	private DirectionDispatcher device_comm;

	private Context mContext;
	private Handler mHandler;

	public static final int DIRECTION_INVALID = -1;
	public static final int DIRECTION_V_NON = 0x01;
	public static final int DIRECTION_UP    = 0x02;
	public static final int DIRECTION_DOWN  = 0x04;
	public static final int DIRECTION_H_NON = 0x10;
	public static final int DIRECTION_LEFT  = 0x20;
	public static final int DIRECTION_RIGHT = 0x40;


	public  static final int MSG_JOYSTICK_IS_BEING_USED= 0x11;
	public  static final int MSG_JOYSTICK_IS_BEING_MOVED= 0x13;
	public  static final int MSG_JOYSTICK_IS_NOT_BEING_USED= 0x12;
	private boolean mIsPortrait; 
	
	/* 
	 * These are used to calculated the max_translation_up/down/left/right 
	 */
	public DirectionTouchListener_bb(Context mContext, Handler h, ImageView directionPad, View v,
			DirectionDispatcher device_comm)
	{
		this.directionPad = directionPad;
		if (v != null)
		{
			panPoint = (ImageView) v.findViewById(R.id.panPoint);
			panLine = (ImageView) v.findViewById(R.id.panHandler);
		}
		fr_deltaX = fr_deltaY  = 0;
		this.device_comm = device_comm;
		mHandler = h;
		this.mContext = mContext;
		mIsPortrait = false; 
	}

	public void setDirectionDispatcher(DirectionDispatcher newDispatcher)
	{
		this.device_comm =newDispatcher;
	}
	


	public synchronized void setOrientation(boolean isPortrait)
	{
		mIsPortrait = isPortrait;

	}


	/* Return  True if the listener has consumed the event, false otherwise.*/
	public boolean onTouch(View v, MotionEvent event)
	{

		switch(event.getAction())
		{
		case MotionEvent.ACTION_DOWN:



			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_BEING_USED));


			directionPad.setVisibility(View.VISIBLE);

			indicator_center_x = directionPad.getWidth()/2;
			indicator_center_y = directionPad.getHeight()/2;

			translation_down_limit = directionPad.getHeight()/2 ;
			translation_up_limit = (-1)*translation_down_limit;
			translation_right_limit = directionPad.getWidth()/2 ;
			translation_left_limit = (-1)*translation_right_limit;

			validate_and_translate(event.getX(), event.getY());



			break;
		case MotionEvent.ACTION_MOVE:
			validate_and_translate(event.getX(), event.getY());
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_BEING_MOVED));
			break;
		case MotionEvent.ACTION_UP:
			validate_and_translate(indicator_center_x, indicator_center_y);
			fr_deltaX = fr_deltaY  = 0;
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_JOYSTICK_IS_NOT_BEING_USED));

			break;
		default:
			break;
		}
		return true;
	}



	/* @new_x, new_y: new location to translate the direction indicator to
	 * This function makes sure the translation will not be outof bound 
	 */
	private void validate_and_translate(float new_x, float new_y)
	{
		float deltaX , deltaY; 
		deltaX = new_x -indicator_center_x;
		deltaY = new_y -indicator_center_y;

		/* Check outer boundary */
		if (deltaY > translation_down_limit)
		{
			deltaY = translation_down_limit;
		}
		else if (deltaY <  translation_up_limit)
		{
			deltaY =  translation_up_limit;
		}

		if (deltaX >  translation_right_limit )
		{
			deltaX = translation_right_limit;
		}
		else if  (deltaX <  translation_left_limit )
		{
			deltaX = translation_left_limit;
		}

		/* Check inner boundary: define some slack 

		if ( Math.abs(deltaY)< (directionIndicator.getHeight()/2))
		{
			deltaY = 0;
		}



		if ( Math.abs(deltaX) < (directionIndicator.getWidth()/2))
		{
			deltaX = 0;
		}*/

		/* Just take 1 dimension (either updown or leftright 
		 * - To do this, compare the ABS(delta) and take the dominant one 
		 */
		if (Math.abs(deltaX) > Math.abs(deltaY))
		{
			deltaY = 0;
		}
		else
		{
			deltaX = 0;
		}
		
		
		int dir = eval_direction(deltaX, deltaY);

		/*20120717 : Support only 1 DIRECTION now  */
		switch (dir & 0x0F)
		{
		case DIRECTION_V_NON:
			if ((dir & 0xF0) == DIRECTION_H_NON)
			{	
				//directionPad.setImageResource(R.drawable.circle_buttons1_2);
				panLine.setTranslationX(0);
				panLine.setTranslationY(0);
				panLine.setVisibility(View.INVISIBLE);
				
				panPoint.setTranslationY(0);
				panPoint.setTranslationX(0);
			}
			break;
		case DIRECTION_UP:
			//directionPad.setImageResource(R.drawable.circle_buttons1_up);
			panLine.setVisibility(View.VISIBLE);
			panLine.setTranslationX(0);
			panLine.setTranslationY(-panLine.getHeight()/2);
			panLine.setRotation(0);
			
			panPoint.setTranslationX(0);
			panPoint.setTranslationY(-panLine.getHeight());
			
			break;
		case DIRECTION_DOWN:
			//directionPad.setImageResource(R.drawable.circle_buttons1_dn);
			panLine.setVisibility(View.VISIBLE);
			panLine.setTranslationX(0);
			panLine.setTranslationY(panLine.getHeight()/2);
			panLine.setRotation(0);
			
			panPoint.setTranslationX(0);
			panPoint.setTranslationY(panLine.getHeight());
			
			break;
		}

		switch (dir & 0xF0)
		{
		case DIRECTION_H_NON:
			if ((dir & 0x0F) == DIRECTION_V_NON)
			{	
				//directionPad.setImageResource(R.drawable.circle_buttons1_2);
				panLine.setRotation(0);
				panLine.setTranslationX(0);
				panLine.setTranslationY(0);
				panLine.setVisibility(View.INVISIBLE);
				
				panPoint.setTranslationY(0);
				panPoint.setTranslationX(0);
			}
			break;
		case DIRECTION_RIGHT:
			//directionPad.setImageResource(R.drawable.circle_buttons1_rt);
			panLine.setVisibility(View.VISIBLE);
			panLine.setTranslationY(0);
			panLine.setTranslationX(panLine.getHeight()/2);
			panLine.setRotation(90);
			
			panPoint.setTranslationX(panLine.getHeight());
			panPoint.setTranslationY(0);
			
			break;
		case DIRECTION_LEFT:
			//directionPad.setImageResource(R.drawable.circle_buttons1_lf);
			panLine.setVisibility(View.VISIBLE);
			panLine.setTranslationY(0);
			panLine.setTranslationX(-panLine.getHeight()/2);
			panLine.setRotation(90);
			
			panPoint.setTranslationX(-panLine.getHeight());
			panPoint.setTranslationY(0);
			
			break;
		}
		//directionPad.invalidate();
	
		
		
		
		dir = fix_direction(dir);
		
		
		device_comm.postDirection(dir);

	}


	private int fix_direction(int currentDir)
	{

		int rotatedDir = 0; 
		if (mIsPortrait == true)
		{

			

			switch (currentDir & 0x0F)
			{
			case DIRECTION_V_NON:

				rotatedDir |= DIRECTION_H_NON ;

				break;
			case DIRECTION_UP:
				rotatedDir |= DIRECTION_LEFT ;
				break;
			case DIRECTION_DOWN:
				rotatedDir |= DIRECTION_RIGHT;
				break;
			}


			switch (currentDir & 0xF0)
			{
			case DIRECTION_H_NON:
				rotatedDir |= DIRECTION_V_NON ;
				break;
			case DIRECTION_RIGHT:
				rotatedDir |= DIRECTION_UP;
				break;
			case DIRECTION_LEFT:
				rotatedDir |= DIRECTION_DOWN;
				break;
			}



		}
		else
		{
			rotatedDir = currentDir;
		}

		return rotatedDir;
	}

	/* Find out which direction this location is in relative with the center */
	private int eval_direction(float dx, float dy)
	{
		int direction = 0;
		String direction_str = null;

		/* UP DOWN */
		if(dy == 0)
		{
			direction |= DIRECTION_V_NON ;	
		}
		else if (dy > 0)
		{

			direction |= DIRECTION_DOWN;	
		}
		else
		{

			direction |= DIRECTION_UP;	
		}

		/* LEFT RIGHT */
		if(dx == 0)
		{
			direction |= DIRECTION_H_NON ;

		}
		else if (dx > 0)
		{

			direction |= DIRECTION_RIGHT;	
		}
		else
		{

			direction |= DIRECTION_LEFT;

		}







		return direction  ;
	}

}
