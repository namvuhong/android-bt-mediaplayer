/**
 * 
 */
package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;

/**
 * @author NxComm
 *
 */
public class ViewRemoteCamMediaServerRequestTask extends ViewRemoteCamRequestTask {
	
	private static final String str_url = "rtmpurl:";

	private String userName;
	private String userPass;
	private String mac;
	private String url;
	
	private int server_err_code;
	
	/**
	 * @param h
	 * @param mContext
	 */
	public ViewRemoteCamMediaServerRequestTask(Handler h, Context mContext) {
		super(h, mContext);
		url = null;
		server_err_code = 404;
	}
	

	@Override
	protected BabyMonitorAuthentication doInBackground(String... params) {
		userName = params[0];
		userPass = params[1];
		mac = params[2];
		mac = PublicDefine.strip_colon_from_mac(mac).toUpperCase();
		
		BabyMonitorAuthentication auth = null;
		
		Log.d("mbp", "View cam h264 request");
		
		String usr_pass =  String.format("%s:%s", userName, userPass);

		String http_cmd = PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART + 
				PublicDefine.VIEW_CAM_H264_CMD +
				PublicDefine.VIEW_CAM_H264_PARAM_1 + mac +
				PublicDefine.VIEW_CAM_H264_PARAM_2 + userName;

		
		URL url = null;
		HttpsURLConnection conn = null;
		DataInputStream inputStream = null;
		
		try {

			url = new URL(http_cmd);
			conn = (HttpsURLConnection) url.openConnection();
			if (ssl_context != null)
			{
				conn.setSSLSocketFactory(ssl_context.getSocketFactory());
			}
			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(40000);

			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream(), 4*1024));
			server_err_code = conn.getResponseCode();
			Log.d("mbp", "View cam h264 responseCode: " + server_err_code);
			if (server_err_code == HttpURLConnection.HTTP_OK)
			{
				String response_str = inputStream.readLine();
				if (response_str != null)
				{
					response_str = response_str.trim();
					if (response_str.startsWith(str_url))
					{
						String response_url = response_str.substring(str_url.length());
						String channelId = "123456789012";
						String sskey = null;
						
						auth = new BabyMonitorRelayAuthentication(
								"127.0.0.1", "80",
								channelId, mac, sskey, response_url, 80, userName, userPass);
					}
				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return auth;
	}
	
	@Override
	protected void onPostExecute(BabyMonitorAuthentication result) {
		Message m;
		
		if (result != null)
		{
			Log.d("mbp", "Preparing to get video stream from Wowza server...");
//			try {
//				Log.d("mbp", "Wait for Wowza server setup..sleep 10s...");
//				Thread.sleep(10000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			m = Message.obtain(mHandler, ViewRemoteCamRequestTask.MSG_VIEW_CAM_SUCCESS, result);
		}
		else
		{
			m = Message.obtain(mHandler, ViewRemoteCamRequestTask.MSG_VIEW_CAM_FALIED, server_err_code, server_err_code);
		}
		
		mHandler.dispatchMessage(m);
	}
	
}
