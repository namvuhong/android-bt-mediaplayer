package com.msc3;




import com.blinkhd.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainMenuImageAdapter extends BaseAdapter {
    private Context mContext;
    private boolean hideSomeItems;

    public MainMenuImageAdapter(Context c) {
        mContext = c;
        hideSomeItems = false;
    }
    
    /* @isInMultiMode = true -> only display those items 
     * that can be accessed in multi camera mode such as Setup, Playback
     * Other items are hidden.
     */
    public MainMenuImageAdapter(Context c, boolean isInMultiMode) {
        mContext = c;
        hideSomeItems = isInMultiMode;
    }

    public int getCount() {
    	if (hideSomeItems)
    	{
    		/* this is because the first 2 items are valid 
    		 * for multi camera mode
    		 */
    		return 2;
    	}
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(90, 90));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(4, 4, 4, 4);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        
        imageView.setOnTouchListener(
        		new ButtonTouchListener(mContext.getResources().getDrawable(mThumbIds[position]),
        				                mContext.getResources().getDrawable(mThumbIds_disabled[position])));
        
        
        return imageView;
    }

    public void setNormalItemList( Integer[] newlist)
    {
    	mThumbIds  = newlist;
    }
    public void setHighlightedItemList( Integer[] newlist)
    {
    	mThumbIds_disabled  = newlist;
    }
  
    
    // default icons - Not supposed to be used
    private Integer[] mThumbIds = {
            R.drawable.large_icon1_1, R.drawable.large_icon2_1,
            R.drawable.large_icon3_5,  R.drawable.large_icon6_1,
    };
    // default icons - Not supposed to be used
    private Integer[] mThumbIds_disabled = {
            R.drawable.large_icon1_1_d, R.drawable.large_icon2_1_d,
            R.drawable.large_icon3_5_off,R.drawable.large_icon6_1_d,
    };

}
