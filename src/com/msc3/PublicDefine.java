package com.msc3;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.blinkhd.R;
import com.google.gson.Gson;

public class PublicDefine
{
	public static final String PREFS_READ_WRITE_OFFLINE_DATA				  = "bool_read_write_offline_data";
	// use for enable stun or not
	// 1: enabled, 0: disabled
	public static final String	PREFS_IS_STUN_ENABLED	                      = "is_stun_enabled";
	public static final String  PREFS_USE_REMOTE_ONLY						  = "bool_use_remote_only";
	public static final String  PREFS_MBP_DISCONNECT_ALERT					  = "bool_is_disconnect_alert_enabled";
	/**
	 * Variables for Do not disturb function
	 */
	public static final String	PREFS_IS_DO_NOT_DISTURB_ENABLE	              = "is_do_not_disturb_enable";
	public static final String	PREFS_TIME_DO_NOT_DISTURB_EXPIRED	          = "time_do_not_disturb_expired";
	public static final String	PREFS_DO_NOT_DISTURB_REMAINING_TIME	          = "do_not_disturb_remaining_time";

	public static final String	PREFS_SAVED_SERVER_URL	                      = "string_ServerUrl";

	public static final String	PUBLIC_TOKEN	                              = "xFny6NgC363acP1iyQ8z";

	public static final String	PREFS_NAME	                                  = "MBP_SETTINGS";
	public static final String	PHONE_MBP2k	                                  = "Home Phone MBP2000";
	public static final String	PHONE_MBP1k	                                  = "MBP1000";
	public static final String	PHONE_IHOMEPHONE5	                          = "A13MID";

	public static final String	PREFS_APP_LAUNCHED_NOT_FIRST_TIME	          = "bool_appLaunched2ndtime";
	public static final Gson	gson	                                      = new Gson();
	// Preference Constants

	public static final String	PREF_TWITTER_KEY_OAUTH_TOKEN	              = "oauth_token";
	public static final String	PREF_TWITTER_KEY_OAUTH_SECRET	              = "oauth_token_secret";
	public static final String	PREF_KEY_TWITTER_LOGIN	                      = "is_loged_to_twitter";

	public static final String	PREFS_TRIGGER_RECORDING	                      = "bool_TriggerRecording";

	public static final String	PREFT_GOOGLE_LOGIN_TOKEN	                  = "google_login_token";
	public static final String	PREFT_GOOGLE_EMAIL_ID	                      = "google_email_id";

	public static final String	LOGIN_METHOD	                              = "login_method";
	public static final String	PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING	= "selected_camera_mac_from_camera_setting";

	public static final String	FACEBOOK_LOGIN	                              = "facebook";
	public static final String	TWITTER_LOGIN	                              = "twitter";
	public static final String	GOOGLE_LOGIN	                              = "google";
	public static final String	CLOSE_P2P_RTSP_STUN	                          = "action=command&command=close_p2p_rtsp_stun&port1=%d&port2=%d&ip=%s";
	public static final String	OPEN_P2P_RTSP_STUN	                          = "action=command&command=get_session_key&mode=p2p_stun_rtsp&port1=%d&port2=%d&ip=%s";
	public static final String	PREFS_VQUALITY	                              = "int_VideoQuality";
	public static final String	PREFS_MULTI_MODE	                          = "bool_IsMultiMode";

	public static final String	PREFS_CURRENT_DEVICE	                      = "string_CurrentDevice";
	public static final String	PREFS_CURRENT_DEVICE_MAC	                  = "string_CurrentDeviceMac";
	public static final String	PREFS_CURRENT_SSID	                          = "string_CurrentSSID";
	public static final String	PREFS_CURRENT_NW_SEC	                      = "string_CurrentNwSec";
	public static final String	PREFS_CURRENT_CAMERA_MODE	                  = "int_ConnectionMode";
	public static final String	PREFS_TRANSIENT_USERNAME	                  = "string_UserName";
	public static final String	PREFS_TRANSIENT_PASSWD	                      = "string_Passwd";
	public static final String	PREFS_TRANSIENT_DEV_MAC	                      = "string_DevMac";
	public static final String	PREFS_TRANSIENT_DEV_IP	                      = "string_DevIp";
	public static final String	PREFS_TRANSIENT_DEV_PORT	                  = "int_DevPort";
	public static final String	PREFS_TEMPERATURE_UNIT	                      = "int_tempUnit";
	public static final String	PREFS_CLOCK_MODE	                          = "int_clockMode";

	public static final String	PREFS_USED_SAVED_USR_PWD	                  = "bool_UseSavedPortalUsr";
	public static final String	PREFS_SAVED_PORTAL_USR	                      = "string_PortalUsr";
	public static final String	PREFS_SAVED_PORTAL_ID	                      = "string_PortalUsrId";	                                                             // JUST
	                                                                                                                                                                 // TO
	                                                                                                                                                                 // Display
	public static final String	PREFS_SAVED_PORTAL_PWD	                      = "string_PortalPass";
	public static final String	PREFS_SAVED_PORTAL_TOKEN	                  = "string_PortalToken";

	public static final String	PREFS_TEMP_PORTAL_USR	                      = "string_TempUsr";
	public static final String	PREFS_TEMP_PORTAL_PWD	                      = "string_TempPass";

	public static final String	PREFS_SAVED_WIFI_SSID	                      = "string_SavedSsid";
	public static final String	PREFS_SAVED_WIFI_PWD	                      = "string_SavedSsidPass";
	public static final String	PREFS_SAVED_WIFI_SEC	                      = "string_SavedWFSec";	                                                             // {"OPEN",
	                                                                                                                                                                 // "WEP",
	                                                                                                                                                                 // "WPA"}
	public static final String	PREFS_SAVED_WIFI_SEC_WEP_IDX	              = "string_SavedWFSecKeyIdx";	                                                         /*
																																									 * key
																																									 * index
																																									 * 1
																																									 * -
																																									 * 4
																																									 */
	public static final String	PREFS_SAVED_WIFI_SEC_WEP_AUTH	              = "string_SavedWFSecAuth";	                                                         /*
																																									 * Open
																																									 * ,
																																									 * Shared
																																									 */
	public static final String	PREFS_SAVED_WIFI_HIDDEN_SSID	              = "string_SavedHiddenSsid";	                                                         /*
																																									 * true
																																									 * ,
																																									 * false
																																									 */

	public static final String	PREFS_AUTO_SHOW_CAM_LIST	                  = "bool_autoShowCamList";

	public static final String	PREFS_CAM_VOLUME	                          = "int_Volume";

	public static final String	PREFS_SAVED_CAM_SSID_IN_DIRECT_MODE	          = "string_DirectModeSsid";

	public static final String	PREFS_USER_ACCES_INFRA_OFFLINE	              = "bool_InfraOfflineMode";

	public static final String	PREFS_VOX_SHOULD_TAKE_WAKELOCK	              = "bool_VoxShouldTakeWakelock";

	public static final String	PREFS_PUSH_NOTIFICATION_ID	                  = "string_push_notification_reg_id";
	public static final String	PREFS_PUSH_REG_STATUS	                      = "bool_has_registered_with_BMS";
	public static final String	PREFS_CAM_BEING_VIEWED	                      = "string_CameraBeingViewed";

	public static final String	PREFS_PUSH_NOTIFICATION_APP_ID	              = "long_push_notification_app_id";

	public static final String	PREFS_LOCAL_VIEW_MUTED	                      = "bool_audio_is_muted";

	/* MBP-specific setting */
	public static final String	PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL	          = "bool_vox_alarm_enabled_on_call";
	public static final String	PREFS_MBP_VOX_ALERT_IS_RECURRING	          = "bool_vox_alarm_is_recurring";
	public static final String	PREFS_MBP_CAMERA_DISCONNECT_ALERT	          = "bool_camera_disconnect_alert";
	// 20121204: ONLY used for Camera disconnect Alert check.
	public static final String	PREFS_MBP_CAMERA_DISCONNECT_HOME_WIFI	      = "string_camera_home_wifi";

	/*
	 * 20130121: issue 1173: hoang: ask user whether switch to Wifi or not
	 */
	public static final String	PREFS_DONT_ASK_ME_AGAIN	                      = "bool_dont_ask_me_again";
	public static final String	PREFS_SHOULD_TURN_ON_WIFI	                  = "bool_should_turn_on_wifi";
	public static final String	PREFS_SHOULD_USE_LOCAL_WOWZA_SERVER	          = "bool_should_use_local_wowza_server";

	public static final String	DEFAULT_SSID	                              = "Camera-";
	public static final String	DEFAULT_SSID_HD	                              = "CameraHD-";
	public static final String	DEFAULT_BASIC_AUTH_USR	                      = "camera";
	public static final String	DEFAULT_CAM_NAME	                          = "Camera";

	public static final int	    TEMPERATURE_UNIT_DEG_F	                      = 0;
	public static final int	    TEMPERATURE_UNIT_DEG_C	                      = 1;
	public static final int	    HIGH_F_TEMPERATURE_THRESHOLD	              = 84;
	public static final int	    LOW_F_TEMPERATURE_THRESHOLD	                  = 57;
	public static final int	    HIGH_C_TEMPERATURE_THRESHOLD	              = 29;
	public static final int	    LOW_C_TEMPERATURE_THRESHOLD	                  = 14;

	public static final int	    CLOCK_MODE_12H	                              = 0;
	public static final int	    CLOCK_MODE_24H	                              = 1;

	public static final int	    VOX_SENSITIVITY_LOW	                          = -10;
	public static final int	    VOX_SENSITIVITY_MED	                          = -20;
	public static final int	    VOX_SENSITIVITY_HI	                          = -25;
	public static final int	    VOX_SENSITIVITY_V_HI	                      = -30;
	public static final int	    VOX_SENSITIVITY_DISABLED	                  = -1;

	public static final int	    DEVICE_RESTART_TOTAL_TIME	                  = 40 * 1000;	                                                                         // 40sec

	// increase timeout to 180 sec.
	public static final int	    DEVICE_RESTART_INFRA_TOTAL_TIME	              = 60 * 1000;	                                                                         // 60sec

	public static final int	    BUFFER_EMPTY	                              = 0;
	public static final int	    BUFFER_PROCESSING	                          = 1;
	public static final int	    BUFFER_FULL	                                  = 2;
	public static final int	    STOP	                                      = 0;
	public static final int	    RUN	                                          = 1;
	public static final int	    MAX_REACHED	                                  = 2;

	// public static final int MAX_IMAGE_BUF_LEN =20*1024;// 65536;
	// public static final int MAX_IMAGE_BUF_NUMBER = 16;//10;
	public static final int	    MAX_IMAGE_BUF_LEN	                          = 65536;	                                                                             // Needed
	                                                                                                                                                                 // for
	                                                                                                                                                                 // 640x480
	                                                                                                                                                                 // format
	public static final int	    MAX_IMAGE_BUF_NUMBER	                      = 16;	                                                                             // 10;

	public static final int	    MAX_AUDIO_BUF_LEN	                          = 1010;
	public static final int	    MAX_AUDIO_BUR_NUMBER	                      = 16;

	public static final int	    RESOLUTON_VGA	                              = 0;
	public static final int	    RESOLUTON_QVGA	                              = 1;
	public static final int	    RESOLUTON_QQVGA	                              = 2;

	public static final int	    MAX_RECORD_SIZE_DEFAULT	                      = 50;

	public static final String	DEVICE_IP	                                  = "192.168.2.1";
	public static final String	DEFAULT_DEVICE_PORT	                          = "80";

	public static final String	DEFAULT_HTTP	                              = "http://"
	                                                                                  + DEVICE_IP
	                                                                                  + ":80";

	public static final int	    DEFAULT_AUDIO_PORT	                          = 51108;
	public static final int	    DEFAULT_ALARM_TCP_PORT	                      = 51109;
	public static final int	    DEFAULT_ALARM_LISTEN_PORT	                  = 51110;
	public static final int	    DEFAULT_ALARM_DEACTIVATE_PORT	              = 51111;

	public static final String	VOX_ALARM	                                  = "VOX:";
	public static final String	HOT_ALARM	                                  = "TEMP_HIGH:";
	public static final String	COLD_ALARM	                                  = "TEMP_LOW:";

	public static final String	DEFAULT_CAM_PWD	                              = "000000";
	public static final String	DEFAULT_WPA_PRESHAREDKEY					  = "00000000";
	
	public static final String  TALKBACK_SERVER 							  = "http://talkback.hubble.in/devices";
	public static final String	START_TALKBACK_CMD							  = "/start_talk_back?";
	public static final String  STOP_TALKBACK_CMD							  = "/stop_talk_back?";
	public static final String  TALKBACK_PARAM_1							  = "session_key=";
	public static final String  TALKBACK_PARAM_2							  = "&stream_id=";

	/*
	 * HTTP command : TO BE USE INCONJUNCTION WITH HTTP address for eg:
	 * "http://192.168.2.1:80" + HTTP_CMD_PART + GET_MELODY_VALUE;
	 */
	public static final String	HTTP_CMD_PART	                              = "/?action=command&command=";

	public static final String	DIR_FB_STOP	                                  = "fb_stop";
	public static final String	DIR_MOVE_FWD	                              = "move_forward";
	public static final String	DIR_MOVE_BWD	                              = "move_backward";

	public static final String	DIR_LR_STOP	                                  = "lr_stop";
	public static final String	DIR_MOVE_RIGHT	                              = "move_right";
	public static final String	DIR_MOVE_LEFT	                              = "move_left";

	public static final String	SET_RESOLUTION_VGA	                          = "VGA640_480";
	public static final String	SET_RESOLUTION_QVGA	                          = "QVGA";
	public static final String	SET_RESOLUTION_QQVGA	                      = "QQVGA160_120";

	public static final String	SET_RESOLUTION_CMD	                          = "set_resolution";	                                                                 // &mode=720p
	public static final String	SET_RESOLUTION_PARAM_1	                      = "&mode=";

	public static final String	RESOLUTION_480P	                              = "480p";
	public static final String	RESOLUTION_360P	                              = "360p";
	public static final String	RESOLUTION_720P	                              = "720p";

	public static final String	RESOLUTION_720P_10	                          = "720p_10";
	public static final String	RESOLUTION_720P_15	                          = "720p_15";
	public static final String	RESOLUTION_VGA	                              = "VGA640_480";
	public static final String	RESOLUTION_QVGA	                              = "QVGA";
	public static final String	RESOLUTION_720P_926	                          = "720p_926";
	public static final String	RESOLUTION_480P_926	                          = "480p_926";
	public static final String	RESOLUTION_360P_926	                          = "360p_926";
	// "720p" , "480p"
	public static final String	GET_RESOLUTION_CMD	                          = "get_resolution";

	public static final String	GET_MOTION_AREA_CMD	                          = "get_motion_area";
	public static final String	SET_MOTION_AREA_CMD	                          = "set_motion_area";
	public static final String	SET_MOTION_AREA_PARAM_1	                      = "&grid=";
	public static final String	SET_MOTION_AREA_PARAM_2	                      = "&zone=";

	public static final String	MOTION_ON_PARAM	                              = SET_MOTION_AREA_PARAM_1
	                                                                                  + "1x1"
	                                                                                  + SET_MOTION_AREA_PARAM_2
	                                                                                  + "00,";
	public static final String	MOTION_OFF_PARAM	                          = SET_MOTION_AREA_PARAM_1
	                                                                                  + "1x1"
	                                                                                  + SET_MOTION_AREA_PARAM_2;

	public static final String	SET_MOTION_SENSITIVITY_CMD	                  = "set_motion_sensitivity";
	public static final String	SET_MOTION_SENSITIVITY_PARAM	              = "&value=";
	public static final String	VALUE_MOTION_SENSITIVITY	                  = "value_motion_sensitivity";
	
	public static final String SET_ADAPTIVE_BITRATE							  = "set_adaptive_bitrate";
	public static final String SET_ADAPTIVE_BITRATE_PARAM					  = "&mode=";
	
	public static final String SET_VIDEO_BITRATE							  = "set_video_bitrate";
	public static final String SET_VIDEO_BITRATE_PARAM						  = "&value=";

	/*
	 * response: value_resolution: <number>
	 * 
	 * number = 0 : vga 1 : qvga 2 : qqvga
	 */

	/*
	 * GET /?action=appletvastream&remote_session=<Session Authentication
	 * Key-32bytes HEX string > HTTP/1.1
	 */
	public static final String	GET_AV_STREAM_CMD	                          = "/?action=appletvastream";
	public static final String	GET_A_STREAM_CMD	                          = "/?action=appletastream";

	public static final String	GET_FLV_STREAM_CMD	                          = "/?action=flvstream";
	public static final String	GET_FLV_STREAM_PARAM_1	                      = "&remote_session=";

	public static final String	GET_AV_STREAM_UDT_CMD	                      = "action=appletvastream";
	public static final String	GET_A_STREAM_UDT_CMD	                      = "action=appletastream";
	public static final String	GET_AV_STREAM_PARAM_1	                      = "&remote_session=";

	public static final String	GET_MELODY_VALUE	                          = "value_melody";
	public static final String	SET_MELODY	                                  = "melody";
	public static final String	SET_MELODY_OFF	                              = "melodystop";

	public static final String	GET_TEMP_VALUE	                              = "value_temperature";
	public static final String  GET_RUNNING_OS								  = "get_running_os";

	public static final String	GET_BRIGHTNESS_VALUE	                      = "value_brightness";
	public static final String	GET_CONTRAST_VALUE	                          = "value_contract";

	public static final String	GET_BRIGHTNESS_PLUS	                          = "brightness_plus";
	public static final String	GET_BRIGHTNESS_MINUS	                      = "brightness_minus";

	public static final String	GET_CONTRAST_PLUS	                          = "contrast_plus";
	public static final String	GET_CONTRAST_MINUS	                          = "contrast_minus";

	public static final String	SET_DEVICE_AUDIO_OFF	                          = "audio_out0";
	public static final String	SET_DEVICE_AUDIO_ON	                      = "audio_out1";

	public static final String	SET_LED_ON	                                  = "setup_led1";
	public static final String	SET_LED_OFF	                                  = "setup_led0";
	public static final String	GET_LED	                                      = "value_setupled";

	public static final String	VOX_GET_THRESHOLD	                          = "vox_get_threshold";
	public static final String	VOX_SET_THRESHOLD	                          = "vox_set_threshold";
	public static final String	VOX_SET_THRESHOLD_VALUE	                      = "&value=";

	public static final String	VOX_ENABLE	                                  = "vox_enable";
	public static final String	VOX_DISABLE	                                  = "vox_disable";
	public static final String	VOX_STATUS	                                  = "vox_get_status";

	public static final String	GET_MODEL	                                  = "get_model";
	public static final String	GET_CODECS_SUPPORT	                          = "get_codecs_support";
	public static final String	GET_VERSION	                                  = "get_version";
	public static final String	GET_MAC_ADDRESS	                              = "get_mac_address";
	public static final String	GET_UDID	                                  = "get_udid";
	public static final String	RESET_FACTORY	                              = "reset_factory";
	public static final String	RESTART_DEVICE	                              = "restart_system";

	public static final String	SET_VOLUME	                                  = "spk_volume";
	public static final String	GET_VOLUME	                                  = "get_spk_volume";
	public static final String	SET_VOLUME_PARAM_1	                          = "&setup=";

	public static final String	FLIP_IMAGE	                                  = "flipup";

	// http://192.168.2.1/?action=command&command=save_wifi_usr_passwd&setup=mot-cam:123456
	public static final String	BASIC_AUTH_USR_PWD_CHANGE	                  = "save_http_usr_passwd";
	public static final String	BASIC_AUTH_USR_PWD_CHANGE_PARAM_1	          = "&setup=";

	public static final String	SWITCH_TO_DIRECT_MODE	                      = "switch_to_uap";

	// ?action=command&command=check_upnp
	// 0 - not ok -> popup
	// 1 - ok
	// 2 - in progress
	public static final String	CHECK_UPNP	                                  = "check_upnp";
	public static final String	RESET_UPNP	                                  = "reset_upnp";

	// set_upnp_port&setup=Z
	public static final String	SET_UPNP_PORT	                              = "set_upnp_port";
	public static final String	SET_UPNP_PORT_PARAM_1	                      = "&setup=";
	public static final String	GET_UPNP_PORT	                              = "get_upnp_port";
	public static final String	GET_DEFAULT_VERSION	                          = "get_default_version";

	public static final String	CLOSE_UDT_SESSION	                          = "close_session";
	public static final String	CLOSE_RELAY_SESSION2	                      = "close_relay_session2";
	public static final String	CLOSE_RELAY_RTMP	                          = "close_relay_rtmp";
	
	public static final String  SET_TIME_ZONE								  = "set_time_zone";
	public static final String  SET_TIME_ZONE_PARAM							  = "&value=";

	public static final String	WIRELESS_SETUP_SAVE_RESPONSE	              = "setup_wireless_save: ";
	public static final String	SET_MASTER_KEY	                              = "set_master_key";
	public static final String	SET_MASTER_KEY_PARAM_1	                      = "&setup=";
	public static final String	RESTART_DEVICE_RESPONSE	                      = "restart_system: ";
	public static final String	MASTER_KEY_RESPONSE	                          = "set_master_key: ";
	public static final String	SET_TIME_ZONE_RESPONSE	                      = "set_time_zone: ";

	public static final String	GET_ROUTER_LIST	                              = "get_routers_list";
	public static final String	GET_RT_LIST	                              	  = "get_rt_list";

	public static final String	SET_TEMP_HI	                                  = "set_temp_hi_enable";
	public static final String	SET_TEMP_HI_OFF_PARAM	                      = "&value=0";
	public static final String	SET_TEMP_HI_ON_PARAM	                      = "&value=1";

	public static final String	GET_TEMP_HI_THRESHOLD	                      = "get_temp_hi_threshold";
	public static final String	SET_TEMP_HI_THRESHOLD	                      = "set_temp_hi_threshold";

	public static final String	SET_TEMP_LO	                                  = "set_temp_lo_enable";
	public static final String	GET_TEMP_LO_THRESHOLD	                      = "get_temp_lo_threshold";
	public static final String	SET_TEMP_LO_THRESHOLD	                      = "set_temp_lo_threshold";

	/*
	 * command: device_setting camera return: device_setting: ms=[motion
	 * status],me=[motion sensitivity],vs=[vox status],vt=[vox
	 * threshold],hs=[high temp detection status],ls=[high temp detection
	 * status],ht=[high temp threshold],lt=[low temp threshold]
	 * 
	 * For example: camera return: device_setting:
	 * ms=1,me=70,vs=1,vt=80,hs=0,ls=1,ht=30,lt=18
	 */
	public static final String	DEVICE_SETTING	                              = "device_setting";

	public static final String	SETUP_WIRELESS_HTTP_CMD	                      = DEFAULT_HTTP
	                                                                                  + "/?action=command&command=setup_wireless_save&setup=";

	public static final String	SETUP_READ_WIRELESS_HTTP_CMD	              = DEFAULT_HTTP
	                                                                                  + "/?action=command&command=setup_wireless_read";

	public static final String	RESTART_DEVICE_HTTP_CMD	                      = DEFAULT_HTTP
	                                                                                  + "/?action=command&command=restart_system";

	public static final String	RESTART_APP_HTTP_CMD	                      = DEFAULT_HTTP
	                                                                                  + "/?action=command&command=restart_app";

	public static final String	SET_MKEY_HTTP_CMD	                          = DEFAULT_HTTP
	                                                                                  + "/?action=command&command=set_master_key&setup=";

	/* User account HTTP command */
	/*
	 * Camera list
	 * monitoreverywhere.com/BMS/service?action=command&command=camera_list
	 * &email=<email>
	 */

	public static final String	UDT_SERVER	                                  = "udt.monitoreverywhere.com";
	public static final int	    UDT_SERVER_PORT	                              = 8000;	                                                                             // 7000;
	                                                                                                                                                                 // //secure
	                                                                                                                                                                 // server
	public static final String	RELAY_SERVER	                              = "relay.monitoreverywhere.com";	                                                     // UDT_SERVER;
	public static final String	RELAY_SERVER2	                              = "relay2.monitoreverywhere.com";	                                                 // new
	                                                                                                                                                                 // relay
	                                                                                                                                                                 // server
	public static final int	    RELAY_SERVER_PRT	                          = 44444;

	public static final String	BM_SERVER	                                  = "https://monitoreverywhere.com/BMS/phoneservice?";
	public static final String	BM_SERVER_CAM	                              = "https://monitoreverywhere.com/BMS/cameraservice?";
	public static final String	BM_SERVER_ADDR	                              = "http://monitoreverywhere.com/BMS";
	public static final String	GOOGLE_ADDR	                                  = "http://www.goolge.com";

	// simplimonitor for new relay streamer
	// public static final String BM_SERVER =
	// "https://simplimonitor.com/BMS/phoneservice?";
	// public static final String BM_SERVER_CAM =
	// "https://simplimonitor.com/BMS/cameraservice?";
	// public static final String BM_SERVER_ADDR =
	// "http://simplimonitor.com/BMS";

	public static final int	    BM_SERVER_CONNECTION_TIMEOUT	              = 10000;	                                                                             // 10sec
	public static final int	    BM_SERVER_READ_TIMEOUT	                      = 10000;	                                                                             // 10sec

	public static final String	BM_HTTP_CMD_PART	                          = "action=command&command=";
	public static final String	GET_CAM_LIST_CMD	                          = "camera_list";
	public static final String	GET_CAM_LIST4_CMD	                          = "camera_list4";
	public static final String	GET_CAM_LIST5_CMD	                          = "camera_list5";
	public static final String	GET_CAM_LIST_PARAM	                          = "&email=";

	/*
	 * monitoreverywhere.com/BMS/service?action=command&command=user_login&
	 * email=<email>&pass=<password>&device=<device>&app_type=<app_type>
	 */
	public static final String	USR_LOGIN_CMD	                              = "user_login";
	public static final String	USR_LOGIN_PARAM_1	                          = "&email=";
	public static final String	USR_LOGIN_PARAM_2	                          = "&pass=";
	public static final String	USR_LOGIN_PARAM_3	                          = "&device=";
	public static final String	USR_LOGIN_PARAM_4	                          = "&app_type=";
	public static final String	USR_LOGIN_PARAM_5	                          = "&pass_len=";

	/*
	 * monitoreverywhere.com/BMS/service?action=command&command=user_register&
	 * email=<email>&pass=<password>
	 */
	public static final String	USR_REG_CMD	                                  = "user_register";
	public static final String	USR_REG_PARAM_1	                              = "&email=";
	public static final String	USR_REG_PARAM_2	                              = "&pass=";
	public static final String	USR_REG_PARAM_3	                              = "&pass_len=";

	/*
	 * A different way to register..
	 * https://monitoreverywhere.com/BMS_n8/phoneservice?action=command&command=
	 * user_registration&email=<email>&pass=<pass>&username=<username>
	 */
	public static final String	USR_REG1_CMD	                              = "user_registration";
	public static final String	USR_REG1_PARAM_1	                          = "&email=";
	public static final String	USR_REG1_PARAM_2	                          = "&pass=";
	public static final String	USR_REG1_PARAM_3	                          = "&username=";

	/*
	 * monitoreverywhere.com/BMS/service?action=command&command=user_activation&
	 * email=<email>&key=<activationkey>
	 */
	public static final String	USR_ACT_CMD	                                  = "user_activation";
	public static final String	USR_ACT_PARAM_1	                              = "&email=";
	public static final String	USR_ACT_PARAM_2	                              = "&key=";

	/*
	 * monitoreverywhere.com/BMS/service?action=command&command=add_cam&
	 * email=<email>&pass=<password>&macaddress=<macaddress>&cam_name=<camera
	 * name>
	 */
	public static final String	ADD_CAM_CMD	                                  = "add_cam";
	public static final String	ADD_CAM_PARAM_1	                              = "&email=";
	public static final String	ADD_CAM_PARAM_2	                              = "&pass=";
	public static final String	ADD_CAM_PARAM_3	                              = "&macaddress=";
	public static final String	ADD_CAM_PARAM_4	                              = "&cam_name=";
	public static final String	ADD_CAM_PARAM_5	                              = "&pass_len=";
	public static final String	ADD_CAM_PARAM_6	                              = "&codec=";

	/*
	 * monitoreverywhere.com/BMS/service?action=command&command=delete_cam&
	 * email=< email>&mac=<mac>
	 */
	public static final String	DEL_CAM_CMD	                                  = "delete_cam";
	public static final String	DEL_CAM_PARAM_1	                              = "&email=";
	public static final String	DEL_CAM_PARAM_2	                              = "&macaddress=";
	/*
	 * 20130325: hoang: issue 926: Delete camera improvement change to new http
	 * command: monitoreverywhere.com/BMS/phoneservice?action=command&
	 * command=delete_reset_cam&macaddress=<mac>&email=<email>
	 */
	public static final String	DEL_RESET_CAM_CMD	                          = "delete_reset_cam";
	public static final String	DEL_RESET_CAM_PARAM_1	                      = "&macaddress=";
	public static final String	DEL_RESET_CAM_PARAM_2	                      = "&email=";

	/*
	 * Request To view
	 * /?action=command&command=view_cam&email=<user_email>&mac=<device_mac>
	 */
	public static final String	VIEW_CAM_CMD	                              = "view_cam";
	public static final String	VIEW_CAM_PARAM_1	                          = "&email=";
	public static final String	VIEW_CAM_PARAM_2	                          = "&macaddress=";

	/*
	 * Reset Password
	 * /phoneservice?action=command&command=reset_password&email=<email_id>
	 */

	public static final String	RESET_PWD_CMD	                              = "reset_password";
	public static final String	RESET_PWD_PARAM_1	                          = "&email=";

	/*
	 * Query Snapshot
	 */

	public static final String	GET_IMG_CMD	                                  = "get_image";
	public static final String	GET_IMG_PARAM_1	                              = "&macaddress=";

	/*
	 * Query ports
	 */
	public static final String	GET_PORT_CMD	                              = "get_port_info";
	public static final String	GET_PORT_PARAM_1	                          = "&macaddress=";

	/*
	 * Update Camera Name
	 */
	public static final String	UPDATE_CAM_CMD	                              = "update_user_cam";
	public static final String	UPDATE_CAM_PARAM_1	                          = "&email=";
	public static final String	UPDATE_CAM_PARAM_2	                          = "&macaddress=";
	public static final String	UPDATE_CAM_PARAM_3	                          = "&cam_name=";

	/*
	 * Get Stream Mode
	 */

	public static final String	GET_STREAM_MODE_CMD	                          = "get_stream_mode";
	public static final String	GET_STREAM_MODE_PARAM_1	                      = "&mac=";

	/*
	 * get_channelid
	 * http://monitoreverywhere.com/BMS28/phoneservice?action=command
	 * &command=get_channelid
	 */
	public static final String	GET_CHANNEL_ID_CMD	                          = "get_channelid";

	/*
	 * is port open TO USED WITH cameraservice ONLY
	 */
	public static final String	IS_PORT_OPEN	                              = "is_port_open";
	public static final String	IS_PORT_OPEN_PARAM_1	                      = "&mac=";
	public static final String	IS_PORT_OPEN_PARAM_2	                      = "&port=";

	/*
	 * 
	 * For channelID & secretKey(cipher key) : Request :
	 * phoneservice?action=command
	 * &command=get_security_info&email=<email>&pass=<passward> Response :
	 * ChannelID & secretKey
	 * 
	 * App to UDT server Over UDT : Request :
	 * concat(encrypt(mac),encrypt(channelID)) Response : Encrypt(Output of the
	 * cam_view_udp)
	 */

	public static final String	GET_SECURITY_INFO	                          = "get_security_info";
	public static final String	GET_SECURITY_INFO_PARAM_1	                  = "&email=";
	public static final String	GET_SECURITY_INFO_PARAM_2	                  = "&pass=";

	/*
	 * phoneservice?action=command&command=is_cam_available&macaddress=<mac>
	 * 
	 * -AVAIL : 200 HTTP response code -BUSY : 614 HTTP response code
	 */

	public static final String	IS_CAM_AVAIL	                              = "is_cam_available";
	public static final String	IS_CAM_AVIL_PARAM_1	                          = "&macaddress=";

	/*
	 * phoneservice?action=command&command=get_relaysec_info&macaddress=<mac>
	 */
	public static final String	GET_RELAY_SECURITY	                          = "get_relaysec_info";
	public static final String	GET_RELAY_SECURITY_PARAM_1	                  = "&macaddress=";

	/*
	 * 
	 * https://monitoreverywhere.com/BMS2.12/phoneservice?action=command&command=
	 * register_gcm_device &email=<email>&registrationId=<registrationId>
	 * https:/
	 * /monitoreverywhere.com/BMS2.12/phoneservice?action=command&command=
	 * unregister_gcm_device &registrationId=<registrationId>
	 * 
	 * 699 - unhandled case 200- success 602 - parameter missing
	 * 
	 * - HTTP OK 200 - OK - 1. User without prior regId: new regID added for
	 * user 2. User with a regId: re-register this ID for user (overwrite the
	 * old regId) 3. re-register same id with same user
	 */
	public static final String	PUSH_REGISTER	                              = "register_gcm_device";
	public static final String	PUSH_REGISTER_PARAM_1	                      = "&email=";
	public static final String	PUSH_REGISTER_PARAM_2	                      = "&registrationId=";

	/*
	 * - HTTP OK 200 - OK - Unregister OK 801(or other code) - OK - No regId
	 * correspond with the USER
	 */
	public static final String	PUSH_UNREGISTER	                              = "unregister_gcm_device";
	public static final String	PUSH_UNREGISTER_PARAM_1	                      = "&registrationId=";

	/*
	 * OLD ---- DONT USE Command to send query to camera VIA STUN
	 */
	public static final String	_SEND_CTRL_CMD	                              = "send_control_command";
	public static final String	_SEND_CTRL_PARAM_1	                          = "&macaddress=";
	public static final String	_SEND_CTRL_PARAM_2	                          = "&channelid=";
	public static final String	_SEND_CTRL_PARAM_3	                          = "&query=";

	/*
	 * 
	 * Command to send query to camera VIA STUN
	 */
	public static final String	SEND_CTRL_CMD	                              = "send_stun_command";
	public static final String	SEND_CTRL_PARAM_1	                          = "&macaddress=";
	public static final String	SEND_CTRL_PARAM_2	                          = "&channelid=";
	public static final String	SEND_CTRL_PARAM_3	                          = "&query=";

	/*
	 * /phoneservice?action=command&command=enable_notifications_gcm&registrationId
	 * =<registrationid>&mac=<mac>
	 * /phoneservice?action=command&command=disable_notifications_gcm
	 * &registrationId=<registrationid>&mac=<mac>
	 */
	public static final String	ENABLE_NOTIFICATIONS_CMD	                  = "enable_notifications_gcm";
	public static final String	DISABLE_NOTIFICATIONS_CMD	                  = "disable_notifications_gcm";
	public static final String	ENABLE_NOTIFICATIONS_PARAM_1	              = "&registrationId=";
	public static final String	ENABLE_NOTIFICATIONS_PARAM_2	              = "&mac=";
	public static final String	ENABLE_NOTIFICATIONS_PARAM_3	              = "&alert=";

	/*
	 * /phoneservice?action=command&command=get_disabled_alerts&registrationId=<
	 * registrationid>&mac=<mac> response: <br>mac=[mac address]
	 * <br>cameraname=[camera name] <br>Total_disabled_alerts=[count]
	 * <br>alert=<alert> <br>alert=<alert> <br>alert=<alert>
	 */

	public static final String	GET_DISABLED_NOTIFICATIONS_CMD	              = "get_disabled_alerts_gcm";
	public static final String	GET_DISABLED_NOTIFICATIONS_PARAM_1	          = "&registrationId=";
	public static final String	GET_DISABLED_NOTIFICATIONS_PARAM_2	          = "&mac=";

	/*
	 * /phoneservice?action=command&command=enable_notifications&username=<username
	 * >&mac=<mac>&alert=<alert>
	 * /phoneservice?action=command&command=disable_notifications
	 * &username=<username>&mac=<mac>&alert=<alert>
	 * /phoneservice?action=command&
	 * command=get_disabled_alerts&username=<username>&mac=<mac>
	 */

	public static final String	ENABLE_NOTIFICATIONS_U_CMD	                  = "enable_notifications";
	public static final String	DISABLE_NOTIFICATIONS_U_CMD	                  = "disable_notifications";
	public static final String	ENABLE_NOTIFICATIONS_U_PARAM_1	              = "&username=";
	public static final String	ENABLE_NOTIFICATIONS_U_PARAM_2	              = "&mac=";
	public static final String	ENABLE_NOTIFICATIONS_U_PARAM_3	              = "&alert=";

	public static final String	GET_DISABLED_NOTIFICATIONS_U_CMD	          = "get_disabled_alerts";
	public static final String	GET_DISABLED_NOTIFICATIONS_U_PARAM_1	      = "&username=";
	public static final String	GET_DISABLED_NOTIFICATIONS_U_PARAM_2	      = "&mac=";

	/*
	 * Get video format
	 */
	public static final String	GET_VIDEO_FORMAT	                          = "get_video_format";

	/*
	 * Check camera availability IS_CAM_AVAILABLE_ONLOAD_ - udt cameras?
	 * IS_CAM_AVAILABLE_UPNP_CMD - upnp cameras?
	 * 
	 * response: <Cam_Mac_NO_COLON>: <Status> <status> could be one of these
	 * string: AVAILABLE : camera is up & running ERROR: timeout in checking or
	 * other error (however it takes quite a long time to show this) BUSY:
	 * camera is busy - someone is viewing..
	 * 
	 * eg: 000DA3120583: AVAILABLE
	 */

	public static final String	IS_CAM_AVAILABLE_ONLOAD_CMD	                  = "is_cam_available_onload";
	public static final String	IS_CAM_AVAILABLE_ONLOAD_PARAM_1	              = "&macaddress=";

	public static final String	IS_CAM_AVAILABLE_ONLOAD2_CMD	              = "is_cam_available_onload2";
	public static final String	IS_CAM_AVAILABLE_ONLOAD2_PARAM_1	          = "&macaddress=";

	public static final String	IS_CAM_AVAILABLE_UPNP_CMD	                  = "is_cam_available_upnp";
	public static final String	IS_CAM_AVAILABLE_UPNP_CMD_PARAM_1	          = "&macaddress=";

	/*
	 * View cam relay
	 * https://www.monitoreverywhere.com/BMS/phoneservice?action=command
	 * &command=view_cam_relay& macaddress=<mac_address>&email=<email>
	 */
	public static final String	VIEW_CAM_RELAY_CMD	                          = "view_cam_relay";
	public static final String	VIEW_CAM_RELAY_PARAM_1	                      = "&macaddress=";
	public static final String	VIEW_CAM_RELAY_PARAM_2	                      = "&email=";

	/*
	 * View cam through Wowza server command
	 * https://www.monitoreverywhere.com/BMS
	 * /phoneservice?action=command&command=view_cam_h264& macaddress=&email=
	 */
	public static final String	VIEW_CAM_H264_CMD	                          = "view_cam_h264";
	public static final String	VIEW_CAM_H264_PARAM_1	                      = "&macaddress=";
	public static final String	VIEW_CAM_H264_PARAM_2	                      = "&email=";

	public static final String	SET_RECORDING_STAT_CMD	                      = "set_recording_stat";
	public static final String	SET_RECORDING_STAT_PARAM_1	                  = "&mode=";
	public static final String	RECORDING_STAT_MODE_ON	                      = "on";
	public static final String	RECORDING_STAT_MODE_OFF	                      = "off";

	public static final String	GET_RECORDING_STAT_CMD	                      = "get_recording_stat";

	public static final boolean	start_to_use	                              = true;

	public static String strip_colon_from_mac(String mac_with_colon)
	{
		if (start_to_use == false)
		{
			return mac_with_colon;
		}

		String mac_no_colon = "";
		StringTokenizer stringTok = new StringTokenizer(mac_with_colon, ":");
		if (stringTok.countTokens() != 6)
		{
			Log.e("mbp", "strip_colon_from_mac-ERROR mac:" + mac_with_colon);
		}
		else
		{
			while (stringTok.hasMoreElements())
			{
				mac_no_colon += stringTok.nextToken();
			}
		}

		return mac_no_colon;

	}

	public static String add_colon_to_mac(String mac_no_colon)
	{
		if (start_to_use == false)
		{
			return mac_no_colon;
		}
		String mac_with_colon = "";
		if (mac_no_colon.length() != 12)
		{
			Log.e("mbp", "add_colon_to_mac-ERROR mac:" + mac_no_colon);
		}
		else
		{
			mac_with_colon = mac_no_colon.substring(0, 2) + ":"
			        + mac_no_colon.substring(2, 4) + ":"
			        + mac_no_colon.substring(4, 6) + ":"
			        + mac_no_colon.substring(6, 8) + ":"
			        + mac_no_colon.substring(8, 10) + ":"
			        + mac_no_colon.substring(10);
		}

		return mac_with_colon;

	}

	public static String getSDPFilePath(Context c, String resolution)
	{
		if (resolution != null)
		{
			try
			{
				InputStream inputStream = null;
				if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_720P_10)
				        || resolution
				                .equalsIgnoreCase(PublicDefine.RESOLUTION_720P_15))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.stream720p);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_480P))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.stream480p);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_720P_926))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.blink11hd720p);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_480P_926))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.blink11hd480p);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_360P_926))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.blink11hd360p);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_VGA))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.streamvga);
				}
				else if (resolution
				        .equalsIgnoreCase(PublicDefine.RESOLUTION_QVGA))
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.streamqvga);
				}
				else
				{
					inputStream = c.getResources().openRawResource(
					        R.raw.streamqvga);
				}

				File f = new File(c.getFilesDir() + File.separator
				        + "stream.sdp");
				OutputStream outputStream = new FileOutputStream(f);
				byte buffer[] = new byte[1024];
				int length = 0;

				while ((length = inputStream.read(buffer)) > 0)
				{
					outputStream.write(buffer, 0, length);
				}

				outputStream.close();
				inputStream.close();

				return f.getAbsolutePath();
			}
			catch (IOException e)
			{
				// Logging exception
				e.printStackTrace();
			}
		}

		return null;
	}

	public static String get_error_description(Context c, int bms_error_code)
	{
		String result = null;
		if (c == null)
		{
			result = "Unknown server error";
			switch (bms_error_code)
			{
			case 119:
				result = "Camera not connected. Either the camera is switched off or this could be because of the low/weak internet connection. Please try in a while again.";
				break;
			case 120:
				result = "Insufficient parameters passed.";
				break;
			case 121:
				result = "bind exception";
				break;
			case 122:
				result = "local discovery failure.";
				break;
			case 123:
				result = "Audio plugin is disabled on your system.";
				break;
			case 124:
				result = "camera initial and control commands have some problem.";
				break;
			case 400:
				result = "Invalid parameters received.";
				break;
			case 401:
				result = "Looks like the registration for this camera was not completed. You need delete this camera and add again.";
				break;
			case 404:
				result = "Server is temporarily not available";
				break;
			case 505:
				result = "Username cannot have space and special character";
				break;
			case 601:
				result = "Invalid command passed. Please check the query.";
				break;
			case 602:
				result = "Required parameter(s) are missing. Please check the query.";
				break;
			case 603:
				result = "Length of the parameter is out of expected boundaries. Please check the query.";
				break;
			case 604:
				result = "Camera is busy or not available.";
				break;
			case 606:
				result = "Unable to send email. Email format is invalid.";
				break;
			case 611:
				result = "Camera does not exist. Please check the query.";
				break;
			case 612:
				result = "We are having problems to access your camera currently. Your action is required. Because of your network configuration - manual configuration might be required. Please visit FAQ to fix this issue.";
				break;
			case 613:
				result = "Cannot communicate with camera. Either IP Address or the port information is unavailable.";
				break;
			case 614:
				result = "Your camera is being accessed by other user currently and not available for streaming. Please try in a while again.";
				break;
			case 621:
				result = "User credentials entered are incorrect.";
				break;
			case 622:
				result = "Email Id is registered but not activated.";
				break;
			case 623:
				result = "Email Id is already activated.";
				break;
			case 624:
				result = "Activation failed. Either user is not registered or the activation period is expired. Please register again.";
				break;
			case 625:
				result = "Activation failed. Invalid activation key.";
				break;
			case 626:
				result = "Username or Password entered is incorrect.";
				break;
			case 627:
				result = "Camera is not associated with any user (email id).";
				break;
			case 628:
				result = "User registration failed. Email is already registered.";
				break;
			case 629:
				result = "Streaming port is closed on camera. Please restart the camera and try again.";
				break;
			case 630:
				result = "Requested image does not exist.";
				break;
			case 631:
				result = "Port forwarding needs to be done again, please contact administrator.";
				break;
			case 632:
				result = "We are having problems to access your camera currently. This could be because of the internet connection. Please try in a while again.";
				break;
			case 636:
				result = "User registration failed. Username is already registered.";
				break;
			case 699:
				result = "Unhandled exception occured, please contact administrator.";
				break;
			case 701:
				result = "Camera not connected. Either the camera is switched off or this could be because of the low/weak internet connection. Please try in a while again.";
				break;
			case 702:
				result = "Camera is busy or not available, Please try again";
				break;
			case 703:
				result = "Unable to communicate with camera, Please try again";
				break;
			case 704:
				result = "Unable to communicate with server, Please try again";
				break;
			case 705:
				result = "Unable to communicate with server, Please try again";
				break;
			case 706:
				result = "Server not reachable or not started, Please try again";
				break;
			case 707:
				result = "Can not connect to camera, please make sure that it is turned on.";
				break;
			case 708:
				result = "Your previous request is already in process, please wait...";
				break;
			case 709:
				result = "Camera not connected. Either the camera is switched off or this could be because of the low/weak internet connection. Please try in a while again.";
				break;
			case 851:
				result = "Stun failure response from camera for start stream request.";
				break;
			case 901:
				result = "Server is busy,Please try after sometime.";
				break;
			case 902:
				result = "Network failure,Please reload the page.";
				break;
			case 903:
				result = "Relay Server failed to connect to camera, please try again.";
				break;
			case 904:
				result = "Relay Server is busy, Please Reload the page.";
				break;
			case 905:
				result = "Relay Server is down, Please reload after some time.";
				break;
			case 906:
				result = "There is a possible Symmetric NAT connection in your network.";
				break;
			case 907:
				result = "Problem in getting Relay server location. Please try again.";
				break;
			case 908:
				result = "Problem in getting relay secret key.";
				break;
			case 12007:
				result = "Internet not accessible! Check your network connection! ";
				break;
			case 12029:
				result = "Internet not accessible! Check your network connection! ";
				break;
			default:
				result = "Unknown server error - " + bms_error_code;
				break;

			}

		}
		else
		{
			result = c.getString(R.string.unknown_error);
			switch (bms_error_code)
			{
			case 119:
				result = c.getString(R.string.bms_http_err_119);
				break;
			case 120:
				result = c.getString(R.string.bms_http_err_120);
				break;
			case 121:
				result = c.getString(R.string.bms_http_err_121);
				break;
			case 122:
				result = c.getString(R.string.bms_http_err_122);
				break;
			case 123:
				result = c.getString(R.string.bms_http_err_123);
				break;
			case 124:
				result = c.getString(R.string.bms_http_err_124);
				break;
			case 400:
				result = c.getString(R.string.bms_http_err_400);
				break;
			case 401:
				result = c.getString(R.string.bms_http_err_401);
				break;
			case 404:
				result = c
				        .getString(R.string.server_is_temporarily_not_available);
				break;
			case 505:
				result = c.getString(R.string.bms_http_err_505);
				break;
			case 601:
				result = c.getString(R.string.bms_http_err_601);
				break;
			case 602:
				result = c.getString(R.string.bms_http_err_602);
				break;
			case 603:
				result = c.getString(R.string.bms_http_err_603);
				break;
			case 604:
				result = c.getString(R.string.bms_http_err_604);
				break;
			case 611:
				result = c.getString(R.string.bms_http_err_611);
				break;
			case 612:
				result = c.getString(R.string.bms_http_err_612);
				break;
			case 613:
				result = c.getString(R.string.bms_http_err_613);
				break;
			case 614:
				result = c.getString(R.string.bms_http_err_614);
				break;
			case 621:
				result = c.getString(R.string.bms_http_err_621);
				break;
			case 622:
				result = c.getString(R.string.bms_http_err_622);
				break;
			case 623:
				result = c.getString(R.string.bms_http_err_623);
				break;
			case 624:
				result = c.getString(R.string.bms_http_err_624);
				break;
			case 625:
				result = c.getString(R.string.bms_http_err_625);
				break;
			case 626:
				result = c.getString(R.string.bms_http_err_626);
				break;
			case 627:
				result = c.getString(R.string.bms_http_err_627);
				break;
			case 628:
				result = c.getString(R.string.bms_http_err_628);
				break;
			case 629:
				result = c.getString(R.string.bms_http_err_629);
				break;
			case 630:
				result = c.getString(R.string.bms_http_err_630);
				break;
			case 631:
				result = c.getString(R.string.bms_http_err_631);
				break;
			case 632:
				result = c.getString(R.string.bms_http_err_632);
				break;
			case 636:
				result = c.getString(R.string.username_is_already_registered);
				break;
			case 699:
				result = c.getString(R.string.bms_http_err_699);
				break;
			case 701:
				result = c.getString(R.string.bms_http_err_701);
				break;
			case 702:
				result = c.getString(R.string.bms_http_err_702);
				break;
			case 703:
				result = c.getString(R.string.bms_http_err_703);
				break;
			case 704:
				result = c.getString(R.string.bms_http_err_704);
				break;
			case 705:
				result = c.getString(R.string.bms_http_err_705);
				break;
			case 706:
				result = c.getString(R.string.bms_http_err_706);
				break;
			case 707:
				result = c.getString(R.string.bms_http_err_707);
				break;
			case 708:
				result = c.getString(R.string.bms_http_err_708);
				break;
			case 709:
				result = c.getString(R.string.bms_http_err_709);
				break;
			case 851:
				result = c.getString(R.string.bms_http_err_851);
				break;
			case 901:
				result = c.getString(R.string.bms_http_err_901);
				break;
			case 902:
				result = c.getString(R.string.bms_http_err_902);
				break;
			case 903:
				result = c.getString(R.string.bms_http_err_903);
				break;
			case 904:
				result = c.getString(R.string.bms_http_err_904);
				break;
			case 905:
				result = c.getString(R.string.bms_http_err_905);
				break;
			case 906:
				result = c.getString(R.string.bms_http_err_906);
				break;
			case 907:
				result = c.getString(R.string.bms_http_err_907);
				break;
			case 908:
				result = c.getString(R.string.bms_http_err_908);
				break;
			case 12007:
				result = c.getString(R.string.bms_http_err_12007);
				break;
			case 12029:
				result = c.getString(R.string.bms_http_err_12029);
				break;
			default:
				result = c.getString(R.string.bms_http_err_unknown)
				        + bms_error_code;
				break;

			}
		}

		return result;
	}

	public static String capitalizeFirstCharacter(String str)
	{
		String convertedStr = null;
		if (str != null)
		{
			try
			{
				convertedStr = str.replaceAll("\\s+", " ");
				if (convertedStr != null)
				{
					String[] split = convertedStr.split(" ");
					convertedStr = "";
					for (int i = 0; i < split.length; i++)
					{
						split[i] = Character.toUpperCase(split[i].charAt(0))
						        + split[i].substring(1);
						convertedStr += split[i] + " ";
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return convertedStr;
	}

}