package com.msc3;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Hashtable;
import java.net.URLConnection;

import android.util.Log;

public class StreamSplit {
    public static final String BOUNDARY_MARKER_PREFIX  = "--";
    public static final String BOUNDARY_MARKER_TERM    = BOUNDARY_MARKER_PREFIX;
    public static final String STUN_SESSION_KEY_INVALID_601 = "601";
    public static final String STUN_SESSION_KEY_INVALID_401 = "401";

	protected DataInputStream m_dis;
	private boolean m_streamEnd;
	private String boundary;


	public StreamSplit(DataInputStream dis)
	{
		m_dis = dis;
		m_streamEnd = false;
		this.boundary = null;
	}

	public void setBoundary (String boundary)
	{
		this.boundary = boundary;
	}
	public String getBoundary ()
	{
		return this.boundary;
	}

	public Hashtable<String, String> readHeaders() throws StunInvalidSskeyException
	{
		Hashtable<String, String> ht = new Hashtable<String, String>();
		String response = null;
		boolean satisfied = false;

		try 
		{
			do {
				response = m_dis.readLine();
				if (response == null) {
					m_streamEnd = true;
					break;
				} else if (response.equals("")) {
	                if (satisfied) {
					    break;
	                } else {
	                    // Carry on...
	                }
				} else {
					//Log.d("mbp", "UDT header is: " +response);
					if (response.startsWith(STUN_SESSION_KEY_INVALID_601) ||
							response.startsWith(STUN_SESSION_KEY_INVALID_401))
					{
						throw new StunInvalidSskeyException(VideoStreamer.SESSION_KEY_MISMATCHED);
					}
	                satisfied = true;
	            }
	            addPropValue(response, ht);
	        } while (true);
			
		}
		catch (StunInvalidSskeyException soe)
		{
			throw soe;
		}
		catch (IOException ioe)
		{
			ht = null; 
		}
		
		return ht;
	}

    protected static void addPropValue(String response, Hashtable<String, String> ht)
    {
        int idx = response.indexOf(":");
        if (idx == -1) {
            return;
        }
        String tag = response.substring(0, idx);
        String val = response.substring(idx + 1).trim();
        ht.put(tag.toLowerCase(), val);
    }

    public static Hashtable<String, String> readHeaders(DataInputStream dis) throws IOException
    {
    	Hashtable<String, String> ht = new Hashtable<String, String>();
		String response = null;
		boolean satisfied = false;

		do {
			response = dis.readLine();
			if (response == null) {
				break;
			} else if (response.equals("")) {
                if (satisfied) {
				    break;
                } else {
                    // Carry on...
                }
			} else {
                satisfied = true;
            }
            addPropValue(response, ht);
        } while (true);
		
		return ht;
    }

    public static Hashtable<String, String> readHeaders(URLConnection conn)
    {
        Hashtable<String, String> ht = new Hashtable<String, String>();
        int i = 0;
        do {
            String key = conn.getHeaderFieldKey(i);
            if (key == null) {
                if (i == 0) {
                    i++;
                    continue;
                } else {
                    break;
                }
            }
            String val = conn.getHeaderField(i);
            ht.put(key.toLowerCase(), val);
            
            i++;
        } while (true);
        return ht;
    }


	public void skipToBoundary() throws IOException
	{
		readToBoundary(boundary);
	}

	/* This function is call to get Data part from HTTP responses 
	 * According to the protocol document, there is no other boundaries inside
	 * data part until the end of the stream.
	 * @boundary: boundary string
	 * @outBuffer: buffer to be filled with data 
	 * @data_len: number of bytes to be read 
	 * 
	 * return : -1 if no more data 
	 */
	private static final int READ_RETRY_COUNT = 1000;
	public int  readDataToBoundary(String boundary, byte[] outBuffer, int data_len) throws IOException
	{
		int byteRead = -1;
		int accumulated_data_len = 0;
		int retry = 0;
		try {
			
			byteRead = m_dis.read(outBuffer,0, data_len);
			
			accumulated_data_len = byteRead;
			/* if byteRead < data_len : we maybe faster than the device
			 *  -> continue to fillup the outBuffer until one of the 2 condition below
			 *  1. byteRead == data_len
			 *  2. we hit the boundary strings (i dont like this concept so.. forget it for now)
			 *  3. we hit the re-try count - we can't wait here forever 
			 */
			
			while  (accumulated_data_len < data_len)
			{
				Thread.sleep(10);
				byteRead = m_dis.read(outBuffer,accumulated_data_len, data_len - accumulated_data_len );
				
				accumulated_data_len += byteRead;
				
				if (retry++ > READ_RETRY_COUNT)
				{
					
					break;
				}
			}
			
			
			
		} catch (EOFException e) {
			Log.d("mbp", "readDataToBoundary-EOFexception: " + e.toString());
			m_streamEnd = true;
		} catch (Exception e)
		{
			m_streamEnd = true;
			
			Log.d("mbp", "retry: " + retry + " readDataToBoundary--exception: " + e.toString());
		}
		return accumulated_data_len;
	}

	public byte[] readToBoundary(String boundary) throws IOException
	{
		ResizableByteArrayOutputStream baos = new ResizableByteArrayOutputStream();
		StringBuffer lastLine = new StringBuffer();
		int lineidx = 0;
		int chidx = 0;
		byte ch;
		do {
			try {
				ch = m_dis.readByte();
			} catch (EOFException e) {
				m_streamEnd = true;
				break;
			}
			if (ch == '\n' || ch == '\r') {
				//
				// End of line... Note, this will now look for the boundary
                // within the line - more flexible as it can habdle
                // arfle--boundary\n  as well as
                // arfle\n--boundary\n
				//
				String lls = lastLine.toString();
                int idx = lls.indexOf(BOUNDARY_MARKER_PREFIX);
                if (idx != -1) {
                    lls = lastLine.substring(idx);
                    if (lls.startsWith(boundary)) {
                        //
                        // Boundary found - check for termination
                        //
                        String btest = lls.substring(boundary.length());
                        if (btest.equals(BOUNDARY_MARKER_TERM)) {
                            m_streamEnd = true;
                        }
                        chidx = lineidx+idx;
                        break;
                    }
				}
				lastLine = new StringBuffer();
				lineidx = chidx + 1;
			} else {
				lastLine.append((char) ch);
			}
			chidx++;
			baos.write(ch);
		} while (true);
		//
		baos.close();
		baos.resize(chidx);
		return baos.toByteArray();
	}


	public boolean isAtStreamEnd()
	{
		return m_streamEnd;
	}
}


class ResizableByteArrayOutputStream extends ByteArrayOutputStream {
	public ResizableByteArrayOutputStream()
	{
		super();
	}


	public void resize(int size)
	{
		count = size;
	}
}
