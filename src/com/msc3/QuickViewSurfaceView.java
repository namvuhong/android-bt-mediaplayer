package com.msc3;

import com.blinkhd.R;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Hashtable;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class QuickViewSurfaceView extends SurfaceView 
				implements SurfaceHolder.Callback,Handler.Callback {

	public static final int MSG_LOAD_VIDEO_ERR = 0xCAFEBABE;
	public static final int MSG_LOAD_VIDEO_SUCCESS = 0xCAFECAFE;
	
	private SurfaceHolder mSurfaceHolder;

	private Thread quickView ;
	private MiniStreamer miniStream;
	private CamChannel mChannel;
	private Context mContext;
	private boolean loadVideoSucceeded ;
	

	

	public QuickViewSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
		mContext = context;
	}
	public QuickViewSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
		mContext = context;
	}

	public QuickViewSurfaceView(Context context) {
		super(context);
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
		mContext = context;

	}

	public void setCamChannel(CamChannel ch)
	{
		
		mChannel = ch;
		
	}
	
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

		//Log.w("mbp","surface change: " + width + " "  + height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if ( mChannel == null)
		{
			Log.w("mbp","QuickView No camera channel set");
			return;
		}
		
		miniStream = new MiniStreamer(mContext,new Handler(this),
				                      mSurfaceHolder,mChannel);
		quickView = new Thread(miniStream, "mini Streamer");

		loadVideoSucceeded = true; // assume success first
		miniStream.setRunning(true);
		quickView.start();
		
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;

		/* synchronize closing with worker thread*/
		if ( miniStream != null)
		{
			miniStream.setRunning(false);
			while (retry) {
				try {
					Log.w("mbp","wait for close");
					quickView.join(10000);

					retry = false;
				} catch (InterruptedException e) {
					// we will try it again and again...
				}
			}
		}
	}
	
	public boolean loadVideoSuccess()
	{
		return loadVideoSucceeded;
	}
	@Override
	public boolean handleMessage(Message msg) {
		
		switch (msg.what)
		{
		case MSG_LOAD_VIDEO_ERR:
			loadVideoSucceeded = false;
			break;
		case MSG_LOAD_VIDEO_SUCCESS:
			loadVideoSucceeded = true;
		default:
			break;
		}
		return false;
	}
	

	
}
class MiniStreamer implements Runnable {

	private SurfaceHolder mSurfaceHolder;
	private boolean isRunning ;
	private CamChannel mCamera;
	private Context mContext;
	private Handler mHandler;//Report Error 

	public MiniStreamer (Context c,Handler h, SurfaceHolder sh, CamChannel cam)
	{
		mSurfaceHolder = sh;
		isRunning = false;
		mCamera = cam;
		mContext = c;
		mHandler = h;
	}

	public void setRunning(boolean t)
	{
		isRunning = t;
	}
	
	
	
	private void reportError()
	{
		setDummyIcon();
		mHandler.dispatchMessage(
				Message.obtain(mHandler, QuickViewSurfaceView.MSG_LOAD_VIDEO_ERR));
	}
	
	
	private void setDummyIcon()
	{
		Canvas c = null;
		StaticLayout textLayout = null;
		TextPaint textPaint = new TextPaint();
		textPaint.setColor(Color.WHITE);
		textPaint.setTextSize(14);
		textPaint.setAntiAlias(true);
		Bitmap b = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.video_error);
		
		if (mSurfaceHolder == null) {
			Log.w("mbp","_surfaceHolder is NULL");
			return;
		}

		try {
			c = mSurfaceHolder.lockCanvas(null);
			if (c == null) {
				Log.w("mbp","canvas is NULL");
				return;
			}

			synchronized (mSurfaceHolder) {
				
				
				/* Draw the text */
				textLayout = new StaticLayout("Video not available,please go to Setup and verify", 
						textPaint, c.getWidth(), Alignment.ALIGN_CENTER, 1, 0, false);
				textLayout.draw(c);
				int shiftright, shiftdown; 
				shiftdown = textLayout.getHeight();
				shiftright = c.getWidth()/2 - b.getWidth()/2 ;
				c.drawBitmap(b,shiftright,shiftdown,null);
				
				
			}
		} finally {
			// do this in a finally so that if an exception is thrown
			// during the above, we don't leave the Surface in an
			// inconsistent state
			if (c != null) {
				mSurfaceHolder.unlockCanvasAndPost(c);
			}
		}
		
		
	}

	public void run()
	{
		String http_addr = null;

		if (mCamera == null)
		{
			mHandler.dispatchMessage(
					Message.obtain(mHandler, QuickViewSurfaceView.MSG_LOAD_VIDEO_ERR));
			return ;
		}
		http_addr = "http://"+ mCamera.getCamProfile().get_inetAddress().getHostAddress() + ":" +
				mCamera.getCamProfile().get_port();
		String http_cmd = http_addr + PublicDefine.HTTP_CMD_PART + PublicDefine.SET_RESOLUTION_QQVGA;


		URL url = null;
		URLConnection conn = null;
		DataInputStream inputStream = null;
		Hashtable headers = null;
		StreamSplit ssplit = null;


		try {
			/*send QQVGA cmd */
			url = new URL(http_cmd);
			conn = url.openConnection();
			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream()));
			
			
			url = new URL( http_addr +"/?action=stream");
			conn = url.openConnection();
			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			Log.d("mbp", "ERR add:" + mCamera.getCamProfile().get_inetAddress().getHostAddress());
			isRunning = false;
			e.printStackTrace();
			reportError();
			return;
			
		}

		headers = StreamSplit.readHeaders(conn);
		ssplit = new StreamSplit(inputStream);

		/*---- Find the boundary String */
		String ctype = (String) headers.get("content-type");

		int bidx = ctype.indexOf("boundary=");
		String boundary = StreamSplit.BOUNDARY_MARKER_PREFIX;
		if (bidx != -1) {
			boundary = ctype.substring(bidx + 9);
			ctype = ctype.substring(0, bidx);
			if (boundary.startsWith("\"") && boundary.endsWith("\""))
			{
				boundary = boundary.substring(1, boundary.length()-1);
			}
			if (!boundary.startsWith(StreamSplit.BOUNDARY_MARKER_PREFIX)) {
				boundary = StreamSplit.BOUNDARY_MARKER_PREFIX + boundary;
			}

			ssplit.setBoundary(boundary);
		}

		if (ctype.startsWith("multipart/x-mixed-replace")) {
			try {
				ssplit.skipToBoundary();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte[] data = new byte [10*1024];
		Canvas c = null;
		Bitmap b;
		
		mHandler.dispatchMessage(
				Message.obtain(mHandler, QuickViewSurfaceView.MSG_LOAD_VIDEO_SUCCESS));
		
		while (isRunning)
		{
			try {
				headers = ssplit.readHeaders();

				if (headers == null)
				{
					reportError();
					break;
				}
				
				ctype = (String) headers.get("content-type");
				if (ctype == null) {
					Log.e("mbp", "ctype == null");
					reportError();
					break; 
				}

				if (ctype.startsWith("image/jpeg")) 
				{
					ctype = (String) headers.get("content-length");
					int data_len = Integer.parseInt(ctype);
					int actual_data_read = 0;
					actual_data_read = ssplit.readDataToBoundary(boundary,data, data_len );

					if (actual_data_read == 0) {
						reportError();
						break;
					}
					if (actual_data_read == -1) {
						reportError();
						break;
					}

					b = BitmapFactory.decodeByteArray(data,0, actual_data_read);
					
					/* update the canvas with new image */
					if (mSurfaceHolder == null) {
						Log.w("mbp","_surfaceHolder is NULL");
						break;
					}

					try {
						c = mSurfaceHolder.lockCanvas(null);
						if (c == null) {
							Log.w("mbp","canvas is NULL");
							break;
						}

						synchronized (mSurfaceHolder) {
							b = Bitmap.createScaledBitmap(b, c.getWidth(), c.getHeight(), true);
							c.drawBitmap(b,0,0,null);
						}
					} finally {
						// do this in a finally so that if an exception is thrown
						// during the above, we don't leave the Surface in an
						// inconsistent state
						if (c != null) {
							mSurfaceHolder.unlockCanvasAndPost(c);
						}
					}
					
				}

			} catch (IOException e) {
				reportError();
				e.printStackTrace();
				
			}

		} /* while (isRunning) */

	}

}
