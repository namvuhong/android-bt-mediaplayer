package com.msc3;

import java.io.IOException;
import java.io.Serializable;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

/* Store some information regarding the channel
 * - ImageView
 * - Index 
 * - CamProfile that is bound to this Channel 
 */





/**
 * @author phung
 * 
 * Remote connection simple state machine.
 * UPNP : 
 *                                                         
 *          |<-------------------------------------------------------Stopped by user
 *          |                                                                  |               
 *     Invalid State ----> GetPortState -----------> ViewReqState -----> Streaming ---- stopped Unexpectedly 
 *                          <if current state                                                |   
 *                           is streaming>                                                   |
 *                          set relink = true                                                |
 *                                                                                   ....GetPortState
 * STUN :
 * 
 *         |<---------------------------- stopped by user
 *         |                                      |
 *    Invalid State ---> ViewReqState -------> Streamming --- Stopped unexpectedly              
 *                       <check for relink>                       |                                                  
 *                             |                                  |
 *                             |<---------------------------------|                                                          
 * 
 *
 */
public class CamChannel implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4306588857336898530L;
	private int        channel_index;
	private transient  ImageView  channel_view; //non serialize this
	private int        channel_view_disable_res_id;
	private int        channel_view_enable_res_id;

	private CamProfile profile;
	private int        channel_configure_status; 

	private String stream_url;

	public static final int LOCAL_VIEW = 1;
	public static final int REMOTE_HTTP_VIEW = 2;
	public static final int REMOTE_UDT_VIEW = 3;
	public static final int REMOTE_RELAY_VIEW = 4;
	private transient int currentViewSession; 

	//*only valid if currentViewSession = REMOTE_HTTP_VIEW
	public static final int RV_INVALID_STATE = -1;
	public static final int RV_GET_PORT_STATE = 1;
	public static final int RV_VIEW_REQ_STATE = 2;
	public static final int RV_STREAM_STATE = 3;
	private transient int remoteViewSessionState; 
	
	//private transient GetRemoteCamPortTask getPortTask; // just keep a reference
	private transient ViewRemoteCamRequestTask viewRMTask; // just keep a reference
	private transient boolean relinkMode ; 


	public static final int CONFIGURE_STATUS_NOT_ASSIGNED = 0x100;
	public static final int CONFIGURE_STATUS_AWAITING_FOR_ASSIGNMENT = 0x101;
	public static final int CONFIGURE_STATUS_ASSIGNED = 0x102;



	public CamChannel(int index)
	{
		channel_index = index;
		profile = null;
		stream_url = null;
		channel_configure_status = CONFIGURE_STATUS_NOT_ASSIGNED;
		
		currentViewSession = LOCAL_VIEW;
		remoteViewSessionState = RV_INVALID_STATE;
		relinkMode = false; 
	}


//	//* maintain a state machine: 
//	public boolean setGetPortState(GetRemoteCamPortTask gportTask)
//	{
//		//If it was streaming state before entering this
//		//    that means we are trying to relink -- set the Task to retryUntilSuccess mode
//		if ( ( (currentViewSession == REMOTE_HTTP_VIEW) ) && 
//				(RV_STREAM_STATE == remoteViewSessionState )
//				)
//		{
//			Log.d("mbp","Was in RV_STREAM_STATE before this -- set relink to TRUE");
//			relinkMode = true;
//			
//		}
//		
//		synchronized (this) {
//			getPortTask = gportTask;
//			getPortTask.setRetryUntilSuccess(relinkMode);
//			remoteViewSessionState = RV_GET_PORT_STATE;
//		}
//
//		return true;
//	}
	
	public boolean setViewReqState(ViewRemoteCamRequestTask viewTask)
	{
		
//		if ( ( (currentViewSession == REMOTE_HTTP_VIEW) &&
//				(RV_GET_PORT_STATE != remoteViewSessionState ) ) &&
//				(currentViewSession != REMOTE_UDT_VIEW)
//				)
		if ( ( (currentViewSession == REMOTE_HTTP_VIEW) &&
				(RV_GET_PORT_STATE != remoteViewSessionState ) ) &&
				(currentViewSession != REMOTE_UDT_VIEW) &&
				(currentViewSession != REMOTE_RELAY_VIEW)
				)
		{
			Log.d("mbp","invalid state at: setViewreq -- return false; ");
			return false; 
		}
		
		if ( (currentViewSession == REMOTE_UDT_VIEW)  &&  
				(RV_STREAM_STATE == remoteViewSessionState ) )
		{
			Log.d("mbp","UDT STREAM : Was in RV_STREAM_STATE before this -- set relink to TRUE");
			relinkMode = true;
		}
		
		
		synchronized (this) {
			viewRMTask = viewTask; 
			viewRMTask.setRetryUntilSuccess(relinkMode);
			remoteViewSessionState = RV_VIEW_REQ_STATE;
		}
		return true; 
	}

	/**
	 * @return the stream_url
	 */
	public String getStreamUrl() {
		return stream_url;
	}


	/**
	 * @param stream_url the stream_url to set
	 */
	public void setStreamUrl(String stream_url) {
		this.stream_url = stream_url;
	}


	public boolean setStreamingState()
	{
		if  ( RV_VIEW_REQ_STATE != remoteViewSessionState )
		{
			Log.d("mbp","setStreamingState():invalid state at: setStream -- return false; ");
			return false; 
		}
		
		
		
		synchronized (this) {
			remoteViewSessionState = RV_STREAM_STATE;
		}
		
		return true; 
	}


	public int getRemoteViewSessionState() {
		return remoteViewSessionState;
	}



	public void  cancelRemoteConnection()
	{
		Log.d("mbp","Cancel current remote connection");
		synchronized (this) {
			switch (remoteViewSessionState)
			{
			case RV_GET_PORT_STATE:
				//Log.d("mbp","Current state: RV_GET_PORT_STATE (getPortTask != null)? " +(getPortTask != null));

//				if (getPortTask != null)
//				{
//					getPortTask.cancel(true);
//					getPortTask = null; 
//				}
				
				
				break;
			case RV_VIEW_REQ_STATE:
				
				Log.d("mbp","Current state: RV_VIEW_REQ_STATE (viewRMTask != null)? " + (viewRMTask != null));
				
				if (viewRMTask != null)
				{
					viewRMTask.cancel(true);
					viewRMTask = null;
				}
				break;

			case RV_STREAM_STATE:
				Log.d("mbp","Current state: RV_STREAM_STATE"); 
				break;
			default:
				break;
			}
			
			//Reset state machine variables 
			remoteViewSessionState = RV_INVALID_STATE;
			relinkMode = false;  
		}
		
		
	}



	private void setRemoteViewSessionState(int remoteViewSessionState) {
		this.remoteViewSessionState = remoteViewSessionState;
	}


	public int getCurrentViewSession() {
		return currentViewSession;
	}





	public void setCurrentViewSession(int currentViewSession) {
		this.currentViewSession = currentViewSession;
	}

	/**
	 * 20121121: Do nothing here for now
	 * Copy some session data that needs to be reserved throughout a data restore. 
	 * 
	 * For now just copy the melody status 
	 * @param src
	 * @param dest
	 */
	public static void copySessionData(CamChannel src, CamChannel dest)
	{
//		if (src.getCamProfile() != null && 
//				dest.getCamProfile() != null  &&
//				src.getCamProfile().get_MAC().equalsIgnoreCase(dest.getCamProfile().get_MAC())	
//				)
//		{
//			dest.getCamProfile().setMelodyIsOn(src.getCamProfile().isMelodyIsOn());
//		}
	}







	public boolean setCamProfile(CamProfile cp)
	{
		if ( cp == null )
		{
			return false;
		}

		setState(CONFIGURE_STATUS_ASSIGNED);
		profile = cp;



		return true; 
	}

	public void setViewEnable()
	{
		if (channel_view != null)
			channel_view.setImageResource(channel_view_enable_res_id);
	}


	public CamProfile getCamProfile()
	{
		return profile;
	}

	public void reset()
	{
		setState(CONFIGURE_STATUS_NOT_ASSIGNED);
		profile = null;
		if (channel_view != null)
			channel_view.setImageResource(channel_view_disable_res_id);
	}

	/* reflect the change in image if any, 
	 * called after data restored to refresh the UI
	 */
	public void refresh()
	{
		if( (channel_configure_status == CONFIGURE_STATUS_ASSIGNED) &&
				(profile != null) &&
				(channel_view != null))
		{
			channel_view.setImageResource(channel_view_enable_res_id);
		}
		else 
		{
			reset();
		}
	}

	public boolean setConfigure()
	{
		switch (channel_configure_status)
		{
		case CONFIGURE_STATUS_NOT_ASSIGNED:
		case CONFIGURE_STATUS_ASSIGNED: /*silently accept reconfigure*/	

			if (profile != null)
			{
				profile.bind(false);
				profile.setChannel(null);
				profile = null;
			}
			channel_view.setImageResource(channel_view_disable_res_id);

			channel_configure_status = CONFIGURE_STATUS_AWAITING_FOR_ASSIGNMENT;
			break;
		case CONFIGURE_STATUS_AWAITING_FOR_ASSIGNMENT:
			break;
		default:
			break;
		}

		return true;
	}
	private boolean setState(int state)
	{
		if ( (state >= CONFIGURE_STATUS_NOT_ASSIGNED) &&
				(state <=CONFIGURE_STATUS_ASSIGNED ))
		{
			channel_configure_status = state;
			return true;
		}
		return false;
	}


	public int getState()
	{
		return channel_configure_status;
	}

	public int getChannel()
	{
		return channel_index ; 
	}

	public void setChannelImage(ImageView channel)
	{
		this.channel_view = channel; 
	}
	public void setChannelHighlightImage(int resource_id)
	{
		this.channel_view_enable_res_id = resource_id;
	}
	public void setChannelNormalImage(int resource_id)
	{
		this.channel_view_disable_res_id = resource_id;
	}

	public String toString()
	{
		String out = null;

		if (channel_configure_status ==CONFIGURE_STATUS_ASSIGNED )
		{
			out = String.format("Channel %d: %s",channel_index+1, profile.getName() );
		}
		else
		{
			out = String.format("Channel %d: ",channel_index+1 );
		}
		return out;
	}
	/*private void writeObject(java.io.ObjectOutputStream out) throws IOException
	{

	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
	{

	}*/

}
