package com.msc3;

public interface ITimerUpdater {

	public void updateCurrentCount(int count);
	public void timerKick();
	public void timeUp(); 
}
