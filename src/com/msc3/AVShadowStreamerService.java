package com.msc3;



import com.blinkhd.MBPActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.blinkhd.R;

public class AVShadowStreamerService extends Service implements IVideoSink,ITemperatureUpdater{

	
	
	/*
	 * Message to register the service with the service controller 
	 *         the streamer object is attached in this message
	 */
	public static final int AVS_MSG_REGISTER_CLIENT = 0x101;
	public static final int AVS_MSG_UNREGISTER_CLIENT = 0x102;
	public static final int AVS_MSG_ACTIVITY_NOT_READY = 0x104;
	
	public static final int AVS_MSG_STATUS_ICON = 0x105;
	
	private boolean muteAudioForCall = false; 
	
	private Messenger mClients ;
	
	private PCMPlayer _pcmPlayer ; 
	private Thread    pcmPlayer_thrd;
	private Streamer vstreamer;
	
	public AVShadowStreamerService() {
		super();

	}
	public AVShadowStreamerService(String name) {
		super();
	}


	private class MBroadcastReceiver extends BroadcastReceiver 
	{
        @Override
        public void onReceive(Context ctxt, Intent i) {

        	//ARGGG: close streamer now... 
			mClients= null;
			
			if (keep_cpu_running != null && keep_cpu_running.isHeld())
			{
				keep_cpu_running.release();
			}
			
			if (vstreamer != null)
			{
				vstreamer.removeVideoSink(AVShadowStreamerService.this);
				//vstreamer.setTemperatureUpdater(null);
			}
			
			stopPcmPlayer();
			
			vstreamer.stop();
        }
    };
    
    private MBroadcastReceiver cameraRemovedReceiver;
    
	@Override
	public void onCreate() 
	{
		super.onCreate();
		cameraRemovedReceiver = new MBroadcastReceiver();
		IntentFilter filter = new IntentFilter(CameraMenuActivity.ACTION_CAMERA_REMOVED);
		registerReceiver( cameraRemovedReceiver, filter);
		
		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		tm.listen(mPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
	}



	@Override
	public void onDestroy() 
	{
		
		super.onDestroy();
		if (cameraRemovedReceiver != null)
		{
			unregisterReceiver(cameraRemovedReceiver);
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		//Log.d("mbp", this.getClass().getName() + ": starting...");

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}
	
	private void stopPcmPlayer()
	{
		if ( pcmPlayer_thrd != null )
		{
			_pcmPlayer.stop();
			boolean retry = true;
			while (retry)
			{	
				try {
					pcmPlayer_thrd.join(2000);
					retry = false;
				} catch (InterruptedException e) {
				}
			}
			pcmPlayer_thrd = null;
		}
		
	}
	
	private void startPcmPlayer()
	{
		_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, false);
		pcmPlayer_thrd =new Thread(_pcmPlayer,"PCMPlayer");
		pcmPlayer_thrd.start();
	}
	
	

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}
	
	private WakeLock keep_cpu_running; 
	
	final Messenger mMessenger = new Messenger(new AVShadowIncomingHandler()); 
	
	class AVShadowIncomingHandler extends Handler
	{ // Handler of incoming messages from clients.
		@Override
		public void handleMessage(Message msg) {

			/* Handle message FROM UI activity*/
			switch (msg.what) {
			case AVS_MSG_REGISTER_CLIENT:

				
				
				synchronized (AVShadowStreamerService.this) {
					mClients = msg.replyTo;
					
					if (keep_cpu_running != null && keep_cpu_running.isHeld())
					{
						keep_cpu_running.release();
					}
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					keep_cpu_running = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
							"KEEP CPU RUNNING");
					keep_cpu_running.acquire();
					
					vstreamer = (Streamer)msg.obj;
					if (vstreamer != null)
					{
						vstreamer.addVideoSink(AVShadowStreamerService.this);
						//vstreamer.setTemperatureUpdater(AVShadowStreamerService.this);
					
					}
					else
					{
						Log.e("mbp","AVShadowStreamerService vstreamer obj is NULL in REGISTER_CLIENT??");
					}
					
					//just to make sure -- close the current player
					stopPcmPlayer();
					startPcmPlayer();
				}	

				break;
			case AVS_MSG_UNREGISTER_CLIENT:

				synchronized (AVShadowStreamerService.this) {
					mClients= null;
					
					if (keep_cpu_running != null && keep_cpu_running.isHeld())
					{
						keep_cpu_running.release();
					}
					
					if (vstreamer != null)
					{
						vstreamer.removeVideoSink(AVShadowStreamerService.this);
						//vstreamer.setTemperatureUpdater(null);
					}
					
					stopPcmPlayer();
					
				}	

				break;
			case AVS_MSG_STATUS_ICON:
				int turnOn = msg.arg1;
				if (turnOn ==1)
				{
					showSOL((String)msg.obj);
				}
				else
				{
					hideSOL(); 
				}
				break; 
			default:
				super.handleMessage(msg);
			}
		}
	}
	
	
	private void showSOL(String cameraName)
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		
		int icon = R.drawable.mini_app_icon;
		CharSequence tickerText = getResources().getString(R.string.app_name);
		long when = System.currentTimeMillis();
		
		if (cameraName == null)
		{
			cameraName = "Camera"; //getResources().getString(R.string.app_name);
		}
		
		
		Notification notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_NO_CLEAR ;
		notification.flags |= Notification.FLAG_ONGOING_EVENT ;
		
		Context context = getApplicationContext();      // application Context
		CharSequence contentTitle = cameraName;
		CharSequence contentText = getResources().getString(R.string.vox_service_enabled);
		
		Intent intent = new Intent(this, MBPActivity.class);
		intent.setAction(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);

		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		
		mNotificationManager.notify(100, notification);
		
	}
	
	private void hideSOL()
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		
		mNotificationManager.cancel(100);
	}
	

	
	@Override
	public void onFrame(byte[] frame, byte[] pcm, int pcm_len) 	 
	{
		if  (muteAudioForCall == false)
		{
			/* Dont show image, just play audio */
			if( (pcm != null) &&
					(pcmPlayer_thrd != null) &&
					pcmPlayer_thrd.isAlive() ) {
				_pcmPlayer.writePCM(pcm, pcm_len);
			}
		}

	} 
	
	@Override
	public void onInitError(String errorMessage) {
		
	}
	@Override
	public void onVideoEnd() {
		
	}
	
	@Override
	public void updateTemperature(int level)
	{
		//DO nothing here. 
	}

	
	
	private PhoneStateListener mPhoneListener = new PhoneStateListener() 
	{
		
		public void onCallStateChanged(int state, String incomingNumber) {
			try {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					muteAudioForCall = true; 
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					muteAudioForCall = false; 
					break;
				default:
				}
			} catch (Exception e) {
				Log.i("Exception", "PhoneStateListener() e = " + e);
			}
		}
	};

	



	
}
