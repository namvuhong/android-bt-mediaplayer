package com.msc3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceStartUpReceiver extends BroadcastReceiver {

	 @Override
	    public void onReceive(Context context, Intent intent) {
		 
		 /*20120717: phung: dont start VOX service on reboot  
	      
	        Intent startServiceIntent = new Intent(context, VoiceActivationService.class);
	        context.startService(startServiceIntent);
	        
	      */ 
	    }
}
