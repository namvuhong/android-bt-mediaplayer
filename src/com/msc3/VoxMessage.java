package com.msc3;

import java.io.Serializable;
import java.net.InetAddress;

import android.util.Log;

public class VoxMessage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5472789015279784645L;

	public static final long VOX_MSG_EXPIRY = 19000;//19-20 sec 

	public static final int VOX_TYPE_UNKNOWN = 0;
	public static final int VOX_TYPE_VOICE = 1;
	public static final int VOX_TYPE_TEMP_HI = 2;
	public static final int VOX_TYPE_TEMP_LO = 4;
	public static final int VOX_TYPE_MOTION_ON = 8;
	
	public static final int ALERT_TYPE_INFO = 0;
	public static final int ALERT_TYPE_SOUND = 1; 
	public static final int ALERT_TYPE_TEMP_HI = 2;
	public static final int ALERT_TYPE_TEMP_LO= 3; 
	public static final int ALERT_TYPE_MOTION_ON = 4;

	private static final String [] voxTypes = {"Unknown VOX TYPE", "VOICE", "HOT","Unknown2", "COLD","6","7","8","Motion Detected"};
	
	

	private int voxType; 
	private String voxTypeStr;
	private String trigger_ip;
	private String trigger_mac;
	private String trigger_name; // Camera name 
	private long trigger_time;
	private  long expired_time;
	private String eventCode;
	private int voxVal; // Optional : only when it's HOT or COLD
	private String voxValueStr;
	public static final int VOX_VAL_INVALID = 0xFFFFFFFF; 
	public String getEventTimeCode()
	{
		String ret = null;
		if(voxType == VOX_TYPE_MOTION_ON)
		{
			ret = eventCode;
		}
		return ret;
	}
	public static VoxMessage buildMessage(String alertType, String macAddr, 
			String alertVal, String alertTime, String cameraName, String url)
	{

		VoxMessage incoming = null; 
		int _alertType = -1; 
		long triggerTime = 0; 
		int _alertVal = VOX_VAL_INVALID;

		try
		{
			_alertType = Integer.parseInt(alertType);
		}
		catch (NumberFormatException nfe)
		{
			Log.d("mbp", "can't parse alert Type: "+alertType );
			_alertType = -1;
		}

		try
		{
			triggerTime = Long.parseLong(alertTime);
		}
		catch (NumberFormatException nfe)
		{
			Log.d("mbp", "can't parse alert time: "+alertTime );
			triggerTime = 0;
		}


		try
		{
			_alertVal = Integer.parseInt(alertType);
		}
		catch (NumberFormatException nfe)
		{
			Log.d("mbp", "can't parse alert val: "+alertVal );
			_alertVal = VOX_VAL_INVALID;
		}

		//1 = vox, 2 = temp hi, 3 = temp lo 
		
		switch (_alertType)
		{
		case ALERT_TYPE_INFO:
			incoming = new ServerMessage(VoxMessage.ALERT_TYPE_INFO,
					null, macAddr, triggerTime,alertVal, cameraName, url);
			break;
		
		case ALERT_TYPE_SOUND:
			incoming = new VoxMessage(VoxMessage.VOX_TYPE_VOICE,
					null, macAddr, triggerTime,VOX_VAL_INVALID, cameraName);
			break;
		case ALERT_TYPE_TEMP_HI:
			incoming = new VoxMessage(VoxMessage.VOX_TYPE_TEMP_HI,
					null, macAddr, triggerTime,_alertVal, cameraName);
			break;
		case ALERT_TYPE_TEMP_LO:
			incoming = new VoxMessage(VoxMessage.VOX_TYPE_TEMP_LO,
					null, macAddr, triggerTime,_alertVal, cameraName);
			break;
		case ALERT_TYPE_MOTION_ON:
			incoming = new VoxMessage(VoxMessage.VOX_TYPE_MOTION_ON,
					null, macAddr, triggerTime, alertVal , cameraName, url);
		default://Unknown
			break;
		}

		
		return incoming; 
	}

	public VoxMessage( int type, InetAddress ip, String mac, long trigger_time, String val, String name)
	{
		this.trigger_time = trigger_time; 
		this.expired_time = trigger_time + VOX_MSG_EXPIRY;
		if (ip == null)
		{
			this.trigger_ip = null; 
		}
		else
		{
			this.trigger_ip = ip.getHostAddress() ;
		}

		this.trigger_mac = mac;

		if ( type >= VOX_TYPE_VOICE && type <=VOX_TYPE_TEMP_LO)
		{
			this.voxType = type;
		}
		else if( type == VOX_TYPE_MOTION_ON)
		{
			this.voxType = VOX_TYPE_MOTION_ON;
		}
		else
		{
			this.voxType = VOX_TYPE_UNKNOWN;
		}

		this.voxTypeStr = voxTypes[this.voxType];

		this.voxValueStr = val;

		trigger_name = name;
		if ( name == null)
			trigger_name = "camera";

		Log.d("LOG FROM VOXMESSAGE","EVENT TYPE "+this.voxType);

	}
	
	public VoxMessage( int type, InetAddress ip, String mac, long trigger_time, String val, String name, String ftp_url)
	{
		this.trigger_time = trigger_time; 
		this.expired_time = trigger_time + VOX_MSG_EXPIRY;
		if (ip == null)
		{
			this.trigger_ip = null; 
		}
		else
		{
			this.trigger_ip = ip.getHostAddress() ;
		}

		this.trigger_mac = mac;

		if ( type >= VOX_TYPE_VOICE && type <=VOX_TYPE_TEMP_LO)
		{
			this.voxType = type;
		}
		else if( type == VOX_TYPE_MOTION_ON)
		{
			this.voxType = VOX_TYPE_MOTION_ON;
		}
		else
		{
			this.voxType = VOX_TYPE_UNKNOWN;
		}

		this.voxTypeStr = voxTypes[this.voxType];

		this.voxValueStr = val;
		
		this.eventCode = getEventCodeFromUrl(ftp_url);

		trigger_name = name;
		if ( name == null)
			trigger_name = "camera";

		Log.d("LOG FROM VOXMESSAGE","EVENT TYPE "+this.voxType);

	}
	
	public VoxMessage( int type, InetAddress ip, String mac, long trigger_time, int val, String name)
	{
		this.trigger_time = trigger_time; 
		this.expired_time = trigger_time + VOX_MSG_EXPIRY;
		if (ip == null)
		{
			this.trigger_ip = null; 
		}
		else
		{
			this.trigger_ip = ip.getHostAddress() ;
		}

		this.trigger_mac = mac;

		if ( type >= VOX_TYPE_VOICE && type <=VOX_TYPE_TEMP_LO)
		{
			this.voxType = type;
		}
		else
		{
			this.voxType = VOX_TYPE_UNKNOWN;
		}

		this.voxTypeStr = voxTypes[this.voxType];

		this.voxVal = val;

		trigger_name = name;
		if ( name == null)
			trigger_name = "camera";


	}



	public VoxMessage( int type, String ip, String mac)
	{
		trigger_time = expired_time = 0; 

		this.trigger_ip = ip ;
		this.trigger_mac = mac;

		if ( type >= VOX_TYPE_VOICE && type <=VOX_TYPE_TEMP_LO)
		{
			this.voxType = type;
		}
		else
		{
			this.voxType = VOX_TYPE_UNKNOWN;
		}

		this.voxTypeStr = voxTypes[this.voxType];

		this.voxVal = 0;
		trigger_name = "Camera";
	}

	private String getEventCodeFromUrl(String url)
	{
		String result = null;
		
		if (url != null)
		{
			int endIdx = url.indexOf(".jpg");
			int startIdx = endIdx - 33;
			
			try {
				result = url.substring(startIdx, endIdx);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public String getTrigger_ip() {
		return trigger_ip;
	}

	public int getVoxType() {
		return voxType;
	}

	public void setVoxType(int voxType) {
		this.voxType = voxType;
	}

	public void setTrigger_ip(InetAddress trigger_ip) {
		this.trigger_ip = trigger_ip.getHostAddress();
	}

	/**
	 * 
	 * @return MAC address with COLON
	 */
	public String getTrigger_mac() {
		return trigger_mac;
	}

	public void setTrigger_mac(String trigger_mac) {
		this.trigger_mac = trigger_mac;
	}

	public long getTrigger_time() {
		return trigger_time;
	}

	public void setTrigger_time(long trigger_time) {
		this.trigger_time = trigger_time;
	}

	public long getExpired_time() {
		return expired_time;
	}

	public void setExpired_time(long expired_time) {
		this.expired_time = expired_time;
	}

	public String getTrigger_name() {
		return trigger_name;
	}


	public void setTrigger_name(String trigger_name) {
		this.trigger_name = trigger_name;
	}

	public String toString()
	{
		String msg = "Unknown alert";
		switch(this.voxType)
		{
		case  VOX_TYPE_TEMP_LO:
			msg = "Temperature too low";
			break;
		case  VOX_TYPE_TEMP_HI:
			msg = "Temperature too high";
			break;
		case  VOX_TYPE_VOICE:
			msg = "Sound detected";
			break;
		case VOX_TYPE_MOTION_ON:
			msg = "Motion detected";
			break;
		default:
			break;

		}
		
		return msg; // + " From: " + trigger_mac; 
	}
}