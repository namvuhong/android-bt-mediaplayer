package com.msc3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import com.discovery.ScanProfile;

import android.util.Log;
import android.widget.ImageView;

public class CamProfile extends ScanProfile {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3199093442896698501L;

	private static final int CAMERA_UNREACHABLE_THRESHOLD = 10; //minutes
	
	public static final String CODEC_MJPEG = "MJPEG";
	public static final String CODEC_H264 = "H264";
	
	public static final String MODEL_MBP36N = "MBP36N";
	public static final String MODEL_MBP33N = "MBP33N";
	public static final String MODEL_MBP41N = "MBP41N";
	public static final String MODEL_MBP836 = "MBP836";
	public static final String MODEL_FOCUS66 = "FOCUS66";
	public static final String MODEL_FOCUS96 = "FOCUS96";
	public static final String MODEL_MBP83 = "MBP83";
		
	public static final String MODEL_ID_MBP36N = "0036";
	public static final String MODEL_ID_MBP33N = "0033";
	public static final String MODEL_ID_MBP41N = "0041";
	public static final String MODEL_ID_MBP836 = "0836";
	public static final String MODEL_ID_FOCUS66 = "0066";
	public static final String MODEL_ID_FOCUS96 = "0096";
	public static final String MODEL_ID_MBP83 = "0083";

	private InetAddress remote_addr;
	private int remoteRtspPort;
	private int remoteRtpVideoPort;
	private int remoteRtpAudioPort;
	private int remoteTalkBackPort;
	
	/* 20120201: add push-to-talk port */
	private int ptt_port;

	private String   version_string;
	private String codec;
	private String firmwareVersion;
	private String model;
	private String modelId;
	private String planId;
	private String registrationId;
	private boolean isUpgrading;
	
	

	private ArrayList<byte[]> shortclip;
	private boolean     isBound;
	private static final int SHORT_CLIP_SIZE = 2;
	private CamChannel mChannel;
	private String camName;
	private int camId;
	/*20120105:FROM SERVER: Last access Time and date in human-readable format */
	private String lastCommStatus; 
	/*20120105:FROM SERVER: time between this request and last communication from the device (in minutes)*/
	private int minutesSinceLastComm;
	private String lastUpdate;

	/* 20111215: basic authentication usr/pass*/
	private String basicAuth_usr;
	private String basicAuth_pass;
	
	/*20120223: set if melody is being play on this camera */
	private boolean melodyIsOn;

	/*20120420: set for remote case */
	private int remoteCommMode;
	
	/*20120606: set if voxIsOn */ 
	private boolean voxEnabled;
	
	/*20120910: set to receive sound/temp alert */
	private boolean soundAlertEnabled;
	private boolean tempHiAlertEnabled;
	private boolean tempLoAlertEnabled;
	private boolean motionAlertEnabled;
	
	/*20131221: store path to image in gallery */ 
	private String galleryImagePath; 
	
	public static final int CAMERA_UPNP_NOT_OK = 0;
	public static final int CAMERA_UPNP_OK = 1;
	public static final int CAMERA_UPNP_IN_PROGRESS = 2;
	
	
	/**
	 * 
	 * Create a CamProfile with the given inet address, port and mac address
	 * @param iaddress
	 * @param port
	 * @param MAC_addr
	 */
	public CamProfile(InetAddress iaddress, int port, String MAC_addr)
	{
		super(iaddress, port, MAC_addr);
		ptt_port = PublicDefine.DEFAULT_AUDIO_PORT;
		isBound = false;
		mChannel = null;
		shortclip = new ArrayList<byte[]>(SHORT_CLIP_SIZE);
		camName = null;
		lastCommStatus="none";
		minutesSinceLastComm = CAMERA_UNREACHABLE_THRESHOLD;
		lastUpdate = null;
		
		melodyIsOn = false;
		remoteCommMode = StreamerFactory.STREAM_MODE_HTTP_LOCAL;
		firmwareVersion = "0";
		codec = CODEC_MJPEG;
		camId = -1;
	}

	/**
	 * 
	 * Create a CamProfile with the given inet address, and mac address
	 * port will be set to 80 by default.
	 * 
	 * @param iaddress
	 * @param MAC_addr
	 */
	public CamProfile(InetAddress iaddress, String MAC_addr)
	{
		super(iaddress, MAC_addr);
		ptt_port = PublicDefine.DEFAULT_AUDIO_PORT;
		isBound = false;
		mChannel = null;
		shortclip = new ArrayList<byte[]>(SHORT_CLIP_SIZE);
		camName = null;
		lastCommStatus="none";
		minutesSinceLastComm = CAMERA_UNREACHABLE_THRESHOLD;
		lastUpdate = null;
		melodyIsOn = false;
		
		remoteCommMode = StreamerFactory.STREAM_MODE_HTTP_LOCAL;
		firmwareVersion = "0";
		codec = CODEC_MJPEG;
		camId = -1;
	}
	
	
	
	
	
	public String getPlanId()
	{
		return planId;
	}

	public void setPlanId(String planId)
	{
		this.planId = planId;
	}

	public String getRegistrationId()
	{
		return registrationId;
	}

	public void setRegistrationId(String registrationId)
	{
		this.registrationId = registrationId;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
		if (modelId != null)
		{
			if (modelId.equalsIgnoreCase(MODEL_ID_FOCUS66))
			{
				model = MODEL_FOCUS66;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_FOCUS96))
			{
				model = MODEL_ID_FOCUS96;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_MBP36N))
			{
				model = MODEL_MBP36N;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_MBP83))
			{
				model = MODEL_MBP83;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_MBP836))
			{
				model = MODEL_MBP836;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_MBP33N))
			{
				model = MODEL_MBP33N;
			}
			else if (modelId.equalsIgnoreCase(MODEL_ID_MBP41N))
			{
				model = MODEL_MBP41N;
			}
		}
	}

	public int getCamId() {
		return camId;
	}

	public void setCamId(int camId) {
		this.camId = camId;
	}

	/**
	 * @return the remote_addr
	 */
	public InetAddress getRemote_addr() {
		return remote_addr;
	}

	/**
	 * @param remote_addr the remote_addr to set
	 */
	public void setRemote_addr(InetAddress remote_addr) {
		this.remote_addr = remote_addr;
	}

	/**
	 * @return the remoteRtspPort
	 */
	public int getRemoteRtspPort() {
		return remoteRtspPort;
	}

	/**
	 * @param remoteRtspPort the remoteRtspPort to set
	 */
	public void setRemoteRtspPort(int remoteRtspPort) {
		this.remoteRtspPort = remoteRtspPort;
	}

	/**
	 * @return the remoteRtpVideoPort
	 */
	public int getRemoteRtpVideoPort() {
		return remoteRtpVideoPort;
	}

	/**
	 * @param remoteRtpVideoPort the remoteRtpVideoPort to set
	 */
	public void setRemoteRtpVideoPort(int remoteRtpVideoPort) {
		this.remoteRtpVideoPort = remoteRtpVideoPort;
	}

	/**
	 * @return the remoteRtpAudioPort
	 */
	public int getRemoteRtpAudioPort() {
		return remoteRtpAudioPort;
	}

	/**
	 * @param remoteRtpAudioPort the remoteRtpAudioPort to set
	 */
	public void setRemoteRtpAudioPort(int remoteRtpAudioPort) {
		this.remoteRtpAudioPort = remoteRtpAudioPort;
	}

	/**
	 * @return the remoteTalkBackPort
	 */
	public int getRemoteTalkBackPort() {
		return remoteTalkBackPort;
	}

	/**
	 * @param remoteTalkBackPort the remoteTalkBackPort to set
	 */
	public void setRemoteTalkBackPort(int remoteTalkBackPort) {
		this.remoteTalkBackPort = remoteTalkBackPort;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the firmwareVersion
	 */
	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	/**
	 * @param firmwareVersion the firmwareVersion to set
	 */
	public void setFirmwareVersion(String firmwareVersion) {
		if (firmwareVersion == null)
		{
			this.firmwareVersion = "0";
		}
		else
		{
			this.firmwareVersion = firmwareVersion;
		}
	}

	/**
	 * @return the codec
	 */
	public String getCodec() {
		return codec;
	}

	/**
	 * @param codec the codec to set
	 */
	public void setCodec(String codec) {
		if (codec == null)
		{
			this.codec = CODEC_MJPEG;
		}
		else
		{
			this.codec = codec;
		}
	}

	public boolean isSoundAlertEnabled() {
		return soundAlertEnabled;
	}

	public void setSoundAlertEnabled(boolean soundAlertEnabled) {
		this.soundAlertEnabled = soundAlertEnabled;
	}

	public boolean isTempHiAlertEnabled() {
		return tempHiAlertEnabled;
	}

	public void setTempHiAlertEnabled(boolean tempHiAlertEnabled) {
		this.tempHiAlertEnabled = tempHiAlertEnabled;
	}

	public boolean isTempLoAlertEnabled() {
		return tempLoAlertEnabled;
	}

	public void setTempLoAlertEnabled(boolean tempLoAlertEnabled) {
		this.tempLoAlertEnabled = tempLoAlertEnabled;
	}
	
	public boolean isMotionAlertEnabled() {
		return motionAlertEnabled;
	}

	public void setMotionAlertEnabled(boolean motionAlertEnabled) {
		this.motionAlertEnabled = motionAlertEnabled;
	}

	public boolean isVoxEnabled() {
		return voxEnabled;
	}

	public void setVoxEnabled(boolean voxIsEnabled) {
		this.voxEnabled = voxIsEnabled;
	}

	public void setName(String name)
	{
		camName = name;
	}
	public String getName()
	{
		return camName; 
	}
	
//	public String getVersion_string() {
//		return version_string;
//	}
//
//	public void setVersion_string(String version_string) {
//		this.version_string = version_string;
//	}
	
	public int getRemoteCommMode() {
		return remoteCommMode;
	}

	public void setRemoteCommMode(int remoteCommMode) {
		this.remoteCommMode = remoteCommMode;
	}
	
	
	public void  setMinutesSinceLastComm(int mins)
	{
		if ((int)mins > CAMERA_UNREACHABLE_THRESHOLD)
		{
			minutesSinceLastComm = CAMERA_UNREACHABLE_THRESHOLD;
		}
		else
		{
			minutesSinceLastComm = (int)mins;
		}
		
		
		if (minutesSinceLastComm < CAMERA_UNREACHABLE_THRESHOLD )
		{
			camLocation = CAMERA_REACHABLE_IN_REMOTE_NW;
		}
		else
		{
			camLocation = CAMERA_UNREACHABLE;
		}
	}
	public void setLastCommStatus(String lastComm)
	{
		lastCommStatus = lastComm;
	}
	public String getLastCommStatus()
	{
		return lastCommStatus;
	}
	
	
	public String getBasicAuth_usr()
	{
		return basicAuth_usr;
	}
	public void  setBasicAuth_usr(String usr)
	{
		basicAuth_usr = usr;
	}
	
	public String getBasicAuth_pass()
	{
		return basicAuth_pass;
	}
	public void  setBasicAuth_pass(String pass)
	{
		basicAuth_pass = pass;
	}

	public int get_ptt_port()
	{
		return ptt_port;
	}
	
	public boolean isBound()
	{
		return isBound;
	}
	public void bind(boolean sel)
	{
		isBound = sel;
	}
	
	public void setPTTPort(int port)
	{
		this.ptt_port = port;
		
	}
	public void setChannel(CamChannel chan)
	{
		mChannel = chan;
	}
	public boolean equals(CamProfile o) {

		if ( o == null || o.get_MAC() == null)
		{
			return false;
		}

		if (this.MAC_addr.equalsIgnoreCase(o.get_MAC()))
			return true;


		return false;
	}


	public ArrayList<byte[]> getShortClip()
	{
		return shortclip;
	}

	public void setShortClip(ArrayList<byte[]> snapshots)
	{
		shortclip = snapshots;
	}


	public String toString()
	{
		//return inet_addr.toString()+ "@"+ MAC_addr;
		return MAC_addr;
	}

	public void unSelect()
	{
		isBound = false;
		if ( mChannel != null)
		{
			mChannel.reset();
			mChannel = null;
		}
	}

	
	/**
	 * 20120223: melodyIsOn is the lullaby status. 
	 * This can only be set in two places: 
	 *     - EntryActivity.updateMelodyIndex() called by VideoStreamer during connection setup
	 *     - ScanForCamera.updateMelodyIndex(CamProfile cam) called when scanning for 
	 *     camera in local network. 
	 *     
	 *     The update order should be : scanforcamera -> updateMelodyIndex() 
	 *     Once restoredataSession is called, this value is passed to the newly restore 
	 *     data as it is not SAVED in SetupData
	 *     
	 *     in Remote Mode, The update order should be just updateMelodyIndex() because 
	 *     scanforcamera should not find this particular camera
	 *     
	 *     
	 * @return
	
	public boolean isMelodyIsOn() {
		return melodyIsOn;
	}

	public void setMelodyIsOn(boolean melodyIsOn) {
		this.melodyIsOn = melodyIsOn;
	} */

	@Override 
	public int hashCode() {
		throw new UnsupportedOperationException();
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		hasUpdateLocalStatus = false;
	}
	
	public boolean isSupportNewRelay()
	{
		boolean result = false;
		int version_part1 = -1;
		int version_part2 = -1;
		if (!firmwareVersion.equals("0"))
		{
			try {
				version_part1 = Integer.parseInt(firmwareVersion.substring(0, 2));
				version_part2 = Integer.parseInt(firmwareVersion.substring(3));
			}
			catch (NumberFormatException nfe)
			{
			}
			
			if ( (version_part1 > 8) || ((version_part1 == 8) && (version_part2 >= 39)) )
			{
				result  = true;
			}
		}
		
		return result;
	}

	private boolean enable; 
	private void  setSelected(boolean select)
	{
		enable = select; 
	}
	
	public boolean isSelected() 
	{
		return enable;
	}
	
	
	public void setImageLink(String selectedImagePath) 
	{
		galleryImagePath =  selectedImagePath; 
	}
	
	public String getImageLink() 
	{
		return galleryImagePath; 
	}
	
	public static boolean shouldEnableMic(String deviceId)
	{
		if (deviceId.equalsIgnoreCase(MODEL_ID_MBP36N) ||
				deviceId.equalsIgnoreCase(MODEL_ID_MBP33N) ||
				deviceId.equalsIgnoreCase(MODEL_ID_MBP41N))
		{
			return false;
		}
		
		return true;
	}
	
	public static boolean shouldEnablePanTilt(String deviceId)
	{
		if (deviceId.equalsIgnoreCase(MODEL_ID_FOCUS66) ||
				deviceId.equalsIgnoreCase(MODEL_ID_MBP33N))
		{
			return false;
		}
		
		return true;
	}
	
	public boolean isSharedCam()
	{
		if (modelId.equalsIgnoreCase(MODEL_ID_MBP36N) ||
				modelId.equalsIgnoreCase(MODEL_ID_MBP33N) ||
				modelId.equalsIgnoreCase(MODEL_ID_MBP41N))
		{
			return true;
		}
		
		return false;
	}
	
	public static String getMacFromRegId(String regId)
	{
		String res = null;
		
		int startIdx = 6;
		int endIdx = startIdx + 12;
		try
        {
	        res = regId.substring(startIdx, endIdx);
        }
        catch (Exception e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		
		return res;
	}
	
	public static String getModelIdFromRegId(String regId)
	{
		String res = null;
		
		int startIdx = 2;
		int endIdx = startIdx + 4;
		try
        {
	        res = regId.substring(startIdx, endIdx);
        }
        catch (Exception e)
        {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		
		return res;
	}

	public boolean isUpgrading()
    {
	    return isUpgrading;
    }

	public void setUpgrading(boolean isUpgrading)
    {
	    this.isUpgrading = isUpgrading;
    }
}
