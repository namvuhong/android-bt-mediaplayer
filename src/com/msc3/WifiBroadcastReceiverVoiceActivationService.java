package com.msc3;

import com.msc3.registration.SingleCamConfigureActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.util.Log;

public class WifiBroadcastReceiverVoiceActivationService extends BroadcastReceiver {

	VoiceActivationService voiceactivationservice=null;
	private boolean shouldTurnOnWifiNotif;
	
	public WifiBroadcastReceiverVoiceActivationService(VoiceActivationService voiceactivationservice)
	{
		this.voiceactivationservice= voiceactivationservice;
		shouldTurnOnWifiNotif = true;
	}
	
	@Override
	    public void onReceive(Context context, Intent intent) {
			identifyChangeState( context,  intent);
	    }
	
	 
		 private void identifyChangeState(Context context, Intent intent) {

			 if (shouldTurnOnWifiNotif == true)
			 {
				 if (intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)&& intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,-1) == WifiManager.WIFI_STATE_DISABLED) {
					 voiceactivationservice.noficationMessage("WIFI DISABLE");

				 } else if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
					 {

						 NetworkInfo nw = (NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
						 if (nw.isConnectedOrConnecting() == false){          			
							 voiceactivationservice.noficationMessage("NO NETWORK ACCESS");
						 }
					 }

				 } else if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {

					 SupplicantState ss = (SupplicantState) intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE); 
					 int state = intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, 0);        

					 //DISCONNECTED 
					 if(SupplicantState.DISCONNECTED.equals(ss)) {
						 voiceactivationservice.noficationMessage("NO WIFI - OUT OF RANGE");
					 }
				 }//end if
			 }
			
			 if (intent.getAction().equals(SingleCamConfigureActivity.ACTION_TURN_WIFI_NOTIF_ON_OFF))
			 {
				 shouldTurnOnWifiNotif = intent.getBooleanExtra(
						 SingleCamConfigureActivity.WIFI_NOTIF_BOOL_VALUE,
						 true);
			 }
		 }

		    
}
