package com.msc3;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.media.AudioFormat;
import android.util.Log;
import android.view.SurfaceHolder;

public class AviPlayer implements Runnable {
	private BufferedInputStream _bis;
	private boolean _isAlive = true;
	private boolean _isPaused = false;
	private ArrayList<IVideoSink> _videoSinks;
	private ArrayList<IAudioSink> _audioSinks;
	private long _lastFrameTime;
	private int _frameRate;
	private int _totalFrames;
	private int _width;
	private int _height;

	private byte[]mbuffer ;
	private int MBUFFER_SIZE = 16 *1024;
	private PCMPlayer _pcmPlayer;
	private ImagePlayer img; 

	public AviPlayer(InputStream is) {
		_videoSinks = new ArrayList<IVideoSink>();
		_audioSinks = new ArrayList<IAudioSink>();

		_bis = new BufferedInputStream(is);
		mbuffer = new byte[MBUFFER_SIZE];

		img = new ImagePlayer();
		_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO, false);

	}

	public AviPlayer(String filename) {
		_videoSinks = new ArrayList<IVideoSink>();
		_audioSinks = new ArrayList<IAudioSink>();

		try {
			_bis = new BufferedInputStream(new FileInputStream(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mbuffer = new byte[MBUFFER_SIZE];

		img = new ImagePlayer();
	}

	public void addVideoSink(IVideoSink videoSink) {
		synchronized(_videoSinks) {
			_videoSinks.add(videoSink);
		}

		img.addVideoSink(videoSink);
	}

	public void removeVideoSink(IVideoSink videoSink) {
		synchronized(_videoSinks) {
			if(_videoSinks.contains(videoSink))
				_videoSinks.remove(videoSink);
		}
	}

	public void addAudioSink(IAudioSink audioSink) {
		synchronized(_audioSinks) {
			_audioSinks.add(audioSink);
		}
	}



	@Override
	public void run() {
		int count = 0;
		int data_read = 0;

		Thread _img = new Thread(img);
		_img.start();
		new Thread(_pcmPlayer).start();

		if(parseAviHeader()) {

			while(_isAlive) {
				try {
					int frameType = readFrameType();
					if(frameType == -1) {
						// end of stream
						break;
					}
					//byte[] buffer = readFrame();
					
					while (_isPaused)
					{
						try
						{
							Thread.sleep(10);
						} 
						catch (Exception ex)
						{}
					}
					
					data_read = readFrame_new();

					if(frameType == 1) {


						// video
						if(_lastFrameTime == 0) { 
							_lastFrameTime = System.currentTimeMillis();
						}
						long currentTime = System.currentTimeMillis();
						if(currentTime - _lastFrameTime < 1000000 / _frameRate) {
							//Log.d("mbp","sleep extra: " + (1000000 / _frameRate - (currentTime - _lastFrameTime)));

							Thread.sleep(1000000 / _frameRate - (currentTime - _lastFrameTime));
						}
						_lastFrameTime = System.currentTimeMillis();


						//						byte [] buffer = new byte[data_read];
						//						System.arraycopy(mbuffer,0 , buffer, 0, data_read);
						//						/* TEMP:*/
						//						for(IVideoSink videoSink : _videoSinks) {
						//							videoSink.onFrame(buffer, null,0 );
						//						}

						img.postData(mbuffer, data_read);


					} else if(frameType == 2) {


						/* TEMP: */
						byte [] buffer = new byte[data_read];
						System.arraycopy(mbuffer,0 , buffer, 0, data_read);

						//					    for(IAudioSink audioSink : _audioSinks) {
						//								audioSink.onPCM(buffer);
						//						}

						onPCM(buffer);



						/*// Use audio buffer
				//global		 	
	private static final int AUD_BUF_LEN = 8080;
	byte [] audio_buffer = new byte [AUD_BUF_LEN] ; 
	int audio_buffer_offset = 0;

						if ( (data_read + audio_buffer_offset) <= AUD_BUF_LEN)
						{
							System.arraycopy(mbuffer,0 , audio_buffer, audio_buffer_offset, data_read);	
							audio_buffer_offset+= data_read;
						}
						else 
						{
							 pass the current audio buff to player 
							for(IAudioSink audioSink : _audioSinks) {
								audioSink.onPCM(audio_buffer);
							}
							audio_buffer_offset = 0;
							System.arraycopy(mbuffer,0 , audio_buffer, audio_buffer_offset, data_read);
						}*/

					}


				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		try {
			_bis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(IVideoSink videoSink : _videoSinks) {
			videoSink.onVideoEnd();
		}

		img.setRunning(false);
		try {
			_img.join(3000);
		} catch (InterruptedException e) {
		}

		if (_pcmPlayer != null)
		{
			_pcmPlayer.stop();
		}
	}

	private int readFrameType() {
		int peek = 0;
		int type = 0;
		try {
			peek = _bis.read();
			if(peek == -1)
			{
				return -1;
			}
			peek = _bis.read();
			_bis.skip(2);
			if(peek == 49) {
				// beginning of 01wb - audio
				type = 2;
			} else if(peek == 48) {
				// beginning of 00db - video
				type = 1;
			}
		} catch (IOException e) {
			return -1;
		}

		return type;
	}

	private byte[] readFrame() {
		byte[] buffer = null;

		try {
			int size = readInt();
			buffer = new byte[size];
			_bis.read(buffer, 0, size);
		} catch (IOException e) {
			return null;
		}

		return buffer;
	}

	private int readFrame_new() {
		int size_to_read, size_read =-1;
		try {
			size_to_read= readInt();

			if (size_to_read > MBUFFER_SIZE)
			{
				enlage_buffer(size_to_read);
			}

			size_read =_bis.read(mbuffer, 0, size_to_read);
		} catch (IOException e) {
			return -1;
		}

		return size_read;
	}

	private void onPCM(byte[] pcmData) {
		if(_pcmPlayer != null) {
			_pcmPlayer.writePCM(pcmData, pcmData.length);
		}
	}

	private void enlage_buffer(int new_size)
	{
		MBUFFER_SIZE = new_size;
		mbuffer = new byte [MBUFFER_SIZE];
	}

	private boolean parseAviHeader() {
		// todo: add more checks for robustness
		try {
			_bis.skip(0x84);
			_frameRate = readInt();
			_bis.skip(4);
			_totalFrames = readInt();
			_bis.skip(32);
			_width = readInt();
			_height = readInt();
			_bis.skip(28);
			// junk
			_bis.skip(4);
			int junkSize = readInt();
			_bis.skip(junkSize);
			// list
			_bis.skip(76);
			// strf
			_bis.skip(4);
			int srtfSize = readInt();
			_bis.skip(srtfSize);
			// junk 2
			_bis.skip(4);
			int junk2Size = readInt();
			_bis.skip(junk2Size);
			// junk 3
			_bis.skip(4);
			int junk3Size = readInt();
			_bis.skip(junk3Size);
			// list
			_bis.skip(12);

			// now it should be either audio or video data
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}

		return true;
	}

	private int readInt() throws IOException {
		try {
			int	b0 = _bis.read();
			int b1 = _bis.read();
			int b2 = _bis.read();
			int b3 = _bis.read();
			return b0 + (b1 << 8) + (b2 << 16) + (b3 << 24);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw(e);
		}
	}

	public void stop() {
		_isAlive = false;
	}
	public boolean isPlaying()
	{
		return _isAlive;
	}

	public void pause()
	{
		_isPaused = true;
	}
	public void resume()
	{
		_isPaused = false;
	}
	public boolean isPaused()
	{
		return _isPaused;
	}


	private class ImagePlayer implements Runnable
	{
		private boolean isRunning;

		private byte[][] m_ImageBuf;
		private int[] m_ImageBufLen;
		private int[] m_ImageBufStatus;

		private int write_idx; 
		private int read_idx; 


		private ArrayList<IVideoSink> _videoSinks ;
		ImagePlayer()
		{
			isRunning = true;

			m_ImageBuf = new byte[PublicDefine.MAX_IMAGE_BUF_NUMBER][PublicDefine.MAX_IMAGE_BUF_LEN];
			m_ImageBufLen = new int[PublicDefine.MAX_IMAGE_BUF_NUMBER];
			m_ImageBufStatus = new int[PublicDefine.MAX_IMAGE_BUF_NUMBER];



			write_idx = read_idx = 0; 
			_videoSinks = new ArrayList<IVideoSink>();

		}
		public void setRunning(boolean b)
		{
			isRunning = b;
		}

		public void run()
		{
			while (isRunning)
			{
//				try {
//					//15 frame per sec
//					Thread.sleep(66);
//				} catch (InterruptedException e) {
//				}

				if(m_ImageBufStatus[read_idx] == PublicDefine.BUFFER_FULL)
				{

					byte[] one_frame = new byte [m_ImageBufLen[read_idx]]; 
					System.arraycopy(m_ImageBuf[write_idx],0,one_frame, 0, m_ImageBufLen[read_idx]); 
					
					for(IVideoSink videoSink : _videoSinks) {
						videoSink.onFrame(one_frame, null,0 );
					}
					
					
					m_ImageBufStatus[read_idx] = PublicDefine.BUFFER_EMPTY;
					m_ImageBufLen[read_idx] = 0; 
					//Log.d("mbp", "writing video @"  + m_Image_idx);
					read_idx = (read_idx + 1) % PublicDefine.MAX_IMAGE_BUF_NUMBER;
				}else{
					//Log.d("mbp", "waiting for new buffer");
					try{
						Thread.sleep(10);
					}catch(Exception ex)
					{}
				}



			}
		}
		public void postData(byte [] imgData, int imgLen )
		{

			while(true)
			{
				if(m_ImageBufStatus[write_idx] == PublicDefine.BUFFER_EMPTY)
				{
					m_ImageBufStatus[write_idx] = PublicDefine.BUFFER_PROCESSING;
					System.arraycopy(imgData, 0, m_ImageBuf[write_idx], 0, imgLen);
					m_ImageBufLen[write_idx] = imgLen;

					m_ImageBufStatus[write_idx] = PublicDefine.BUFFER_FULL;
					write_idx = (write_idx + 1) % PublicDefine.MAX_IMAGE_BUF_NUMBER;


					return;
				}else{
					//wait for the next empty buffer ..
					try{
						Thread.sleep(5);
						//todosang:test

					}catch(Exception ex)
					{}
				}
			}
		}

		public void addVideoSink(IVideoSink videoSink) {
			synchronized(_videoSinks) {
				_videoSinks.add(videoSink);
			}
		}
	}
}
