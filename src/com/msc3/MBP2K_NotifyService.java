package com.msc3;

import com.blinkhd.R;
import java.io.IOException;


import android.app.ActivityManager;
import android.app.Service;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
 

/**20121116- 
 * @author phung
 * Created/Revived because of issue #714 
 * Used to create recurring audio alert sound. 
 *
 *
 */
public class MBP2K_NotifyService extends Service
{
 
    private Thread reminder = null; 
    private static final int PUSH_VOX_ID = 2;
    MediaPlayer mMediaPlayer;
    
    @Override
    public void onCreate() {
    	super.onCreate();
    }
 
    
    public void onDestroy()
    {
    	super.onDestroy();
    	Log.d("mbp", "MBP2K_NotifyService: onDestroy()");
    	
    	if (mMediaPlayer != null)
    	{
    		mMediaPlayer.release(); 
    	}
    	
		if (reminder != null)
		{
			reminder.interrupt();
			try {
				reminder.join(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	Log.d("mbp", "MBP2K_NotifyService: onStartCommand()");
        startReminderThread();
         
        return START_STICKY;
    }
 
   
    private  void startReminderThread ()
    {
    	
    	mMediaPlayer = new MediaPlayer();
		try {
			mMediaPlayer.setDataSource(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
			mMediaPlayer.setLooping(false);
			mMediaPlayer.prepare();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
    	if (reminder == null || !reminder.isAlive())
    	{
    		reminder =  new Thread()
    		{
    			private boolean running; 
    			public void run()
    			{
    				
    				running = true; 
    				while (running)
    				{
    					
    					try {
    						Thread.sleep(10000);
    					} catch (InterruptedException e) {
    						e.printStackTrace();
    						Log.d("mbp", "Stop notification ... ");	
    						break;
    					} 
    					playAlert();

    					
    				}
    			}
    		};

			reminder.start(); 
		}
		else
		{
			Log.d("mbp", "reminder Thread is running .. don't do anything");
			
		}

	}
    
    
    
	protected void playAlert() {
		/* 
		 * start playing beep tone??
		 */
		if (mMediaPlayer != null )
		{
			mMediaPlayer.start();
		}
		
	}

	final Messenger mMessenger = new Messenger(new NotifyMsgHandler()); 

	public IBinder onBind(Intent intent) 
	{
		Log.d("mbp", "NotifyService: onBind");

		// mMessenger is not really USED here 
		return mMessenger.getBinder();
	}


	public static final int MSG_NEW_VOX = 1; 
	
	class NotifyMsgHandler extends Handler { // Handler of incoming messages from clients.
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_NEW_VOX:
				//Do nothing... 
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	
	public static boolean isServiceRunning(Context c)
	{
		ActivityManager manager = (ActivityManager) c.getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.msc3.MBP2K_NotifyService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
    
    
}