package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Vector;

import com.msc3.comm.HTTPRequestSendRecv;

import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.util.Base64;
import android.util.Log;

public class CamConfiguration {

	
	
	private String device_ssid;
	private String max_cam;
	
	private String ssid,security_type, pass_string;
	private String key_index, auth_method ;
	private String address_mode;
	private String static_ip_gw, netmask, static_ip;
	private String usr_name, pass_wd;
	private WifiConfiguration wc;
	private Vector<String> device_bssid;
	
	private CamProfile [] cam_profiles;
	private CamChannel [] cam_channels;
	private Vector<String> skipped_device_bssid;
	
	private String master_key;
	
	
	public CamConfiguration (String ssid, String security_type, String pass,
			                 String key_idx, String auth_method, String address_mode,
			                 String gw, String nm, String ip, String usrName, String pwd,
			 String device_ssid, String max_cam)
	{
		
		this.ssid = ssid;
		this.security_type = security_type;
		this.pass_string = pass;
		this.key_index = key_idx;
		this.auth_method = auth_method;
		this.address_mode = address_mode;
		this.static_ip_gw = gw;
		this.static_ip = ip;
		this.netmask = nm;
		
//		this.usr_name = "";
//		this.pass_wd = "";
		this.usr_name = (usrName!=null)?usrName:"";
		this.pass_wd = (pwd!=null)?pwd:"";
		
		
		this.device_ssid = device_ssid;
		this.max_cam = max_cam;
		device_bssid = null;
		cam_profiles = null;
		cam_channels = null;
		this.master_key	 = null;
		skipped_device_bssid = new Vector<String>();
		
	}
	
	public void setMasterKey(String mkey)
	{
		this.master_key = mkey;
	}
	public String getMasterKey()
	{
		return this.master_key ;
	}
	public void setCamProfiles(CamProfile [] cp)
	{
		this.cam_profiles = cp ;
	}

	public void setCamChannels(CamChannel [] cp)
	{
		this.cam_channels = cp ;
	}
	public CamProfile [] getCamProfiles()
	{
		return this.cam_profiles  ;
	}

	public CamChannel [] getCamChannels( )
	{
		return this.cam_channels;
	}
	public Vector<String> getSkippedDeviceList()
	{
		return this.skipped_device_bssid;
	}
	
	
	public void setDeviceList(Vector<String> list)
	{
		device_bssid = list;
	}
	public Vector<String> getDeviceBSSIDList()
	{
		return device_bssid;
	}
	public void setWifiConf (WifiConfiguration wc)
	{
		this.wc = wc;
		
		Log.d("mbp", "setWifiConf: wc is NULL? " + (wc==null )); 
		
	}
	
	/*
	 *  "WPA/WPA2", "WEP", "OPEN"
	 */
	public String security_type() {
		return security_type;
	}
	public String device_ssid()
	{
		return device_ssid;
	}
	
	public String max_cam()
	{
		return max_cam;
	}
	
	public String ssid()
	{
		return ssid;
	}
	
	public String pass_string()
	{
		return pass_string;
	}

	public String key_index()
	{
		return key_index;
	}
	public String auth_method() {
		return auth_method;
	}

	public String address_mode() {
		return address_mode;
	}
	
	public String static_ip_gw() {
		return static_ip_gw; 
	}
	
	public String netmask () {
		return netmask;
	}
	public String static_ip() {
		return static_ip;
	}
	
	public String getHttpUsr() {
		return usr_name;
	}
	public String getHttpPass() {
		return pass_wd;
	}
	public WifiConfiguration wc() 
	{
		Log.d("mbp", "wc is NULL? " + (wc==null )); 
		return wc;
	}
	
	public String build_setup_request()
	{
		String setup_request=null ;
		setup_request = build_setup_core_request() ;
		return setup_request;
	}
	
	
	public String build_setup_core_request()
	{
		String setup_request=null ;

		/* setup in infra mode */
		String wifi_mode = "1";

		/* channel in adhoc mode */
		String adhoc_chan = "00";

		String auth_mode = null, key_index = null ;
		
//		Log.d("mbp", " sec type : " + security_type);
		
		if (security_type.equalsIgnoreCase("WEP"))
		{
			/* use Wep */
			auth_mode =(auth_method.equalsIgnoreCase("Open"))?"0":"1";
			
			key_index = String.format("%d",Integer.parseInt(this.key_index)-1 );
		}
		else if (security_type.equalsIgnoreCase("OPEN"))
		{
			auth_mode ="0";
			key_index = "0";
		}
		else 
		{
			/* use WPA-PSK */
			auth_mode ="2";
			key_index = "0";
		}

		
		/* DHCP*/
		String address_mode = "0";

		/* len of SSID*/
//		String ssid_len = String.format("%03d", ssid.length());
		/*
		 * 20130604: hoang: use byte[] length
		 */
		String ssid_len = String.format("%03d", ssid.getBytes().length);

		/* len of security key */
		String sec_key_len = String.format("%02d", pass_string.length());

		/* Dont use static ip so static ip len = 0*/
		String static_ip_len = "00";

		String static_ip_netmask ="00";
		String static_ip_gw_len = "00";
		String port = "0";

		/*20120313: temporary use empty user/pass here
		String usr = "";//this.usr_name;
		String pass = "";//this.pass_pwd;
		*/
		/*20120322: enable use and pass */
		
		String usr_name_len = String.format("%02d",this.usr_name.length());
		String passwd_len = String.format("%02d", this.pass_wd.length());
		/* 20130121: hoang:
		 * encode setup data in URL
		 */
		String setup_value = wifi_mode + adhoc_chan + auth_mode + key_index + 
				address_mode + ssid_len + sec_key_len + static_ip_len + 
				static_ip_netmask +  static_ip_gw_len + port +usr_name_len + passwd_len +
				/* add the values */
				this.ssid+ this.pass_string + this.usr_name + this.pass_wd ;

		Log.d("mbp", "Encode setup data");
		try {
			setup_value = URLEncoder.encode(setup_value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
//		setup_request = "/?action=command&command=setup_wireless_save&setup=" +
//				/* set some parameters */
//				wifi_mode + adhoc_chan + auth_mode + key_index + 
//				address_mode + ssid_len + sec_key_len + static_ip_len + 
//				static_ip_netmask +  static_ip_gw_len + port +usr_name_len + passwd_len +
//				/* add the values */
//				this.ssid+ this.pass_string + this.usr_name + this.pass_wd ;
		setup_request = "/?action=command&command=setup_wireless_save&setup=" +
						setup_value;

		return setup_request;
	}
	
}
