package com.msc3;

import com.blinkhd.TalkbackFragment;

import cz.havlena.ffmpeg.ui.FFMpegPlayerActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.os.Handler;
import android.os.Message;

/* finger press down or move: Start streaming voice from hp --> irabot
 *        release : stop streaming voice 
 * */
public class TalkBackTouchListener implements  OnTouchListener
{
	private Handler mHandler;
	private boolean isPressed;
	private boolean isTalkBackEnabled;
	private Thread debounce_thread;
	private long last_time = 0;
	
	public TalkBackTouchListener(Handler c)
	{
		mHandler = c;
		isPressed = false;
		isTalkBackEnabled = false;
		debounce_thread = null;
	}
	public boolean onTouch(View v, MotionEvent event)
	{
		
		switch(event.getAction() & MotionEvent.ACTION_MASK)
		{
		case MotionEvent.ACTION_DOWN:
			long current_time = System.currentTimeMillis();
			if (last_time == 0 || current_time - last_time > 2000)
			{
				last_time = current_time;
				//synchronized (this) 
				{
					isPressed = true;
					isTalkBackEnabled = false;
				}
				
				//when start touching .. send first message to enable the text .. 
				mHandler.sendMessage(Message.obtain(
						mHandler, TalkbackFragment.MSG_LONG_TOUCH_START));
				
				if ( (debounce_thread == null) || !debounce_thread.isAlive())
				{
					DebounceRunnable debounce = new DebounceRunnable(isPressed);
					debounce_thread =new Thread(debounce," DEBOUNCE"); 
					debounce_thread.start();
				}
			}
			
			break;
		case MotionEvent.ACTION_UP:
			if (isPressed == true)
			{
				//synchronized (this) 
				{
					isPressed = false;
					if (isTalkBackEnabled)
					{
						mHandler.sendMessage(Message.obtain(
								mHandler, TalkbackFragment.MSG_LONG_TOUCH_RELEASED));
					}
					else
					{
						mHandler.sendMessage(Message.obtain(
								mHandler, TalkbackFragment.MSG_SHORT_TOUCH_RELEASED));
					}

				}
			}
			
			break;
		case MotionEvent.ACTION_MOVE:
			break;
			
		default:
			break;

		}
		
		return true;
	}
	
	/** Show an event in the LogCat view, for debugging */
	@SuppressWarnings("unused")
	private void dumpEvent(MotionEvent event) {
	   String names[] = { "DOWN" , "UP" , "MOVE" , "CANCEL" , "OUTSIDE" ,
	      "POINTER_DOWN" , "POINTER_UP" , "7?" , "8?" , "9?" };
	   StringBuilder sb = new StringBuilder();
	   int action = event.getAction();
	   int actionCode = action & MotionEvent.ACTION_MASK;
	   sb.append("event ACTION_" ).append(names[actionCode]);
	   if (actionCode == MotionEvent.ACTION_POINTER_DOWN
	         || actionCode == MotionEvent.ACTION_POINTER_UP) {
	      sb.append("(pid " ).append(
	      action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
	      sb.append(")" );
	   }
	   sb.append("[" );
	   for (int i = 0; i < event.getPointerCount(); i++) {
	      sb.append("#" ).append(i);
	      sb.append("(pid " ).append(event.getPointerId(i));
	      sb.append(")=" ).append((int) event.getX(i));
	      sb.append("," ).append((int) event.getY(i));
	      if (i + 1 < event.getPointerCount())
	         sb.append(";" );
	   }
	   sb.append("]" );
	   Log.d("mbp", sb.toString());
	}
	
	private class DebounceRunnable implements Runnable {
		private boolean mMode ;
		DebounceRunnable(boolean init_mode)
		{
			mMode = init_mode;
		}

		public void run()
		{

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//synchronized(TalkBackTouchListener.this)
			{
				if (mMode == isPressed)
				{
					TalkBackTouchListener.this.mHandler.sendMessage(Message.obtain(
							TalkBackTouchListener.this.mHandler, TalkbackFragment.MSG_LONG_TOUCH));
					isTalkBackEnabled = true;
				}
			}



		}
	}

}
