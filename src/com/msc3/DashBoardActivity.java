package com.msc3;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.R;
import com.blinkhd.gcm.AlertData;
import com.blinkhd.gcm.GcmIntentService;
import com.blinkhd.playback.EventManagerActivity;
//import com.blinkhd.playback.NewEventManangerUI;
import com.blinkhd.playback.PlaybackManagerActivity;
import com.discovery.LocalScanForCameras;
import com.discovery.ScanProfile;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.msc3.registration.SingleCamConfigureActivity;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;
import com.nxcomm.jstun_android.RtspStunBridgeService;
import com.nxcomm.jstun_android.RtspStunBridgeService.RtspStunBridgeServiceBinder;
import com.nxcomm.meapi.App;

import cz.havlena.ffmpeg.ui.FFMpegPlaybackActivity;
import cz.havlena.ffmpeg.ui.FFMpegPlayerActivity;

public class DashBoardActivity extends Activity implements Handler.Callback, ICameraScanner
{

	private static final int DIALOG_NO_CAM_IN_THIS_ACCOUNT = 19;
	private static final int DIALOG_NEED_TO_LOGIN = 22;
	private static final int DIALOG_NO_OFFLINE_DATA = 23;
	public static final int  DIALOG_STORAGE_UNAVAILABLE = 24;
	
	private static final int DIALOG_MAX_CAM_IN_THIS_ACCOUNT = 26;
	private static final int DIALOG_CONNECT_TO_WIFI_NETWORK_TO_ADD_THE_CAMERA = 35;


	private static final int REQUEST_START_CAMERA_MENU = 0x200;
	private static final int REQUEST_ADD_CAMERA_WHEN_USING_3G = 0x201;
	private static final int REQUEST_VIEW_LOCAL_CAMERA = 0x202;
	private static final int REQUEST_VIEW_REMOTE_CAMERA = 0x203;

	//Result code that ViewCameraActivity can set before finishing
	public  static final int DONT_RESCAN_CAMERA = 100; 
	public  static final int RESCAN_CAMERA = 101; 

	public static final String string_voxDeviceAddr = "vox_device_mac_address";
	public static final String CAMCHANNEL_SHOWING_CHANNEL = "camera_channel";


	private GridView cam_list;
	/* To restore data from */
	private CamChannel[] restored_channels;
	private CamProfile[] restored_profiles;


	private CamChannel selected_channel;
	private int access_mode;

	private LocalScanForCameras scan_task;

	private boolean show_add_cam_dialog_once ; 

	private boolean shouldRescan; 
	
	private Tracker tracker;
	private EasyTracker easyTracker;
	
	
	

	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(DashBoardActivity.this);
		tracker = easyTracker.getTracker();

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		selected_channel = null;
		access_mode = SetupData.ACCESS_VIA_LAN;


		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		/* clear usr/pass field to force re-auth */
		editor.putString(PublicDefine.PREFS_TRANSIENT_USERNAME, null);
		editor.putString(PublicDefine.PREFS_TRANSIENT_PASSWD, null);
		editor.putInt(PublicDefine.PREFS_CURRENT_CAMERA_MODE, -1);
		// Commit the edits!
		editor.commit();

		show_add_cam_dialog_once = false; 
		
		shouldRescan = true; 


	}

	public void onStart()
	{
		super.onStart();
		
	
		
		tracker.sendView("Camera List Screen");
		Log.d("mbp", "DashBoard: onStart()");

		/* Special case: MBP2k, 1k */
		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
				|| phonemodel.equals(PublicDefine.PHONE_MBP1k)) 
		{
			/* Stop Camera Connectivity Detector service if it's running. */
			if (FirstTimeActivity.isCamDetectServiceRunning(this))
			{
				Log.d("mbp", "Stop Cam Detector");
				Intent i = new Intent(this,CameraDetectorService.class);
				stopService(i);
			}
			
			startVoxService();
		}

		
		

		if (shouldRescan == true)
		{
			if  (restore_session_data() == true) {
				// do not do udp broadcast scan for camera when using 3G/mobile
				// data
				if ( (ConnectToNetworkActivity.haveInternetViaOtherMedia(this) == true) )
				{
					updateScanResult(null, LocalScanForCameras.SUCCESS, -1);
				}
				else 
				{
					// setup the camera list
					//setup_camera_list(restored_channels);

					new Thread(new Runnable() {
						public void run() {
							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									scan_task = new LocalScanForCameras(
											DashBoardActivity.this,
											new Handler(DashBoardActivity.this),
											DashBoardActivity.this);
									scan_task.setShouldGetSnapshot(false);
									// Callback: updateScanResult()
									scan_task.startScan(restored_profiles);
								}
							});
						}
					}).start();

				}
			} 
			else
			{

				showDialog(DIALOG_NO_OFFLINE_DATA);
			}
		} //if (shouldRescan = false)		
		
	}

	public void onStop()
	{
		super.onStop();
		
		startVoxService();

	}





	public void onDestroy()
	{
		
				
		Log.d("mbp", "DashBoard onDestroy...");
		
	
		
		super.onDestroy(); 
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		View v = findViewById(R.id.cam_list_root_view);
		if (v != null && v.isShown()) {
			// currently showing the cameralistView
			setup_camera_list(restored_channels);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_VIEW_LOCAL_CAMERA)
		{

			if ((ConnectToNetworkActivity.haveInternetViaOtherMedia(this) == false ) && 
					(resultCode == RESCAN_CAMERA))
			{
				shouldRescan = true; 
			}
			
			if (resultCode == DONT_RESCAN_CAMERA)
			{
				shouldRescan = false; 
			}
		}
		
		if (requestCode == REQUEST_VIEW_REMOTE_CAMERA)
		{
			if ((ConnectToNetworkActivity.haveInternetViaOtherMedia(this) == false ) && 
					(resultCode == RESCAN_CAMERA))
			{
				shouldRescan = true; 
			}
			
			if (resultCode == DONT_RESCAN_CAMERA)
			{
				shouldRescan = false; 
			}
		}

		if (requestCode == REQUEST_ADD_CAMERA_WHEN_USING_3G) {
			if (resultCode == RESULT_OK) {
				finish();
			}
		}
	}
	
	private int numberOfLocalAvailableCamera(CamProfile[] cps) {
		int count = 0;
		for (int i = 0; i < cps.length; i++) {
			if (cps[i].isInLocal()) {
				count++;
			}
		}
		return count;
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.account_logout_menu, menu);
		// menu.
		if (numberOfLocalAvailableCamera(restored_profiles) <= 1) {
			// Disable item
			MenuItem scan_item = menu.findItem(R.id.scan);
			scan_item.setVisible(false);
			scan_item.setEnabled(false);

		}

		// disable Logout button while in offline mode
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		boolean offlineMode = settings.getBoolean(
				PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
		if (offlineMode == true) {
			// Disable item
			MenuItem logout_item = menu.findItem(R.id.logout);
			logout_item.setVisible(false);
			logout_item.setEnabled(false);
		}

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId())
		{
		case R.id.scan:/* scan button */
			if (numberOfLocalAvailableCamera(restored_profiles) >1 ) 
			{
				Intent quickView = new Intent(this,QuickViewCameraActivity.class);
				startActivity(quickView);
			}
			return true;
		case R.id.logout: /*logout */
			
			onUserLogout();
			/*
			 * 20130418: hoang: issue 1742
			 * stop VoiceActivationService if user logged out
			 */
			stopVoxService();
			
			Log.d("mbp", "DashBoardActivity onOptionsItemSelected..");
			return true;
		case R.id.get_log_item:// Get log
			LogCollectorTask logC = new LogCollectorTask(this,
					getExternalFilesDir(null));
			logC.execute(new ArrayList<String>());
			return true;

		case R.id.about_item:
			onInfo();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		Spanned msg;
		switch (id) {

		case DIALOG_NO_CAM_IN_THIS_ACCOUNT:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_bm_in_acccount)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_NEED_TO_LOGIN:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources()
					.getString(R.string.EntryActivity_not_login)
					+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_NO_OFFLINE_DATA:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.no_offline_data_found_please_try_to_login_to_server_)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					finish();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					Intent homeScreen = new Intent(
							DashBoardActivity.this,
							FirstTimeActivity.class);
					homeScreen
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeScreen);
					finish();
				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_MAX_CAM_IN_THIS_ACCOUNT:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.please_remove_one_camera_from_the_current_list_before_adding_the_new_one)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_CONNECT_TO_WIFI_NETWORK_TO_ADD_THE_CAMERA:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.please_connect_to_wifi_network_to_add_the_camera)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.Yes),
					new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					Intent intent = new Intent(
							DashBoardActivity.this,
							ConnectToNetworkActivity.class);
					intent.putExtra(
							ConnectToNetworkActivity.bool_AddCameraWhenUsing3G,
							true);
					DashBoardActivity.this.startActivityForResult(
							intent,
							REQUEST_ADD_CAMERA_WHEN_USING_3G);
				}
			})
			.setNegativeButton(getResources().getString(R.string.No),
					new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		default:
			break;
		}
		return null;
	}



	private void setup_camera_list(final CamChannel[] chs) 
	{
		setContentView(R.layout.bb_is_camera_channel_selection_form);
//		RelativeLayout status_bar = (RelativeLayout) findViewById(R.id.bm_setup);
//		if (status_bar != null) {
//			status_bar.setBackgroundColor(R.drawable.camlist_title_bar);
//		}

		// Show status bar
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


		if (selected_channel != null) {
			Log.d("mbp", "setup_list_cam_xxxx; cancelRemoteConnection");
			selected_channel.cancelRemoteConnection(); // Reset status variables
		}

		this.selected_channel = null;


		CamChannelListAdapter cam_list_adaptor = new CamChannelListAdapter(
				this, chs);

		cam_list = (GridView) findViewById(R.id.cam_list);
		cam_list.setAdapter(cam_list_adaptor);



		if (cam_list_adaptor.getValidChannelCount() == 0 && 
				(show_add_cam_dialog_once == false)   ) {
			Log.d("mbp", "No camera in account");
			showDialog(DIALOG_NO_CAM_IN_THIS_ACCOUNT);
			show_add_cam_dialog_once = true; // only allow first time entry
		}


		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		boolean offlineMode = settings.getBoolean(
				PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);

		if (offlineMode == false)
		{

			/* setup the title */
			TextView usrId = (TextView) findViewById(R.id.textTitle);
			ImageView lower_bar = (ImageView) findViewById(R.id.lower_bar);
			lower_bar.setVisibility(View.INVISIBLE);
			if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == 
					Configuration.ORIENTATION_LANDSCAPE)
			{
				// hide textTitle and show textTitle_landscape
				TextView usrId_hide = (TextView) findViewById(R.id.textTitle);
				usrId_hide.setVisibility(View.INVISIBLE);
				usrId = (TextView) findViewById(R.id.textTitle_landscape);
				usrId.setVisibility(View.VISIBLE);
			} 
			else if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_PORTRAIT) == 
					Configuration.ORIENTATION_PORTRAIT)
			{
				// if the usrEmail too long,hide textTitle_landscape and show
				// textTitle
				TextView usrId_hide = (TextView) findViewById(R.id.textTitle_landscape);
				usrId_hide.setVisibility(View.INVISIBLE);
				usrId = (TextView) findViewById(R.id.textTitle);
				usrId.setVisibility(View.VISIBLE);
			}

			// usrEmail can be NULL here if we skipp the layout because of AUTO
			// CONNECT FLAG
			if (usrId != null) {
				settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				String saved_usr = settings.getString(
						PublicDefine.PREFS_SAVED_PORTAL_ID, null);
				usrId.setText(saved_usr);
			}

			final boolean have3GConnection = ConnectToNetworkActivity
					.haveInternetViaOtherMedia(this);

			/* Setup add Refresh button */
			ImageButton buttoRefresh = (ImageButton) findViewById(R.id.buttonRefreshDevice);
			buttoRefresh.setVisibility(View.VISIBLE);
			buttoRefresh.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					if (scan_task != null) 
					{
						scan_task.stopScan();
					}
					
					Intent intent = new Intent(DashBoardActivity.this,
							FirstTimeActivity.class);
					intent.putExtra(FirstTimeActivity.bool_InfraMode, true);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);

				}
			});

			/* Setup add Camera button */
			ImageButton buttonAdd = (ImageButton) findViewById(R.id.buttonAdd);
			buttonAdd.setVisibility(View.VISIBLE);
			buttonAdd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (have3GConnection == true)
					{
						showDialog(DIALOG_CONNECT_TO_WIFI_NETWORK_TO_ADD_THE_CAMERA);
					} 
					else 
					{
						//if there are 4 cams in the list-
						//     dont allow user to add
						if (restored_profiles.length == 4) 
						{
							showDialog(DIALOG_MAX_CAM_IN_THIS_ACCOUNT);
							return;
						}
						
						if (scan_task != null) 
						{
							scan_task.stopScan();
						}

						SharedPreferences settings = getSharedPreferences(
								PublicDefine.PREFS_NAME, 0);
						String saved_token = settings.getString(
								PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

						Intent cam_conf = new Intent(DashBoardActivity.this,
								SingleCamConfigureActivity.class);
						cam_conf.putExtra(
								SingleCamConfigureActivity.str_userToken,
								saved_token);
						startActivity(cam_conf);
						finish();
					}
				}
			});

		} 
		else // OFFLINE mode
		{
			RelativeLayout offlineModeRoot = (RelativeLayout) findViewById(R.id.offline_mode_grp);

			if (offlineModeRoot != null) {
				offlineModeRoot.setVisibility(View.VISIBLE);

				Button checkNow = (Button) offlineModeRoot
						.findViewById(R.id.buttonCheckNow);
				if (checkNow != null) {
					checkNow.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent new_login = new Intent(DashBoardActivity.this,
									LoginOrRegistrationActivity.class);
							new_login
							.putExtra(
									LoginOrRegistrationActivity.bool_shouldNotAutoLogin,
									false);

							new_login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(new_login);
							DashBoardActivity.this.finish();
						}
					});
				}

			}

			/* setup the title */
			TextView title = (TextView) findViewById(R.id.textTitle);
			ImageView lower_bar = (ImageView) findViewById(R.id.lower_bar);
			lower_bar.setVisibility(View.INVISIBLE);
			if (title != null) {
				title.setText(getResources().getString(
						R.string.EntryActivity_offline_mode));
			}

			/* Setup add Refresh button */
			ImageButton buttoRefresh = (ImageButton) findViewById(R.id.buttonRefreshDevice);
			buttoRefresh.setVisibility(View.INVISIBLE);

			/* Setup add Camera button */
			ImageButton buttonAdd = (ImageButton) findViewById(R.id.buttonAdd);
			buttonAdd.setVisibility(View.VISIBLE);
			// buttonAdd.setBackgroundColor(R.color.button_gray);
			buttonAdd.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					showDialog(DIALOG_NEED_TO_LOGIN);
				}
			});

		}

	}

	/**
	 * 
	 * restored_profiles - from setupdata.camprofiles which is synced with the
	 * online list prior to this
	 */

	public void updateScanResult(ScanProfile[] results, int status, int index) {

		boolean voxShouldHoldWakeLock = false;
//		if ((results != null) && 
//				(restored_profiles != null) &&
//				(restored_profiles.length > 0)
//				) 
//		{
//			Intent intent = new Intent(this, HomeSreenActivity);
//			intent.putExtra(name, value)
//		}
//			for (int i = 0; i < restored_profiles.length; i++) {
//				// Default to offline
//				if (restored_profiles[i] == null) {
//					continue;
//				}
//				
//				//if camera has updated location and in local --> don't update again
//				if (restored_profiles[i].hasUpdatedLocation())
//				{
//					continue;
//				}
//				
//				restored_profiles[i].setInLocal(false);
//				for (int j = 0; j < results.length; j++) {
//					if (restored_profiles[i].get_MAC().equalsIgnoreCase(
//							results[j].get_MAC())) {
//
//						// ONLINE only when we have a match MAC in local
//						restored_profiles[i].setInLocal(true);
//						// Update the new IP
//						restored_profiles[i].setInetAddr(results[j]
//								.get_inetAddress());
//						// Update the new port
//						restored_profiles[i].setPort(results[j]
//								.get_port());
//						//checkVoxStatus(restored_profiles[i]);
//						
//						break;
//					}
//				}
//			} // end for
//
//			/* Update the channel list */
//			for (int i = 0; i < restored_channels.length; i++) {
//				if (restored_channels[i].getCamProfile() != null) {
//
//					for (int j = 0; j < restored_profiles.length; j++) {
//
//						if (restored_channels[i].getCamProfile().equals(
//								restored_profiles[j])) {
//							restored_channels[i]
//									.setCamProfile(restored_profiles[j]);
//							restored_profiles[j]
//									.setChannel(restored_channels[i]);
//							restored_profiles[j].bind(true);
//							
//							break;
//						}
//					}
//
//				}
//			}
//
//			// 20120608: store data to offline storage
//			save_session_data();
//
//			String phonemodel = android.os.Build.MODEL;
//			if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
//					|| phonemodel.equals(PublicDefine.PHONE_MBP1k)) {
//				// set a default HOME WIFI
//				SharedPreferences settings = getSharedPreferences(
//						PublicDefine.PREFS_NAME, 0);
//
//				WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//				if ((w != null) && (w.isWifiEnabled())
//						&& (w.getConnectionInfo() != null)) {
//					String _wifi_name = w.getConnectionInfo().getSSID();
//
//					Log.d("mbp",
//							"EntryAct: Store default wifi for camera alert : "
//									+ _wifi_name);
//					SharedPreferences.Editor editor = settings.edit();
//					editor.putString(
//							PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_HOME_WIFI,
//							_wifi_name);
//					editor.commit();
//				}
//			}
//
//			// 20120606: check if all camera does not have Vox enabled - disable
//			// wakelock
//			voxShouldHoldWakeLock = false;
//			for (int i = 0; i < restored_profiles.length; i++) {
//				// Default to offline
//				if (restored_profiles[i] == null) {
//					continue;
//				}
//				if (restored_profiles[i].isVoxEnabled()) {
//					voxShouldHoldWakeLock = true;
//				}
//
//			} // end for
//
//		} 
//		else // (results == null) || (restore_profiles ==null) ||
//			// restore_profile.len =0
//		{
//
//			// NO INFRA camera found
//			for (int i = 0; i < restored_channels.length; i++) {
//
//				if ((restored_channels[i] != null)
//						&& (restored_channels[i].getCamProfile() != null)) {
//					restored_channels[i].getCamProfile().setInLocal(false);
//				}
//			}
//
//		}
//
//
//		// 20120606: update preference - later vox service will check this flag
//		// before holding wakelock
//		SharedPreferences settings = getSharedPreferences(
//				PublicDefine.PREFS_NAME, 0);
//		SharedPreferences.Editor editor = settings.edit();
//		editor.putBoolean(PublicDefine.PREFS_VOX_SHOULD_TAKE_WAKELOCK,
//				voxShouldHoldWakeLock);
//		editor.commit();


		//setup_camera_list(restored_channels);
	}


	/*
	 * 20130131: hoang: update scan result for each camera
	 */
	private synchronized void updateScanResultForEachCamera(ScanProfile result) {
		
		if ((result != null) && (restored_profiles != null)
				&& (restored_profiles.length > 0)) 
		{
			
			for (int i = 0; i < restored_profiles.length; i++) 
			{
				if (restored_profiles[i] == null) {
					continue;
				}

				if (restored_profiles[i].get_MAC().equalsIgnoreCase(
						result.get_MAC())) {
					
					if (restored_profiles[i].hasUpdatedLocation())
					{
						return;
					}
					
					restored_profiles[i].setInLocal(result.isInLocal());
					restored_profiles[i].setInetAddr(result.get_inetAddress());
					restored_profiles[i].setPort(result.get_port());
					//checkVoxStatus(restored_profiles[i]);
					
					break;
				}
			}

			/* Update channel list */
			for (int i = 0; i < restored_channels.length; i++) {
				if (restored_channels[i].getCamProfile() != null) {
					for (int j = 0; j < restored_profiles.length; j++) {
						if (restored_channels[i].getCamProfile().equals(
								restored_profiles[j])) {
							restored_channels[i]
									.setCamProfile(restored_profiles[j]);
							restored_profiles[j]
									.setChannel(restored_channels[i]);
							restored_profiles[j].bind(true);
							break;
						}
					}

				}
			}
			
			//invalidate & redraw the camera list .. with the updated list 
			cam_list.invalidateViews();

		}

	}

	
	/**
	 * 
	 * Setup the Settings Button for a channel
	 * 
	 * @param channel - camera channel to setup
	 * @param itemView - the view that shows this channel 
	 * 
	 */
	public void setupSettingButton(CamChannel channel, View itemView)
	{
		CamProfile camera = channel.getCamProfile(); 
		ImageButton camSettings = (ImageButton) itemView
				.findViewById(R.id.list_row_camera_setting_camSettingBtn);
		
		if (camera.isInLocal()) {
			final String camera_url = camera.get_inetAddress()
					.getHostName() + ":" + camera.get_port();
			final String camera_mac = camera.get_MAC();
			final String camera_name = camera.getName();

			camSettings.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					if (scan_task != null) 
					{
						scan_task.stopScan();
					}

					SharedPreferences settings = getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);
					String saved_token = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

					boolean offlineMode = settings
							.getBoolean(
									PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE,
									false);

					Intent i = new Intent(DashBoardActivity.this,CameraMenuActivity.class);
					i.putExtra(CameraMenuActivity.bool_isLoggedIn,!offlineMode);
					i.putExtra(CameraMenuActivity.str_userToken, saved_token);
					i.putExtra(CameraMenuActivity.str_deviceUrl,camera_url);
					i.putExtra(CameraMenuActivity.str_deviceMac,camera_mac);
					i.putExtra(CameraMenuActivity.str_deviceName,camera_name);
					i.putExtra(CameraMenuActivity.bool_shouldGotoShortSubmenu,true);
					i.putExtra(CameraMenuActivity.bool_cameraInLocal,true);
					// mContext.startActivity(i);
					startActivityForResult(i,DashBoardActivity.REQUEST_START_CAMERA_MENU);

				}
			});

		}
		else /* remote camera */
		{
			final String camera_mac = camera.get_MAC();
			final String camera_name = camera.getName();

			camSettings.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (scan_task != null) 
					{
						scan_task.stopScan();
					}

					SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
					String saved_token = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

					boolean offlineMode = settings.getBoolean(
									PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE,
									false);

					Intent i = new Intent(DashBoardActivity.this,CameraMenuActivity.class);
					i.putExtra(CameraMenuActivity.bool_isLoggedIn,!offlineMode);
					i.putExtra(CameraMenuActivity.str_userToken, saved_token);
					i.putExtra(CameraMenuActivity.str_deviceMac,camera_mac);
					i.putExtra(CameraMenuActivity.str_deviceName,camera_name);
					i.putExtra(
							CameraMenuActivity.bool_shouldGotoShortSubmenu,
							true);
					startActivityForResult(i,
							DashBoardActivity.REQUEST_START_CAMERA_MENU);
				}
			});
		}
	}
	
	/**
	 * Setup "View" button 
	 * 
	 * @param channel - channel to setup for
	 * @param itemView - view that show the channel 
	 */
	public void setupViewButton(final CamChannel channel, View itemView)
	{
		CamProfile camera = channel.getCamProfile(); 
		Button buttonView = (Button) itemView.findViewById(R.id.buttonView);
		Button buttonViewEvents = (Button) itemView.findViewById(R.id.buttonPlayList);
		
		
		buttonViewEvents.getBackground().setColorFilter(0xff7fae00,
				android.graphics.PorterDuff.Mode.MULTIPLY);
		
		buttonView.getBackground().setColorFilter(0xff7fae00,
				android.graphics.PorterDuff.Mode.MULTIPLY);
		// set background color into grey
		if (camera.isInLocal() == false
				&& camera.isReachableInRemote() == false) {
			buttonView.setEnabled(false);
			buttonView.getBackground().setColorFilter(0xff575757,
					android.graphics.PorterDuff.Mode.MULTIPLY);
		}
		buttonViewEvents.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				CamChannel s_channel = channel;
				
				if (s_channel.getCamProfile() != null)
				{
					if (scan_task != null) 
					{
						scan_task.stopScan();
					}

					String mac = s_channel.getCamProfile().get_MAC().replace(":","");
					Intent intent = new Intent(DashBoardActivity.this,
								PlaybackManagerActivity.class);
					intent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA, mac);
					//intent.putExtra(name, value)
					startActivity(intent);
				}
			}
		
		});
		buttonView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				CamChannel s_channel = channel;
				
				if (s_channel.getCamProfile() != null)
				{
					if (scan_task != null) 
					{
						scan_task.stopScan();
					}

					String camModel = s_channel.getCamProfile().getModel();
					camModel = CamProfile.MODEL_ID_MBP836;
					
					
					if (s_channel.getCamProfile().isInLocal())
					{ 
						/***** LOCAL **************************/
						// network
						selected_channel = s_channel;

						
						if (!camModel.equalsIgnoreCase(CamProfile.MODEL_ID_MBP836))
						{
							Intent viewCam = new Intent(DashBoardActivity.this, ViewCameraActivity.class);
							viewCam.putExtra(CAMCHANNEL_SHOWING_CHANNEL,channel);
							startActivityForResult(viewCam, REQUEST_VIEW_LOCAL_CAMERA);
						}
						else
						{
							Intent viewCam = new Intent(DashBoardActivity.this, FFMpegPlayerActivity.class);
							viewCam.putExtra(CAMCHANNEL_SHOWING_CHANNEL,channel);
							startActivityForResult(viewCam, REQUEST_VIEW_LOCAL_CAMERA);
						}
						
						
						//finish();
					} 
					else
					{
						/***** REMOTE *************************/
						
						SharedPreferences settings = getSharedPreferences(
								PublicDefine.PREFS_NAME, 0);
						boolean offlineMode = settings
								.getBoolean(
										PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE,
										false);
						if (offlineMode)
						{
							showDialog(DIALOG_NEED_TO_LOGIN);
						} 
						else
						{
							if (ConnectToNetworkActivity.haveInternetViaOtherMedia(
									DashBoardActivity.this) == true)
							{
								tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY,
										"View Remote Cam Total", "View Remote Cam - Mobile Data", null);
							}
							else
							{
								tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY,
										"View Remote Cam Total", "View Remote Cam - WiFi", null);
							}
							
							selected_channel = s_channel;

							SharedPreferences.Editor editor = settings
									.edit();
							editor.putInt(PublicDefine.PREFS_VQUALITY,
									PublicDefine.RESOLUTON_QVGA);
							editor.commit();
							
							
							
							if (!camModel.equalsIgnoreCase(CamProfile.MODEL_MBP36N))
							{
								Intent viewCam = new Intent(DashBoardActivity.this, ViewCameraActivity.class);
								viewCam.putExtra(CAMCHANNEL_SHOWING_CHANNEL,channel);
								
								startActivityForResult(viewCam, REQUEST_VIEW_REMOTE_CAMERA);
							}
							else
							{
								Intent viewCam = new Intent(DashBoardActivity.this, FFMpegPlayerActivity.class);
								viewCam.putExtra(CAMCHANNEL_SHOWING_CHANNEL,channel);
								
								startActivityForResult(viewCam, REQUEST_VIEW_REMOTE_CAMERA);
							}
							
							//finish(); 
							
						}

					}
				}

			}
		});
		
	}
	
	public void updateAlertForChannel(CamChannel camChannel, View itemView) 
	{
		CamProfile camera = camChannel.getCamProfile(); 
		ImageView imageView; 
		int alertType = AlertData.getAlertForCamera(camera.get_MAC(),getExternalFilesDir(null));
		if (alertType == 0) {
			// NO alert
			imageView = (ImageView) itemView
					.findViewById(R.id.camAlertVoice);
			imageView.setVisibility(View.INVISIBLE);
			imageView = (ImageView) itemView
					.findViewById(R.id.camAlertTemp);
			imageView.setVisibility(View.INVISIBLE);
		} 
		else
		{
			if ((alertType & VoxMessage.VOX_TYPE_VOICE) == VoxMessage.VOX_TYPE_VOICE) 
			{
				imageView = (ImageView) itemView
						.findViewById(R.id.camAlertVoice);
				imageView.setVisibility(View.VISIBLE);
			}

			if (((alertType & VoxMessage.VOX_TYPE_TEMP_LO) == VoxMessage.VOX_TYPE_TEMP_LO)
					|| ((alertType & VoxMessage.VOX_TYPE_TEMP_HI) == VoxMessage.VOX_TYPE_TEMP_HI)) 
			{
				imageView = (ImageView) itemView
						.findViewById(R.id.camAlertTemp);
				imageView.setVisibility(View.VISIBLE);
			}
		}
		
	}
	
	
	
	

	public boolean handleMessage(Message msg) 
	{

		switch (msg.what) 
		{
		case LocalScanForCameras.SCAN_CAMERA_PROGRESS:
			if (msg.obj != null) 
			{
				ScanProfile scan_result = (ScanProfile) msg.obj;
				updateScanResultForEachCamera(scan_result);
			}
			break;

		default:
			break;
		}

		return false;
	}

	/* mainly to save the ip addresses */
	private void save_session_data()
	{
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, true);
		editor.commit();
		SetupData savedData = new SetupData(access_mode, "NA",
				restored_channels, restored_profiles);
		savedData.save_session_data(getExternalFilesDir(null));
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
		editor.commit();
	}

	/* restore functions */
	private boolean restore_session_data() {

		/*
		 * 20120223: save some session data like melody, vox etc.. because after
		 * restore these data is erased
		 */
		CamChannel[] temp = restored_channels;

		SetupData savedData = new SetupData();
		try {
			if (savedData.restore_session_data(getExternalFilesDir(null))) {
				access_mode = savedData.get_AccessMode();
				restored_channels = savedData.get_Channels();
				restored_profiles = savedData.get_CamProfiles();

				if (temp != null) {
					for (int i = 0; i < temp.length; i++) {
						if (temp[i] != null) {
							for (int j = 0; j < restored_channels.length; j++) {
								if (restored_channels[j] != null) {
									CamChannel.copySessionData(temp[i],
											restored_channels[j]);
								}
							}
						}
					}
				}

				return true;
			}
		} catch (StorageException e) {
			e.printStackTrace();
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
		}
		return false;

	}


	private void startVoxService() {
		if (!LoginOrRegistrationActivity.isVOXServiceRunning(this)) {
			Intent i = new Intent(this, VoiceActivationService.class);
			startService(i);
			Log.d("mbp", "VoiceActivationService started..");
		}
	}

	private void stopVoxService() {
		if (LoginOrRegistrationActivity.isVOXServiceRunning(this)) {
			Intent i = new Intent(this, VoiceActivationService.class);
			stopService(i);
			Log.d("mbp", "VoiceActivationService stopped..");
		}
	}


	private void unregisterWithGCM(String api_token) {
		
		Log.d("mbp","unregister app via Me-Api Json.");
		//unregisterWithBMS
		SharedPreferences set = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int appID = (int) set.getLong(PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID, 0);
		
		try {
			App.unregisterNotification(api_token, appID);
			App.unregisterApp(api_token, appID);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Intent unregIntent = new Intent(
				"com.google.android.c2dm.intent.UNREGISTER");

		//Put it into bundle, because we will remove it from PREFERENCES after this call..
		Intent pendingIntent = new Intent(); 
		pendingIntent.putExtra("api_token", api_token);
		
				
		unregIntent.putExtra("app",
				PendingIntent.getBroadcast(this, 0, pendingIntent, 0));
		

		
		
		startService(unregIntent);
	}

	private void onUserLogout()
	{
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		boolean offlineMode = settings.getBoolean(
				PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
		if (offlineMode == false) 
		{

			// vox service should not take wakelock
			settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(PublicDefine.PREFS_VOX_SHOULD_TAKE_WAKELOCK,
					false);
			// remove password when user logout
			editor.remove(PublicDefine.PREFS_TEMP_PORTAL_PWD);
			
			String api_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			unregisterWithGCM(api_token);
		
			

			Intent new_login = new Intent(this,LoginOrRegistrationActivity.class);
			new_login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(new_login);

			

			// Remove offline data.
			new SetupData().clear_session_data(getExternalFilesDir(null));

			// Remove all unread alerts
			Log.d("mbp", "Delete all unread alerts & clear notifications");
			AlertData.purgeAlertsNotFromCameras(null,
					getExternalFilesDir(null));

			// Remove all pending notification on Status bar
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			int i = 0;
			int current_id;
			while (i < GcmIntentService.PUSH_IDs.length) {
				current_id = GcmIntentService.PUSH_IDs[i];
				notificationManager.cancel(current_id);
				i++;
			}
			
			editor.remove(PublicDefine.PREFS_SAVED_PORTAL_TOKEN);
			editor.commit();
			finish();
		} 
		else
		{
			showDialog(DIALOG_NEED_TO_LOGIN);
		}
	}
	
	private void onInfo() {

		String app_version = null;
		PackageInfo pinfo;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			app_version = pinfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			app_version = "Unknown";
		}

		String message = String.format(
				getResources().getString(R.string.EntryActivity_about_info),
				app_version);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
				.setCancelable(true)
				.setPositiveButton(getResources().getString(R.string.OK),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface _dialog,
									int which) {
								_dialog.dismiss();
							}
						});

		builder.create().show();

	}

	
	private void checkVoxStatus(CamProfile camProfile) {
		if ( camProfile == null)
			return;
		String device_address_port = camProfile.get_inetAddress().getHostAddress() + ":" + camProfile.get_port() ;
		String res = null; 
		String http_addr =String.format("%1$s%2$s%3$s","http://",
				device_address_port,
				PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_STATUS) ;
		res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,  camProfile.getBasicAuth_usr(), camProfile.getBasicAuth_pass());

		if ( (res!= null) && res.startsWith(PublicDefine.VOX_STATUS))
		{
			String str_value = res.substring(PublicDefine.VOX_STATUS.length()
					+ 2 ); 

			int vox_status = Integer.parseInt(str_value);


			if (vox_status == 0)//Disabled
			{
				camProfile.setVoxEnabled(false); 
			}
			else
			{
				//Enabled
				camProfile.setVoxEnabled(true); 
			}

		}
		
	}
}


class CamChannelListAdapter extends BaseAdapter {
	private Context mContext;
	private CamChannel[] items;

	public CamChannelListAdapter(Context c, CamChannel[] channels) {
		mContext = c;
		this.items = channels;
	}

	public int getCount() {

		if (items != null)
			// return items.length;
			return getValidChannelCount();
		else
			return 0;
	}

	public Object getItem(int position) {
		return items[position];
	}

	public long getItemId(int position) {
		if (items[position] != null) {
			return items[position].getChannel();
		}
		return 0;
	}

	public int getValidChannelCount() {
		int count = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i].getCamProfile() != null) {
				count++;
			}
		}

		return count;
	}

	private boolean isTooLarge(TextView text, String newText) {
		float textWidth = text.getPaint().measureText(newText);
		return (textWidth >= text.getMeasuredWidth());
	}

	public View getView(final int position, View convertView,
			ViewGroup parent) {

		RelativeLayout itemView = null;
		ImageView imageView = null;
		CamProfile camera = null;

		/* Stat setting ups the view */
		if (convertView == null) { // if it's not recycled, initialize some
			// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if ( mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) 
			{ // support
				// landscape
				itemView = (RelativeLayout) inflater.inflate(
						R.layout.bb_is_camera_channel_list_item_landscape,
						null);
			} 
			else
			{ // support portrait
				itemView = (RelativeLayout) inflater.inflate(
						R.layout.bb_is_camera_channel_list_item_portrait,
						null);
			}

		} else {

			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) { // support
				// landscape
				itemView = (RelativeLayout) inflater.inflate(
						R.layout.bb_is_camera_channel_list_item_landscape,
						null);
			} else { // support portrait
				itemView = (RelativeLayout) inflater.inflate(
						R.layout.bb_is_camera_channel_list_item_portrait,
						null);
			}

		}

		camera = items[position].getCamProfile();
		if (camera == null) {
			itemView.setClickable(false);
			itemView.setVisibility(View.INVISIBLE);
			return itemView;
		}

		itemView.setVisibility(View.VISIBLE);

		// Setup the Cam Name:
		TextView camName = (TextView) itemView
				.findViewById(R.id.textCamName);
		// camName.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
		if (isTooLarge(camName, camera.getName())) {
			// Log.d("mbp", "reduce font size ");
			camName.setTextSize(20); // 20dp
		}
		camName.setText(camera.getName());

		if (camera.hasUpdatedLocation()) 
		{
			// Setup the setting button
			((DashBoardActivity) mContext).updateAlertForChannel(items[position],itemView );
			

			// Setup the Cam Status : local/remote/offline
			imageView = (ImageView) itemView
					.findViewById(R.id.camStatusView);
			if (camera.isInLocal()) {
				imageView.setImageResource(R.drawable.camera_online);
			} else if (camera.isReachableInRemote()) {
				imageView.setImageResource(R.drawable.camera_online);
			} else {
				imageView.setImageResource(R.drawable.camera_offline);
			}

			// Setup the Cam info: in local network/last seen
			TextView camInfo = (TextView) itemView
					.findViewById(R.id.textCamInfo);
			TextView textStatusView = (TextView) itemView
					.findViewById(R.id.textStatusView);
			if (camera.isInLocal()) 
			{
				camInfo.setText(mContext.getResources().getString(R.string.from_local_wi_fi));
				textStatusView.setText(mContext.getResources().getString(R.string.available));
			}
			else if (camera.isReachableInRemote())
			{
				camInfo.setText(mContext.getResources().getString(R.string.remote_camera));
				textStatusView.setText(R.string.camera_is_not_in_local_network);
			}
			else
			{
				camInfo.setText(mContext.getResources().getString(R.string.remote_camera));
				textStatusView.setText(mContext.getResources().getString(R.string.Not_Available));
			}

			imageView.setVisibility(View.VISIBLE);
			camInfo.setVisibility(View.VISIBLE);
			textStatusView.setVisibility(View.VISIBLE);

			ProgressBar spinner = (ProgressBar) itemView.findViewById(R.id.camStatusSearching);
			spinner.setVisibility(View.INVISIBLE);
			
			// Setup the setting button
			((DashBoardActivity) mContext).setupSettingButton(items[position],itemView );
			
			// Setup the view button
			((DashBoardActivity) mContext).setupViewButton(items[position], itemView);
			
			

		} 
		else // has not update the location - keep spinning
		{

			// Setup the Cam Status : local/remote/offline
			imageView = (ImageView) itemView
					.findViewById(R.id.camStatusView);
			// Setup the Cam info: in local network/last seen
			TextView camInfo = (TextView) itemView
					.findViewById(R.id.textCamInfo);
			TextView textStatusView = (TextView) itemView
					.findViewById(R.id.textStatusView);

			imageView.setVisibility(View.INVISIBLE);
			camInfo.setVisibility(View.INVISIBLE);
			textStatusView.setVisibility(View.INVISIBLE);

			ProgressBar spinner = (ProgressBar) itemView
					.findViewById(R.id.camStatusSearching);
			spinner.setVisibility(View.VISIBLE);

			Button buttonView = (Button) itemView
					.findViewById(R.id.buttonView);
			// set background color into grey
			buttonView.setEnabled(false);
			buttonView.getBackground().setColorFilter(0xff444444,
					android.graphics.PorterDuff.Mode.MULTIPLY);

		}

		return itemView;

	}

}
