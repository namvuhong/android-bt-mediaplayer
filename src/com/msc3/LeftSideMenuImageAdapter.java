package com.msc3;

import com.blinkhd.R;


import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

public class LeftSideMenuImageAdapter extends BaseAdapter {

	private Context mContext;
	private boolean[] status;


	static public final int pos_pan = 0;
	static public final int pos_mic = 1;
	static public final int pos_rec = 2;
	static public final int pos_melody = 3;
	static public final int pos_temp= 4;



	private Integer[] mThumbIds_full = {
			R.drawable.video_action_pan,
			R.drawable.video_action_mic,
			R.drawable.video_action_video,
			R.drawable.video_action_music,
			R.drawable.video_action_temp

	};


	private Integer[] mThumbIds_full_disabled = {
			R.drawable.video_action_pan_pressed,
			R.drawable.video_action_mic_pressed,
			R.drawable.video_action_video_pressed,
			R.drawable.video_action_music_pressed,
			R.drawable.video_action_temp_pressed
	};

	private Integer[] mThumbIds_without_mic = {
			R.drawable.video_action_pan,
			R.drawable.video_action_video,
			R.drawable.video_action_music,
			R.drawable.video_action_temp
	};
	
	private Integer[] mThumbIds_without_mic_disabled = {
			R.drawable.video_action_pan_pressed,
			R.drawable.video_action_video_pressed,
			R.drawable.video_action_music_pressed,
			R.drawable.video_action_temp_pressed
	};
	
	private Integer[] mThumbIds_without_pan = {
			R.drawable.video_action_mic,
			R.drawable.video_action_video,
			R.drawable.video_action_music,
			R.drawable.video_action_temp
	};
	
	private Integer[] mThumbIds_without_pan_disabled = {
			R.drawable.video_action_mic_pressed,
			R.drawable.video_action_video_pressed,
			R.drawable.video_action_music_pressed,
			R.drawable.video_action_temp_pressed
	};
	
	private Integer[] mThumbIds_without_pan_mic = {
			R.drawable.video_action_video,
			R.drawable.video_action_music,
			R.drawable.video_action_temp
	};
	
	private Integer[] mThumbIds_without_pan_mic_disabled = {
			R.drawable.video_action_video_pressed,
			R.drawable.video_action_music_pressed,
			R.drawable.video_action_temp_pressed
	};
	
	private Integer[] mThumbIds_without_pan_mic_melody = {
			R.drawable.video_action_video,
			R.drawable.video_action_temp
	};
	
	private Integer[] mThumbIds_without_pan_mic_melody_disabled = {
			R.drawable.video_action_video_pressed,
			R.drawable.video_action_temp_pressed
	};

	final float scale ;

	boolean enableMic = false;
	boolean enablePanTilt = false;
	boolean enableMelody = false;
	int numberOfItems;
	private Integer[] mThumbIds;
	private Integer[] mThumbIds_disabled;
	public LeftSideMenuImageAdapter(Context c, boolean enableMicFeature,
			boolean enablePanTiltFeature, boolean enableMelodyFeature) {
		mContext = c;
		scale = mContext.getResources().getDisplayMetrics().density;
		enableMic = enableMicFeature;
		enablePanTilt = enablePanTiltFeature;
		enableMelody = enableMelodyFeature;
		numberOfItems = getNumberOfItems();
		status = new boolean[numberOfItems];
		for (int i = 0; i<numberOfItems; i++)
		{
			status[i] = false;
		}
	}

	private int getNumberOfItems()
	{
		int numItems = 2;
		if (enableMic == true &&
				enablePanTilt == true &&
						enableMelody == true) 
		{
			//mbp83
			numItems = 5;
			mThumbIds = mThumbIds_full;
			mThumbIds_disabled = mThumbIds_full_disabled;
		}
		else if (enableMic == true &&
				enablePanTilt == false &&
				enableMelody == true)
		{
			//focus66
			numItems = 4;
			mThumbIds = mThumbIds_without_pan;
			mThumbIds_disabled = mThumbIds_without_pan_disabled;
		}
		else if (enableMic == false &&
				enablePanTilt == true &&
				enableMelody == true)
		{
			//shared cam 36/41 on Windows
			numItems = 4;
			mThumbIds = mThumbIds_without_mic;
			mThumbIds_disabled = mThumbIds_without_mic_disabled;
		}
		else if (enableMic == false &&
				enablePanTilt == false &&
				enableMelody == true)
		{
			//shared cam 33 on Windows
			numItems = 3;
			mThumbIds = mThumbIds_without_pan_mic;
			mThumbIds_disabled = mThumbIds_without_pan_mic_disabled;
		}
		else if (enableMic == false &&
				enablePanTilt == false &&
				enableMelody == false)
		{
			//shared cam on Mac
			numItems = 2;
			mThumbIds = mThumbIds_without_pan_mic_melody;
			mThumbIds_disabled = mThumbIds_without_pan_mic_melody_disabled;
		}
		
		return numItems;
	}
	
	public boolean isEnablePan() {
		return status[pos_pan];
	}

	public void setEnablePan(boolean enablePan) {
		status[pos_pan] = enablePan;
	}

	public void toggleItem(int position)
	{
		if (status[position] == true)
		{
			status[position] = false;
		}
		else
		{
			status[position] = true;
		}
	}

	public boolean isEnableMic() {
		return status[pos_mic];
	}



	public void setEnableMic(boolean enableMic) {
		status[pos_mic] = enableMic;
	}

	public boolean isEnableRec() {
		return status[pos_rec];
	}



	public void setEnableRec(boolean enableRec) {
		status[pos_rec] = enableRec;
	}


	public boolean isEnableMelody() {
		return status[pos_melody];
	}



	public void setEnableMelody(boolean enableMelody) {
		status[pos_melody] = enableMelody;
	}

	public boolean isEnableTemp() {
		return status[pos_temp];
	}



	public void setEnableTemp(boolean enableTemp) {
		status[pos_temp] = enableTemp;
	}


	public int getCount() 
	{
		return numberOfItems;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public void clearAllItemSettings()
	{
		for (int i=0; i<numberOfItems; i++)
		{
			status[i] = false;
		}
	}

	public void clearOtherSettings(int position)
	{
		for (int i = 0; i < numberOfItems; i++)
		{
			if (i != position)
			{
				status[i] = false;
			}
		}
	}

	public boolean areAllItemsEnabled()
	{
		return false;
	}

	public boolean isShownEnabled(int position)
	{
		if (position >= 0 &&
				position < getCount())
		{
			return status[position];
		}

		return true;
	}

	@Override
	public boolean isEnabled(int position)
	{
		switch (position)
		{
		default: //else
			return true;
		}
	}


	///SUPPORT lanscape mode first

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		int width, height,padding;


		/* 4 icons visible 
		 */
		//int height_dp = 84;//dp
		int padding_dp = 1;//dp 
		padding = (int) (padding_dp* scale + 0.5f);
		//Convert to system pixel -- this value should be different from screen to screen 
		//height =(int) (height_dp * scale + 0.5f);
		//width = height;



		if (convertView == null) {  // if it's not recycled, initialize some attributes
			imageView = new ImageView(mContext);
			//imageView.setLayoutParams(new GridView.LayoutParams(width,height));
			imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imageView.setPadding(padding,padding, padding, padding);


		} else {
			imageView = (ImageView) convertView;
			//imageView.setLayoutParams(new GridView.LayoutParams(width,height));
			imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			imageView.setPadding(padding,padding, padding, padding);
		}

		
		if (!isShownEnabled(position))
		{
			imageView.setImageResource(mThumbIds[position]);
			imageView.setOnTouchListener(
					new ButtonTouchListener(mContext.getResources().getDrawable(mThumbIds[position]),
							mContext.getResources().getDrawable(mThumbIds_disabled[position])));
		}
		else
		{
			imageView.setImageResource(mThumbIds_disabled[position]);
			imageView.setOnTouchListener(
					new ButtonTouchListener(mContext.getResources().getDrawable(mThumbIds_disabled[position]),
							mContext.getResources().getDrawable(mThumbIds[position])));
		}

		return imageView;
	}


}

