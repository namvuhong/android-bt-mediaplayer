package com.msc3;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLContext;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blinkhd.R;
import com.blinkhd.gcm.GetDisabledAlertsTask;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.msc3.registration.SingleCamConfigureActivity;
import com.msc3.registration.UserAccount;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;

public class CameraMenuActivity extends PreferenceActivity implements  IChangeNameCallBack, IUpdateAlertCallBack {


	public static final int CAMERA_REMOVED = 0x201;
	public static final int CAMERA_INFO_UPDATED = 0x202;
	public static final int CLOSE_APP_AND_EXIT = 0x203;

	private static final int DIALOG_UPDATING_CAMERA_STATUS = 1;
	private static final int DIALOG_REMOVE_DONE = 2;
	private static final int DIALOG_IS_NOT_REACHABLE = 3;
	private static final int DIALOG_REMOVE_FAILED = 4;
	private static final int DIALOG_CAMERA_NOT_REMOVABLE = 5; 
	//private static final int DIALOG_CONFIRM_EXIT =6;
	private static final int DIALOG_CAMERA_NOT_REMOVABLE_1 = 7;
	private static final int DIALOG_CHANGE_PWD_FAILED = 8;
	private static final int DIALOG_VERIFY_CHANGE_PWD_FAILED = 9;
	private static final int DIALOG_CHANGE_PWD_OK = 10;
	private static final int DIALOG_UPDATING_CAMERA_STATUS_FAILED = 11;
	private static final int DIALOG_STORAGE_UNAVAILABLE =12;
	private static final int DIALOG_MANUAL_FWD_FAILED = 13; 
	
	private static final int DIALOG_UPNP_IS_NOT_ENABLED = 14; 
	private static final int DIALOG_UPNP_COULD_NOT_FIND_PORT = 15;
	private static final int DIALOG_UPNP_OK = 16;
	private static final int DIALOG_UPNP_IS_RUNNING= 17;
	private static final int DIALOG_MANUAL_MODE_ENABLE = 18 ;

	private static final int DIALOG_MANUAL_FWD_FAILED_PORT_NOT_OPEN = 19;
	private static final int DIALOG_NEED_TO_BE_IN_LOCAL = 20;
	private static final int DIALOG_IS_NOT_REACHABLE_RETRY_EXIT =21;
	private static final int DIALOG_IS_NOT_SUPPORTED = 22;
	private static final int DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN = 23;
	private static final int DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY = 24;
	private static final int DIALOG_CAMERA_NAME_IS_INVALID = 25;
	

	public static final String bool_isLoggedIn = "CameraMenuActivity_bool_isLoggedIn";
	public static final String str_userToken = "CameraMenuActivity_str_UserToken";
	public static final String str_deviceUrl="CameraMenuActivity_str_deviceUrl";
	public static final String str_deviceMac="CameraMenuActivity_str_deviceMac";
	public static final String str_deviceName="CameraMenuActivity_str_deviceName";
	public static final String int_CommMode="CameraMenuActivity_int_CommMode";
	public static final String str_deviceMac_out="CameraMenuActivity_str_deviceMac_out";
	public static final String str_deviceName_out="CameraMenuActivity_str_deviceName_out";
	public static final String bool_cameraInLocal="CameraMenuActivity_bool_cameraInLocal";
	
	public static final String bool_shouldGotoShortSubmenu = "CameraMenuActivity_bool_shouldGotoShortSubmenu";
	public static final String int_UdtLocalPort = "CameraMenuActivity_int_UdtLocalPort";
	public static final String string_UdtChannelId = "CameraMenuActivity_string_UdtChannelId";

	
	
	public static final String ACTION_CAMERA_REMOVED = "com.msc3.CameraMenuActivity.CAMERA_REMOVED";
	public static final String ACTION_CAMERA_REMOVED_MAC = "camera-mac";
	public static final String ACTION_STOP_CAMERA_VIEW = "com.msc3.CameraMenuActivity.STOP_VIEW";
	
	
	public static final int COMM_MODE_HTTP = 1;
	public static final int COMM_MODE_UDT = 2;
	
	/*
	 * PublicDefine.VOX_SENSITIVITY_LOW
	 * PublicDefine.VOX_SENSITIVITY_MED
	 * PublicDefine.VOX_SENSITIVITY_HI
	 */
	private int vox_sensitivity; 
	private boolean vox_enabled;

	/*
	 * PublicDefine.TEMPERATURE_UNIT_DEG_F or
	 * PublicDefine.TEMPERATURE_UNIT_DEG_C
	 */
	private int temperature_unit; 

	private int camera_volume;
	private int camera_brightness;
	private int camera_name;

	private SSLContext ssl_context; 
	/* page title */
	private TextView textCamName;

	private String device_address_port;
	private String device_mac; 
	private String device_id;
	private String device_name;
	private String portal_usrToken;
	private int    comm_mode; 
	private int    udtLocalPort ; ///valid if comm_mode = UDT
	
	private String http_userName; 
	private String http_userPass;

	private String[] menu_items;
	private String[] short_menu_items;

	private String udtChannelId ; 
	

	private CharSequence[] level_items;

	private CharSequence[] temp_items;

	private CharSequence[] vox_items;

	private  static final Integer[] vox_item_values = {-10,-20,-25,-30};

	private  String [] videoQuality ;


	private boolean isLoaded=false; // check preference screen is loaded?
	private boolean isshortLoaded=false;
	
	 
	
	private CamProfile cameraProfile = null; 
	private int access_mode;
	private CamChannel[] restored_channels;
	private CamProfile[] restored_profiles;
	
	private boolean  isLocalCamera; 
	
	/** Called when the activity is first created. */
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);

		menu_items = getResources().getStringArray(R.array.CameraMenuActivity_menu_items_2);
		short_menu_items = getResources().getStringArray(R.array.CameraMenuActivity_short_menu_items_2);
		level_items = getResources().getStringArray(R.array.CameraMenuActivity_level_items);
		temp_items= getResources().getStringArray(R.array.CameraMenuActivity_temp_items);
		vox_items = getResources().getStringArray(R.array.CameraMenuActivity_vox_items);
		videoQuality = getResources().getStringArray(R.array.CameraMenuActivity_vq_items);

		/* read the keystore and create the ssl context for HTTPS connections*/
		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(this);


		portal_usrToken = null;

		Bundle extra = getIntent().getExtras();

		if (extra != null)
		{
			boolean isLoggedIn = extra.getBoolean(bool_isLoggedIn);
			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);; 
			boolean offlineMode = settings.getBoolean(PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
			if (isLoggedIn)
			{
				portal_usrToken = extra.getString(str_userToken);
			}
			else if (offlineMode == false)
			{
				//use a different menu - for direct mode 
				menu_items = getResources().getStringArray(R.array.CameraMenuActivity_menu_dm_items);
			}

			
			isLocalCamera = extra.getBoolean(bool_cameraInLocal, false); 
			
			
			/* Could be null in case of remote connection*/
			device_address_port=  extra.getString(str_deviceUrl);

			device_mac = extra.getString(str_deviceMac);
			device_name = extra.getString(str_deviceName);

			//default to HTTP 
			comm_mode = extra.getInt(int_CommMode, COMM_MODE_HTTP);
			udtLocalPort = extra.getInt(int_UdtLocalPort, -1);

			
			udtChannelId = extra.getString(string_UdtChannelId); 
			
			if (comm_mode == COMM_MODE_UDT && udtLocalPort == -1 && udtChannelId == null)
			{
				Log.d("mbp", "ERROR : mode is UDT but port is invalid");
				
			}
				
			
			
			http_userName = PublicDefine.DEFAULT_BASIC_AUTH_USR;
			try {
				http_userPass = CameraPassword.getPasswordforCam(getExternalFilesDir(null),device_mac);
			} catch (StorageException e) {
				Log.d("mbp", e.getLocalizedMessage());
				showDialog(DIALOG_STORAGE_UNAVAILABLE);
			}
			if (http_userPass == null) http_userPass ="";//default;
			
			
			
			
			/* read the profile from offline data */ 
			try {
				restore_session_data();
			} catch (StorageException e) {
				Log.d("mbp", e.getLocalizedMessage());
				showDialog(DIALOG_STORAGE_UNAVAILABLE);
				return;
			}
			for (int i =0; i<restored_profiles.length; i++)
			{
				if (restored_profiles[i] != null && 
						restored_profiles[i].get_MAC().equalsIgnoreCase(device_mac))
				{
					cameraProfile = restored_profiles[i];
					device_id = String.valueOf(cameraProfile.getCamId());
				}
					
			}
			
		}


	}

	protected void onResume()
	{

		super.onResume();

	}

	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
	}

	protected void onStart()
	{
		super.onStart();

		Thread aaa = new Thread() {
			public void run()
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						onCameraSubSetting();

					}
				});

			}
		};
		aaa.start();
		
		return;


	}
	
	protected void onPause()
	{
		super.onPause();
		
	}
	
	protected void onStop()
	{
		super.onStop();
	
		if (isLocalCamera == false )
		{
			Intent intent=new Intent();
			intent.setAction(ACTION_STOP_CAMERA_VIEW);
			intent.putExtra(ACTION_CAMERA_REMOVED_MAC,device_mac);
			sendBroadcast(intent);
			
			finish(); 
		}
		
		
	}

	protected void onDestroy()
	{
		super.onDestroy();
		setResult(Activity.RESULT_OK);

		
		
	}
	
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		Spanned msg ;
		switch(id) {
		case DIALOG_UPDATING_CAMERA_STATUS:
			dialog = new ProgressDialog(this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_dialog_cam_status)+"</big>");
			((ProgressDialog) dialog).setMessage(msg);
			((ProgressDialog)dialog).setIndeterminate(true);
			dialog.setCancelable(false);
			return dialog;
		case DIALOG_REMOVE_DONE:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_rem_cam_done)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					/* Store the mac address of the removed camera to pass back to caller */
					Intent data = new Intent() ; 
					data.putExtra(str_deviceMac_out, device_mac);
					CameraMenuActivity.this.setResult(CAMERA_REMOVED,data);

					CameraMenuActivity.this.finish();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_IS_NOT_REACHABLE:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_cam_unreachable)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_REMOVE_FAILED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_rem_cam_failed)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_CAMERA_NOT_REMOVABLE:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_rem_cam_failed_1)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		
		case DIALOG_CAMERA_NOT_REMOVABLE_1:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_rem_cam_failed_2)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_CHANGE_PWD_FAILED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_cam_pwd_failed)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}

		case DIALOG_VERIFY_CHANGE_PWD_FAILED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_cam_pwd_failed_1)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_CHANGE_PWD_OK :
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.CameraMenuActivity_cam_pwd_ok)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}
		case DIALOG_UPDATING_CAMERA_STATUS_FAILED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			msg= Html.fromHtml("<big>"+getResources().getString(R.string.failed_to_change_camera_setting)+"</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
			AlertDialog alert = builder.create();
			return alert;
		}	
		case DIALOG_STORAGE_UNAVAILABLE : 
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

					Intent homeScreen = new Intent(CameraMenuActivity.this, FirstTimeActivity.class);
					homeScreen.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeScreen);
					CameraMenuActivity.this.finish();
				}
			});

			return builder.create();
		}
		case DIALOG_MANUAL_FWD_FAILED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.manual_port_forwarding_failed_camera_connection_timeout)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}	
		case DIALOG_UPNP_IS_NOT_ENABLED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.upnp_is_not_enabled_or_not_supported_by_router)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}	
		case DIALOG_UPNP_COULD_NOT_FIND_PORT:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.camera_could_not_open_ports_on_router_refer_to_faq_for_further_information)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}
		case DIALOG_UPNP_IS_RUNNING:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.camera_is_still_in_the_process_of_running_upnp_please_check_again_later)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}
		case DIALOG_UPNP_OK:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.camera_has_successfully_opened_ports_on_router)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}
		case DIALOG_MANUAL_MODE_ENABLE:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.camera_is_in_manual_port_forwarding_mode_please_check_router_port_forwarding_settings_for_more_info)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}	
		case DIALOG_MANUAL_FWD_FAILED_PORT_NOT_OPEN:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.specified_ports_are_not_opened_on_router_please_check_and_try_again)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}
		case DIALOG_NEED_TO_BE_IN_LOCAL:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.can_t_proceed_you_need_to_be_in_the_same_network_with_the_cameras_to_carry_out_this_operation)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			}); 
			

			return builder.create();
		}
		
		case DIALOG_IS_NOT_REACHABLE_RETRY_EXIT:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.can_t_proceed_you_need_to_be_in_the_same_network_with_the_cameras_to_carry_out_this_operation)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
					//showDialog(DIALOG_UPDATING_CAMERA_STATUS);
					
					//backgroundConnect(); 
					
				}
			})
			.setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
					//TODO return to camera list;
					CameraMenuActivity.this.finish();
				}
			});

			return builder.create();
		}
		
		case DIALOG_IS_NOT_SUPPORTED:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.CameraMenuActivity_not_supported)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		}
		case DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN:
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getString(R.string.update_status_failed_please_try_again_) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
			
		}
		case DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"+getResources().getString(R.string.camera_name_length_is_out_of_boundary)+"</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface diag, int which) {
					diag.cancel();
				}
			})
			.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface diag) {
				}
			});
			
			alert = builder.create();
			return alert;
		}
		case DIALOG_CAMERA_NAME_IS_INVALID:
		{
			AlertDialog.Builder builder;
			AlertDialog alert;
			msg = Html.fromHtml("<big>"+getResources().getString(R.string.camera_name_is_invalid)+"</big>");
			builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface diag, int which) {
					diag.cancel();
				}
			})
			.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface diag) {
				}
			});
			
			alert = builder.create();
			return alert;
		}
		default:
			break;
		}
		return null;
	}


	
	/* restore functions */
	private boolean restore_session_data() throws StorageException
	{
		SetupData savedData = new SetupData();
		try {
			if (savedData.restore_session_data(getExternalFilesDir(null))) {
				restored_profiles = savedData.get_CamProfiles();
				restored_channels = savedData.get_Channels();
				access_mode = savedData.get_AccessMode();
				return true;
			}
		} catch (StorageException e) {
			throw e;
		}
		return false;

	}
	
	private void save_session_data() {

		SetupData savedData = new SetupData(access_mode, "NA",
				restored_channels, restored_profiles);
		savedData.save_session_data(getExternalFilesDir(null));

	}



	/**
	 *  Similar to Initial BM setup but skip the first page- Go directly to 
	 *  Login??? 
	 */
	protected void onRouterMode() {

		/* same idea: clear all activities first and start from login.. 
		 * 
		 * But LoginOrRegistrationActivit was killed... damn..
		 * */
		//Intent login = new Intent(this, LoginOrRegistrationActivity.class);


		/* use this with another flag -- what a mess!!!*/
		Intent initialSetup = new Intent(this, FirstTimeActivity.class);
		initialSetup.putExtra(FirstTimeActivity.bool_InfraMode,true);
		initialSetup.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(initialSetup);
		this.finish();
	}

	
	protected void onCameraSubSetting() {

		try {
			showDialog(DIALOG_UPDATING_CAMERA_STATUS);
		}
		catch (IllegalArgumentException  ie)
		{
		}
		catch (BadTokenException  ie)
		{
		}

		if (comm_mode == COMM_MODE_UDT)
		{
			//backgroundConnect(); 
			
			Log.d("mbp", "In menu ARRG Dont open   newUDT session here -- ");
			Runnable worker = new Runnable() {
				@Override
				public void run() {
					if (device_address_port != null)
					{
						queryCameraSettings();
					}


					CameraMenuActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {

							setupMenuItems();

							try
							{
								//Dismis dialog
								dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
							}
							catch (IllegalArgumentException  ie)
							{
							}
							catch (BadTokenException  ie)
							{
							}

						}
					});

				}
			};
			new Thread(worker,"Worker Thread").start();
		}
		else
		{
			
			Runnable worker = new Runnable() {
				@Override
				public void run() {
					
					queryCameraSettings();


					CameraMenuActivity.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {

							setupMenuItems();

							try
							{
								//Dismis dialog
								dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
							}
							catch (IllegalArgumentException  ie)
							{
							}
							catch (BadTokenException  ie)
							{
							}

						}
					});

				}
			};
			new Thread(worker,"Worker Thread").start();
		}
	}
	
	
	
	

	private void setupMenuItems()
	{
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);; 
		boolean offlineMode = settings.getBoolean(PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
		if (portal_usrToken == null && (offlineMode == false))
		{
			//Direct mode
			///SHOULD NOT BE HERE
			Log.e("mbp", "CameraMenuActivity setupMenuItems() - No longer support direct mode");
		}
		else 
		{
			//Router mode
			setupMenuItems_rm();
		}
	}

	
	

	private void setupMenuItems_rm()
	{
		Bundle extra = getIntent().getExtras();
		Preference namecamera;
		Preference volumecamera;
		Preference brightness;
		Preference soundsensitivity;
		Preference videoquality;
		Preference temperatureUnit;
		Preference removecamera;
		Preference inform;
		boolean goToShortSubmenu = false;
		if (extra != null)
		{
			goToShortSubmenu = extra.getBoolean(bool_shouldGotoShortSubmenu, false);
			
		}

		if (goToShortSubmenu == false)
		{	
			//Load cover page first 		
			if (isLoaded == false) // load once
			{
				addPreferencesFromResource(R.xml.preference_screen);
				getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.bb_camera_setting_title);
				textCamName = (TextView) findViewById(R.id.textCamName);
				textCamName.setText(device_name);
				isLoaded = true;
			}		
			
			if (device_address_port != null)
			{
				namecamera = findPreference("camera_name");	
				namecamera.setSummary(device_name);
				namecamera.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onName(null);
						return false;
					}
				});
				
				volumecamera = findPreference("volume");	
				volumecamera.setSummary(level_items[camera_volume].toString());
				volumecamera.setOnPreferenceClickListener(new OnPreferenceClickListener() {		
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onVolLevel(null);
						return false;
					}
				});
				
				brightness = findPreference("brightness");	
				brightness.setSummary(level_items[camera_brightness].toString());
				brightness.setOnPreferenceClickListener(new OnPreferenceClickListener() {				
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onBrightness(null);
						return false;
					}
				});
				
				soundsensitivity =  findPreference("sound_sensitivity");
				int idx = -1; 
				switch (vox_sensitivity)
				{
				case PublicDefine.VOX_SENSITIVITY_LOW:
					idx = 0;
					break;
				case PublicDefine.VOX_SENSITIVITY_HI:
					idx = 2;
					break;
				case PublicDefine.VOX_SENSITIVITY_MED:
					idx = 1;
					break;
				case PublicDefine.VOX_SENSITIVITY_V_HI:
					idx = 3;
					break;
				default:
					break;
				}

				if (idx == -1)
				{
					soundsensitivity.setSummary("");
				}
				else
				{
					soundsensitivity.setSummary(vox_items[idx] .toString());
				}

				soundsensitivity.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onVOX(null);
						return false;
					}
				});
				
				
				temperatureUnit = findPreference("temperature_unit");
				String temp_unit_str = "\u00B0" + ((temperature_unit == PublicDefine.TEMPERATURE_UNIT_DEG_F)?"F":"C");
				temperatureUnit.setSummary(temp_unit_str);	
				temperatureUnit.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onTempConversion(null);
						return false;
					}
				});
				
				videoquality = findPreference("video_quality");
				SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);
				videoquality.setSummary(videoQuality[img_res]);
				videoquality.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onVideoQuality(null);
						return false;
					}
				});
				
				removecamera = findPreference("remove_this_camera");
				removecamera.setSummary(null);				
				removecamera.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					
					@Override
					public boolean onPreferenceClick(Preference preference) {
						//Only do this if user is logged in 
						if (portal_usrToken != null)
						{
							if (isLocalCamera == true)
							{
								AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
								builder.setMessage(getResources().getString(R.string.CameraMenuActivity_rem_cam_cfm))
								.setCancelable(false)
								.setPositiveButton(getResources().getString(R.string.Confirm), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										onRemoveCamera();
									}
								})
								.setNegativeButton(getResources().getString(R.string.Cancel),  new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.cancel();
									}
								});
								AlertDialog alert = builder.create();
								alert.show();
							}
							else
							{
								AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
								builder.setMessage(getResources().getString(R.string.CameraMenuActivity_rem_cam_cfm_1))
								.setCancelable(false)
								.setPositiveButton(getResources().getString(R.string.Confirm), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.dismiss();
										onRemoveCamera_online();
									}
								})
								.setNegativeButton(getResources().getString(R.string.Cancel),  new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.cancel();
									}
								});
								AlertDialog alert = builder.create();
								alert.show();

							}
						}
						else
						{
							showDialog(DIALOG_CAMERA_NOT_REMOVABLE);
						}
						return false;
					}
				});
				
				inform = findPreference("information");
				inform.setSummary(null);	
				inform.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					
					@Override
					public boolean onPreferenceClick(Preference preference) {
						onInfo(null);
						return false;
					}
				});
			}	
		}
		else
		{
			//Load cover page first 		
			if(isshortLoaded==false)
			{
				String phonemodel = android.os.Build.MODEL;
				if (phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k)) 
				{
					addPreferencesFromResource(R.xml.preference_screen_short_menu_mbp2k);
				}
				else //Normal phones
				{
					addPreferencesFromResource(R.xml.preference_screen_short_menu);
				}
				
				getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.bb_camera_setting_title);
				textCamName = (TextView) findViewById(R.id.textCamName);
				textCamName.setText(device_name);
				isshortLoaded = true;
			}
				
			//got to Short Settings when press arrow button
			namecamera = findPreference("cameraName");	
			namecamera.setSummary(device_name);
			namecamera.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					onName(null);
					return false;
				}
			});
			removecamera = findPreference("remove_this_camera");
			removecamera.setSummary(null);
			removecamera.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					//Only do this if user is logged in 
					if (portal_usrToken != null)
					{
						if (isLocalCamera)
						{
							AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
							builder.setMessage(getResources().getString(R.string.CameraMenuActivity_rem_cam_cfm))
							.setCancelable(false)
							.setPositiveButton(getResources().getString(R.string.Confirm), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
									onRemoveCamera();
								}
							})
							.setNegativeButton(getResources().getString(R.string.Cancel),  new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
							AlertDialog alert = builder.create();
							alert.show();
						}
						else
						{
							AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
							builder.setMessage(getResources().getString(R.string.CameraMenuActivity_rem_cam_cfm_1))
							.setCancelable(false)
							.setPositiveButton(getResources().getString(R.string.Confirm), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
									onRemoveCamera_online();
								}
							})
							.setNegativeButton(getResources().getString(R.string.Cancel),  new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
							AlertDialog alert = builder.create();
							alert.show();

						}
					}
					else
					{
						showDialog(DIALOG_CAMERA_NOT_REMOVABLE);
					}
					return false;
				}
			});
			
			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			final String saved_token = settings.getString(
					PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			final String regId = settings.getString(PublicDefine.PREFS_PUSH_NOTIFICATION_ID, null); 
			
			CheckBoxPreference alert = (CheckBoxPreference)findPreference("sound_alert");
			if (alert != null)
			{
				if (cameraProfile != null)
				{
					alert.setChecked(cameraProfile.isSoundAlertEnabled());
				}
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						Log.d("mbp", "sound_alert: objc is:" + newValue.toString());

						if (cameraProfile != null)
						{
							Boolean bObj = (Boolean) newValue;
							if (cameraProfile.isSoundAlertEnabled() == bObj.booleanValue())
							{
								//Do nothing
							}
							else
							{
								//update and save
								cameraProfile.setSoundAlertEnabled(bObj.booleanValue());
								String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD; 
								if (bObj.booleanValue() == false)
								{
									enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD; 
								}

								String alertType = String.valueOf(VoxMessage.ALERT_TYPE_SOUND);
								UpdateAlertTask alertUpdate = new UpdateAlertTask(CameraMenuActivity.this, CameraMenuActivity.this);
								alertUpdate.execute(saved_token, device_id, alertType, enableOrDisabled);

							}
						}
						return true;
					}
				});
			}
			
			alert = (CheckBoxPreference)findPreference("tempHi_alert");	
			if (alert != null)
			{
				if (cameraProfile != null)
				{
					alert.setChecked(cameraProfile.isTempHiAlertEnabled());
				}
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						Log.d("mbp", "tempHi_alert: objc is:" + newValue.toString());
						if (cameraProfile != null)
						{
							Boolean bObj = (Boolean) newValue;
							if (cameraProfile.isTempHiAlertEnabled() == bObj.booleanValue())
							{
								//Do nothing
							}
							else
							{

								//update and save
								cameraProfile.setTempHiAlertEnabled(bObj.booleanValue());
								String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD; 
								if (bObj.booleanValue() == false)
								{
									enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD; 
								}

								

								String alertType = String.valueOf(VoxMessage.ALERT_TYPE_TEMP_HI); 

								UpdateAlertTask alertUpdate = new UpdateAlertTask(
										CameraMenuActivity.this, CameraMenuActivity.this);
								alertUpdate.execute(
										saved_token, device_id, alertType, enableOrDisabled);

							}
						}
						return true;
					}
				});
			}
			alert = (CheckBoxPreference)findPreference("tempLo_alert");
			if (alert != null)
			{
				if (cameraProfile != null)
				{
					alert.setChecked(cameraProfile.isTempLoAlertEnabled());
				}
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						Log.d("mbp", "tempLo_alert: objc is:" + newValue.toString());
						if (cameraProfile != null)
						{
							Boolean bObj = (Boolean) newValue;
							if (cameraProfile.isTempLoAlertEnabled() == bObj.booleanValue())
							{
								//Do nothing
							}
							else
							{
								//update and save
								cameraProfile.setTempLoAlertEnabled(bObj.booleanValue());
								String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD; 
								if (bObj.booleanValue() == false)
								{
									enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD; 
								}

								

								String alertType = String.valueOf(VoxMessage.ALERT_TYPE_TEMP_LO); 

								UpdateAlertTask alertUpdate = new UpdateAlertTask(
										CameraMenuActivity.this, CameraMenuActivity.this);
								alertUpdate.execute(
										saved_token, device_id, alertType, enableOrDisabled);

							}
						}
						return true;
					}
				});
			}
			
			alert = (CheckBoxPreference)findPreference("motion_alert");
			if (alert != null)
			{
				if (cameraProfile != null)
				{
					alert.setChecked(cameraProfile.isMotionAlertEnabled());
				}
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						Log.d("mbp", "motion_alert: objc is:" + newValue.toString());
						if (cameraProfile != null)
						{
							Boolean bObj = (Boolean) newValue;
							if (cameraProfile.isMotionAlertEnabled() == bObj.booleanValue())
							{
								//Do nothing
							}
							else
							{
								//update and save
								cameraProfile.setMotionAlertEnabled(bObj.booleanValue());
								String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD; 
								if (bObj.booleanValue() == false)
								{
									enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD; 
								}

								String alertType = String.valueOf(VoxMessage.ALERT_TYPE_MOTION_ON); 

								UpdateAlertTask alertUpdate = new UpdateAlertTask(
										CameraMenuActivity.this, CameraMenuActivity.this);
								alertUpdate.execute(
										saved_token, device_id, alertType, enableOrDisabled);

							}
						}
						return true;
					}
				});
			}
			
			
			alert = (CheckBoxPreference)findPreference("recurring_alert");
			if (alert != null)
			{
				alert.setChecked(settings.getBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_IS_RECURRING, false));
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						SharedPreferences settings = getSharedPreferences(
								PublicDefine.PREFS_NAME, 0);
						SharedPreferences.Editor editor = settings.edit();
						editor.putBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_IS_RECURRING, ((Boolean) newValue).booleanValue());
						editor.commit();
						
						return true;
					}
				});
			}
			
			
			alert = (CheckBoxPreference)findPreference("alert_in_call");
			if (alert != null)
			{
				String phonemodel = android.os.Build.MODEL;
				if (phonemodel.equals(PublicDefine.PHONE_MBP2k) ) 
				{
					alert.setChecked(settings.getBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL, false));
					alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

						@Override
						public boolean onPreferenceChange(Preference preference, Object newValue) {
							SharedPreferences settings = getSharedPreferences(
									PublicDefine.PREFS_NAME, 0);
							SharedPreferences.Editor editor = settings.edit();
							editor.putBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL, ((Boolean) newValue).booleanValue());
							editor.commit();
							
							return true;
						}
					});
				}
				else if ( phonemodel.equals(PublicDefine.PHONE_MBP1k))
				{
					alert.setChecked(false); 
					alert.setEnabled(false);
				}
				
			}
			
			alert = (CheckBoxPreference)findPreference("camera_connectivity_alert");
			if (alert != null)
			{
				alert.setChecked(
						settings.getBoolean(
								PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_ALERT,
								false)
								);
				alert.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
				{

					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						
						boolean isEnable =((Boolean) newValue).booleanValue();  
						
						SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
						
						SharedPreferences.Editor editor = settings.edit();
						
						if (isEnable == true)
						{
							WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
							if ((w != null) && (w.isWifiEnabled()) && (w.getConnectionInfo() != null))
							{
								String _wifi_name = w.getConnectionInfo().getSSID();

								Log.d("mbp", "CameraMenu: Store wifi for camera alert : "+ _wifi_name); 
								
								editor.putString(
										PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_HOME_WIFI, 
										_wifi_name );
							}
						}
						
						
						editor.putBoolean(PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_ALERT,isEnable );
						
						editor.commit();
						
						return true;
					}
					
				});
			}
			
			
		}
	}
	

	@Override
	public void pre_update() {
		//Send query to server
		showDialog(DIALOG_UPDATING_CAMERA_STATUS);
		
	}
	

	
	@Override
	public void update_alert_success() {
		save_session_data();
		
		try {
			dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
		}
		catch (IllegalArgumentException  ie)
		{
		}
		catch (BadTokenException  ie)
		{
		}	
		
	}

	@Override
	public void update_alert_failed(int alertType, boolean enableOrDisable) {
		try {
			dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
		}
		catch (IllegalArgumentException  ie)
		{
		}
		catch (BadTokenException  ie)
		{
		}
		//DONT save session data here 
		
		boolean isAlertEnabled ;
		CheckBoxPreference alert;
		
		switch (alertType)
		{
		case VoxMessage.ALERT_TYPE_SOUND:
			isAlertEnabled= cameraProfile.isSoundAlertEnabled();
			cameraProfile.setSoundAlertEnabled(!isAlertEnabled);
			
			alert = (CheckBoxPreference)findPreference("sound_alert");
			if (alert != null)
			{
				alert.setChecked(!isAlertEnabled);
			}
			
			break;
		case VoxMessage.ALERT_TYPE_TEMP_HI:
			isAlertEnabled= cameraProfile.isTempHiAlertEnabled();
			cameraProfile.setTempHiAlertEnabled(!isAlertEnabled);

			alert = (CheckBoxPreference)findPreference("tempHi_alert");
			if (alert != null)
			{
				alert.setChecked(!isAlertEnabled);
			}
			break;
		case VoxMessage.ALERT_TYPE_TEMP_LO:
			isAlertEnabled= cameraProfile.isTempLoAlertEnabled();
			cameraProfile.setTempLoAlertEnabled(!isAlertEnabled);
		
			alert = (CheckBoxPreference)findPreference("tempLo_alert");
			if (alert != null)
			{
				alert.setChecked(!isAlertEnabled);
			}
			break;
		case VoxMessage.ALERT_TYPE_MOTION_ON:
			isAlertEnabled = cameraProfile.isMotionAlertEnabled();
			cameraProfile.setMotionAlertEnabled(!isAlertEnabled);
		
			alert = (CheckBoxPreference)findPreference("motion_alert");
			if (alert != null)
			{
				alert.setChecked(!isAlertEnabled);
			}
			break;
		default://Unknown
			break;
		}
		
		try
		{
			showDialog(DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN);
		} 
		catch(Exception e)
		{
		}
	}

	
	private void updateVOXStatus()
	{
		String res = null; 
		if (comm_mode == COMM_MODE_HTTP)
		{
			String http_addr =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_STATUS) ;
			 res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
		}
		else if (comm_mode == COMM_MODE_UDT)
		{
			
			String request = PublicDefine.VOX_STATUS ;

			//TODO coment for test
//			res = UDTRequestSendRecv.sendRequest_via_stun(device_mac,
//					udtChannelId, 
//					request ,
//					portal_usrName,
//					portal_usrPwd);
//			Log.d("mbp", "updateVOXStatus() - update res: " + res);
		}
		
		
		
		if ( (res!= null) && res.startsWith(PublicDefine.VOX_STATUS))
		{
			String str_value = res.substring(PublicDefine.VOX_STATUS.length()
					+ 2 ); 
			int vox_status = Integer.parseInt(str_value.substring(0, 1));

			if (vox_status == 0)//Disabled
			{
				vox_enabled = false;
				vox_sensitivity = PublicDefine.VOX_SENSITIVITY_DISABLED;//-1 
				return; 
			}
			vox_enabled = true;

		}

		if (comm_mode == COMM_MODE_HTTP)
		{
			String http_addr; 
			//Vox enabled.. find out which level is it 
			http_addr =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_GET_THRESHOLD) ;
			res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
		}
		else if (comm_mode == COMM_MODE_UDT)
		{
			String request = PublicDefine.VOX_GET_THRESHOLD ;

			//TODO comment for test
//			res = UDTRequestSendRecv.sendRequest_via_stun(device_mac,
//					udtChannelId, 
//					request ,
//					portal_usrName,
//					portal_usrPwd);
			
			
			
		}
		Log.d("mbp", "updateVOXStatus() - VOX_GET_THRESHOLD: " + res);
		
		if ( (res!= null) && res.startsWith(PublicDefine.VOX_GET_THRESHOLD))
		{
			String str_value = res.substring(PublicDefine.VOX_GET_THRESHOLD.length()
					+ 2 ); 
			vox_sensitivity = Integer.parseInt(str_value.substring(0, 3));
		}

	}


	protected void updateTempConversion() {
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		temperature_unit = settings.getInt(PublicDefine.PREFS_TEMPERATURE_UNIT, PublicDefine.TEMPERATURE_UNIT_DEG_F);

	}


	private void updateBrightnessLevel()
	{
		int brightness_lvl = level_items.length-1; 
		String res =null;
		if (comm_mode == COMM_MODE_HTTP)
		{
			/* query the current level */
			String http_req =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.GET_BRIGHTNESS_VALUE) ;
			res=  HTTPRequestSendRecv.sendRequest_block_for_response(http_req, http_userName,http_userPass);
		}
		else if (comm_mode == COMM_MODE_UDT)
		{
			
			String request = PublicDefine.GET_BRIGHTNESS_VALUE ;

			//TODO comment for test
//			res = UDTRequestSendRecv.sendRequest_via_stun(device_mac,
//					udtChannelId, 
//					request ,
//					portal_usrName,
//					portal_usrPwd);
//			Log.d("mbp", "updateBrightnessLevel() - update res: " + res);
			
		}
		
		
		if ( (res!= null) && res.startsWith(PublicDefine.GET_BRIGHTNESS_VALUE))
		{
			final String str_value = res.substring(PublicDefine.GET_BRIGHTNESS_VALUE.length()
					+2); 
			brightness_lvl = Integer.parseInt(str_value.substring(0, 1));
			//Log.e("mbp","brightness_lvl: " + brightness_lvl);

		}

		//Update the level 
		this.camera_brightness = brightness_lvl/2;
		
		if (this.camera_brightness > (level_items.length-1	) )
		{
			this.camera_brightness = (level_items.length-1);
		}
	}


	private void updateVolLevel()
	{
		int raw_level = 100; 
		String res =null;
		if (comm_mode == COMM_MODE_HTTP)
		{
			/* query the current level */
			String http_req =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.GET_VOLUME) ;
			res = HTTPRequestSendRecv.sendRequest_block_for_response(http_req, http_userName,http_userPass);

		}
		else if (comm_mode == COMM_MODE_UDT)
		{
			

			String request = PublicDefine.GET_VOLUME ;

			//TODO comment for test
//			res = UDTRequestSendRecv.sendRequest_via_stun(device_mac,
//					udtChannelId, 
//					request ,
//					portal_usrName,
//					portal_usrPwd);
//			Log.d("mbp", "updateVolLevel() - update res: " + res);
			
		}
		
		
		if ( (res!= null) && res.startsWith(PublicDefine.GET_VOLUME))
		{
			final String str_value = res.substring(PublicDefine.GET_VOLUME.length()
					+2); 
			
			Log.d("mbp","vol: " + str_value);
			
			if (str_value.length() > 3)
			{
				try 

				{
					raw_level = Integer.parseInt(str_value.substring(0, 3));
				}
				catch (NumberFormatException nfe)
				{
					raw_level = Integer.parseInt(str_value.substring(0, 2));
				}
			}
			else
			{
				try 
				{
					raw_level = Integer.parseInt(str_value.substring(0));
				}
				catch (NumberFormatException nfe)
				{
				}
			}

		}


		this.camera_volume = (raw_level/15  - 1);
		//possible value 0-3
		if (this.camera_volume >3)
		{
			this.camera_volume = 3;
		}
		if (this.camera_volume<0)
		{
			this.camera_volume = 0;
		}

	}
	
	/**
	 * Notes: run this in background thread 
	 */
	private void queryCameraSettings()
	{
		Bundle extra = getIntent().getExtras();
		boolean goToShortSubmenu = false;
		if (extra != null)
		{
			goToShortSubmenu = extra.getBoolean(bool_shouldGotoShortSubmenu, false);
		}
		
		
		if (goToShortSubmenu ==true)
		{

			//query
			GetDisabledAlertsTask getAlerts= new GetDisabledAlertsTask(this, null, this.portal_usrToken); 
			getAlerts.execute(new CamProfile[]{cameraProfile});
			//we wait here .. 

			try {
				getAlerts.get(5000, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
			
		}
		else
		{
			
			if (device_address_port != null)
			{
				updateVOXStatus();
				updateTempConversion();
				updateBrightnessLevel();
				updateVolLevel();
			}
		}
		
		
		
		
	}
	
	

	private void onBrightness(View v)
	{

		final Thread worker = new Thread (new UpdateBrightness(comm_mode));

		/* dont do anything if we don't have the device addr and port at the moment */
		if (device_address_port == null) 
		{
			Log.e("mbp", "No device found!");
			showDialog(DIALOG_IS_NOT_REACHABLE);
			return;
		}


		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Set brightness level");
		builder.setSingleChoiceItems(level_items, this.camera_brightness, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				camera_brightness =item;
				showDialog(DIALOG_UPDATING_CAMERA_STATUS);
				worker.start();
				//* update items
				setupMenuItems();
				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();

		alert.show();

	}
	private void onName (final View v)
	{
		if (portal_usrToken == null)
		{
			showDialog(DIALOG_CAMERA_NOT_REMOVABLE_1);
			return; // dont do anything
		}

		String gatewayIp = SingleCamConfigureActivity.getGatewayIp(this);
		if ( device_name.equalsIgnoreCase(PublicDefine.DEFAULT_CAM_NAME) &&
				device_address_port.startsWith(gatewayIp))
		{
			//Direct mode - change name is not allowed 
			AlertDialog.Builder builder = new AlertDialog.Builder(CameraMenuActivity.this);
			builder.setMessage(getResources().getString(R.string.change_camera_name_is_not_allowed_in_direct_mode_))
			.setNeutralButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
			return;
		}



		final Dialog dialog = new Dialog(this,R.style.myDialogTheme);
		dialog.setContentView(R.layout.bb_camera_menu_camera_name);
		dialog.setCancelable(true);


		//setup connect button
		Button connect = (Button) dialog.findViewById(R.id.change_btn);
		connect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText text = (EditText) dialog.findViewById(R.id.text_new_name);
				if (text == null)
				{
					return;
				}

				device_name = text.getText().toString().trim();
				
				/* 20140211: hoang: issue 330
				 * length of camera name must be between 4 to 31 characters.
				 */
				if ( (device_name.length() < 4) || (device_name.length() > 31) )
				{
					showDialog(DIALOG_CAMERA_NAME_LENGTH_IS_OUT_OF_BOUNDARY);
					text.setText("");
				}
				else if (LoginOrRegistrationActivity.validate(
						LoginOrRegistrationActivity.CAMERA_NAME, device_name) == false)
				{
					showDialog(DIALOG_CAMERA_NAME_IS_INVALID);
					text.setText("");
				}
				else
				{
					showDialog(DIALOG_UPDATING_CAMERA_STATUS);

					ChangeNameTask rename = new ChangeNameTask(
							CameraMenuActivity.this, CameraMenuActivity.this);
					rename.execute(portal_usrToken, device_name, device_mac);
					dialog.cancel();
				}
			}
		});


		Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		dialog.show();
		return;
	}

	private void onVolLevel (final View v)
	{

		if ( device_address_port == null)
		{
			Log.e("mbp","device_address_port is NULL!");
			showDialog(DIALOG_IS_NOT_REACHABLE);
			return; // dont do anything
		}

		final int oldValue = this.camera_volume;
		final Thread worker = new Thread (new Runnable() {

			@Override
			public void run() {
   
				String http_req = null;
				String res = null;
				//possible value: 0 -- 3
				
				/*requirement: "Level 4" = current level 3  = 75%
				  requirement: "Level 3" = current level 2  = 50%...
				                 .. "Level 1" = reduce abit -> 10% 
				  Thus, vol = 0 +1)*15-> 15%
				        vol = 1 +1)*15-> 30
				        vol = 2 +1)*15-> 45
				        vol = 3 +1)*15-> 75 %
				
				*/
				
				int level = (CameraMenuActivity.this.camera_volume<=0)?1:(CameraMenuActivity.this.camera_volume+1);
				
				if (isLocalCamera)
				{
					/* query the current level */
					http_req =String.format("%1$s%2$s%3$s","http://",
							device_address_port,
							PublicDefine.HTTP_CMD_PART+PublicDefine.SET_VOLUME+ 
							PublicDefine.SET_VOLUME_PARAM_1+(level*15)) ;
					res = HTTPRequestSendRecv.sendRequest_block_for_response(http_req, http_userName,http_userPass);
				}
				else
				{

					String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.SET_VOLUME + 
							PublicDefine.SET_VOLUME_PARAM_1+(level*15);

					Log.d("mbp", "onVolLevel() - set request: " + PublicDefine.SET_VOLUME + 
							PublicDefine.SET_VOLUME_PARAM_1+(level*15));

					res = UDTRequestSendRecv.sendRequest_via_stun2(
							portal_usrToken,
							device_mac,
							request);
					Log.d("mbp", "onVolLevel() - set res: " + res);
					
					

				}

				runOnUiThread( new Runnable() {
					public void run() {
						dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
					}
				});


				if ( (res!= null) && res.startsWith(PublicDefine.SET_VOLUME +": "))
				{
					String cmd_part = PublicDefine.SET_VOLUME +": ";

					int retVal = -1;
					try
					{
						retVal= Integer.parseInt(res.substring(cmd_part.length(),cmd_part.length()+1 ));
					}
					catch(NumberFormatException nfe)
					{
						retVal = -1;
					}


					if (retVal != 0)
					{

						runOnUiThread( new Runnable() {
							public void run() {
								/* update has failed, revert  the old value */
								CameraMenuActivity.this.camera_volume = oldValue;
								showDialog(DIALOG_UPDATING_CAMERA_STATUS_FAILED);
							}
						});

					}
				}
				else
				{
					runOnUiThread( new Runnable() {
						public void run() {
							/* update has failed, revert  the old value */
							CameraMenuActivity.this.camera_volume = oldValue;
							showDialog(DIALOG_UPDATING_CAMERA_STATUS_FAILED);
						}
					});
				}

				runOnUiThread( new Runnable() {
					public void run() {
						//* update items
						setupMenuItems();
					}
				});


			}
		});


		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.set_volume_level));
		builder.setSingleChoiceItems(level_items, this.camera_volume, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				
				//possible value: 0 -- 3
				camera_volume =item;
				showDialog(DIALOG_UPDATING_CAMERA_STATUS);


				worker.start();


				dialog.dismiss();

			}
		});
		AlertDialog alert = builder.create();

		alert.show();


	}


	private  void onInfo(View v)  {

		if ( device_address_port == null)
		{

			showDialog(DIALOG_IS_NOT_REACHABLE);
			return; // dont do anything
		}
		
		String response = null;
		if (isLocalCamera)
		{
			/*query current version */
			String http_addr =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.GET_VERSION) ;
			
			response = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
	
		}
		else //if (comm_mode ==COMM_MODE_UDT)
		{

			String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_VERSION;

			response = UDTRequestSendRecv.sendRequest_via_stun2(
					portal_usrToken,
					device_mac,
					request);
			Log.d("mbp", "onInfo() - set res: " + response);
		}
		
		
		String version = null;
		if ( (response != null) &&
				(response.startsWith(PublicDefine.GET_VERSION)))
		{
			version = response.substring(PublicDefine.GET_VERSION.length() + 1);
		}

		if(version!=null && version.startsWith("-1"))
		{
			version = "Unknown";
		}

		String app_version = null;
		PackageInfo pinfo;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			app_version = pinfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			app_version= "Unknown";
		}

		String message = String.format(getResources().getString(R.string.CameraMenuActivity_cam_info),
				app_version, version);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message)
		.setCancelable(true)
		.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface _dialog, int which) {
				_dialog.dismiss();
			}
		});

		builder.create().show();

	}




	private void onTempConversion(View v) {
		if ( device_address_port == null)
		{

			showDialog(DIALOG_IS_NOT_REACHABLE);
			return; // dont do anything
		}


		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Set Temperature unit");
		builder.setSingleChoiceItems(temp_items, this.temperature_unit, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				temperature_unit =item;


				SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt(PublicDefine.PREFS_TEMPERATURE_UNIT, temperature_unit);
				editor.commit();

				//* update items
				setupMenuItems();

				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();

		alert.show();

	}


	


	private  void onVOX (final View v)
	{

		/* dont do anything if we don't have the device addr and port at the moment */
		if (device_address_port == null) 
		{

			showDialog(DIALOG_IS_NOT_REACHABLE);
			return;
		}
		int vox_status = 0;
		String res= null;
		String http_addr = null;
		if (isLocalCamera)
		{
			/* find out the current status */
			http_addr =String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_STATUS) ;
			res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
		}	
		else //if (comm_mode ==COMM_MODE_UDT)
		{
			/*
			 * 20130807: hoang:
			 * New upnp flow also send cmd via stun
			 */
			String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.VOX_STATUS;

			res = UDTRequestSendRecv.sendRequest_via_stun2(
					portal_usrToken,
					device_mac,
					request);
			Log.d("mbp", "onVOX() - set res: " + res);
		}
		
		
		
		if ( (res!= null) && res.startsWith(PublicDefine.VOX_STATUS))
		{
			String str_value = res.substring(PublicDefine.VOX_STATUS.length()
					+ 2 ); 

			try
			{
				vox_status = Integer.parseInt(str_value);
			}
			catch( NumberFormatException nfe)
			{
				return; 
			}



			if (vox_status == 1)
			{
				
				if (isLocalCamera)
				{
					
					http_addr =String.format("%1$s%2$s%3$s","http://",
							device_address_port,
							PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_GET_THRESHOLD) ;
					res = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
				}	
				else //if (comm_mode ==COMM_MODE_UDT)
				{
					String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.VOX_GET_THRESHOLD;

					res = UDTRequestSendRecv.sendRequest_via_stun2(
							portal_usrToken,
							device_mac,
							request);
					Log.d("mbp", "onVOX() - set res: " + res);
				}
				

				if ( (res!= null) && res.startsWith(PublicDefine.VOX_GET_THRESHOLD))
				{
					str_value = res.substring(PublicDefine.VOX_GET_THRESHOLD.length()
							+ 2 ); 
					try
					{
						vox_status = Integer.parseInt(str_value);
					}
					catch( NumberFormatException nfe)
					{
						return; 
					}

					vox_status = Arrays.asList(vox_item_values).indexOf(new Integer(vox_status));
					vox_status = (vox_status==-1)?0:vox_status;
				}		
			}
			else
			{
				vox_status = -1; // so that there is no pre-selected option 
			}
		}



		

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.CameraMenuActivity_vox_setting));	
		builder.setSingleChoiceItems(vox_items, vox_status, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int position) {
				switch (position)
				{
				case 0:
					vox_sensitivity = PublicDefine.VOX_SENSITIVITY_LOW;
					break;
				case 1:
					vox_sensitivity = PublicDefine.VOX_SENSITIVITY_MED;
					break;
				case 2:
					vox_sensitivity = PublicDefine.VOX_SENSITIVITY_HI;
					break;
				case 3:
					vox_sensitivity = PublicDefine.VOX_SENSITIVITY_V_HI;
					break;
				default: 
					vox_sensitivity = PublicDefine.VOX_SENSITIVITY_MED;
					break;

				}
				
				//Runnable url_req = 
				new Thread(new Runnable() {
					public void run()
					{
						if (vox_sensitivity == PublicDefine.VOX_SENSITIVITY_DISABLED)
						{
							
							if (isLocalCamera)
							{
								/* disable VOX */
								String http_addr =String.format("%1$s%2$s%3$s","http://",
										device_address_port,
										PublicDefine.HTTP_CMD_PART+PublicDefine.VOX_DISABLE) ;
								HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);
							}
							else //if (comm_mode ==COMM_MODE_UDT)
							{
								String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.VOX_DISABLE;

								UDTRequestSendRecv.sendRequest_via_stun2(
										portal_usrToken,
										device_mac,
										request);
								
							}
						}
						else
						{

							if (isLocalCamera)
							{
								/* 
								 * - Write HTTP request to device to setup the db threshold ()
								 * - Start the service
								 */
								String http_addr = String.format("%1$s%2$s%3$s%4$s%5$s",
										"http://",
										device_address_port,
										"/?action=command&command=" +PublicDefine.VOX_SET_THRESHOLD,
										"&",PublicDefine.VOX_SET_THRESHOLD_VALUE+vox_sensitivity);

								// Blocking post
								HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName, http_userPass);

								http_addr = String.format("%1$s%2$s%3$s",
										"http://",
										device_address_port,
										"/?action=command&command=" +PublicDefine.VOX_ENABLE);

								/* Blocking post */
								HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_userName,http_userPass);

							}
							else //if (comm_mode == COMM_MODE_UDT)
							{
								String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.VOX_SET_THRESHOLD +"&" + 
										PublicDefine.VOX_SET_THRESHOLD_VALUE + vox_sensitivity;
								

								UDTRequestSendRecv.sendRequest_via_stun2(
										portal_usrToken,
										device_mac,
										request);
								
								request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.VOX_ENABLE;
								UDTRequestSendRecv.sendRequest_via_stun2(
										portal_usrToken,
										device_mac,
										request);
							}

						}

						dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
					}
				}).start();
				
				showDialog(DIALOG_UPDATING_CAMERA_STATUS);
				//v.post(url_req);

				//* update items
				setupMenuItems();
				dialog.dismiss();

			}
		});
				
		AlertDialog alert = builder.create();
		alert.show();

	}


	private void onVideoQuality(View v)
	{
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);

		if ( device_address_port == null)
		{

			showDialog(DIALOG_IS_NOT_REACHABLE);
			return; // dont do anything
		}


		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Set Video Quality");
		builder.setSingleChoiceItems(videoQuality, img_res, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putInt(PublicDefine.PREFS_VQUALITY, item);
				editor.commit();
				
				//* update items
				setupMenuItems();

				dialog.dismiss();
			}
		});
		AlertDialog alert = builder.create();

		alert.show();

	}


	private void onRemoveCamera_online()
	{
		showDialog(DIALOG_UPDATING_CAMERA_STATUS);

		RemoveCameraTask try_delCam = new RemoveCameraTask();
		try_delCam.setOnlyRemoveFrServer(true);
		try_delCam.execute(portal_usrToken, device_mac);

	}
	
	
	private void onRemoveCamera()
	{
		showDialog(DIALOG_UPDATING_CAMERA_STATUS);

		/* read the keystore and create the ssl context for HTTPS connections*/
		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(this);

	
		// broadcast ... 
		Intent intent = new Intent();
		intent.setAction(ACTION_CAMERA_REMOVED);
		intent.putExtra(ACTION_CAMERA_REMOVED_MAC,device_mac);
		sendBroadcast(intent);
		
		
		RemoveCameraTask try_delCam = new RemoveCameraTask();
		try_delCam.execute(portal_usrToken, device_mac,
				device_address_port, http_userName, http_userPass);

	}

	private void del_cam_success() {

		Log.d("mbp","delete cam done.. send broadcast ");
		

		UserAccount online_user;
		try {
			online_user = new UserAccount(
					portal_usrToken, getExternalFilesDir(null), this.ssl_context, this);
		} catch (StorageException e1) {
			Log.d("mbp", e1.getLocalizedMessage());
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
			return;
		}
		try {
			online_user.sync_user_data();
		} catch (IOException e) {
			e.printStackTrace();
		}


		dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);

		showDialog(DIALOG_REMOVE_DONE);

	}
	private void del_cam_failed() {
		dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
		showDialog(DIALOG_REMOVE_FAILED);

	}


	

	///Used by ChangeNameTask
	public void update_cam_success() {

		UserAccount online_user;
		try {
			online_user = new UserAccount(portal_usrToken, getExternalFilesDir(null), this.ssl_context, this );
		} catch (StorageException e1) {
			Log.d("mbp", e1.getLocalizedMessage());
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
			return;
		}
		try {
			online_user.sync_user_data();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (textCamName!= null)
		{
			textCamName.setText(CameraMenuActivity.this.device_name);
		}

		Intent data = new Intent() ; 
		data.putExtra(str_deviceMac_out, device_mac);
		data.putExtra(str_deviceName_out, device_name);
		CameraMenuActivity.this.setResult(CAMERA_INFO_UPDATED,data);

		//* update items
		setupMenuItems();



		dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);

	}
	///Used by ChangeNameTask
	public void update_cam_failed() {
		dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);

	}

	

	
	/**************** PRIVATE HELPER CLASSES *************************************/
	
	
	
	class RemoveCameraTask extends AsyncTask<String, String, Integer> {

		private String usrToken;
		private String macAddress;
		private String camName;


		private boolean only_remove_from_server;

		private static final int DEL_CAM_SUCCESS = 0x1;
		private static final int DEL_CAM_FAILED_UNKNOWN = 0x2;
		private static final int DEL_CAM_FAILED_SERVER_DOWN = 0x11;

		public RemoveCameraTask()
		{
			only_remove_from_server = false;
		}

		public void setOnlyRemoveFrServer(boolean b)
		{
			only_remove_from_server = b;
		}

		/* Dont need to support UDT because 
		 * Remove camera in remote will only remove camera from server   
		 *
		 */
		@Override
		protected Integer doInBackground(String... params) {

			usrToken = params[0];
			macAddress = params[1];

			String http_usr, http_pass, http_cmd; 


			macAddress= PublicDefine.strip_colon_from_mac(macAddress).toUpperCase();

			
			if (only_remove_from_server == false)
			{
				String device_ip_and_port = params[2];
				/*20120911:   
				 *  OBSOLETE: switch_to_uAP mode & reset
				 *  
				 *  reset_factory & restart_system
				 */ 

				http_usr = params[3];
				http_pass = params[4];

				String http_addr =String.format("%1$s%2$s%3$s%4$s","http://",
						device_ip_and_port,
						PublicDefine.HTTP_CMD_PART, PublicDefine.RESET_FACTORY) ;

				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_usr,http_pass);

				http_addr =String.format("%1$s%2$s%3$s%4$s","http://",
						device_ip_and_port,
						PublicDefine.HTTP_CMD_PART, PublicDefine.RESTART_DEVICE) ;
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_usr,http_pass);

				/* SEND THIS CMD UNTIL WE DON'T GET THE RESPONSE
				 * That means the device is already reset
				 */
				String test_conn = null;

				do
				{
					http_addr =String.format("%1$s%2$s%3$s%4$s","http://",
							device_ip_and_port,
							PublicDefine.HTTP_CMD_PART, PublicDefine.GET_VERSION) ;
					test_conn = HTTPRequestSendRecv.sendRequest_block_for_response(http_addr, http_usr,http_pass);
				} while (test_conn != null);

			}
			
			int ret = -1;
			try {
				SimpleJsonResponse response = Device.delete(usrToken, macAddress);
				if (response != null && response.isSucceed())
				{
					int status_code = response.getStatus();
					Log.d("mbp", "Del cam response code: "+status_code);
					if (status_code == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = DEL_CAM_FAILED_UNKNOWN;
					}
					else if (status_code == HttpURLConnection.HTTP_OK)
					{
						ret = DEL_CAM_SUCCESS;
					}
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			} catch (SocketTimeoutException e)
			{
				//Connection Timeout - Server unreachable ??? 
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e) {
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (result.intValue() == DEL_CAM_SUCCESS)
			{
				CameraMenuActivity.this.del_cam_success();
			}
			else
			{
				CameraMenuActivity.this.del_cam_failed();
			}
		}
	}

	
	private class UpdateBrightness implements Runnable 
	{
		private int commMode; 
		UpdateBrightness (int commMode)
		{
			this.commMode = commMode;
		}
		public void run()
		{
			int brightness_lvl = camera_brightness;

			String http_req = null;
			String request = null;
			String res = null;
			int status =  0; 
			
			if (isLocalCamera)
			{
				do 
				{
					/* query the current level */
					http_req =String.format("%1$s%2$s%3$s","http://",
							device_address_port,
							PublicDefine.HTTP_CMD_PART+PublicDefine.GET_BRIGHTNESS_VALUE) ;
					res = HTTPRequestSendRecv.sendRequest_block_for_response(http_req, http_userName,http_userPass);
					if ( (res!= null) && res.startsWith(PublicDefine.GET_BRIGHTNESS_VALUE))
					{
						final String str_value = res.substring(PublicDefine.GET_BRIGHTNESS_VALUE.length()
								+2); 
						brightness_lvl = Integer.parseInt(str_value);
						//Log.e("mbp","set brightness_lvl: " + brightness_lvl);

						if (brightness_lvl < 0 )
						{
							//ERROR
							status =-1;
							break;
						}

					}
					else
					{
						//ERROR
						status =-1;
						break;
					}

					/* plus or minus */ 
					if ( (brightness_lvl/2) < camera_brightness)
					{
						http_req =String.format("%1$s%2$s%3$s","http://",
								device_address_port,
								PublicDefine.HTTP_CMD_PART+PublicDefine.GET_BRIGHTNESS_PLUS) ;
					}
					else if  ( (brightness_lvl/2) > camera_brightness)
					{

						http_req =String.format("%1$s%2$s%3$s","http://",
								device_address_port,
								PublicDefine.HTTP_CMD_PART+PublicDefine.GET_BRIGHTNESS_MINUS) ;

					}
					else
					{
						//OK
						status =0 ;
						break;
					}

					res = HTTPRequestSendRecv.sendRequest_block_for_response(http_req, http_userName,http_userPass);
					if (res == null)
					{
						//ERROR 
						status =-1;
						break;
					}

				} while ( (brightness_lvl/2) != camera_brightness);

			}
			else //if (commMode == CameraMenuActivity.COMM_MODE_UDT)
			{
				/*
				 * 20130807: hoang:
				 * New upnp flow also send control cmd via stun
				 */
				status =0;
				do 
				{
					
					request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_BRIGHTNESS_VALUE ;

					res = UDTRequestSendRecv.sendRequest_via_stun2(
							portal_usrToken,
							device_mac, 
							request);
					
					
					if ( (res!= null) && res.startsWith(PublicDefine.GET_BRIGHTNESS_VALUE))
					{
						final String str_value = res.substring(PublicDefine.GET_BRIGHTNESS_VALUE.length()
								+2); 
						brightness_lvl = Integer.parseInt(str_value);
						

						
						if (brightness_lvl < 0 )
						{
							Log.e("mbp","set brightness_lvl: " + brightness_lvl);
							//ERROR
							status =-1;
							break;
						}
						
					}
					else
					{
						//ERROR
						status =-1;
						break;
					}
					
					
					
					

					/* plus or minus */ 
					if ( ((brightness_lvl +1)/2) < camera_brightness)
					{
						request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_BRIGHTNESS_PLUS ;
						
					}
					else if  ( ((brightness_lvl +1)/2) > camera_brightness)
					{
						request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_BRIGHTNESS_MINUS ;
					}
					else 
					{
						//OK
						status =0;
						break;
					}

					res = UDTRequestSendRecv.sendRequest_via_stun2(
							portal_usrToken,
							device_mac, 
							request);

					if (res == null)
					{
						//ERROR 
						status =-1;
						break;
					}

				} while ( (brightness_lvl/2) != camera_brightness);
			} // camera not in local 
			
			
			final int _status = status; 
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					dismissDialog(DIALOG_UPDATING_CAMERA_STATUS);
					
					if (_status ==-1)
					{
						//Some error
						showDialog(DIALOG_UPDATING_CAMERA_STATUS_FAILED);
					}
				}
			});
			
			
			
		}
	}


	
	
	

	
	
	
	
}