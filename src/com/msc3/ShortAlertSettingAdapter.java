package com.msc3;

import com.blinkhd.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.view.*;

public class ShortAlertSettingAdapter extends BaseAdapter
{

	private static final int SOUND_DETECTED_INDEX = 0;
	private static final int TEMP_HI_INDEX = 1;
	private static final int TEMP_LO_INDEX = 2;
	private static final int MOTION_DETECTED_INDEX = 3;
	// only available for MBP
	private static final int RECURRING_ALERT = 4;
	private static final int ALERT_WHILE_IN_CALL = 5;
	private static final int CAMERA_CONNECTIVITY_DETECTED_INDEX = 6;

	private String[] alarmItems ; 
	private  String[] alarmItems_mbp ;
	private Context mContext; 
	
	private CamChannel selected_channel;
	
	private IUpdateAlertCallBack uiCallBack; 
	
	
	
	public ShortAlertSettingAdapter (Context c, CamChannel channel,IUpdateAlertCallBack cb)
	{
		mContext = c; 
		selected_channel = channel;
		uiCallBack = cb;
		
		alarmItems= mContext.getResources().getStringArray(R.array.alarm_settings);
		alarmItems_mbp = mContext.getResources().getStringArray(R.array.alarm_settings_mbp);
	}
	
	
	
	public long getItemId(int position) {
		return position;
	}

	public Object getItem(int position) {

		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
				|| phonemodel.equals(PublicDefine.PHONE_MBP1k)) 
		{
			return alarmItems_mbp[position];
		}

		return alarmItems[position];
	}

	public int getCount() {
		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP2k)
				|| phonemodel.equals(PublicDefine.PHONE_MBP1k)) {
			return alarmItems_mbp.length;
		}
		return alarmItems.length;
	}

	public View getView(int position, View convertView, ViewGroup parent) 
	{
		LinearLayout itemView;
		TextView txt;
		CheckBox checkBox = null;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemView = (LinearLayout) inflater.inflate(
					R.layout.bb_alarm_list_item, null);
		} else {
			itemView = (LinearLayout) convertView;

			checkBox = (CheckBox) itemView.findViewById(R.id.checkBox1);
			checkBox.setOnCheckedChangeListener(null);
			checkBox.setChecked(false);
		}

		txt = (TextView) itemView.findViewById(R.id.AlarmItem);
		txt.setText((String) getItem(position));

		checkBox = (CheckBox) itemView.findViewById(R.id.checkBox1);
		checkBox.setEnabled(true);

		SharedPreferences settings =  mContext.getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		final String regId = settings.getString(
				PublicDefine.PREFS_PUSH_NOTIFICATION_ID, null);
		final String saved_token = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		
		switch (position) {

		case SOUND_DETECTED_INDEX:
			boolean soundAlertEnabled = selected_channel.getCamProfile().isSoundAlertEnabled();

			checkBox.setChecked(soundAlertEnabled);
			
			checkBox.setOnCheckedChangeListener(
					new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					if (selected_channel != null) {
						selected_channel.getCamProfile()
						.setSoundAlertEnabled(isChecked);

						String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD;
						if (!isChecked) {
							enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD;
						}

						String alertType = String
								.valueOf(VoxMessage.ALERT_TYPE_SOUND); // Sound
						String deviceId = String.valueOf(selected_channel
								.getCamProfile().getCamId());

						UpdateAlertTask alertUpdate = new UpdateAlertTask(
								mContext,
								uiCallBack );
						// Session data will be saved after the task is
						// done - update_alert_success();
						alertUpdate.execute(saved_token, deviceId,
								alertType, enableOrDisabled);
					}
				}
			});
			break;
		case TEMP_HI_INDEX:
			boolean tempHiAlertEnabled = selected_channel
			.getCamProfile().isTempHiAlertEnabled();
			checkBox.setChecked(tempHiAlertEnabled);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					if (selected_channel != null) {
						selected_channel.getCamProfile()
						.setTempHiAlertEnabled(isChecked);

						String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD;
						if (!isChecked) {
							enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD;
						}

						String alertType = String
								.valueOf(VoxMessage.ALERT_TYPE_TEMP_HI);
						String deviceId = String.valueOf(selected_channel
								.getCamProfile().getCamId());

						UpdateAlertTask alertUpdate = new UpdateAlertTask(
								mContext,
								uiCallBack );
						// Session data will be saved after the task is
						// done - update_alert_success();
						alertUpdate.execute(saved_token, deviceId,
								alertType, enableOrDisabled);
					}
				}
			});

			break;
		case TEMP_LO_INDEX:
			boolean tempLoAlertEnabled = selected_channel
			.getCamProfile().isTempLoAlertEnabled();
			checkBox.setChecked(tempLoAlertEnabled);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					selected_channel.getCamProfile()
					.setTempLoAlertEnabled(isChecked);

					String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD;
					if (!isChecked) {
						enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD;
					}

					String alertType = String
							.valueOf(VoxMessage.ALERT_TYPE_TEMP_LO);
					String deviceId = String.valueOf(selected_channel
							.getCamProfile().getCamId());

					UpdateAlertTask alertUpdate = new UpdateAlertTask(
							mContext,
							uiCallBack );
					alertUpdate.execute(saved_token, deviceId,
							alertType, enableOrDisabled);
				}
			});

			break;
			
		case MOTION_DETECTED_INDEX:
			boolean motionAlertEnabled = selected_channel
			.getCamProfile().isMotionAlertEnabled();
			checkBox.setChecked(motionAlertEnabled);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					selected_channel.getCamProfile()
					.setMotionAlertEnabled(isChecked);

					String enableOrDisabled = PublicDefine.ENABLE_NOTIFICATIONS_U_CMD;
					if (!isChecked) {
						enableOrDisabled = PublicDefine.DISABLE_NOTIFICATIONS_U_CMD;
					}

					String alertType = String
							.valueOf(VoxMessage.ALERT_TYPE_MOTION_ON);
					String deviceId = String.valueOf(selected_channel
							.getCamProfile().getCamId());

					UpdateAlertTask alertUpdate = new UpdateAlertTask(
							mContext,
							uiCallBack );
					alertUpdate.execute(saved_token, deviceId,
							alertType, enableOrDisabled);
				}
			});

			break;
			
		case RECURRING_ALERT:
			checkBox.setChecked(settings.getBoolean(
					PublicDefine.PREFS_MBP_VOX_ALERT_IS_RECURRING,
					false));
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					SharedPreferences settings = mContext.getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putBoolean(
							PublicDefine.PREFS_MBP_VOX_ALERT_IS_RECURRING,
							isChecked);
					editor.commit();
					return;
				}
			});

			break;
		case ALERT_WHILE_IN_CALL:

			String phonemodel = android.os.Build.MODEL;
			if (phonemodel.equals(PublicDefine.PHONE_MBP2k)) {
				checkBox.setChecked(settings
						.getBoolean(
								PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL,
								false));
				checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(
							CompoundButton buttonView, boolean isChecked) {
						SharedPreferences settings = mContext.getSharedPreferences(
								PublicDefine.PREFS_NAME, 0);
						SharedPreferences.Editor editor = settings
								.edit();
						editor.putBoolean(
								PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL,
								isChecked);
						editor.commit();
						return;
					}
				});
			} else if (phonemodel.equals(PublicDefine.PHONE_MBP1k)) {
				checkBox.setChecked(false);
				checkBox.setEnabled(false);
			}

			break;

		case CAMERA_CONNECTIVITY_DETECTED_INDEX:
			String phoneModel = android.os.Build.MODEL;
			if (phoneModel.equals(PublicDefine.PHONE_MBP2k)
					|| phoneModel.equals(PublicDefine.PHONE_MBP1k)) {
				checkBox.setChecked(settings.getBoolean(
						PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_ALERT,
						false));
				checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(
							CompoundButton buttonView, boolean isChecked) {
						SharedPreferences settings = mContext.getSharedPreferences(
								PublicDefine.PREFS_NAME, 0);

						SharedPreferences.Editor editor = settings
								.edit();

						if (isChecked == true) {
							WifiManager w = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
							if ((w != null) && (w.isWifiEnabled())
									&& (w.getConnectionInfo() != null)) {
								String _wifi_name = w
										.getConnectionInfo().getSSID();

								Log.d("mbp",
										"EntryAct: Store wifi for camera alert : "
												+ _wifi_name);

								editor.putString(
										PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_HOME_WIFI,
										_wifi_name);
							}
						}

						editor.putBoolean(
								PublicDefine.PREFS_MBP_CAMERA_DISCONNECT_ALERT,
								isChecked);
						editor.commit();

						return;
					}
				});
			}
			break;

		}

		return itemView;
	}


}
