package com.msc3;

import java.util.List;

import android.net.wifi.ScanResult;


public interface IWifiScanUpdater {
	
	public void updateWifiScanResult(List<ScanResult> result);
	
	public void scanWasCanceled();
}
