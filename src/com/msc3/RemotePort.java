package com.msc3;

import java.util.Arrays;

public class RemotePort {

	public static final String [] _PORT_NAME = {"video_port", "audio_port", "vox_port"};
	
	public static final int [] LOCAL_PORT = {80, 51108, 51109};
	// add new port definition here.. 
	
	public static final int AV_PORT_IDX = 0;
	public static final int PTT_PORT_IDX = 1;
	public static final int VOX_PORT_IDX = 3;
	
	private String name; 
	private int remote_port_number;
	private int local_port_number;
	
	
	/**
	 * @param port
	 * @return the index to be used in the result list. 
	 * This is used again when query for the port
	 */
	public static int getIndex(String portName) throws UndefinedPortException
	{
		int idx = 0;
		idx = Arrays.asList(_PORT_NAME).indexOf(portName);
		if (idx < 0)
		{
			throw new UndefinedPortException("Port name: " + portName + " is not defined.");
		}
		return idx;
	}
	
	
	public RemotePort(String name, int port) throws UndefinedPortException
	{
		this.name = name;
		this.remote_port_number = port;
		
		int idx = 0;
		idx = Arrays.asList(_PORT_NAME).indexOf(name);
		if (idx < 0)
		{
			throw new UndefinedPortException("Port name: " + name + " is not defined.");
		}
		
		this.local_port_number =  LOCAL_PORT[idx];
		
	}

	public String getName()
	{
		return name;
	}
	public int getRemotePort()
	{
		return remote_port_number;
	}
	public int getLocalPort()
	{
		return local_port_number;
	}
	
	public String toString()
	{
		return name + ":" + remote_port_number;
	}
	
}
