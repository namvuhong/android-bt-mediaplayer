package com.msc3;


import com.blinkhd.R;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.BadTokenException;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VerticalSeekBar;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.blinkhd.MBPActivity;
import com.blinkhd.gcm.AlertData;
import com.blinkhd.gcm.GetDisabledAlertsTask;
import com.discovery.LocalScanForCameras;
import com.discovery.ScanProfile;
import com.msc3.update.CheckVersionFW;
import com.msc3.update.IpAndVersion;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CloseSessionResponse;

import cz.havlena.ffmpeg.ui.FFMpegPlayerActivity;

public class ViewCameraActivity extends Activity implements Callback,
IVideoSink, ITemperatureUpdater, IMelodyUpdater, IResUpdater
{

	public static final String GA_VIEW_CAMERA_CATEGORY = "View Remote Camera";

	public static final String string_voxDeviceAddr = "vox_device_mac_address";

	public static final int DIALOG_CONNECTION_FAILED = 2;

	public static final int DIALOG_BMS_CONNECTION_IN_PROGRESS = 6;

	public static final int DIALOG_REMOTE_BM_IS_OFFLINE = 8;
	public static final int DIALOG_CAMERA_PORT_IS_INACCESSIBLE = 10;

	public static final int DIALOG_VIDEO_STOPPED_UNEXPECTEDLY = 12;
	public static final int DIALOG_WIFI_CANT_RECONNECT = 13;

	public static final int DIALOG_REMOTE_VIDEO_STREAM_TIMEOUT = 15;
	public static final int DIALOG_REMOTE_VIDEO_STREAM_STOPPED_UNEXPECTEDLY = 16;
	public static final int DIALOG_REMOTE_BM_IS_BUSY = 17;

	public static final int DIALOG_BMS_GET_PORT_ERROR = 21;
	public static final int DIALOG_NEED_TO_LOGIN = 22;

	public static final int DIALOG_STORAGE_UNAVAILABLE = 24;

	public static final int DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_SNAPSHOT = 27;
	public static final int DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_VIDEO = 28;

	public static final int DIALOG_BMS_GET_STREAM_MODE_ERROR = 31;
	public static final int DIALOG_FAILED_TO_UNMUTE_CAM_AUDIO = 32;
	public static final int DIALOG_UDT_RELAY_CONNECTION_IN_PROG = 33;


	public static final int DIALOG_FW_PATCH_FOUND = 36;
	public static final int DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN = 37;
	public static final int DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW = 38;


	public static final int DIALOG_SESSION_KEY_MISMATCHED = 41;
	public static final int DIALOG_CAMERA_IS_NOT_AVAILABLE = 42;



	/* Start Camera Menu activity request */
	private static final int REQUEST_START_CAMERA_MENU = 0x200;

	/* Connection Constant */
	public static final int CONNECTION_MODE_LOCAL_INFRA = 1;
	public static final int CONNECTION_MODE_REMOTE = 2;

	private static final int DEFAUL_ZOOM_LEVEL = 5;
	private static final double ZOOM_STEP = 0.10;

	/*
	 * 20121119: phung: Merely used to keep track of whether or not app should
	 * terminate the connection when stopped.
	 */
	private static final int REASON_UNKNOWN = 0;
	private static final int REASON_LAUNCH_OTHER_ACTIVITY = 1;
	private static final int REASON_GO_TO_HOME_SCREEN = 2;
	private int reason_to_leave = REASON_UNKNOWN;

	// min
	private static final int VIDEO_TIMEOUT = 5 * 60 * 1000; // 5 min

	private AviRecord _aviRecorder;
	private LeftSideMenuImageAdapter leftSideMenuAdpt;
	/* indicate where recording or snapshot is in progress */
	private boolean _recordInProgress, _snapshot;
	private boolean _enableAudio = true, _enablePtt;
	private boolean talkback_enabled, record_enabled, snapshot_enabled;
	private boolean enableSpeaker = true;


	/*****  Video-Worker threads .. *****/
	private Thread streamer_thrd;
	private Thread pcmPlayer_thrd;
	private Thread _playTone;
	private Thread _timeOut;

	// and their Runnables
	private Streamer _streamer;
	private PCMPlayer _pcmPlayer;
	private PCMRecorder pcmRecorder;
	private PlayTone playTone;

	private ScreenTimeOutRunnable timeOut;
	private Timer remoteVideoTimer;

	private VideoOutOfRangeReminder outOfRange;
	private Thread _outOfRange;
	/**
	 * int shouldBeepAndShow
	 * 
	 * if >=2 then start to show the dialog and beeping ... if <2 then don't do
	 * anything just try to reset connect from here
	 */
	private int shouldBeepAndShow;


	/* Control whether to run PCMplayer in activity context
	 *    set to false to use the service context for playing audio 
	 */
	private final boolean use_pcmPlayer = false;


	/**** Direction  ****/
	private DirectionTouchListener_bb joystickListener;
	private DirectionDispatcher device_comm;
	private Thread device_comm_thrd;


	/*** Video Screen graphics ***/
	private RelativeLayout videoGrp;
	private VideoSurfaceView _vImageView;
	private Bitmap video_background;

	private long _maxSize;
	private int currentZoomInLevel;


	/**** Camera channel provides: 
	 * - device ip , device port, bm authentication object 
	 * - 
	 * ****/
	private CamChannel selected_channel;


	/*** Frame rate *****/
	private long total_frame;
	private Timer frameRateTimer;


	/*** Melody control *****/

	private final int[] melody_icons = new int[] {
			R.drawable.melody_muted_icon, R.drawable.melody_1_icon,
			R.drawable.melody_2_icon, R.drawable.melody_3_icon,
			R.drawable.melody_4_icon, R.drawable.melody_5_icon };

	private int currentMelodyIndx = 0;



	/**** not sure if needed ***/
	private LocalScanForCameras scan_task;
	private PowerManager.WakeLock wl;


	private int access_mode;
	private int currentConnectionMode;



	private String string_currentSSID = "string_currentSSID";
	boolean shouldRotateBitmap = false;
	private boolean isFullScreen;

	private boolean ACTIVITY_HAS_STOPPED;

	// flagTempHighOrLow = true: temperature is too high
	// flagTempHighOrLow = false: temperature is too low
	private boolean flagTempHighOrLow;
	private int numberOfFrameToSkip;
	private boolean isUpdatingResolution = false;
	private boolean videoIsShowing;


	private static IpAndVersion device;
	private String fileName; 

	private UDTRequestSendRecv dev_comm;

	private BroadcastReceiver ffmpeg_br;
	
	/*
	 * Transient data: update with default info @ startup and subsequently
	 * updated at runtime
	 */
	private String device_ip; /* This is the ip that will be queried for a/v data */
	private int device_audio_in_port;
	private int device_port;
	private String http_pass;
	private BabyMonitorAuthentication bm_session_auth; //


	private boolean muteAudioForCall;
	
	public static final int[] batteryImages = { R.drawable.status_icon1_5,
		R.drawable.status_icon1_1, R.drawable.status_icon1_2,
		R.drawable.status_icon1_3, R.drawable.status_icon1_4 };
	
	private Tracker tracker;
	private EasyTracker easyTracker;

	private BroadcastReceiver cameraSettingClosed = new  BroadcastReceiver() 
			{
				public void onReceive(Context context, Intent intent) {
					if (intent.getAction().equals(CameraMenuActivity.ACTION_STOP_CAMERA_VIEW))
					{
						
						stopAllThread(); 
						stopAVSService();
						// Call 2nd times
						cancelVideoStoppedReminder();

						ACTIVITY_HAS_STOPPED = true;
						
						
						// Stop Play tone if still running.
						if (_playTone != null && _playTone.isAlive()) {
							Log.d("mbp","stop play Tone thread now");
							playTone.stopPlaying();
							_playTone.interrupt();
							_playTone = null;
						}		
					}
				}
			};
			
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(ViewCameraActivity.this);
		tracker = easyTracker.getTracker();
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		currentConnectionMode = -1;

		TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		tm.listen(mPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);


		videoIsShowing = false;
		selected_channel = null;

		//Read bundle camera channel information 
		selected_channel = (CamChannel) getIntent().getExtras().getSerializable(DashBoardActivity.CAMCHANNEL_SHOWING_CHANNEL);
		//WARNING: could be null

		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PublicDefine.PREFS_CURRENT_CAMERA_MODE, -1);
		editor.putString(PublicDefine.PREFS_CAM_BEING_VIEWED, selected_channel.getCamProfile().get_MAC());
		// Commit the edits!
		editor.commit();
		Log.d("mbp", "ViewCameraAct Created.. " +
				"local channel: " + selected_channel.getCamProfile().isInLocal() +
				" ip: "+selected_channel.getCamProfile().get_inetAddress() +
				" mac:" + selected_channel.getCamProfile().get_MAC()
				);

		access_mode = SetupData.ACCESS_VIA_LAN;

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) 
		{
			video_background = BitmapFactory.decodeResource(
					this.getResources(), R.drawable.homepage);
		}
		else 
		{
			video_background = BitmapFactory.decodeResource(
					this.getResources(), R.drawable.homepage_p);
		}

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		video_background = Bitmap.createScaledBitmap(video_background, wm
				.getDefaultDisplay().getWidth(), wm.getDefaultDisplay()
				.getHeight(), false);



		/* force video to show at nozoom whenever we resume */
		currentZoomInLevel = DEFAUL_ZOOM_LEVEL;



		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(
				PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.ON_AFTER_RELEASE,
				"TURN ON for first time connect");
		wl.setReferenceCounted(false);
		wl.acquire();

		/*
		 * 20130327: hoang: issue 1531: Disable recurring services when viewing camera
		 * Check alert for camera. If there are alerts for this camera,
		 * clear all alerts and stop recurring service. Otherwise, do nothing.
		 */
		int shouldDisableRecurring = AlertData.getAlertForCamera(selected_channel.getCamProfile().get_MAC(),
											getExternalFilesDir(null));
		if (shouldDisableRecurring != 0)
		{
			Log.d("mbp", "Clear all alerts for camera: "+
					selected_channel.getCamProfile().get_MAC());
			AlertData.clearAlertForCamera(
					selected_channel.getCamProfile().get_MAC(),
					getExternalFilesDir(null));
			
			if (MBP2K_NotifyService.isServiceRunning(this))
			{
				Log.d("mbp", "Stop recurring sound...");
				Intent recurringSerice = new Intent(this, MBP2K_NotifyService.class);
				stopService(recurringSerice);
			}
		}
		
		/*
		 * 20130529: hoang:
		 * Start FFMpeg BroadcastReceiver
		 */
		startFFMpegBroadcastReceiver();

	}

	/**
	 * 
	 */
	private void startFFMpegBroadcastReceiver()
	{
		if (ffmpeg_br == null)
		{
			ffmpeg_br = new BroadcastReceiver() {

				@Override
				public void onReceive(Context context, Intent intent) {
					if (intent.getAction().equals(
							FFMpegPlayerActivity.ACTION_FFMPEG_PLAYER_STOPPED))
					{
						/*
						 * send close relay session to server
						 */
						SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
						SharedPreferences.Editor editor = settings.edit();
						String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
						int commMode = selected_channel.getCamProfile().getRemoteCommMode();
						if (commMode == StreamerFactory.STREAM_MODE_UDT)
						{
							Log.d("mbp", "Send close_relay_session2 cmd...");
							String request = "action=" + PublicDefine.CLOSE_RELAY_SESSION2;
							UDTRequestSendRecv.sendRequest_via_stun2(
									saved_token,
									((BabyMonitorRelayAuthentication)bm_session_auth).getRestrationId(),
									request);
						}
						else if (commMode == StreamerFactory.STREAM_MODE_RELAY)
						{
//							Log.d("mbp", "Send close upnp session...");
//							int retry = 3;
//							do
//							{
//								try {
//									CloseSessionResponse close_res = Device.closeSession(
//											saved_token,
//											((BabyMonitorRelayAuthentication)bm_session_auth).getDeviceMac(),
//											((BabyMonitorRelayAuthentication)bm_session_auth).getChannelID() );
//									if (close_res != null && close_res.isSucceed())
//									{
//										break;
//									}
//									else
//									{
//										Log.d("mbp", "Retry to close session: #" + retry);
//									}
//										
//								} catch (SocketTimeoutException e) {
//									e.printStackTrace();
//									Log.d("mbp", "Retry to close session: #" + retry);
//								} catch (MalformedURLException e) {
//									e.printStackTrace();
//									Log.d("mbp", "Retry to close session: #" + retry);
//								} catch (IOException e) {
//									e.printStackTrace();
//									Log.d("mbp", "Retry to close session: #" + retry);
//								}
//							} while (retry-- > 0);
						}

						finish();
					}
				}
			};

			IntentFilter iFilter = new IntentFilter();
			iFilter.addAction(FFMpegPlayerActivity.ACTION_FFMPEG_PLAYER_STOPPED);
			registerReceiver(ffmpeg_br,	iFilter);
		}
	}
	
	private void stopFFMpegBroadcastReceiver()
	{
		if (ffmpeg_br != null)
		{
			unregisterReceiver(ffmpeg_br);
			ffmpeg_br = null;
		}
	}

	public void onStart()
	{
		super.onStart();
		tracker.sendView("View Screen");
		Log.d("mbp", "ViewCameraAct onStart..");

		if (selected_channel == null)
		{
			//Can't CONTINUE any more has to go back..
			Log.d("mbp", " selected channel is NULL");
			finish(); 
		}
		
		synchronized (this) {
			reason_to_leave = REASON_UNKNOWN;
		}

		/* Get current connection mode */
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		currentConnectionMode = settings.getInt(
				PublicDefine.PREFS_CURRENT_CAMERA_MODE, -1);
		
		switch (currentConnectionMode) {

		case CONNECTION_MODE_LOCAL_INFRA:

			if (streamer_thrd != null && streamer_thrd.isAlive())
			{
				/* 20130402: hoang: refresh HQ icon */
				refreshHqIcon();
				
				Log.d("mbp", "ADD BACK Video screen & temperature update");
				_streamer.addVideoSink(ViewCameraActivity.this);
				_streamer.setTemperatureUpdater(ViewCameraActivity.this);
			}
			if (avsCtl != null)
			{
				avsCtl.setEnableStatusBarIcon(false, selected_channel.getCamProfile().getName());
			}


			break;

		case CONNECTION_MODE_REMOTE:

			


			if (selected_channel != null)
			{

				if (_streamer!= null && !_streamer.isStreaming())
				{
					Log.d("mbp",
							"Streamer is not Streaming -- restart it");

					/* 20130201: hoang: issue 1260 turn on screen while
					 * preparing to view remotely
					 */	
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP
							| PowerManager.ON_AFTER_RELEASE,
							"TURN ON Because of error");
					wl.setReferenceCounted(false);
					wl.acquire();
					Log.d("mbp",
							"Acquire WakeLock for prepare to view remotely");

					prepareToViewCameraRemotely(true);
				}
				else if (streamer_thrd != null && streamer_thrd.isAlive())
				{
					/* 20130402: hoang: updateHQicon */
					refreshHqIcon();
					
					Log.d("mbp", "ADD BACK Video screen && temperature update");
					_streamer.addVideoSink(ViewCameraActivity.this);
					_streamer.setTemperatureUpdater(ViewCameraActivity.this);
				}
				


			}






			break;


		default:// First time launch
			
			setContentView(R.layout.bb_is_waiting_screen);

			if (selected_channel.getCamProfile().isInLocal()) 
			{ // local

				Log.d("mbp", "ViewCameraAct setupInfraCamera..");
				setupInfraCamera(selected_channel);
			} 
			else
			{
				Log.d("mbp", "ViewCameraAct prepareToViewCameraRemotely..");
				prepareToViewCameraRemotely(false);

			}

			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQUEST_START_CAMERA_MENU) 
		{


			/*
			 * Signal that remove camera has be done inside menu - switch back
			 * to camera list rather than stay on this camera (it was removed )
			 */
			if (resultCode == CameraMenuActivity.CAMERA_REMOVED) 
			{

				cancelVideoStoppedReminder();

				setResult(DashBoardActivity.RESCAN_CAMERA);
				finish();
			}
			else if (resultCode == CameraMenuActivity.CAMERA_INFO_UPDATED) 
			{
				String newName = data.getStringExtra(CameraMenuActivity.str_deviceName_out);
				selected_channel.getCamProfile().setName(newName);

			} 
			else if (resultCode == CameraMenuActivity.CLOSE_APP_AND_EXIT)
			{
				///Obsolete -- 
			}
			
			
			unregisterReceiver(cameraSettingClosed);
		}
		
		
	}
	
	

	public void onStop()
	{
		super.onStop();

		Log.d("mbp", "ViewCameraAct onStop..");

		/*
		 * 20130603: hoang: release the native heap
		 */
		if (video_background != null)
		{
			video_background.recycle();
			video_background = null;
		}

		if (currentConnectionMode == CONNECTION_MODE_LOCAL_INFRA)
		{

			if (streamer_thrd != null && streamer_thrd.isAlive())
			{
				Log.d("mbp", "remove Video screen && temperature update");
				_streamer.removeVideoSink(ViewCameraActivity.this);
				_streamer.setTemperatureUpdater(null);
			}

			if (avsCtl != null)
			{
				avsCtl.setEnableStatusBarIcon(true, selected_channel.getCamProfile().getName());
			}
		}
		else if (currentConnectionMode == CONNECTION_MODE_REMOTE)
		{
			
			if (reason_to_leave == REASON_LAUNCH_OTHER_ACTIVITY) 
			{
				//Launching settings -- Dont stop streaming.. 
				if (streamer_thrd != null && streamer_thrd.isAlive())
				{
					Log.d("mbp", "remove Video screen && temperature update");
					_streamer.removeVideoSink(ViewCameraActivity.this);
					_streamer.setTemperatureUpdater(null);
				}
				
			} 
			else
			{
				stopAllThread(); 
				stopAVSService();
				// Call 2nd times
				cancelVideoStoppedReminder();

				ACTIVITY_HAS_STOPPED = true;
				
				
				// Stop Play tone if still running.
				if (_playTone != null && _playTone.isAlive()) {
					Log.d("mbp","stop play Tone thread now");
					playTone.stopPlaying();
					_playTone.interrupt();
					_playTone = null;
				}		
			}
			

		}
		

		/* Save current connection mode */
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		editor.putInt(PublicDefine.PREFS_CURRENT_CAMERA_MODE,
				currentConnectionMode);
		editor.commit();


	}

	public void onDestroy()
	{
		super.onDestroy();
		
		Log.d("mbp", "ViewCameraAct onDestroy..");

		stopAllThread(); 
		//remove_ScreenTimeout_br(); 
		stopAVSService();

		
		ACTIVITY_HAS_STOPPED = true; 
		
		// Stop Play tone if still running.
		if (_playTone != null && _playTone.isAlive()) {
			Log.d("mbp","stop play Tone thread now");
			playTone.stopPlaying();
			_playTone.interrupt();
			_playTone = null;
		}		

		




		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		/* clear usr/pass field to force re-auth */
		editor.putString(PublicDefine.PREFS_TRANSIENT_USERNAME, null);
		editor.putString(PublicDefine.PREFS_TRANSIENT_PASSWD, null);
		editor.putInt(PublicDefine.PREFS_CURRENT_CAMERA_MODE, -1);

		editor.putString(PublicDefine.PREFS_CAM_BEING_VIEWED, null);
		editor.putBoolean(PublicDefine.PREFS_LOCAL_VIEW_MUTED, false);
		// Commit the edits!
		editor.commit();

		if (wl != null && wl.isHeld()) {
			wl.release();
			wl = null;
			Log.d("mbp", "ViewCameraAct onDestroy() - release WakeLock");
		}

		if (remoteVideoTimer != null) {
			Log.d("mbp","stop remoteVideoTimer .. since video stopped ");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}
		
		stopFFMpegBroadcastReceiver();
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		View v;
		v = findViewById(R.id.imageVideo);
		if (v != null && v.isShown()) {
			// showing video
			viewCameraLayout();
		}

	}
	
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP1k)) {

			// MBP1K only
			if (keyCode == KeyEvent.KEYCODE_CALL) {

				if (selected_channel != null && videoIsShowing) {
					if (talkback_enabled == true) {
						setTalkBackEnabled(false);
					}
				}
				return true;
			}
		}
		return false;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		String phonemodel = android.os.Build.MODEL;
		if (phonemodel.equals(PublicDefine.PHONE_MBP1k)) {
			// MBP1K only
			if (keyCode == KeyEvent.KEYCODE_CALL) {

				if (selected_channel != null && videoIsShowing) {
					if (talkback_enabled == false) {
						setTalkBackEnabled(true);
					}
					return true;
				}
			}
			

		}
		
		
		if ((keyCode == KeyEvent.KEYCODE_BACK)) 
		{
				onBackPressed();
		}


		return super.onKeyDown(keyCode, event);
	}

	private void showGetPortErrDialog(Message getPortmsg) {
		AlertDialog.Builder builder;
		Spanned msg;
		Dialog alert = null;

		switch (getPortmsg.what) {
		case GetRemoteCamPortTask.MSG_GET_PORT_TASK_FALIED_REPONSE_ERR:
			String err_str = (String) getPortmsg.obj;

			if (err_str == null) {
				err_str = "Invalid Response";
			}

			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.can_t_start_video_stream_invalid_server_response_)
							+ err_str + getString(R.string._please_try_again)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					setResult(DashBoardActivity.RESCAN_CAMERA); 
					finish();


				}
			});

			alert = builder.create();
			break;
		case GetRemoteCamPortTask.MSG_GET_PORT_TASK_FALIED_SERVER_UNREACHABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.can_t_start_video_stream_failed_to_connect_to_bms_please_try_again)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					setResult(DashBoardActivity.RESCAN_CAMERA); 
					finish();

				}
			});

			alert = builder.create();
			break;
		default:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_7) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					setResult(DashBoardActivity.RESCAN_CAMERA); 
					finish();

				}
			});

			alert = builder.create();
			break;
		}

		if (alert != null) {
			alert.show();
		} else {
			Log.d("mbp", "showGetPortalert: alert = null");
		}
		return;

	}

	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		ProgressDialog dialog;
		Spanned msg;
		switch (id) {
		case DIALOG_CAMERA_PORT_IS_INACCESSIBLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources().getString(
									R.string.camera_port_is_inaccessible )
									+ " (612)"+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();

				}
			}).setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {

				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_CONNECTION_FAILED:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources().getString(
									R.string.EntryActivity_conn_failed_wifi)
									+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();
				}
			}).setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					record_enabled = false;
					snapshot_enabled = false;
				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_BMS_CONNECTION_IN_PROGRESS:
			dialog = new ProgressDialog(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources().getString(
									R.string.EntryActivity_connecting_to_bm)
									+ "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {

					stopAllThread();

					cancelVideoStoppedReminder();

					// release WakeLock if it's held
					if (wl != null && wl.isHeld()) {
						wl.release();
						wl = null;
						Log.d("mbp", "release WakeLock");
					}

					setResult(DashBoardActivity.RESCAN_CAMERA); 
					finish();
				}
			});

			dialog.setButton(getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			return dialog;

		case DIALOG_REMOTE_BM_IS_OFFLINE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources().getString(
									R.string.EntryActivity_cant_reach_cam_2)
									+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();

				}
			}).setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {

				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_VIDEO_STOPPED_UNEXPECTEDLY:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_1) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setNegativeButton(
					getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();


							try {
								dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
							} catch (Exception ie) {

							}
							stopAllThread();
							cancelVideoStoppedReminder();

							setResult(DashBoardActivity.RESCAN_CAMERA);
							finish(); 
						}
					});

			alert = builder.create();
			return alert;

		case DIALOG_WIFI_CANT_RECONNECT:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_2) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();
					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();
				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_REMOTE_VIDEO_STREAM_TIMEOUT:

			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_8) + "</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.Yes),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					if (selected_channel != null) 
					{
						// cancel current remote connection
						selected_channel.cancelRemoteConnection();
					}
					// RESTART new session

					/*
					 * 20130201: hoang: issue 1260 turn on
					 * screen while preparing to view remotely
					 */
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					wl = pm.newWakeLock(
							PowerManager.FULL_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP
							| PowerManager.ON_AFTER_RELEASE,
							"TURN ON Because of error");
					wl.setReferenceCounted(false);
					wl.acquire();
					Log.d("mbp",
							"Acquire WakeLock for prepare to view remotely");

					prepareToViewCameraRemotely(true);
					// Play tone
					if (_playTone != null
							&& _playTone.isAlive()) {
						Log.d("mbp",
								"stop play Tone thread now");
						playTone.stopPlaying();
						_playTone.interrupt();
						_playTone = null;
					}
				}
			})
			.setNegativeButton(getResources().getString(R.string.No),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					// GO back to camera selection screens
					/*
					 * Don't rescan, just build base on the list
					 * we have
					 */
					stopAllThread();

					// Play tone
					if (_playTone != null
							&& _playTone.isAlive()) {
						Log.d("mbp",
								"stop play Tone thread now");
						playTone.stopPlaying();
						_playTone.interrupt();
						_playTone = null;
					}

					// cancel current remote connection
					selected_channel.cancelRemoteConnection();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_REMOTE_VIDEO_STREAM_STOPPED_UNEXPECTEDLY:

			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_1) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setNegativeButton(
					getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();
							try {
								dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
							} catch (Exception e) {
							}

							try {
								dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
							} catch (Exception e) {
							}

							// Stop popup thread
							if (_playTone != null
									&& _playTone.isAlive()) {
								Log.d("mbp", "Stop popup thread  now");
								playTone.stopPlaying();
								_playTone.interrupt();
								_playTone = null;
							}

							// cautious..
							if (remoteVideoTimer != null) {
								Log.d("mbp",
										"stop remoteVideoTimer .. since video stopped ");
								remoteVideoTimer.cancel();
								remoteVideoTimer = null;
							}

							selected_channel.cancelRemoteConnection();

							stopAllThread();
							cancelVideoStoppedReminder();


							setResult(DashBoardActivity.RESCAN_CAMERA);
							finish();

						}
					});

			alert = builder.create();
			return alert;

		case DIALOG_REMOTE_BM_IS_BUSY:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_5) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_BMS_GET_PORT_ERROR:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_7) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_NEED_TO_LOGIN:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources()
					.getString(R.string.EntryActivity_not_login)
					+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;


		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();


					Intent homeScreen = new Intent(ViewCameraActivity.this,FirstTimeActivity.class);
					homeScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeScreen);
					ViewCameraActivity.this.finish();

				}
			});

			alert = builder.create();
			return alert;


		case DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_SNAPSHOT:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.there_is_not_enough_space_to_store_the_snapshot_please_remove_some_files_and_try_again_)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_VIDEO:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.application_needs_free_storage_space_of_at_least_100mb_to_start_recording_)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_BMS_GET_STREAM_MODE_ERROR:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.EntryActivity_no_signal_10) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish();

				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_FAILED_TO_UNMUTE_CAM_AUDIO:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.cannot_communicate_with_the_camera_to_de_activate_talk_back_due_to_network_issue_)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(
					getResources().getString(R.string.retry),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							dialog.dismiss();

							Runnable resend = new Runnable() {
								@Override
								public void run() {

									/* send audio_out0 cmd */
									HttpURLConnection conn;
									int responseCode = -1;
									String usr_pass = String
											.format("%s:%s",
													PublicDefine.DEFAULT_BASIC_AUTH_USR,
													http_pass);

									try {
										URL url = new URL(
												"http://"
														+ device_ip
														+ ":"
														+ device_port
														+ PublicDefine.HTTP_CMD_PART
														+ PublicDefine.SET_DEVICE_AUDIO_ON);

										conn = (HttpURLConnection) url
												.openConnection();
										conn.setConnectTimeout(5000);
										conn.setReadTimeout(5000);
										conn.addRequestProperty(
												"Authorization",
												"Basic "
														+ Base64.encodeToString(
																usr_pass.getBytes("UTF-8"),
																Base64.NO_WRAP));
										conn.connect();
										conn.getContentType();
										responseCode = conn
												.getResponseCode();
										Log.d("mbp", "responseCode:"
												+ responseCode);
									} catch (SocketTimeoutException ste) {
										// Timeout ;
										Log.d("mbp",
												"Socket Timeout -- send error now ");

										runOnUiThread(new Runnable() {

											@Override
											public void run() {
												showDialog(DIALOG_FAILED_TO_UNMUTE_CAM_AUDIO);
											}
										});
									} catch (IOException e) {
										e.printStackTrace();
									}

								}
							};
							runOnUiThread(resend);

						}
					})
					.setNegativeButton(
							getResources().getString(R.string.ignore),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							});

			alert = builder.create();
			return alert;
		case DIALOG_UDT_RELAY_CONNECTION_IN_PROG:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.connecting_through_relay_please_wait_)
							+ "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;
		case DIALOG_FW_PATCH_FOUND:
			builder = new AlertDialog.Builder(this);
			String device_version = device.device_version;
			msg = Html.fromHtml("<big>"
					+ getString(R.string.a_camera_firmware_upgrade_) + " "
					+ device_version + " " + getString(R.string.is_available)
					+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.Yes),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					if (selected_channel != null) {
						// stop all before upgrade
						stopAllThread();
						cancelVideoStoppedReminder();
						//remove_ScreenTimeout_br();
						
						// this task is to display the %
						CheckVersionFW test = new CheckVersionFW(
								ViewCameraActivity.this,
								new Handler(ViewCameraActivity.this),
								true, device.device_version,
								selected_channel.getCamProfile().get_MAC());
						
						test.execute(
								device.device_ip,
								String.valueOf(device_port),
								PublicDefine.HTTP_CMD_PART,
								CheckVersionFW.REQUEST_FW_UPGRADE);

					}
				}
			})
			.setNegativeButton(getResources().getString(R.string.No),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.update_status_failed_please_try_again_)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});

			alert = builder.create();

			return alert;

		case DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>" + getString(R.string.DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW) + "</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
				}
			});
			alert = builder.create();
			return alert;
		case DIALOG_SESSION_KEY_MISMATCHED:

			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"
					+ getResources().getString(
							R.string.the_session_key_on_camera_is_mismatched)
							+ "</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					// GO back to camera selection screens
					/*
					 * Don't rescan, just build base on the list
					 * we have
					 */
					stopAllThread();

					// Play tone
					if (_playTone != null
							&& _playTone.isAlive()) {
						Log.d("mbp",
								"stop play Tone thread now");
						playTone.stopPlaying();
						_playTone.interrupt();
						_playTone = null;
					}

					selected_channel.cancelRemoteConnection();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish(); 
				}
			});

			alert = builder.create();
			return alert;

		case DIALOG_CAMERA_IS_NOT_AVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources()
							.getString(
									R.string.camera_is_not_available_please_make_sure_that_it_is_turned_on)
									+ "</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.dismiss();
					// GO back to camera selection screens
					stopAllThread();

					// Play tone
					if (_playTone != null
							&& _playTone.isAlive()) {
						Log.d("mbp",
								"stop play Tone thread now");
						playTone.stopPlaying();
						_playTone.interrupt();
						_playTone = null;
					}

					selected_channel.cancelRemoteConnection();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish(); 


				}
			});

			alert = builder.create();
			return alert;


		default:
			break;
		}
		return null;
	}


	private void showFileDialogMessage(String _msg) {
		Spanned msg = Html.fromHtml(_msg);
		AlertDialog.Builder builder = new AlertDialog.Builder(
				this);
		builder.setMessage(msg)
		.setCancelable(true)
		.setPositiveButton(getResources().getString(R.string.OK),
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,
					int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();

	}

	private void videoHasStoppedUnexpectedly() {

		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {

				stopAllThread();
				// Decide whether Router disconnects or Camera disconnect

				WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);

				SharedPreferences settings = getSharedPreferences(
						PublicDefine.PREFS_NAME, 0);
				String ssid_no_quote = settings.getString(string_currentSSID,
						null);

				if (wm.getConnectionInfo() != null
						&& wm.getConnectionInfo().getSSID() != null
						&& wm.getConnectionInfo().getSSID()
						.equalsIgnoreCase(ssid_no_quote)) 
				{
					// Still on the same network --> camera down
					Log.d("mbp","Wifi SSID is still the same, camera is probably down ");


					// re-scann
					scan_task = new LocalScanForCameras(ViewCameraActivity.this,
							new OneCameraScanner());
					scan_task.setShouldGetSnapshot(false);
					// Callback: updateScanResult()
					// setup scanning for just 1 camera -
					Log.d("mbp", "setup scanning for just 1 camera - ");
					scan_task.startScan(new CamProfile[] { selected_channel.getCamProfile() });


				}
				else // Router down
				{
					shouldBeepAndShow = 2;
					Log.d("mbp",
							"Wifi SSID is not the same, Router is probably down ");
					MiniWifiScanUpdater iw = new MiniWifiScanUpdater();
					WifiScan ws = new WifiScan(ViewCameraActivity.this, iw);
					ws.setSilence(true);
					ws.execute("Scan now");
				}

				shouldBeepAndShow++;
				if (shouldBeepAndShow >= 2) {

					if (outOfRange == null || !_outOfRange.isAlive()) {
						Log.d("mbp", "start Reminder now!");
						outOfRange = new VideoOutOfRangeReminder();
						_outOfRange = new Thread(outOfRange, "outOfRange");
						_outOfRange.start();
					} else {
						Log.d("mbp",
								"reminder is running... dont start another one");
					}

					displayBG(true);

				} else {
					Log.d("mbp", "skip  Reminder first time!");
				}

			}
		});

	}
	private void cancelVideoStoppedReminder() {
		boolean retry = true;
		if (_outOfRange != null && _outOfRange.isAlive()) {
			Log.d("mbp", "stop alarm tone thread now");
			outOfRange.stop();
			/* try to interrupt with this thread is sleeping */
			_outOfRange.interrupt();
			while (retry) {
				try {
					_outOfRange.join(5000);

					retry = false;
				} catch (InterruptedException e) {
				}
			}
			_outOfRange = null;
			outOfRange = null;
		}
	}


	//	private void remove_ScreenTimeout_br()
	//	{
	//		if (screen_br != null) 
	//		{
	//			try {
	//				unregisterReceiver(screen_br);
	//			} 
	//			catch (IllegalArgumentException ile) 
	//			{
	//			}
	//			screen_br = null;
	//
	//		}
	//	}
	//
	//	private void setup_ScreenTimeout_br()
	//	{
	//		remove_ScreenTimeout_br();
	//
	//		screen_br = new ScreenTOBroadcastReceiver();
	//
	//		IntentFilter screenTOfilter = new IntentFilter();
	//		screenTOfilter.addAction(Intent.ACTION_SCREEN_OFF);
	//		screenTOfilter.addAction(Intent.ACTION_SCREEN_ON);
	//		registerReceiver(screen_br, screenTOfilter);
	//
	//	}

	private void exitSubfunction() {
		// while recording, if user exit subfunction automatically stop
		// recording
		if (_recordInProgress) {
			onRecord(null);// toggle recording
		}

		showSideMenusAndStatus();

		RelativeLayout direction = (RelativeLayout) findViewById(R.id.directionLayout);
		if (direction != null) {
			direction.setVisibility(View.VISIBLE);
		}

		RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
		if (pttLayout != null) {
			pttLayout.setVisibility(View.INVISIBLE);
		}

		RelativeLayout recMenu = (RelativeLayout) findViewById(R.id.rec_menu);
		if (recMenu != null) {
			recMenu.setVisibility(View.GONE);
		}

		RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
		if (refreshRoot != null) {
			refreshRoot.setVisibility(View.INVISIBLE);
		}

	}



	/***
	 * Split UI and Video Viewing
	 */
	/* keep them on */
	private void showSideMenusAndStatus() {
		/* cancel any timeout running */
		cancelFullscreenTimer();

		// Show status bar
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		VerticalSeekBar zoomBar = (VerticalSeekBar) findViewById(R.id.zoomBar);
		if (zoomBar != null) {
			zoomBar.clearAnimation();
			zoomBar.setVisibility(View.VISIBLE);
		}

		RelativeLayout leftSideMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
		if (leftSideMenu != null) {
			leftSideMenu.clearAnimation();
			leftSideMenu.setVisibility(View.VISIBLE);
		}

		ImageView plusIcon = (ImageView) findViewById(R.id.plusImage);
		if (plusIcon != null) {
			plusIcon.clearAnimation();
			plusIcon.setVisibility(View.VISIBLE);
		}

		ImageView minusIcon = (ImageView) findViewById(R.id.minusImage);
		if (minusIcon != null) {
			minusIcon.clearAnimation();
			minusIcon.setVisibility(View.VISIBLE);
		}

		// also show the joystick
		ImageView direction_pad = (ImageView) findViewById(R.id.directionPad);
		// ImageView direction_indicator = (ImageView)
		// findViewById(R.id.directionInd);
		if (direction_pad != null
				/* && direction_indicator != null */) {
			// direction_indicator.clearAnimation();
			direction_pad.clearAnimation();
			direction_pad.setVisibility(View.VISIBLE);
			// direction_indicator.setVisibility(View.VISIBLE);
		}

	}

	private void showJoystickOnly() {
		// show the joystick
		ImageView direction_pad = (ImageView) findViewById(R.id.directionPad);
		// ImageView direction_indicator = (ImageView)
		// findViewById(R.id.directionInd);
		if (direction_pad != null
				/* && direction_indicator != null */) {
			// direction_indicator.clearAnimation();
			direction_pad.clearAnimation();
			direction_pad.setVisibility(View.VISIBLE);
			// direction_indicator.setVisibility(View.VISIBLE);
		}

		// Fade all others -if they r shown
		RelativeLayout leftSideMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
		if (leftSideMenu != null && leftSideMenu.isShown()) {
			fade_out_view(leftSideMenu, 1000);
		}

		VerticalSeekBar zoomBar = (VerticalSeekBar) findViewById(R.id.zoomBar);
		if (zoomBar != null && zoomBar.isShown()) {
			fade_out_view(zoomBar, 1000);
		}

		ImageView plusIcon = (ImageView) findViewById(R.id.plusImage);
		if (plusIcon != null && plusIcon.isShown()) {
			fade_out_view(plusIcon, 1000);
		}

		ImageView minusIcon = (ImageView) findViewById(R.id.minusImage);
		if (minusIcon != null && minusIcon.isShown()) {
			fade_out_view(minusIcon, 1000);
		}

	}

	private void cancelFullscreenTimer() {

		/* cancel any timeout running */
		if (timeOut != null) {
			timeOut.setCancel(true);
			_timeOut.interrupt();
			try {
				_timeOut.join(1000);
			} catch (InterruptedException e) {
			}
			timeOut = null;
		}
	}

	private void tryToGoToFullScreen() {

		cancelFullscreenTimer();

		/* Start a 10sec remoteVideoTimer and made menus disappear */
		timeOut = new ScreenTimeOutRunnable(this, new Runnable() {

			@Override
			public void run() {

				goToFullScreen();
			}
		});
		_timeOut = new Thread(timeOut, "Screen Timeout");
		_timeOut.start();

	}

	private void goToFullScreen() {

		isFullScreen = true;

		// also show the joystick
		ImageView direction_pad = (ImageView) findViewById(R.id.directionPad);
		// ImageView direction_indicator = (ImageView)
		// findViewById(R.id.directionInd);
		if (direction_pad != null
				/* && direction_indicator != null */) {
			fade_out_view(direction_pad, 1000);
			// fade_out_view(direction_indicator, 1000);
		}

		RelativeLayout leftSideMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
		if (leftSideMenu != null && leftSideMenu.isShown()) {
			fade_out_view(leftSideMenu, 1000);
		}

		VerticalSeekBar zoomBar = (VerticalSeekBar) findViewById(R.id.zoomBar);
		if (zoomBar != null && zoomBar.isShown()) {
			fade_out_view(zoomBar, 1000);
		}

		ImageView plusIcon = (ImageView) findViewById(R.id.plusImage);
		if (plusIcon != null && plusIcon.isShown()) {
			fade_out_view(plusIcon, 1000);
		}

		ImageView minusIcon = (ImageView) findViewById(R.id.minusImage);
		if (minusIcon != null && minusIcon.isShown()) {
			fade_out_view(minusIcon, 1000);
		}

		// Hide status bar
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// exit subfunctions as well.
		//backKeyToGoBack = false;

		RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
		if (pttLayout != null && pttLayout.isShown()) {
			fade_out_view(pttLayout, 1000);
		}

		RelativeLayout recMenu = (RelativeLayout) findViewById(R.id.rec_menu);
		if (recMenu != null && recMenu.isShown()) {
			fade_out_view(recMenu, 1000);
		}

		RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
		if (refreshRoot != null && refreshRoot.isShown()) {
			fade_out_view(refreshRoot, 1000);
		}
	}

	private void fade_out_view(View v, int duration_ms) {
		Animation myFadeAnimation = AnimationUtils.loadAnimation(this,
				R.anim.fadeout);
		myFadeAnimation.setDuration(duration_ms);
		myFadeAnimation
		.setAnimationListener(new FadeOutAnimationAndGoneListener(v));
		v.startAnimation(myFadeAnimation);
	}


	/* also used by VideoStreamer */
	public void displayBG(boolean shouldDisplay) {
		Canvas c = null;
		SurfaceHolder _surfaceHolder = _vImageView.get_SurfaceHolder();
		if (_surfaceHolder == null) {
			Log.w("mbp", "_surfaceHolder is NULL");
			return;
		}

		try {
			c = _surfaceHolder.lockCanvas(null);
			if (c == null) {
				return;
			}
			/*
			 * video_background = Bitmap.createScaledBitmap(video_background,
			 * Math.min(c.getWidth(),video_background.getWidth()),
			 * Math.min(c.getHeight(),video_background.getHeight()),false);
			 */
			
			/*
			 * 20130603: hoang: release the native heap
			 */
			if (video_background != null)
			{
				video_background.recycle();
				video_background = null;
			}

			
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				video_background = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.homepage);
			} else {
				video_background = BitmapFactory.decodeResource(
						this.getResources(), R.drawable.homepage_p);
			}
			video_background = Bitmap.createScaledBitmap(video_background,
					c.getWidth(), c.getHeight(), false);

			synchronized (_surfaceHolder) {
				// c.drawBitmap(background,0,0,null);

				if (shouldDisplay) {
					c.drawBitmap(video_background, 0, 0, null);
				} else {
					c.drawColor(Color.BLACK);
				}
			}
		} finally {
			// do this in a finally so that if an exception is thrown
			// during the above, we don't leave the Surface in an
			// inconsistent state
			if (c != null) {
				_surfaceHolder.unlockCanvasAndPost(c);
			}
		}
	}

	private void setupVideoThreads(final CamChannel s_channel) {
		Log.d("mbp", "Thread:" + Thread.currentThread().getId()
				+ ": setupVideoThread for:" + s_channel);

		synchronized (ViewCameraActivity.this) {
			device_ip = s_channel.getCamProfile().get_inetAddress()
					.getHostAddress();
			device_port = s_channel.getCamProfile().get_port();
			try {
				http_pass = CameraPassword.getPasswordforCam(
						getExternalFilesDir(null), s_channel.getCamProfile()
						.get_MAC());
			} catch (StorageException e) {
				Log.d("mbp", e.getLocalizedMessage());
				showDialog(DIALOG_STORAGE_UNAVAILABLE);
				return;
			}

			device_audio_in_port = PublicDefine.DEFAULT_AUDIO_PORT;
			
			CheckVersionFW test = new CheckVersionFW(this, new Handler(this),
					false, null, s_channel.getCamProfile().get_MAC());
			test.execute(device_ip, String.valueOf(device_port),
					PublicDefine.HTTP_CMD_PART, CheckVersionFW.CHECK_FW_UPGRADE);

			_streamer = StreamerFactory.getStreamer(s_channel.getCamProfile(),
					new Handler(this), ViewCameraActivity.this, device_ip, device_port);

			if (http_pass != null) {
				_streamer.setHTTPCredential(
						PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);
			}

			_streamer.enableAudio(_enableAudio);
			_streamer.addVideoSink(this);
			_streamer.setMelodyUpdater(this);
			_streamer.setResUpdater(this);
			_streamer.setTemperatureUpdater(this);
			_streamer.restart();
			try {
				showDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {

			}
			streamer_thrd = new Thread(_streamer, "VAStreamer");
			streamer_thrd.start();


			if (use_pcmPlayer)
			{
				_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO);
				pcmPlayer_thrd = new Thread(_pcmPlayer, "PCMPlayer");
				pcmPlayer_thrd.start();
			}
			else
			{
				pcmPlayer_thrd= null; 
				_pcmPlayer = null; 
			}



			/* setup connection to send direction */
			device_comm = new DirectionDispatcher("http://" + device_ip + ":"
					+ device_port);
			device_comm_thrd = new Thread(device_comm, "DirectionHTTP");
			device_comm_thrd.start();

			/* 20120626: update joystickListener */
			if (joystickListener != null) 
			{
				joystickListener.setDirectionDispatcher(device_comm);
			}

		}

		///USE SERVICE .. 
		Log.d("mbp","[LOCAL] Turning on services...");
		startAVSService();



	}
	private void onSwitchToRecordMenu(View v) {
		showJoystickOnly();
		RelativeLayout direction = (RelativeLayout) findViewById(R.id.directionLayout);
		if (direction != null) {
			direction.setVisibility(View.INVISIBLE);
		}

		final RelativeLayout recMenu = (RelativeLayout) findViewById(R.id.rec_menu);
		if (recMenu != null) {
			recMenu.setVisibility(View.VISIBLE);
		}


		// Override the control joystick with the joystick on this menu
		ImageView direction_pad = (ImageView) recMenu
				.findViewById(R.id.directionPad);
		// ImageView direction_indicator = (ImageView) recMenu
		// .findViewById(R.id.directionInd);
		DirectionTouchListener_bb _joystickListener = new DirectionTouchListener_bb(this, 
				new Handler(new Handler.Callback() {
					@Override
					public boolean handleMessage(Message msg) {
						return false;
					}

				}), direction_pad, null, device_comm);

		direction_pad.setOnTouchListener(_joystickListener);

		final ImageButton snapShot = (ImageButton) recMenu
				.findViewById(R.id.imageSnap);
		final ImageButton videoRec = (ImageButton) recMenu
				.findViewById(R.id.imageRec);

		final ImageButton engageBtn = (ImageButton) recMenu
				.findViewById(R.id.btnImg);

		snapShot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// during videorecording should not allow camera snapshot
				if (_recordInProgress) {
					showDialog(DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW);
					return;
				}

				snapShot.setImageResource(R.drawable.bb_snap_icon);
				videoRec.setImageResource(R.drawable.bb_rec_icon_d);
				engageBtn.setImageResource(R.drawable.bb_snap_btn);

				// setup the switch b/w snapshot & recording
				if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
					VerticalSeekBar sb = (VerticalSeekBar) recMenu
							.findViewById(R.id.seekBar1);
					sb.setProgress(0);
				} else {
					SeekBar sb = (SeekBar) recMenu.findViewById(R.id.seekBar1);
					sb.setProgress(0);
				}

			}

		});

		videoRec.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				snapShot.setImageResource(R.drawable.bb_snap_icon_d);
				videoRec.setImageResource(R.drawable.bb_rec_icon);

				// check if recording is still in process
				if (_recordInProgress) {
					engageBtn.setImageResource(R.drawable.bb_rec_stop_btn);
				} else {
					engageBtn.setImageResource(R.drawable.bb_rec_start_btn);
				}

				// setup the switch b/w snapshot & recording
				if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
					VerticalSeekBar sb = (VerticalSeekBar) recMenu
							.findViewById(R.id.seekBar1);
					sb.setProgress(1);
				} else {
					SeekBar sb = (SeekBar) recMenu.findViewById(R.id.seekBar1);
					sb.setProgress(1);
				}
			}
		});

		engageBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				int value = 0;
				if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
					VerticalSeekBar sb = (VerticalSeekBar) recMenu
							.findViewById(R.id.seekBar1);
					value = sb.getProgress();
				} else {
					SeekBar sb = (SeekBar) recMenu.findViewById(R.id.seekBar1);
					value = sb.getProgress();
				}

				if (value == 0) 
				{
					onSnaptshot();
				} 
				else 
				{
					// for record : if user start recording.. leave it .. until
					// the recording is done.
					cancelFullscreenTimer();
					onRecord(v);
				}

			}
		});

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
			VerticalSeekBar sb = (VerticalSeekBar) recMenu
					.findViewById(R.id.seekBar1);

			sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar arg0) {

					if (arg0.getProgress() == 0) {

						// during videorecording should not allow camera
						// snapshot
						if (_recordInProgress) {
							showDialog(DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW);
							arg0.setProgress(1);

							return;
						}

						snapShot.setImageResource(R.drawable.bb_snap_icon);
						videoRec.setImageResource(R.drawable.bb_rec_icon_d);
						engageBtn.setImageResource(R.drawable.bb_snap_btn);

					} else {
						snapShot.setImageResource(R.drawable.bb_snap_icon_d);
						videoRec.setImageResource(R.drawable.bb_rec_icon);
						// check if recording is still in process
						if (_recordInProgress) {
							engageBtn
							.setImageResource(R.drawable.bb_rec_stop_btn);
						} else {
							engageBtn
							.setImageResource(R.drawable.bb_rec_start_btn);
						}
					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {

				}

				@Override
				public void onProgressChanged(SeekBar arg0, int arg1,
						boolean arg2) {

				}
			});

		} else {
			SeekBar sb = (SeekBar) recMenu.findViewById(R.id.seekBar1);

			sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar arg0) {

					if (arg0.getProgress() == 0) {
						// during videorecording should not allow camera
						// snapshot
						if (_recordInProgress) {
							showDialog(DIALOG_VIDEO_RECORDING_MODE_NO_CAMERA_SNAPSHOT_ALLOW);
							arg0.setProgress(1);

							return;
						}
						snapShot.setImageResource(R.drawable.bb_snap_icon);
						videoRec.setImageResource(R.drawable.bb_rec_icon_d);
						engageBtn.setImageResource(R.drawable.bb_snap_btn);

					} else {

						snapShot.setImageResource(R.drawable.bb_snap_icon_d);
						videoRec.setImageResource(R.drawable.bb_rec_icon);
						// check if recording is still in process
						if (_recordInProgress) {
							engageBtn
							.setImageResource(R.drawable.bb_rec_stop_btn);
						} else {
							engageBtn
							.setImageResource(R.drawable.bb_rec_start_btn);
						}

					}
				}

				@Override
				public void onStartTrackingTouch(SeekBar arg0) {

				}

				@Override
				public void onProgressChanged(SeekBar arg0, int arg1,
						boolean arg2) {

				}
			});
		}

		tryToGoToFullScreen();

	}

	private void updateHQIcon() {
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY,
				PublicDefine.RESOLUTON_QVGA);

//		if (img_res == PublicDefine.RESOLUTON_VGA) {
//			// VGA - HQ enabled
//			leftSideMenuAdpt.setHQenable(true);
//		} else {
//			leftSideMenuAdpt.setHQenable(false);
//		}

		// Refresh the ui
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout leftMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
				if (leftMenu != null) {
					GridView gridview = (GridView) leftMenu
							.findViewById(R.id.slide_content);
					gridview.invalidateViews();

				}

			}
		});

	}


	private void onPushToTalk(View v) {

		showJoystickOnly();

		RelativeLayout direction = (RelativeLayout) findViewById(R.id.directionLayout);
		if (direction != null) {
			direction.setVisibility(View.INVISIBLE);
		}
		RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
		if (pttLayout != null) {
			pttLayout.setVisibility(View.VISIBLE);
		}

		TalkBackTouchListener tb = new TalkBackTouchListener(new Handler(this));
		ImageView btn = (ImageView) pttLayout.findViewById(R.id.micIcon);
		btn.setVisibility(View.VISIBLE);
		btn.setOnTouchListener(tb);



	}


	private void onHqSwitch(View v) {
		// /
		if (device_ip == null) {
			return;
		}

		isUpdatingResolution = true;
		//skip 10 frames when updating resolution
		numberOfFrameToSkip = 0;

		Thread worker = new Thread() {
			public void run() {
				boolean send_via_udt = false;
				if (selected_channel != null) {
					if ( (selected_channel.getCamProfile().getRemoteCommMode() == 
							StreamerFactory.STREAM_MODE_UDT) || (selected_channel.getCamProfile().getRemoteCommMode() ==
							StreamerFactory.STREAM_MODE_RELAY) )
					{
						send_via_udt = true;
					}
				}

				// TODO: please change this.. very dirty way of doing
				// things

				SharedPreferences settings = getSharedPreferences(
						PublicDefine.PREFS_NAME, 0);
				int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY,
						PublicDefine.RESOLUTON_QVGA);

				if (send_via_udt == true) {

					String request = PublicDefine.SET_RESOLUTION_QVGA;
					if (img_res == PublicDefine.RESOLUTON_QVGA) {
						request = PublicDefine.SET_RESOLUTION_VGA;
						img_res = PublicDefine.RESOLUTON_VGA;
					} 
					else 
					{

						request = PublicDefine.SET_RESOLUTION_QVGA;
						img_res = PublicDefine.RESOLUTON_QVGA;

					}

					if ((selected_channel.getCamProfile().getRemoteCommMode() == 
							StreamerFactory.STREAM_MODE_UDT))
					{
						UDTRequestSendRecv.sendRequest_via_stun(
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getDeviceMac(),
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getChannelID(), request,
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getUser(),
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getPass());
					}
					else
					{
						UDTRequestSendRecv.sendRequest_via_stun(
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getRestrationId(),
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getChannelID(), request,
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getUser(),
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getPass());
					}

				} 
				else 
				{

					final String device_address_port = device_ip + ":"
							+ device_port;
					String http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
							"http://", device_address_port,
							PublicDefine.HTTP_CMD_PART,
							PublicDefine.SET_RESOLUTION_QVGA);

					if (img_res == PublicDefine.RESOLUTON_QVGA) {
						http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
								"http://", device_address_port,
								PublicDefine.HTTP_CMD_PART,
								PublicDefine.SET_RESOLUTION_VGA);

						img_res = PublicDefine.RESOLUTON_VGA;
					} else {
						http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
								"http://", device_address_port,
								PublicDefine.HTTP_CMD_PART,
								PublicDefine.SET_RESOLUTION_QVGA);

						img_res = PublicDefine.RESOLUTON_QVGA;

					}

					HTTPRequestSendRecv.sendRequest_block_for_response(
							http_addr_1, PublicDefine.DEFAULT_BASIC_AUTH_USR,
							http_pass);

				}

				SharedPreferences.Editor editor = settings.edit();
				editor.putInt(PublicDefine.PREFS_VQUALITY, img_res);
				editor.commit();

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateHQIcon();

					}

				});

			}
		};

		worker.start();

	}
	
	/* 20130402: hoang: refresh HQ icon. */
	private void refreshHqIcon() {
		// /
		if (device_ip == null) {
			return;
		}

		isUpdatingResolution = true;
		//skip 10 frames when updating resolution
		numberOfFrameToSkip = 0;

		Thread worker = new Thread() {
			public void run() {
				boolean send_via_udt = false;
				if (selected_channel != null) {
					if ((selected_channel.getCamProfile().getRemoteCommMode() == 
							StreamerFactory.STREAM_MODE_UDT) || (selected_channel.getCamProfile().getRemoteCommMode() == 
							StreamerFactory.STREAM_MODE_RELAY) )
					{
						send_via_udt = true;
					}
				}

				// TODO: please change this.. very dirty way of doing
				// things

				SharedPreferences settings = getSharedPreferences(
						PublicDefine.PREFS_NAME, 0);
				int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY,
						PublicDefine.RESOLUTON_QVGA);

				if (send_via_udt == true) 
				{

					String request = PublicDefine.SET_RESOLUTION_QVGA;
					if (img_res == PublicDefine.RESOLUTON_QVGA) {
						request = PublicDefine.SET_RESOLUTION_QVGA;
					} else {
						request = PublicDefine.SET_RESOLUTION_VGA;
					}

					if (selected_channel.getCamProfile().getRemoteCommMode() == 
							StreamerFactory.STREAM_MODE_UDT)
					{
						UDTRequestSendRecv.sendRequest_via_stun(
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getDeviceMac(),
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getChannelID(), request,
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getUser(),
								((BabyMonitorUdtAuthentication) bm_session_auth)
								.getPass());
					}
					else
					{
						UDTRequestSendRecv.sendRequest_via_stun(
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getRestrationId(),
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getChannelID(), request,
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getUser(),
								((BabyMonitorRelayAuthentication) bm_session_auth)
								.getPass());
					}

				} 
				else 
				{

					final String device_address_port = device_ip + ":"
							+ device_port;
					String http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
							"http://", device_address_port,
							PublicDefine.HTTP_CMD_PART,
							PublicDefine.SET_RESOLUTION_QVGA);

					if (img_res == PublicDefine.RESOLUTON_QVGA) {
						http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
								"http://", device_address_port,
								PublicDefine.HTTP_CMD_PART,
								PublicDefine.SET_RESOLUTION_QVGA);
					} else {
						http_addr_1 = String.format("%1$s%2$s%3$s%4$s",
								"http://", device_address_port,
								PublicDefine.HTTP_CMD_PART,
								PublicDefine.SET_RESOLUTION_VGA);
					}

					HTTPRequestSendRecv.sendRequest_block_for_response(
							http_addr_1, PublicDefine.DEFAULT_BASIC_AUTH_USR,
							http_pass);

				}
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateHQIcon();

					}

				});

			}
		};

		worker.start();

	}


	// MBP 2k,1K only
	private void mbp_updateAlertSettings(CamChannel thisChannel) {

		/* Special case: MBP2k, 1k */
		String phonemodel = android.os.Build.MODEL;
		if (!phonemodel.equals(PublicDefine.PHONE_MBP2k)
				&& !phonemodel.equals(PublicDefine.PHONE_MBP1k)) {
			// Do nothing
			return;
		}
		CamProfile cp = thisChannel.getCamProfile();

		SetupData tempData = new SetupData();
		try {
			if (tempData.restore_session_data(getExternalFilesDir(null))) {

				CamProfile[] temp_profiles = tempData.get_CamProfiles();
				if (cp != null) {
					for (int j = 0; j < temp_profiles.length; j++) {
						if (temp_profiles[j] != null
								&& temp_profiles[j].get_MAC() != null
								&& temp_profiles[j].get_MAC().equalsIgnoreCase(
										cp.get_MAC())) {
							cp.setSoundAlertEnabled(temp_profiles[j]
									.isSoundAlertEnabled());
							cp.setTempHiAlertEnabled(temp_profiles[j]
									.isTempHiAlertEnabled());
							cp.setTempLoAlertEnabled(temp_profiles[j]
									.isTempLoAlertEnabled());
							break;
						}
					}
				}
			}
		} catch (StorageException e) {
			e.printStackTrace();
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
		}
	}

	private void onAlarmSettings() {

		if (device_ip == null || selected_channel == null) {
			Log.e("mbp", "Device ip or Seleted channel = NULL");
			return;
		}

		final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
		dialog.setContentView(R.layout.bb_melody_dialog);

		ListView alarms = (ListView) dialog.findViewById(R.id.melodies);
		if (alarms == null) {
			return;
		}

		TextView title = (TextView) dialog.findViewById(R.id.t0);
		if (title != null) {
			title.setText(R.string.notifications);
		}

		ShortAlertSettingAdapter  adapter = new ShortAlertSettingAdapter(this, selected_channel, 
				new UpdateAlertUICallback());

		alarms.setAdapter(adapter);


		showDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
		// MBP only ..
		mbp_updateAlertSettings(selected_channel);

		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		GetDisabledAlertsTask getAlertsTask = new GetDisabledAlertsTask(
				this,
				new GetDisabledAlertsTask.IGetAlertsCallBack() {

					// Called in PostExcute - UI THread
					public void onSuccess() {
						dialog.show();

						try {
							dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
						} catch (Exception e) {

						}
					}

					public void onError() {

					}
				}, saved_token);

		CamProfile[] arr = new CamProfile[] { selected_channel.getCamProfile() };


		//For MBP2k,1K --this will simply return without connecting to Server
		getAlertsTask.execute(arr);

	}


	private void onMelody(View v) {

		if (device_ip == null) {
			return;
		}

		final String device_address_port = device_ip + ":" + device_port;

		final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
		dialog.setContentView(R.layout.bb_melody_dialog);

		ListView melodies = (ListView) dialog.findViewById(R.id.melodies);
		if (melodies == null) {
			return;
		}

		melodies.setSelection(currentMelodyIndx);

		melodies.setAdapter(new BaseAdapter() {

			private final String[] melodyItems = getMelodyItems(
					selected_channel.getCamProfile().getRemoteCommMode());
			
			private String[] getMelodyItems(int stream_mode)
			{
				String[] items = getResources().getStringArray(
						R.array.CameraMenuActivity_melody_items);
				String version = null;
				switch (stream_mode)
				{
				case StreamerFactory.STREAM_MODE_HTTP_LOCAL:
				case StreamerFactory.STREAM_MODE_HTTP_REMOTE:
					
					try {
						version = HTTPRequestSendRecv.getFirmwareVersion(
								device_ip, String.valueOf(device_port), null, null);
					} catch (SocketException e) {
						e.printStackTrace();
					}

					if (version != null)
					{
						if (version.startsWith("1"))
						{
							items = getResources().getStringArray(
									R.array.CameraMenuActivity_melody_items_2);
						}
					}
					
					break;
					
				case StreamerFactory.STREAM_MODE_UDT:
					
					version = UDTRequestSendRecv.getFirmwareVersionViaStun(
							((BabyMonitorUdtAuthentication)bm_session_auth).getDeviceMac(),
							((BabyMonitorUdtAuthentication)bm_session_auth).getChannelID(),
							PublicDefine.GET_VERSION,
							((BabyMonitorUdtAuthentication)bm_session_auth).getUser(),
							((BabyMonitorUdtAuthentication)bm_session_auth).getPass());

					if (version != null)
					{
						if (version.startsWith("1"))
						{
							items = getResources().getStringArray(
									R.array.CameraMenuActivity_melody_items_2);
						}
					}
					
					break;
					
				case StreamerFactory.STREAM_MODE_RELAY:
					
					version = UDTRequestSendRecv.getFirmwareVersionViaStun(
							((BabyMonitorRelayAuthentication)bm_session_auth).getRestrationId(),
							((BabyMonitorRelayAuthentication)bm_session_auth).getChannelID(),
							PublicDefine.GET_VERSION,
							((BabyMonitorRelayAuthentication)bm_session_auth).getUser(),
							((BabyMonitorRelayAuthentication)bm_session_auth).getPass());

					if (version != null)
					{
						if (version.startsWith("1"))
						{
							items = getResources().getStringArray(
									R.array.CameraMenuActivity_melody_items_2);
						}
					}
					
					break;
					
				default:
					items = getResources().getStringArray(
							R.array.CameraMenuActivity_melody_items);
					break;
				}
				
				return items;
			}

			public View getView(int position, View convertView, ViewGroup parent) {
				LinearLayout itemView;
				TextView txt;
				if (convertView == null) { // if it's not recycled, initialize
					// some
					// attributes
					LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					itemView = (LinearLayout) inflater.inflate(
							R.layout.bb_melody_list_item, null);

				} else {
					itemView = (LinearLayout) convertView;

				}
				ImageView ind = (ImageView) itemView.findViewById(R.id.img);
				if (ind != null) {
					if (position == currentMelodyIndx) {
						ind.setVisibility(View.VISIBLE);
					} else {
						ind.setVisibility(View.INVISIBLE);
					}
				}

				txt = (TextView) itemView.findViewById(R.id.melodyItem);
				txt.setText((String) getItem(position));

				if (position == currentMelodyIndx) {
					txt.setTypeface(Typeface.DEFAULT_BOLD);
				} else {
					txt.setTypeface(Typeface.DEFAULT);
				}

				return itemView;
			}

			public long getItemId(int position) {
				return position;
			}

			public Object getItem(int position) {
				return melodyItems[position];
			}

			public int getCount() {
				return melodyItems.length;
			}

		});

		melodies.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {

				Thread worker = new Thread() {
					public void run() {
						boolean send_via_udt = false;
						if (selected_channel != null) {
							if ( (selected_channel.getCamProfile()
									.getRemoteCommMode() == StreamerFactory.STREAM_MODE_UDT) ||
									(selected_channel.getCamProfile()
									.getRemoteCommMode() == StreamerFactory.STREAM_MODE_RELAY) )
							{
								send_via_udt = true;
							}
						}

						// TODO: please change this.. very dirty way of doing
						// things

						if (send_via_udt == true) {
							/*int localPort = 0;
							if (bm_session_auth != null) {
								localPort = bm_session_auth.getLocalPort();
							}*/

							if (position == 0) {

								String request = PublicDefine.SET_MELODY_OFF;
								if (selected_channel.getCamProfile()
										.getRemoteCommMode() == StreamerFactory.STREAM_MODE_UDT)
								{
									Log.d("mbp",
											"relayToken is: "
													+ ((BabyMonitorUdtAuthentication) bm_session_auth)
													.getRelayToken());
									UDTRequestSendRecv
									.sendRequest_via_stun(
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getDeviceMac(),
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getChannelID(),
											request,
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getUser(),
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getPass());
								}
								else
								{
									UDTRequestSendRecv
									.sendRequest_via_stun(
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getRestrationId(),
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getChannelID(),
											request,
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getUser(),
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getPass());
								}
							} else {
								String command = String.format("%1$s%2$s",
										"melody", position);
								if (selected_channel.getCamProfile()
										.getRemoteCommMode() == StreamerFactory.STREAM_MODE_UDT)
								{
									UDTRequestSendRecv
									.sendRequest_via_stun(
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getDeviceMac(),
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getChannelID(),
											command,
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getUser(),
											((BabyMonitorUdtAuthentication) bm_session_auth)
											.getPass());
								}
								else
								{
									UDTRequestSendRecv
									.sendRequest_via_stun(
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getRestrationId(),
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getChannelID(),
											command,
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getUser(),
											((BabyMonitorRelayAuthentication) bm_session_auth)
											.getPass());
								}

							}

						} else {
							String http_addr = null;
							http_addr = String
									.format("%1$s%2$s%3$s%4$d", "http://",
											device_address_port,
											"/?action=command&command=melody",
											position);
							if (position == 0) {
								String http_addr_1 = String.format(
										"%1$s%2$s%3$s%4$s", "http://",
										device_address_port,
										PublicDefine.HTTP_CMD_PART,
										PublicDefine.SET_MELODY_OFF);

								HTTPRequestSendRecv
								.sendRequest_block_for_response(
										http_addr_1,
										PublicDefine.DEFAULT_BASIC_AUTH_USR,
										http_pass);
							} else {
								HTTPRequestSendRecv
								.sendRequest_block_for_response(
										http_addr,
										PublicDefine.DEFAULT_BASIC_AUTH_USR,
										http_pass);
							}
						}
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								updateMelodyIcon(position);

							}

						});

					}
				};

				worker.start();
				dialog.dismiss();

			}

		});

		dialog.show();

		return;
	}

	private void setupSideMenu()
	{
		RelativeLayout leftSideMenu = (RelativeLayout) findViewById(R.id.left_side_menu);

		/* build the grid base on given size */
		GridView gridview = (GridView) leftSideMenu
				.findViewById(R.id.slide_content);
		gridview.setAdapter(null);
		if (leftSideMenuAdpt == null) {
			leftSideMenuAdpt = new LeftSideMenuImageAdapter(this);
		}

		gridview.setAdapter(leftSideMenuAdpt);
		gridview.invalidateViews();

		gridview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction() & MotionEvent.ACTION_MASK) {

				case MotionEvent.ACTION_DOWN:
					showSideMenusAndStatus();
					break;
				case MotionEvent.ACTION_UP:
					for (int i = 0; i < ((GridView) v).getChildCount(); i++) {
						View img = ((GridView) v).getChildAt(i);
						img.dispatchTouchEvent(MotionEvent.obtain(0,
								System.currentTimeMillis(),
								MotionEvent.ACTION_UP, 0, 0, 0));
					}

					tryToGoToFullScreen();
					break;
				default:

					break;
				}

				/* return false means we don't handle the event */
				return false;
			}
		});
		// ... and the handlers
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, final View v,
					int position, long id) {

				switch (position) {
//				case LeftSideMenuImageAdapter.pos_menu:
//
//					int commMode = CameraMenuActivity.COMM_MODE_HTTP;
//					String channelId = "dummy";
//					if ( (selected_channel.getCamProfile().getRemoteCommMode() == 
//							StreamerFactory.STREAM_MODE_UDT) ||
//							(selected_channel.getCamProfile().getRemoteCommMode() == 
//							StreamerFactory.STREAM_MODE_RELAY) )
//					{
//						commMode = CameraMenuActivity.COMM_MODE_UDT;
//
//					}
//
//
//					//					cancelVideoStoppedReminder();
//
//
//					//					if (selected_channel != null) {
//					//						// cancel current remote connection
//					//						selected_channel.cancelRemoteConnection();
//					//					}
//
//
//					synchronized (ViewCameraActivity.this) {
//						reason_to_leave = REASON_LAUNCH_OTHER_ACTIVITY;
//					}
//
//					SharedPreferences settings = getSharedPreferences(
//							PublicDefine.PREFS_NAME, 0);
//					String saved_token = settings.getString(
//							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
//
//					String camera_mac = selected_channel.getCamProfile()
//							.get_MAC();
//					String camera_name = selected_channel.getCamProfile()
//							.getName();
//					String camera_url = device_ip + ":" + device_port;
//
//					boolean offlineMode = settings.getBoolean(
//							PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
//
//					Intent i = new Intent(ViewCameraActivity.this,
//							CameraMenuActivity.class);
//
//					i.putExtra(CameraMenuActivity.bool_isLoggedIn, !offlineMode);
//					i.putExtra(CameraMenuActivity.str_userToken, saved_token);
//					i.putExtra(CameraMenuActivity.str_deviceUrl, camera_url);
//					i.putExtra(CameraMenuActivity.str_deviceMac, camera_mac);
//					i.putExtra(CameraMenuActivity.str_deviceName, camera_name);
//
//					i.putExtra(CameraMenuActivity.int_CommMode, commMode);
//
//					int port = -1;
//
//					if (ViewCameraActivity.this.bm_session_auth != null) {
//						port = bm_session_auth.getLocalPort();
//
//					}
//
//					i.putExtra(CameraMenuActivity.int_UdtLocalPort, port);
//
//					if (!selected_channel.getCamProfile().isInLocal()) 
//					{
//						if (selected_channel.getCamProfile().getRemoteCommMode() == 
//								StreamerFactory.STREAM_MODE_UDT)
//						{
//							channelId = ((BabyMonitorUdtAuthentication) bm_session_auth)
//									.getChannelID();
//						}
//						else
//						{
//							channelId = ((BabyMonitorRelayAuthentication) bm_session_auth)
//									.getChannelID();
//						}
//						i.putExtra(CameraMenuActivity.string_UdtChannelId,
//								channelId);
//					}
//
//					i.putExtra(CameraMenuActivity.bool_cameraInLocal,
//							selected_channel.getCamProfile().isInLocal());
//
//
//
//					IntentFilter cameraSettingFilter = new IntentFilter();
//					cameraSettingFilter.addAction(CameraMenuActivity.ACTION_STOP_CAMERA_VIEW);
//					
//					registerReceiver(cameraSettingClosed, cameraSettingFilter);
//					
//					// EntryActivity.this.startActivity(i);
//					ViewCameraActivity.this.startActivityForResult(i,
//							REQUEST_START_CAMERA_MENU);
//					break;
//
////				case LeftSideMenuImageAdapter.pos_snapshot:// Take Snapshot
////					onSwitchToRecordMenu(v);
////					break;
//				case LeftSideMenuImageAdapter.pos_rec:// Take Snapshot
//					onSwitchToRecordMenu(v);
//					break;
//				case LeftSideMenuImageAdapter.pos_vox_settings:
//					onAlarmSettings();
//					break;
//				case LeftSideMenuImageAdapter.pos_melody: // melody
//					onMelody(v);
//					break;
//				case LeftSideMenuImageAdapter.pos_mic:
//					/*
//					 * 20130204: hoang: issue 1260 disable push-to-talk in STUN
//					 * mode
//					 */
//					if ((selected_channel != null)
//							&& ( (selected_channel.getCamProfile()
//									.getRemoteCommMode() == StreamerFactory.STREAM_MODE_UDT) ||
//									(selected_channel.getCamProfile()
//											.getRemoteCommMode() == StreamerFactory.STREAM_MODE_RELAY))) {
//						// do nothing
//					} 
//					else
//					{
//						if (_enablePtt == false) 
//						{
//							_enablePtt = true;
//							enablePtt(_enablePtt);
//						}
//						onPushToTalk(v);
//					}
//					break;
//				case LeftSideMenuImageAdapter.pos_cam_spk:
//					onSpeaker(v);
//					break;
//				case LeftSideMenuImageAdapter.pos_highquality:
//					onHqSwitch(v);
//					break;
//				default:
//					break;
				}
			}
		});

	}

	private void viewCameraLayout()
	{
		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
			setContentView(R.layout.bb_main_landscape);
			shouldRotateBitmap = false;

		} else if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT) {
			setContentView(R.layout.bb_main);

			shouldRotateBitmap = true;
		}

		isFullScreen = false;


		// Dont need to register for screen timeout event 
		//setup_ScreenTimeout_br();

		VerticalSeekBar zoomBar = (VerticalSeekBar) findViewById(R.id.zoomBar);
		if (zoomBar != null) {
			// update the current progress.
			zoomBar.setProgress(DEFAUL_ZOOM_LEVEL - currentZoomInLevel);
			zoomBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					int value = seekBar.getProgress();

					if (value == 0) {
						// no zoom:
						currentZoomInLevel = DEFAUL_ZOOM_LEVEL;
					} else if (value < DEFAUL_ZOOM_LEVEL) {
						currentZoomInLevel = DEFAUL_ZOOM_LEVEL - value;
					}

					tryToGoToFullScreen();
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

					showSideMenusAndStatus();
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {

				}
			});
		}

		setupSideMenu();

		record_enabled = snapshot_enabled = false;

		videoGrp = (RelativeLayout) findViewById(R.id.video_grp);

		_vImageView = (VideoSurfaceView) findViewById(R.id.imageVideo);

		_vImageView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				// IF videorecording is going on.. ignore these event
				RelativeLayout recMenu = (RelativeLayout) findViewById(R.id.rec_menu);
				if (_recordInProgress && (recMenu != null)
						&& (recMenu.isShown())) {
					// INGORE TOUCH event
					return true;
				}

				final int action = event.getAction();
				switch (action & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_DOWN:
					if (isFullScreen == true) 
					{
						exitSubfunction();
						showSideMenusAndStatus();

						isFullScreen = false;
					}
					else
					{
						goToFullScreen();
					}
					break;
				case MotionEvent.ACTION_UP:
					tryToGoToFullScreen();
					break;
				}

				return true;
			}
		});

		_vImageView.setHandler(new Handler(this));

		ImageView direction_pad = (ImageView) findViewById(R.id.directionPad);
		// ImageView direction_indicator = (ImageView)
		// findViewById(R.id.directionInd);
		joystickListener = new DirectionTouchListener_bb(this, new Handler(
				new Handler.Callback() {

					@Override
					public boolean handleMessage(Message msg) {
						switch (msg.what) {
						case DirectionTouchListener.MSG_JOYSTICK_IS_BEING_USED:
							// showSideMenusAndStatus();

							showJoystickOnly();
							cancelFullscreenTimer();
							break;
						case DirectionTouchListener.MSG_JOYSTICK_IS_BEING_MOVED:
							cancelFullscreenTimer();
							break;
						case DirectionTouchListener.MSG_JOYSTICK_IS_NOT_BEING_USED:
							tryToGoToFullScreen();
							break;
						default:
							break;
						}

						return false;
					}

				}), direction_pad, null, device_comm);

		// joystickListener.setOrientation(shouldRotateBitmap);
		direction_pad.setOnTouchListener(joystickListener);

		record_enabled = snapshot_enabled = true;

		displayBG(true);

		tryToGoToFullScreen();
		videoIsShowing = true;

	}


	private void requestViewCamViaUDT(CamChannel ch, Handler h) 
	{
		ch.getCamProfile().setRemoteCommMode(StreamerFactory.STREAM_MODE_UDT);

		/*
		 * by pass the getPortState here - as UDT the port will be part of
		 * viewcamrequest inside
		 */
		ch.setCurrentViewSession(CamChannel.REMOTE_UDT_VIEW);

		/* get user name & password */
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		String saved_usr = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_USR, null);
		String saved_pwd = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_PWD, null);

		ViewRemoteCamUdtRequestTask viewCamViaUdt = new ViewRemoteCamUdtRequestTask(h, ViewCameraActivity.this);

		ch.setViewReqState(viewCamViaUdt);
		/* at the end, MSG_VIEW_CAM_SUCCESS or FAILED will be sent to this class */
		viewCamViaUdt.execute(saved_usr, saved_pwd, ch.getCamProfile()
				.get_MAC());
	}
	
	private void requestViewCamViaRelay(CamChannel ch, Handler h)
	{
		
		try {
			dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
		} catch (Exception e) {
		}

		try {
			showDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
		} catch (Exception ie) {
		}
		
		ch.getCamProfile().setRemoteCommMode(
				StreamerFactory.STREAM_MODE_RELAY);
		ch.setCurrentViewSession(CamChannel.REMOTE_RELAY_VIEW);
		
		ViewRelayCamRequestTask viewCamViaRelay = 
				new ViewRelayCamRequestTask(h, ViewCameraActivity.this);
		
		ch.setViewReqState(viewCamViaRelay);
		
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);

		String saved_usr = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_USR, null);
		String saved_pwd = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_PWD, null);
		
		viewCamViaRelay.execute(saved_usr, saved_pwd, 
				ch.getCamProfile().get_MAC());
	}
	
	/*
	 * 20130523: hoang: use for request view camera through media server
	 */
	private void requestViewCamViaServer(CamChannel ch, Handler h)
	{
		
		try {
			dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
		} catch (Exception e) {
		}

		try {
			showDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
		} catch (Exception ie) {
		}
		
		ch.getCamProfile().setRemoteCommMode(
				StreamerFactory.STREAM_MODE_RELAY);
		ch.setCurrentViewSession(CamChannel.REMOTE_RELAY_VIEW);
		
		ViewRemoteCamMediaServerRequestTask viewCamViaServer = 
				new ViewRemoteCamMediaServerRequestTask(h, ViewCameraActivity.this);
		
		ch.setViewReqState(viewCamViaServer);
		
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);

		String saved_usr = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_USR, null);

		String saved_pwd = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_PWD, null);
		
		viewCamViaServer.execute(saved_usr, saved_pwd, 
				ch.getCamProfile().get_MAC());
	}

	/**
	 * GLOBAL - Send ViewCam Req to BMS to ask for camera url and start
	 * streaming
	 * 
	 * @selected_channel has to be not - null
	 */

	private void prepareToViewCameraRemotely(final boolean shouldIgnoreErrorAndRetry) 
	{
		if (selected_channel == null) 
		{
			return; // STH is WRONG!!!!
		}

		Handler.Callback getStreamCB = new GetStreamModeCb(shouldIgnoreErrorAndRetry);

		/* In some cases, if we show the dialog and the whole remote view process failed quickly 
		 *  which lead to calling this function multiple times -> the dialog will be flashing on/off
		 *  
		 *  One case we know for sure is when there is  no SSID & no IP, i.e. no Connection 
		 */
		boolean shouldShowConnectingDialog = true; 

		// Simple check to see if we use wifi or other data network
		if (ConnectToNetworkActivity.haveInternetViaOtherMedia(this)) {
			// Can't do anything.
		} 
		else
		{

			WifiManager w = (WifiManager) getSystemService(WIFI_SERVICE);
			if (w.getConnectionInfo() != null) 
			{
				String curr_ssid = w.getConnectionInfo().getSSID();
				Log.e("mbp","prepareToViewCameraRemotely:Before GET STREAM  SSID: "
						+ curr_ssid + " ip is: "
						+ w.getConnectionInfo().getIpAddress());

				if ( (curr_ssid == null) && (w.getConnectionInfo().getIpAddress() == 0) ) //NO IP address
				{
					shouldShowConnectingDialog = false; 
				}

			}
		}


		selected_channel.getCamProfile().setRemoteCommMode(
				StreamerFactory.STREAM_MODE_HTTP_REMOTE);

		ViewUpnpCamRequestTask viewUpnpTask = new ViewUpnpCamRequestTask(
						new Handler(ViewCameraActivity.this),
						ViewCameraActivity.this);
		
		selected_channel.setCurrentViewSession(CamChannel.REMOTE_RELAY_VIEW);
		selected_channel.setViewReqState(viewUpnpTask);
		
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(
				PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		
		String clientType = "android";
		viewUpnpTask.execute(selected_channel.getCamProfile().get_MAC(),
				saved_token, clientType);

		if (shouldShowConnectingDialog ==true)
		{
			try
			{
				showDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			}
			catch (Exception e){};
		}

		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PublicDefine.PREFS_CAM_BEING_VIEWED, selected_channel.getCamProfile().get_MAC());
		editor.commit();

		/* 20120906: clear all alert for this camera */
		AlertData.clearAlertForCamera(selected_channel.getCamProfile()
				.get_MAC(), getExternalFilesDir(null));

		if (remoteVideoTimer != null) {
			Log.d("mbp", "cancel current VideoTimeoutTask");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}

		remoteVideoTimer = new Timer();
		remoteVideoTimer.schedule(new VideoTimeoutTask(), VIDEO_TIMEOUT);

	}

	/**
	 * Update device ip and port from this camProfile object. This is called
	 * when app has successfully connected to the camera ip, port will be passed
	 * back to caller. This is to support UDT connection where initially we
	 * don't know the ip just yet. Only when app has connected to camera then we
	 * wil get back this info
	 * 
	 * by right until here the : audio and video player is working alright just
	 * the direction control thread needs to be restarted ..
	 * 
	 * @param cp
	 * 
	 */
	private void updateAuthenticationObject(BabyMonitorAuthentication bm_auth) {
		/*
		 * kill away the direction control thread this was setup with the old
		 * ip/port
		 */
		if (device_comm_thrd != null) {
			device_comm.terminate();
			boolean retry = true;
			while (retry) {
				try {
					device_comm_thrd.join(2000);

					retry = false;
				} catch (InterruptedException e) {
				}

			}
			device_comm_thrd = null;
		}

		InetAddress remote_addr;
		try {
			remote_addr = InetAddress.getByName(bm_auth.getIP());

			selected_channel.getCamProfile().setInetAddr(remote_addr);
			selected_channel.getCamProfile().setPort(bm_auth.getPort());

			// Re-layout , create new joystick & control thread
			setupRemoteCamera(selected_channel, bm_auth);

			updateMelodyIcon(currentMelodyIndx);

			setSpeakerOn(enableSpeaker);
			enablePtt(_enablePtt);
		} catch (UnknownHostException e) {
		}

	}

	/**
	 * 
	 * Precondition: s_channel should be set to a non-null channel which has a
	 * valid (IP)cam profile
	 * 
	 * @param s_channel
	 *            - must have a non-null cam profile in which device_ip,
	 *            device_port are set.
	 */
	private void setupRemoteCamera(final CamChannel s_channel,
			BabyMonitorAuthentication bm_auth) {

		currentConnectionMode = CONNECTION_MODE_REMOTE;

		if (bm_auth != null) {
			device_ip = bm_auth.getIP();
			device_port = bm_auth.getPort();
			bm_session_auth = bm_auth;// reserved to used later if we need to
			// restart the videostreamer -audio only
			// mode
		}

		try {
			http_pass = CameraPassword.getPasswordforCam(
					getExternalFilesDir(null), s_channel.getCamProfile()
					.get_MAC());
		} catch (StorageException e) {
			Log.d("mbp", e.getLocalizedMessage());
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
			return;
		}
		
		device_audio_in_port = s_channel.getCamProfile().get_ptt_port();
		record_enabled = snapshot_enabled = false;
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
		/* set Touches */
		if (s_channel.getCamProfile().getRemoteCommMode() == 
				StreamerFactory.STREAM_MODE_UDT) 
		{
			dev_comm = new UDTRequestSendRecv(device_ip, device_port,
					(BabyMonitorUdtAuthentication) bm_session_auth);
			device_comm = new DirectionDispatcher(saved_token, selected_channel.getCamProfile().get_MAC());
		}
		else if (s_channel.getCamProfile().getRemoteCommMode() ==
				StreamerFactory.STREAM_MODE_RELAY)
		{
//			dev_comm = new RelayRequestSendRecv(device_ip, device_port,
//					(BabyMonitorRelayAuthentication) bm_session_auth);
//			device_comm = new DirectionDispatcher(saved_token, selected_channel.getCamProfile().get_MAC());
		}
		else
		{
			device_comm = new DirectionDispatcher("http://" + device_ip + ":"
					+ device_port);
		}

		String camModel = s_channel.getCamProfile().getModel();
		camModel = CamProfile.MODEL_ID_MBP836;
		if (camModel.equalsIgnoreCase(CamProfile.MODEL_ID_MBP836))
		{
			//Launch H.264
			Intent intent = new Intent(ViewCameraActivity.this,
					FFMpegPlayerActivity.class);
			intent.putExtra(DashBoardActivity.CAMCHANNEL_SHOWING_CHANNEL,s_channel);
			String streamUrl = s_channel.getStreamUrl();
			Log.d("mbp", "Stream url: " + streamUrl);
			intent.putExtra("StreamUrl", streamUrl);
			startActivity(intent);
			// will finish ViewCameraActivity when received broadcast from ffmpeg
//			finish();
		}
	}



	private void setupRemoteVideoThreads(CamChannel s_channel,
			BabyMonitorAuthentication bm_auth) {

		// setup audio player
		if (use_pcmPlayer)
		{
			_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO);

			pcmPlayer_thrd = new Thread(_pcmPlayer, "PCMPlayer");
			pcmPlayer_thrd.start();
		}
		else
		{
			pcmPlayer_thrd= null; 
			_pcmPlayer = null; 
		}


		// Use a factory to get the correct streamer be it Http or UDT
		// streamer..
		//		Log.d("mbp", "Thread: " + Thread.currentThread().getId()
		//				+ " setupRemoteVideoThreads : configuring video threads ");
		_streamer = StreamerFactory.getStreamer(s_channel.getCamProfile(),
				new Handler(this), this, bm_auth.getIP(), bm_auth.getPort());

		if (http_pass != null) {
			_streamer.setHTTPCredential(PublicDefine.DEFAULT_BASIC_AUTH_USR,
					http_pass);
		}

		// For REMOTE CONNECTION: Disabled Speaker by default
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		enableSpeaker = settings.getBoolean(
				PublicDefine.PREFS_LOCAL_VIEW_MUTED, false);

		_enablePtt = false;

		_streamer.enableAudio(_enableAudio);
		_streamer.addVideoSink(this);
		_streamer.setMelodyUpdater(this);
		_streamer.setResUpdater(this);
		_streamer.setTemperatureUpdater(this);

		/* secure connection */
		if (bm_auth != null) {
			_streamer.setRemoteAuthentication(bm_auth);
		}
		_streamer.restart();

		// // Myth: ***** UDT has to run on the same thread that it receives the
		// camera info ****////

		streamer_thrd = new Thread(_streamer, "VAStreamer");
		streamer_thrd.start();
		Log.d("mbp", "setupRemoteVideoThreads : start new streamer thread: "
				+ streamer_thrd.getId());




		///USE SERVICE .. 
		Log.d("mbp","[REMOTE] Turning on services...");
		startAVSService();

	}



	private void setupInfraCamera(final CamChannel s_channel) {

		currentConnectionMode = CONNECTION_MODE_LOCAL_INFRA;

		WifiManager w = (WifiManager) getSystemService(WIFI_SERVICE);
		String curr_ssid = w.getConnectionInfo().getSSID();

		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(string_currentSSID, curr_ssid);
		editor.putString(PublicDefine.PREFS_CAM_BEING_VIEWED, s_channel
				.getCamProfile().get_MAC());

		// Commit the edits!
		editor.commit();
		boolean shouldMuteSpk = settings.getBoolean(
				PublicDefine.PREFS_LOCAL_VIEW_MUTED, false);

		String camModel = s_channel.getCamProfile().getModel();
		camModel = CamProfile.MODEL_ID_MBP836;
		Log.d("mbp", "cam model: "+camModel);
		if (camModel.equalsIgnoreCase(CamProfile.MODEL_ID_MBP836))
		{
			device_ip = s_channel.getCamProfile().get_inetAddress()
					.getHostAddress();
			device_port = s_channel.getCamProfile().get_port();
			
			//Launch H.264
			Intent intent = new Intent(ViewCameraActivity.this,
					FFMpegPlayerActivity.class);
			
			intent.putExtra(DashBoardActivity.CAMCHANNEL_SHOWING_CHANNEL,s_channel);
			
			
//			String streamUrl = String.format("http://%s:%d%s",
//					device_ip, device_port, PublicDefine.GET_FLV_STREAM_CMD);
			String streamUrl = String.format("rtsp://user:pass@%s:%d/blinkhd",
					device_ip, 6667);
			Log.d("mbp", "Stream url: " + streamUrl);
			intent.putExtra("StreamUrl", streamUrl);
			startActivity(intent);
			// will finish ViewCameraActivity when received broadcast from ffmpeg
//			finish();
		}

	}

	private void enablePtt(boolean enable) {

//		if (leftSideMenuAdpt != null) {
//			leftSideMenuAdpt.setEnablePtt(enable);
//		} else {
//			Log.d("mbp", "leftSideMenuAdpt = null");
//		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				RelativeLayout leftMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
				if (leftMenu != null) {
					GridView gridview = (GridView) leftMenu
							.findViewById(R.id.slide_content);
					gridview.invalidateViews();

				}

			}
		});
	}


	/**
	 * 
	 * Can be called when v = null
	 * 
	 * 
	 * @param v
	 *            - can be null
	 */
	private void onSpeaker(View v) {

		enableSpeaker = !(enableSpeaker);
		_enableAudio = enableSpeaker;
		setSpeakerOn(enableSpeaker);

		if (this.selected_channel != null && this.selected_channel.getCamProfile() != null)
		{
			Log.d("mbp", "store spk status"); 
			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(PublicDefine.PREFS_LOCAL_VIEW_MUTED,!enableSpeaker);

			// Commit the edits!
			editor.commit();
		}

	}

	/**
	 * TO BE CALL AFTER _streamer & leftSideMenyAdpt are setup
	 * 
	 * @param b
	 */
	private void setSpeakerOn(boolean b) {
//		if (leftSideMenuAdpt != null) {
//			leftSideMenuAdpt.setEnableSpeaker(b);
//		}
//		if (_streamer != null) {
//			_streamer.enableAudio(b);
//		}

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout leftMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
				if (leftMenu != null) {
					GridView gridview = (GridView) leftMenu
							.findViewById(R.id.slide_content);
					gridview.invalidateViews();

				}

			}
		});
	}

	/**
	 * Should not be called in UI Thread
	 * 
	 * @param enabled
	 * @return
	 */
	private boolean setTalkBackEnabled(boolean enabled)
	{
		if (enabled == true)
		{

			/*
			 * Disable audio - But dont touch the UI stuf onSpeaker(null);
			 */
			if (_streamer != null) {
				_streamer.enableAudio(false);
			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					/* enable talkback */
					pcmRecorder = new PCMRecorder(device_ip, device_port,
							http_pass, device_audio_in_port, new Handler(
									ViewCameraActivity.this));

					talkback_enabled = pcmRecorder.startRecording();

					if (!talkback_enabled) {

						Spanned msg = Html.fromHtml("<big>"
								+ getResources().getString(
										R.string.EntryActivity_ptt_2)
										+ "</big>");
						Toast.makeText(ViewCameraActivity.this, msg,
								Toast.LENGTH_LONG).show();
					}
				}
			});

			// Dont fade
			cancelFullscreenTimer();
		}
		else
		{
			/* disable talkback */
			talkback_enabled = false;
			if (pcmRecorder != null) {
				pcmRecorder.stopRecording();
				pcmRecorder = null;
			}
			/* Enable audio */
			if (!enableSpeaker) {
				onSpeaker(null);
			} else {
				setSpeakerOn(true);
			}

			tryToGoToFullScreen();
		}

		return talkback_enabled;
	}

	public void onSnaptshot() {

		if (!snapshot_enabled || _recordInProgress) {
			return;
		}
		_snapshot = true;

	}

	public void onRecord(View view) {

		if (record_enabled == false)
			return;

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == 
				Configuration.ORIENTATION_LANDSCAPE)
		{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		} 
		else
		{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		final ImageView rec_status = (ImageView) findViewById(R.id.rec_status_icon);

		if (_recordInProgress) 
		{
			/* release keep screen on */
			RelativeLayout root = (RelativeLayout) findViewById(R.id.videoScreenRoot);
			if (root != null) {
				root.setKeepScreenOn(false);
			}

			this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					// update icon ---
					ImageButton rec_btn = (ImageButton) findViewById(R.id.btnImg);
					if (rec_btn != null) {
						rec_btn.setImageResource(R.drawable.bb_rec_start_btn);
					}
				}
			});
			// Stop recording
			_aviRecorder.setRecordFlag(PublicDefine.STOP);
			_recordInProgress = false;

			// showDialog(DIALOG_TO_DISPLAY_VIDEO_FILENAME);
			showFileDialogMessage(Util.getShortFileName(fileName));

			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

			/* 20120321- */
			// This also works: Util.addImageGallery(this, fileName);
			Log.d("mbp", "Use unmount/mount to refresh");
			/* 20130322: hoang: fix crash when snapshot or record video
			 * action MEDIA_UNMOUNTED don't work on Android version 3.x
			 * use this broadcast if this is MBP phone
			 */
			String phoneModel = android.os.Build.MODEL;
			if (phoneModel.equals(PublicDefine.PHONE_MBP2k)
					|| phoneModel.equals(PublicDefine.PHONE_MBP1k))
			{
				sendBroadcast(new Intent(Intent.ACTION_MEDIA_UNMOUNTED,
						Uri.parse("file://"
								+ Environment.getExternalStorageDirectory())));
			}
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
					Uri.parse("file://"
							+ Environment.getExternalStorageDirectory())));

			this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (rec_status != null)
						rec_status.setVisibility(View.INVISIBLE);

					// try go go to fullscreen after recording is done
					tryToGoToFullScreen();
				}
			});

			_aviRecorder = null;
			return;
		}

		if (Util.spaceAvailableOnDisk() < 100 * 1024 * 1024) 
		{
			showDialog(DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_VIDEO);
		} 
		else
		{
			/* keep screen on */
			RelativeLayout root = (RelativeLayout) findViewById(R.id.videoScreenRoot);
			if (root != null) {
				root.setKeepScreenOn(true);
			}

			// update icon ---
			this.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					ImageButton rec_btn = (ImageButton) findViewById(R.id.btnImg);
					if (rec_btn != null) {
						rec_btn.setImageResource(R.drawable.bb_rec_stop_btn);
					}
				}
			});

			fileName = Util.getRecordFileName();

			/*
			 * SharedPreferences settings =
			 * getSharedPreferences(PublicDefine.PREFS_NAME, 0); _maxSize = 1024
			 * * 1024 * settings.getInt("maxRecordSize",
			 * PublicDefine.MAX_RECORD_SIZE_DEFAULT);
			 */

			// fixed at 100mb now
			_maxSize = 100 * 1024 * 1024;

			_aviRecorder = new AviRecord(fileName,
					_streamer.getImageResolution(), _enableAudio);
			_aviRecorder.start();
			_aviRecorder.resetRecordFlag();
			_aviRecorder.setRecordFlag(PublicDefine.RUN);

			_recordInProgress = true;

			rec_status.setVisibility(View.VISIBLE);

			final TextView recTime = (TextView) findViewById(R.id.textRecTime);
			if (recTime != null) {
				recTime.setVisibility(View.VISIBLE);
				final long startTime = System.currentTimeMillis();
				Runnable timer = new Runnable() {

					@Override
					public void run() {
						while (_recordInProgress) {
							long duration = System.currentTimeMillis()
									- startTime;

							// Date date = new Date(duration);
							// SimpleDateFormat df = new SimpleDateFormat(
							// "HH:mm:ss");
							// final String duration_str = df.format(date);

							final String duration_str = Util
									.formatMillis(duration);

							ViewCameraActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									recTime.setText(duration_str);
								}
							});
						}

						ViewCameraActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								recTime.setText("");
								recTime.setVisibility(View.INVISIBLE);

							}
						});

					}
				};

				new Thread(timer).start();

			}
		}
	}


	private void stopAllThread() {
		synchronized (this) {
			boolean retry = true;

			stopFrameRateTimer();

			// 20120511: change the temperature popup logic check updateTemp..
			// startTempAlarmCounting = false;

			if (scan_task != null
					&& scan_task.getScanStatus() != LocalScanForCameras.SCAN_CAMERA_FINISHED) {
				/* if it's either PENDING or RUNNING */
				Log.d("mbp", "EntryActivity: cancel SCAN task");
				scan_task.stopScan();
				// wait for this task to be canccel completely
				try {
					// at most 2 seconds
					Thread.sleep(2100);
				} catch (Exception e) {
					Log.d("mbp", "Exception while waiting for scan task to end");
					e.printStackTrace();
				}
			}

			/*
			 * Once we stop all thread, we will need to disable the talkback and
			 * recording as well
			 */
			if (talkback_enabled == true) {
				setTalkBackEnabled(false);
			}
			if (_recordInProgress == true) {
				this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// Stop recording
						onRecord(null);
					}
				});
			}

			if (streamer_thrd != null) {
				// streamer_thrd.
				_streamer.stop();
				while (retry) {
					try {
						streamer_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}
				}
				streamer_thrd = null;
			}

			if (pcmPlayer_thrd != null) {
				_pcmPlayer.stop();
				retry = true;
				while (retry) {
					try {
						pcmPlayer_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}

				}
				pcmPlayer_thrd = null;
			}

			if (device_comm_thrd != null) {
				device_comm.terminate();
				retry = true;
				while (retry) {
					try {
						device_comm_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}

				}
				device_comm_thrd = null;
			}



		}
	}




	private void updateFrameCount() {
		synchronized (this) {
			total_frame++;
		}
	}

	private void startFrameRateTimer() {

		stopFrameRateTimer();

		frameRateTimer = new Timer();

		frameRateTimer.scheduleAtFixedRate(new FrameRateTimeoutTask(), 5000,
				5000);

	}

	private void stopFrameRateTimer() {
		if (frameRateTimer != null) {
			frameRateTimer.cancel();
			frameRateTimer = null;
		}
	}

	private boolean shouldShowFrame() {
		boolean result;

		if (isUpdatingResolution)
		{
			numberOfFrameToSkip++;
			if (numberOfFrameToSkip <= 10) // skip 10 frames
			{
				result = false;
			}
			else
			{
				result = true;
				isUpdatingResolution = false;
			}
		} 
		else
		{
			result = true;
		}

		return result;
	}

	@Override
	public void onFrame(byte[] frame, byte[] pcm, int pcm_len)
	{
		updateFrameCount();
		if (shouldShowFrame() && (frame.length > 0))
		{
			/**** Start drawing ****/
			Bitmap b = null;
			b = BitmapFactory.decodeByteArray(frame, 0, frame.length);

			Canvas c = null;

			SurfaceHolder _surfaceHolder = _vImageView.get_SurfaceHolder();

			if (_surfaceHolder == null) {
				Log.w("mbp", "_surfaceHolder is NULL");
				return;
			}
			


			try {
				c = _surfaceHolder.lockCanvas(null);
				if (c == null) {
					// Log.w("mbp", "onFrame: canvas is NULL");
					return;
				}

				/* Check if we should crop for portrait */
				if (shouldRotateBitmap == true) {

					int newHeight = b.getHeight();

					// double hw_ratio =
					// (double)b.getHeight()/(double)b.getWidth();

					double hw_ratio = (double) c.getHeight()
							/ (double) c.getWidth();

					double newWidth = (int) (newHeight / hw_ratio);
					// Log.d("mbp", "hw_ratio: " + hw_ratio );

					int new_x = (int) (((double) b.getWidth() - newWidth) / 2);

					// Log.d("mbp", "b: " + new_x + ", 0,"+ newWidth + "," +
					// newHeight);
					b = Bitmap.createBitmap(b, new_x, 0, (int) newWidth,
							newHeight);

				}

				int left = 0;
				int top = 0;
				int right = b.getWidth();
				int bottom = b.getHeight();
				int destWidth;
				int destHeight;
				/*
				 * Maintain Aspect Ratio float ratio = (float) right/bottom;
				 * 
				 * float fw = c.getHeight()* ratio; float fh = c.getWidth() *
				 * ratio;
				 * 
				 * int destWidth; int destHeight ;
				 * 
				 * if ( fw > c.getWidth()) { destWidth = c.getWidth();
				 * destHeight = (int)fh; } else { destWidth = (int) fw;
				 * destHeight = c.getHeight();
				 * 
				 * }
				 */

				/*
				 * Strech mode
				 * 
				 * 
				 * destWidth = c.getWidth(); destHeight = c.getHeight();
				 */

				/*
				 * 3. Honor the width ratio, Dont care about the height being
				 * cut off
				 */
				float ratio = (float) right / bottom;
				float fh = c.getWidth() / ratio;

				destWidth = c.getWidth();
				destHeight = (int) fh;

				int dst_top = 0;
				if (destHeight > c.getHeight()) {
					int delta = destHeight - c.getHeight();
					dst_top = -delta / 2;
					destHeight -= delta / 2;
				} else {
					// expand the height
					destHeight = c.getHeight();
				}

				if (currentZoomInLevel < DEFAUL_ZOOM_LEVEL)
				{

					int newWidth = (int) (right
							* (2 * DEFAUL_ZOOM_LEVEL - (DEFAUL_ZOOM_LEVEL - currentZoomInLevel)) * ZOOM_STEP);
					int newHeight = (int) (bottom
							* (2 * DEFAUL_ZOOM_LEVEL - (DEFAUL_ZOOM_LEVEL - currentZoomInLevel)) * ZOOM_STEP);

					left = (b.getWidth() - newWidth) / 2;
					top = (b.getHeight() - newHeight) / 2;
					right = left + newWidth;
					bottom = top + newHeight;

				}

				Rect src = new Rect(left, top, right, bottom);

				Rect dest = new Rect(0, dst_top, destWidth, destHeight);

				synchronized (_surfaceHolder) {
					c.drawBitmap(b, src, dest, null);
				}
			} finally {
				if (c != null) {
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}
		/**** END drawing ****/

		/* --- IF RECORDING ---- */
		if (_recordInProgress) {

			if (_aviRecorder.getCurrentRecordSize() >= _maxSize) {
				// Stop recording
				_aviRecorder.setRecordFlag(PublicDefine.STOP);
				// *continue recording on a new file
				if (Util.spaceAvailableOnDisk() < 100 * 1024 * 1024) // 100 MB
					// at
					// least
					// to
					// store
					// the
					// video
				{
					_vImageView.post(new Runnable() {
						public void run() {
							showDialog(DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_VIDEO);
						}
					});

					onRecord(null);
				} else {
					String fileName = Util.getRecordFileName();
					Log.d("mbp", "continue recording on a new file: "
							+ fileName);
					_aviRecorder = new AviRecord(fileName,
							_streamer.getImageResolution(), _enableAudio);
					_aviRecorder.start();
					_aviRecorder.resetRecordFlag();
					_aviRecorder.setRecordFlag(PublicDefine.RUN);
				}
			}

			if (_streamer.getResetFlag() == 1) {
				_aviRecorder.resetCounter();
			}

			/*
			 * Amazingly Java!!!: if ((Array)pcm).length is not called ever
			 * before it will add upto 500ms delay for large data ~8kbytes
			 */
			// if(pcm.length > 0)
			if (pcm_len > 0) {
				if (_aviRecorder.getRecordFlag() == PublicDefine.RUN) {
					_aviRecorder.getAudio(pcm, pcm_len,
							_streamer.getResetAudioBufferCount());
				}
			}

			if (_aviRecorder.getRecordFlag() == PublicDefine.RUN) {
				_aviRecorder.getImage(frame, frame.length,
						_streamer.getImgCurrentIndex());
			}

		} /* if(_recordInProgress) */

		/* --- IF SNAPSHOT ---- */
		if (_snapshot) {

			if (Util.spaceAvailableOnDisk() < 100 * 1024) // 100 kb at least to
				// store the
				// snapshot
			{
				showDialog(DIALOG_STORAGE_NOT_ENOUGH_FREE_SPACE_FOR_SNAPSHOT);
			} else {
				final String fileName = Util.getSnapshotFileName();
				BufferedOutputStream bos = null;

				try {
					bos = new BufferedOutputStream(new FileOutputStream(
							fileName));
					bos.write(frame);
					bos.close();

				} catch (IOException e) {
					// todo: error handling
				}

				/* 20120321- */
				// This also works: Util.addImageGallery(this, fileName);
				// May block the ui..?-- check
				Log.d("mbp", "Use unmount/mount to refresh");
				/* 20130322: hoang: fix crash when snapshot or record video
				 * action MEDIA_UNMOUNTED don't work on Android version 3.x
				 * use this broadcast if this is MBP phone
				 */
				String phoneModel = android.os.Build.MODEL;
				if (phoneModel.equals(PublicDefine.PHONE_MBP2k)
						|| phoneModel.equals(PublicDefine.PHONE_MBP1k))
				{
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_UNMOUNTED,
							Uri.parse("file://"
									+ Environment.getExternalStorageDirectory())));
				}
				sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
						Uri.parse("file://"
								+ Environment.getExternalStorageDirectory())));

				_vImageView.post(new Runnable() {
					public void run() {

						Spanned msg = Html.fromHtml("<big>"
								+ getResources().getString(
										R.string.EntryActivity_rec_2)
										+ Util.getShortFileName(fileName) + "</big>");
						AlertDialog snapshotFileNameDialog = new AlertDialog.Builder(
								ViewCameraActivity.this)
						.setMessage(msg)
						.setNeutralButton(android.R.string.ok, null)
						.create();

						snapshotFileNameDialog.show();


					}
				});
			}
			_snapshot = false;

		} /* if(_snapshot) { */


		/*20130307: phung : use the service to play sound.
		if (use_pcmPlayer && 
				(pcm != null) && 
				(pcmPlayer_thrd != null) && 
				pcmPlayer_thrd.isAlive()
				)
		{
			_pcmPlayer.writePCM(pcm, pcm_len);
		}
		.*/

	}


	@Override
	public void updateMelodyIcon(int index) {
//		if (index >= 0 && index < melody_icons.length) {
//			currentMelodyIndx = index;
//
//			leftSideMenuAdpt.setMelodyMuted((currentMelodyIndx == 0));
//		}

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				RelativeLayout leftMenu = (RelativeLayout) findViewById(R.id.left_side_menu);
				if (leftMenu != null) {
					GridView gridview = (GridView) leftMenu
							.findViewById(R.id.slide_content);
					gridview.invalidateViews();

				}

			}
		});

	}

	@Override
	public void updateTemperature(int degreeC)
	{
		final TextView temp_txt = (TextView) findViewById(R.id.textTemp);
		final TextView temp_status = (TextView) findViewById(R.id.textNwSlowOrTempAlert);
		final RelativeLayout statusBar = (RelativeLayout) findViewById(R.id.statusBar);
		final ImageView tempAlarm = (ImageView) findViewById(R.id.tempAlarm);
		final ImageView nwSlowAlarm = (ImageView) findViewById(R.id.nwSlowAlarm);

		//Log.d("mbp", "temperature is : "+ degreeC);
		/*
		 * 20120402: another hack dont display if temp is <0 or > 60
		 */
		if (degreeC < 1 || degreeC > 60) {
			if (temp_txt != null) {
				temp_txt.post(new Runnable() {

					@Override
					public void run() {
						temp_txt.setText("");
					}
				});
			}

			//Log.d("mbp", "temperature is : "+ degreeC);

			return;
		}

		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		int temp_index = settings.getInt(PublicDefine.PREFS_TEMPERATURE_UNIT,
				PublicDefine.TEMPERATURE_UNIT_DEG_F);

		float deg;
		String unit;
		boolean shouldGoRed = false;
		if (temp_index == PublicDefine.TEMPERATURE_UNIT_DEG_F) 
		{
			deg = ((float) degreeC * 9 + 32 * 5) / 5;
			unit = "\u2109";

			if (deg > PublicDefine.HIGH_F_TEMPERATURE_THRESHOLD) 
			{
				flagTempHighOrLow = true;
				shouldGoRed = true;
			} 
			else if (deg < PublicDefine.LOW_F_TEMPERATURE_THRESHOLD) 
			{
				flagTempHighOrLow = false;
				shouldGoRed = true;
			}
		} 
		else 
		{
			deg = (float) degreeC;
			unit = "\u2103";
			if (deg > PublicDefine.HIGH_C_TEMPERATURE_THRESHOLD)
			{
				flagTempHighOrLow = true;
				shouldGoRed = true;
			} 
			else if (deg < PublicDefine.LOW_C_TEMPERATURE_THRESHOLD) 
			{
				flagTempHighOrLow = false;
				shouldGoRed = true;
			}
		}

		/*
		 * "\u2103" = deg Celsius symbol "\u2109" = deg Farenheit Symbol
		 * Conversion:
		 * 
		 * F = C* 9/5 + 32 = (C*9 + 32 *5)/5
		 */

		/*
		 * 20130122: hoang: round down or round up temperature
		 */
		int rounded_deg = Math.round(deg);
		final String display = String.format("%d %s", rounded_deg, unit);
		final boolean goRed = shouldGoRed;
		if (temp_txt != null) {
			temp_txt.post(new Runnable() {

				@Override
				public void run() {

					temp_txt.setText(display);
				}
			});

		}

		if ((nwSlowAlarm != null) && !(nwSlowAlarm.isShown())) {
			// Alert temperature above normal
			if (goRed) {

				// Show the alarm settings icon
				if (tempAlarm != null) {
					tempAlarm.post(new Runnable() {

						@Override
						public void run() {

							tempAlarm.setVisibility(View.VISIBLE);
						}
					});
				}

				// Set the background color is red
				if (statusBar != null) {
					statusBar.post(new Runnable() {

						@Override
						public void run() {

							statusBar.setBackgroundColor(Color.RED);
						}
					});
				}

				if (temp_status != null) {
					temp_status.post(new Runnable() {

						@Override
						public void run() {
							temp_status.setVisibility(View.VISIBLE);
							if (flagTempHighOrLow) {
								temp_status.setText(getResources().getString(
										R.string.temp_above_normal));
							} else {
								temp_status.setText(getResources().getString(
										R.string.temp_below_normal));
							}
						}
					});
				}

			} else {

				// Hide the alarm settings icon
				if (tempAlarm != null) {
					tempAlarm.post(new Runnable() {

						@Override
						public void run() {

							tempAlarm.setVisibility(View.INVISIBLE);
						}
					});
				}

				// Set the background color is normal
				if (statusBar != null) {
					statusBar.post(new Runnable() {

						@Override
						public void run() {

							statusBar.setBackgroundColor(Color.TRANSPARENT);
						}
					});
				}

				if (temp_status != null) {
					temp_status.post(new Runnable() {

						@Override
						public void run() {

							temp_status.setVisibility(View.GONE);
						}
					});
				}
			}
		}
	}


	@Override
	public void updateResolution(int res) {
		SharedPreferences settings = getSharedPreferences(
				PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PublicDefine.PREFS_VQUALITY, res);
		editor.commit();
		updateHQIcon();
	}



	@Override
	public boolean handleMessage(Message msg)
	{

		switch (msg.what) {
		case MSG_SHORT_TOUCH_RELEASED:
			videoGrp.post(new Runnable() {

				@Override
				public void run() {
					RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
					ImageView btn = (ImageView) pttLayout
							.findViewById(R.id.micIcon);
					if (btn != null)
						btn.setImageDrawable((getResources()
								.getDrawable(R.drawable.bb_vs_mike_on)));
				}
			});
			break;
		case MSG_LONG_TOUCH_RELEASED:
			videoGrp.post(new Runnable() {

				@Override
				public void run() {
					RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
					ImageView btn = (ImageView) pttLayout
							.findViewById(R.id.micIcon);
					if (btn != null)
						btn.setImageDrawable((getResources()
								.getDrawable(R.drawable.bb_vs_mike_on)));
				}
			});
			setTalkBackEnabled(false);
			break;
		case MSG_LONG_TOUCH_START:
			videoGrp.post(new Runnable() {

				public void run() {
					RelativeLayout pttLayout = (RelativeLayout) findViewById(R.id.pttLayout);
					ImageView btn = (ImageView) pttLayout
							.findViewById(R.id.micIcon);
					if (btn != null)
						btn.setImageDrawable((getResources()
								.getDrawable(R.drawable.bb_vs_mike_off)));
				}
			});

			break;
		case MSG_LONG_TOUCH:
			setTalkBackEnabled(true);
			break;
		case MSG_PCM_RECORDER_ERR:// talkback problem from PCMRecorder
			if (talkback_enabled == true) {
				setTalkBackEnabled(false);
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(
								ViewCameraActivity.this,
								getResources().getString(
										R.string.EntryActivity_ptt_1),
										Toast.LENGTH_LONG).show();

					}
				});
			}

			break;
		case MSG_SURFACE_CREATED:
			displayBG(true);
			break;

		case AudioOutStreamer.UNMUTE_CAMERA_AUDIO_FAILED:

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					try {
						showDialog(DIALOG_FAILED_TO_UNMUTE_CAM_AUDIO);
					} catch (Exception ie) {
					}
				}
			});
			break;

		case VideoStreamer.MSG_VIDEO_STREAM_SWICTHED_TO_UDT_RELAY:

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {
					}


					try {
						showDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
					} catch (Exception ie) {
					}
					
				}
			});
			break;
			
		case VideoStreamer.MSG_VIDEO_STREAM_SWICTHED_TO_UDT_RELAY_2:
			
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {
					}


					try {
						showDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
					} catch (Exception ie) {
					}
					
					/*
					 * 20130509: hoang: switch to new relay solution
					 */
					selected_channel.getCamProfile().setRemoteCommMode(
							StreamerFactory.STREAM_MODE_RELAY);
					selected_channel.setCurrentViewSession(CamChannel.REMOTE_RELAY_VIEW);
					
					ViewRelayCamRequestTask viewCamViaRelay = new ViewRelayCamRequestTask(
							new Handler(ViewCameraActivity.this), ViewCameraActivity.this);
					
					selected_channel.setViewReqState(viewCamViaRelay);
					
					SharedPreferences settings = getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);

					String saved_usr = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_USR, null);

					String saved_pwd = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_PWD, null);
					
					viewCamViaRelay.execute(saved_usr, saved_pwd, 
							selected_channel.getCamProfile().get_MAC());
					
				}
			});
			
			break;
			
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER:

			try {
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {
			}

			try {
				dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
			} catch (Exception e) {
			}

			remoteVideoHasStopped(VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER);
			break;

		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_TIMEOUT: 
		{
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY,
						"Connect To Cam Failed", "Timeout", null);
			}
			
			final Runnable showDialog = new Runnable() {
				@Override
				public void run() {
					/*
					 * 20130201: hoang: issue 1260 turn off screen when finish
					 * view request
					 */
					if (wl != null && wl.isHeld()) {
						wl.release();
						wl = null;
						Log.d("mbp", "release WakeLock");
					}

					stopAllThread();
					cancelVideoStoppedReminder();



					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {
					}
					try {
						showDialog(DIALOG_REMOTE_VIDEO_STREAM_TIMEOUT);
					} catch (Exception ie) {
					}

					if (remoteVideoTimer != null) {
						Log.d("mbp", "stop remoteVideoTimer .. since video stopped ");
						remoteVideoTimer.cancel();
						remoteVideoTimer = null;
					}
					/* turn on the screen */
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					if (!pm.isScreenOn()) {
						Log.d("mbp", "Turn screen on for a bit");
						wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
								| PowerManager.ACQUIRE_CAUSES_WAKEUP
								| PowerManager.ON_AFTER_RELEASE,
								"TURN ON Because of error");
						wl.setReferenceCounted(false);
						wl.acquire(10000);
					}


				}
			};

			if (_vImageView != null && _vImageView.isShown())
			{
				runOnUiThread(showDialog);
			}
			else
			{
				Log.d("mbp", "app at background.. AND TIMEOUT ");
				//app is at background -- beeeeepp 
				remoteVideoHasStopped(VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER);
			}
			break;
		}

		case VideoStreamer.MSG_SESSION_KEY_MISMATCHED: 
		{
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY,
						"Connect To Cam Failed", "Session key mismatched", null);
			}

			final Runnable showDialog = new Runnable() {
				@Override
				public void run() {
					// Should happen during remote-access only
					if (wl != null && wl.isHeld()) {
						wl.release();
						wl = null;
						Log.d("mbp", "MSG_SESSION_KEY_MISMATCHED - release WakeLock");
					}
					stopAllThread();
					cancelVideoStoppedReminder();

					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {
					}
					try {

						showDialog(DIALOG_SESSION_KEY_MISMATCHED);
					} catch (Exception ie) {
					}

					if (remoteVideoTimer != null) {
						Log.d("mbp", "stop remoteVideoTimer .. since video stopped ");
						remoteVideoTimer.cancel();
						remoteVideoTimer = null;
					}

				}
			};
			runOnUiThread(showDialog);
			break;
		}

		case VideoStreamer.MSG_CAMERA_IS_NOT_AVAILABLE: 
		{
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY,
						"Connect To Cam Failed", "Camera is not available", null);
			}

			final Runnable showDialog = new Runnable() {
				@Override
				public void run() {
					// Should happen during remote-access only
					if (wl != null && wl.isHeld()) {
						wl.release();
						wl = null;
						Log.d("mbp", "MSG_CAMERA_IS_NOT_AVAILABLE - release WakeLock");
					}
					stopAllThread();
					cancelVideoStoppedReminder();

					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {
					}
					try {
						showDialog(DIALOG_CAMERA_IS_NOT_AVAILABLE);
					} catch (Exception ie) {
					}

					if (remoteVideoTimer != null) {
						Log.d("mbp", "stop remoteVideoTimer .. since video stopped ");
						remoteVideoTimer.cancel();
						remoteVideoTimer = null;
					}

				}
			};
			runOnUiThread(showDialog);
			break;
		}

		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY:
		{
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY,
						"Connect To Cam Failed", "Other Cam Connect Error", null);
			}
			
			if (wl != null && wl.isHeld()) 
			{
				wl.release();
				wl = null;
				Log.d("mbp", 
						"MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY - release WakeLock");
			}

			try 
			{
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} 
			catch (Exception e) 
			{
			}

			try 
			{
				dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
			} 
			catch (Exception e) 
			{
			}

			// GET this when VideoStream about to end unexpectedly

			if ((_vImageView != null && _vImageView.isShown())
					|| ViewCameraActivity.this.videoIsShowing) 
			{

				if (ViewCameraActivity.this.selected_channel.getCamProfile().isInLocal())
				{
					videoHasStoppedUnexpectedly();
				} 
				else if (ViewCameraActivity.this.selected_channel.getCamProfile().isReachableInRemote()) 
				{
					remoteVideoHasStopped(VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY);
				}


			}

			break;
		}
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED:
			if (wl != null && wl.isHeld()) {
				wl.release();
				wl = null;
				Log.d("mbp", "MSG_VIDEO_STREAM_HAS_STOPPED - release WakeLock");
			}
			break;

		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STARTED:
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY,
						"Start Stream Success", "Start Stream Success", null);
			}

			if (wl != null && wl.isHeld()) {
				wl.release();
				wl = null;
				Log.d("mbp", "MSG_VIDEO_STREAM_HAS_STARTED - release WakeLock");
			}

			cancelVideoStoppedReminder();

			shouldBeepAndShow = 0;

			if (_playTone != null && _playTone.isAlive()) {
				Log.d("mbp",
						"MSG_VIDEO_STREAM_HAS_STARTED: stop play Tone thread now");
				playTone.stopPlaying();
				_playTone.interrupt();
				_playTone = null;
			}


			try {
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {
			}


			try {
				dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
			} catch (Exception e) {
			}

			if (msg.obj != null) {
				final BabyMonitorAuthentication auth = (BabyMonitorAuthentication) msg.obj;
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						updateAuthenticationObject(auth);

					}
				});

			}

			// Start thread to cal frameRate
			startFrameRateTimer();

			if (_streamer != null) {
				new Thread() {
					public void run() {
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
						}

						if (_streamer != null) {
							_streamer.initQueries();
						}
					}
				}.start();

			}

			break;
		case Streamer.MSG_VIDEO_STREAM_INTERNAL_ERROR: 
		{
			if ( (selected_channel != null) &&
					(!selected_channel.getCamProfile().isInLocal()) )
			{
				tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY,
						"Connect To Cam Failed" , "Other Cam Connect Error", null);
			}
			
			if (wl != null && wl.isHeld()) {
				wl.release();
				wl = null;
				Log.d("mbp", "MSG_VIDEO_STREAM_INTERNAL_ERROR - release WakeLock");
			}

			try {
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {
			}

			try {
				dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
			} catch (Exception e) {
			}

			final int err = msg.arg1;

			Log.d("mbp", "MSG_VIDEO_STREAM_INTERNAL_ERROR : err: " + err);


			if ((_vImageView != null && _vImageView.isShown())
					|| videoIsShowing)
			{

				if (selected_channel.getCamProfile().isInLocal()) 
				{
					Log.e("mbp", " call  videoHasStoppedUnexpectedly");
					videoHasStoppedUnexpectedly();
				} 
				else if (selected_channel.getCamProfile().isReachableInRemote()) 
				{
					Log.e("mbp", " call  remoteVideoHasStopped");
					remoteVideoHasStopped(VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY);
				}


			}

			break;
		}
		// //UI thread .. UI Thread ..
		case ViewRemoteCamRequestTask.MSG_VIEW_CAM_SUCCESS:

			BabyMonitorAuthentication bm_auth = (BabyMonitorAuthentication) msg.obj;
			if (selected_channel != null) {

				if (selected_channel.setStreamingState() == true) {

					InetAddress remote_addr;
					try {

						remote_addr = InetAddress.getByName(bm_auth.getIP());

						selected_channel.getCamProfile().setInetAddr(
								remote_addr);
						selected_channel.getCamProfile().setPort(
								bm_auth.getPort());
						
						//set stream url for ffmpeg player if codec is H264
						String camModel = selected_channel.getCamProfile().getModel();
						camModel = CamProfile.MODEL_ID_MBP836;
						
						if (camModel.equalsIgnoreCase(CamProfile.MODEL_ID_MBP836))
						{
							try {
								dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
							} catch (Exception e) {
							}

							try {
								dismissDialog(DIALOG_UDT_RELAY_CONNECTION_IN_PROG);
							} catch (Exception ie) {
							}
							
							if (bm_auth != null)
							{
								String stream_url = bm_auth.getStreamUrl();
								selected_channel.setStreamUrl(stream_url);
							}
						}

						setupRemoteCamera(selected_channel, bm_auth);
						
//						if (camModel.equalsIgnoreCase(CamProfile.MODEL_BLINK1) ||
//								camModel.equalsIgnoreCase(CamProfile.MODEL_BLINK1_1))
//						{
//							setupRemoteVideoThreads(selected_channel, bm_auth);
//						}

					} catch (UnknownHostException e) {
						e.printStackTrace();

						/* can't find the host */
						showDialog(DIALOG_CONNECTION_FAILED);
					}
				} 
				else
				{
					if (remoteVideoTimer != null) {
						Log.d("mbp", "cancel current remoteVideoTimer");
						remoteVideoTimer.cancel();
						remoteVideoTimer = null;
					}
					/*
					 * 20130201: hoang: issue 1260 turn off screen when finish
					 * view request
					 */
					if (wl != null && wl.isHeld()) {
						wl.release();
						wl = null;
						Log.d("mbp", "MSG_VIEW_CAM_SUCCESS - release WakeLock");
					}
				}
			}

			break;
			
		case ViewRemoteCamRequestTask.MSG_VIEW_CAM_SWITCH_TO_RELAY:
			Log.d("mbp", "App is behind symmetric NAT...switch to relay stream...");
			Handler h = (Handler) msg.obj;
			requestViewCamViaRelay(selected_channel, h);
			
			break;

		case ViewRemoteCamRequestTask.MSG_VIEW_CAM_FALIED:
			
			try {
				this.dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);

				if (remoteVideoTimer != null) {
					Log.d("mbp", "cancel current remoteVideoTimer");
					remoteVideoTimer.cancel();
					remoteVideoTimer = null;
				}

				/*
				 * 20130201: hoang: issue 1260 turn off screen when finish view
				 * request
				 */
				if (wl != null && wl.isHeld()) {
					wl.release();
					wl = null;
					Log.d("mbp", "MSG_VIEW_CAM_FAILED - release WakeLock");
				}

				int err = msg.arg1;
				switch (err) {
				case 614: // Camera is busy
					showDialog(DIALOG_REMOTE_BM_IS_BUSY);
					break;

				case 612: // Cant' communicate with cam
				default:
					if (selected_channel != null && selected_channel.getCamProfile()
					.getRemoteCommMode() == StreamerFactory.STREAM_MODE_HTTP_REMOTE)
					{
						showDialog(DIALOG_CAMERA_PORT_IS_INACCESSIBLE);
					}
					else
					{
						showDialog(DIALOG_REMOTE_BM_IS_OFFLINE);
					}
					break;
				}

			} catch (Exception e) {
			}

			selected_channel.cancelRemoteConnection();

			if (_playTone != null && _playTone.isAlive()) {
				Log.d("mbp",
						"MSG_VIDEO_STREAM_HAS_STARTED: stop play Tone thread now");
				playTone.stopPlaying();
				_playTone.interrupt();
				_playTone = null;
			}



			break;

		case GetRemoteCamPortTask.MSG_GET_PORT_TASK_SUCCESS:
		{
			ArrayList<RemotePort> result = (ArrayList<RemotePort>) msg.obj;

			try {
				RemotePort ptt_port = result
						.get(RemotePort
								.getIndex(RemotePort._PORT_NAME[RemotePort.PTT_PORT_IDX]));

				selected_channel.getCamProfile().setPTTPort(
						ptt_port.getRemotePort());

				/* get user name & password */
				SharedPreferences settings = getSharedPreferences(
						PublicDefine.PREFS_NAME, 0);
				String saved_usr = settings.getString(
						PublicDefine.PREFS_SAVED_PORTAL_USR, null);
				String saved_pwd = settings.getString(
						PublicDefine.PREFS_SAVED_PORTAL_PWD, null);

				ViewRemoteCamRequestTask viewCam = new ViewRemoteCamRequestTask(
						new Handler(this), this);

				if (selected_channel.setViewReqState(viewCam) == false)
				{
					Log.d("mbp", "return here-- dont viewCam ");
					break;
				}

				/*
				 * at the end, MSG_VIEW_CAM_SUCCESS or FAILED will be send to
				 * this class
				 */
				viewCam.execute(saved_usr, saved_pwd, selected_channel
						.getCamProfile().get_MAC());

			} catch (UndefinedPortException e)
			{
				showDialog(DIALOG_BMS_GET_PORT_ERROR);
			}

			break;
		}
		case GetRemoteCamPortTask.MSG_GET_PORT_TASK_FALIED_REPONSE_ERR:
		case GetRemoteCamPortTask.MSG_GET_PORT_TASK_FALIED_SERVER_UNREACHABLE:

			tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "Get Remote Cam Port Failed",
					"Get Remote Cam Port Failed", null);
			if (remoteVideoTimer != null) {
				remoteVideoTimer.cancel();
				remoteVideoTimer = null;
			}

			if (wl != null && wl.isHeld()) {
				wl.release();
				wl = null;
				Log.d("mbp", "MSG_GET_PORT_TASK_FAILED - release WakeLock");
			}

			selected_channel.cancelRemoteConnection();

			if (_playTone != null && _playTone.isAlive()) {
				Log.d("mbp",
						"MSG_VIDEO_STREAM_HAS_STARTED: stop play Tone thread now");
				playTone.stopPlaying();
				_playTone.interrupt();
				_playTone = null;
			}


			try {
				this.dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);

			} catch (IllegalArgumentException ie) {
			} catch (BadTokenException ie) {
			}

			try {
				showGetPortErrDialog(msg);
			} catch (IllegalArgumentException ie) {
			} catch (BadTokenException ie) {
			}

			break;
		case CheckVersionFW.PATCH_AVAILABLE:
			device = (IpAndVersion) msg.obj;
			String device_ip = device.device_ip;
			Log.d("mbp", "device ip >>> " + device_ip);
			if (device_ip != null) {
				if (selected_channel != null
						&& selected_channel.getCamProfile() != null
						&& selected_channel.getCamProfile().get_inetAddress()
						.getHostAddress().equalsIgnoreCase(device_ip)) {
					try {
						// Log.d("mbp", "fjdkafjladjfldkas");
						this.showDialog(DIALOG_FW_PATCH_FOUND);
					} catch (IllegalArgumentException ie) {
					} catch (BadTokenException ie) {
					}
				} else {
					Log.d("mbp", "selected channel is null: "
							+ (selected_channel == null));
				}
			}
			break;
		case CheckVersionFW.UPGRADE_DONE:
			Log.d("mbp", "case UPGRADE_DONE>>>");
			// show a dialog to wait 80s

			Spanned message = Html
					.fromHtml("<big>"
							+ getString(R.string.upgrade_done_camera_is_rebooting_please_wait_for_about_1_minute_)
							+ "<big>");
			final ProgressDialog dialog_wait = new ProgressDialog(this);
			dialog_wait.setMessage(message);
			dialog_wait.setIndeterminate(true);
			dialog_wait.setCancelable(false);
			dialog_wait.show();
			Handler hl = new Handler();
			hl.postDelayed(new Runnable() {
				@Override
				public void run() {
					dialog_wait.dismiss();

					setResult(DashBoardActivity.RESCAN_CAMERA);
					finish(); 

				}
			}, 80000);
			break;

		case CheckVersionFW.UPGRADE_FAILED:
			Log.d("mbp", "UPGRADE_FAILED");
			AlertDialog.Builder builder;
			AlertDialog alert;
			ProgressDialog dialog;
			Spanned message1;
			builder = new AlertDialog.Builder(this);
			message1 = Html.fromHtml("<big>"
					+ getString(R.string.upgrade_fw_failed) + "</big>");
			builder.setMessage(message1).setPositiveButton(
					getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							setResult(DashBoardActivity.RESCAN_CAMERA);
							finish(); 
						}
					});

			alert = builder.create();
			alert.show();
			break;

		default:
			break;
		}
		return false;
	}


	/**
	 * called from handleMessages() -maybe non ui thread
	 * 
	 * @param reason
	 */
	private void remoteVideoHasStopped(int reason) {

		// Should happen during remote-access only
		stopAllThread();

		// just to be cautious - but should not happen
		cancelVideoStoppedReminder();

		if (remoteVideoTimer != null) {
			Log.d("mbp", "stop remoteVideoTimer .. since video stopped ");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}

		displayBG(true);

		switch (reason) {
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY:
			Log.d("mbp", "remote- video is stopped unexpectedly.");
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// Play tone -start if not start
					if (_playTone == null || !_playTone.isAlive()) {
						playTone = new PlayTone(
								VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY);
						_playTone = new Thread(playTone);
						_playTone.start();
					} else {
						Log.d("mbp",
								"PlayTone is running.. dont start another one");
					}

					// 20120509: auto-relink..???
					Log.d("mbp", "Auto - relink remote camera ");

					/*
					 * 20130201: hoang: issue 1260 turn on screen while
					 * preparing to view remotely
					 */
					PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
					wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
							| PowerManager.ACQUIRE_CAUSES_WAKEUP
							| PowerManager.ON_AFTER_RELEASE,
							"TURN ON Because of error");
					wl.setReferenceCounted(false);
					wl.acquire();
					Log.d("mbp",
							"Acquire WakeLock for prepare to view remotely");

					prepareToViewCameraRemotely(true);
				}
			});

			break;
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER:

			final Runnable showDialog = new Runnable() {
				@Override
				public void run() {
					// Play tone -start if not start
					if (_playTone == null || !_playTone.isAlive())
					{
						playTone = new PlayTone(
								VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER);
						_playTone = new Thread(playTone);
						_playTone.start();
					}

				}
			};
			runOnUiThread(showDialog);
			break;
		}
	}



	@Override
	public void onInitError(String errorMessage) {
	}

	@Override
	public void onVideoEnd() {
	}


	private void switchToAVStreamOnHTTP(boolean videoAndAudio)
	{
		/* Stop the current streamer */
		if (streamer_thrd != null && streamer_thrd.isAlive()) 
		{
			Log.d("mbp", "switchToAVStream: Stop streamer thread: "
					+ streamer_thrd.getId());
			// streamer_thrd.
			_streamer.stop();
			boolean retry = true;
			while (retry) {
				try {
					streamer_thrd.join(2000);

					retry = false;
				} catch (InterruptedException e) {
				}
			}
			streamer_thrd = null;

			if (device_comm_thrd != null) {
				device_comm.terminate();
				retry = true;
				while (retry) {
					try {
						device_comm_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}

				}
				device_comm_thrd = null;
			}

			createNewStreamers(videoAndAudio);

		} 
		else 
		{
			if (streamer_thrd == null) {
				Log.d("mbp",
						"switchToAVStream: Streamer thread is NULL, dont switch");
			} else {
				Log.d("mbp",
						"switchToAVStream: Streamer thread is not alive, dont switch. Thread:"
								+ streamer_thrd.getId());
			}
		}
	}

	/**
	 * switchToAVStream - re-connect the video threads to different audio/video
	 * 
	 * @param videoAndAudio
	 *            - true - switch to Audio Video stream false - switch to Audio
	 *            ONLY stream
	 */
	private void createNewStreamers(boolean videoAndAudio) {

		// Most important thing -update bm_session_auth
		if (bm_session_auth != null) {
			device_ip = bm_session_auth.getIP();
			device_port = bm_session_auth.getPort();
			Log.d("mbp",
					"bm_session_auth deviceport: " + bm_session_auth.getPort());
		}

		// Only switch if the current thread is running
		if (videoAndAudio) 
		{
			/*
			 * set Control dispatcher again - Dont need to do so for audio only
			 * mode
			 */
			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			if (saved_token != null)
			{
				if (selected_channel.getCamProfile().getRemoteCommMode() == 
						StreamerFactory.STREAM_MODE_UDT) {
					
					device_comm = new DirectionDispatcher(saved_token, selected_channel.getCamProfile().get_MAC());
				} 
				else if (selected_channel.getCamProfile().getRemoteCommMode() == 
						StreamerFactory.STREAM_MODE_RELAY)
				{
//					dev_comm = new RelayRequestSendRecv(device_ip, device_port,
//							(BabyMonitorRelayAuthentication) bm_session_auth);
//					device_comm = new DirectionDispatcher(saved_token, selected_channel.getCamProfile().get_MAC());
				}
				else
				{
					device_comm = new DirectionDispatcher("http://" + device_ip
							+ ":" + device_port);
				}
				/* setup connection to send direction */
				device_comm_thrd = new Thread(device_comm, "DirectionHTTP");
				device_comm_thrd.start();

				/* 20120626: update joystickListener */
				if (joystickListener != null)
				{
					Log.d("mbp", "createNewStreamers : update direction Dispatcher");
					joystickListener.setDirectionDispatcher(device_comm);
				}
			}

			_streamer = StreamerFactory.getStreamer(selected_channel
					.getCamProfile(), new Handler(this),
					ViewCameraActivity.this, device_ip, device_port);
			
			_streamer.setAccessToken(saved_token);
			
			if (http_pass != null)
			{
				_streamer.setHTTPCredential(
						PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);
			}
			_streamer.enableAudio(_enableAudio);

			_streamer.addVideoSink(this);
			_streamer.setMelodyUpdater(this);
			_streamer.setResUpdater(this);
			_streamer.setTemperatureUpdater(this);

			if (bm_session_auth != null) {
				_streamer.setRemoteAuthentication(bm_session_auth);
			}

			streamer_thrd = new Thread(_streamer, "AV-Streamer");
			Log.d("mbp", "switchToAVStream: start new AV streamer thread: "
					+ streamer_thrd.getId());

			streamer_thrd.start();

		} 
		else /* play audio stream only */
		{
			_streamer = StreamerFactory.getStreamer(selected_channel
					.getCamProfile(), new Handler(this),
					ViewCameraActivity.this, device_ip, device_port);

			if (http_pass != null) {
				_streamer.setHTTPCredential(
						PublicDefine.DEFAULT_BASIC_AUTH_USR, http_pass);
			}

			if (selected_channel.getCamProfile().getRemoteCommMode() != StreamerFactory.STREAM_MODE_UDT) {
				_streamer.setEnableVideo(false); // STREAM AUDIO ONLY
			}

			_streamer.enableAudio(_enableAudio);
			_streamer.setTemperatureUpdater(null);

			if (bm_session_auth != null) 
			{
				_streamer.setRemoteAuthentication(bm_session_auth);
			}

			streamer_thrd = new Thread(_streamer, "AudioOnly - Streamer");
			Log.d("mbp", "switchToAVStream: start new A only streamer thread: "
					+ streamer_thrd.getId());
			streamer_thrd.start();

		}

	}

	private AVShadowStreamerController avsCtl;

	private void stopAVSService() {
		if (avsCtl != null) {

			avsCtl.stopAVSService();
			avsCtl = null;
		}


	}

	private void startAVSService() {


		if (avsCtl != null)
		{
			avsCtl.rebindAVSService(this._streamer);
		}
		else
		{
			avsCtl = new AVShadowStreamerController(this, this._streamer);
			avsCtl.startAVSService();
		}
	}







	/**********************************************************************************************
	 ********************************** PRIVATE CLASSES *******************************************
	 **********************************************************************************************/
	/**
	 * 
	 * PlayTone and also SHOW dialog in a while loop i.e. dialog showing is
	 * called everytime ..
	 * 
	 * @author phung
	 * 
	 */
	class PlayTone implements Runnable {
		private boolean isRunning;
		private int dialogId;

		/**
		 * @param reason
		 *            - VideoStreamer stopped reason i.e.:
		 *            VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER OR
		 *            VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY
		 */
		public PlayTone(int reason) {
			isRunning = true;

			dialogId = DIALOG_REMOTE_VIDEO_STREAM_STOPPED_UNEXPECTEDLY;
			if (reason == VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER) {
				dialogId =DIALOG_REMOTE_VIDEO_STREAM_TIMEOUT;
			}

		}

		public void stopPlaying() {
			isRunning = false;
		}

		public boolean isRunning() {
			return isRunning;
		}

		@Override
		public void run() {

			MediaPlayer mMediaPlayer = new MediaPlayer();
			String uri = "android.resource://" + getPackageName() + "/"
					+ R.raw.beep;
			try {
				mMediaPlayer.setDataSource(ViewCameraActivity.this, Uri.parse(uri));
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
				mMediaPlayer.prepare();

			} catch (IOException e) {
				e.printStackTrace();
			}
			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
					+ ":PlayTone class " + "is running and showing dialog");

			/* turn on the screen for once */
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			if (!pm.isScreenOn()) {
				Log.d("mbp", "Turn on once ");
				wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP
						| PowerManager.ON_AFTER_RELEASE,
						"TURN ON Because of error");
				wl.setReferenceCounted(false);
				wl.acquire(10000);

			}

			do {


				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// Log.d("mbp","show both..bg & dialog");
						try {
							showDialog(dialogId);
						} catch (Exception e) {
						}

						displayBG(true);

					}
				});

				if  (muteAudioForCall == false)
				{
					mMediaPlayer.start();
				}

				try 
				{
					Thread.sleep(5500);
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (mMediaPlayer.isPlaying())
					mMediaPlayer.stop();


				if (ACTIVITY_HAS_STOPPED == true) {
					Log.d("mbp", "activity has stopped, STOP Playing tone too");
					break;
				}

			} 
			while (isRunning);

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					try {
						dismissDialog(dialogId);
					} catch (Exception ie) {
					}
				}
			});

			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
					+ ":PlayTone class is stopped & dismiss dialog");
		}
	};

	private class MiniWifiScanUpdater implements IWifiScanUpdater 
	{

		@Override
		public void scanWasCanceled() {
			// Do nothing here for now
		}

		@Override
		public void updateWifiScanResult(List<ScanResult> results) {
			if (results == null)
				return;
			SharedPreferences settings = getSharedPreferences(
					PublicDefine.PREFS_NAME, 0);
			String check_SSID = settings.getString(string_currentSSID, null);
			String check_SSID_w_quote = "\"" + check_SSID + "\"";
			boolean found_in_range = false;
			for (ScanResult result : results) {
				if ((result.SSID != null)
						&& (result.SSID.equalsIgnoreCase(check_SSID))) {
					Log.d("mbp", "found " + check_SSID + " .. in range");
					found_in_range = true;
					break;
				}
			}

			if (found_in_range) {
				/* try to connect back to this BSSID */
				WifiManager w = (WifiManager) getSystemService(WIFI_SERVICE);
				List<WifiConfiguration> wcs = w.getConfiguredNetworks();

				Handler.Callback h = new Handler.Callback() {

					@Override
					public boolean handleMessage(Message msg) {
						switch (msg.what) {
						case ConnectToNetwork.MSG_CONNECT_TO_NW_DONE:

							runOnUiThread(new Runnable() {

								@Override
								public void run() {
									if (streamer_thrd != null) {
										Log.d("mbp",
												"Thread: "
														+ Thread.currentThread()
														.getId()
														+ ": some streaming activity is going on.. stop them now..");
										stopAllThread();
									}

									if (ACTIVITY_HAS_STOPPED == false)
									{
										setupVideoThreads(selected_channel);
									}

								}
							});


							break;
						case ConnectToNetwork.MSG_CONNECT_TO_NW_FAILED:
							// /Scan again
							WifiScan ws = new WifiScan(ViewCameraActivity.this,
									new MiniWifiScanUpdater());
							ws.setSilence(true);
							ws.execute("Scan now");

							break;
						}
						return false;
					}
				};

				ConnectToNetwork connect_task = new ConnectToNetwork(
						ViewCameraActivity.this, new Handler(h));
				connect_task.dontRemoveFailedConnection(true);
				connect_task.setSilence(true);
				connect_task.setIgnoreBSSID(true);
				boolean foundExisting = false;
				for (WifiConfiguration wc : wcs) {
					if ((wc.SSID != null)
							&& wc.SSID.equalsIgnoreCase(check_SSID_w_quote)) {
						// This task will make sure the app is connected to the
						// camera.
						// At the end it will send MSG_CONNECT_TO_NW_DONE
						connect_task.execute(wc);
						foundExisting = true;
						break;
					}
				}
				if (!foundExisting) {
					// popup dialog
					showDialog(DIALOG_WIFI_CANT_RECONNECT);

				}
			} 
			else /* not found the SSID */
			{

				// /Scan again
				WifiScan ws = new WifiScan(ViewCameraActivity.this,new MiniWifiScanUpdater());
				ws.setSilence(true);
				ws.execute("Scan now");
			}

		}
	}


	/**
	 * @author phung
	 *  This is to reconnect just 1 camera after scanning
        If no camera found -- rescan again
		 until we found one or -- sbdy press
		 stop
	 */
	private class OneCameraScanner implements ICameraScanner
	{

		@Override
		public void updateScanResult(ScanProfile[] results, int status,int index) {

			if (results != null
					&& results.length == 1) {
				ScanProfile cp = results[0];
				// update the ip address
				CamProfile seletectProfile = selected_channel.getCamProfile();

				if (seletectProfile.get_MAC()
						.equalsIgnoreCase(
								results[0]
										.get_MAC())) {

					// Update the new IP
					seletectProfile.setInetAddr(results[0].get_inetAddress());
					seletectProfile.setPort(results[0].get_port());
					seletectProfile.setInLocal(true);

					// start connecting now..
					setupVideoThreads(selected_channel);
					return;
				}
			}

			// If the camera is not found -- send
			// the error message to trigger scanning
			// again .
			Handler dummy = new Handler(ViewCameraActivity.this);
			dummy.dispatchMessage(Message
					.obtain(dummy,
							VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY));

		}
	}	


	private class VideoTimeoutTask extends TimerTask
	{
		public void run() 
		{
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Log.d("mbp", "5 min timeout is up ...start counting down ");

					exitSubfunction();

					RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
					if (refreshRoot != null) {
						refreshRoot.setVisibility(View.VISIBLE);
					}

					final CountDownTimer ctimer = new CountDownTimer(10,
							new ITimerUpdater() {

						@Override
						public void updateCurrentCount(final int count) 
						{
							runOnUiThread(new Runnable() {
								public void run() {
									TextView txt = (TextView) findViewById(R.id.textStopping);
									if (txt != null) {
										String msg = String
												.format("%s %d",
														getString(R.string.stopping_in),
														count);
										txt.setText(msg);
									}
								}
							});
						}

						@Override
						public void timeUp() {
							// Time is realy up --
							ViewCameraActivity.this
							.runOnUiThread(new Runnable() {
								public void run() {
									if (_streamer != null)
									{
										_streamer.stop(Streamer.STOP_REASON_TIMEOUT);
										if (streamer_thrd != null) 
										{
											streamer_thrd
											.interrupt();
										}
									}

									if (ViewCameraActivity.this.remoteVideoTimer != null)
										ViewCameraActivity.this.remoteVideoTimer
										.cancel();

									RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
									if (refreshRoot != null) {
										refreshRoot
										.setVisibility(View.INVISIBLE);
									}
								}
							});

						}

						@Override
						public void timerKick() {
							// Timer is kicked before timeout
							// create a new remoteVideoTimer ..
							if (ViewCameraActivity.this.remoteVideoTimer != null)
								ViewCameraActivity.this.remoteVideoTimer.cancel();

							ViewCameraActivity.this.remoteVideoTimer = new Timer();
							ViewCameraActivity.this.remoteVideoTimer.schedule(
									new VideoTimeoutTask(),
									VIDEO_TIMEOUT);

							runOnUiThread(new Runnable() {
								public void run() {
									// hide the refresh screen
									exitSubfunction();
								}
							});

						}
					});

					if (refreshRoot != null) {
						// setup button now..
						Button refresh = (Button) refreshRoot
								.findViewById(R.id.refreshBtn);
						if (refresh != null) {
							refresh.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									ctimer.stop();

								}
							});

							new Thread(ctimer).start();
						}

					}
				}
			});
		}
	}


	private class GetStreamModeCb implements Handler.Callback
	{

		private boolean shouldIgnoreErrorAndRetry;

		public GetStreamModeCb(boolean retry) {
			shouldIgnoreErrorAndRetry = retry;
		}

		@Override
		public boolean handleMessage(Message msg) {

			switch (msg.what) {
			case GetRemoteStreamModeTask.MSG_GET_MODE_TASK_SUCCESS:
				Integer currentMode = (Integer) msg.obj;
				if (selected_channel == null) {
					// TODO: error handling here
					return false;
				}
				
				if (currentMode.intValue() == GetRemoteStreamModeTask.STREAM_MODE_UNKNOWN) {
					try {

						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
						if (remoteVideoTimer != null) {
							Log.d("mbp", "cancel current remoteVideoTimer");
							remoteVideoTimer.cancel();
							remoteVideoTimer = null;
						}

						int err = msg.arg1;
						switch (err) {
						default:
							showDialog(DIALOG_BMS_GET_STREAM_MODE_ERROR);
							break;
						}
					} catch (Exception e) {
					}
					selected_channel.getCamProfile().setRemoteCommMode(StreamerFactory.STREAM_MODE_HTTP_LOCAL);

				} 
				else if (currentMode.intValue() == GetRemoteStreamModeTask.STREAM_MODE_UDT) 
				{
					tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "Get Remote Stream Mode Success",
							"Stream Mode STUN", null);
					// UDT flow starts here--
					String videoCodec = selected_channel.getCamProfile().getCodec();
					// if codec is H264, stream through Wowza server
					if (videoCodec.equalsIgnoreCase(CamProfile.CODEC_H264))
					{
						requestViewCamViaServer(selected_channel,
								new Handler(ViewCameraActivity.this));
					}
					else
					{
						requestViewCamViaUDT(selected_channel, 
								new Handler(ViewCameraActivity.this));
					}

				}
				else if (currentMode.intValue() == GetRemoteStreamModeTask.STREAM_MODE_RELAY)
				{
					tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "Get Remote Stream Mode Success",
							"Stream Mode RELAY", null);
					Log.d("mbp", "Camera is behind symmetric NAT...switch to relay stream...");
					requestViewCamViaRelay(selected_channel, 
							new Handler(ViewCameraActivity.this));
					
				}
				else // for now both UPNP & manual are the same--
				{
					tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "Get Remote Stream Mode Success",
							"Stream Mode UPNP", null);
					selected_channel.getCamProfile().setRemoteCommMode(
							StreamerFactory.STREAM_MODE_HTTP_REMOTE);

					ViewUpnpCamRequestTask viewUpnpTask = new ViewUpnpCamRequestTask(
									new Handler(ViewCameraActivity.this),
									ViewCameraActivity.this);
					
					selected_channel.setCurrentViewSession(CamChannel.REMOTE_HTTP_VIEW);
					
					SharedPreferences settings = getSharedPreferences(
							PublicDefine.PREFS_NAME, 0);
					String saved_token = settings.getString(
							PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
					
					String clientType = "android";
					viewUpnpTask.execute(selected_channel.getCamProfile().get_MAC(),
							saved_token, clientType);

					showDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);

				}

				break;
			case GetRemoteStreamModeTask.MSG_GET_MODE_TASK_FAILED:
				
				tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "Get Remote Stream Mode Failed",
						"Get Remote Stream Mode Failed", null);
				
				if (shouldIgnoreErrorAndRetry) 
				{
					/*
					 * 20120817: phung: Hack- Trick this activity into thinking
					 * that the video stream has just stopped again IF -
					 * shouldIgnoreErrorAndRetry = TRUE
					 */
					Handler dummy = new Handler(ViewCameraActivity.this);
					dummy.dispatchMessage(Message
							.obtain(dummy,
									VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY));

				} 
				else
				{
					try {
						dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
						if (remoteVideoTimer != null) {
							Log.d("mbp", "cancel current remoteVideoTimer");
							remoteVideoTimer.cancel();
							remoteVideoTimer = null;
						}

						/*
						 * 20130201: hoang: issue 1260 turn off screen when
						 * finish view request
						 */
						if (wl != null && wl.isHeld()) {
							wl.release();
							wl = null;
							Log.d("mbp", "MSG_GET_MODE_TASK_FAILED - release WakeLock");
						}

						int err = msg.arg1;
						switch (err) {
						default:
							showDialog(DIALOG_BMS_GET_STREAM_MODE_ERROR);
							break;
						}
					} catch (Exception ie) {
					}
				}
				break;
			}

			return false;
		}
	}

	

	// THis task is run every 5 sec to update frame rate -
	private class FrameRateTimeoutTask extends TimerTask {
		public void run() {

			ViewCameraActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {

					// get total_frame
					double rate = total_frame / 5; // 5sec

					// display the rate
					final TextView frate = (TextView) findViewById(R.id.textFrameRate);
					if (frate != null) {
						final String s = String.format("%.1f fps", rate);
						frate.post(new Runnable() {
							@Override
							public void run() {
								frate.setVisibility(View.VISIBLE);
								frate.setText(s);
							}
						});
					}

					// clear total_frame
					synchronized (ViewCameraActivity.this) {
						total_frame = 0;
					}

					ImageView direction_pad = (ImageView) findViewById(R.id.directionPad);
					TextView temp_status = (TextView) findViewById(R.id.textNwSlowOrTempAlert);
					ImageView nwSlowAlarm = (ImageView) findViewById(R.id.nwSlowAlarm);
					RelativeLayout statusBar = (RelativeLayout) findViewById(R.id.statusBar);

					// TextView disabledText = (TextView)
					// findViewById(R.id.textDisabled);

					// if (direction_pad != null)
					// {
					// //if (rate < 3 && direction_pad.isClickable())
					// if (rate < 3)
					// {
					// Log.d("mbp", "Low frame low frame.. disable controls");
					// //Disable pan & Tilt -- show some snail image..
					// //direction_pad.setImageResource(R.drawable.circle_buttons1_2_d);
					// direction_pad.setImageResource(R.drawable.circle_buttons1_2);
					// direction_pad.setOnTouchListener(joystickListener);
					// direction_pad.setClickable(true);
					//
					//
					// if (statusBar != null)
					// {
					// statusBar.setVisibility(View.VISIBLE);
					// statusBar.setBackgroundColor(Color.BLUE);
					// }
					// if (nwSlowAlarm!= null)
					// {
					// nwSlowAlarm.setVisibility(View.VISIBLE);
					// nwSlowAlarm.setVisibility(View.VISIBLE);
					//
					// }
					// if (temp_status != null)
					// {
					// temp_status.setVisibility(View.VISIBLE);
					// temp_status.setText(getString(R.string.pan_tilt_disabled_because_of_network_speed));
					// }
					//
					// }
					// //else if ( rate >= 3 && direction_pad.isClickable() ==
					// false)
					// else if ( rate >= 3)
					// {
					// Log.d("mbp", "good frame good frame.. enable controls");
					// //Enable pan & tilt
					// direction_pad.setImageResource(R.drawable.circle_buttons1_2);
					// direction_pad.setOnTouchListener(joystickListener);
					// direction_pad.setClickable(true);
					//
					// if (statusBar != null)
					// {
					// statusBar.setVisibility(View.INVISIBLE);
					// }
					// if (nwSlowAlarm!= null)
					// {
					// nwSlowAlarm.setVisibility(View.INVISIBLE);
					//
					// }
					// if (temp_status != null)
					// {
					// temp_status.setVisibility(View.INVISIBLE);
					// }
					// }
					// }

				}
			});
		}
	}


	private class VideoOutOfRangeReminder implements Runnable {

		private boolean running;

		public VideoOutOfRangeReminder() {
			running = true;
		}

		@Override
		public void run() {
			/* Play beep: preparing */
			MediaPlayer mMediaPlayer = new MediaPlayer();
			String uri = "android.resource://" + getPackageName() + "/"
					+ R.raw.beep;
			try {
				mMediaPlayer.setDataSource(ViewCameraActivity.this, Uri.parse(uri));
			} catch (IOException e) {
				e.printStackTrace();
			}

			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
			mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					// mp.stop();
				}
			});

			try {
				mMediaPlayer.prepare();
			} catch (IllegalStateException e1) {
				mMediaPlayer = null;
				e1.printStackTrace();
			} catch (IOException e1) {
				mMediaPlayer = null;
				e1.printStackTrace();
			}

			/* turn on the screen only once when we start.. */
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			if (!pm.isScreenOn()) {
				wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP
						| PowerManager.ON_AFTER_RELEASE,
						"TURN ON Because of error");
				wl.setReferenceCounted(false);
				wl.acquire(10000);
			}

			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
					+ ":Beeping in local router mode start");
			while (running) {

				ViewCameraActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						displayBG(true);

						try {
							showDialog(DIALOG_VIDEO_STOPPED_UNEXPECTEDLY);
						} catch (Exception ie) {

						}
					}
				});

				try {
					Thread.sleep(5500);
				} catch (InterruptedException e) {
				}

				if (ACTIVITY_HAS_STOPPED == true) {
					Log.d("mbp",
							"Stop Beeping in local coz activity is stopped.");
					break;
				}

				// 20120511_: move the beeping to after 5 sec
				if (mMediaPlayer != null) 
				{
					if (mMediaPlayer.isPlaying())
						mMediaPlayer.stop();


					if  (muteAudioForCall == false)
					{
						mMediaPlayer.start();
					}
				}


			} // while running
			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
					+ ":Beeping in local router mode stop");

			if ((mMediaPlayer != null) && mMediaPlayer.isPlaying())
			{
				mMediaPlayer.stop();
			}

			runOnUiThread(new Runnable() {

				@Override
				public void run() {

					try {
						removeDialog(DIALOG_VIDEO_STOPPED_UNEXPECTEDLY);
					} catch (Exception e) {
					}
				}
			});

		}

		public void stop() {
			running = false;
		}

	}

	



	private class ScreenTOBroadcastReceiver extends BroadcastReceiver 
	{
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				// SCREEN is turned OFF

				if (selected_channel != null) {
					switch (selected_channel.getCamProfile()
							.getRemoteCommMode()) {
							case StreamerFactory.STREAM_MODE_HTTP_LOCAL:
							case StreamerFactory.STREAM_MODE_HTTP_REMOTE:
								/* kill the current audio player */
								if (pcmPlayer_thrd != null) {
									_pcmPlayer.stop();
									boolean retry = true;
									while (retry) {
										try {
											pcmPlayer_thrd.join(1000);

											retry = false;
										} catch (InterruptedException e) {
										}
									}
									pcmPlayer_thrd = null;
								}
								// Log.d("mbp","Switching to audio only - start vox");
								switchToAVStreamOnHTTP(false);

								// pass the newly created streamer to Service
								startAVSService();

								break;

							case StreamerFactory.STREAM_MODE_UDT:
							case StreamerFactory.STREAM_MODE_RELAY:
								// 20120625 dont create a new stream -
								// switchToAVStreamOnUDT(false);
								// resuse th current one.

								if (_streamer != null) {
									Log.d("mbp",
											"ScreenTOBroadcastReceiver:remove the video update/temp");
									// simply remove the video update/temp update
									_streamer.removeVideoSink(ViewCameraActivity.this);
									_streamer.setTemperatureUpdater(null);
									startAVSService();
								}

								break;

					}
				} else {
					Log.d("mbp",
							"ScreenTOBroadcastReceiver: SCREEN OFF selected_channel= null-- return ");
				}


			}
			else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON))
			{

				if (selected_channel != null) {
					switch (selected_channel.getCamProfile()
							.getRemoteCommMode()) {
							case StreamerFactory.STREAM_MODE_HTTP_LOCAL:
							case StreamerFactory.STREAM_MODE_HTTP_REMOTE:

								/* create a new PCM player here */
								if ((_pcmPlayer == null) || (pcmPlayer_thrd == null)) {
									_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO);

									pcmPlayer_thrd = new Thread(_pcmPlayer, "PCMPlayer");
									pcmPlayer_thrd.start();
								}
								switchToAVStreamOnHTTP(true);
								stopAVSService();

								break;
							case StreamerFactory.STREAM_MODE_UDT:

								// switchToAVStreamOnUDT(true);

								if (_streamer != null) {
									stopAVSService();
									Log.d("mbp",
											"ScreenTOBroadcastReceiver:  add back the video update/temp");
									// simply add back the video update/temp update
									_streamer.addVideoSink(ViewCameraActivity.this);
									_streamer.setTemperatureUpdater(ViewCameraActivity.this);

								}
								break;

					}
				} else {
					Log.d("mbp",
							"ScreenTOBroadcastReceiver: SCREEN on  selected_channel= null-- return ");
				}

				/*
				 * after the screen turns on again, the direction pad is still
				 * working but it does not appear when touched. -- Restart the
				 * animation here seems to solve the problem
				 */
				ViewCameraActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						/*
						 * this call will also turn on USB polling if it's not
						 * started before
						 */
						tryToGoToFullScreen();
						/*
						 * Screen is on now -- reset USB counting if (usbPoll!=
						 * null && usbPoll.isPolling()) { usbPoll.reset(); }
						 */
					}
				});

			}

		}
	};



	/* mainly to save the ip addresses */
	private synchronized void save_session_data(CamChannel myChannel) 
	{

		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, true);
		editor.commit();

		SetupData savedData =  new SetupData(); 

		try {
			savedData.restore_session_data(getExternalFilesDir(null));
		} catch (StorageException e) {
			e.printStackTrace();
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
		}

		CamProfile [] restored_profile = savedData.get_CamProfiles();

		CamProfile cp = myChannel.getCamProfile();
		for (int i=0 ; i<restored_profile.length; i++ )
		{
			if  ( (restored_profile[i] != null) &&
					cp.get_MAC().equalsIgnoreCase(restored_profile[i].get_MAC())
					)
			{
				//update profile...
				restored_profile[i] = cp;
			}
		}


		savedData.set_CamProfiles(restored_profile);
		savedData.save_session_data(getExternalFilesDir(null));


		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
		editor.commit();
	}


	private class UpdateAlertUICallback implements IUpdateAlertCallBack
	{

		@Override
		public void pre_update()
		{
			showDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
		}

		@Override
		public void update_alert_success() {
			save_session_data(selected_channel);
			try {
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {

			}
		}

		@Override
		public void update_alert_failed(int alertType, boolean enableOrDisable) {
			try {
				dismissDialog(DIALOG_BMS_CONNECTION_IN_PROGRESS);
			} catch (Exception e) {
			}

			boolean isAlertEnabled;
			switch (alertType) 
			{
			case VoxMessage.ALERT_TYPE_SOUND:
				isAlertEnabled = selected_channel.getCamProfile().isSoundAlertEnabled();
				selected_channel.getCamProfile().setSoundAlertEnabled(!isAlertEnabled);
				break;
			case VoxMessage.ALERT_TYPE_TEMP_HI:
				isAlertEnabled = selected_channel.getCamProfile().isTempHiAlertEnabled();
				selected_channel.getCamProfile().setTempHiAlertEnabled(!isAlertEnabled);
				break;
			case VoxMessage.ALERT_TYPE_TEMP_LO:
				isAlertEnabled = selected_channel.getCamProfile().isTempLoAlertEnabled();
				selected_channel.getCamProfile().setTempLoAlertEnabled(!isAlertEnabled);
				break;
			default:// Unknown
			break;
			}

			try {
				showDialog(DIALOG_BMS_UPDATE_FAILED_TRY_AGAIN);
			} catch (Exception e) {
			}
			onAlarmSettings();

		}
	}



	private PhoneStateListener mPhoneListener = new PhoneStateListener() 
	{

		public void onCallStateChanged(int state, String incomingNumber) {
			try {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					muteAudioForCall = true; 
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					muteAudioForCall = false; 
					break;
				default:
				}
			} catch (Exception e) {
				Log.i("Exception", "PhoneStateListener() e = " + e);
			}
		}
	};


	/*************************************************************************************************/




}






