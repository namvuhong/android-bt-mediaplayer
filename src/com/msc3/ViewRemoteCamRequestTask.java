package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.registration.LoginOrRegistrationActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;



/**
 * @author phung
 * 
 * 
 * Response: 
HTTP/1.1 200 OK
Contents:
cam_url=http://<ip_address>:<cam_port>/?action=appletvastream&
 remote_session=<session authentication key 32 bytes HEX string><CR><LF>

HTTP/1.1 202 ACCEPTED (but not processed due to errors)
Contents:
Error=251<CR><LF>Description=Invalid MAC Address/invalid format
Error=201<CR><LF>Description=User email is not registered
Error=911<CR><LF>Description=Web server under maintenance

 * Result: 
 *  - Code : OK or not OK, any errors
 *  - Ip:port
 *  - session_key:
 */
public class ViewRemoteCamRequestTask extends AsyncTask<String, String, BabyMonitorAuthentication> {

	private String usrName;
	private String usrPass;
	private String macAddress;
	private String camName;


	private static final String CAM_IP="Camera_IP=";
	private static final String CAM_PORT="<br>Camera_Port=";
	private static final String SS_KEY="SessionAutenticationKey=";
	private static final int  SS_KEY_LEN=64;
	
	private static final int VIEW_CAM_SUCCESS = 0x1;
	private static final int VIEW_CAM_FAILED_UNKNOWN = 0x2;
	private static final int VIEW_CAM_FAILED_SERVER_DOWN = 0x11;
	
	public static final int MSG_VIEW_CAM_SUCCESS = 0xDE000001;
	public static final int MSG_VIEW_CAM_FALIED = 0xDE000002;
	public static final int MSG_VIEW_CAM_CANCELED = 0xDE00000A;
	public static final int MSG_VIEW_CAM_SWITCH_TO_RELAY = 0xDE00000B;
	
	
	protected Handler mHandler;
	protected SSLContext ssl_context ;
	
	private int server_err_code; 
	
	
	/* 20120509 - Auto-Relink camera in remote - 
	 *  retryUntilSuccess - set to True - this Task will run forever until : 
	 *   								 1. it get the correct result
	 *                                   2. it is canceled
	 *                    -  set to False - run once - any error just return
	 */
	protected boolean retryUntilSuccess; 
	
	private Tracker tracker;
	private EasyTracker easyTracker;
	
	
	public ViewRemoteCamRequestTask(Handler h, Context mContext) {
		mHandler = h;
		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(mContext);
		retryUntilSuccess = false;
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(mContext);
		tracker = easyTracker.getTracker();
	}

	
	public boolean isRetryUntilSuccess() {
		return retryUntilSuccess;
	}

	public void setRetryUntilSuccess(boolean retryUntilSuccess) {
		this.retryUntilSuccess = retryUntilSuccess;
	}
	

	@Override
	protected BabyMonitorAuthentication doInBackground(String... params) {

		usrName = params[0];
		usrPass = params[1];
		macAddress = params[2];

		macAddress= PublicDefine.strip_colon_from_mac(macAddress).toUpperCase();
		//BabyMonitorAuthentication authentication = null;
		BabyMonitorHTTPAuthentication authentication = null;

		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.VIEW_CAM_CMD+ PublicDefine.VIEW_CAM_PARAM_1 +
				usrName + PublicDefine.VIEW_CAM_PARAM_2 +  macAddress;

		Log.d("mbp","View remote cam request");
		
		int localRetry = -1; 
		
		do 
		{
			URL url = null;
			HttpsURLConnection conn = null;
			DataInputStream inputStream = null;
			String response = null;
			int respondeCode = -1;
			int ret = -1;

			String usr_pass =  String.format("%s:%s", usrName, usrPass);

			try {
				url = new URL(http_cmd );
				conn = (HttpsURLConnection)url.openConnection();

				if (ssl_context!=null)
				{
					conn.setSSLSocketFactory(ssl_context.getSocketFactory());
				}

				conn.addRequestProperty("Authorization", "Basic " +
						Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn.setConnectTimeout(15000);
				conn.setReadTimeout(10000);


				respondeCode = conn.getResponseCode();
				if (respondeCode == HttpURLConnection.HTTP_OK )
				{
					inputStream = new DataInputStream(
							new BufferedInputStream(conn.getInputStream(),4*1024));

					ret =VIEW_CAM_SUCCESS;

					response = inputStream.readLine();
					

					//Sample
					//Camera_IP=219.75.112.7<br> Camera_Port=80<br> SessionAutenticationKey=
					//5DC572A5E8005408E3CEA028BEBF63B9BF2999FD45CFD848AFCA0EE0A68ED7EE
					if (response != null && response.startsWith(CAM_IP))
					{
						String response_part = response.substring(CAM_IP.length());

						String ip = response_part.substring(0,response_part.indexOf("<br>"));

						int port_idx = response_part.indexOf(CAM_PORT);
						response_part = response_part.substring(port_idx+ CAM_PORT.length());//80<br> Session.. 

						port_idx = response_part.indexOf("<br>");
						String port = response_part.substring(0,port_idx);



						int ssk_index = response_part.indexOf(SS_KEY);
						String ss_key = response_part.substring(ssk_index+SS_KEY.length(), 
								ssk_index+SS_KEY.length() + SS_KEY_LEN );


						//Log.d("mbp","cam ip:" + ip + " camport:" + port + "\n SSKEY:" + ss_key);
						//authentication = new BabyMonitorAuthentication(ip, port, ss_key);
						authentication = new BabyMonitorHTTPAuthentication(ip, port, ss_key, macAddress);
						String streamUrl = String.format("http://%s:%s%s%s%s",
								ip, port, PublicDefine.GET_FLV_STREAM_CMD,
								PublicDefine.GET_FLV_STREAM_PARAM_1, ss_key);
						authentication.setStreamUrl(streamUrl);

					}
					
				}
				else
				{
					server_err_code = respondeCode;
				}

			} 
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				ret =VIEW_CAM_FAILED_SERVER_DOWN;
			} 
			catch (SocketTimeoutException se)
			{
				
				/* Android Doc:
				 * This exception is thrown when a timeout expired
				 *  on a socket "read" or "accept" operation.
				 *  
				 *  So if read failed, we will try again until we get read OK or a different Exception 
				 *  If server down, we should get SocketException .. 
				 */
				
				
				
				Log.d("mbp", "ViewRemoteCamRequest:" +
						"SocketTimeoutException: " + se.getLocalizedMessage() +
						" while sending request");
				se.printStackTrace();
				//localRetry = 1; 
				//if (read failed) we try again
				
				
				
			}
			catch (SocketException se) 
			{
				/* 
				 * This SocketException may be thrown during socket creation or setting options, 
				 * and is the superclass of all other socket related exceptions.
				 */
				se.printStackTrace();
				ret =VIEW_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e) 
			{
				e.printStackTrace();
				ret =VIEW_CAM_FAILED_SERVER_DOWN;
			}

			if (isCancelled())
			{
				Log.d("mbp","viewReq is cancelled-- return NULL");
				break;
			}
			
			if (ret == VIEW_CAM_SUCCESS)
			{
				break; 
			}
			else
			{
				Log.d("mbp","some error :" + server_err_code +
						" in viewReq retry?: " + (retryUntilSuccess || (localRetry >=0)) ); 
			}

		
		}
		while(retryUntilSuccess || (localRetry-- >=0));
		
		
		return authentication;
	}


	protected void onCancelled ()
	{
		Log.d("mbp","viewReq get cancelled");
		mHandler.dispatchMessage(Message.obtain(mHandler, MSG_VIEW_CAM_CANCELED));
	}
	
	public static final String GA_VIEW_CAMERA_CATEGORY = "View Remote Camera";
	/* UI thread */
	protected void onPostExecute(BabyMonitorAuthentication result)
	{
		Message m; 
		if (result != null)
		{
			m = Message.obtain(mHandler, MSG_VIEW_CAM_SUCCESS, result);
			
		}
		else
		{
			tracker.sendEvent(GA_VIEW_CAMERA_CATEGORY, "View Remote Cam Request Failed",
					"UPNP Cam Info Access Failed", null);
			m = Message.obtain(mHandler, MSG_VIEW_CAM_FALIED,server_err_code,server_err_code);
		}
		mHandler.dispatchMessage(m);
	}
}




