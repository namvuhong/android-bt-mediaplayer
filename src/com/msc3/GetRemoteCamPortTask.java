package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import com.msc3.registration.LoginOrRegistrationActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;



/**
 * @author phung
 * 
 * 
 * Response: 
HTTP/1.1 200 OK
Contents:
total_ports=3<br>video_port=47429<br>audio_port=59696<br>vox_port=0

HTTP/1.1 202 ACCEPTED (but not processed due to errors)
Contents:
Error=251<CR><LF>Description=Invalid MAC Address/invalid format
Error=201<CR><LF>Description=User email is not registered
Error=911<CR><LF>Description=Web server under maintenance

 * Result: 
 *  - Code : OK or not OK, any errors
 *  - Ip:port
 *  - session_key:
 */
public class GetRemoteCamPortTask extends AsyncTask<String, String, List<RemotePort>> {

	private String macAddress;
	private String userToken;
	private static final String TOTAL_PORTS="total_ports=";
	private static final String BR_TAG="<br>";

	private static final int GET_PORT_SUCCESS = 0x1;
	public static final int GET_PORT_FAILED_UNKNOWN_RESPONSE = 0x2;
	public static final int GET_PORT_FAILED_SERVER_DOWN = 0x11;
	public static final int GET_PORT_FAILED_SERVER_CODE = 0x12;

	public static final int MSG_GET_PORT_TASK_SUCCESS = 0xDE000003;
	public static final int MSG_GET_PORT_TASK_FALIED_REPONSE_ERR = 0xDE000004;
	public static final int MSG_GET_PORT_TASK_FALIED_SERVER_UNREACHABLE = 0xDE000006;


	private Handler mHandler;
	private SSLContext ssl_context ;
	private int server_err_code; 
	private String server_err_str; 
	ArrayList<RemotePort> results; 

	/* 20120509 - Auto-Relink camera in remote - 
	 *  retryUntilSuccess - set to True - this Task will run forever until : 
	 *   								 1. it get the correct result
	 *                                   2. it is canceled
	 *                    -  set to False - run once - any error just return
	 */
	private boolean retryUntilSuccess;
	private String usrPass;
	private String usrName; 
	
	


	public GetRemoteCamPortTask(Handler h, Context mContext) {
		mHandler = h;
		ssl_context = LoginOrRegistrationActivity.prepare_SSL_context(mContext);
		results = new ArrayList<RemotePort>();
		
		server_err_str = null;
		retryUntilSuccess = false; 
	}

	public boolean isRetryUntilSuccess() {
		return retryUntilSuccess;
	}

	public void setRetryUntilSuccess(boolean retryUntilSuccess) {
		this.retryUntilSuccess = retryUntilSuccess;
	}


	@Override
	protected ArrayList<RemotePort> doInBackground(String... params) {

		macAddress = params[0];
		macAddress= PublicDefine.strip_colon_from_mac(macAddress).toUpperCase();
		userToken = params[1];


		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.GET_PORT_CMD+ PublicDefine.GET_PORT_PARAM_1 +
				macAddress;

		int localRetry = -1; 
		
		do 
		{
			URL url = null;
			HttpsURLConnection conn = null;
			DataInputStream inputStream = null;
			String response = null;
			int respondeCode = -1;
			int ret = -1;

			String usr_pass =  String.format("%s:%s", usrName, usrPass);
			try {
				url = new URL(http_cmd );
				conn = (HttpsURLConnection)url.openConnection();

				if (ssl_context!=null)
				{
					conn.setSSLSocketFactory(ssl_context.getSocketFactory());
				}

				conn.addRequestProperty("Authorization", "Basic " +
						Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn.setConnectTimeout(15000);
				conn.setReadTimeout(10000);


				respondeCode = conn.getResponseCode();
				if (respondeCode == HttpURLConnection.HTTP_OK )
				{

					inputStream = new DataInputStream(
							new BufferedInputStream(conn.getInputStream(),4*1024));

					server_err_code =GET_PORT_SUCCESS;

					response = inputStream.readLine();
					Log.d("mbp","getport:" + response);
					//Sample
					//total_ports=3<br>video_port=47429<br>audio_port=59696<br>vox_port=0
					if (response != null && response.startsWith(TOTAL_PORTS))
					{

						int end_idx; 
						end_idx = response.indexOf(BR_TAG);
						String total_prt_str = response.substring(TOTAL_PORTS.length(),end_idx);
						int total_port = 0, i = 0 ; 
						total_port = Integer.parseInt(total_prt_str);

						//trim off the first token "total... <br>"
						response = response.substring(end_idx+BR_TAG.length());

						String port, port_name, port_num_str;
						RemotePort new_port = null;
						while (i<total_port)
						{
							end_idx = response.indexOf(BR_TAG);
							if ( end_idx < 0)
							{
								//there is no <br> at the end of the last token
								port =  response;
							}
							else
							{
								port =  response.substring(0,end_idx);
							}
							port_name = port.substring(0, port.indexOf("="));
							port_num_str = port.substring(port.indexOf("=") +1);

							try
							{
								new_port = new RemotePort(port_name, Integer.parseInt(port_num_str));

								//insert this port to result list 
								results.add(RemotePort.getIndex(port_name),new_port);


							}
							catch (UndefinedPortException ude)
							{
								//just skip this port, dont store it
								ude.printStackTrace();
							}
							catch (NumberFormatException nfe )
							{
								Log.d("mbp","get NumberFormatException .. sth is wrong with the response ");
								server_err_code =GET_PORT_FAILED_UNKNOWN_RESPONSE;
								server_err_str = port; 
								break;
							}

							i++;
							//trim off this token "...=port#<br>"
							response = response.substring(end_idx+BR_TAG.length());
						} // while..
					}
				}
				else
				{
					server_err_code = GET_PORT_FAILED_SERVER_CODE;
					server_err_str = "Server Error Code " + respondeCode;
				}


			}
			catch (NumberFormatException nfe )
			{
				nfe.printStackTrace();
				server_err_code =GET_PORT_FAILED_UNKNOWN_RESPONSE;
				server_err_str = response;
			}
			catch (MalformedURLException e) {
				e.printStackTrace();
				server_err_code =GET_PORT_FAILED_SERVER_DOWN;
			}
			catch (SocketTimeoutException se)
			{
				
				/* Android Doc:
				 * This exception is thrown when a timeout expired
				 *  on a socket "read" or "accept" operation.
				 *  
				 *  So if read failed, we will try again until we get read OK or a different Exception 
				 *  If server down, we should get SocketException .. 
				 */
				
				
				
				Log.d("mbp", "SocketTimeoutException:  " + se.getLocalizedMessage());
				se.printStackTrace();
				//localRetry = 1; 
				//if (read failed) we try again
				
				
				
			}
			catch (SocketException se) 
			{
				/* 
				 * This SocketException may be thrown during socket creation or setting options, 
				 * and is the superclass of all other socket related exceptions.
				 */
				se.printStackTrace();
				server_err_code =GET_PORT_FAILED_SERVER_DOWN;
			}
			
			catch (IOException e) {
				e.printStackTrace();
				server_err_code =GET_PORT_FAILED_SERVER_DOWN;
			}

			if (isCancelled())
			{
				Log.d("mbp","GetPort is cancelled-- return NULL");
				break;  
			}

			if (server_err_code == GET_PORT_SUCCESS)
			{
				break; 
			}
			else 
			{

				results.clear();
				Log.d("mbp","Some error in getPort -- retry?" +  (retryUntilSuccess || (localRetry >=0)));
			}
			
		} while (retryUntilSuccess || (localRetry-- >=0));
			
		return results;
	}
	
	protected void onCancelled ()
	{
		Log.d("mbp","GetPort get cancelled");
	}

	/* UI thread */
	protected void onPostExecute(List<RemotePort> result)
	{
		Message m = null; 
		if ((result != null) && result.size()>0 )
		{
			m = Message.obtain(mHandler, MSG_GET_PORT_TASK_SUCCESS, result);
		}
		else if (server_err_code ==GET_PORT_FAILED_UNKNOWN_RESPONSE  )
		{
			m = Message.obtain(mHandler, MSG_GET_PORT_TASK_FALIED_REPONSE_ERR ,server_err_code,server_err_code, server_err_str);
		}
		else if (server_err_code ==GET_PORT_FAILED_SERVER_DOWN  )
		{
			m = Message.obtain(mHandler, MSG_GET_PORT_TASK_FALIED_SERVER_UNREACHABLE ,server_err_code,server_err_code);
		}
		else  // GET_PORT_FAILED_SERVER_CODE
		{
			m = Message.obtain(mHandler, MSG_GET_PORT_TASK_FALIED_REPONSE_ERR ,server_err_code,server_err_code, server_err_str);
		}
		
		
		
		mHandler.dispatchMessage(m);
	}
}




