package com.msc3;

import java.io.IOException;
import java.lang.reflect.Method;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.blinkhd.R;
import com.blinkhd.gcm.GcmIntentService;
import com.blinkhd.playback.EventManagerActivity;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;

/**
 * @author phung
 *
 *  IMPORTANT: BOTH MBPxxx and Non-MBP phone share this class. 
 *  Use it carefullly.
 *
 */
public class VoxActivity extends Activity  {

	public static final String TRIGGER_MAC_EXTRA = "string_DeviceMac";
	public static final String TRIGGER_IP_EXTRA = "string_DeviceIp";
	public static final String TRIGGER_TYPE = "int_VoxType";
	public static final String TRIGGER_EVENT_CODE_EXTRA = "string_EventCode";
	public static final String NOTIFICATION_CLEARED = "bool_NotificationCleared";


	private PowerManager.WakeLock wl;

	private String trigger_name;
	private String trigger_mac;
	private int trigger_VoxType;
	private String eventTimeCode;
	private BroadcastReceiver usr_present_br; 

	private MediaPlayer mMediaPlayer;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vox_main);

		// get bundle
		Bundle extras = getIntent().getExtras();
		trigger_mac = extras.getString(VoxActivity.TRIGGER_MAC_EXTRA);
		this.trigger_VoxType = extras.getInt(VoxActivity.TRIGGER_TYPE);

		if(this.trigger_VoxType != 0 && this.trigger_VoxType == VoxMessage.VOX_TYPE_MOTION_ON)
		{
			this.eventTimeCode = extras.getString(VoxActivity.TRIGGER_EVENT_CODE_EXTRA);
		}
		/*20121119: phung: NON_MBP phones.. Simply clear notifications and Return*/ 
		NotificationManager notificationManager = (NotificationManager)
				getSystemService(Context.NOTIFICATION_SERVICE);

		for (int i =0; i< GcmIntentService.PUSH_IDs.length; i++)
		{
			notificationManager.cancel(GcmIntentService.PUSH_IDs[i]);
		}

		/* turn on the screen */
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = null; 
		if (!pm.isScreenOn()) {
			wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP, "My Tag");
			wl.acquire();
		}


	}

	protected void onStart() {
		super.onStart();

		String phonemodel = android.os.Build.MODEL;
		// register if it's not iHome Phone
		if(phonemodel.equals(PublicDefine.PHONE_MBP2k) || 
				phonemodel.equals(PublicDefine.PHONE_MBP1k)) 
		{
			TelephonyManager tm = (TelephonyManager) VoxActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
			if  (tm.getCallState() == TelephonyManager.CALL_STATE_OFFHOOK)
			{
				Log.d("mbp", "VoxActivity: INCALL show popup on Start");

				//20120821: if INCALL- show-popup 
				showDialog(DIALOG_SOUND_DETECT);

			}

			if (trigger_mac == null)
			{
				finish();
				return; 
			}
		}
		else //Non MBP_phones
		{	

			//Show camera list & finish this activity..
			finishAndloginToUserAccount();

			return; 
		}



	}

	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
	}

	protected void onNewIntent (Intent intent)
	{
	}


	private synchronized void finishAndloginToUserAccount()
	{
		if(this.trigger_VoxType == VoxMessage.VOX_TYPE_MOTION_ON)
		{

			Intent entry_intent = new Intent(VoxActivity.this, EventManagerActivity.class);
			entry_intent.putExtra(VoxActivity.TRIGGER_EVENT_CODE_EXTRA, this.eventTimeCode);
			entry_intent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA, trigger_mac);
			entry_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(entry_intent);

			if ( mMediaPlayer != null && mMediaPlayer.isPlaying())
			{
				mMediaPlayer.stop();
			}
		}
		else
		{
			Intent entry_intent = new Intent(VoxActivity.this, FirstTimeActivity.class);
			entry_intent.putExtra(FirstTimeActivity.bool_VoxInfraMode, true);
			entry_intent.putExtra(FirstTimeActivity.string_VoxDeviceAddr, trigger_mac);
			entry_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(entry_intent);

			if ( mMediaPlayer != null && mMediaPlayer.isPlaying())
			{
				mMediaPlayer.stop();
			}
		}
		VoxActivity.this.finish();

	}

	protected void onPause() {
		super.onPause();

		Log.d("mbp","VOXAC onPause");

	}


	protected void onResume()
	{
		super.onResume();
		Log.d("mbp","VOXAC onResume");
	}

	protected void onStop() {
		super.onStop();
		//Log.d("mbp","VOX act onStop");
		try 
		{
			this.dismissDialog(DIALOG_SOUND_DETECT);
		} catch (IllegalArgumentException  ie)
		{
		}
	}

	protected void onDestroy()
	{
		super.onDestroy();
		Log.d("mbp","VOX act onDestroy");

		if ( mMediaPlayer != null && mMediaPlayer.isPlaying())
		{
			mMediaPlayer.stop();
		}


		if (wl != null && wl.isHeld())
			wl.release();

		if ( usr_present_br!= null)
		{
			unregisterReceiver(usr_present_br);
		}

	}

	private static final int DIALOG_SOUND_DETECT = 3;
	//private static final int DIALOG_CALL_IS_ON = 2;

	protected Dialog onCreateDialog(int id) 
	{
		AlertDialog.Builder  builder;
		AlertDialog alert;

		switch (id)
		{
		case 1:
			builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.VoxActivity_vox_1))
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.Yes), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					/* check if we are in a call, end it : non-official way*/
					TelephonyManager tm = (TelephonyManager) VoxActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
					switch (tm.getCallState())
					{
					case TelephonyManager.CALL_STATE_OFFHOOK:
						//case TelephonyManager.CALL_STATE_RINGING:
						dialog.dismiss();
						VoxActivity.this.showDialog(2);
						break;
					default:
						dialog.dismiss();
						finishAndloginToUserAccount();
						break;
					}


				}
			})
			.setNegativeButton(getResources().getString(R.string.No), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					//Cancel the diaglog , finish the vox activity
					dialog.dismiss();
					VoxActivity.this.finish();
				}
			});



			alert = builder.create();

			return alert;

		case DIALOG_SOUND_DETECT:

			builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.VoxActivity_vox_1))
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.go_to_camera_list), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

					/*Clear all notifications. */ 
					NotificationManager notificationManager = (NotificationManager)
							getSystemService(Context.NOTIFICATION_SERVICE);

					for (int i =0; i< GcmIntentService.PUSH_IDs.length; i++)
					{
						notificationManager.cancel(GcmIntentService.PUSH_IDs[i]);
					}

					//Hangup call  
					TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
					try {
						// Java reflection to gain access to TelephonyManager's
						// ITelephony getter
						Log.v("mbp", "Get getTeleService...");
						Class c = Class.forName(tm.getClass().getName());
						Method m = c.getDeclaredMethod("getITelephony");
						m.setAccessible(true);
						com.android.internal.telephony.ITelephony telephonyService = (com.android.internal.telephony.ITelephony)m.invoke(tm);
						Log.d("mbp", " HaNG UP call here..");
						telephonyService.endCall();
					} catch (Exception e) {
						e.printStackTrace();
						Log.e("mbp",
								"FATAL ERROR: could not connect to telephony subsystem");
						Log.e("mbp", "Exception object: " + e);
					}


					dialog.dismiss();
					finishAndloginToUserAccount();

				}
			})
			.setNegativeButton(getResources().getString(R.string.dismiss), new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					//Cancel the diaglog , finish the vox activity
					dialog.dismiss();

					Log.d("mbp", "Reset client status");
					SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt(VoiceActivationService.str_client_status,VoiceActivationService.CLIENT_NOT_CREATED);
					editor.commit();


					/*20121106: vox service should be terminated by now. restart if needed 
					 */
					if (!LoginOrRegistrationActivity.isVOXServiceRunning(VoxActivity.this))
					{
						Log.d("mbp", "Restart. vox .");
						Intent i = new Intent(VoxActivity.this,VoiceActivationService.class);
						startService(i);
					}


					VoxActivity.this.finish();

					Log.d("mbp", "Show the dail pad");
					//show the on-going call again.. 
					Intent showcall = new Intent(Intent.ACTION_DIAL);
					startActivity(showcall); 

				}
			});



			alert = builder.create();

			return alert;
		default:
			break;
		}

		return null;

	}

}
