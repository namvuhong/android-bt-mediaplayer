package com.msc3;

import java.io.IOException;

import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.LoginOrRegistrationActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.view.WindowManager.BadTokenException;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blinkhd.R;
import com.discovery.LocalScanForCameras;
import com.discovery.ScanProfile;



/**
 * @author MC
 *  Quickly scan thru the available cameras - for each camera play video for 15 sec 
 *  Or user pressing NEXT ..
 *  
 *  VOX has to be stopped before entering this 
 *
 */
public class QuickViewCameraActivity extends Activity implements ICameraScanner, IVideoSink, ITemperatureUpdater, Callback {

	private static final int DIALOG_CONNECTION_PROGRESS = 1;
	private static final int DIALOG_NO_CAM_FOUND_AND_EXIT = 2;
	private static final int DIALOG_STORAGE_UNAVAILABLE = 3;
	private static final int DIALOG_CAMERA_IS_NOT_AVAILABLE = 4;
	private static final int DIALOG_SCANNING_FOR_CONFIGURED_CAMERAS = 5;

	private CamChannel [] restored_channels;
	private CamProfile [] restored_profiles;
	private int access_mode;

	private LocalScanForCameras  scan_task;
	private Bitmap video_background;

	private  VideoSurfaceView _vImageView;
	
	private VideoStreamer _streamer; 
	private PCMPlayer _pcmPlayer;
	private Thread streamer_thrd, pcmPlayer_thrd;

	private boolean current_video_has_ended_unexpectedly ;
	private boolean current_video_has_started ;
	
	private BroadcastReceiver batteryLevelReceiver, wifi_br;
	
	private ChannelFlip _flipper;
	private Thread flip_channel;
	
	private String camName;
	private boolean shouldRotateBitmap = false;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		current_video_has_ended_unexpectedly = false;
		current_video_has_started = false;
		
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			video_background = BitmapFactory.decodeResource(this.getResources(),
					R.drawable.homepage);
		}
		else 
		{
			video_background = BitmapFactory.decodeResource(this.getResources(),
					R.drawable.homepage_p);
		}
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		video_background = Bitmap.createScaledBitmap(video_background, wm
				.getDefaultDisplay().getWidth(), wm.getDefaultDisplay()
				.getHeight(), false);

	}
	protected void onPause() {
		super.onPause();
	}


	protected void onStop() {
		super.onStop();
		if ( batteryLevelReceiver != null)
		{
			try
			{
				unregisterReceiver(batteryLevelReceiver);
			}
			catch (IllegalArgumentException iae)
			{
				//ignore it
			}
			batteryLevelReceiver = null;
		}

		if (wifi_br != null)
		{
			try
			{
				unregisterReceiver(wifi_br);
			}
			catch (IllegalArgumentException iae)
			{
				//ignore it
			}
			wifi_br = null;
		}
		
		if (_flipper != null && _flipper.isRunning())
		{
			_flipper.stop();
			flip_channel.interrupt();
			try {
				flip_channel.join(3000);
			} catch (InterruptedException e) {
			}
		}
		
		
	}
	protected void onRestart() {
		super.onRestart();
	}

	protected void onActivityResult (int requestCode, int resultCode, Intent data)
	{
	}

	protected void onResume()
	{
		super.onResume();
	}
	protected void onStart() {

		super.onStart();
		try {
			if (restore_session_data() ==true) 
			{
				try {
					showDialog(DIALOG_SCANNING_FOR_CONFIGURED_CAMERAS);
				} catch (Exception e) {
				}
				
				scan_task = new LocalScanForCameras(this, this);
				scan_task.setShouldGetSnapshot(false);
				//Callback: updateScanResult()
				scan_task.startScan(restored_profiles);
			}
		} catch (StorageException e) {
			Log.d("mbp", e.getLocalizedMessage());
			showDialog(DIALOG_STORAGE_UNAVAILABLE);
			return;
		}

	}
	protected void onDestroy()
	{
		super.onDestroy();
		
		
		
	}

	public void onConfigurationChanged (Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		
		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
			shouldRotateBitmap = false; 

		} else if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT) {
			shouldRotateBitmap = true;
		}
	}

	protected Dialog onCreateDialog(int id)
	{
		ProgressDialog dialog;
		switch (id)
		{
		case DIALOG_SCANNING_FOR_CONFIGURED_CAMERAS:
			dialog = new ProgressDialog(this);
			Spanned msg = Html.fromHtml("<big>"+getString(R.string.ScanCam_3)+"</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
	
			return dialog;
		case DIALOG_CONNECTION_PROGRESS:
			dialog = new ProgressDialog(this);
			msg = Html.fromHtml("<big>"+getResources().getString(R.string.EntryActivity_connecting_to_bm)+"</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);

			return dialog;
		case DIALOG_NO_CAM_FOUND_AND_EXIT:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.no_camera_found_)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					QuickViewCameraActivity.this.finish();
				}
			});
			return builder.create();
		case DIALOG_STORAGE_UNAVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.usb_storage_is_turned_on_please_turn_off_usb_storage_before_launching_the_application)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
					Intent homeScreen = new Intent(QuickViewCameraActivity.this, FirstTimeActivity.class);
					homeScreen.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(homeScreen);
					QuickViewCameraActivity.this.finish();
				}
			});

			return builder.create();
		case DIALOG_CAMERA_IS_NOT_AVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html.fromHtml("<big>"+getString(R.string.no_signal_please_check_turn_on_the_camera_and_wifi_router_or_move_closer_to_the_router)+"</big>");
			builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			return builder.create();
		default:
			break;
		}
		return null;
	}


	/* Save and restore functions */
	private boolean restore_session_data() throws StorageException
	{

		/* 20120223: save some session data like melody, vox etc.. because after restore these data is erased */ 
		CamChannel [] temp = restored_channels; 


		SetupData savedData = new SetupData();
		try {
			if (savedData.restore_session_data(getExternalFilesDir(null)))
			{
				access_mode = savedData.get_AccessMode();
				restored_channels = savedData.get_Channels();
				restored_profiles = savedData.get_CamProfiles();

				if (temp != null)
				{
					for (int i = 0; i < temp.length; i++)
					{
						if (temp[i] != null)
						{
							for (int j = 0; j < restored_channels.length; j++)
							{
								if (restored_channels[j] != null )
								{
									CamChannel.copySessionData(temp[i], restored_channels[j]);
								}
							}
						}
					}
				}



				return true;
			}
		} catch (StorageException e) {
			throw e;
		}
		return false; 


	}

	
	
	private void flipChannel(final CamChannel [] channels)
	{
		
	
		if (flip_channel == null || !flip_channel.isAlive())
		{
			_flipper = new ChannelFlip(channels);
			flip_channel = new Thread(_flipper,"Channel Flipper");
			flip_channel.start();
		}
		else
		{
			Log.d("mbp", " FLipper is working...dont create new thread here ");
		}
	}


	/**
	 * 
	 * Make sure it run sequentially with setupVideoThreads()
	 * 
	 */
	private void stopCurrentVideoThread()
	{
		
		synchronized (QuickViewCameraActivity.this) {
		
			boolean retry = true;


			if (streamer_thrd != null)
			{
				//streamer_thrd.
				_streamer.stop();
				while (retry)
				{
					try {
						streamer_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}
				}
				streamer_thrd = null;
			}

			if ( pcmPlayer_thrd != null )
			{
				_pcmPlayer.stop();
				retry = true;
				while (retry)
				{	
					try {
						pcmPlayer_thrd.join(2000);

						retry = false;
					} catch (InterruptedException e) {
					}


				}
				pcmPlayer_thrd = null;
			}

		}

	}
	
	
	private void doLayout()
	{
		setContentView(R.layout.bb_is_quick_view);
		
		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) {
			shouldRotateBitmap = false; 

		} else if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT) {
			shouldRotateBitmap = true;
		}
		
		_vImageView = (VideoSurfaceView)findViewById(R.id.imageVideo);
		Handler.Callback h = new Handler.Callback() {

			@Override
			public boolean handleMessage(Message msg) {
				switch(msg.what)
				{
				case ViewCameraActivity.MSG_SURFACE_CREATED:
					displayBG(_vImageView,true);
					break;
				default:
					break;
				}
				return false;
			}
		};
		_vImageView.setHandler(new Handler(h));

	}

	private void viewOneChannel(final CamChannel s_channel)
	{

		camName = s_channel.getCamProfile().getName();
		
		updateCamName();
		try
		{
			showDialog(DIALOG_CONNECTION_PROGRESS);
		}
		catch(IllegalStateException ie)
		{

		}
		catch (BadTokenException be)
		{

		}
		/* Do video processing here */
		setupVideoThreads(s_channel);

	}
	
	

	private void setupVideoThreads(CamChannel s_channel) {
		
		synchronized (QuickViewCameraActivity.this) {
			String device_ip, http_pass;
			int device_port;

			current_video_has_ended_unexpectedly = false;
			current_video_has_started = false;
			
			device_ip = s_channel.getCamProfile().get_inetAddress().getHostAddress();
			device_port = s_channel.getCamProfile().get_port();
			try {
				http_pass = CameraPassword.getPasswordforCam(getExternalFilesDir(null),s_channel.getCamProfile().get_MAC() );
			} catch (StorageException e) {
				Log.d("mbp", e.getLocalizedMessage());
				showDialog(DIALOG_STORAGE_UNAVAILABLE);
				return;
			}


			_streamer = new VideoStreamer(new Handler(this), QuickViewCameraActivity.this, device_ip,device_port);
			if (http_pass != null)
			{
				Log.d("mbp","http_pass: " + http_pass);
				_streamer.setHTTPCredential(PublicDefine.DEFAULT_BASIC_AUTH_USR,http_pass);
			}

			_streamer.enableAudio(true);
			_streamer.addVideoSink(this);
			_streamer.setTemperatureUpdater(this);

			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);
			_streamer.setImageResolution(img_res);

			

			_pcmPlayer = new PCMPlayer(8000, AudioFormat.CHANNEL_CONFIGURATION_MONO);
			streamer_thrd= new Thread(_streamer,"VAStreamer");
			pcmPlayer_thrd =new Thread(_pcmPlayer,"PCMPlayer");

			_streamer.restart();
			streamer_thrd.start();
			pcmPlayer_thrd.start();
		}

	}

	/* also used by VideoStreamer */
	public  void displayBG(VideoSurfaceView _vImageView, boolean shouldDisplay)
	{
		Canvas c = null;
		SurfaceHolder _surfaceHolder = _vImageView.get_SurfaceHolder();
		if (_surfaceHolder == null) {
			return;
		}

		try {
			c = _surfaceHolder.lockCanvas(null);
			if (c == null) {
				Log.w("mbp","displayBG: canvas is NULL");
				return;
			}
			
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				video_background = BitmapFactory.decodeResource(this.getResources(),
						R.drawable.homepage);
			}
			else 
			{
				video_background = BitmapFactory.decodeResource(this.getResources(),
						R.drawable.homepage_p);
			}
			// show full screen of Motorola logo when the camera video cannot be opened
			WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
			video_background = Bitmap.createScaledBitmap(video_background, wm
					.getDefaultDisplay().getWidth(), wm.getDefaultDisplay()
					.getHeight(), false);

			synchronized (_surfaceHolder) {
				//c.drawBitmap(background,0,0,null);


				if (shouldDisplay)
				{
					c.drawBitmap(video_background,0,0,null);
				}
				else
				{
					c.drawColor(Color.BLACK);
				}
			}
		} finally {

			if (c != null) {
				_surfaceHolder.unlockCanvasAndPost(c);
			}
		}
	}


	private final Integer [] channelStatus = {
			R.drawable.status_icon4_1,R.drawable.status_icon4_2,
			R.drawable.status_icon4_3, R.drawable.status_icon4_4
	};
	
	
	private void updateCamName()
	{
		final TextView text_camName = (TextView) findViewById(R.id.textCamName);
		if ( text_camName != null)
		{
			text_camName.post(new Runnable() {

				@Override
				public void run() {

					text_camName.setText(camName);
				}	
			});
		}
	}
	
	@Override
	public void updateScanResult(ScanProfile[] results, int status, int index) {
		if (status == ShortClipRecoder.REASON_AUTHORIZED_PWD_NEEDED)
		{
			Log.e("mbp", "SHOULD NOT BE HERE");
		}
		else if ( (results != null) && 
				(restored_profiles != null) && 
				(restored_profiles.length >0) )
		{
			for ( int i = 0 ; i < restored_profiles.length; i++)
			{
				//Default to offline 
				if (restored_profiles[i] == null)
				{
					continue;
				}
				
				if (restored_profiles[i].hasUpdatedLocation() && restored_profiles[i].isInLocal())
				{
					continue;
				}
				
				restored_profiles[i].setInLocal(false);
				for (int j = 0; j < results.length; j++)
				{
					if (restored_profiles[i].get_MAC().equalsIgnoreCase(results[j].get_MAC()))
					{
						//Update the new set of snapshot 
//						restored_profiles[i].getShortClip().addAll(results[j].getShortClip());
						//Update the new IP
						restored_profiles[i].setInetAddr(results[j].get_inetAddress());
						//ONLINE only when we have a match MAC in local 
						restored_profiles[i].setInLocal(true);
						//restored_profiles[i].setMelodyIsOn(results[j].isMelodyIsOn());
						//Log.d("mbp", "cam:"+ restored_profiles[i].getName()+ " is online");
						break;
					}
				}
			}
			
			try {
				dismissDialog(DIALOG_SCANNING_FOR_CONFIGURED_CAMERAS);
			} catch (Exception e) {
			}

			/* Update the channel list */
			for ( int i = 0 ; i < restored_channels.length; i++)
			{
				if (restored_channels[i].getCamProfile() != null)
				{

					for (int j = 0; j < restored_profiles.length; j++)
					{

						if (restored_channels[i].getCamProfile().equals(restored_profiles[j]))
						{
							restored_channels[i].setCamProfile(restored_profiles[j]);
							restored_profiles[j].setChannel(restored_channels[i]);
							restored_profiles[j].bind(true);
							
							break;
						}
					}

				}
			}
			
			doLayout();

			flipChannel(restored_channels);

		}
		else  // (results == null) || (restore_profiles ==null) || restore_profile.len =0
		{
			//NO INFRA camera found 
			showDialog(DIALOG_NO_CAM_FOUND_AND_EXIT);
		}		
	}


	@Override
	public void onFrame(byte[] frame, byte[] pcm, int pcm_len)
	{
		if (frame.length > 0)
		{
			/**** Start drawing ****/
			Bitmap b= null;
			b = BitmapFactory.decodeByteArray(frame, 0, frame.length);

			if (shouldRotateBitmap == true)
			{
				/*
			//rotate the bitmap before drawing/scalling
			// createa matrix for the manipulation
			Matrix matrix = new Matrix();
			matrix.postScale(1,1);
			// rotate the Bitmap
			matrix.postRotate(90);

			// recreate the new Bitmap
			b = Bitmap.createBitmap(b, 0, 0,
					b.getWidth(), b.getHeight(), matrix, true);

				 */

				int newHeight = b.getHeight();
				double hw_ratio = (double)b.getHeight()/(double)b.getWidth(); 
				int  newWidth =  (int)  (hw_ratio* newHeight);
				int new_x = (int) (((double)b.getWidth() - newWidth)/2);

				//Log.d("mbp", "b: " + new_x + ", 0,"+ newWidth + "," + newHeight);
				b =  Bitmap.createBitmap(b,new_x, 0, newWidth, newHeight);


			}


			Canvas c = null;

			SurfaceHolder _surfaceHolder = _vImageView.get_SurfaceHolder();

			if (_surfaceHolder == null) {
				return;
			}


			try {
				c = _surfaceHolder.lockCanvas(null);
				if (c == null) {
					Log.w("mbp","onFrame: canvas is NULL");
					return;
				}

				//currentZoomInLevel = 0,1,2,3,4 <-> 0,80,60,40,20 % zoom
				int left = 0;
				int top  = 0;
				int right = b.getWidth();
				int bottom = b.getHeight() ;
				int destWidth;
				int destHeight ;

				/* 3. Honor the width ratio, 
				 * Dont care about the height being cut off
				 * 
				 */
				float ratio = (float) right/bottom;
				float fh =  c.getWidth() / ratio;

				destWidth = c.getWidth();
				destHeight = (int)fh; 

				int  dst_top = 0;
				if ( destHeight > c.getHeight())
				{
					int delta = destHeight - c.getHeight();
					dst_top = -delta/2;
					destHeight -= delta/2;
				}
				else
				{ 
					//				expand the height 
					destHeight = c.getHeight();
				}

				Rect src = new Rect(left,top,right, bottom);

				Rect dest = new Rect(0,dst_top,destWidth,destHeight);

				synchronized (_surfaceHolder) {     	
					c.drawBitmap(b,src ,dest, null);
				}
			} finally {
				if (c != null) {
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}

		}
		/**** END drawing ****/
		
		if( (pcm != null) &&
				(pcmPlayer_thrd != null) &&
				pcmPlayer_thrd.isAlive() ) {
			_pcmPlayer.writePCM(pcm, pcm_len);
		}

	}
	@Override
	public void onInitError(String errorMessage) {

	}
	@Override
	public void onVideoEnd() {

	}
	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what){
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY:

		{
			try 
			{
				this.dismissDialog(DIALOG_CONNECTION_PROGRESS);
			} catch (IllegalArgumentException  ie)
			{
			}
			catch (BadTokenException  ie)
			{
			}
			synchronized (QuickViewCameraActivity.this) {
				current_video_has_ended_unexpectedly = true;
				current_video_has_started = false;
			}
			
			break;
		}
		case VideoStreamer.MSG_CAMERA_IS_NOT_AVAILABLE:
		{
			try 
			{
				this.dismissDialog(DIALOG_CONNECTION_PROGRESS);
			} catch (IllegalArgumentException  ie)
			{
			}
			catch (BadTokenException  ie)
			{
			}
			synchronized (QuickViewCameraActivity.this) {
				current_video_has_ended_unexpectedly = true;
				current_video_has_started = false;
			}
			
			break;
		}
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STARTED:
			synchronized (QuickViewCameraActivity.this) {
				current_video_has_started = true;
				current_video_has_ended_unexpectedly = false;
			}
			
			try 
			{
				this.dismissDialog(DIALOG_CONNECTION_PROGRESS);
			} catch (IllegalArgumentException  ie)
			{
			}
			catch (BadTokenException  ie)
			{
			}
			break;

		default:
			break;
		}
		return false;
	}
	@Override
	public void updateTemperature(int level) {
		final RelativeLayout statusBar = (RelativeLayout)findViewById(R.id.statusBar);
		final ImageView tempAlarm = (ImageView)findViewById(R.id.tempAlarm);
		final TextView temp_txt = (TextView) findViewById(R.id.textTemp);
		

		/*20120402: another hack 
		 * dont display if temp is <0 or > 60 
		 * */
		if(level < 1 || level > 60 )
		{
			if (temp_txt != null)
			{
				temp_txt.setText("");
			}
			return; 
		}
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int temp_index = settings.getInt(PublicDefine.PREFS_TEMPERATURE_UNIT, PublicDefine.TEMPERATURE_UNIT_DEG_F);

		float deg;
		String unit;
		boolean shouldGoRed = false;
		if (temp_index == PublicDefine.TEMPERATURE_UNIT_DEG_F)
		{
			deg = ((float)level * 9 + 32*5 )/5;
			unit = "\u2109";


			if (deg>PublicDefine.HIGH_F_TEMPERATURE_THRESHOLD || 
					deg <PublicDefine.LOW_F_TEMPERATURE_THRESHOLD)
			{
				shouldGoRed =true;
			}
		}
		else
		{
			deg = (float) level;
			unit = "\u2103";
			if (deg>PublicDefine.HIGH_C_TEMPERATURE_THRESHOLD || 
					deg <PublicDefine.LOW_C_TEMPERATURE_THRESHOLD)
			{
				shouldGoRed =true;
			}
		}

		/* "\u2103" = deg Celsius symbol
		 * "\u2109" = deg Farenheit Symbol  
		 * Conversion:
		 * 
		 * F = C* 9/5 + 32 = (C*9 + 32 *5)/5
		 * 
		 * */
		
		/* 20130201: hoang: change temperature display
		 */
		int rounded_deg = Math.round(deg);
		final String display = String.format("%d %s", rounded_deg, unit) ;
		final boolean goRed = shouldGoRed;

		
		

		if ( temp_txt != null)
		{
			temp_txt.post(new Runnable() {

				@Override
				public void run() {

					temp_txt.setText(display);
				}	
			});
		}

		if (goRed)
		{

			if(tempAlarm != null) {
				tempAlarm.post(new Runnable() {

					@Override
					public void run() {

						tempAlarm.setVisibility(View.VISIBLE);
					}
				});
			}

			if(statusBar != null) {
				statusBar.post(new Runnable() {

					@Override
					public void run() {

						statusBar.setBackgroundColor(Color.RED);
					}
				});
			}
		}
		else
		{
			if(statusBar != null) {
				statusBar.post(new Runnable() {

					@Override
					public void run() {

						statusBar.setBackgroundColor(Color.TRANSPARENT);
					}
				});
			}

			if(tempAlarm != null) {
				tempAlarm.post(new Runnable() {

					@Override
					public void run() {

						tempAlarm.setVisibility(View.INVISIBLE);
					}
				});
			}
		}

	}


	
	/**
	 * Computes the battery level by registering a receiver to the intent triggered 
	 * by a battery status/level change.
	 */

	private static final int [] batteryImages = {R.drawable.status_icon1_5,R.drawable.status_icon1_1,
		R.drawable.status_icon1_2,
		R.drawable.status_icon1_3, R.drawable.status_icon1_4};


	private void setupBatteryUpdate(ImageView img) {
		if ( img == null)
		{
			Log.e("mbp", "battery imageview is not available");
			return; 
		}
		
		if ( batteryLevelReceiver != null)
		{
			try
			{
				unregisterReceiver(batteryLevelReceiver);
			}
			catch(IllegalArgumentException iae)
			{

			}

			batteryLevelReceiver = null;

		}
		final ImageView battImg = img;
		batteryLevelReceiver = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {

				int rawlevel = intent.getIntExtra("level", -1);
				int scale = intent.getIntExtra("scale", -1);
				int level = -1;
				if (rawlevel >= 0 && scale > 0) {
					level = (rawlevel * 100) / scale;
				}
				battImg.post(new Runnable() {

					private int mLevel ;
					public Runnable setLevel(int level)
					{
						if ( level < 5)
						{
							mLevel = 0;
						}
						else if ( level <25)
						{
							mLevel = 1;
						}
						else if ( level <50)
						{
							mLevel = 2;
						}
						else if ( level <75)
						{
							mLevel = 3;
						}
						else //level >75
						{
							mLevel = 4;
						}
						return this;
					}
					@Override
					public void run() {
						battImg.setImageResource(ViewCameraActivity.batteryImages[mLevel]);
					}
				}.setLevel(level));

			}
		};
		IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		registerReceiver(batteryLevelReceiver, batteryLevelFilter);
	}
	

	private final static int [] wifi_icons = new int [] {R.drawable.wifi_0_icon,R.drawable.wifi_1_icon,
		R.drawable.wifi_2_icon,R.drawable.wifi_3_icon,R.drawable.wifi_full_icon
	};


	private void setup_Wifi_listener(ImageView img)
	{
		if ( img == null)
		{
			Log.e("mbp", "wifi levelview is not available");
			return; 
		}
		if ( wifi_br != null)
		{
			unregisterReceiver(wifi_br);
			wifi_br = null;

		}


		final ImageView wifiImg = img;
		wifiImg.setVisibility(View.VISIBLE);


		WifiManager wm = (WifiManager) this.getSystemService(WIFI_SERVICE);
		final int cur_level =WifiManager.calculateSignalLevel( wm.getConnectionInfo().getRssi(), 5);
		//Update the current WIFI level
		wifiImg.post(new Runnable() {

			@Override
			public void run() {
				wifiImg.setImageResource(wifi_icons[cur_level]);
			}
		});


		wifi_br = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {

				int rawlevel = intent.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0);
				int level = -1;
				level = WifiManager.calculateSignalLevel(rawlevel, 5); 
				wifiImg.post(new Runnable() {

					private int mLevel ;
					public Runnable setLevel(int level)
					{
						mLevel = level;
						return this;
					}
					@Override
					public void run() {
						wifiImg.setImageResource(wifi_icons[mLevel]);
						//wifiImg.setVisibility(View.VISIBLE);
					}
				}.setLevel(level));

			}
		};
		IntentFilter wifiLevelFilter = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
		registerReceiver(wifi_br, wifiLevelFilter);

		WifiManager w = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		w.startScan();

	}
	
	
	private class ChannelFlip implements Runnable 
	{
		private CamChannel []  channels;
		private boolean running ; 
		public ChannelFlip(CamChannel [] channels)
		{
			this.channels = channels;
			running = true; 
		}
		
		public boolean isRunning() {
			return running;
		}

		public void stop()
		{
			running = false;
		}
		
		public void run()
		{
			while (running)
			{
				for (int i = 0; i< channels.length; i++)
				{
					if ( channels[i] != null &&
							channels[i].getCamProfile() != null &&
							channels[i].getCamProfile().isInLocal() )
					{
						
						
						final CamChannel ch = channels[i];
						Log.d("mbp","Switch to cam: " +ch.getCamProfile() );
						
						
						
						QuickViewCameraActivity.this.runOnUiThread(new Runnable() {
	
							@Override
							public void run() {
								viewOneChannel(ch);
							}
						});
						
						int count = 20; 
						boolean showDisconnectAndPlayTone = false;
						while (count > 0 )
						{
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								Log.d("mbp", "Interrupted ex");
							}//sleep 20 sec
							finally
							{
								if (running == false)
								{
									break;
								}
							}

							//If video stopped unexpectedly and we have not showed the dialog & playtone yet
							if (current_video_has_ended_unexpectedly == true) 
							{
								//Retry --- 
								QuickViewCameraActivity.this.runOnUiThread(new Runnable() {
									
									@Override
									public void run() {
										stopCurrentVideoThread();
										setupVideoThreads(ch);
									}
								});
								if (showDisconnectAndPlayTone == false)
								{
									/* stop while viewing ... reset the loop to 60sec*/
									Log.d("mbp", "reset count& start alarm");
									count = 60;
									showDisconnectAndPlayTone = true; 
								}
							}
							
							if (current_video_has_started == true) 
							{
								//Stop playing..
								if (showDisconnectAndPlayTone == true)
								{
									Log.d("mbp", "stop alarm");
									showDisconnectAndPlayTone = false;
									count = 20;//reset time wait to 20secs
									QuickViewCameraActivity.this.runOnUiThread(new Runnable() {

										@Override
										public void run() {
											try 
											{
												dismissDialog(DIALOG_CAMERA_IS_NOT_AVAILABLE);
											} catch (IllegalArgumentException  ie)
											{
											}
											catch (BadTokenException  ie)
											{
											}
										}
									});
								}
							}
							
							
							//if some video is disconnected, show up a dialog and play a tone 
							//at the 5 seconds mark
							if (showDisconnectAndPlayTone &&
									( (count%5) == 0 ))
							{
								Log.d("mbp", "showing dialog and play tone now ");
								/* Play beep:  */
								MediaPlayer mMediaPlayer = new MediaPlayer();
								String uri = "android.resource://" + getPackageName() + "/" + R.raw.beep;
								try {
									mMediaPlayer.setDataSource(QuickViewCameraActivity.this, Uri.parse(uri));
								} catch (IOException e) {
									e.printStackTrace();
								}
								mMediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
								try {
									mMediaPlayer.prepare();
								} catch (IllegalStateException e1) {
									mMediaPlayer = null;
									e1.printStackTrace();
								} catch (IOException e1) {
									mMediaPlayer = null;
									e1.printStackTrace();
								} 

								mMediaPlayer.start();
								
								QuickViewCameraActivity.this.runOnUiThread(new Runnable() {
									
									@Override
									public void run() {
										displayBG(_vImageView,true);
										try 
										{
											showDialog(DIALOG_CAMERA_IS_NOT_AVAILABLE);
										} catch (IllegalArgumentException  ie)
										{
										}
										catch (BadTokenException  ie)
										{
										}
									}
								});
							}

							count--;

							// stop to switch to next camera video, until this camera is recovered
							// reset the loop to 60sec
							if ( showDisconnectAndPlayTone == true && count == 0 ) {
								count = count + 60;
							}

						}
						
						
						if ((current_video_has_ended_unexpectedly == true) 
							 || (showDisconnectAndPlayTone == true) )
						{

							showDisconnectAndPlayTone = false;
							
							QuickViewCameraActivity.this.runOnUiThread(new Runnable() {
		
								@Override
								public void run() {
									try 
									{
										dismissDialog(DIALOG_CAMERA_IS_NOT_AVAILABLE);
									} catch (IllegalArgumentException  ie)
									{
									}
									catch (BadTokenException  ie)
									{
									}
								}
							});
							
						}
						
						
						QuickViewCameraActivity.this.runOnUiThread(new Runnable() {
	
							@Override
							public void run() {
								stopCurrentVideoThread();
							}
						});
					} 
					
					
					
					if (running == false)
					{
						break;
					}
					
				}//for ()
				
				
			} //while (repeat)
			
		}
	};
	
}
