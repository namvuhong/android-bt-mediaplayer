package com.msc3;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

class DropDownAnim extends Animation{
	int targetHeight;
	RelativeLayout v;
	boolean down;

	public DropDownAnim(RelativeLayout wv,  boolean d, int newH){
		v = wv;
		targetHeight = newH;
		down = d;
	}


	protected void applyTransformation(float interpolatedTime, Transformation t){
		int newHeight;
		if(down){
			newHeight = (int)(targetHeight*interpolatedTime);
		}
		else{
			newHeight = (int)(targetHeight*(1-interpolatedTime));
		}
		v.getLayoutParams().height = newHeight;
		v.requestLayout();
	}

	public void initalize(int width, int height, int parentWidth, int parentHeight){
		super.initialize(width,height,parentWidth,parentHeight);

	}

	public boolean willChangeBounds(){
		return true;
	}
}