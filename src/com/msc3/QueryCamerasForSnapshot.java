package com.msc3;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Vector;



import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.os.AsyncTask;
import android.util.Log;


/*
 * Connect to a given Camera, enable video streaming, 
 * Store a number of frame in a buffer, then return 
 */

public class QueryCamerasForSnapshot extends AsyncTask<List<ScanResult> , String, Vector<CamProfile>> {


	
	
	private Context mContext;
	private Object wifiLockObject;
	
	private ProgressDialog dialog;

	private WifiBr br;
	private IQuerySnapshotCallback _callback ; 
	public QueryCamerasForSnapshot(Context c,IQuerySnapshotCallback cb )
	{
		mContext = c;
		wifiLockObject = new Object();
		br = new WifiBr();
		_callback = cb;
	}


	protected void onPreExecute ()
	{
		
		dialog = new ProgressDialog(mContext);
		dialog.setMessage("Getting snapshots... \nThis process can take up to a few minutes, Please be patient...");
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		dialog.show();
		
		IntentFilter i_filter = new IntentFilter();
		i_filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		br = new WifiBr();
		mContext.registerReceiver(br,i_filter);
	}
	
	protected void onPostExecute (Vector<CamProfile> result)
	{
		
		if ( br != null)
		{
			mContext.unregisterReceiver(br);
		}
		if (dialog != null)
		{
			dialog.dismiss();
		}
		
		_callback.sendSnapshotResult(result);
		
		
	}

	
	@Override
	protected Vector<CamProfile> doInBackground(List<ScanResult>... params) {
		
		List<ScanResult> results = params[0];
		WifiManager w = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE); 

		String bssid= null, ssid = null;
		CamProfile temp_cp = null; 
		InetAddress default_inet_address = null;
		Vector<CamProfile> cps = new Vector<CamProfile>();
		Vector<NameAndSecurity> bssid_list = new Vector<NameAndSecurity>();

		String current_ssid = w.getConnectionInfo().getSSID();
		long start_restart_system = 0;
		
		for(ScanResult result : results) {
			if (result.SSID.startsWith(PublicDefine.DEFAULT_SSID))
			{
				bssid_list.add(new NameAndSecurity(result.SSID,result.capabilities, result.BSSID));
			}
		}


		w.disableNetwork(w.getConnectionInfo().getNetworkId());

		List<WifiConfiguration> wcs = null; 

		for (int i = 0; i< bssid_list.size(); i++)
		{
			bssid = bssid_list.elementAt(i).BSSID;
			ssid = bssid_list.elementAt(i).name;

			int nw_id = -1;
			/*1. try to connect to the corresponding BSSID */

			Log.d("mbp", "Connect to : " + bssid + " for snapshot");
			WifiConfiguration wc ;
			wcs = w.getConfiguredNetworks();
			for(int j = 0 ; j< wcs.size(); j++){
				wc =  wcs.get(j);
				if(  (wc.SSID != null) && 
						(wc.SSID.equalsIgnoreCase("\"" + ssid + "\"")) 
						/*&&
	    				 (wc.BSSID != null) &&
	    				 (wc.BSSID.equalsIgnoreCase(bssid))*/ )
				{
//					w.disableNetwork(wc.networkId);
//					w.removeNetwork(wc.networkId);

					//DONT REMOVE THIS
					Log.d("mbp", "disable: " + wc.networkId + ":" + w.disableNetwork(wc.networkId));
					Log.d("mbp", "removeNetwork id: " + wc.networkId + ":" + w.removeNetwork(wc.networkId));
					


				}
			}


			//create a new configuration 
			WifiConfiguration config = new WifiConfiguration();
			config.SSID =  "\"" + ssid +"\"";
			config.BSSID = bssid;
			config.allowedKeyManagement.set(KeyMgmt.NONE);

			nw_id = w.addNetwork(config);
			if ( nw_id == -1)
			{
				Log.e("mbp", "ADD new configure error");
				return null;
			}
			Log.d("mbp", "new id: " + nw_id);
			
			/* Note: It is possible for this method to change the network 
			 * IDs of existing networks. You should assume the network IDs can 
			 * be different after calling this method.
			 */
			w.saveConfiguration();
			
			
			int retries = 3;
			do 
			{
				boolean enabled ;
				enabled = w.enableNetwork(nw_id, true);

				while (enabled == false)
				{
					
					w.disconnect();
					
					/* CHECK THE NOTE ABOVE:
					 * -->Search the list again to find out if the new Id was changed
					 * */
					wcs = w.getConfiguredNetworks();
					for(int j = 0 ; j< wcs.size(); j++){
						wc =  wcs.get(j);
						if(  (wc.SSID != null) && 
								(wc.SSID.equalsIgnoreCase("\"" + ssid + "\"")) &&
			    				 (wc.BSSID != null) &&
			    				 (wc.BSSID.equalsIgnoreCase(bssid)))
						{
							if (nw_id != wc.networkId)
							{
								Log.d("mbp", "new id CHANGED after saveConfiguration: " + wc.networkId);
								nw_id = wc.networkId; 
							}
							break;
						}
					}
					
					enabled = w.enableNetwork(nw_id, true);
					Log.d("mbp", "re-enable after disconnect: " +enabled);
				}


				//need to wait for the network to be up
				synchronized (wifiLockObject) {
					try {
						wifiLockObject.wait(30000);

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				Log.d("mbp", retries+ "-after wait for connection... " +
						w.getConnectionInfo().getNetworkId() + " " +
						w.getConnectionInfo().getBSSID() );

				if ( (w.getConnectionInfo().getBSSID()!= null) &&
						w.getConnectionInfo().getBSSID().equalsIgnoreCase(bssid))
				{
					Log.d("mbp", "got the correct BSSID!!>>>>>>> ip: " + w.getDhcpInfo().ipAddress +
								"\n gw ip: " + w.getDhcpInfo().gateway);
					
					if (  (w.getConnectionInfo().getIpAddress() == 0 )|| 
							(w.getDhcpInfo().ipAddress == 0) )
					{
						//We're connected but don't have any IP yet
						retries++; //give 1 more chance
						continue;
					}
					else
					{
						//Now we're sure that the connection is ready
						break;
					}
				}
				else
				{
					//Restart this cam so the other will come up

					URL url = null;
					URLConnection conn = null;
					try {
						url = new URL(PublicDefine.RESTART_DEVICE_HTTP_CMD);
						//url = new URL(PublicDefine.RESTART_APP_HTTP_CMD);
						conn = url.openConnection();
						conn.setConnectTimeout(1000);
						conn.setReadTimeout(1000);
						
						((HttpURLConnection) conn).getResponseCode();
						
						start_restart_system = System.currentTimeMillis();
						
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					/* Sleep for a while to let the old BSSID disappear 
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					*/
				}

			} while (retries-- > 0);



			/*2. Connect to the camera to pull some images*/
			Log.d("mbp", "pulling some snapshot from: " + nw_id);
			try {
				default_inet_address = InetAddress.getByName(PublicDefine.DEVICE_IP);
			} catch (UnknownHostException e) {
				Log.d("mbp", "exception: " + this.getClass().getName());
				e.printStackTrace();
			}

			temp_cp = new CamProfile(default_inet_address,  bssid);
			ShortClipRecoder.doRecordShortClip(temp_cp,false);
			
			cps.addElement(temp_cp);

			nw_id = w.getConnectionInfo().getNetworkId();
			w.disconnect();
			w.removeNetwork(nw_id);
			


		}//for (int i = 0; i< bssid_list.size(); i++)

		
		/* make sure that all cams that were restarted are all up */
		long time_to_finish_restart = PublicDefine.DEVICE_RESTART_TOTAL_TIME - (System.currentTimeMillis() - start_restart_system);
		if (time_to_finish_restart > 0 )
		{
			try {
				Log.d("mbp","wait additional: " + time_to_finish_restart );
				Thread.sleep(time_to_finish_restart);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		
		
		if ( current_ssid != null)
		{
			wcs = w.getConfiguredNetworks();
			for(WifiConfiguration wc : wcs) {
				if ( (wc == null)|| (wc.SSID == null))
					continue;

				if(wc.SSID.equalsIgnoreCase("\""+current_ssid + "\"")) {
					w.enableNetwork(wc.networkId, true);
					w.reconnect();
				}
			}
		}

		
		
		return cps;
	}



	private class WifiBr extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			NetworkInfo mWifiNetworkInfo;
			mWifiNetworkInfo =
					(NetworkInfo) intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

			WifiManager w = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

			if ( (mWifiNetworkInfo != null)&& mWifiNetworkInfo.isConnected() )
			{

				String mBssid = intent.getStringExtra(WifiManager.EXTRA_BSSID);
				//Log.d("mbp", "NW_STATE: CONNECTED bssid:" + mBssid );

				synchronized (wifiLockObject) {
					wifiLockObject.notify();
				}
			}				
		}
	}
}
