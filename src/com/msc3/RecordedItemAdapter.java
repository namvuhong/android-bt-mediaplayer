package com.msc3;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;



import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinkhd.R;

public class RecordedItemAdapter extends BaseAdapter {
    private Context mContext;
    private File[] file_list;
    
    public final int ITEM_ICON_VIEW_INDEX = 0;
    public final int ITEM_TEXT_VIEW_INDEX = 1;

    public RecordedItemAdapter(Context c) {

    	mContext = c;
        //XXX: what if files are added after this is called??? 
        file_list = Util.getRecordFiles();
        
    }

    public int getCount() {
    	return file_list.length;
    }

    /* return a FILE in Object place */
    public Object getItem(int position) {
    	File item = null; 
    	if ( position < file_list.length)
    	{
    		item = file_list[position];
    	}
        return item;
    }

    public long getItemId(int position) {
        return 0;
    }
    
    
  
    public View getView(int position, View convertView, ViewGroup parent) {
    	LinearLayout itemView = null;
    	ImageView imageView = null;
    	TextView file_name = null;
    	File image_or_video = null;
    	
        if (convertView == null) {  // if it's not recycled, initialize some attributes
        	itemView = new LinearLayout(mContext);
        	
        } else {
        	itemView = (LinearLayout) convertView;
        	
        	/* clear all children */
        	itemView.removeAllViews();
        }
        //create an Icon with a text view to show file name
        itemView.setLayoutParams(
        		new GridView.LayoutParams(
        				LinearLayout.LayoutParams.WRAP_CONTENT, 
        				LinearLayout.LayoutParams.WRAP_CONTENT));
        itemView.setPadding(5, 5, 5, 5);
        
    	
    	/*create new.. */
    	imageView = new ImageView(itemView.getContext());
    	file_name = new TextView(itemView.getContext()); 
    	
    	
    	
     	image_or_video = file_list[position];
        
     	imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        //For now, use a generic icon for photo/video - based on extension
        imageView.setPadding(10,10, 10,10);
    	if(image_or_video.getName().endsWith(".avi")) {
    		
    		String thumbnail = Util.getVideoThumbnailFileName(image_or_video.getName());
    		Bitmap b = BitmapFactory.decodeFile(thumbnail);
    		if (b == null)
    		{
				/* use a default icon if thumbnail is not found */
    			imageView.setImageResource(R.drawable.video_item);
    		}
    		else
    		{
    			imageView.setImageBitmap(b);
    			
    		}
    		
    	}
    	if(image_or_video.getName().endsWith(".jpg")) {
    		String thumbnail = Util.getPhotoThumbnailFileName(image_or_video.getName());
    		Bitmap b = BitmapFactory.decodeFile(thumbnail);
    		if (b == null)
    		{
				/* use a default icon if thumbnail is not found */
	    		imageView.setImageResource(R.drawable.photo_item);
    		}
    		else
    		{
    			imageView.setImageBitmap(b);
    		}
    		
    	}
    	
        /* Textview to show file name*/
        file_name.setText( image_or_video.getName());
        file_name.setTextSize(16);
        file_name.setTextColor(Color.BLACK);
        file_name.setPadding(10,10, 10,10);
        
        //Add icon and text 
        itemView.addView(imageView, ITEM_ICON_VIEW_INDEX, 
        		         new LinearLayout.LayoutParams(100,100));
        itemView.addView(file_name, ITEM_TEXT_VIEW_INDEX, 
        		new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
        									  LinearLayout.LayoutParams.FILL_PARENT));
        
        
        return itemView;
    }
}
