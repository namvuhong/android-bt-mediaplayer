package com.msc3;

import com.blinkhd.R;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;

import javax.net.ssl.HttpsURLConnection;

import com.barchart.udt.ExceptionUDT;
import com.barchart.udt.SocketUDT;
import com.barchart.udt.TypeUDT;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.monitoreverywhere.utils.SymmetricCipher;
import com.msc3.comm.UDTRequestSendRecv;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;

/**
 * 20120522 ---  GRRRRRRR 
 * 
 * @author phung
 *
 * UDT video streamer : streaming video from a camera running UDT 
 * 
 *   Different from HTTP video streamer: 
 *         HTTP video Streamer: is passed with the REAL address & port at constructor
 *         UDT        streamer : is passed with DUMMY address & port at construct
 *         
 *  Right before making the connection to camera, UDT Streamer will request the UDT SERVER for the camera ip & port. 
 *  After successfully connected to camera, UDT Streamer passes back the REAL auth object (ip,port, sskey, ) to EntryActivity for other use
 *  
 *  WHY: 
 *   - UDT lib has a very strange behavior: 
 *      a UDT socket will be created to get the port&ip from UDT server
 *      then another UDT socket will be created to connect to camera. 
 *      However these two socket MUST be created on THE SAME THREAD. Otherwise, the latter can't connect to camera. 
 *   - Suspicion: Since UDT server sends the request to camera & stun bridge on camera will punch a hole in Router of app. 
 *                Then a UDT Socket from app will bind to that specific port. 
 *                
 *                --> if run from a different thread the binding to that port is either can't be done or failed. 
 *                And app has neither knowledge nor control about this. 
 *     
 *   
 *
 *
 *
 */
public class UDTVideoStreamer extends VideoStreamer {
	
	public static final String BOUNDARY_MARKER  = "--boundarydonotcross";
	private static final int MAX_CONN_RETRIES = 6; //6*3 = 18 sec - timeout 
	private String channel_id; 
	private String mac; 
	
	private byte [] eChannelId;
	private byte [] eMac;
	
	private int udtLocalPort ; 	
	
	private boolean isGoingThruRelay ; 
	private SocketUDT sockdscp ;
	
	private boolean useOldRelay;
	
	private Tracker tracker;
	private EasyTracker easyTracker;

	public UDTVideoStreamer(Handler h, Context m, String device_ip, int port)
	{
		super(h, m, device_ip, port);
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(m);
		tracker = easyTracker.getTracker();
		useOldRelay = true;
		isGoingThruRelay = false; 
	}
	
	public UDTVideoStreamer(Handler h, Context m, String device_ip, int port, boolean oldRelay)
	{
		super(h, m, device_ip, port);
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(m);
		tracker = easyTracker.getTracker();
		useOldRelay = oldRelay;
		isGoingThruRelay = false; 
	}
	
	/**
	 * @return the useOldRelay
	 */
	public boolean isUseOldRelay() {
		return useOldRelay;
	}

	/**
	 * @param useOldRelay the useOldRelay to set
	 */
	public void setUseOldRelay(boolean useOldRelay) {
		this.useOldRelay = useOldRelay;
	}

	/* this should be UDT auth */
	public void setRemoteAuthentication(BabyMonitorAuthentication bm )
	{
		this.bm_auth = bm;
		this.session_key =  bm.getSSKey();
		
		this.device_ip  = bm.getIP();
		this.device_port = bm.getPort();
		
		authentication_required= true;
		if(bm instanceof BabyMonitorUdtAuthentication)
		{
			channel_id = ((BabyMonitorUdtAuthentication)bm).getChannelID();
			udtLocalPort = ((BabyMonitorUdtAuthentication)bm).getUdtLocalPort();
			
			mac = ((BabyMonitorUdtAuthentication)bm).getDeviceMac();
			
			try {
				eChannelId = ((BabyMonitorUdtAuthentication)bm).getEncChannelID();
				eMac = ((BabyMonitorUdtAuthentication)bm).getEncMac();
			} catch (Exception e) {
				Log.d("mbp", "FAILED to get encrypted data ");
				eChannelId  = null;
				eMac = null; 
				e.printStackTrace();
			}
			
		}
		else
		{
			Log.e("mbp","UDT Streamer: Invalid BM authentication object ");
		}
	}
	
	
	
	private boolean tryUDTConnectToAudio() throws IOException
	{
		String  msg;
		byte[] data1 = new byte[1024];
		
		sockdscp = null;
		
		msg = PublicDefine.GET_A_STREAM_UDT_CMD + 
				PublicDefine.GET_AV_STREAM_PARAM_1 + session_key; //+"&channelID="+channelID;
		InetSocketAddress myOwnAddr = new InetSocketAddress(udtLocalPort);
		InetSocketAddress CamInetAddress = new InetSocketAddress(device_ip, device_port);
		int retries = MAX_CONN_RETRIES; 
		do
		{
			try {
				sockdscp = new SocketUDT(TypeUDT.STREAM);
				sockdscp.bind(myOwnAddr);
				//sockdscp.setSoTimeout(5000);
				sockdscp.connect(CamInetAddress);
				data1 = msg.getBytes();
				sockdscp.send(data1);

				/*TODO: -  Have to make sure we got the reply here
				 *  otherwise need to return NULL or throw some exception
				 *- Check the response by trying to receive some data on this socket
				 */
				break; 

				
			}
			catch (ExceptionUDT eudt)
			{
				eudt.printStackTrace();
				if (sockdscp!= null)
				{
					sockdscp.close();
					sockdscp = null; 
				}
				
				retries--;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Log.d("mbp","retry : " + retries);
			}

		}  while ( (retries >0) && (isRunning != false)); 
		
		if (isRunning == false)
		{
			if (sockdscp != null)
			{
				sockdscp.close();
				sockdscp = null; 
			}
			
			return false; 
		}

		InputStream is = null; 
		
		if (sockdscp != null)
		{
			is = sockdscp.getUDTInputStream();
		}
		else
		{
			//Go relay here..
			
			
			mHandler.dispatchMessage(Message.obtain(mHandler, MSG_VIDEO_STREAM_SWICTHED_TO_UDT_RELAY));
			
			sockdscp = useSecureRelaySever(mac,channel_id);
			
			if (sockdscp != null)
			{
				Log.d("mbp","Get input stream from relay sock");
				is = sockdscp.getUDTInputStream();
				isGoingThruRelay = true; 
			}
			else
			{
				Log.d("mbp","Failed to connect to relay");
				return false;
			}
		}
		

		_inputStream = new DataInputStream(new BufferedInputStream(is));

		
		return true; 
	}
	
	
	
	private boolean tryUDTConnect() throws IOException
	{
		String  msg;
		byte[] data1 = new byte[1024];
		sockdscp = null;
        
      
		 
    	if (device_ip == null || session_key == null || channel_id == null)
		{ 
			return false; 
		}
		
    	/*byte[] ip_arr = new byte[4];
    	boolean ret = Util.convertIpStringToByteArray(device_ip, ip_arr);
		InetAddress cameraIPAddress  = InetAddress.getByAddress(ip_arr);*/
		//InetSocketAddress CamInetAddress = new InetSocketAddress(cameraIPAddress.getHostName(), device_port);
		
		InetSocketAddress CamInetAddress = new InetSocketAddress(device_ip, device_port);
		
		
		msg = PublicDefine.GET_AV_STREAM_UDT_CMD + 
				PublicDefine.GET_AV_STREAM_PARAM_1 + session_key;
		InetSocketAddress myOwnAddr = new InetSocketAddress(udtLocalPort);
		
		int retries = MAX_CONN_RETRIES; 
		sockdscp = null; 
		
		//FOR TEST FOR TEST 
//		Log.d("mbp", "Close session FORCE REEEEEELAYYYYYYYYYYYYYYYYYYYY ");
//
//		retries =0; 
//		try {
//			Log.d("mbp", "sleep for 20s for camera timeout");
//			Thread.sleep(25000);
//		} catch (InterruptedException e1) {
//			e1.printStackTrace();
//		} 
//		Log.d("mbp", "Wakeup and go relay .. yay!!#%#%$");
		
		
		
		while ( (retries >0) && (isRunning != false))
		{
			try {
				sockdscp = new SocketUDT(TypeUDT.STREAM);
				sockdscp.bind(myOwnAddr);
				sockdscp.setSoTimeout(5000);
				sockdscp.connect(CamInetAddress);
				data1 = msg.getBytes();
				sockdscp.send(data1);

				/*TODO: -  Have to make sure we got the reply here
				 *  otherwise need to return NULL or throw some exception
				 *- Check the response by trying to receive some data on this socket
				 */
				break; 

				
			}
			catch (ExceptionUDT eudt)
			{
				eudt.printStackTrace();
				if (sockdscp!= null)
				{
					sockdscp.close();
					sockdscp = null; 
				}
				
				retries--;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Log.d("mbp","retry : " + retries + " isRunning=" + isRunning);
			}
			
			

		} 

		
		
		
		
		if (isRunning == false)
		{
			if (sockdscp != null)
			{
				sockdscp.close();
				sockdscp = null; 
			}
			
			return false; 
		}
		InputStream is = null; 
		
		if (sockdscp != null)
		{
			is = sockdscp.getUDTInputStream();
		}
		else
		{
			//check flag useOldRelay
			//Go relay here..
			if (useOldRelay == true)
			{
				mHandler.dispatchMessage(Message.obtain(mHandler,
						MSG_VIDEO_STREAM_SWICTHED_TO_UDT_RELAY));
				sockdscp = useSecureRelaySever(mac,channel_id);

				if (sockdscp != null)
				{
					Log.d("mbp","Get input stream from relay sock");
					is = sockdscp.getUDTInputStream();
					isGoingThruRelay = true; 
				}
				else
				{
					Log.d("mbp","Failed to connect to relay");
					return false;
				}
			}
			else
			{
				mHandler.dispatchMessage(Message.obtain(mHandler,
						MSG_VIDEO_STREAM_SWICTHED_TO_UDT_RELAY_2));
				stop();
				return false;
			}
		}
		

		_inputStream = new DataInputStream(new BufferedInputStream(is));

		//20121126: ?? Why?? only non-relay that we have readTimer?
		if (isGoingThruRelay == false)
		{
			startReadTimer(sockdscp);
		}
		
		
		return true;

	}
	
	
	private SocketUDT useSecureRelaySever(String macAddress, String channelID)
	{
		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.GET_RELAY_SECURITY + 
				PublicDefine.GET_RELAY_SECURITY_PARAM_1 +  macAddress;
		
		//Log.d("mbp","REMOVE B4 RELEASE: getrelaysec http: " + http_cmd);
		
		
		URL url = null;
		HttpsURLConnection conn = null;
		DataInputStream inputStream = null;
		String response = null;
		int respondeCode = -1;
		String relaySk = ""; 
		String inputLine, output=""; 
		try {
			url = new URL(http_cmd );
			conn = (HttpsURLConnection)url.openConnection();


			conn.addRequestProperty("Authorization", "Basic " +
					((BabyMonitorUdtAuthentication)this.bm_auth).getBase64EncodeUserPass());
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(20000);
			respondeCode = conn.getResponseCode();
			Log.d("mbp","getrelaysec respondeCode: " + respondeCode);
			if (respondeCode == 200)
			{
				BufferedReader secureData = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                
                while ((inputLine = secureData.readLine()) != null) {
                    output += inputLine;
                }
                
                //Macaddress:macAddr<br>Secret_key:encodedKey
                relaySk=output.split("<br>")[1].split(":")[1];
                Log.d("mbp","Output: " + output +
                		" relaySk:" + relaySk); 

			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			respondeCode = -1; 
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
			se.printStackTrace();
			respondeCode = -1; 
		}
		catch (IOException e) {
			e.printStackTrace();
			
			respondeCode = -1; 
		}

		if (relaySk == null)
		{
			return null; 
		}

		String relayTok = BabyMonitorUdtAuthentication.calculateRelayToken(relaySk, 
				macAddress,
				(BabyMonitorUdtAuthentication) bm_auth);
		
		BabyMonitorUdtAuthentication authentication = ((BabyMonitorUdtAuthentication) bm_auth);
		
		
		authentication.setRelayToken(relayTok);
		
		
		
		InetSocketAddress relay_conn_addr ; 
		SocketUDT relay_socket;
		
		
		relay_conn_addr = new InetSocketAddress(PublicDefine.RELAY_SERVER, PublicDefine.RELAY_SERVER_PRT);
		if(relay_conn_addr==null)
		{
			Log.d("mbp","Server addr null");
			return null;
		}

		try {
			relay_socket = new SocketUDT(TypeUDT.STREAM);
			Log.d("mbp","RelaySock Set Timeout ");
			relay_socket.setSoTimeout(5000);
			
			Log.d("mbp","RelaySock Get Timeout:" + relay_socket.getSoTimeout());
			
			try{
				relay_socket.connect(relay_conn_addr);
			}catch(Exception  ex){
				ex.printStackTrace(); 
				return null;
			}
			String message = authentication.getRelayToken();
			Log.d("mbp","Start connect to relay now... " +PublicDefine.RELAY_SERVER +
					" w token: " + message);
			
			relay_socket.send(message.getBytes());
			
			
			byte[] recvdata = new  byte[50];
			Log.d("mbp","Port ::"+relay_socket.getLocalInetPort());
			relay_socket.receive(recvdata);

			
			if(new String(recvdata).contains("&&&"))
			{
				/*
				 *####CAMERA CONNECT SUCCESS  WILL BE HERE 
				 */

				Log.d("mbp",new String(recvdata));
			}
			else if(new String(recvdata).contains("###"))  
			{
				//Relay error code -
				Log.d("mbp",new String(recvdata)); 
				return null;
			}
			else if(new String(recvdata).contains("@@@"))
			{
				Log.d("mbp","failure :"+new String(recvdata));
				return null;
			}
			
		}
		catch(ExceptionUDT ex){
			ex.printStackTrace(); 
			return null;
		}
		
		
		udtLocalPort = relay_socket.getLocalInetPort();
		device_ip =PublicDefine.RELAY_SERVER;
		device_port = PublicDefine.RELAY_SERVER_PRT;
		
		
		
		
		//* update ip,port,sskey
		authentication.setCamIp(device_ip);
		authentication.setCamPort(device_port); 
		authentication.setSSKey(""); //we are going thru relay 
		authentication.setUdtLocalPort(udtLocalPort); 

		
		sendKeepAliveMessage(relay_socket);

		return relay_socket;
	}
	

	private boolean iAmReading; 
	private Thread dataReadWD; 
	protected  void kickReadWatchDog()
	{
		synchronized (UDTVideoStreamer.this)
		{
			iAmReading = true; 
		}
	}
	private void startReadTimer(final SocketUDT socketUDT)
	{
		iAmReading = true; 
		dataReadWD = new Thread(
				new Runnable(){
					public void run()
					{
						
						while (!socketUDT.isClosed())
						{
							try {
								//10sec
								Thread.sleep(10000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							} 
							
							
							if (iAmReading == false)
							{
								Log.d("mbp", "WD:NO kicking.. close sock now.. ");
								if (socketUDT != null && !socketUDT.isClosed())
								{
									try {
										socketUDT.close();
									} catch (ExceptionUDT e) {
										e.printStackTrace();
									}
								}
								break;
							}
							else
							{
								
								Log.d("mbp", "WD:reset the flag"); 
								
								synchronized (UDTVideoStreamer.this) {
									iAmReading = false; 
								}
								
							}
						}
						
						
						Log.d("mbp", "WD: exit"); 
						
					}
				});

		dataReadWD.start();
		
	}
	private void stopReadTimer()
	{
		if (dataReadWD != null && dataReadWD.isAlive())
		{
			synchronized (UDTVideoStreamer.this) {
				iAmReading = false; 
			}
			dataReadWD.interrupt();
		}
	}
	
	
	private void sendKeepAliveMessage(final SocketUDT socketUDT)
	{
		Thread t = new Thread(
				new Runnable(){
					public void run()
					{
						int counter = 1;
						Log.d("mbp","Keep Alive: Start ");
						while(socketUDT.isOpen())
						{
							try {
								String message = "hello" + String.valueOf(counter);
								//Log.d("mbp","Keep Alive: message:" + message);
								socketUDT.send(message.getBytes());
								//Log.d("mbp","Keep Alive: send done");

							}
							catch(ExceptionUDT ex){
								ex.printStackTrace();
							}

							// send message every 3 sec
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							counter++;
							
						}
						
						Log.d("mbp","Keep Alive: STOP ");
					}
				});

		t.start();
	}
	
	
	
	protected  StreamSplit streamSplitInit(boolean withVideo) throws IOException
	{
		StreamSplit ssplit = null;
		boolean isConnected = false; 

		//Log.d("mbp","UDT_ streamSplitInit(): start" ); 
		//bm_auth =queryCameraInfoFromServer();
		bm_auth = queryEncCameraInfoFromServer(); 
		
		if (bm_auth == null)
		{
			//Camera is not available 
			//treat it as . failed to connect 
			return null; 
		}
		else
		{
			Log.d("mbp","Recv new auth: " + bm_auth); 
		}
		setRemoteAuthentication(bm_auth);
		
		if (withVideo)
		{
			//Original code
			isConnected = tryUDTConnect();
			
		}
		else
		{
			isConnected = tryUDTConnectToAudio();
		}

		if (isConnected == false)
			return null;

		try {
			//
			// Read Headers for the main thing...
			//
			ssplit = new StreamSplit(_inputStream);
		} catch(Exception ex) {
			for(IVideoSink videoSink : _videoSinks) {
				videoSink.onInitError(ex.getMessage());
			}
			return null;
		}

		_collecting = true;

		ssplit.setBoundary(BOUNDARY_MARKER);

		return ssplit;
	}
	
	
	
	
	
	private BabyMonitorUdtAuthentication queryEncCameraInfoFromServer() throws IOException
	{
		/********* start connection with server *************/
		InetAddress uDT_server_IP=null;
		StringBuffer buffer = null;
		SocketUDT socket = null;
		InetSocketAddress uDT_Server_Addr = null;
		byte[] data = new byte[80];
		byte[] sdata = new byte[24];
		BabyMonitorUdtAuthentication authentication = null;
		int udtLocalPort; 
		int response_len;
		String output = null;
		
		
		if ( eChannelId == null || eMac == null)
		{
			//
			return null; 
		}

		
		
		System.arraycopy(eMac, 0, sdata, 0, eMac.length);
		System.arraycopy(eChannelId, 0, sdata, eMac.length, eChannelId.length);
		
		authentication = (BabyMonitorUdtAuthentication)bm_auth;
		Log.d("mbp","-- channelid: " + authentication.getChannelID());
		
		uDT_server_IP = InetAddress.getByName(PublicDefine.UDT_SERVER);
		uDT_Server_Addr = new InetSocketAddress(uDT_server_IP, PublicDefine.UDT_SERVER_PORT);
		
		
		int  retries = 10;
		boolean canConnect = false; 
		while (retries -- > 0)
		{
			socket = new SocketUDT(TypeUDT.STREAM);

			//socket.setSoTimeout(5000);
			socket.connect(uDT_Server_Addr);

			udtLocalPort = socket.getLocalInetPort();

			socket.send(sdata);

			response_len = socket.receive(data);
			//Log.d("mbp","response len = " + response_len); 
			if (bm_auth == null)
			{
				return null; 
			}
			byte [] response = null; 
			try {
				response =((BabyMonitorUdtAuthentication) bm_auth).decodeServerMessage(data, 0, data.length);
			} catch (Exception e) {
				Log.d("mbp", "FAILED to decrypt");
				response = null; 
				e.printStackTrace();
			} 
			socket.close();

			byte[] errCode = new byte[4];
			byte[] CameraIp = new byte[8];
			byte[] CameraPort = new byte[4];
			byte[] sessionKey = new byte[64];

			//Get encrypted MAC & Channelid from client message
			System.arraycopy(response, 0, errCode, 0, 4);
			System.arraycopy(response, 4, CameraIp, 0, 8);
			System.arraycopy(response, 12, CameraPort, 0, 4);
			System.arraycopy(response, 16, sessionKey, 0, 64);
			int resCode=0;
			try{
				//String err=new String(errCode);
				resCode = Integer.valueOf(new String(errCode), 16).intValue();
			}
			catch (Exception e) {
				
				e.printStackTrace();
				Log.d("mbp","May be Decryption error retries: " + retries);
				continue; // retry; 
			}

			Log.d("mbp","Decrypted ErrCode : " + resCode);
			switch (resCode) {
			case 200:
				int port = Integer.valueOf(new String(CameraPort), 16).intValue();
				//Log.d("mbp"," port : " + port);
				String sskey ;
				sskey = new String(sessionKey);
				//Log.d("mbp"," sskey : " + sskey);

				String ip = new String(CameraIp);
				ip = Util.convertHexToIP(ip); 


				//* update ip,port,sskey
				authentication.setCamIp(ip);
				authentication.setCamPort(port); 
				authentication.setSSKey(sskey);
				authentication.setUdtLocalPort(udtLocalPort); 

				canConnect = true; 
				//IF IT"S OK .. set retry = 0; to break the loop 
				retries = 0; 
				break;
			
				
				
			default:
				
				Log.d("mbp"," rescode: " + resCode + "- retries" + retries);
				
				canConnect = false; 
				internalError = resCode; 
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					
				}
				
				break;

			}


			
			
		}
        
		if (canConnect == false)
		{
			//Non-recoverable Internal error
			// set this flag to stop the main video thread and set the error 
			internalNonRecoverableError = true; 
			//force authentication object to null
			authentication = null; 
		}
		
		
        
		/********* end of connection with server *************/
		return authentication; 
	}

	public void initQueries() 
	{
		String request,res ;
		//// NEW WAY TO SEND COMMAND

		request = PublicDefine.GET_MELODY_VALUE;
		
		res = UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorUdtAuthentication)bm_auth).getDeviceMac(),
				((BabyMonitorUdtAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorUdtAuthentication)bm_auth).getUser(),
				((BabyMonitorUdtAuthentication)bm_auth).getPass());
		
		updateMelodyStatus(res);
		
		//set default QVGA
//		request = PublicDefine.SET_RESOLUTION_QVGA;
//		
//		UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorUdtAuthentication)bm_auth).getDeviceMac(),
//				((BabyMonitorUdtAuthentication)bm_auth).getChannelID(), 
//				request ,
//				((BabyMonitorUdtAuthentication)bm_auth).getUser(),
//				((BabyMonitorUdtAuthentication)bm_auth).getPass());
//		
//		request = PublicDefine.GET_RESOLUTION;
//		
//		
//		res = UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorUdtAuthentication)bm_auth).getDeviceMac(),
//				((BabyMonitorUdtAuthentication)bm_auth).getChannelID(), 
//				request ,
//				((BabyMonitorUdtAuthentication)bm_auth).getUser(),
//				((BabyMonitorUdtAuthentication)bm_auth).getPass());
		
		SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);
		if (img_res == PublicDefine.RESOLUTON_QVGA)
		{
			request = PublicDefine.SET_RESOLUTION_QVGA;
		}
		else
		{
			request = PublicDefine.SET_RESOLUTION_VGA;
		}
		UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorUdtAuthentication)bm_auth).getDeviceMac(),
				((BabyMonitorUdtAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorUdtAuthentication)bm_auth).getUser(),
				((BabyMonitorUdtAuthentication)bm_auth).getPass());

		if (_resolutionUpdater != null) {
			_resolutionUpdater.updateResolution(img_res);
		}
	}
	
	
	protected  void checkDisconnectReason()
	{
		closeCurrentSession();
		Log.e("mbp", "closeCurrentSession done" ); 
		
		super.checkDisconnectReason();
		// no matter what the reason was---> send a stop session to kill the current session
		
		
		
	}
	
	
	protected void closeCurrentSession()
	{
		Log.d("mbp", "Closing session---on BMport: "+device_port+ " frmlocalport:"+ udtLocalPort );
		if (((BabyMonitorUdtAuthentication)bm_auth) == null)
		{
			//forget it
			Log.d("mbp", "bm_auth = null"); 
			return; 
		}
		
		
		
		/* @param urls[0] : mac 
		 *        urls[1] : channelId 
		 *        urls[2] : camera query (command part only, eg: get_skp_volume) 
		 *        urls[3] : user
		 *      - urls[4] : pass  
		*/
		
		Log.d("mbp", "Closing session--- ");
		String request = PublicDefine.CLOSE_UDT_SESSION;
		
		String response = UDTRequestSendRecv.sendRequest_via_stun(((BabyMonitorUdtAuthentication)bm_auth).getDeviceMac(),
				((BabyMonitorUdtAuthentication)bm_auth).getChannelID(), 
				request ,
				((BabyMonitorUdtAuthentication)bm_auth).getUser(),
				((BabyMonitorUdtAuthentication)bm_auth).getPass());
				
	}
	
	public void stop(int reason)
	{
		super.stop(reason);
		
		if (sockdscp!= null)
		{
			try {
				Log.d("mbp", "CLOSE UDT SOCKET "); 
				sockdscp.close();
			} catch (ExceptionUDT e) {
				Log.d("mbp","UDT close exception: " + e.getLocalizedMessage());
			}
			sockdscp = null;
		}
		
		Log.d("mbp", "Stop read timer ");
		stopReadTimer();
		
		
	}
	
	public static String getUDTErrorMessage(Context context, int errorCode)
	{
		String output = context.getString(R.string.failed_to_communicate_with_udt_server_); 
		switch (errorCode) {
		case 200:
			//Success
			output =context.getString(R.string.success); 
			break;
		case 400:
			// response.sendError(400, "Invalid parameters recieved.");
			output= context.getString(R.string.invalid_parameters_recieved_);
			break;
		case 401:
			//response.sendError(401, "Camera not registred in portal.");
			output= context.getString(R.string.camera_not_registered_in_portal);
			break;
		case 701:
			//response.sendError(701, "Camera not connected.");
			output= context.getString(R.string.camera_not_connected_);
			break;
		case 702:
			// response.sendError(702, "Camera is busy or not available.");
			output= context.getString(R.string.camera_is_busy_or_not_available_);
			break;
		case 703:
			// response.sendError(703, "Unable to communicate with camera.");
			output= context.getString(R.string.unable_to_communicate_with_camera_);
			break;
		case 704:
			// response.sendError(704, "Unable to communicate with server.");
			output= context.getString(R.string.unable_to_communicate_with_server);
			break;
		case 705:
			//response.sendError(705, "Unable to communicate with server.");
			output= context.getString(R.string.unable_to_communicate_with_server_);
			break;
		case 706:
			//response.sendError(706, "Server not reachable or not started.");
			output= context.getString(R.string.server_not_reachable_or_not_started_);
			break;
		case 707:
			// response.sendError(707, "Camera not connected.");
			output= context.getString(R.string.camera_not_connected_);
			break;
		case 708:
			// response.sendError(708, "Already your previous requesting is process.");
			output= context.getString(R.string.already_your_previous_requesting_is_process_);
			break;
		case 709:
			//response.sendError(709, "Camera disconnected.");
			output= context.getString(R.string.camera_disconnected_);
			break;
		default:
			break;
		}
		return output;
	}
}
