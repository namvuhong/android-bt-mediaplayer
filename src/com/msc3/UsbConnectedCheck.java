package com.msc3;


import java.io.FileNotFoundException;
import java.io.FileReader;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class UsbConnectedCheck implements Runnable{

	private static final String USB_CONFIGURATION_PATH = "/sys/class/switch/usb_configuration/state";
	private Context mContext; 
	private static final int TIMEOUT_30_MINS_IN_5_SEC = 360;//360 * 5sec = 30 mins

	private static final int UNKNOWN_STATE = 0;
	private static final int WAITING_TO_30MIN_TIMEOUT_STATE = 1; 
	private static final int IN_30MIN_TIMEOUT_STATE = 2; 
	private static final int USB_NOT_AVAILABLE = 3; 

	private int state; 

	

	private boolean running; 
	private Thread _worker;
	private boolean isPoweredByUsb;
	private int time_count ; 
	private static UsbConnectedCheck instance = new UsbConnectedCheck();

	public static synchronized UsbConnectedCheck getInstance(Context c)
	{
		instance.setContext(c);
		return instance; 
	}



	private UsbConnectedCheck () 
	{
		mContext = null; 
		running = false;
		isPoweredByUsb = false;
	}




	public synchronized void setContext (Context c)
	{
		mContext =c;
	}

	/**
	 * start usb poling and create a new thread
	 * 
	 */
	public void startPoll()
	{

		if (running == false)
		{
			Log.d("mbp", "start polling usb");

			running = true; 

			_worker = new Thread(this,"usb polling" );
			_worker.start();
		}
	}

	/**
	 * Stop usb poling and also terminate the thread
	 * 
	 */
	public void stopPoll()
	{
		if (running)
		{
			running = false; 

			try {
				_worker.join(1000);
			} catch (InterruptedException e) {
			}
		}

	}

	public boolean isPolling()
	{
		return running;
	}


	/**
	 * Set by external party. 
	 * @param plugged
	 */
	public void setUSBpluggedIn(boolean plugged)
	{
		isPoweredByUsb =  plugged;
	}


	/**
	 * 
	 * Called to bring it out of IN_30MIN_TIMEOUT_STATE
	 * then the counting to 30minutes timeout will also reset
	 */
	public void reset()
	{
		synchronized (this) {
			state = USB_NOT_AVAILABLE;
			time_count = 0; 
		}
		
	}
	
	
	public void run ()
	{
		WakeLock wl = null; 
		time_count = 0; 
		PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);

		state = USB_NOT_AVAILABLE;

		while (running)
		{

			if (isPoweredByUsb)
			{
				//Keep screen on
				if  (state != IN_30MIN_TIMEOUT_STATE) 
				{
					if (wl == null || !pm.isScreenOn()) 
					{
						wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP |
								PowerManager.ON_AFTER_RELEASE, "USB POLL");
						wl.acquire();
					}

					state = WAITING_TO_30MIN_TIMEOUT_STATE;
					time_count +=1;

				}

			}
			else
			{
				//Do not Keep screen on

				if ( wl != null && wl.isHeld() )
				{
					wl.release();
					wl = null;
				}

				state = USB_NOT_AVAILABLE;
				time_count =0;
			}

			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}


			/* check if we reach 30 mins timeout */
			if (time_count >= TIMEOUT_30_MINS_IN_5_SEC)
			{
				Log.d("mbp", "Enter 30mins state");
				state = IN_30MIN_TIMEOUT_STATE;
				time_count = 400;//cap at this number so as not to overflow the integer range 
				//Log.d("mbp","Enter sleep time after 30mins");
				//go to sleep 
				if ( wl != null && wl.isHeld() )
				{
					wl.release();
					wl = null;
				}
			}

		}


		Log.d("mbp", "stop polling usb");
	}



}