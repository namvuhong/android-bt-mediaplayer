package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.msc3.comm.UDTRequestSendRecv;

import android.util.Base64;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.msc3.comm.UDTRequestSendRecv;

import android.util.Base64;
import android.util.Log;


public class DirectionDispatcher implements Runnable {

	private boolean shouldTerminate ;
	private Object lock;
	private int current_UD_direction, current_LR_direction;
	private boolean UD_direction_update_needed, LR_direction_update_needed;

	private  String http_req,http_pass, http_user;
	
	/*20120423: temporary solution - maintain the communication mode here. 
	 * Future work: Abstract the communication interface, and use it here as a generic method. 
	 *  @comm_mode will be removed.
	 */
	private int comm_mode ; 
	private String userToken;
	private String regId;
	/*************************/
	

	public DirectionDispatcher(String userToken, String regId)
	{
		lock = new Object();//Use mainly for synchronize purpose 

		shouldTerminate = false;
		UD_direction_update_needed = LR_direction_update_needed = false;
		current_UD_direction = DirectionTouchListener.DIRECTION_V_NON;
		current_LR_direction = DirectionTouchListener.DIRECTION_H_NON;

		this.userToken = userToken;
		this.regId = regId;
		comm_mode = 2; //UDT
	}


	/* @http_addres: is in the form of "http://x.y.z.t"*/
	public DirectionDispatcher(String http_address)
	{
		lock = new Object();//Use mainly for synchronize purpose 

		shouldTerminate = false;
		UD_direction_update_needed = LR_direction_update_needed = false;
		current_UD_direction = DirectionTouchListener.DIRECTION_V_NON;
		current_LR_direction = DirectionTouchListener.DIRECTION_H_NON;
		http_req = http_address +PublicDefine.HTTP_CMD_PART;
		comm_mode = 1; //HTTP 
 
	}

	private DirectionDispatcher()
	{
		lock = new Object();//Use mainly for synchronize purpose 

		shouldTerminate = false;
		UD_direction_update_needed = LR_direction_update_needed = false;
		current_UD_direction = DirectionTouchListener.DIRECTION_V_NON;
		current_LR_direction = DirectionTouchListener.DIRECTION_H_NON;

		http_req = PublicDefine.DEFAULT_HTTP+PublicDefine.HTTP_CMD_PART ;
	}
	public void terminate()
	{
		shouldTerminate = true;
	}

	public void postDirection(int direction)
	{
		synchronized(lock)
		{
			if ( (direction & 0x0F) != current_UD_direction)
			{
				current_UD_direction = direction & 0x0F;
				UD_direction_update_needed = true;
			}

			if ((direction & 0xF0) != current_LR_direction)
			{
				current_LR_direction = direction & 0xF0;
				LR_direction_update_needed = true;
			}
		}
	}

	public void run()
	{
		int UD_update_count = 0, UD_stop_count = 0;
		int LR_update_count = 0, LR_stop_count = 0 ;
		while (!shouldTerminate)
		{

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			UD_stop_count --;
			LR_stop_count --;
			//Cap at -100 to avoid overflow 
			if (UD_stop_count < -100)
			{
				UD_stop_count = -100; 
			}
			if (LR_stop_count < -100)
			{
				LR_stop_count = -100;
			}

			if (current_UD_direction != DirectionTouchListener.DIRECTION_V_NON)
			{
				UD_update_count ++;

			}
			else
			{
				UD_update_count =0 ;
				if (UD_direction_update_needed)
					UD_stop_count =2; 
			}

			if (current_LR_direction != DirectionTouchListener.DIRECTION_H_NON)
			{
				LR_update_count ++;
				LR_stop_count = 0;
			}
			else
			{
				LR_update_count =0 ;
				if (LR_direction_update_needed)
					LR_stop_count =2;
			}

			
			if (UD_direction_update_needed ||
					(UD_update_count >= 10) ||
					(UD_stop_count > 0)
					)
			{
				UD_update_count = 0;

				synchronized(lock)
				{
					UD_direction_update_needed = false;
				}

				send_direction(current_UD_direction);
			}

			if (LR_direction_update_needed ||
					(LR_update_count >= 10 ) ||
					(LR_stop_count >0)
					)
			{
				LR_update_count = 0;

				synchronized(lock)
				{
					LR_direction_update_needed = false;
				}
				send_direction(current_LR_direction);
			}

		}

	}

	
	
	private void send_direction(int direction)
	{
		switch(comm_mode)
		{
		case 1: //HTTP
			send_via_http_ignore_response(
					convert_to_http_request(direction));
			break; 
		case 2: //UDT
			String res = UDTRequestSendRecv.sendRequest_via_stun2(
					userToken, regId, convert_to_udt_request2(direction));
			Log.d("mbp", "Direction res: "+res);
			break;
		default:
			break; 
		}

	}


	/**
	 * @param req - of the form "http://<ip>:<port>/?action=command&command=<direction_command>
	 * 
	 */
	private void send_via_http_ignore_response(String req)
	{
		URL url;
		URLConnection conn;
		String contentType,response;
		DataInputStream inputStream;

		String usr_pass =  String.format("%s:%s", http_user, http_pass);
		
		
		try
		{
			url = new URL(req);
			conn = url.openConnection();
			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(1000);
			conn.setReadTimeout(2000);
			conn.getInputStream();
			contentType = conn.getContentType();
			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream()));
			/* make sure the return type is text before using readLine */
			if (contentType.equalsIgnoreCase("text/plain"))
			{
				response = inputStream.readLine();
				Log.d("mbp", "Direction res: "+response);
			}

		}
		catch (MalformedURLException e) {
			
			e.printStackTrace();
			
			Log.d("mbp", "Malformed ?? req: " + req);
			
		}
		catch (IOException ioe)
		{
			//ioe.printStackTrace();
			Log.e("mbp","Direction Dispatcher: "+ ioe.toString());
		}


	}

	private String convert_to_udt_request(int direction)
	{
		String direction_str = null;

		boolean updown_swap = true;
		boolean leftright_swap = false;


		/* UP DOWN */
		switch(direction)
		{
		case DirectionTouchListener.DIRECTION_V_NON:
			direction_str = PublicDefine.DIR_FB_STOP;
			break;
		case DirectionTouchListener.DIRECTION_DOWN:
			if (updown_swap)
				direction_str =PublicDefine.DIR_MOVE_FWD+"0.1";
			else
				direction_str =PublicDefine.DIR_MOVE_BWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_UP:
			if (updown_swap)
				direction_str = PublicDefine.DIR_MOVE_BWD+"0.1";
			else
				direction_str = PublicDefine.DIR_MOVE_FWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_H_NON:
			direction_str = PublicDefine.DIR_LR_STOP;
			break;
		case DirectionTouchListener.DIRECTION_LEFT:
			if (leftright_swap)
			{
				direction_str = PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			else
			{
				direction_str = PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			break;
		case DirectionTouchListener.DIRECTION_RIGHT:
			if (leftright_swap)
			{
				direction_str = PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			else
			{
				direction_str = PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			break;
		default:
			break;

		}
		
		/*if (direction_str != null)
		{
			direction_str =  PublicDefine.BM_HTTP_CMD_PART + direction_str;
		}*/
		
		
		return direction_str  ;
	}
	
	
	private String convert_to_udt_request2(int direction)
	{
		String direction_str = null;

		boolean updown_swap = true;
		boolean leftright_swap = false;


		/* UP DOWN */
		switch(direction)
		{
		case DirectionTouchListener.DIRECTION_V_NON:
			direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_FB_STOP;
			break;
		case DirectionTouchListener.DIRECTION_DOWN:
			if (updown_swap)
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_FWD+"0.1";
			else
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_BWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_UP:
			if (updown_swap)
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_BWD+"0.1";
			else
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_FWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_H_NON:
			direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_LR_STOP;
			break;
		case DirectionTouchListener.DIRECTION_LEFT:
			if (leftright_swap)
			{
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			else
			{
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			break;
		case DirectionTouchListener.DIRECTION_RIGHT:
			if (leftright_swap)
			{
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			else
			{
				direction_str = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			break;
		default:
			break;

		}		
		
		Log.d("mbp", "Direction cmd: "+direction_str);
		return direction_str  ;
	}


	/* Find out which direction this location is in relative with the center */
	private String convert_to_http_request(int direction)
	{
		String direction_str = null;

		boolean updown_swap = true;
		boolean leftright_swap = false;


		/* UP DOWN */
		switch(direction)
		{
		case DirectionTouchListener.DIRECTION_V_NON:
			direction_str = http_req+PublicDefine.DIR_FB_STOP;
			break;
		case DirectionTouchListener.DIRECTION_DOWN:
			if (updown_swap)
				direction_str = http_req+PublicDefine.DIR_MOVE_FWD+"0.1";
			else
				direction_str = http_req+PublicDefine.DIR_MOVE_BWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_UP:
			if (updown_swap)
				direction_str = http_req+PublicDefine.DIR_MOVE_BWD+"0.1";
			else
				direction_str = http_req+PublicDefine.DIR_MOVE_FWD+"0.1";
			break;
		case DirectionTouchListener.DIRECTION_H_NON:
			direction_str = http_req+PublicDefine.DIR_LR_STOP;
			break;
		case DirectionTouchListener.DIRECTION_LEFT:
			if (leftright_swap)
			{
				direction_str = http_req+PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			else
			{
				direction_str = http_req+PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			break;
		case DirectionTouchListener.DIRECTION_RIGHT:
			if (leftright_swap)
			{
				direction_str = http_req+PublicDefine.DIR_MOVE_LEFT+"0.1";
			}
			else
			{
				direction_str = http_req+PublicDefine.DIR_MOVE_RIGHT+"0.1";
			}
			break;
		default:
			break;

		}
		Log.d("mbp", "Direction cmd: " + direction_str);
		return direction_str;
	}
}
