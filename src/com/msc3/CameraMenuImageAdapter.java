package com.msc3;


import com.blinkhd.R;
import java.util.ArrayList;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class CameraMenuImageAdapter extends BaseAdapter {
	private Context mContext;

	private ArrayList<LabelValue> item;
	/* in case we need to enable only 1 item in the camera use this 
	 
	 */
	private int enabledPos; 

	public CameraMenuImageAdapter(Context c,ArrayList<LabelValue> item ) {
		mContext = c;
		this.item = item;
		
		//at the start just initialize it to sth 
		enabledPos = item.size() +1;
				
	}
	
	public int getCount() {
		return item.size() ;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	
	public boolean isEnabled (int position)
	{
		if (position >=0 && position < item.size())
		{
			
			//if it is set by setDisableAllButOne() .. then it should be < than item.size()
			if (enabledPos < item.size())
			{
				if (enabledPos == position)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true; 
			}
		}
		else
		{
			return false;
		}
	}
	
	public void setDisableAllButOne(int position)
	{
		enabledPos = position;
	}


	public boolean areAllItemsEnabled()
	{
		return false;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		//LinearLayout itemView = null;
		RelativeLayout itemView = null; 
		TextView label, value; 
		LabelValue lv = item.get(position);
		/* Stat setting ups the view*/
		if (convertView == null) {  // if it's not recycled, initialize some attributes
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService
					(Context.LAYOUT_INFLATER_SERVICE);
			itemView = (RelativeLayout) inflater.inflate(R.layout.bb_camera_menu_item, null);
		} else {
			
			itemView = (RelativeLayout) convertView;
			
		}
		label = (TextView) itemView.findViewById(R.id.MenuItemLabel);
		label.setText(lv.getLabel());
		value = (TextView) itemView.findViewById(R.id.MenuItemValue);
		value.setText(lv.getValue());

		
		//disable all but this pos
//		if (enabledPos < item.size())
//		{
//			if (enabledPos != position)
//			{
//				itemView.setBackgroundColor(Color.GRAY);
//			}
//			else
//			{
//				itemView.setBackgroundColor(Color.WHITE);
//			}
//		}
//		
		
		return itemView;
	}


}

class LabelValue {
	private String label, value;
	
	LabelValue(String label, String value)
	{
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public String toString()
	{
		return label;
	}
}

