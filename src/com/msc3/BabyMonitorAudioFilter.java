package com.msc3;

import android.util.Log;

public class BabyMonitorAudioFilter {
		
	private static final int FILTER_LENGTH = 55;
	private static final int DEFAULT_A0=1;
	private static final int DEFAULT_B0=1;
	
	private static short[] h0  = new short [FILTER_LENGTH+1] ;

	private static double [] fh0 = { 
		0.0003899077,
		0.0003062516,
		0.0017590476,
		0.0002086534,
		0.0015335877,
		0.0028061280,
		-0.0003851417,
		0.0040927360,
		0.0021616226,
		-0.0017131993,
		0.0066463090,
		-0.0040222859,
		-0.0032298256,
		0.0046921688,
		-0.0181493756,
		-0.0035995609,
		-0.0082344089,
		-0.0372516765,
		-0.0017345957,
		-0.0375706757,
		-0.0508023792,
		0.0020492690,
		-0.0875100876,
		-0.0373436471,
		0.0058773470,
		-0.1949621817,
		0.1539664418,
		0.7074890137,
		0.1539664418,
		-0.1949621817,
		0.0058773470,
		-0.0373436471,
		-0.0875100876,
		0.0020492690,
		-0.0508023792,
		-0.0375706757,
		-0.0017345957,
		-0.0372516765,
		-0.0082344089,
		-0.0035995609,
		-0.0181493756,
		0.0046921688,
		-0.0032298256,
		-0.0040222859,
		0.0066463090,
		-0.0017131993,
		0.0021616226,
		0.0040927360,
		-0.0003851417,
		0.0028061280,
		0.0015335877,
		0.0002086534,
		0.0017590476,
		0.0003062516,
		0.0003899077,
		0,
		};
	private short[] temp_voice ;
	
	BabyMonitorAudioFilter()
	{
		Log.d("mbp","audio filter started");
		temp_voice  = new short [FILTER_LENGTH+1] ;
		for (int i=0; i< FILTER_LENGTH; i++) {
		
			h0[i] = (short) (fh0[i]*0x7FFF);
		}
	}
	
	
	/**
	 * 
	 * wrapper for byte array
	 * 
	 * @param pSignal_Input
	 * @param InputSize
	 * @param pSignal_Output
	 * @param OutputSize
	 */
	public void filtering(byte []pSignal_Input,int InputSize,
			byte [] pSignal_Output, int OutputSize)
	{
		short  []input = new short[InputSize/2] ;
		short [] output= new short[OutputSize/2] ;
		int j =0;
		for (int i = 0; i< InputSize; i+=2 )
		{
			
			input[j] =  (short) (pSignal_Input[i+1] & 0xFF);
			input[j] = (short)  (pSignal_Input[j]<<8) ;
			input[j] += pSignal_Input[i]& 0xFF;
			
			j++;
		}
		
		filtering (input, InputSize/2,output, OutputSize/2);
		j =0;
		for (int i = 0; i< output.length;i++ )
		{
			
			pSignal_Output[j]  = (byte) (output[i] >>8) ;
		    pSignal_Output[j+1] = (byte) (output[i]);
		    
		    j+=2;
		    
		}
		
		/* DBG 
		for (int i = 0 ; i<input.length; i++)
		{
			Log.d("mbp", "i: " + input[i] + " --> o: " + output[i]);
		}
		*/
		
		
	}
	/**
	 * Note: InputSize == OutputSize
	 * 
	 * @param pSignal_Input
	 * @param InputSize
	 * @param pSignal_Output
	 * @param OutputSize
	 */
	public void filtering(short []pSignal_Input,int InputSize,
			short [] pSignal_Output, int OutputSize)
	{
		int Tmp_Sum=0;
		int i,j, k=0;
		int Tmp_Output0;
		
		//assert(InputSize == OutputSize);
		
		//System.arraycopy(pSignal_Input, 0, pSignal_Output, 0, OutputSize);
		
		for (i=0; i<FILTER_LENGTH; i++) {
			Tmp_Output0=0;
			for (j=0; j<(FILTER_LENGTH-i-1); j++) {
				Tmp_Output0 += temp_voice[i+j]* h0[j];
			}
			int offset = (FILTER_LENGTH-i-1);
			for (j=(FILTER_LENGTH-i-1); j<FILTER_LENGTH; j++) {
				Tmp_Output0 += pSignal_Input[j-offset]*h0[j];
			}
			Tmp_Sum = (Tmp_Output0>>16);
			if (Tmp_Sum > 32767) {
				pSignal_Output[k] = 32767;
			} 
			else if (Tmp_Sum < -32768) {
				pSignal_Output[k] = -32768;
			}
			else {
				pSignal_Output[k] = (short) Tmp_Sum;
			}
			
			//DBG: 
			//pSignal_Output[k] =pSignal_Input[i];
			
			k++;
		}

		for (i=FILTER_LENGTH; i<InputSize; i++) {
			Tmp_Output0=0;

			int offset = (FILTER_LENGTH-i-1);
			for (j=0; j<FILTER_LENGTH; j++) {
				Tmp_Output0+=(pSignal_Input[j-offset])*h0[j];
			}
			Tmp_Sum = (Tmp_Output0>>16);
			if (Tmp_Sum > 32767) {
				pSignal_Output[k] = 32767;
			} 
			else if (Tmp_Sum < -32768) {
				pSignal_Output[k] = -32768;
			}
			else {
				pSignal_Output[k] = (short) Tmp_Sum;
			}
			//DBG: by pass
			//pSignal_Output[k] =pSignal_Input[i];
			
			k++;
		}
		System.arraycopy(pSignal_Input, InputSize-(FILTER_LENGTH-1), temp_voice, 0, FILTER_LENGTH-1);
	}
}
