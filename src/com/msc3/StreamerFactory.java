package com.msc3;

import com.msc3.CamProfile;
import com.msc3.Streamer;
import com.msc3.VideoStreamer;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class StreamerFactory {
	
	public static final int STREAM_MODE_HTTP_LOCAL = 0;
	public static final int STREAM_MODE_HTTP_REMOTE = 1;
	public static final int STREAM_MODE_UDT = 2;
	public static final int STREAM_MODE_RELAY = 3;
	
	public static Streamer getStreamer(CamProfile cam, Handler h, Context mContext, String device_ip, int port)
	{
		Streamer _streamer = null ;
		int mode = cam.getRemoteCommMode();
		switch(mode)
		{
		case STREAM_MODE_UDT:
//			_streamer = new UDTVideoStreamer(h, mContext, device_ip, port);
//			Log.d("mbp", "Camera firmware version: "+cam.getFirmwareVersion());
//			if (cam.isSupportNewRelay())
//			{
//				Log.d("mbp", "--> Use new relay");
//				((UDTVideoStreamer)_streamer).setUseOldRelay(false);
//			}
//			else
//			{
//				Log.d("mbp", "--> Use old relay");
//				((UDTVideoStreamer)_streamer).setUseOldRelay(true);
//			}
			break; 
		case STREAM_MODE_HTTP_REMOTE:
			_streamer = new VideoStreamer( h, mContext, device_ip, port);
			break;
		case STREAM_MODE_RELAY:
			//_streamer = new RelayVideoStreamer(h, mContext, device_ip, port);
			break;
		default: //default case is to create a local video streamer
			_streamer = new VideoStreamer( h, mContext, device_ip,port);
			break; 	
			
		}
		
		return _streamer;
	}
}
