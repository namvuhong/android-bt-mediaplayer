package com.msc3;

public class StorageException extends Exception {
	public StorageException(String msg)
	{
		super(msg);
	}
}
