package com.msc3;


import com.blinkhd.R;
import android.app.Activity;
import android.view.GestureDetector.OnGestureListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.MotionEvent;
import android.view.View;

/* This fling gesturelistener does only 1 thing, to finish the activity */

public class FlingBackGestureListener implements OnGestureListener {

	private Activity mAct; 
	private View mView;
	public FlingBackGestureListener(Activity a, View v) 
	{
		mAct = a;
		mView = v;
	}

	@Override
	public boolean onDown(MotionEvent arg0) {
		return false;
	}

	@Override
	public boolean onFling(MotionEvent arg0, MotionEvent arg1, float velocityX,
			float velocityY) 
	{
		
		//fling to the right hand side 
		if (velocityX > 400.0 )
		{
			//Log.v("mbp", "onFling.....velocityX:"+ velocityX + " y:" + velocityY);  
			final float distanceTimeFactor = 0.4f;  
		    final float totalDx = (distanceTimeFactor * velocityX/2);  
		  
		    AnimationSet aSet = new AnimationSet(false);
		    
		    /* translate view */
		    TranslateAnimation animate = new TranslateAnimation(0, totalDx,0,  0);
			animate.setDuration(400);
			animate.setFillEnabled(true);
			animate.setFillAfter(true);
			animate.setAnimationListener(new AnimationListener() {
				
				public void onAnimationStart(Animation animation) {
					
				}
				public void onAnimationRepeat(Animation animation) {
				}
				@Override
				public void onAnimationEnd(Animation animation) {
					mAct.finish();
				}
			});
			 /* fade view */
			Animation fadeAnimation = AnimationUtils.loadAnimation(mAct, R.anim.fadeout);
			fadeAnimation.setDuration(500);
			fadeAnimation.setAnimationListener(new AnimationListener() {

			    public void onAnimationStart(Animation animation) {
			    }
			    public void onAnimationRepeat(Animation animation) {
			    }
			    public void onAnimationEnd(Animation animation) {
			    }
			});
			aSet.addAnimation(animate);
			aSet.addAnimation(fadeAnimation);
			
			mView.startAnimation(aSet);
		
		}
		
		return true;
	}

	@Override
	public void onLongPress(MotionEvent arg0) {

	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		return false;
	}


}
