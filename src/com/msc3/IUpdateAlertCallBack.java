package com.msc3;


public interface IUpdateAlertCallBack {

	public void pre_update(); 
	public void update_alert_success(); 
	
	public void update_alert_failed(int alertType, boolean enableOrDisable); 
}
