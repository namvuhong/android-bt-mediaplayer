package com.msc3;

import android.content.Context;

import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class VideoSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

	private SurfaceHolder mSurfaceHolder;
	private Handler mHandler = null;
	public static final int MSG_SURFACE_CREATED = 0xBABEBABE;

	public VideoSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
	}
	public VideoSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
	}

	public VideoSurfaceView(Context context) {
		super(context);
		mSurfaceHolder= getHolder();
		mSurfaceHolder.addCallback(this);
		
		/* TODO: Create a worker thread to handle update
		 * for e.g:   
			_thread = new TutorialThread(getHolder(), this);
		*/
	}

	SurfaceHolder get_SurfaceHolder()
	{
		return mSurfaceHolder;
	}
	
	public void setHandler(Handler h)
	{
		mHandler = h;
	}
	
	@Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub
		
    }
	
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    	
//    	Log.w("mbp","VideoView surface created");
    	if ( mHandler != null)
    	{
    		mHandler.dispatchMessage(
    				Message.obtain(mHandler,
    				               MSG_SURFACE_CREATED));
    	}
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    	 // simply copied from sample application LunarLander:
        // we have to tell thread to shut down & wait for it to finish, or else
        // it might touch the Surface after we return and explode
        boolean retry = true;
       
        /* TODO: synchronize closing with worker thread 
         * _thread.setRunning(false);
        while (retry) {
            try {
                _thread.join();
                retry = false;
            } catch (InterruptedException e) {
                // we will try it again and again...
            }
        }*/
    }
}
