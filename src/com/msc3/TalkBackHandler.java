package com.msc3;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.KeyEvent.DispatcherState;
import android.view.View.OnTouchListener;
import android.widget.Toast;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

/*------




NOT USED 






-----*/
/* finger press down or move: Start streaming voice from hp --> irabot
 *        release : stop streaming voice 
 * */
public class TalkBackHandler implements Handler.Callback
{
	private boolean recording = false;
	private PCMRecorder recorder = null;
	
	private String host ;
	private int port ;
	private Handler mHandler;
	
	public TalkBackHandler(Handler c,String host, int port)
	{
		this.host = host;
		this.port = port;
		
		mHandler = c;
		
		
	}
	
	private TalkBackHandler(Handler c) 
	{
		this.host = PublicDefine.DEVICE_IP;
		this.port = PublicDefine.DEFAULT_AUDIO_PORT;
		mHandler = c;
	}
	
//	public boolean onTouch(View v, MotionEvent event)
//	{
//		switch(event.getAction())
//		{
//		case MotionEvent.ACTION_DOWN:
//			break;
//		case MotionEvent.ACTION_UP:
//			if ( recording == false)
//			{
//				//disable sound playback
//				mHandler.dispatchMessage(Message.obtain(mHandler, 0xdeafbeef));
//				startTalkback ();
//				Toast.makeText(v.getContext(), "Start Talking!", Toast.LENGTH_LONG).show();
//			}
//			else
//			{
//				
//				stopTalkback();
//				Toast.makeText(v.getContext(), "Talkback disabled", Toast.LENGTH_LONG).show();
//				
//				//enable sound playback
//				mHandler.dispatchMessage(Message.obtain(mHandler, 0xbeefbeef));
//			}
//			break;
//		case MotionEvent.ACTION_MOVE:
//		default:
//			break;
//
//		
//		}
//		return true;
//	}

//	public void startTalkback ()
//	{
//		/*String file = Util.getTempFile();
//			//Create a socket an give its outputstream to PCMRecorder 
//			recorder = new PCMRecorder (file);
//			recorder.startRecording();
//			Log.d ("mbp", "tmp pcm file: "+file);*/
//
//		recorder = new PCMRecorder(host, port , new Handler(this));
//		recorder.startRecording();
//
//	}
//	
//	
//	public void stopTalkback()
//	{
//		Log.d ("mbp", "... Stop recording");
//		if (recorder != null)
//			recorder.stopRecording();
//
//		recording = false;
//		recorder = null;
//
//	}

	
	/* Handler callback */
	@Override
	public boolean handleMessage(Message msg) {
		
		
//		if ( msg.what == 0xdeaddead)
//		{
//			Log.d("mbp", "get Callback Handler msg");
//			/* this means audio recording thread has stopped 
//			 * because the outstream has some problem. 
//			 * -> Stop TalkBack now and ask usr to try again 
//			 */
//			stopTalkback();
//		}
		return false;
	}

}
