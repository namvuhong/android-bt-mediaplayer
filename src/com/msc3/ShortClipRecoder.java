package com.msc3;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;


import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;


/*
 * Connect to a given Camera, enable video streaming, 
 * Store a number of frame in a buffer, then return 
 */

public class ShortClipRecoder extends AsyncTask<CamProfile , String, CamProfile> {

	public static final int SUCCESS = 1; 
	public static final int REASON_AUTHORIZED_PWD_NEEDED = 2;
	public static final int REASON_NULL_CAM_PROFILE = 3;
	public static final int REASON_NULL_CONTENT_TYPE = 4;
	
	public ShortClipRecoder()
	{
		
	}
	/* Blocking way of recording 
	 * return false if there is some error during the process
	 * return true if the process completed successfully
	 * */
	public static synchronized int doRecordShortClip(CamProfile cam, boolean restart_after) {
		
		String http_addr = null;
		
		if (cam == null)
			return  REASON_NULL_CAM_PROFILE;
		
	
	
		
		http_addr = "http://"+cam.get_inetAddress().getHostAddress() + ":" + cam.get_port() ;
		/* Adjust to QQVGA mode, to reduce the clip size*/ 

		
		URL url = null;
		HttpURLConnection conn = null;
		DataInputStream inputStream = null;
		Hashtable headers = null;
		StreamSplit ssplit = null;
		int retries = 5;
		String usr = cam.getBasicAuth_usr();
		String pwd = cam.getBasicAuth_pass();
		
		if (usr == null) usr = "";
		if (pwd == null) pwd = "";
		String usr_pass =  String.format("%s:%s", usr, pwd);
		
		do {
			try {
				/*url = new URL(http_addr +"/?action=command&command=QQVGA160_120");
				conn =  (HttpURLConnection) url.openConnection();
				
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(2000);
				
				conn.addRequestProperty("Authorization", "Basic " +
				        Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				
				conn.connect();
				
				
				if (((HttpURLConnection) conn).getResponseCode() == 401)
				{
					Log.d("mbp", "Auth failed!");
					((HttpURLConnection) conn).disconnect();
					return REASON_AUTHORIZED_PWD_NEEDED;
				}
				
				String contentType = conn.getContentType();*/
				
				url = new URL(http_addr +"/?action=stream");
				conn =  (HttpURLConnection) url.openConnection();
				conn.addRequestProperty("Authorization", "Basic " +
				        Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn.connect();
				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream()));
				
				break;
			} 
			catch (SocketTimeoutException se)
			{
				Log.d("mbp", "ShortClipRecoder:SocketTimeoutException: " + se.getLocalizedMessage());
			}
			catch (ConnectException ce)
			{
				Log.d("mbp","ShortClipRecoder:ConnectException: " + ce.getLocalizedMessage());
				
			}
			catch (MalformedURLException e) {
				e.printStackTrace();
				return REASON_NULL_CONTENT_TYPE;
			} 
			catch (IOException e) {
				e.printStackTrace();
				return REASON_NULL_CONTENT_TYPE;
			} 
			
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			retries -- ;
			
		} while (retries > 0);

		
		
		headers = StreamSplit.readHeaders(conn);
		ssplit = new StreamSplit(inputStream);
		
		/*---- Find the boundary String */
		String ctype = (String) headers.get("content-type");

		if (ctype == null)
		{
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return REASON_NULL_CONTENT_TYPE;
		}
		
		int bidx = ctype.indexOf("boundary=");
		String boundary = StreamSplit.BOUNDARY_MARKER_PREFIX;
		if (bidx != -1) {
			boundary = ctype.substring(bidx + 9);
			ctype = ctype.substring(0, bidx);
			if (boundary.startsWith("\"") && boundary.endsWith("\""))
			{
				boundary = boundary.substring(1, boundary.length()-1);
			}
			if (!boundary.startsWith(StreamSplit.BOUNDARY_MARKER_PREFIX)) {
				boundary = StreamSplit.BOUNDARY_MARKER_PREFIX + boundary;
			}
			
			ssplit.setBoundary(boundary);
		}

		if (ctype.startsWith("multipart/x-mixed-replace")) {
			try {
				ssplit.skipToBoundary();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte[] data = new byte [10*1024];
		int i = 0; 
		/*----- read jpeg data and store them 
		 * For now, record just 1 frame
		 * */
		while (i ++ < 2)
		{
			try {
				headers = ssplit.readHeaders();
				
				ctype = (String) headers.get("content-type");
				if (ctype == null) {
					Log.e("mbp", "Throw exception ");
					break; 
				}
				
				if (ctype.startsWith("image/jpeg")) 
				{
					ctype = (String) headers.get("content-length");
					int data_len = Integer.parseInt(ctype);
					if ( data_len > 10*1024 )
					{
						data = new byte[data_len];
					}
					int actual_data_read = 0;
					actual_data_read = ssplit.readDataToBoundary(boundary,data, data_len );
					
					if (actual_data_read == 0) {
						break;
					}
					if (actual_data_read == -1) {
						//TODO
						break;
					}
					
					byte [] img = new byte[actual_data_read];
					System.arraycopy(data,0,img,0,actual_data_read);
					cam.getShortClip().add(img);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
		if (restart_after)
		{
			
			try {
				url = new URL(http_addr +"/?action=command&command=restart_system");
				conn = (HttpURLConnection) url.openConnection();
				conn.setAllowUserInteraction(true);
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(2000);
				conn.connect();
				
				((HttpURLConnection) conn).getResponseCode();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		return SUCCESS ;
	}
	
	
	@Override
	protected CamProfile doInBackground(CamProfile... params) {
		
		String http_addr = null;
		
		if (params[0] == null)
			return null;
		http_addr = "http://"+params[0].get_inetAddress().getHostAddress();

	
		
		
		URL url = null;
		URLConnection conn = null;
		DataInputStream inputStream = null;
		Hashtable headers = null;
		StreamSplit ssplit = null;
	
		
		try {
			//ADjust the resolution 
			url = new URL( http_addr +"/?action=command&command=QQVGA160_120");
			conn = url.openConnection();
			conn.getContentType();//just to make sure the HTTP request is executed
			
			
			//Pull the stream 
			url = new URL( http_addr +"/?action=stream");
			conn = url.openConnection();
			inputStream = new DataInputStream(
					new BufferedInputStream(conn.getInputStream(),64*1024));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		headers = StreamSplit.readHeaders(conn);
		ssplit = new StreamSplit(inputStream);
		
		/*---- Find the boundary String */
		String ctype = (String) headers.get("content-type");

		int bidx = ctype.indexOf("boundary=");
		String boundary = StreamSplit.BOUNDARY_MARKER_PREFIX;
		if (bidx != -1) {
			boundary = ctype.substring(bidx + 9);
			ctype = ctype.substring(0, bidx);
			if (boundary.startsWith("\"") && boundary.endsWith("\""))
			{
				boundary = boundary.substring(1, boundary.length()-1);
			}
			if (!boundary.startsWith(StreamSplit.BOUNDARY_MARKER_PREFIX)) {
				boundary = StreamSplit.BOUNDARY_MARKER_PREFIX + boundary;
			}
			
			ssplit.setBoundary(boundary);
		}

		if (ctype.startsWith("multipart/x-mixed-replace")) {
			try {
				ssplit.skipToBoundary();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte[] data = new byte [10*1024];
		int i = 0; 
		/*----- read jpeg data and store them */
		while (i ++ < 100)
		{
			try {
				headers = ssplit.readHeaders();
				
				ctype = (String) headers.get("content-type");
				if (ctype == null) {
					Log.e("mbp", "Throw exception ");
					break; 
				}
				
				if (ctype.startsWith("image/jpeg")) 
				{
					ctype = (String) headers.get("content-length");
					int data_len = Integer.parseInt(ctype);
					int actual_data_read = 0;
					actual_data_read = ssplit.readDataToBoundary(boundary,data, data_len );
					
					if (actual_data_read == 0) {
						break;
					}
					if (actual_data_read == -1) {
						//TODO
						break;
					}
					
					byte [] img = new byte[actual_data_read];
					System.arraycopy(data,0,img,0,actual_data_read);
					params[0].getShortClip().add(img);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		
		
		return params[0];
	}

}
