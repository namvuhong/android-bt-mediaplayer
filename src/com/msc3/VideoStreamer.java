package com.msc3;

import java.util.ArrayList;



import java.util.Hashtable;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.SntpClient;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

/**
 * @author phung
 * 
 * 
 * typedef struct mlsIballHeaderVA_st
	{
	    unsigned char flagReset;
	    char  abLenAudio[4];
	    char  abIndexCurrentJpgFrame[4];
	    char  resolution;
	    unsigned char countOFbuffAudio;
	    char abTempValue[4];//temperature value 20111103
	}mlsIballHeaderVA_st;
	 
	 
	 
	New struct
	typedef struct mlsIballHeaderVA_st
	{
	unsigned char flagReset;
	char abLenAudio[4];
	char abIndexCurrentJpgFrame[4];
	char resolution;
	unsigned char countOFbuffAudio;
	char abTempValue[4];//temperature value 20111103
	 
	long long frame_ts;
	long long pic_ts;
	long long audio_ts;
	char dummy[16];
	 
	}mlsIballHeaderVA_st;
 * 
 *
 */
public class VideoStreamer extends Streamer {
	
	
	
	private static final int RESET_FLAG_MASK  = 0x01; 
	
	private static final int AUDIO_TYPE_MASK  = 0x06;
	private static final int AUDIO_TYPE_PCM   = 0x02;
	private static final int AUDIO_TYPE_ADPCM = 0x00;
	
	private static final int HEADER_VERSION_MASK = 0x18;
	private static final int HEADER_IBALL_VERSION = 0x00;
	private static final int HEADER_EXTENDED_VERSION = 0x08;
	
	
	protected ArrayList<IVideoSink> _videoSinks;
	protected ITemperatureUpdater _tempUpdater;
	protected IMelodyUpdater _melodyUpdater;
	protected IResUpdater _resolutionUpdater; 
	protected DataInputStream _inputStream;
	protected boolean _collecting, isRunning, _enableAudio;
	protected int _imgidx;
	private static final int IMG_FLUFF_FACTOR = 1;
	private long _startTime;
	// private int _resolutionJpeg;
	private int _resetFlag;
	private int _resetAudioBufferCount = 0;
	private int _imgCurrentIndex;

	protected boolean enableVideo;
	protected boolean viewingSessionHasTimeout;
	protected boolean stopped_by_caller;
	protected boolean internalNonRecoverableError;
	protected int internalError;
	
	
	
	/* 20130124: hoang
	 * use for session key mismatched
	 */
	protected boolean session_key_mismatched;
	protected int session_key_mismatched_code;
	
	/* 20130218: hoang: issue 1222
	 * use for camera is not available in local mode
	 */
	protected boolean camera_is_not_available;

	protected byte[] data;

	protected String device_ip;
	protected int device_port;

	protected int imageResolutionJpg = PublicDefine.RESOLUTON_QVGA;// PublicDefine.RESOLUTON_VGA;
	protected Handler mHandler;
	protected Context mContext;

	protected String session_key;
	protected boolean authentication_required;
	protected String http_usr, http_pass;

	protected byte[] dataPcm;
	protected int dataPcm_len;

	protected static final String REMOTE_TIMEOUT = "ERROR-602: Remote Camera has timeout";
	public static final String SESSION_KEY_MISMATCHED = "Session key mismatched";
	public static final String CAMERA_IS_NOT_AVAILABLE = "Camera is not available";

	/* store also the authentication - this could be UDT or HTTP authentication */
	protected BabyMonitorAuthentication bm_auth;
	protected String usrToken;
	
	private Tracker tracker;
	private EasyTracker easyTracker;

	/**
	 * Factory build hacking.. ****
	 */
	// 20120412: temperature is off by one on 03_31
	protected static final String version_03_031 = "get_version: 03_031";
	protected static final int DATA_PCM_MAX_LEN = 16160;
	protected boolean version_03_031_temperature_incr_by_one = false;

	/*******************/

	// new implementation using surfaceView, surfaceholder

	public VideoStreamer(Handler h, Context m, String device_ip, int port) {
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(m);
		tracker = easyTracker.getTracker();
		
		mHandler = h;
		mContext = m;
		_videoSinks = new ArrayList<IVideoSink>();
		
		/*
		 * allocate the space now, to avoid doing it everytime in a loop malloc
		 * is also costly.. i thought
		 */
		data = new byte[16 * 1024];

		// dataPcm = new byte[DATA_PCM_MAX_LEN];
		// dataPcm_len = 0;

		this.device_ip = device_ip;
		this.device_port = port;
		isRunning = true;
		stopped_by_caller = false;
		//use for check session key mismatched
		session_key_mismatched = false;
		camera_is_not_available = false;

		enableVideo = true;
		_enableAudio = true;

		http_usr = null;
		http_pass = null;
		bm_auth = null;
		usrToken = null;
	}

	public boolean isEnableVideo() {
		return enableVideo;
	}

	public void setEnableVideo(boolean enableVideo) {
		this.enableVideo = enableVideo;
	}

	public void setHTTPCredential(String http_usr, String http_pass) {
		this.http_usr = http_usr;
		this.http_pass = http_pass;

		if (http_usr == null)
			this.http_usr = "";
		if (http_pass == null)
			this.http_pass = "";

	}

	public void setRemoteAuthentication(BabyMonitorAuthentication bm) {
		this.bm_auth = bm;
		this.session_key = bm.getSSKey();
		authentication_required = true;
	}

	public void setTemperatureUpdater(ITemperatureUpdater t) {
		
		//if (t != null)
		{
			
			//Log.d("mbp", " ITemperatureUpdater is: " + t.getClass().getName());

			synchronized (this) {
				_tempUpdater = t;
			}
		
		}
	}

	public void setMelodyUpdater(IMelodyUpdater m) {
		_melodyUpdater = m;
	}
	
	public void setResUpdater(IResUpdater m) {
		_resolutionUpdater = m;
	}

	public void addVideoSink(IVideoSink videoSink) {
		synchronized (_videoSinks) {
			_videoSinks.add(videoSink);
		}
	}

	public void removeVideoSink(IVideoSink videoSink) {
		synchronized (_videoSinks) {
			if (_videoSinks.contains(videoSink))
				_videoSinks.remove(videoSink);
		}
	}

	public void enableAudio(boolean enableAudio) {
		_enableAudio = enableAudio;
	}

	private void enlarge_data(int new_size) {
		data = new byte[new_size];
	}

	public int setImageResolution(int vga_or_qvga) {

//		if ((vga_or_qvga == PublicDefine.RESOLUTON_QVGA)
//				|| (vga_or_qvga == PublicDefine.RESOLUTON_VGA)
//				|| (vga_or_qvga == PublicDefine.RESOLUTON_QQVGA)) {
//			imageResolutionJpg = vga_or_qvga;
//
//		}

		return 0;
	}

	public int getImageResolution() {
		return imageResolutionJpg;
	}

	public void initQueries() {

		URLConnection conn = null;
		URL stream_url = null;
		String contentType = null;
		DataInputStream inputStream = null;

		if (bm_auth == null)
		{
			String usr_pass = String.format("%s:%s", http_usr, http_pass);
			try {

				/* Get the current melody */
				stream_url = new URL("http://" + device_ip + ":" + device_port
						+ "/?action=command&command="
						+ PublicDefine.GET_MELODY_VALUE);
				conn = stream_url.openConnection();
				conn.addRequestProperty(
						"Authorization",
						"Basic "
								+ Base64.encodeToString(usr_pass.getBytes("UTF-8"),
										Base64.NO_WRAP));
				conn.setConnectTimeout(3000);
				conn.setReadTimeout(3000);

				inputStream = new DataInputStream(new BufferedInputStream(
						conn.getInputStream(), 4 * 1024));
				contentType = conn.getContentType();
				/* make sure the return type is text before using readLine */

				if ((contentType != null)
						&& contentType.equalsIgnoreCase("text/plain")) {
					updateMelodyStatus(inputStream.readLine());

				}
				inputStream.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {

			}
		}
		else
		{
			String request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_MELODY_VALUE;
			UDTRequestSendRecv.sendRequest_via_stun2(
					usrToken,
					((BabyMonitorHTTPAuthentication)bm_auth).getMacAddress(),
					request);
		}


		SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int img_res = settings.getInt(PublicDefine.PREFS_VQUALITY, PublicDefine.RESOLUTON_QVGA);

		String request = null;
		if (bm_auth == null)
		{
			if (img_res == PublicDefine.RESOLUTON_QVGA)
			{
				request = PublicDefine.SET_RESOLUTION_QVGA;
			}
			else
			{
				request = PublicDefine.SET_RESOLUTION_VGA;
			}

			final String device_address_port = device_ip + ":" + device_port;
			String http_addr = String.format(
					"%1$s%2$s%3$s%4$s", "http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART,
					request);

			HTTPRequestSendRecv
			.sendRequest_block_for_response(
					http_addr,
					PublicDefine.DEFAULT_BASIC_AUTH_USR,
					http_pass);
		}
		else
		{
			if (img_res == PublicDefine.RESOLUTON_QVGA)
			{
				request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.SET_RESOLUTION_QVGA;
			}
			else
			{
				request = PublicDefine.BM_HTTP_CMD_PART + PublicDefine.SET_RESOLUTION_VGA;
			}
			
			UDTRequestSendRecv.sendRequest_via_stun2(
					usrToken,
					((BabyMonitorHTTPAuthentication)bm_auth).getMacAddress(),
					request);
		}
		
		if (_resolutionUpdater != null)
		{
			_resolutionUpdater.updateResolution(img_res);
		}
		

	}

	protected void updateVideoResolution(String response) {

//		if ((response != null)
//				&& response.startsWith(PublicDefine.GET_RESOLUTION)) {
//			String str_value = response.substring(PublicDefine.GET_RESOLUTION.length() + 2); 
//			int icon_index = Integer.parseInt(str_value);
//			
//			
//			if (_resolutionUpdater != null) {
//				_resolutionUpdater.updateResolution(icon_index);
//			}
//
//		}
		
	}

	protected URLConnection tryConnectToAudio() throws IOException {

		HttpURLConnection conn = null;
		URL stream_url = null;
		int retry = 0;

		String usr_pass = String.format("%s:%s", http_usr, http_pass);
		try {
			/*
			 * this's CAMERA session authentication , not HTTP BASIC
			 * Authentication
			 */
			if (authentication_required == true) {
				stream_url = new URL("http://" + device_ip + ":" + device_port
						+ PublicDefine.GET_A_STREAM_CMD
						+ PublicDefine.GET_AV_STREAM_PARAM_1 + session_key);
				// Log.d("mbp","Audio only:" + stream_url);
			} else {
				stream_url = new URL("http://" + device_ip + ":" + device_port
						+ PublicDefine.GET_A_STREAM_CMD);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		int responseCode = 0;
		if (_inputStream != null) {
			_inputStream.close();
			_inputStream = null;
		}

		while (retry < 15 && isRunning) {
			try {

				conn = (HttpURLConnection) stream_url.openConnection();
				conn.addRequestProperty(
						"Authorization",
						"Basic "
								+ Base64.encodeToString(
										usr_pass.getBytes("UTF-8"),
										Base64.NO_WRAP));

				conn.setConnectTimeout(5000);
				//conn.setReadTimeout(5000);
				responseCode = conn.getResponseCode();

				_inputStream = new DataInputStream(new BufferedInputStream(
						conn.getInputStream()));
				break;
			} catch (IOException ex) {
				conn = null;
				if (ex instanceof SocketException) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				} else {
					Log.d("mbp", "tryConnectToAudio():connect error : " + ex);
					// ex.printStackTrace();
				}

				if (responseCode == 602) {
					// special case remote server has timeout
					throw new IOException(REMOTE_TIMEOUT);
				}

				retry++;
			}
		}

		// Double check
		if ((isRunning == false) && _inputStream != null) {
			conn = null;
			_inputStream.close();
			_inputStream = null;

		}

		return conn;
	}

	
	/* 20130124: hoang:
	 * check reponse code 601 in case session key is mismatch
	 */
	protected URLConnection tryConnect() throws IOException {
		URLConnection conn = null;
		URL stream_url = null;
		int retry = 0;

		String usr_pass = String.format("%s:%s", http_usr, http_pass);

		if (isRunning == false) {
			return null;
		}

		try {
			/*
			 * this's CAMERA session authentication , not HTTP BASIC
			 * Authentication
			 */
			if (authentication_required == true)
			{
				stream_url = new URL("http://" + device_ip + ":" + device_port
						+ PublicDefine.GET_AV_STREAM_CMD
						+ PublicDefine.GET_AV_STREAM_PARAM_1 + session_key);
				// Log.d("mbp","AV request:" + stream_url);
			}
			else
			{
				stream_url = new URL("http://" + device_ip + ":" + device_port
						+ PublicDefine.GET_AV_STREAM_CMD);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		HttpURLConnection videoConn = null;
		int responseCode = 0;
		if (_inputStream != null) {
			_inputStream.close();
			_inputStream = null;
		}
		// to change to 15.......
		while (retry++ < 15 && isRunning) {
			try {

				videoConn = (HttpURLConnection) stream_url.openConnection();
				videoConn.addRequestProperty(
						"Authorization",
						"Basic "
								+ Base64.encodeToString(
										usr_pass.getBytes("UTF-8"),
										Base64.NO_WRAP));

				videoConn.setConnectTimeout(5000);
			
				//If it is local camera (authentication_require is false)
				// Enable the timeout 
				if (authentication_required != true)
				{
					videoConn.setReadTimeout(5000);
				}
				else
				/* 20130227: hoang:
				 * add read timeout 20s for upnp to avoid suspending video
				 */
				{
					videoConn.setReadTimeout(20000);
				}
				responseCode = videoConn.getResponseCode();
				_inputStream = new DataInputStream(new BufferedInputStream(
						videoConn.getInputStream()));
				break;
			} catch (IOException ex) {
				videoConn = null;
				if (ex instanceof SocketException) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Log.d("mbp", "tryConnect():connect error : " + ex);
				}
				else {
					Log.d("mbp", "tryConnect():connect error : " + ex);
				}

				if (responseCode == 602) {
					// special case remote server has timeout
					throw new IOException(REMOTE_TIMEOUT);
				}
				else if ( (responseCode == 601) || (responseCode == 401) ) 
				{
					session_key_mismatched_code = responseCode;
					throw new IOException(SESSION_KEY_MISMATCHED);
				}
				
				/* 20130218: hoang: issue 1222
				 * exit if in local
				 */
				if ( (device_port == 80) && (retry == 2) )
				{
					throw new IOException(CAMERA_IS_NOT_AVAILABLE);
				}

			}
		}

		// Double check
		if ((isRunning == false) && _inputStream != null) {
			videoConn = null;
			_inputStream.close();
			_inputStream = null;

		}

		return videoConn;
	}

	protected StreamSplit streamSplitInit(boolean withVideo) throws IOException {

		String ctype = null;
		Hashtable headers = null;
		URLConnection conn = null;
		StreamSplit ssplit = null;

		if (withVideo) {
			conn = tryConnect();
		} else {
			conn = tryConnectToAudio();
		}

		if (conn == null)
			return null;

		try {
			//
			// Read Headers for the main thing...
			// TODO: check again if this line is really needed
			headers = StreamSplit.readHeaders(conn);

			ssplit = new StreamSplit(_inputStream);
		} catch (Exception ex) {
			for (IVideoSink videoSink : _videoSinks) {
				videoSink.onInitError(ex.getMessage());
			}
			return null;
		}

		_collecting = true;
		ctype = (String) headers.get("content-type");

		int bidx = ctype.indexOf("boundary=");
		String boundary = StreamSplit.BOUNDARY_MARKER_PREFIX;
		if (bidx != -1) {
			boundary = ctype.substring(bidx + 9);
			ctype = ctype.substring(0, bidx);
			//
			// Remove quotes around boundary string [if present]
			//
			if (boundary.startsWith("\"") && boundary.endsWith("\"")) {
				boundary = boundary.substring(1, boundary.length() - 1);
			}
			if (!boundary.startsWith(StreamSplit.BOUNDARY_MARKER_PREFIX)) {
				boundary = StreamSplit.BOUNDARY_MARKER_PREFIX + boundary;
			}

			ssplit.setBoundary(boundary);
		}

		if (ctype.startsWith("multipart/x-mixed-replace")) {
			ssplit.skipToBoundary();
		}

		return ssplit;
	}

	


	@Override
	public void run() {
		StreamSplit ssplit = null;
		String ctype = null, boundary = null;
		Hashtable<String, String> headers = null;
		int retries = 1;

		/*
		 * set to true when we can't read from camera i.e. readLine() or read()
		 * return NULL
		 */
		boolean streamHasEnded = false;

//		Log.d("mbp", "Thread:" + Thread.currentThread().getId() + ": start "
//				+ ((this.enableVideo) ? "Video" : "Audio") + " from: "
//				+ device_ip + ":" + device_port);

		viewingSessionHasTimeout = false;
		internalNonRecoverableError = false;
		internalError = -1;

		long now; 
		boolean hasSyncedTime; 
		//**** Sync the time 
		SntpClient client = new SntpClient();
		if ((hasSyncedTime = client.requestTime(device_ip, 5000))) {
			now = client.getNtpTime() + SystemClock.elapsedRealtime() - client.getNtpTimeReference();
			
//			Log.d("mbp", "Sync time with camera: now is: " + now + 
//					" systemNow: " + System.currentTimeMillis()); 
		}
		
		startKeepAliveSock();
		
		long startTime, frameDuration;

		do { // while (retries > 0)

			try {
				// DO this in entry_activity
				ssplit = streamSplitInit(enableVideo);

				if (ssplit == null) {
					isRunning = false;
				} else {
					boundary = ssplit.getBoundary();
				}

				if (isRunning == true) {
					
					// If this is a remove video streamer bm_auth != null
					if (bm_auth != null) {
						/*
						 * pass back the authentication object Check
						 * UDTVideoStreamer.java for detail.
						 */
						mHandler.dispatchMessage(Message.obtain(mHandler,
								MSG_VIDEO_STREAM_HAS_STARTED, bm_auth));
					} else {
						mHandler.dispatchMessage(Message.obtain(mHandler,
								MSG_VIDEO_STREAM_HAS_STARTED));
					}

				}

				
				while (isRunning) {

					startTime = System.currentTimeMillis();
					 kickReadWatchDog();
					
					if (_collecting) {
						if (boundary != null) {
							/* read sub headers */
							headers = ssplit.readHeaders();

							
							if (headers == null ) //Stream error
							{
								internalError = -100; 
								internalNonRecoverableError = true;
								break;
							}
							
							//
							// Are we at the end of the m_stream?
							//
							if (ssplit.isAtStreamEnd()) {
								Log.e("mbp", " Stream end");
								break;
							}
							ctype = (String) headers.get("content-type");
							if (ctype == null) {
								Log.e("mbp", "Throw exception ");
								throw new Exception("No part content type");
							}
						}
						//
						// Mixed Type -> just skip...
						//
						if (ctype.startsWith("image/jpeg")) {

							ctype = (String) headers.get("content-length");
							int data_len = Integer.parseInt(ctype);
							int actual_data_read = 0;

							if (data_len > data.length) {
								/* increase by 20% */
								int newSize = data_len + (data_len * 20) / 100;
								enlarge_data(newSize);
							}
							// old_slow: byte[] data =
							// ssplit.readToBoundary(boundary);
							actual_data_read = ssplit.readDataToBoundary(
									boundary, data, data_len);

							if (actual_data_read == 0) {
								internalError = -101; 
								internalNonRecoverableError = true;
								break;
							}
							if (actual_data_read == -1) {
								internalError = -102; 
								internalNonRecoverableError = true;
								break;
							}
							if (actual_data_read < data_len) {
								// Stream end
								// Come out --force reconnect from outside
								
								Log.e("mbp", " actual_data_read:"
										+ actual_data_read + " data_len:"
										+ data_len);
								internalError = -103; 
								internalNonRecoverableError = true; 
								
								// skip this frame - wait a bit
								break;
							}

							if (_imgidx > IMG_FLUFF_FACTOR && _startTime == 0) {
								_startTime = System.currentTimeMillis();
							}

							
							
							
							
							byte[] img;
							_resetFlag = (data[0] & RESET_FLAG_MASK) ;
							
							int audioType; 
							audioType = (data[0] & AUDIO_TYPE_MASK);
							
							int headerType; 
							headerType = (data[0] & HEADER_EXTENDED_VERSION);
							
							
							

							int iLength = Util.byteArrayToInt_MSB(data, 1);
							_imgCurrentIndex = Util.byteArrayToInt_MSB(data, 5);

							int resolutionJpeg = data[9];

							_resetAudioBufferCount = data[10];

							int temperature = Util.byteArrayToInt_MSB(data, 11);

							updateTemperature(temperature);

							// outPCM[] - array is allocated here - then use
							// until PCMPlayer.writePCM() - then it will be
							// thrown away
							byte[] outPCM = null, filteredPCM = null;
							int outPCM_len = 0;
							// add offset for temperature value
							int audio_start_offset =0;
							
							if (headerType == HEADER_IBALL_VERSION)
							{
								audio_start_offset = 1 + 4 + 4 + 1 + 1 + 4;
							}
							else if (headerType == HEADER_EXTENDED_VERSION)
							{
								/*
								  long long frame_ts; 8 bytes
								  long long pic_ts;   8 b
								  long long audio_ts; 8 b
								  char dummy[16]; 16 bytes
								 */
								audio_start_offset = 1 + 4 + 4 + 1 + 1 + 4 + 
										1 + //try --
										8 +8 +8 + 16 ;
								
								long frame_ts,pic_ts,  audio_ts;
								frame_ts = Util.byteArrayToLong_LSB(data,16);
								pic_ts = Util.byteArrayToLong_LSB(data,24);
								audio_ts = Util.byteArrayToLong_LSB(data,32);
								
								
								if (hasSyncedTime) {
									now = client.getNtpTime() + SystemClock.elapsedRealtime() - client.getNtpTimeReference();
									
								}
							}
							else
							{
								Log.e("mbp", " invalid header type: " + headerType);
								continue; 
							}
							
							

							

							
							if (iLength > 0) {
								byte[] adpcm = new byte[iLength];

								System.arraycopy(data, audio_start_offset,
										adpcm, 0, iLength);
								if (_enableAudio)
								{

									if (audioType == AUDIO_TYPE_PCM) // audio - pcm
									{
										outPCM = adpcm;
										outPCM_len = iLength;
									}
									else if (audioType == AUDIO_TYPE_ADPCM) // Audio - adpcm
									{
										outPCM_len = ADPCMDecoder.calResultDataSize(adpcm);
										outPCM = new byte[outPCM_len];
										ADPCMDecoder.decode(adpcm, outPCM);

										// use static buffer --
										/*
										 * dataPcm_len =
										 * ADPCMDecoder.calResultDataSize
										 * (adpcm); if (dataPcm.length <
										 * dataPcm_len) {
										 * Log.e("mbp","expanding m_audio.. to "
										 * + dataPcm_len ); dataPcm = new
										 * byte[dataPcm_len]; }
										 * ADPCMDecoder.decode(adpcm,dataPcm);
										 */
									} 
									else
									{
										Log.e("mbp", "Invalid audio type in header: " +audioType );
										outPCM = null;
										outPCM_len = 0; 
									}

								}

								// add offset for temperature value
								int imageDataLen = actual_data_read
										- (audio_start_offset + iLength);

								// int imageDataLen =actual_data_read - (1 + 4 +
								// 4 + 1 + 1 +iLength);
								img = new byte[imageDataLen];

								// add offset for temperature value
								System.arraycopy(data, audio_start_offset
										+ iLength, img, 0, img.length);

							} else {

								/*
								 * Log.d("mbp", "adPCM nonaudio interval(ms): "
								 * + (System.currentTimeMillis() - start_time)
								 * );
								 * 
								 * noaudio_pkt_count ++; start_time =
								 * System.currentTimeMillis();
								 */

								/* with audio */
								int imageDataLen = actual_data_read - audio_start_offset;
								img = new byte[imageDataLen];
								System.arraycopy(data, audio_start_offset, img,
										0, img.length);

								/*
								 * FOR TEST : without audio int imageDataLen =
								 * actual_data_read; img = new
								 * byte[imageDataLen];
								 * System.arraycopy(data,0,img,0,img.length);
								 */
							}

							// static buffer
							// updateImage(img, dataPcm, dataPcm_len );

							// original
							updateImage(img, outPCM, outPCM_len);

						} /*
						 * split audio/video - if
						 * (ctype.startsWith("multipart/x-mixed-replace"))
						 */

					}

				} // while (isRunning)

			} 
			catch (Exception e) 
			{
				String err_str = e.getMessage();
				if (err_str != null)
				{
					if (err_str.equalsIgnoreCase(SESSION_KEY_MISMATCHED))
					{
						e.printStackTrace();
						session_key_mismatched = true;
						break;
					}
					else if (err_str.equalsIgnoreCase(CAMERA_IS_NOT_AVAILABLE))
					{
						e.printStackTrace();
						camera_is_not_available = true;
						break;
					}
					else
					{
						Log.w("mbp", "Thread:" + Thread.currentThread().getId()
							+ ":vstreamer-exception:" + e.toString());
						e.printStackTrace();
					}
				}
				else
				{
					Log.w("mbp", "Thread:" + Thread.currentThread().getId()
						+ ":vstreamer-exception:" + e.toString());
					e.printStackTrace();
				}
				closeCurrentSession();
				
			}

			/* force stop */
			if ((stopped_by_caller == true)
					|| (viewingSessionHasTimeout == true)
					|| (internalNonRecoverableError == true)) {
				retries = 0;
			}

			retries--;

			if (retries >= 0) {
				// Reset it here since we are retrying .
				isRunning = true;
			}

			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
					+ ":retry ? " + (retries >= 0));
			Log.d("mbp", "retries = "+retries);

		} while (retries >= 0);

		isRunning = false;
		
		stopKeepAliveSock();
		
		Log.d("mbp", "Thread:" + Thread.currentThread().getId()
				+ " : Stopping video from: " + device_ip + ":" + device_port +
				" isRunning: " + isRunning);

		checkDisconnectReason();
	}

	protected void checkDisconnectReason() {
		/*
		 * if we are here because of some exception try to reconnect, if we
		 * failed, the reconnect code will handle the disconnection
		 */
		if (session_key_mismatched == true)
		{
			Log.e("mbp", "Session key mismatched...notify to user now.");
			mHandler.dispatchMessage(Message.obtain(mHandler, 
					MSG_SESSION_KEY_MISMATCHED, session_key_mismatched_code, session_key_mismatched_code));
		}
		else if (camera_is_not_available == true)
		{
			Log.e("mbp", "Camera is not available...stop retrying.");
			mHandler.dispatchMessage(Message.obtain(mHandler,
					MSG_CAMERA_IS_NOT_AVAILABLE));
		}
		else if (viewingSessionHasTimeout == true) 
		{
			Log.e("mbp", "Thread:" + Thread.currentThread().getId()
					+ "Stream stop due to timeout  ");
			mHandler.dispatchMessage(Message.obtain(mHandler,
					MSG_VIDEO_STREAM_HAS_STOPPED_TIMEOUT));
		} 
		else if ((stopped_by_caller != true) && (internalNonRecoverableError == true)) 
		{
			Log.e("mbp", "Thread:" + Thread.currentThread().getId()
					+ "Stream stop due to internalERROR  ");
			mHandler.dispatchMessage(Message.obtain(mHandler,
					MSG_VIDEO_STREAM_INTERNAL_ERROR, 
					internalError,
					internalError));
		}
		else if (stopped_by_caller != true)
		{
			Log.e("mbp", "Thread:" + Thread.currentThread().getId()
					+ " : streaming frm: " + device_ip + ":" + device_port
					+ " stop unexpectedly... Send message now ");
			mHandler.dispatchMessage(Message.obtain(mHandler,
					MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY));
		}
		else
		{
			Log.e("mbp", "STOPPED by caller >>>>>>> NO REASON");
			/* 20130201: hoang: issue 1260
			 * turn off screen when finish view request by
			 * sending message to EntryActivity
			 */
			mHandler.dispatchMessage(Message.obtain(mHandler,
					MSG_VIDEO_STREAM_HAS_STOPPED));
		}

	}

	public void restart() {
		isRunning = true;
	}

	public void stop(int reason) {
		
		switch (reason) {
		case Streamer.STOP_REASON_USER:
			stopped_by_caller = true;
			break;
		case Streamer.STOP_REASON_TIMEOUT:
			viewingSessionHasTimeout = true;
			break;
		default:
			break;
		}

		_collecting = false;
		isRunning = false;
		try {
			if (_inputStream != null) {
				_inputStream.close();
			}
			_inputStream = null;
		} catch (Exception e) {
		}

		/* remove all interface */
		_tempUpdater = null;
		_videoSinks.clear();
	}

	private void updateTemperature(int temp)
	{
		if (_tempUpdater != null) 
		{
			//Log.d("mbp", "updateTemperature: _tempUpdater!= null"); 
			if (version_03_031_temperature_incr_by_one) {
				temp += 1; /* degree celsius */
			}
			_tempUpdater.updateTemperature(temp);
		}
		else
		{
		}
	}

	protected void updateMelodyStatus(String response) {

		if ((response != null)
				&& response.startsWith(PublicDefine.GET_MELODY_VALUE)) {
			String str_value = response.substring(PublicDefine.GET_MELODY_VALUE
					.length() + 2); /* value_melody: */
			int icon_index = Integer.parseInt(str_value);
			if (_melodyUpdater != null) {
				_melodyUpdater.updateMelodyIcon(icon_index);
			}

		}

	}

	private void updateImage(byte[] img, byte[] pcm, int pcm_len) {

		try {
			_imgidx++;
			for (IVideoSink videoSink : _videoSinks) {
				videoSink.onFrame(img, pcm, pcm_len);

			}
		} catch (Exception ex) {
		}
	
	}
	
	
	
	protected boolean isStreaming()
	{
		
		return isRunning ; 
	}
	protected void closeCurrentSession()
	{
		//Do nothing
	}

	protected  void kickReadWatchDog()
	{
		//Do nothing
	}
	
	public int getResetFlag() {
		return 0;// /_resetFlag;
	}

	public int getResetAudioBufferCount() {
		return _resetAudioBufferCount;
	}

	public int getImgCurrentIndex() {
		return _imgCurrentIndex;
	}

	/* (non-Javadoc)
	 * @see com.msc3.Streamer#setAccessToken(java.lang.String)
	 */
	@Override
	void setAccessToken(String usrToken) {
		this.usrToken = usrToken;
	}

}
