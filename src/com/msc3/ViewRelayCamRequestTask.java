package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.registration.LoginOrRegistrationActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;

/**
 * @author NxComm
 *
 */
public class ViewRelayCamRequestTask extends ViewRemoteCamRequestTask {
	
	private static final String str_sessionKey = "SessionKey=";
	private static final String str_channelId = "channelId=";
	private static final String str_BRTag = "<br>";
	private static final String str_Equal = "=";
	
	private String userName;
	private String userPass;
	private String mac;
	
	private int server_err_code;

	private Tracker tracker;
	private EasyTracker easyTracker;

	public ViewRelayCamRequestTask(Handler h, Context mContext) {
		super(h, mContext);
		server_err_code = 404;
		
		easyTracker = EasyTracker.getInstance();
		easyTracker.setContext(mContext);
		tracker = easyTracker.getTracker();
	}

	@Override
	protected BabyMonitorAuthentication doInBackground(String... params) {
		
		userName = params[0];
		userPass = params[1];
		mac = params[2];
		mac = PublicDefine.strip_colon_from_mac(mac).toUpperCase();
		
		String channelId = null;
		String sskey = null;
		BabyMonitorAuthentication auth = null;
		
		Log.d("mbp", "View cam relay request");
		
		//Step 1 : check if camera is ready
		int retry = 12; 
		do 
		{
			server_err_code = isCamReady();
			if ( server_err_code == HttpURLConnection.HTTP_OK)
			{
				break; 
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (isCancelled())
			{
				Log.d("mbp", " ViewRemoteCamRelayRequestTask is cancelled " );
				break;
			}
		}
		while (retry -->0);
		
		
		if (server_err_code == HttpURLConnection.HTTP_OK)
		{
			String usr_pass =  String.format("%s:%s", userName, userPass);

			String http_cmd = PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART + 
					PublicDefine.VIEW_CAM_RELAY_CMD +
					PublicDefine.VIEW_CAM_RELAY_PARAM_1 + mac +
					PublicDefine.VIEW_CAM_RELAY_PARAM_2 + userName;

			URL url = null;
			HttpsURLConnection conn = null;
			DataInputStream inputStream = null;
			int resCode = -1;

			try {

				url = new URL(http_cmd);
				conn = (HttpsURLConnection) url.openConnection();
				if (ssl_context != null)
				{
					conn.setSSLSocketFactory(ssl_context.getSocketFactory());
				}
				conn.addRequestProperty("Authorization", "Basic " +
						Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
				conn.setConnectTimeout(15000);
				conn.setReadTimeout(40000);

				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream(), 4*1024));
				resCode = conn.getResponseCode();
				Log.d("mbp", "View cam relay responseCode: " + resCode);
				if (resCode == HttpURLConnection.HTTP_OK)
				{
					String response_str = inputStream.readLine();
					String[] response_tokens = response_str.split(str_BRTag);
					String sskey_arr = response_tokens[1];
					String chann_arr = response_tokens[2];

					if (sskey_arr != null)
					{
						String[] sskey_tokens = sskey_arr.split(str_Equal);
						if (sskey_tokens != null)
						{
							sskey = sskey_tokens[1];
						}
					}

					if (chann_arr != null)
					{
						String[] chann_tokens = chann_arr.split(str_Equal);
						if (chann_tokens != null)
						{
							channelId = chann_tokens[1];
						}
					}

					auth = new BabyMonitorRelayAuthentication(
							"127.0.0.1", "80",
							channelId, mac, sskey, null, 80, userName, userPass);

				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else
		{
			auth = null;
		}
		
		return auth;
	}
	
	@Override
	protected void onPostExecute(BabyMonitorAuthentication result) {
		Message m;
		
		if (result != null)
		{
			m = Message.obtain(mHandler, ViewRemoteCamRequestTask.MSG_VIEW_CAM_SUCCESS, result);
		}
		else
		{
			tracker.sendEvent(ViewCameraActivity.GA_VIEW_CAMERA_CATEGORY, "View Remote Cam Request Failed",
					"RELAY Cam Info Access Failed", null);
			m = Message.obtain(mHandler, ViewRemoteCamRequestTask.MSG_VIEW_CAM_FALIED, server_err_code, server_err_code);
		}
		
		mHandler.dispatchMessage(m);
	}
	
	private int  isCamReady()
	{
		
		String http_cmd = PublicDefine.BM_SERVER+ PublicDefine.BM_HTTP_CMD_PART+ 
				PublicDefine.IS_CAM_AVAILABLE_ONLOAD2_CMD + 
				PublicDefine.IS_CAM_AVAILABLE_ONLOAD2_PARAM_1 +  mac;

		//Log.d("mbp","REMOVE B4 RELEASE: getsec http: " + http_cmd);

		
		
		URL url = null;
		HttpsURLConnection conn = null;
		DataInputStream inputStream = null;
		String response = null;
		int respondeCode = -1;

		String usr_pass =  String.format("%s:%s", userName, userPass);

		try {
			url = new URL(http_cmd );
			conn = (HttpsURLConnection)url.openConnection();

			if (ssl_context!=null)
			{
				conn.setSSLSocketFactory(ssl_context.getSocketFactory());
			}

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));
			conn.setConnectTimeout(15000);
			conn.setReadTimeout(20000);
			respondeCode = conn.getResponseCode();
			Log.d("mbp","iSCamReady respondeCode: " + respondeCode);
			

		} catch (MalformedURLException e) {
			e.printStackTrace();
			respondeCode = -1; 
		} catch (SocketTimeoutException se)
		{
			//Connection Timeout - Server unreachable ??? 
			se.printStackTrace();
			respondeCode = -1; 
		}
		catch (IOException e) {
			e.printStackTrace();
			
			respondeCode = -1; 
		}
		return respondeCode;
	}

}
