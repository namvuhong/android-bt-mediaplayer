package com.msc3;

public class UndefinedPortException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UndefinedPortException(String message)
	{
		super(message);
	}
}
