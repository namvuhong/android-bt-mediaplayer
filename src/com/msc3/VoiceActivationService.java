package com.msc3;


import com.blinkhd.R;
import com.blinkhd.gcm.AlertData;
import com.blinkhd.gcm.GcmIntentService;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.msc3.registration.FirstTimeActivity;
import com.msc3.registration.SingleCamConfigureActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.PowerManager.WakeLock;
import android.telephony.TelephonyManager;
import android.util.Log;

public class VoiceActivationService extends Service{

	public static final int MSG_REGISTER_CLIENT = 0x101;
	public static final int MSG_UNREGISTER_CLIENT = 0x102;
	public static final int MSG_VOX_TRIGGER_MAC = 0x103;
	public static final int MSG_ACTIVITY_NOT_READY = 0x104;

	public static final String GA_NOTIFICATION_CATEGORY = "Notifications";
	
	private UDPSocketRunnable mUdpListener;
	private Thread mServerThread;
	private CamProfile [] restored_profiles;

	private static final int VOX_ID = 1;
	private static final int VOX_STORAGE_UNAVAILABLE_ID = 2;

	public static final int CLIENT_NOT_CREATED = 0x1;
	private static final int CLIENT_NOTIFYING = 0x2;
	public  static final int CLIENT_CREATED = 0x3;
	private int client_status = CLIENT_NOT_CREATED; 
	Messenger mClients ;
	private long lastTimeRead = 0; 
	
	private WakeLock keep_cpu_running; 
	private WifiBroadcastReceiverVoiceActivationService wr; 
	
//	private Tracker tracker;
//	private EasyTracker easyTracker;
	
	public VoiceActivationService() {
		super();

	}
	public VoiceActivationService(String name) {
		super();
	}


	@Override
	public void onCreate() {
		super.onCreate();
		
//		easyTracker = EasyTracker.getInstance();
//		easyTracker.setContext(this);
//		tracker = easyTracker.getTracker();
		
		String phonemodel = android.os.Build.MODEL;
		if(!phonemodel.equals(PublicDefine.PHONE_MBP2k) && !phonemodel.equals(PublicDefine.PHONE_MBP1k)) // if it's not iHome Phone
		{	
			//IGNORE local vox 
			Log.d("mbp","NOT iHome - Dont start this service");
			return;
		}
		
		
		
		SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		boolean  shouldGetWl = settings.getBoolean(PublicDefine.PREFS_VOX_SHOULD_TAKE_WAKELOCK, false);
		
		if (shouldGetWl)
		{
			if (keep_cpu_running != null && keep_cpu_running.isHeld())
			{
				keep_cpu_running.release();
			}
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			keep_cpu_running = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					"KEEP CPU RUNNING");
			keep_cpu_running.acquire();
			
			
			
			
			
			//20121119: phung: dont show SOL as it would confused user
			//showSOL();
		}
		else
		{
			//hideSOL();
		}
		mUdpListener = new UDPSocketRunnable();
		mServerThread = new Thread(mUdpListener,"VoxServerThread");
		mServerThread.start();
		
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(str_client_status, CLIENT_NOT_CREATED);
		editor.commit();
		
		
		startWifiBroadcastReceiver(); 
		
	}



	@Override
	public void onDestroy() {
		super.onDestroy();

		if (keep_cpu_running != null && keep_cpu_running.isHeld())
		{
			Log.d("mbp","VOX releasing wakelock");
			keep_cpu_running.release();
		}

		
		stopWifiBroadcastReceiver();

		hideSOL();
		
		boolean retry = true;
		if (mServerThread != null)
		{
			mUdpListener.stop();
			while (retry)
			{
				try {
					mServerThread.join(5000);
					retry = false;
				} catch (InterruptedException e) {
				}
			}
			mServerThread = null;
		}
		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		if (mServerThread!= null && !mServerThread.isAlive())
		{
			mServerThread.start();
		}
		else
		{
		}

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}
	
	

	
	private void showSOL()
	{
//		String ns = Context.NOTIFICATION_SERVICE;
//		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
//		
//		int icon = R.drawable.cam_icon;
//		CharSequence tickerText = getResources().getString(R.string.vox_service_enabled);
//		long when = System.currentTimeMillis();
//
//		Notification notification = new Notification(icon, tickerText, when);
//		notification.flags |= Notification.FLAG_NO_CLEAR ;
//		notification.flags |= Notification.FLAG_ONGOING_EVENT ;
//		
//		Context context = getApplicationContext();      // application Context
//		CharSequence contentTitle = getResources().getString(R.string.app_name);
//		CharSequence contentText = getResources().getString(R.string.vox_service_enabled);
//		Intent intent = new Intent(this, xxxx);
//		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
//
//		
//		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
//		
//		mNotificationManager.notify(VOX_ID, notification);
		
	}
	
	private void hideSOL()
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		
		mNotificationManager.cancel(VOX_ID);
	}
	
	
	private boolean isOnCall()
	{
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if  (tm.getCallState() == TelephonyManager.CALL_STATE_OFFHOOK)
		{
			//Show dialog -- Start VoxActivity.java
			return true; 
		}
		return false; 
	}
	
	
	private void startWifiBroadcastReceiver()
	{
		
		Log.d("VoiceActivationService","WifiBroadcastReceiver");
		wr  = new WifiBroadcastReceiverVoiceActivationService(this);
    	IntentFilter iFilter= new IntentFilter();
        iFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        iFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        iFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        iFilter.addAction(SingleCamConfigureActivity.ACTION_TURN_WIFI_NOTIF_ON_OFF);
        
        registerReceiver(wr, iFilter);
	}
	
	
	private void stopWifiBroadcastReceiver()
	{
		if (wr != null)
		{
			unregisterReceiver(wr);
			wr = null; 
		}
	}
	
	/**
	 * Used by wifi out of range broadcast receiver
	 * @param message
	 */
	public void noficationMessage(String message)
	{

		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

		int icon = R.drawable.cam_icon;
		//CharSequence tickerText = getResources().getString(R.string.vox_service_enabled);
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, message, when);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_ALL;


		Context context = getApplicationContext();      // application Context
		CharSequence contentTitle = getResources().getString(R.string.app_name);
		//CharSequence contentText = getResources().getString(R.string.vox_service_enabled);
		String contentText = message; //"Low Battery";
		Intent intent = new Intent();


		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

		mNotificationManager.notify(11111, notification);


	}
	
	private void sendNotificationWithSound(String message, int notifId)
	{
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

		int icon = R.drawable.cam_icon;
		CharSequence tickerText = message;
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.defaults = Notification.DEFAULT_ALL;
		

		Context context = getApplicationContext();      // application Context
		CharSequence contentTitle = getResources().getString(R.string.app_name);
		CharSequence contentText = message;
		Intent intent = new Intent(VoiceActivationService.this, FirstTimeActivity.class);
		intent.putExtra(FirstTimeActivity.bool_VoxInfraMode, true);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		PendingIntent contentIntent = PendingIntent.getActivity(VoiceActivationService.this, 0, intent, 0);

		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

		mNotificationManager.notify(notifId, notification);
	}
	
	/* 20121120: may not be needed */ 
	public static final String str_client_status = "Client_Status"; 
	
	/**
	 * Show a notification while this service is running.
	 */
	
	private  void showNotification(VoxMessage newVox) {

		String macAddr = newVox.getTrigger_mac();
		if  (macAddr == null)
		{
			Log.d("mbp", "Mac in vox data is null ");
			return; 
		}
		

		//check offline record to decide whether we should popup  
		SetupData savedData = new SetupData();
		boolean found = false;
		SharedPreferences setting = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		Boolean readFlag = setting.getBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
		if (!readFlag)
		{
			if (savedData.hasUpdate(getExternalFilesDir(null), lastTimeRead))
			{
				try {
					if (savedData.restore_session_data(getExternalFilesDir(null)))
					{
						restored_profiles = savedData.get_CamProfiles();
						lastTimeRead = System.currentTimeMillis();
					}
				} catch (StorageException e) {
					Log.d("mbp", e.getLocalizedMessage());
					String msg = getResources()
							.getString(
									R.string.service_has_stopped_due_to_usb_storage_turned_on);
					sendNotificationWithSound(msg, VOX_STORAGE_UNAVAILABLE_ID);
					VoiceActivationService.this.stopSelf();
				}
			}
		}
		
		if (restored_profiles != null)
		{
			for (int i =0; i<restored_profiles.length;i++)
			{
				if ( (restored_profiles[i] != null) &&
						(restored_profiles[i].get_MAC() != null) &&
						(restored_profiles[i].get_MAC().equalsIgnoreCase(macAddr)) 
						)
				{
					found = true;
					break;
				}
			}
		}
		
		if (found == false)
		{
//			Log.d("mbp","Not my device -- ignore it");
		}
		else
		{
			
			SharedPreferences settings = getSharedPreferences(
					PublicDefine.PREFS_NAME, 0);
			String cameraViewedNow = settings.getString(PublicDefine.PREFS_CAM_BEING_VIEWED, null);
			if (cameraViewedNow!= null && cameraViewedNow.equalsIgnoreCase(macAddr))
			{
				Log.d("mbp","Camera is in view -- ignore it");
			}
			else
			{
				newVox.setTrigger_time(System.currentTimeMillis());
				//Store this mesg 
				boolean status = AlertData.addNewAlert(newVox, getExternalFilesDir(null));
				ArrayList<VoxMessage> messages = AlertData.getAllAlert(getExternalFilesDir(null)); 

				//Show status notification
				generateNotifications(this, messages);
			
				
				//In case we are on call -- show a dialog - 
				if (isOnCall())
				{
					boolean isEnabledWhileOnCall = settings.getBoolean(PublicDefine.PREFS_MBP_VOX_ALERT_ENABLED_IN_CALL, false);
					if (isEnabledWhileOnCall == true)
					{
					
						//Show dialog. - Recurring sound or not is checked And turned on later by VoxActivity
						/* notification to create a client */
						if (keep_cpu_running != null && keep_cpu_running.isHeld())
						{
							keep_cpu_running.release();
						}


						/* turn on the screen */
						PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
						if (!pm.isScreenOn()) {
							WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP, "My Tag");
							wl.setReferenceCounted(false);
							wl.acquire(10000);
						}


						Intent entry_intent = new Intent(this, VoxActivity.class);
						/* 20120104: make this launching CLEAR TOP to avoid multiple instances of 
						 * Vox Activity ( i.e. multiple alarm being played) if the device keep sending VOX 
						 * and app does not care.
						 */

						entry_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|
								Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
						/* 20120316: only need the mac address here because eventually, we will scan the network*/
						entry_intent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,newVox.getTrigger_mac());
						startActivity(entry_intent);
					}
					else
					{
						//Ignore because disabled .. 
						Log.d("mbp","Ignore alert because Settings is not enabled ");
					}
						
				}
				
				
				
				
			} /*  (cameraViewedNow == null ||  !cameraViewedNow.equalsIgnoreCase(macAddr))*/
			
		
		}/* (found == true) */
		
		
	}
	
	/**
	 * FROM GcmIntentService.java
	 */
	private Notification build_notification_with_sound(Context context, VoxMessage newVox, Notification old_notif)
	{
		
		int icon = R.drawable.cam_icon;
		long when =newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name(); 
		Notification notification = null;
		
		//setup pending intent when being clicked
		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,newVox.getTrigger_mac());
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
							Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent =
				PendingIntent.getActivity(context, 0, notificationIntent, 0); 
		
		
		//setup pending intent when being CLEARED
		Intent deleteIntent = new Intent(context, ClearNotificationActivity.class);
		// set intent so it does not start a new activity
		deleteIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|
							Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent deletePIntent =
				PendingIntent.getActivity(context, 0, deleteIntent, 0); 
		
		if (old_notif == null)
		{
			//This is the first message
			//Log.d("mbp", "This is the Notification message");
			
			notification = new Notification(icon, title, when);

			
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);
			
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;
			
			
		}
		else //just update the content of the notification 
		{
			
			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);
			
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_ALL;
			
		}
		
		notification.deleteIntent = deletePIntent;
		
		return notification; 
	}
	
	/**
	 * 
	 * Build notification WITHOUT sound, to avoid too many clustered sound alarm. 
	 * @param context
	 * @param newVox
	 * @param old_notif
	 * @return
	 */
	private Notification build_notification(Context context, VoxMessage newVox, Notification old_notif)
	{
		
		int icon = R.drawable.cam_icon;
		long when =newVox.getTrigger_time();// System.currentTimeMillis();
		String title = newVox.getTrigger_name(); 
		Notification notification = null;
		
		Intent notificationIntent = new Intent(context, VoxActivity.class);
		notificationIntent.putExtra(VoxActivity.TRIGGER_MAC_EXTRA,newVox.getTrigger_mac());
		// set intent so it does not start a new activity
					notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
							Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		PendingIntent intent =
				PendingIntent.getActivity(context, 0, notificationIntent, 0); 
		
		
		if (old_notif == null)
		{
			//This is the first message
			//Log.d("mbp", "This is the Notification message");
			
			notification = new Notification(icon, title, when);

			
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);
			
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE ;
			
		}
		else //just update the content of the notification 
		{
			
			notification = new Notification(icon, title, when);
			notification.setLatestEventInfo(context, title, newVox.toString(), intent);
			
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE ;
			
		}
		return notification; 
	}
	
	
	/**
	 * FROM GcmIntentService.java
	 * 
	 * Show a group of notification 
	 *   For different Macs there is one icons
	 * 
	 * @param context
	 * @param messages
	 */
	private  void generateNotifications(Context context, ArrayList<VoxMessage> messages) 
	{
		NotificationManager notificationManager = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);

		
		HashMap<String, Notification> notification_map = new HashMap<String, Notification>();  
		
		Iterator<VoxMessage> iter = messages.iterator(); 
		VoxMessage mesg; 
		Notification notif; 
		
		//Log.d("mbp", "Number of messages: "+ messages.size());
		iter = messages.iterator(); 
		while (iter.hasNext())
		{
			mesg = iter.next(); 
			notif = notification_map.get(mesg.getTrigger_mac());
			if (iter.hasNext() == true) //one more to come
			{
				notif = build_notification(context, mesg, notif);
			}
			else //no  more..i.e.  this is the last one
			{
				notif = build_notification_with_sound(context, mesg, notif);
			}
			notification_map.put(mesg.getTrigger_mac(), notif);
			
		}
		
		//NOTIFY all .. 
		Set<String> devices = notification_map.keySet();
		Iterator<String> iter2 = devices.iterator();
		int i =0; 
		int current_id ; 
		while (iter2.hasNext() && i < GcmIntentService.PUSH_IDs.length)
		{
			current_id = GcmIntentService.PUSH_IDs[i]; 
			notif = notification_map.get(iter2.next());
			notificationManager.notify(current_id, notif);
			i ++; 
		}
	}
	

	/***************************************
	 *  Activity Communication starts here
	 * @author phung
	 *
	 */


	final Messenger mMessenger = new Messenger(new IncomingHandler()); 


	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}


	class IncomingHandler extends Handler { // Handler of incoming messages from clients.
		@Override
		public void handleMessage(Message msg) {

			/* Handle message FROM UI activity*/
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:

				synchronized (VoiceActivationService.this) {
					mClients = msg.replyTo;
					SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt(str_client_status, CLIENT_CREATED);
					editor.commit();
				}	

				break;
			case MSG_UNREGISTER_CLIENT:

				synchronized (VoiceActivationService.this) {
					mClients= null;
					
					SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt(str_client_status, CLIENT_NOT_CREATED);
					editor.commit();
					
				}	

				break;
			case MSG_ACTIVITY_NOT_READY:

				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/**
	 * 
	 *  20120316: 
	 * Only Mac address here is not enough because, this camera may have just been rebooted, 
	 * and the application running does not have IP of this camera
	 * 
	 * Have to send both IP and Mac address -
	 * 
	 * @param newVox - VoxMessage to be send to UI
	 */
	private void sendMessageToUI(VoxMessage newVox ) {
		if (mClients!= null)
		{
			try{
				//Send data as a String
				Bundle b = new Bundle();
				b.putString(VoxActivity.TRIGGER_MAC_EXTRA, newVox.getTrigger_mac());
				b.putString(VoxActivity.TRIGGER_IP_EXTRA, newVox.getTrigger_ip());
				b.putInt(VoxActivity.TRIGGER_TYPE, newVox.getVoxType());
				Message msg = Message.obtain(null, MSG_VOX_TRIGGER_MAC);
				msg.setData(b);
				mClients.send(msg);

			} catch (RemoteException e) {
			}
		}
	}


	/* 20120118: copied from ScanForCamera.java */
	private InetAddress broadcast, gateWay;
	private void  getAddresses() throws IOException {
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow

		
		int broadcast_addr = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;


		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast_addr >> k * 8) & 0xFF);

		broadcast =  InetAddress.getByAddress(quads);


		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((dhcp.gateway >> k * 8) & 0xFF);

		gateWay = InetAddress.getByAddress(quads);


		return;
	}

	

	/*********************
	 * 20111222: New Implementation Using UDP 
	 * 
	 * @author phung
	 *
	 */

	private class UDPSocketRunnable implements Runnable 
	{
		private boolean running; 
		private DatagramSocket mSSocket;
		
		public UDPSocketRunnable() {

			running = true;
			try {
				mSSocket = new DatagramSocket(PublicDefine.DEFAULT_ALARM_LISTEN_PORT);
				mSSocket.setBroadcast(true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}

		public void stop()
		{
			running = false;
		}

		public void run()
		{
			
			DatagramPacket pkt;

			if (mSSocket == null)
			{
				Log.d("mbp", "mSSocket = null -- recreate");
				try {
					mSSocket = new DatagramSocket(PublicDefine.DEFAULT_ALARM_LISTEN_PORT);
					mSSocket.setBroadcast(true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				mSSocket.setSoTimeout(2000);
			} catch (SocketException e1) {
			}

			while (running)
			{
				
				byte[] buf = new byte[30];
				pkt = new DatagramPacket(buf, buf.length);


				/*
				 *  Receive with Short timeout 
				 *  to check if this thread is to be stopped now 
				 *  
				 */
				boolean retry ;
				do
				{
					retry = false;
					
					try {
						mSSocket.receive(pkt);
					}
					catch(SocketTimeoutException se)
					{
						
						try {
							getAddresses();
						} catch (IOException e) {
							Log.d("mbp", "send a broadcast exception - ignore:" +e.getLocalizedMessage());
						}
						/* 20120508: dont send. */
						//send_a_bc_UDP_packet();
						
						retry = true;
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}

				} while ((running) && (retry == true));

				if (!running)
				{
					break;
				}



				String trigger_device_mac = null;
				String signature = null;
				String deactivate_msg = null;
				try {
					signature = new String (pkt.getData(),0,27, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}


				if ( signature.startsWith(PublicDefine.VOX_ALARM))
				{

					trigger_device_mac = signature.substring(PublicDefine.VOX_ALARM.length(), 
							PublicDefine.VOX_ALARM.length() + 17);
					deactivate_msg = PublicDefine.VOX_ALARM+"deactivate";
				}
				else if ( signature.startsWith(PublicDefine.HOT_ALARM))
				{


					trigger_device_mac = signature.substring(PublicDefine.HOT_ALARM.length(), 
							PublicDefine.HOT_ALARM.length() + 17);
					deactivate_msg = PublicDefine.HOT_ALARM+"deactivate";

				}
				else if ( signature.startsWith(PublicDefine.COLD_ALARM))
				{	
					trigger_device_mac = signature.substring(PublicDefine.COLD_ALARM.length(), 
							PublicDefine.COLD_ALARM.length() + 17);
					deactivate_msg = PublicDefine.COLD_ALARM+"deactivate";
				}


				//check offline record to decide whether we should popup  
				SetupData savedData = new SetupData();
				boolean found = false;
				String cameraName = "camera"; 

				//20121210:phung: avoid reading too many times 
				SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
				Boolean readFlag = settings.getBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
				if (!readFlag)
				{
					if (savedData.hasUpdate(getExternalFilesDir(null), lastTimeRead))
					{
						try {
							if (savedData.restore_session_data(getExternalFilesDir(null)))
							{
								restored_profiles = savedData.get_CamProfiles();
								lastTimeRead = System.currentTimeMillis(); 
							}
						} catch (StorageException e) {
							Log.d("mbp", e.getLocalizedMessage());
							String msg = getResources()
									.getString(
											R.string.service_has_stopped_due_to_usb_storage_turned_on);
							sendNotificationWithSound(msg, VOX_STORAGE_UNAVAILABLE_ID);
							VoiceActivationService.this.stopSelf();
						}
					}
				}
				
				
				if  (restored_profiles != null)
				{
					for (int i =0; i<restored_profiles.length;i++)
					{
						if ( (restored_profiles[i] != null) &&
								(restored_profiles[i].get_MAC() != null) &&
								(restored_profiles[i].get_MAC().equalsIgnoreCase(trigger_device_mac)) 
								)
						{
							
							
							//20121105: phung: check if alert is enabled for this phone
							if ( signature.startsWith(PublicDefine.VOX_ALARM) && restored_profiles[i].isSoundAlertEnabled())
							{
								found = true;
								cameraName = restored_profiles[i].getName();
							}
							else if ( signature.startsWith(PublicDefine.HOT_ALARM)  && restored_profiles[i].isTempHiAlertEnabled() )
							{
								found = true;
								cameraName = restored_profiles[i].getName();
							}
							else if ( signature.startsWith(PublicDefine.COLD_ALARM)  && restored_profiles[i].isTempLoAlertEnabled())
							{	
								found = true;
								cameraName = restored_profiles[i].getName();
							}
							
							
							break;
						}
					}

				}

				if (found == false)
				{
//					Log.d("mbp","Not my device OR alarm is disabled -- ignore it");
					continue;
				}
				
					
				// send deactivate message via TCP 
				try {
					Socket bm_sock =new Socket(pkt.getAddress(), PublicDefine.DEFAULT_ALARM_DEACTIVATE_PORT);
					OutputStream os = bm_sock.getOutputStream();
					os.write(deactivate_msg.getBytes("UTF-8"));
					os.flush();
					os.close();
					bm_sock.close();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				VoxMessage incoming = null;
				if ( signature.startsWith(PublicDefine.VOX_ALARM))
				{
//					tracker.sendEvent(VoiceActivationService.GA_NOTIFICATION_CATEGORY,
//							"Sound notification",
//							"Sound notification", null);
					/*20120307: add a vox message filter here */
					incoming =  new VoxMessage(VoxMessage.VOX_TYPE_VOICE,
							pkt.getAddress(), trigger_device_mac, System.currentTimeMillis(),
							VoxMessage.VOX_VAL_INVALID, cameraName);
				}
				else if ( signature.startsWith(PublicDefine.HOT_ALARM))
				{
//					tracker.sendEvent(VoiceActivationService.GA_NOTIFICATION_CATEGORY,
//							"High temperature notification",
//							"High temperature notification", null);
					incoming =  new VoxMessage(VoxMessage.VOX_TYPE_TEMP_HI,
							pkt.getAddress(), trigger_device_mac, System.currentTimeMillis(),
							VoxMessage.VOX_VAL_INVALID, cameraName);
				}
				else if ( signature.startsWith(PublicDefine.COLD_ALARM))
				{
//					tracker.sendEvent(VoiceActivationService.GA_NOTIFICATION_CATEGORY,
//							"Low temperature notification",
//							"Low temperature notification", null);
					incoming =  new VoxMessage(VoxMessage.VOX_TYPE_TEMP_LO,
							pkt.getAddress(), trigger_device_mac, System.currentTimeMillis(),
							VoxMessage.VOX_VAL_INVALID, cameraName);
				}

				
				if (incoming !=null && VoxMessageFilter.getInstance().checkIncomingVoxMesage(incoming))
				{
					Log.d("mbp", "new Notification .. vox msg: " + incoming + " :fr:" + incoming.getTrigger_mac() );
					
					final VoxMessage _incoming = incoming;
					Runnable notify = new Runnable()
					{
						public void run()
						{
							showNotification(_incoming);
						}
					};

					Thread worker = new Thread(notify);
					worker.start();
					
				}	

			}// while (running)

			mSSocket.close();
		}
	}
}
