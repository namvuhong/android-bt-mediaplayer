package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.msc3.PublicDefine;
import com.msc3.registration.SingleCamConfigureActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;


public class QueryCameraWifiListTask extends AsyncTask<String , String, List<CameraWifiEntry> > {

	
	
	public static final int NEW_APP_AVAILABLE = 1; 
	public static final int NO_UPDATE = 2; 
	
	private static final String MY_DEBUG_TAG = "mbp";
	

	private Context mContext;
	private Handler mHandler; 
	private String fw_version = null;
	
	private boolean reportNoUpdate = false;
	
	public QueryCameraWifiListTask(Context c, String fw_ver)
	{
		mContext = c;
		mHandler = null; 
		fw_version = fw_ver;
	}
	
	public QueryCameraWifiListTask(Context c, Handler mHandler, String fw_ver)
	{
		mContext = c;
		this.mHandler = mHandler;
		fw_version = fw_ver;
	}
	
	protected void onPreExecute ()
	{

	}



	protected void onPostExecute (List<CameraWifiEntry> result)
	{
		
	}

	
	//Assumption: Camera is in Direct Mode - otherwise need to modify code to support different ip
	
	/** Called when the activity is first created. */
	protected List<CameraWifiEntry> doInBackground(String... params) {
		
		URL url = null;
		List<CameraWifiEntry> result = null;
		HttpURLConnection conn = null;
		int responseCode = -1;
		boolean shouldUseNewParser = true;
		DataInputStream inputStream = null;
		
		/*query current  */
		String gatewayIp = SingleCamConfigureActivity.getGatewayIp(mContext);
		if (gatewayIp == null || gatewayIp.isEmpty())
		{
			return null;
		}
		
		int ver_no_1 = -1;
		int ver_no_2 = -1;
		Log.d("mbp", "fw version: " + fw_version);
		if (fw_version != null)
		{
			String[] fw_version_arr = fw_version.split("\\.");
			if (fw_version_arr != null && fw_version_arr.length == 3)
			{
				try
                {
					ver_no_1 = Integer.parseInt(fw_version_arr[1]);
	                ver_no_2 = Integer.parseInt(fw_version_arr[2]);
                }
                catch (NumberFormatException e)
                {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
                }
			}
		}
		
		String device_address_port = gatewayIp + ":" + 80;
		String usr_pass  = "camera:000000";
		String http_addr;
		if (ver_no_1 >= 13 || (ver_no_1 >= 12 && ver_no_2 >= 58))
		{
			http_addr = String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.GET_RT_LIST);
			shouldUseNewParser = true;
		}
		else
		{
			http_addr = String.format("%1$s%2$s%3$s","http://",
					device_address_port,
					PublicDefine.HTTP_CMD_PART+PublicDefine.GET_ROUTER_LIST);
			shouldUseNewParser = false;
		}
		Log.d("mbp", "get wifi list cmd: " + http_addr);
		
		/* Create a new TextView to display the parsingresult later. */
		try {
			url = new URL(http_addr);
			conn = (HttpURLConnection) url.openConnection();

			conn.addRequestProperty("Authorization", "Basic " +
					Base64.encodeToString(usr_pass.getBytes("UTF-8"),Base64.NO_WRAP));

			conn.setConnectTimeout(10000);
			conn.setReadTimeout(20000);
			responseCode = conn.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK)
			{
				inputStream = new DataInputStream(
						new BufferedInputStream(conn.getInputStream(),4*1024));
			}
			
			/* Get a SAXParser from the SAXPArserFactory. */
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();

			/* Get the XMLReader of the SAXParser we created. */
			
			XMLReader xr = sp.getXMLReader();
			/* Create a new ContentHandler and apply it to the XML-Reader*/
			XmlWifiListContentHandler myExampleHandler = 
					new XmlWifiListContentHandler(shouldUseNewParser);
			xr.setContentHandler(myExampleHandler);

			/* Parse the xml-data from our URL. */
			xr.parse(new InputSource(inputStream));
			/* Parsing has finished. */

			/* Our ExampleHandler now provides the parsed data to us. */
			result = myExampleHandler.getParsedData();
		}
		catch (Exception e) {
			/* Display any Error to the GUI. */
			e.printStackTrace();
			return null;
		}
		
		return result;
	}



	

	
	
	
}