package com.msc3;

import java.net.InetAddress;

public class ServerMessage extends VoxMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7464953237370814945L;


	private String server_msg;
	private String msg_url;
	
	public ServerMessage(int type, InetAddress ip, String mac,
			long trigger_time, String val, String name, String url)
	{
		super(type, ip, mac, trigger_time, -1, name);
		
		server_msg = val;
		msg_url = url;
	}

	public String getServer_msg() {
		return server_msg;
	}

	public void setServer_msg(String server_msg) {
		this.server_msg = server_msg;
	}
	
	public String getMsg_url() {
		return msg_url;
	}
	
	public void setMsg_url(String ssg_url) {
		this.msg_url = msg_url;
	}
}
