package com.msc3;


import java.security.NoSuchAlgorithmException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

/*******
 *  Borrowed from Galaxy
 * @author Vandana
 *
 */

public class SymmetricCipher {
    private static byte[] iv = {0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d,0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d}; //Initialization vector
   

    public static byte[] encrypt(byte[] inpBytes,
            String key, String xform) throws Exception {
       
        byte[] symKeyData = Base64.decode(key, Base64.NO_WRAP);
       
        // create the key
        final SecretKeySpec symKey = new SecretKeySpec(symKeyData, "AES");
        Cipher cipher = Cipher.getInstance(xform);
        IvParameterSpec ips = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, symKey, ips); //without IV also possible
        return cipher.doFinal(inpBytes);
    }
    public static byte[] encryptAES(byte[] inpBytes,
            String key) throws Exception {
    	//return encrypt(inpBytes,key,"AES/CTR/PKCS5PADDING" );
    	return encrypt(inpBytes,key,"AES/CTR/NoPadding" );
    }
    
    public static byte[] decryptAES(byte[] inpBytes,
            String key) throws Exception {
    	//return decrypt(inpBytes,key,"AES/CTR/PKCS5PADDING" );
    	return decrypt(inpBytes,key,"AES/CTR/NoPadding" );
    }

    public static byte[] decrypt(byte[] inpBytes,
            String key, String xform) throws Exception {

        
        //byte[] symKeyData = key.getBytes();
         byte[] symKeyData = Base64.decode(key, Base64.NO_WRAP);
        // create the key
        final SecretKeySpec symKey = new SecretKeySpec(symKeyData, "AES");

        Cipher cipher = Cipher.getInstance(xform);
        IvParameterSpec ips = new IvParameterSpec(iv);
        cipher.init(Cipher.DECRYPT_MODE, symKey, ips);
        return cipher.doFinal(inpBytes);
    }

    
    public static SecretKey secretkey() throws NoSuchAlgorithmException {

        //String xform = "DES/ECB/PKCS5Padding";
        //String xform="AES/CTR/PKCS5PADDING";
        // Generate a secret key
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        kg.init(128); // 128/256 key size is for AES , 56 is the keysize. Fixed for DES
        SecretKey key = kg.generateKey();

        return key;
    }

}
