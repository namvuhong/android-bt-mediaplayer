package com.msc3;

import java.util.Vector;


public interface IQuerySnapshotCallback {
	
	public void sendSnapshotResult(Vector<CamProfile> result);
}
