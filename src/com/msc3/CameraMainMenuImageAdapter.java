package com.msc3;



import com.blinkhd.R;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RelativeLayout;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

public class CameraMainMenuImageAdapter extends BaseAdapter {
	private Context mContext;

	final float scale ;
	
	private static final Integer[] routerOrDirect  =
	{
		R.drawable.main_menu_initial_bm, R.drawable.main_menu_direct_mode
	};
	
	// references to our images
	private Integer[] mThumbIds = {
			routerOrDirect[1], R.drawable.main_menu_router_mode, 
			R.drawable.main_menu_cam_settings, R.drawable.main_menu_exit 
	};
	



	public CameraMainMenuImageAdapter(Context c, boolean isRouterMode) {
		mContext = c;
		scale = mContext.getResources().getDisplayMetrics().density;
		
//		if (isRouterMode )
//		{
//			//Switch the image here 
//			mThumbIds[0] = routerOrDirect[1];
//		}
	}

	
	
	public int getCount() {
		return mThumbIds.length ;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}



	public boolean areAllItemsEnabled()
	{
		return true;
	}

	
	
	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout itemView = null;
		ImageView imageView = null;

		int height_dp = 46;//dp
		int width_dp = 400;//dp 
		//Convert to system pixel -- this value should be different from screen to screen 
		int height =(int) (height_dp * scale + 0.5f);
		int width = (int) (width_dp * scale + 0.5f);
		
		if (convertView == null) {  // if it's not recycled, initialize some attributes
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService
					(Context.LAYOUT_INFLATER_SERVICE);
			imageView = (ImageView) inflater.inflate(R.layout.infra_camera_main_menu_item_1, null);
		} else {
			imageView = (ImageView) convertView;
		}
		
		imageView.setLayoutParams(new ListView.LayoutParams(width, height));//pixel
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setImageResource(mThumbIds[position]);

		return imageView;
	}


}

