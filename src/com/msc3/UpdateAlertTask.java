package com.msc3;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.nxcomm.meapi.App;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.app.NotificationSettingRequestData;
import com.nxcomm.meapi.app.NotificationSettingResponse;
import com.nxcomm.meapi.app.RegisterAppResponse;
import com.nxcomm.meapi.device.CameraInfo;
import com.nxcomm.meapi.device.DeviceSetting;
import com.nxcomm.meapi.device.GetCameraInfoResponse;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView.SavedState;

public class UpdateAlertTask extends AsyncTask<String, String, Integer> {

	private String _error_desc;

	private static final int UPDATE_SUCCESS = 0x1;
	private static final int UPDATE_FAILED_SERVER_UNREACHABLE = 0x11;
	private static final int UPDATE_FAILED_WITH_DESC = 0x12;
	
	private Context mContext; 
	private IUpdateAlertCallBack mCallBack; 
	private int alertType;
	private boolean enableOrDisable;
	private String userToken;
	private int device_id;
	private static String device_code;
	
	public UpdateAlertTask(Context c, IUpdateAlertCallBack cb)
	{
		mContext = c; 
		mCallBack = cb;
		device_code = Secure.getString(c.getContentResolver(),
				Secure.ANDROID_ID);
	}

	@Override
	protected Integer doInBackground(String... params) {

		int ret = -1;
//		String usrToken = params[0];
//		String usrName = params[0];
//		String usrPass = params[0];
		userToken = params[0];
		
		//NOT USED NOW 
		//String regId  = params[1];
		
		/* Special case: MBP2k, 1k */
		String phonemodel = android.os.Build.MODEL;
		if  (phonemodel.equals(PublicDefine.PHONE_MBP2k) || phonemodel.equals(PublicDefine.PHONE_MBP1k))
		{
			ret = UPDATE_SUCCESS;
			return Integer.valueOf(ret);
		}
		
		String macAddress = PublicDefine.strip_colon_from_mac(params[1]);
		device_id = -1;
		try {
			device_id = Integer.parseInt(params[1]);
		} catch (NumberFormatException e2) {
			e2.printStackTrace();
		}
		
		String alertTypeStr = params[2];
		alertType = -1;
		try {
			alertType = Integer.parseInt(alertTypeStr);
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
		
		String enableOrDisableStr = params[3];
		if (enableOrDisableStr.equalsIgnoreCase(PublicDefine.ENABLE_NOTIFICATIONS_U_CMD))
		{
			enableOrDisable = true;
		}
		else //PublicDefine.DISABLE_NOTIFICATIONS_U_CMD
		{
			enableOrDisable = false;
		}
		
		//20120927:phung : use a different method here 
//		String http_cmd = PublicDefine.BM_SERVER + PublicDefine.BM_HTTP_CMD_PART + enableOrDisable + 
//				PublicDefine.ENABLE_NOTIFICATIONS_U_PARAM_1+ usrName +
//				PublicDefine.ENABLE_NOTIFICATIONS_U_PARAM_2+ macAddress +
//				PublicDefine.ENABLE_NOTIFICATIONS_U_PARAM_3 + alertType;
		
		SharedPreferences settings = mContext.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		int appId = (int) settings.getLong(PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID, -1);

		Log.d("mbp", "saved_token: " + userToken);
		Log.d("mbp", "Notification settings: appId: " + appId +
				", registration_id: " + device_id +
				", alertType: " + alertType +
				", enable? " + enableOrDisable);

		if (appId != -1 && device_id != -1 && alertType != -1)
		{
			NotificationSettingRequestData settingData = 
					new NotificationSettingRequestData(userToken);
			settingData.addSetting(device_id, alertType, enableOrDisable);

			NotificationSettingResponse notifSettingRes = null;
			try {
				notifSettingRes = App.addNotificationSetting(appId, settingData);
				Log.d("mbp", "Notification setting res: " + notifSettingRes.getStatus());
				if (notifSettingRes != null && 
						notifSettingRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					ret = UPDATE_SUCCESS;
					Log.d("mbp", "Update alert settings success");
				}
			} catch (SocketTimeoutException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else
		{
			Log.d("mbp", "Update alert settings failed");
		}


		return new Integer(ret);
	}
	
	protected void onPreExcecute()
	{
		if (mCallBack != null)
		{
			mCallBack.pre_update();
		}
	}

	/* UI thread */
	protected void onPostExecute(Integer result)
	{
		Log.d("mbp", "$$$result: " + result.intValue());
		if(result.intValue() == UPDATE_SUCCESS)
		{
			mCallBack.update_alert_success();
		}
		else
		{
			mCallBack.update_alert_failed(alertType, enableOrDisable);
		}
	}
}
