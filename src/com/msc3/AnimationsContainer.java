package com.msc3;

import java.lang.ref.SoftReference;

import com.blinkhd.R;

import cz.havlena.ffmpeg.ui.FFMpegPlayerActivity;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.ImageView;

public class AnimationsContainer {
	public int FPS = 10;  // animation FPS

	// single instance procedures
	private static AnimationsContainer mInstance;

	private AnimationsContainer() {
	};

	public static AnimationsContainer getInstance() {
		if (mInstance == null)
			mInstance = new AnimationsContainer();
		return mInstance;
	}

	// animation progress dialog frames
	private int[] mProgressAnimFrames = { 
			//R.drawable.logo_30001, R.drawable.logo_30002, R.drawable.logo_30003 
	};

	// animation splash screen frames
	private int[] mSplashAnimFrames = { 
//			R.drawable.mestartup000001,
//			R.drawable.mestartup000002,
//			R.drawable.mestartup000003,
//			R.drawable.mestartup000004,
//			R.drawable.mestartup000005,
//			R.drawable.mestartup000006,
//			R.drawable.mestartup000007,
//			R.drawable.mestartup000008,
//			R.drawable.mestartup000009,
//
//			R.drawable.mestartup000010,
//			R.drawable.mestartup000011,
//			R.drawable.mestartup000012,
//			R.drawable.mestartup000013,
//			R.drawable.mestartup000014,
//			R.drawable.mestartup000015,
//			R.drawable.mestartup000016,
//			R.drawable.mestartup000017,
//			R.drawable.mestartup000018,
			R.drawable.mestartup000019,

			R.drawable.mestartup000020,
			R.drawable.mestartup000021,
			R.drawable.mestartup000022,
			R.drawable.mestartup000023,
			R.drawable.mestartup000024,
			R.drawable.mestartup000025,
			R.drawable.mestartup000026,
			R.drawable.mestartup000027,
			R.drawable.mestartup000028,
			R.drawable.mestartup000029,

			R.drawable.mestartup000030,
			R.drawable.mestartup000031,
			R.drawable.mestartup000032,

	};


	/**
	 * @param imageView 
	 * @return progress dialog animation
	 */
	//public FramesSequenceAnimation createProgressDialogAnim(ImageView imageView) {
	//return new FramesSequenceAnimation(imageView, mProgressAnimFrames);
	//}

	/**
	 * @param imageView
	 * @return splash screen animation
	 */
	public FramesSequenceAnimation createSplashAnim(ImageView imageView) {
		return new FramesSequenceAnimation(imageView, mSplashAnimFrames, FPS);
	}


	public FramesSequenceAnimation createSplashAnim1(VideoSurfaceView mView) {
		return new FramesSequenceAnimation(mView, mSplashAnimFrames, FPS);
	}

	/**
	 * AnimationPlayer. Plays animation frames sequence in loop
	 */
	public class FramesSequenceAnimation {
		private int[] mFrames; // animation frames
		private int mIndex; // current frame
		private boolean mShouldRun; // true if the animation should continue running. Used to stop the animation
		private boolean mIsRunning; // true if the animation currently running. prevents starting the animation twice
		private SoftReference<ImageView> mSoftReferenceImageView; // Used to prevent holding ImageView when it should be dead.
		
		private SoftReference<VideoSurfaceView> mSoftReferenceVideoView; // Used to prevent holding ImageView when it should be dead.
		
		private Handler mHandler;
		private int mDelayMillis;
		//private OnAnimationStoppedListener mOnAnimationStoppedListener;

		private Bitmap mBitmap = null;
		private BitmapFactory.Options mBitmapOptions;

		public FramesSequenceAnimation(ImageView imageView, int[] frames, int fps) {
			mHandler = new Handler();
			mFrames = frames;
			mIndex = -1;
			mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
			mShouldRun = false;
			mIsRunning = false;
			mDelayMillis = 1000 / fps;

			imageView.setImageResource(mFrames[0]);

			// use in place bitmap to save GC work (when animation images are the same size & type)
			//if (Build.VERSION.SDK_INT >= 11)
			{
				Bitmap bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
				int width = bmp.getWidth();
				int height = bmp.getHeight();
				Bitmap.Config config = bmp.getConfig();
				mBitmap = Bitmap.createBitmap(width, height, config);
				mBitmapOptions = new BitmapFactory.Options();
				// setup bitmap reuse options. 
				//mBitmapOptions.inBitmap = mBitmap;
				//mBitmapOptions.inMutable = true;
				mBitmapOptions.inSampleSize = 1;
				mBitmapOptions.inDither = true;
				
			}


		}


		public FramesSequenceAnimation(VideoSurfaceView imageView, int[] frames, int fps) {

			
			mBitmapOptions = new BitmapFactory.Options();
			// setup bitmap reuse options. 
			//mBitmapOptions.inBitmap = mBitmap;
			//mBitmapOptions.inMutable = true;
			mBitmapOptions.inSampleSize = 1;
			mBitmapOptions.inDither = true;
			mBitmapOptions.inPreferredConfig = Config.ARGB_8888;
			
			
			
			
			mHandler = new Handler()
			{
				@Override
				public void handleMessage(Message msg) {

					switch(msg.what)
					{
					case FFMpegPlayerActivity.MSG_SURFACE_CREATED:

						new Thread (new Runnable() {
							
							@Override
							public void run() {
								
								while (mIsRunning == true)
								{
									showFrame_bg();

								}
								
								
							}
						}).start(); 
						
						
						break;
					default:
						break;
					}


					super.handleMessage(msg);
				}
			};
			mFrames = frames;
			mIndex = -1;
			mShouldRun = false;
			mIsRunning = true;
			mDelayMillis = 1000 / fps;

			mSoftReferenceVideoView = new SoftReference<VideoSurfaceView>(imageView);

			imageView.setHandler(null);//mHandler);

		}




		private int getNext() {
			mIndex++;

			//Log.d("mbp", "get frame: "+ mIndex);

			if (mIndex >= mFrames.length)
			{
				mIndex =  mFrames.length-1;

				return -1; 
			}
			else
			{
				return mFrames[mIndex];
			}
		}
		
		private Bitmap mFrame ; 

		public void showFrame_bg() {
			Canvas c = null;
			
			//get the next frame 
			int imageRes = getNext();

			if (imageRes == -1)
			{
				Log.d("mbp", "Stop myself"); 
				mIsRunning = false; 
				return;
			}

			
			
			VideoSurfaceView mView = mSoftReferenceVideoView.get();
			SurfaceHolder _surfaceHolder = mView.get_SurfaceHolder();
			if (_surfaceHolder == null) {
				Log.w("mbp", "_surfaceHolder is NULL");
				return;
			}

			try {
				c = _surfaceHolder.lockCanvas(null);
				if (c == null) {
					return;
				}
			
				
				mFrame = BitmapFactory.decodeResource(
						mView.getResources(), imageRes, mBitmapOptions);

			
				Rect src = new Rect(0, 0, mFrame.getWidth(),mFrame.getHeight());

				Rect dest = new Rect(0, 0, c.getWidth(), c.getHeight());

				synchronized (_surfaceHolder) {
					c.drawBitmap(mFrame, src, dest, null);
				}
			} finally {
				// do this in a finally so that if an exception is thrown
				// during the above, we don't leave the Surface in an
				// inconsistent state
				if (c != null) {
					_surfaceHolder.unlockCanvasAndPost(c);
				}
			}
		}
		
		/**
		 * Starts the animation
		 */
		public synchronized void start() {
			mShouldRun = true;
			if (mIsRunning)
				return;

			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					ImageView imageView = mSoftReferenceImageView.get();
					if (!mShouldRun || imageView == null) {
						mIsRunning = false;
						/*if (mOnAnimationStoppedListener != null) {
                        mOnAnimationStoppedListener.AnimationStopped();
                    }*/
						return;
					}

					mIsRunning = true;


					//get the next frame 
					int imageRes = getNext();

					if (imageRes == -1)
					{
						Log.d("mbp", "Stop myself"); 
						mIsRunning = false; 
						return;
					}


					long beforeSetImageRes = System.currentTimeMillis();
					

					if (imageView.isShown()) {


						/*if (mBitmap != null) 
						{ // so Build.VERSION.SDK_INT >= 11
							Bitmap bitmap = null;
							try {
								bitmap = BitmapFactory.decodeResource(imageView.getResources(), imageRes, mBitmapOptions);
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (bitmap != null) {
								Log.d("mpb", "Decoded");
								imageView.setImageBitmap(bitmap);
							} else {
								imageView.setImageResource(imageRes);
								mBitmap.recycle();
								mBitmap = null;
							}
						} 
						else*/


						{
							imageView.setImageResource(imageRes);
						}



					}

					
					long afterSetImageRes = System.currentTimeMillis();
					int wasted_durations = (int) (afterSetImageRes - beforeSetImageRes) ; 

					if ( wasted_durations < mDelayMillis)
					{
//						mHandler.postDelayed(this, mDelayMillis-wasted_durations);
						mHandler.postDelayed(this, 10);
					}
					else
					{
						//DO IT now
						mHandler.postDelayed(this,0);
					}
					

				}
			};



			mHandler.post(runnable);
		}

		/**
		 * Stops the animation
		 */
		public synchronized void stop() {
			mShouldRun = false;
		}
	}
}