package com.msc3;

public interface IAudioSink {
	void onPCM(byte[] pcmData);
}
