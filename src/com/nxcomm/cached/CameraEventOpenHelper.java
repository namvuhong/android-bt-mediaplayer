package com.nxcomm.cached;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.HttpResponse;
import com.nxcomm.meapi.device.GeneralData;
import com.nxcomm.meapi.device.GetTimelineEventsResponse;
import com.nxcomm.meapi.device.TimelineEvent;

public class CameraEventOpenHelper extends SQLiteOpenHelper
{
	private static final String			 TAG					 = "CameraEventOpenHelper";
	private static CameraEventOpenHelper	mInstance	         = null;
	public static final String	         TABLE_CAMERA_EVENTS	 = "camera_events";

	// Database column

	public static final String	         COLUMN_ID	             = "_id";
	public static final String	         COLUMN_CAMERA_UDID	     = "camera_source_udid";
	public static final String	         COLUMN_CAMERA_OWNER	 = "camera_owner";

	public static final String	         COLUMN_EVENT_ID	     = "event_id";
	public static final String	         COLUMN_EVENT_ALERT	     = "event_alert";
	public static final String	         COLUMN_EVENT_VALUE	     = "event_value";
	public static final String	         COLUMN_EVENT_ALERT_NAME = "event_id_alert_name";
	public static final String	         COLUMN_EVENT_TIMESTAMP	 = "event_ts";
	public static final String	         COLUMN_EVENT_DATA	     = "event_data";

	private static final String	         DATABASE_NAME	         = "hubble.db";
	private static final int	         DATABASE_VERSION	     = 1;

	// Database creation sql statement
	private static final String	         DATABASE_CREATE	     = "create table "
	                                                                     + TABLE_CAMERA_EVENTS
	                                                                     + "("
	                                                                     + COLUMN_CAMERA_UDID
	                                                                     + " text not null, "
	                                                                     + COLUMN_CAMERA_OWNER
	                                                                     + " text not null,"
	                                                                     + COLUMN_EVENT_ID
	                                                                     + " text primary key,"
	                                                                     + COLUMN_EVENT_ALERT
	                                                                     + " integer,"
	                                                                     + COLUMN_EVENT_VALUE
	                                                                     + " text,"
	                                                                     + COLUMN_EVENT_ALERT_NAME
	                                                                     + " text,"
	                                                                     + COLUMN_EVENT_TIMESTAMP
	                                                                     + " integer,"
	                                                                     + COLUMN_EVENT_DATA
	                                                                     + " text);";

	private static volatile long	     NumberOfRowInserted	 = 0;

	public static CameraEventOpenHelper getInstance(Context ctx)
	{

		if (mInstance == null)
		{
			mInstance = new CameraEventOpenHelper(ctx.getApplicationContext());
		}
		return mInstance;
	}

	private CameraEventOpenHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database)
	{
		Log.i("mbp", "DATABASE is created.");
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onOpen(SQLiteDatabase db)
	{
		super.onOpen(db);
		db.enableWriteAheadLogging();
		Log.i("mbp", "DATABASE file path: " + db.getPath());
		Log.i("mbp", "DATABASE opened.");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
//		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
//		        + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CAMERA_EVENTS);
		onCreate(db);
	}

	/**
	 * Convert date string to time stamp (second from 1970)
	 * 
	 * @param dateString
	 *            format "yyyy-MM-dd HH:mm:ss"
	 * @return milli-seconds passed from 1970-1-1 0:0:0
	 */
	private long convertDateToTimeStamp(String dateString)
	{
		long ret = 0;

		SimpleDateFormat dateFormat = new SimpleDateFormat(
		        "yyyy-MM-dd HH:mm:ss");
		Date convertedDate = new Date();
		TimeZone tz = TimeZone.getTimeZone("UTC");
		dateFormat.setTimeZone(tz);

		try
		{
			convertedDate = dateFormat.parse(dateString);
			ret = convertedDate.getTime();
			//Log.i("mbp", "Before time stamp " + ret);
		}
		catch (Exception e)
		{
			Log.i(TAG, "Convert dateString to timestamp failed.");
			e.printStackTrace();
		}

		return ret;
	}

	public TimelineEvent[] getTimelineEventFromDB(String apiKey, String regId,
	        String before_start_time, String event_code, String alerts,
	        int page, int size)
	{

		TimelineEvent[] tes = null;
		try
		{
			if (page == 1)
			{
				startUpdateTask(apiKey, regId);
			}

			long event_ts = convertDateToTimeStamp(before_start_time);

			String[] column = { CameraEventOpenHelper.COLUMN_EVENT_ID,
			        CameraEventOpenHelper.COLUMN_EVENT_ALERT,
			        CameraEventOpenHelper.COLUMN_EVENT_ALERT_NAME,
			        CameraEventOpenHelper.COLUMN_EVENT_VALUE,
			        CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP,
			        CameraEventOpenHelper.COLUMN_EVENT_DATA };
			String selection = CameraEventOpenHelper.COLUMN_CAMERA_UDID
			        + " =? AND " + CameraEventOpenHelper.COLUMN_CAMERA_OWNER
			        + " =? AND " + CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP
			        + " <= " + event_ts;

			String[] selectionArgs = { regId, apiKey };

			String sortOrder = CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP
			        + " DESC";

			SQLiteDatabase db = getWritableDatabase();
			db.enableWriteAheadLogging();
			db.beginTransaction();

			Cursor event_cursor = db.query(
			        CameraEventOpenHelper.TABLE_CAMERA_EVENTS, column,
			        selection, selectionArgs, null, null, sortOrder, (page - 1)
			                * size + "," + size);

			if (event_cursor.getCount() > 0)
			{
				tes = new TimelineEvent[event_cursor.getCount()];
				//Log.i(TAG, "Camera events from db count: " + tes.length);
				int i = 0;
				if (event_cursor.moveToFirst())
				{

					do
					{
						tes[i] = new TimelineEvent();

						String event_id = event_cursor
						        .getString(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_ID));
						int event_alert = event_cursor
						        .getInt(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_ALERT));
						String event_value = event_cursor
						        .getString(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_VALUE));
						String event_alert_name = event_cursor
						        .getString(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_ALERT_NAME));
						long event_timestamp = event_cursor
						        .getLong(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP));
						String event_data = event_cursor
						        .getString(event_cursor
						                .getColumnIndexOrThrow(CameraEventOpenHelper.COLUMN_EVENT_DATA));

						// decode
						Date date = new Date(event_timestamp);
						event_data = decodeBase64String(event_data);
						Gson gson = new Gson();

						GeneralData[] data = gson.fromJson(event_data,
						        GeneralData[].class);

						tes[i].setId(Integer.parseInt(event_id));
						tes[i].setAlert(event_alert);
						tes[i].setTime_stamp(date);
						tes[i].setAlert_name(event_alert_name);
						tes[i].setValue(event_value);
						tes[i].setData(data);

						//Log.i("mbp", "Timestamp when get from db: "
						//        + tes[i].getTime_stamp().getTime());

						i++;

					}
					while (event_cursor.moveToNext());

				}

				// free
				// db.enableWriteAheadLogging();
				db.setTransactionSuccessful();
			}

			db.endTransaction();
			event_cursor.close();
			//db.close();

		}

		catch (SQLiteException ex)
		{
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return tes;
	}

	/**
	 * Insert TimelineEvent array to database, return number of row inserted
	 * succeeded.
	 * 
	 * @param userName
	 *            : camera_owner
	 * @param udid
	 *            : camera_udid
	 * @param events
	 *            : events array
	 * @return number of row insert sucessfull.
	 */
	public int insertEvent(String userName, String udid, TimelineEvent[] events)
	{
		int result = 0;

		try
		{
			SQLiteDatabase db = getWritableDatabase();
			db.enableWriteAheadLogging();
			db.beginTransaction();
			
			// Create a new map of values, where column names are the keys
			for (TimelineEvent event : events)
			{
				ContentValues values = new ContentValues();

				values.put(CameraEventOpenHelper.COLUMN_CAMERA_UDID, udid);
				values.put(CameraEventOpenHelper.COLUMN_CAMERA_OWNER, userName);

				values.put(CameraEventOpenHelper.COLUMN_EVENT_ID, event.getId());
				values.put(CameraEventOpenHelper.COLUMN_EVENT_ALERT,
				        event.getAlert());
				values.put(CameraEventOpenHelper.COLUMN_EVENT_ALERT_NAME,
				        event.getAlert_name());
				values.put(CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP, event
				        .getTime_stamp().getTime());
//				Log.i(TAG, "Event Time stamp: "
//				        + event.getTime_stamp().getTime());

				if (event.getData() != null)
				{
					Gson gson = new Gson();

					String str = gson.toJson(event.getData());

					String encodeBase64String = encodeString2Base64(str);
					values.put(CameraEventOpenHelper.COLUMN_EVENT_DATA,
					        encodeBase64String);

				}

				// Insert the new row, returning the primary key value of the
				// new
				// row
				long newRowID = db
				        .insert(CameraEventOpenHelper.TABLE_CAMERA_EVENTS,
				                null, values);
				if (newRowID == -1)
				{
					//Log.i(TAG, "Cannot insert row to database.");
				}
				else
				{
					NumberOfRowInserted++;
					//Log.i("mbp", "Number of row inserted successfull = "
					//        + NumberOfRowInserted + " row ID " + newRowID);
					result++;
					//Log.i("mbp", "Insert row successfull. " + values);
				}
			}
			
			db.setTransactionSuccessful();
			db.endTransaction();

			//db.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return result;
	}

	public boolean insertEvent(String userName, String udid,
	        TimelineEvent event, SQLiteDatabase db)
	{
		boolean result = false;

		try
		{
			// Create a new map of values, where column names are the keys
			ContentValues values = new ContentValues();

			values.put(CameraEventOpenHelper.COLUMN_CAMERA_UDID, udid);
			values.put(CameraEventOpenHelper.COLUMN_CAMERA_OWNER, userName);

			values.put(CameraEventOpenHelper.COLUMN_EVENT_ID, event.getId());
			values.put(CameraEventOpenHelper.COLUMN_EVENT_ALERT,
			        event.getAlert());
			values.put(CameraEventOpenHelper.COLUMN_EVENT_ALERT_NAME,
			        event.getAlert_name());
			values.put(CameraEventOpenHelper.COLUMN_EVENT_TIMESTAMP, event
			        .getTime_stamp().getTime());

			if (event.getData() != null)
			{
				Gson gson = new Gson();

				String str = gson.toJson(event.getData());

				String encodeBase64String = encodeString2Base64(str);
				values.put(CameraEventOpenHelper.COLUMN_EVENT_DATA,
				        encodeBase64String);

			}

			// Insert the new row, returning the primary key value of the new
			// row
			long newRowID = db.insert(
			        CameraEventOpenHelper.TABLE_CAMERA_EVENTS, null, values);
			if (newRowID == -1)
			{
				//Log.i(TAG, "Cannot insert row to database.");
			}
			else
			{
				//Log.i("mbp", "Insert row successfull. ");
			}

			result = true;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return result;

	}

	public void removeCacheForCamera(String regId)
	{
		try
		{
			SQLiteDatabase db = getWritableDatabase();

			String whereClause = COLUMN_CAMERA_UDID + " =? ";

			int rowDeleted = db.delete(TABLE_CAMERA_EVENTS, whereClause,
			        new String[] { regId });

			//Log.i(TAG, "Row deleted: " + rowDeleted);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void removeCacheForUser(String userToken)
	{
		try
		{
			SQLiteDatabase db = getWritableDatabase();

			String whereClause = COLUMN_CAMERA_OWNER + " =? ";

			int rowDeleted = db.delete(TABLE_CAMERA_EVENTS, whereClause,
			        new String[] { userToken });

			//Log.i(TAG, "Row deleted: " + rowDeleted);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	private void startUpdateTask(String apiKey, String regId)
	{
		Date dateTime = new Date(System.currentTimeMillis());
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat destFormat = new SimpleDateFormat(
		        "yyyy-MM-dd HH:mm:ss");
		destFormat.setTimeZone(tz);
		String before_start_time = destFormat.format(dateTime);

		//Log.i(TAG, "Starting update database.");

		UpdateDatabaseTask updateDBTask = new UpdateDatabaseTask();
		updateDBTask.execute(apiKey, regId, before_start_time, "1");
	}

	private String encodeString2Base64(String str)
	{
		return Base64.encodeToString(str.getBytes(), Base64.DEFAULT);

	}

	private String decodeBase64String(String str)
	{
		if (str != null)
		{
			byte[] orginValue = Base64.decode(str, Base64.DEFAULT);
			return new String(orginValue);
		}
		return null;
	}

	private class UpdateDatabaseTask extends AsyncTask<String, Void, Boolean>
	{
		private String	saved_token;
		private String	regId;
		private String	before_start_time;
		private String	event_code;
		private String	alerts;
		private int		page	= 0;
		private int		offset;
		private int		size;

		protected Boolean doInBackground(String... params)
		{
			boolean needToRunMore = false;

			saved_token = params[0];
			regId = params[1];
			before_start_time = params[2];
			page = Integer.parseInt(params[3]);

			event_code = null;
			alerts = null;
			size = 50;

			TimelineEvent[] timelineEvents = null;
			try
			{

				String before_start_time_enc = Uri.encode(before_start_time);

				GetTimelineEventsResponse eventRes = Device.getTimelineEvents(
				        saved_token, regId, before_start_time_enc, event_code,
				        alerts, page, size);

				if (eventRes != null
				        && eventRes.getStatus() == HttpURLConnection.HTTP_OK)
				{
					timelineEvents = eventRes.getEvents();
					int rowInserted = insertEvent(saved_token, regId,
					        timelineEvents);

//					Log.i(TAG, "Number of row inserted for update: "
//					        + rowInserted);
					if (rowInserted == 0)
					{
						return false;
					}

					if (rowInserted < timelineEvents.length)
					{
						needToRunMore = false;
					}

					else
					{
						needToRunMore = true;
					}
				}

			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return needToRunMore;
		}

		protected void onPostExecute(Boolean result)
		{
			if (result == true)
			{
				//Log.i(TAG, "Camera event database need to update more.");

				page += 1;
				new UpdateDatabaseTask().execute(saved_token, regId,
				        before_start_time, page + "");
			}
			else
			{
				//Log.i(TAG, "Camera events database is updated all.");
			}
		}

	}

}
