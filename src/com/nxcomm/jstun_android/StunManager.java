package com.nxcomm.jstun_android;

import java.math.BigInteger;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.util.Log;

public class StunManager 
{
	
	/*
	 * init class
	 *
	private static StunManager mStunManager = new StunManager();


	public static StunManager getInstance()
	{
		return mStunManager;
	}
	*/
	public StunManager()
	{
		
	}
	/*
	 * remain
	 */
	private DatagramSocket audioRtpReceiver;
	private DatagramSocket videoRtpReceiver;
	private String publicIP;
	private boolean isSymmetricNet;
	public DatagramSocket getAudioRtpReceiverSock() {
		return audioRtpReceiver;
	}
	public void setPublicIP(int intIP) throws UnknownHostException
	{
		byte[] bytes = BigInteger.valueOf(intIP).toByteArray();
		for(int i = 0; i < bytes.length / 2; i++)
		{
		    byte temp = bytes[i];
		    bytes[i] = bytes[bytes.length - i - 1];
		    bytes[bytes.length - i - 1] = temp;
		}
		InetAddress address = InetAddress.getByAddress(bytes);
		this.publicIP = address.getHostAddress();
	}
	public DatagramSocket getVideoRtpReceiverSock() {
		return videoRtpReceiver;
	}
	
	public void setAudioRtpReceiverSock(int port) throws SocketException {
		audioRtpReceiver = new DatagramSocket(port);
		//Log.d("mbp","setAudioRtpReceiver Sock completed. Port "+port);
	}

	public void setVideoRtpReceiverSock(int port) throws SocketException {
		videoRtpReceiver = new DatagramSocket(port);
		//Log.d("mbp","setVideoRtpReceiver Sock completed. Port "+port);
	}

	public String getPublicIP() {
		return publicIP;
	}

	public void setPublicIP(String publicIP) {
		this.publicIP = publicIP;
	}

	public boolean isSymmetricNet() {
		return isSymmetricNet;
	}

	public void setSymmetricNet(boolean isSymmetricNet) {
		this.isSymmetricNet = isSymmetricNet;
	}
}
