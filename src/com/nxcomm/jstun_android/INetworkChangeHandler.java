package com.nxcomm.jstun_android;

public interface INetworkChangeHandler
{
	public void onNetworkChangeCompleted();
}
