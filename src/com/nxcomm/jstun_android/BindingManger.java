

package com.nxcomm.jstun_android;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.javawi.jstun.attribute.ChangeRequest;
import de.javawi.jstun.attribute.ErrorCode;
import de.javawi.jstun.attribute.MappedAddress;
import de.javawi.jstun.attribute.MessageAttribute;
import de.javawi.jstun.attribute.MessageAttributeException;
import de.javawi.jstun.attribute.MessageAttributeParsingException;
import de.javawi.jstun.attribute.ResponseAddress;
import de.javawi.jstun.header.MessageHeader;
import de.javawi.jstun.header.MessageHeaderParsingException;
import de.javawi.jstun.util.UtilityException;

public class BindingManger 
{
	
	//public static final String STUN_SERVER = "stun.sipgate.net";
	//public static final int STUN_SERVER_PORT = 10000;
	public static final String STUN_SERVER = "stun.monitoreverywhere.com";
	public static final int STUN_SERVER_PORT = 3478;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BindingManger.class);
	private static final int DEFAULT_DATASOCK_TIMEOUT = 10*1000;
	
	String stunServer;
	int port;
	int timeout = 300; //ms
	Timer timer;

	// start value for binary search - should be carefully choosen
	int upperBinarySearchLifetime = 345000; // ms
	int lowerBinarySearchLifetime = 0;
	int binarySearchLifetime = ( upperBinarySearchLifetime + lowerBinarySearchLifetime ) / 2;
	
	// lifetime value
	int lifetime = -1; // -1 means undefined.
	boolean completed = false;
		
	
	private MappedAddress audioRtpReceiver_ma, videoRtpReceiver_ma, commandReceiver_ma;

	private DatagramSocket audioRtpReceiver;
	private DatagramSocket videoRtpReceiver;
	private DatagramSocket commandReceiver;
	
	//Future: RTCP
	private MappedAddress audioRtcpReceiver, videoRtcpReceiver;
	
	public MappedAddress getAudioRtpReceiver() {
		return audioRtpReceiver_ma;
	}

	public MappedAddress getVideoRtpReceiver() {
		return videoRtpReceiver_ma;
	}

	public MappedAddress getCommandReceiver() {
		return commandReceiver_ma;
	}
	
	public DatagramSocket getAudioRtpReceiverSock() {
		return audioRtpReceiver;
	}

	public DatagramSocket getVideoRtpReceiverSock() {
		return videoRtpReceiver;
	}

	public DatagramSocket getCommandReceiverSock() {
		return commandReceiver;
	}
	
	

	
	
	private static BindingManger mStunBinder = new BindingManger(STUN_SERVER, STUN_SERVER_PORT);


	public static BindingManger getInstance()
	{
		return mStunBinder;
	}





	private BindingManger(String stunServer, int port) {
		super();
		this.stunServer = stunServer;
		this.port = port;
		timer = new Timer(true);
	}
	
	public boolean createSockets() throws UtilityException, SocketException, UnknownHostException, IOException, MessageAttributeParsingException, MessageAttributeException, MessageHeaderParsingException {
		
		
		if (bindingAudioRtpSocket() || 
				bindingVideoRtpSocket() ||
				bindingCommandSocket()) 
		{
			return false;
		}
		
		/*TODO:
		 * BindingLifetimeTask task = new BindingLifetimeTask();
		timer.schedule(task, binarySearchLifetime);
		LOGGER.debug("Timer scheduled initially: " + binarySearchLifetime + ".");*/
		
		return true; 
	}
	
	
	
	


	/**
	 * @return false  - OK  - first socket is created on server
	 *         true  -  some error 
	 * @throws UtilityException
	 * @throws IOException
	 * @throws MessageHeaderParsingException
	 * @throws MessageAttributeParsingException
	 */
	private boolean bindingAudioRtpSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = DEFAULT_DATASOCK_TIMEOUT;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port);
		initialSocket.send(send);
		LOGGER.debug("Binding Request sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		audioRtpReceiver = initialSocket; 

		audioRtpReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			LOGGER.debug("Message header contains an Errorcode message attribute.");
			return true;
		}
		if (audioRtpReceiver_ma == null) {
			LOGGER.debug("Response does not contain a Mapped Address message attribute.");
			return true;
		}
		return false;
	}
	
	private boolean bindingVideoRtpSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = 10*1000;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port+1);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port+1);
		initialSocket.send(send);
		LOGGER.debug("Binding Request sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		videoRtpReceiver = initialSocket; 

		videoRtpReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			LOGGER.debug("Message header contains an Errorcode message attribute.");
			return true;
		}
		if (videoRtpReceiver_ma == null) {
			LOGGER.debug("Response does not contain a Mapped Address message attribute.");
			return true;
		}
		
		return false;
	}
	
	
	private boolean bindingCommandSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = 10*1000;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port+1);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port+1);
		initialSocket.send(send);
		LOGGER.debug("Binding Request sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		commandReceiver = initialSocket; 

		commandReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			LOGGER.debug("Message header contains an Errorcode message attribute.");
			return true;
		}
		if (videoRtpReceiver_ma == null) {
			LOGGER.debug("Response does not contain a Mapped Address message attribute.");
			return true;
		}
		
		return false;
	}
	
	
	
	public int getLifetime() {
		return lifetime;
	}
	
	public boolean isCompleted() {
		return completed;
	}
	
	public void setUpperBinarySearchLifetime(int upperBinarySearchLifetime) {
		this.upperBinarySearchLifetime = upperBinarySearchLifetime;
		binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
	}
	
	class BindingLifetimeTask extends TimerTask {
		
		public BindingLifetimeTask() {
			super();
		}
		
		public void run() {
			try {
				lifetimeQuery();
			} catch (Exception e) {
				LOGGER.debug("Unhandled Exception. BindLifetimeTasks stopped.");
				e.printStackTrace();
			}
		}
		
		public void lifetimeQuery() throws UtilityException, MessageAttributeException, MessageHeaderParsingException, MessageAttributeParsingException, IOException {
			try {
				DatagramSocket socket = new DatagramSocket();
				socket.connect(InetAddress.getByName(stunServer), port);
				socket.setSoTimeout(timeout);
			
				MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
				sendMH.generateTransactionID();
				ChangeRequest changeRequest = new ChangeRequest();
				ResponseAddress responseAddress = new ResponseAddress();
				responseAddress.setAddress(audioRtpReceiver_ma.getAddress());
				responseAddress.setPort(audioRtpReceiver_ma.getPort());
				
				
				
				sendMH.addMessageAttribute(changeRequest);
				sendMH.addMessageAttribute(responseAddress);
				byte[] data = sendMH.getBytes();
			
				DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port);
				socket.send(send);
				LOGGER.debug("Binding Request sent.");
		
				MessageHeader receiveMH = new MessageHeader();
				while (!(receiveMH.equalTransactionID(sendMH))) {
					DatagramPacket receive = new DatagramPacket(new byte[200], 200);
					audioRtpReceiver.receive(receive);
					receiveMH = MessageHeader.parseHeader(receive.getData());
					receiveMH.parseAttributes(receive.getData());
				}
				ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
				if (ec != null) {
					LOGGER.debug("Message header contains errorcode message attribute.");
					return;
				}
				
				System.out.println("lifetimeQuery 08: ma:" +audioRtcpReceiver);
				
				LOGGER.debug("Binding Response received.");
				if (upperBinarySearchLifetime == (lowerBinarySearchLifetime + 1)) {
					LOGGER.debug("BindingManger completed. UDP binding lifetime: " + binarySearchLifetime + ".");
					completed = true;
					return;
				}
				lifetime = binarySearchLifetime;
				LOGGER.debug("Lifetime update: " + lifetime + ".");
				lowerBinarySearchLifetime = binarySearchLifetime;
				binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
				if (binarySearchLifetime > 0) {
					BindingLifetimeTask task = new BindingLifetimeTask();
					timer.schedule(task, binarySearchLifetime);
					LOGGER.debug("Timer scheduled: " + binarySearchLifetime + ".");
				} else {
					completed = true;
				}
			} catch (SocketTimeoutException ste) {
				LOGGER.debug("Read operation at query socket timeout.");
				if (upperBinarySearchLifetime == (lowerBinarySearchLifetime + 1)) {
					LOGGER.debug("BindingManger completed. UDP binding lifetime: " + binarySearchLifetime + ".");
					completed = true;
					return;
				}
				upperBinarySearchLifetime = binarySearchLifetime;
				binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
				
				if (binarySearchLifetime > 0) {
					if (bindingAudioRtpSocket()) {
						return;
					}
					BindingLifetimeTask task = new BindingLifetimeTask();
					timer.schedule(task, binarySearchLifetime);
					LOGGER.debug("Timer scheduled: " + binarySearchLifetime + ".");
				} else {
					completed = true;
				}
			}
		}
	}
}

