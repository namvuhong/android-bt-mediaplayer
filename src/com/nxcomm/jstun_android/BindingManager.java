

package com.nxcomm.jstun_android;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;


import android.util.Log;

import com.google.gson.Gson;

import de.javawi.jstun.attribute.ChangeRequest;
import de.javawi.jstun.attribute.ErrorCode;
import de.javawi.jstun.attribute.MappedAddress;
import de.javawi.jstun.attribute.MessageAttribute;
import de.javawi.jstun.attribute.MessageAttributeException;
import de.javawi.jstun.attribute.MessageAttributeParsingException;
import de.javawi.jstun.attribute.ResponseAddress;
import de.javawi.jstun.header.MessageHeader;
import de.javawi.jstun.header.MessageHeaderParsingException;
import de.javawi.jstun.util.UtilityException;

abstract class BindingManager 
{
	
	//public static final String STUN_SERVER = "stun.sipgate.net";
	//public static final int STUN_SERVER_PORT = 10000;
	public static final String STUN_SERVER = "stunserver.org";
	public static final int STUN_SERVER_PORT = 3478;
	
	
	private static final int DEFAULT_DATASOCK_TIMEOUT = 10*1000;
	
	String stunServer;
	int port;
	int timeout = 3000; //ms
	Timer timer;

	// start value for binary search - should be carefully choosen
	int upperBinarySearchLifetime = 1800 * 1000;// 345000; // ms
	int lowerBinarySearchLifetime = 0;
	int binarySearchLifetime = ( upperBinarySearchLifetime + lowerBinarySearchLifetime ) / 2;
	
	// lifetime value
	int lifetime = -1; // -1 means undefined.
	boolean completed = false;
		
	
	private MappedAddress audioRtpReceiver_ma, videoRtpReceiver_ma, commandReceiver_ma;

	private DatagramSocket audioRtpReceiver;
	private DatagramSocket videoRtpReceiver;
	private DatagramSocket commandReceiver;
	
	private boolean portMapOK = false;
	public boolean isPortMapOK()
	{
		return portMapOK;
	}
	
	private boolean cancel = false;
	public void setCancelled(boolean value)
	{
		this.cancel = value;
	}
	//Future: RTCP
	private MappedAddress audioRtcpReceiver, videoRtcpReceiver;
	
	public MappedAddress getAudioRtpReceiver() {
		return audioRtpReceiver_ma;
	}

	public MappedAddress getVideoRtpReceiver() {
		return videoRtpReceiver_ma;
	}

	public MappedAddress getCommandReceiver() {
		return commandReceiver_ma;
	}
	
	public DatagramSocket getAudioRtpReceiverSock() {
		return audioRtpReceiver;
	}

	public DatagramSocket getVideoRtpReceiverSock() {
		return videoRtpReceiver;
	}
	
	public void setAudioRtpReceiverSock(int port) throws SocketException {
		audioRtpReceiver = new DatagramSocket(port);
	}

	public void setVideoRtpReceiverSock() throws SocketException {
		videoRtpReceiver = new DatagramSocket(port);
	}

	public DatagramSocket getCommandReceiverSock() {
		return commandReceiver;
	}
	
	

	
	
	/*private static BindingManager mStunBinder = new BindingManager(STUN_SERVER, STUN_SERVER_PORT);


	public static BindingManager getInstance()
	{
		return mStunBinder;
	}
	*/
	private BindingManager(String stunServer, int port) {
		super();
		this.stunServer = stunServer;
		this.port = port;
		timer = new Timer(true);
	}
	
	public boolean createSockets() throws UtilityException, SocketException, UnknownHostException, IOException, MessageAttributeParsingException, MessageAttributeException, MessageHeaderParsingException {
		
		long startTime = System.currentTimeMillis();
		if (bindingAudioRtpSocket() || 
				bindingVideoRtpSocket() ) 
		{
			this.portMapOK = false;
			return false;
		}
		long totalTime = System.currentTimeMillis() - startTime;
		String timeExeInfo = String.format("It's take %d+%d second to aquire two stun port.", (totalTime/1000),(totalTime%1000));
		Log.d("mbp",timeExeInfo);
		//TODO:
		/*
		 BindingLifetimeTask task = new BindingLifetimeTask();
		timer.schedule(task, binarySearchLifetime);
		Log.d("mbp","Timer scheduled initially: " + binarySearchLifetime + ".");
		*/
		this.portMapOK = true;
		return true; 
	}
	
	
	
	


	/**
	 * @return false  - OK  - first socket is created on server
	 *         true  -  some error 
	 * @throws UtilityException
	 * @throws IOException
	 * @throws MessageHeaderParsingException
	 * @throws MessageAttributeParsingException
	 */
	private boolean bindingAudioRtpSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = DEFAULT_DATASOCK_TIMEOUT;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port);
		initialSocket.send(send);
		Log.d("mbp","Request audio stun port sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		audioRtpReceiver = initialSocket; 

		audioRtpReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			Log.d("mbp","Message header contains an Errorcode message attribute.");
			return true;
		}
		if (audioRtpReceiver_ma == null) {
			Log.d("mbp","Response does not contain a Mapped Address message attribute.");
			return true;
		}
		return false;
	}
	
	private boolean bindingVideoRtpSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = 10*1000;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port+1);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port+1);
		initialSocket.send(send);
		Log.d("mbp","Request video stun port sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		videoRtpReceiver = initialSocket; 

		videoRtpReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			Log.d("mbp","Message header contains an Errorcode message attribute.");
			return true;
		}
		if (videoRtpReceiver_ma == null) {
			Log.d("mbp","Response does not contain a Mapped Address message attribute.");
			return true;
		}
		
		return false;
	}
	
	
	private boolean bindingCommandSocket() throws UtilityException, IOException, MessageHeaderParsingException, MessageAttributeParsingException {
		DatagramSocket initialSocket = null;
		MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
		sendMH.generateTransactionID();
		ChangeRequest changeRequest = new ChangeRequest();
		sendMH.addMessageAttribute(changeRequest);
		byte[] data = sendMH.getBytes();
		
		int _timeout = 10*1000;
		
		MessageHeader receiveMH = new MessageHeader();
		

		initialSocket = new DatagramSocket();
		//initialSocket.connect(InetAddress.getByName(stunServer), port+1);
		initialSocket.setSoTimeout(_timeout);

		DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port+1);
		initialSocket.send(send);
		Log.d("mbp","Request command stun port sent.");



		while (!(receiveMH.equalTransactionID(sendMH))) {
			DatagramPacket receive = new DatagramPacket(new byte[200], 200);
			initialSocket.receive(receive);
			receiveMH = MessageHeader.parseHeader(receive.getData());
			receiveMH.parseAttributes(receive.getData());
		}

		commandReceiver = initialSocket; 

		commandReceiver_ma = (MappedAddress) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.MappedAddress);
		ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
		if (ec != null) {
			Log.d("mbp","Message header contains an Errorcode message attribute.");
			return true;
		}
		if (videoRtpReceiver_ma == null) {
			Log.d("mbp","Response does not contain a Mapped Address message attribute.");
			return true;
		}
		
		return false;
	}
	
	
	
	public int getLifetime() {
		return lifetime;
	}
	
	public boolean isCompleted() {
		return completed;
	}
	
	public void setUpperBinarySearchLifetime(int upperBinarySearchLifetime) {
		this.upperBinarySearchLifetime = upperBinarySearchLifetime;
		binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
	}
	
	class BindingLifetimeTask extends TimerTask {
		
		public BindingLifetimeTask() {
			super();
		}
		
		public void run() {
			Log.d("mbp","Cancel = "+cancel);
			if(!cancel)
			{
				try {
					lifetimeQuery();
				} catch (Exception e) {
					e.printStackTrace();
					Log.d("mbp",e.toString())	;
					Log.d("mbp","Unhandled Exception. BindLifetimeTasks stopped.");
					
				}
			}
			else
			{
				Log.d("mbp","Stun client service is stop, stop keep-alive timer task too.");
			}
		}
		private void keepAliveAudioPort() throws UtilityException, MessageAttributeException, IOException, MessageHeaderParsingException
		{
			try
			{
			//fresh audio port 
			DatagramSocket initSocket = new DatagramSocket();
			DatagramSocket socket = new DatagramSocket();
			//socket.connect(InetAddress.getByName(stunServer), port);
			socket.setSoTimeout(timeout);
		
			MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
			sendMH.generateTransactionID();
			ChangeRequest changeRequest = new ChangeRequest();
			ResponseAddress responseAddress = new ResponseAddress();
			responseAddress.setAddress(audioRtpReceiver_ma.getAddress());
			responseAddress.setPort(audioRtpReceiver_ma.getPort());
		
			sendMH.addMessageAttribute(changeRequest);
			sendMH.addMessageAttribute(responseAddress);
			byte[] data = sendMH.getBytes();
		
			DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port);
			socket.send(send);
			Log.d("mbp","Refresh audio stun port.");
	
			MessageHeader receiveMH = new MessageHeader();
			while (!(receiveMH.equalTransactionID(sendMH))) {
				DatagramPacket receive = new DatagramPacket(new byte[200], 200);
				audioRtpReceiver.receive(receive);
				receiveMH = MessageHeader.parseHeader(receive.getData());
				receiveMH.parseAttributes(receive.getData());
			}
			ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
			if (ec != null) {
				Log.d("mbp","Message header contains errorcode message attribute.");
				return;
			}
			
			Log.d("mbp","Refresh audio port sucessfull. Port is " + audioRtpReceiver_ma.getPort());
			}catch(Exception ex)
			{
				ex.printStackTrace();
				Log.d("mbp","Refresh audio port failed. Retry later.");
			}
		}
		private void keepAliveVideoPort() throws UtilityException, MessageAttributeException, IOException, MessageHeaderParsingException
		{
			//fresh audio port 
			try
			{
				
			DatagramSocket socket = new DatagramSocket();
			//socket.connect(InetAddress.getByName(stunServer), port);
			socket.setSoTimeout(timeout);
		
			MessageHeader sendMH = new MessageHeader(MessageHeader.MessageHeaderType.BindingRequest);
			sendMH.generateTransactionID();
			ChangeRequest changeRequest = new ChangeRequest();
			ResponseAddress responseAddress = new ResponseAddress();
			responseAddress.setAddress(videoRtpReceiver_ma.getAddress());
			responseAddress.setPort(videoRtpReceiver_ma.getPort());
			
			sendMH.addMessageAttribute(changeRequest);
			sendMH.addMessageAttribute(responseAddress);
			byte[] data = sendMH.getBytes();
		
			DatagramPacket send = new DatagramPacket(data, data.length, InetAddress.getByName(stunServer), port + 1);
			socket.send(send);
			Log.d("mbp","Refresh video stun port.");
	
			MessageHeader receiveMH = new MessageHeader();
			while (!(receiveMH.equalTransactionID(sendMH))) 
			{
				DatagramPacket receive = new DatagramPacket(new byte[400], 200);
				videoRtpReceiver.receive(receive);
				receiveMH = MessageHeader.parseHeader(receive.getData());
				receiveMH.parseAttributes(receive.getData());
			}
			ErrorCode ec = (ErrorCode) receiveMH.getMessageAttribute(MessageAttribute.MessageAttributeType.ErrorCode);
			if (ec != null) {
				Log.d("mbp","Message header contains errorcode message attribute.");
				return;
			}
			
			Log.d("mbp","Refresh video port sucessfull. Port is " + videoRtpReceiver_ma.getPort());
			}catch(Exception ex)
			{
				ex.printStackTrace();
				Log.d("mbp","Refresh video port failed. Retry later.");
			}
		}
		public void lifetimeQuery() throws UtilityException, MessageAttributeException, MessageHeaderParsingException, MessageAttributeParsingException, IOException {
			try {
				
				keepAliveAudioPort();
				keepAliveVideoPort();
				
				if (upperBinarySearchLifetime == (lowerBinarySearchLifetime + 1)) {
					Log.d("mbp","BindingManger completed. UDP binding lifetime: " + binarySearchLifetime + ".");
					completed = true;
					return;
				}
				
				lifetime = binarySearchLifetime;
				Log.d("mbp","Lifetime update: " + lifetime + ".");
				lowerBinarySearchLifetime = binarySearchLifetime;
				binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
				if (binarySearchLifetime > 0) {
					BindingLifetimeTask task = new BindingLifetimeTask();
					timer.schedule(task, binarySearchLifetime);
					Log.d("mbp","Timer scheduled: " + binarySearchLifetime + ".");
				} else {
					completed = true;
				}
			} 
			catch (SocketTimeoutException ste) {
				Log.d("mbp","Read operation at query socket timeout.");
				if (upperBinarySearchLifetime == (lowerBinarySearchLifetime + 1)) {
					Log.d("mbp","BindingManger completed. UDP binding lifetime: " + binarySearchLifetime + ".");
					completed = true;
					return;
				}
				upperBinarySearchLifetime = binarySearchLifetime;
				binarySearchLifetime = (upperBinarySearchLifetime + lowerBinarySearchLifetime) / 2;
				
				if (binarySearchLifetime > 0) {
					if (bindingAudioRtpSocket() || bindingVideoRtpSocket()) {
						
						return;
					}
					//Log.d("mbp",String.format("Refresher: audio port %d, video port %d",audioRtcpReceiver.getPort(),videoRtcpReceiver.getPort()));
					if(!cancel)
					{
						BindingLifetimeTask task = new BindingLifetimeTask();
						timer.schedule(task, binarySearchLifetime);
						Log.d("mbp","Timer scheduled: " + binarySearchLifetime + ".");
					}
					else
					{
						Log.d("mbp","Stun client service stop, stop timer task too.");
					}
				} else {
					completed = true;
				}
			}
		}
	}
}


