package com.nxcomm.jstun_android;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.OpenableColumns;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.SendCommandResponse;
import com.nxcomm.testpjnath.Pjnath;

public class RtspStunBridgeService extends Service implements
CreateStunSocketsRunnable.IStunSocketCallBack,
SymmetricNATCheckerTask.SymmetricNATCheckerTaskHandler,
INetworkChangeHandler
{

	private static final String		CMD_STREAM_START							= "CMD:STREAM_START";
	private static final String		CMD_STREAM_STOP								= "CMD:STREAM_STOP";

	private static final String		FWD_ADDRESS									= "127.0.0.1";
	// private static final String FWD_ADDRESS = "192.168.5.102";
	private static final int		VIDEO_PORT									= 13000;
	private static final int		AUDIO_PORT									= 12000;

	public static final String		ACTION_RTSP_BRIDGE_READY					= "com.nxcomm.jstun_android.BRIDGE_READY";
	public static final String		ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED	= "com.nxcomm.jstun_android.CREATE_SESSION_FAILED";
	public static final String		RESOLUTION									= "resolution";
	private static Thread			recv_a, recv_v, rtcp_a, rtcp_v;
	private RtcpSender              mRtcpVideo;
	private RtcpSender              mRtcpAudio;
	private Receiver				mRcvVideo;
	private Receiver				mRcvAudio;

	// private static Sender mCmdSender;
	private WakeLock				keep_cpu_running;
	private String					apiKey, deviceID;
	private SymmetricNATCheckerTask	natCheckerTask;
	public static final String		SESSION_ID_EXTRA							= "RtspStunBridgeService.session_id";
	private StunManager				stunManager;
	private NetworkChangeReceiver	networkChangeReceiver;
	private boolean					shouldIgnoreBroadcast;

	private static final Pjnath		pjnath = new Pjnath();
	private String					server_ip;
	private int 					server_audio_port = -1;
	private int						server_video_port = -1;

	//private static Pjnath pjnath;
	public RtspStunBridgeService()
	{
		super();

	}

	public RtspStunBridgeService(String name)
	{
		super();
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		Log.d("mbp", "RtspStunBridgeService: acquire wakelock...");
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		keep_cpu_running = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"KEEP CPU RUNNING");
		keep_cpu_running.setReferenceCounted(false);
		keep_cpu_running.acquire();
		
		return this.mBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent)
	{
	    // TODO Auto-generated method stub
		//cleanUpForwarder();
		if (keep_cpu_running != null && keep_cpu_running.isHeld())
		{
			Log.d("mbp", "RtspStunBridgeService: release wakelock...");
			keep_cpu_running.release();
			keep_cpu_running = null;
		}
		
	    return super.onUnbind(intent);
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		

		int retries = 5;
		while (retries > 0)
		{
			try {
				pjnath.init();
				doNatCheck();
				break;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				retries--;
				Log.d("mbp", "Init pjnath, retries: " + retries);

				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
				}
			}
		}

		shouldIgnoreBroadcast = true;
		recv_a = null;
		recv_v = null;
		rtcp_a = null; 
		rtcp_v = null;

		// mCmdSender = null;
		// STUN CLIENT SERVICE
		// Start up the thread running the service. Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block. We also make it
		// background priority so CPU-intensive work will not disrupt our UI.

		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		// Get the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		// when create service, get 2 stun port for App


		networkChangeReceiver = new NetworkChangeReceiver(this);
		IntentFilter intentFilter = new IntentFilter();
		//intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
		intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		//intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(networkChangeReceiver, intentFilter);

	}
	
	
	public boolean isStunPortValid()
	{
		boolean isValid = false;
		if (pjnath != null && (pjnath.getAudioPort() < 0 || pjnath.getAudioPort() > 65535 ||
				pjnath.getVideoPort() < 0 || pjnath.getVideoPort() > 65535) )
		{
			isValid = false;
		}
		else
		{
			isValid = true;
		}
		
		return isValid;
	}
	

	public void onNetworkChangeCompleted()
	{		
		if (pjnath != null && !pjnath.isRunning())
		{
			Log.d("mbp", "Clean up pjnath...");
			pjnath.cleanUp();

			int retries = 5;
			while (retries > 0)
			{
				try {
					pjnath.init();
					doNatCheck();
					break;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					retries--;

					try {
						Thread.sleep(3000);
					} catch (InterruptedException e1) {
					}
				}
				Log.d("mbp", "Init pjnath, retries: " + retries);
			} //while (retries > 0)

		} //if (pjnath != null)

	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		//cleanUpForwarder();

		if (pjnath != null)
		{
			Log.d("mbp", "Clean up pjnath...");
			pjnath.cleanUp();
		}

		unregisterReceiver(networkChangeReceiver);
	}

	private void doNatCheck()
	{
		if (pjnath.isRunning() == false)
		{
			pjnath.check_nat_type();
		}
	}

	public boolean isSymmetricNAT()
	{
		return pjnath.is_in_symmetric_nat();
	}

	private boolean open_rtsp_stun()
	{
		boolean result = false;
		
		if (isStunPortValid())
		{
			int retries = 3;
			do {
				try {
					String cmd = String.format(PublicDefine.OPEN_P2P_RTSP_STUN,
							pjnath.getAudioPort(), pjnath.getVideoPort(), pjnath.getPublicIP());
					SendCommandResponse cmdResponse = Device.sendCommand(apiKey,
							deviceID, cmd);

					Gson gson = new Gson();
					Log.d("mbp", "Send command via stun " + cmd
							+ "\n Cmd Response " + gson.toJson(cmdResponse));

					if (cmdResponse == null) {
						try {
							Thread.sleep(1000);
							continue;
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (cmdResponse.getStatus() == 200) 
					{
						String response = cmdResponse.getSendCommandResponseData()
								.getDevice_response().getBody();
						// Command is correct and we have result.
						if (response != null) {
							// Camera return NA mean not availabe.
							if (response.equalsIgnoreCase("NA")) {
								Log.d("mbp",
										"body of p2p_rtsp_stun command result is \"NA\" retry after 1s. ");
								try {
									Thread.sleep(1000);
									continue;
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							// Camera is available and have the result
							else {
								// format: get_session_key:
								// error=200,port1=37942&port2=39487&ip=115.77.252.118,mode=p2p_stun_rtsp
								// try to get mode
								Log.d("mbp", "OPEN_P2P_RTSP_STUN response: " + response);
								response = response + ".";
								int startIdx = response.indexOf("error=") + 6;
								int endIdx = startIdx + 3;
								String error = null;
								try {
									error = response.substring(startIdx, endIdx);
								} catch (Exception e1) {
									e1.printStackTrace();
								}

								if (error != null)
								{
									if (error.equalsIgnoreCase("200"))
									{
										String mode = getValueFromCmdString(response,
												"mode", ".");
										// if response don't content "mode"
										if (mode == null) {
											Log.d("mbp",
													"From CreateStunSocketRunnable: INVALID result from camera when create p2p_rtsp_stun session.");
											try {
												Thread.sleep(1000);
												continue;
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
										// if response have "mode" and "mode" =
										// p2p_rtsp_stun
										else if (mode.equalsIgnoreCase("p2p_stun_rtsp")) {
											try {
												server_ip = getValueFromCmdString(
														response, "ip", ",");
												server_ip = getValueFromCmdString(
														response, "ip", ",");
												//server_video_ip = "115.77.250.193";
												server_audio_port = Integer
														.parseInt(getValueFromCmdString(
																response, "port1", "&"));

												server_video_port = Integer
														.parseInt(getValueFromCmdString(
																response, "port2", "&"));

												//server_video_port = 55477;

												Log.d("FFMpegFileExplorer", "server A ip: "
														+ server_ip + ":"
														+ server_audio_port);
												Log.d("FFMpegFileExplorer", "server V ip: "
														+ server_ip + ":"
														+ server_video_port);

												if (server_ip == null) {
													Log.d("FFMpegFileExplorer",
															"Failed to get server info");
												} else {
													Log.d("FFMpegFileExplorer",
															"Got to get server info >>>> Send probe");
													result = true;
													pjnath.sendAudioProbe(server_ip, server_audio_port);
													pjnath.sendVideoProbe(server_ip, server_video_port);
													break;
												}

											}

											catch (Exception ex) {
												Log.d("mpb",
														"Parse result error when create rtsp stun connection.");
											}
										}

										else if (mode.equalsIgnoreCase("replay_rtmp")) {
											// TODO: mode REPLAY_RTMP
										} else {
											// Other unknown mode???
											Log.d("mbp", "Mode is " + mode);
										}
									} //if (error.equalsIgnoreCase("200"))
									else
									{
										break;
									}
								} //if (error != null)
							}
						} else {
							Log.d("mbp", "P2P RTSP session create failed");
						}

					}
					else
					{
						Log.d("mbp",
								"Cannot send command to get camera ip & av port via server.");
					}
					// Log.d("FFMpegFileExplorer", "server C ip: " + server_cmd_ip+
					// ":" + server_cmd_port);
				} catch (SocketTimeoutException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
				}

			} while (retries-- > 0);

		} //if (isStunPortValid())
		
		return result;
	}

	private String getValueFromCmdString(String cmdString, String startChar,
			String endChar) 
	{
		String value = null;
		if (cmdString.indexOf(startChar) > 0) {
			int start = cmdString.indexOf(startChar) + startChar.length() + 1;
			int end = cmdString.indexOf(endChar, start);
			value = cmdString.substring(start, end);
		}
		return value;

	}

	public StunManager getStunManager()
	{
		return stunManager;
	}

	/**
	 * 
	 * Stop all children thread and also release any wakelock if being held
	 * before
	 */
	private void cleanUpForwarder()
	{
		if (pjnath != null)
		{
			pjnath.destroyStunForwarder();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{

		/*
		 * Ac
		 */
//		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		keep_cpu_running = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//				"KEEP CPU RUNNING");
//		keep_cpu_running.acquire();

		return START_STICKY;
	}

	private boolean tryToLaunchPlayer(boolean connectViaStun)
	{
		Intent intent;
		if (connectViaStun == true)
		{
			String result = this.queryHQStatus();
			if (result != null && !result.isEmpty())
			{
				intent = new Intent(ACTION_RTSP_BRIDGE_READY);
				intent.putExtra(RESOLUTION, result);
			}
			else
			{
				cleanUpForwarder();
				deviceID = null;
				apiKey = null;
				intent = new Intent(ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED);
			}
		}
		else
		{
			cleanUpForwarder();
			deviceID = null;
			apiKey = null;
			intent = new Intent(ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED);
		}
		
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(intent);

		return true;
	}

	private String queryHQStatus()
	{

		String resolution = null;
		String command = PublicDefine.BM_HTTP_CMD_PART
				+ PublicDefine.GET_RESOLUTION_CMD;

		try
		{
			SendCommandResponse resp = Device.sendCommand(apiKey, deviceID,
					command);

			// if not 200 switch back..
			if (resp != null && resp.getStatus() == 200)
			{
				// get_resolution: [720p,480p]

				String camera_response = resp.getSendCommandResponseData()
						.getDevice_response().getBody();
				Log.d("mbp", "Get resolution from camera response "
						+ PublicDefine.gson.toJson(camera_response));
				if (camera_response.startsWith(PublicDefine.GET_RESOLUTION_CMD))
				{
					resolution = camera_response
							.substring(PublicDefine.GET_RESOLUTION_CMD.length() + 2);
					if (resolution.equalsIgnoreCase("-1"))
					{
						resolution = null;
					}
				}
				else
				{
					resolution = null;
					Log.e("mbp", "INVALID get_resolution response: "
							+ camera_response);
				}

			}

		}
		catch (SocketTimeoutException e)
		{
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		// return result;
		return resolution;

	}

	//NOT USED
	public void updateAudioSock(DatagramSocket rcvFrom)
	{

		SocketAddress playerAddress = new InetSocketAddress(FWD_ADDRESS,
				AUDIO_PORT);
		SocketAddress playerRtcpAddress = new InetSocketAddress(
				AUDIO_PORT+1);
		// set up receiver here..
		try
		{
			mRcvAudio = new Receiver(rcvFrom, playerAddress);
			(recv_a = new Thread(mRcvAudio, "Receive Audio")).start();

			mRtcpAudio = new RtcpSender(rcvFrom, playerRtcpAddress);
			(rtcp_a = new Thread(mRcvAudio, "Rtcp fwder")).start(); 

		}
		catch (SocketException e)
		{
			e.printStackTrace();
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}


	}


	//NOT USED
	public void updateVideoSock(DatagramSocket rcvFrom)
	{

		SocketAddress playerAddress = new InetSocketAddress(FWD_ADDRESS,
				VIDEO_PORT);
		SocketAddress playerRtcpAddress = new InetSocketAddress(
				VIDEO_PORT+1);
		try
		{
			mRcvVideo = new Receiver(rcvFrom, playerAddress);
			mRcvVideo.isVideo = true;

			(recv_v = new Thread(mRcvVideo, "Receive Video")).start();

			mRtcpVideo= new RtcpSender(rcvFrom, playerRtcpAddress);
			(rtcp_v = new Thread(mRcvVideo, "Rtcp fwder")).start(); 


		}
		catch (SocketException e)
		{
			e.printStackTrace();
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}


	}


	/**
	 * probe packet for command ... This should not be done this way though
	 * There should be another channel to pass commands
	 * 
	 * @param packet
	 * @return command if found, Null otherwise
	 */
	private String extractCommand(byte[] packet)
	{
		String str_ = null;
		try
		{
			str_ = new String(packet, 0, CMD_STREAM_START.length(), "UTF8");

			if (str_ != null && CMD_STREAM_START.equals(str_))
			{
				System.out.println("Found Stream start");
				return str_;
			}

			str_ = new String(packet, 0, CMD_STREAM_STOP.length(), "UTF8");
			if (str_ != null && CMD_STREAM_STOP.equals(str_))
			{
				System.out.println("Found Stream stop");
				return str_;
			}

			str_ = null;

		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		return str_;
	}

	private class RtcpSender implements Runnable
	{
		private DatagramSocket	stunSock;

		private DatagramSocket	localSock;
		private SocketAddress	playerRtcpAddress;
		private boolean			running;
		public boolean			isVideo	= false;

		public RtcpSender(DatagramSocket stunSock, SocketAddress receiveFrom)
				throws SocketException, UnknownHostException
				{
			this.stunSock = stunSock;
			localSock = new DatagramSocket(receiveFrom);
			this.playerRtcpAddress = receiveFrom;


				}

		public void stop()
		{
			running = false;
		}
		public void run()
		{

			running = true;
			byte[] buf;
			int buf_len;
			buf = new byte[16 * 1024];
			while (running)
			{
				//Listen on Rtcp port
				DatagramPacket recv_packet = new DatagramPacket(buf,
						buf.length);
				try {
					localSock.setSoTimeout(1000);
					localSock.receive(recv_packet);

					buf = recv_packet.getData();
					buf_len = recv_packet.getLength();

					if (buf_len > 0)
					{
						//FWD this rtcp packet into the stun channel 

						DatagramPacket fwd_packet = new DatagramPacket(buf,
								buf_len,stunSock.getRemoteSocketAddress());

						// FWD the packet
						stunSock.send(fwd_packet);
					}


				} 
				catch (SocketException e)
				{
					e.printStackTrace();
				} 
				catch (SocketTimeoutException ste)
				{
					//could be read timeout
					ste.printStackTrace();
					continue; 
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}



			}
		}

	}
	private class Receiver implements Runnable
	{
		private DatagramSocket	ma;

		private DatagramSocket	localSock;
		private SocketAddress	playerAddress;
		private boolean			running;
		public boolean			isVideo	= false;

		public Receiver(DatagramSocket mine, SocketAddress playerAddress)
				throws SocketException, UnknownHostException
				{
			this.ma = mine;
			localSock = new DatagramSocket();
			this.playerAddress = playerAddress;
				}

		public void stop()
		{
			running = false;
		}

		public void run()
		{

			running = true;
			byte[] buf;
			int buf_len;
			buf = new byte[16 * 1024];
			while (running)
			{


				// /Test connection
				try
				{

					DatagramPacket recv_packet = new DatagramPacket(buf,
							buf.length);
					ma.setSoTimeout(0);
					ma.receive(recv_packet);

					buf = recv_packet.getData();
					buf_len = recv_packet.getLength();

					String text = extractCommand(buf);

					if (text == null)
					{

						DatagramPacket fwd_packet = new DatagramPacket(buf,
								buf_len, playerAddress);

						// FWD the packet
						localSock.send(fwd_packet);

					}

				}
				catch (SocketTimeoutException se)
				{
					Log.d("FFMpegFileExplorer", "Timeout !");
					se.printStackTrace();
					/*
					 * if (running && mCmdSender != null) {
					 * Log.d("FFMpegFileExplorer", "--->Send start");
					 * mCmdSender.sendCommand(CMD_STREAM_START);
					 * 
					 * }
					 */

				}
				catch (SocketException e)
				{
					e.printStackTrace();
				}
				catch (UnknownHostException e)
				{
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}

			Log.d("FFMpegFileExplorer", "Fwd Thread Exit !");

		}

	}

	// ---------------- STUN CLIENT SERVICE MERGE
	// HERE------------------------------
	private Looper			mServiceLooper;
	private ServiceHandler	mServiceHandler;

	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler
	{
		public ServiceHandler(Looper looper)
		{
			super(looper);
		}

		@Override
		public void handleMessage(Message msg)
		{
		}
	}

	private final IBinder	mBinder	= new RtspStunBridgeServiceBinder();

	/**
	 * Class used for the client Binder. Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with IPC.
	 */
	public class RtspStunBridgeServiceBinder extends Binder
	{
		public RtspStunBridgeService getService()
		{
			// Return this instance of LocalService so clients can call public
			// methods
			return RtspStunBridgeService.this;
		}
	}

	/**
	 * Start Stun view Session -
	 * 
	 *  This is non-blocking call, another thread is spawn to do the setup with pjnath. 
	 *  At the end of the procedure, either 
	 *  
			intent = new Intent(ACTION_RTSP_BRIDGE_READY) //success 
			OR 
			intent = new Intent(ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED) // failure
			
	    Will be broadcasted
		
	 * 
	 * apiKey & deviceID is used to send stun request to camera 
	 * 
	 * @param apiKey
	 * @param deviceID
	 */
	public void initViewSession(String apiKey, String deviceID)
	{

//		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		if (keep_cpu_running!= null && keep_cpu_running.isHeld())
//		{
//
//		}
//		else
//		{
//
//			Log.d("mbp",
//					"Get the wakelock >>>>> ");
//
//			keep_cpu_running = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//					"KEEP CPU RUNNING");
//			keep_cpu_running.acquire();
//		}

		this.apiKey = apiKey;
		this.deviceID = deviceID;

		new Thread(new Runnable() {

			@Override
			public void run() {
				pjnath.createStunForwarder(); 

				Log.d("mbp","w " + pjnath.getPublicIP() + 
						" "+ pjnath.getAudioPort()+
						" "+pjnath.getVideoPort());

				boolean connectViaStun = open_rtsp_stun();
				if (connectViaStun == false)
				{
					Log.d("mbp", "open_rtsp_stun failed...destroy stun forwarder");
					pjnath.destroyStunForwarder();
					tryToLaunchPlayer(connectViaStun);
					return; 
				}


				try
				{

					for (int i = 0 ; i <2; i++)
					{
						if (pjnath== null)
						{
							break; 

						}

						pjnath.sendAudioProbe(server_ip, server_audio_port);
						Thread.sleep(300); 
						pjnath.sendVideoProbe(server_ip, server_video_port);
						Thread.sleep(300); 

					}
				}
				catch (InterruptedException ie)
				{

				}
				tryToLaunchPlayer(connectViaStun);

			}
		}).start();

	}



	
	/**
	 * Call this in Background thread only 
	 * @param apiKey
	 * @param deviceID
	 */
	public void closeViewSessionBlocked()
	{
		cleanUpForwarder();
		try
		{
			String cmd = String.format(PublicDefine.CLOSE_P2P_RTSP_STUN,
					pjnath.getAudioPort(), pjnath.getVideoPort(), pjnath.getPublicIP());
			if (Device.sendCommand(apiKey, deviceID, cmd).getStatus() == 200)
			{
				Log.d("mbp", "Close p2p rtsp stun succeeded.");
				this.deviceID = null;
				this.apiKey = null;
			}
			else
			{
				Log.d("mbp", "Close p2p rtsp stun failed.");
			}
		}
		catch (SocketTimeoutException e)
		{
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
 
	}
	/**
	 * Close the current Viewing session 
	 * 
	 */
	public void closeViewSession()
	{
		new Thread(new Runnable() {

			@Override
			public void run() 
			{
				closeViewSessionBlocked();
			}
		}).start();

	}

	
	public boolean isBusy()
	{
		if ( this.deviceID != null &&  this.apiKey != null)
		{
			return true; 
		}
		
		return false; 
	}
	
	
	@Override
	public void onError(String msg)
	{
		Log.d("mbp", "SymmetricNATChecker task execute failed because " + msg);
		stunManager = null;
	}

	@Override
	public void onCreateSessionFailed(int error)
	{

		Intent intent = new Intent(ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED);
		intent.putExtra("error",error); 
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
				.getInstance(this);
		localBroadcastManager.sendBroadcast(intent);

	}

	@Override
	public void onCheckCompleted(StunManager result)
	{
		this.stunManager = result;
	}

	public boolean isDiscoveryCompleted()
	{
		// return true;
		return (pjnath != null && !pjnath.isRunning());
	}

}
