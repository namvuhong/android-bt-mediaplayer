package com.nxcomm.jstun_android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.util.Log;

public class NetworkChangeReceiver extends BroadcastReceiver
{
	private INetworkChangeHandler actionHandler;
	public NetworkChangeReceiver(INetworkChangeHandler iah)
	{
		this.actionHandler = iah;
	}
	@Override
	public void onReceive(Context context, Intent intent)
	{
		final String action = intent.getAction();
		
		//if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION))
		if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION))
		{
			
			NetworkInfo nw = (NetworkInfo) intent
					.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
			
//			ConnectivityManager cm1 = 
//					(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//			NetworkInfo wifiInfo = cm1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//			Log.d("mbp", "WifiManager wifi state: " + nw.getState());
//			Log.d("mbp", "ConnectivityManager wifi state: " + wifiInfo.getState());
			if (nw.isConnected())
			{
				Log.d("mbp", "Wi-Fi network change and connected to internet");
				Log.d("mbp", "WifiManager wifi state: " + nw.getState());
				//need to refresh Stun
				if(this.actionHandler != null)
				{
					this.actionHandler.onNetworkChangeCompleted();
				}
			}
			//else if (nw.getState() == NetworkInfo.State.DISCONNECTED)
			else
			{
				Log.d("mbp","Wi-Fi network lost or connecting.");
				ConnectivityManager cm = 
						(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
				if (netInfo != null && netInfo.isConnected())
				{
					Log.d("mbp", "ConnectivityManager mobile state: " + netInfo.getState());
					if(this.actionHandler != null)
					{
						this.actionHandler.onNetworkChangeCompleted();
					}
				}
			}

		} //if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION))

	}
}