package com.nxcomm.jstun_android;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.nxcomm.testpjnath.Pjnath;

import de.javawi.jstun.test.DiscoveryInfo;

public class SymmetricNATCheckerTask extends
		AsyncTask<Void, Void, StunManager> {
	public interface SymmetricNATCheckerTaskHandler 
	{
		public void onCheckCompleted(StunManager result);
		public void onError(String msg);
	}
	private String errorMsg;
	private SymmetricNATCheckerTaskHandler handler;
	public SymmetricNATCheckerTask(Context c)
	{
		this.handler = (SymmetricNATCheckerTaskHandler) c;
	}
	
	@Override
	protected StunManager doInBackground(Void... params) {
		// TODO Auto-generated method stub
		StunManager stunManager = new StunManager();
		
		Log.d("mbp", "Start test NAT type using PJNATH-ANDROID"); 
		Pjnath pjnath = new Pjnath(); 
		
		int isSymmetric;
		isSymmetric = pjnath.check_nat_type();


		if (isSymmetric == 1)
		{
			stunManager.setSymmetricNet(true);
		}
		else
		{
			stunManager.setSymmetricNet(false);
		}

		try 
		{

			stunManager.setAudioRtpReceiverSock(pjnath.getAudioPort());
			stunManager.setVideoRtpReceiverSock(pjnath.getVideoPort());
			stunManager.setPublicIP(pjnath.getPublicIP());	

			Gson gson = new Gson();
			Log.d("mbp","STUNMANAGER dump: "+gson.toJson(stunManager));
			Log.d("mbp","Public IP is "+stunManager.getPublicIP()+". Audio port:"+ pjnath.getAudioPort()+" and video port: "+pjnath.getVideoPort());
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorMsg = e.getLocalizedMessage();
			stunManager = null;
			pjnath = null;
		}
		Log.d("mbp", "End test NAT type using PJNATH-ANDROID");
		
		return stunManager; 
		
		
	}

	@Override
	protected void onCancelled() {
		// TODO Auto-generated method stub
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(StunManager stunManager) {
		// TODO Auto-generated method stub

		if (handler != null) 
		{
			if(stunManager != null)
			{
				handler.onCheckCompleted(stunManager);
			}
			else
			{
				handler.onError(this.errorMsg);
			}
		}
		super.onPostExecute(stunManager);
	}

}
