package com.nxcomm.jstun_android;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import android.util.Log;

import com.google.gson.Gson;
import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.SendCommandResponse;

import de.javawi.jstun.attribute.MappedAddress;
import de.javawi.jstun.attribute.MessageAttributeException;
import de.javawi.jstun.attribute.MessageAttributeParsingException;
import de.javawi.jstun.header.MessageHeaderParsingException;
import de.javawi.jstun.util.UtilityException;

public class CreateStunSocketsRunnable implements Runnable {

	private IStunSocketCallBack inf;

	private DatagramSocket sockAudio, sockVideo;// sockCmd;
	// private InetSocketAddress serverCmdSock;
	private String apiKey, deviceID;
	/* server session info -- TODO wrap it */
	private String server_audio_ip, server_video_ip;// , server_cmd_ip;
	private int server_audio_port, server_video_port;// , server_cmd_port;
	private StunManager stunManager;
	//private BindingManager stunManager;

	/**
	 * @param mainActivity
	 * 
	 *            CreateStunSocketsRunnable(int session_id, IStunSocketCallBack
	 *            m) { this.inf = m; sessionId = session_id; }
	 */
	CreateStunSocketsRunnable(IStunSocketCallBack m, String apiKey,
			String deviceID, StunManager stunManager) {
		this.inf = m;
		this.apiKey = apiKey;
		this.deviceID = deviceID;
		this.stunManager = stunManager;
	}

	public void run() {

		
		Log.d("mbp", "Use AUDIO port  "
				+ stunManager.getAudioRtpReceiverSock().getLocalPort()
				+ " and VIDEO port "
				+ stunManager.getVideoRtpReceiverSock().getLocalPort());
		
		sockAudio = stunManager.getAudioRtpReceiverSock();
		sockVideo = stunManager.getVideoRtpReceiverSock();
		
		
		int status = bindToServerAddresses();
		
		if (status == 0)
		{
			this.inf.updateAudioSock(sockAudio);
			this.inf.updateVideoSock(sockVideo);
		} 
		else
		{
			Log.d("mbp", "Bind to camera server failed.");
			// bind to server address failed, should notify for initViewSession
			this.inf.onCreateSessionFailed(status);
		}

	}

	/*  return error status 
	 *     0 - OK 
	 *     != 0 - some error 
     */
	private int bindToServerAddresses() {
		// GET SERVER IP
		int result = 0;
		int retries = 3;
		do {
			try {
				String cmd = String.format(PublicDefine.OPEN_P2P_RTSP_STUN,
						sockAudio.getLocalPort(), sockVideo.getLocalPort(),stunManager.getPublicIP());
				SendCommandResponse cmdResponse = Device.sendCommand(apiKey,
						deviceID, cmd);

				Gson gson = new Gson();
				Log.d("mbp", "Send command via stun " + cmd
						+ "\n Cmd Response " + gson.toJson(cmdResponse));

				if (cmdResponse == null) {
					try {
						Thread.sleep(1000);
						continue;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (cmdResponse.getStatus() == 200) {
					String response = cmdResponse.getSendCommandResponseData()
							.getDevice_response().getBody();
					// Command is correct and we have result.
					if (response != null) {
						// Camera return NA mean not availabe.
						if (response.equalsIgnoreCase("NA")) {
							result = -1 ;
							Log.d("mbp",
									"body of p2p_rtsp_stun command result is \"NA\" retry after 1s. ");
							try {
								Thread.sleep(1000);
								continue;
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						// Camera is available and have the result
						else {
							// format: get_session_key:
							// error=200,port1=37942&port2=39487&ip=115.77.252.118,mode=p2p_stun_rtsp
							// try to get mode
							response = response + ".";
							String mode = getValueFromCmdString(response,
									"mode", ".");
							// if response don't content "mode"
							if (mode == null) {
								result = -2 ;
								Log.d("mbp",
										"From CreateStunSocketRunnable: INVALID result from camera when create p2p_rtsp_stun session.");
								try {
									Thread.sleep(1000);
									continue;
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							// if response have "mode" and "mode" =
							// p2p_rtsp_stun
							else if (mode.equalsIgnoreCase("p2p_stun_rtsp")) {
								try {
									server_audio_ip = getValueFromCmdString(
											response, "ip", ",");
									server_video_ip = getValueFromCmdString(
									response, "ip", ",");
									//server_video_ip = "115.77.250.193";
									server_audio_port = Integer
											.parseInt(getValueFromCmdString(
													response, "port1", "&"));
									
									  server_video_port = Integer
									 .parseInt(getValueFromCmdString(
									  response, "port2", "&"));
									 
									//server_video_port = 55477;

									Log.d("FFMpegFileExplorer", "server A ip: "
											+ server_audio_ip + ":"
											+ server_audio_port);
									Log.d("FFMpegFileExplorer", "server V ip: "
											+ server_video_ip + ":"
											+ server_video_port);

									if (server_video_ip == null
											|| server_audio_ip == null) {
										Log.d("FFMpegFileExplorer",
												"Failed to get server info");
									} else {
										Log.d("FFMpegFileExplorer",
												"Got to get server info >>>>");
										result = 0;
										break;
									}

									try {
										Thread.sleep(3000);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}

								catch (Exception ex) {
									Log.d("mpb",
											"Parse result error when create rtsp stun connection.");
									result = -3 ;
								}
							}

							else if (mode.equalsIgnoreCase("replay_rtmp")) {
								// TODO: mode REPLAY_RTMP
							} else {
								// Other unknown mode???
								Log.d("mbp", "Mode is " + mode);
							}
						}
					} else {
						Log.d("mbp", "P2P RTSP session create failed");
						result = -4;
					}

				} else {
					Log.d("mbp",
							"Cannot send command to get camera ip & av port via server.");
					result = -5;
				}
				// Log.d("FFMpegFileExplorer", "server C ip: " + server_cmd_ip+
				// ":" + server_cmd_port);
			} catch (SocketTimeoutException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				
				result = -6; 
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				
				result = -6;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				result = -6;
			}

		} while (retries-- > 0);

		
		/* Only do if we can reach with success */ 
		if (result == 0 )
		{
			sendProbes();
		}

		return result;

	}

	/* May be called periodically to keep ports open */
	private void sendProbes() {

		
		byte[] buf;
		try {
			// Start Sending probe to open ports
			buf = "CMD:KICK_START".getBytes("UTF8");

			DatagramPacket audioCrap = new DatagramPacket(buf, buf.length,
					InetAddress.getByName(server_audio_ip), server_audio_port);
			
			DatagramPacket videoCrap = new DatagramPacket(buf, buf.length,
					InetAddress.getByName(server_video_ip), server_video_port);
			
		
			
			// just send an dont expect response -open port
			sockVideo.send(videoCrap);
			Thread.sleep(300);
			sockAudio.send(audioCrap);
			Thread.sleep(300);
			sockVideo.send(videoCrap);
			Thread.sleep(300);
			sockAudio.send(audioCrap);
			Thread.sleep(300);
			sockVideo.send(videoCrap);
			Thread.sleep(300);
			sockAudio.send(audioCrap);
			Thread.sleep(300);
		
			Log.d("mbp","Send probes to video port "+server_video_ip+":"+server_video_port);
			Log.d("mbp","Send probes to audio port "+server_audio_ip+":"+server_audio_port);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Log.d("FFMpegFileExplorer", "Audio & Video Probe ---> sent");
	}

	private String getValueFromCmdString(String cmdString, String startChar,
			String endChar) {
		String value = null;
		if (cmdString.indexOf(startChar) > 0) {
			int start = cmdString.indexOf(startChar) + startChar.length() + 1;
			int end = cmdString.indexOf(endChar, start);
			value = cmdString.substring(start, end);
		}
		return value;

	}

	interface IStunSocketCallBack {
		void updateAudioSock(DatagramSocket rcvFrom);

		void updateVideoSock(DatagramSocket rcvFrom);

		void onCreateSessionFailed(int error);
	}
}
