package com.nxcomm.blinkhd.ui;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blinkhd.R;
import com.cvision.eventtimeliner.EventManagerFragment;

public class SavedDayChildFragment extends Fragment
{
	Fragment		frg				= null;
	private boolean	isShowTimeLine	= false;
	private View	myView;
	LinearLayout	rootLayout;
	RelativeLayout	timeLineHolder;
	private int		id				= 20;
	
	public SavedDayChildFragment()
	{
		frg = new EventManagerFragment();
		// frg = new AccountSettingFragment();
	}
	
	public void setID(int id)
	{
		this.id = id;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		ImageView img = (ImageView) myView
				.findViewById(R.id.fragment_saved_child_image_view_CameraImage);
		
		rootLayout = (LinearLayout) myView
				.findViewById(R.id.saved_day_child_fragment_root);
		/*
		 * RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
		 * RelativeLayout.LayoutParams.MATCH_PARENT,
		 * RelativeLayout.LayoutParams.WRAP_CONTENT);
		 * 
		 * timeLineHolder = new
		 * RelativeLayout(this.getActivity().getBaseContext());
		 * rootLayout.addView(timeLineHolder, lp); timeLineHolder.setId(id);
		 */
		
		img.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("mbp", " YOU CLICKED ON IMAGE VIEW...");
				Fragment tmp = getActivity().getFragmentManager()
						.findFragmentByTag(null);
				
				FragmentTransaction trans = getActivity().getFragmentManager()
						.beginTransaction();
				while (tmp != null)
				{
					Log.d("mbp","Remove fragment");
					trans.remove(tmp);
					tmp = getActivity().getFragmentManager().findFragmentByTag(
							null);
				}
				//trans.commit();
				
				trans.replace(R.id.saved_day_fragment_container, frg);
				//trans.addToBackStack(null);
				trans.commit();
				
				/*
				 * if (isShowTimeLine) {
				 * 
				 * if (timeLineHolder != null) {
				 * timeLineHolder.setVisibility(View.GONE); } isShowTimeLine =
				 * false; } else { addTimeLineFragment();
				 * 
				 * if (timeLineHolder != null) {
				 * timeLineHolder.setVisibility(View.VISIBLE); } isShowTimeLine
				 * = true; }
				 */
				
			}
			
		});
	}
	
	private void addTimeLineFragment()
	{
		Log.d("mbp", "Timeline holder id " + this.timeLineHolder.getId());
		
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) timeLineHolder
				.getLayoutParams();
		params.height = 500;
		timeLineHolder.setLayoutParams(params);
		FragmentTransaction trans = getFragmentManager().beginTransaction();
		trans.replace(this.timeLineHolder.getId(), frg);
		trans.commit();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		myView = inflater.inflate(R.layout.list_row_saved_day, container,
				false);
		return myView;
	}
}
