package com.nxcomm.blinkhd.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class EnableDisconnectAlertDialog extends CommonDialog
{
	private RadioButton	yesRadio;
	private RadioButton	noRadio;

	private boolean	    disconnectAlertEnabled	= false;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout

		contentView = inflater.inflate(R.layout.dialog_stun_connection_setting,
		        null);

		builder.setView(contentView);

		// build type face
		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Light.otf");

		TextView titleView = (TextView) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_title);
		TextView desc = (TextView) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_description);
		if (desc != null)
		{
			desc.setText(R.string.enable_disconnect_alert);
		}
		TextView yesTextView = (TextView) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_Yes_TextView);
		TextView noTextView = (TextView) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_No_TextView);

		yesRadio = (RadioButton) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_Yes_Choice);
		noRadio = (RadioButton) contentView
		        .findViewById(R.id.dialog_stun_connection_setting_No_Choice);

		if (disconnectAlertEnabled)
		{
			yesRadio.setChecked(true);
			noRadio.setChecked(false);
		}
		else
		{
			yesRadio.setChecked(false);
			noRadio.setChecked(true);
		}

		View.OnClickListener radioOnClickListener = new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				RadioButton which = (RadioButton) v;
				if (which == yesRadio)
				{
					yesRadio.setChecked(true);
					noRadio.setChecked(false);
				}
				else
				{
					yesRadio.setChecked(false);
					noRadio.setChecked(true);
				}

			}

		};
		yesRadio.setOnClickListener(radioOnClickListener);
		noRadio.setOnClickListener(radioOnClickListener);

		titleView.setTypeface(sboldTf);
		desc.setTypeface(sboldTf);
		yesTextView.setTypeface(sboldTf);
		noTextView.setTypeface(sboldTf);

		builder.setPositiveButton(R.string.OK,
		        new DialogInterface.OnClickListener()
		        {
			        @Override
			        public void onClick(DialogInterface dialog, int id)
			        {
				        if (commonDialogListener != null)
				        {
					        commonDialogListener
					                .onDialogPositiveClick(EnableDisconnectAlertDialog.this);
				        }

			        }
		        }).setNegativeButton(R.string.Cancel,
		        new DialogInterface.OnClickListener()
		        {
			        public void onClick(DialogInterface dialog, int id)
			        {
			        	EnableDisconnectAlertDialog.this.getDialog().cancel();
			        }
		        });
		return builder.create();
	}

	public boolean isEnable()
	{
		boolean result = false;

		if (yesRadio.isChecked())
		{
			result = true;
		}

		return result;
	}

	public void setEnable(boolean isEnable)
	{
		this.disconnectAlertEnabled = isEnable;
		if (yesRadio != null && noRadio != null)
		{
			if (isEnable)
			{
				yesRadio.setChecked(true);
				noRadio.setChecked(false);
			}
			else
			{
				yesRadio.setChecked(false);
				noRadio.setChecked(true);
			}
		}
	}
}
