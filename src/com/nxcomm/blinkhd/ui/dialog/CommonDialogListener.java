package com.nxcomm.blinkhd.ui.dialog;

import android.app.DialogFragment;

public interface CommonDialogListener
{
	public void onDialogPositiveClick(DialogFragment dialog);

	public void onDialogNegativeClick(DialogFragment dialog);
}
