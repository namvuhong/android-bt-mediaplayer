package com.nxcomm.blinkhd.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class ChangeCameraNameDialog extends CommonDialog
{
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout

		contentView = inflater
		        .inflate(R.layout.dialog_change_camera_name, null);

		builder.setView(contentView);

		// build type face
		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getActivity().getAssets(),
		        //"fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;//Typeface.createFromAsset(getActivity().getAssets(),
		        //"fonts/ProximaNova-Light.otf");

		TextView title = (TextView) contentView
		        .findViewById(R.id.dialog_change_camera_name_title);
		TextView desc = (TextView) contentView
		        .findViewById(R.id.dialog_change_camera_name_description);
		EditText name = (EditText) contentView
		        .findViewById(R.id.dialog_change_camera_name_CameraName_EditText);

		title.setTypeface(sboldTf);
		desc.setTypeface(lightTf);
		name.setTypeface(lightTf);

		builder.setPositiveButton(R.string.OK,
		        new DialogInterface.OnClickListener()
		        {
			        @Override
			        public void onClick(DialogInterface dialog, int id)
			        {
				        if (commonDialogListener != null)
				        {
					        commonDialogListener
					                .onDialogPositiveClick(ChangeCameraNameDialog.this);
				        }

			        }
		        }).setNegativeButton(R.string.Cancel,
		        new DialogInterface.OnClickListener()
		        {
			        public void onClick(DialogInterface dialog, int id)
			        {
				        ChangeCameraNameDialog.this.getDialog().cancel();
			        }
		        });
		return builder.create();
	}
}
