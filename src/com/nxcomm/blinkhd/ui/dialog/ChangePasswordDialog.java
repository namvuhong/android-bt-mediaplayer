package com.nxcomm.blinkhd.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class ChangePasswordDialog extends CommonDialog
{

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout

		contentView = inflater.inflate(R.layout.dialog_change_password, null);

		builder.setView(contentView);

		// build type face
		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Light.otf");
		TextView titleView = (TextView) contentView.findViewById(R.id.title);
		titleView.setTypeface(sboldTf);

		EditText pwd = (EditText) contentView.findViewById(R.id.txtNewPassword);
		EditText conf_pwd = (EditText) contentView
		        .findViewById(R.id.txtConfirmNewPassword);

		titleView.setTypeface(sboldTf);
		pwd.setTypeface(lightTf);
		conf_pwd.setTypeface(lightTf);

		builder.setPositiveButton(R.string.OK,
		        new DialogInterface.OnClickListener()
		        {
			        @Override
			        public void onClick(DialogInterface dialog, int id)
			        {
				        if (commonDialogListener != null)
				        {
					        commonDialogListener
					                .onDialogPositiveClick(ChangePasswordDialog.this);
				        }

			        }
		        }).setNegativeButton(R.string.Cancel,
		        new DialogInterface.OnClickListener()
		        {
			        public void onClick(DialogInterface dialog, int id)
			        {
				        ChangePasswordDialog.this.getDialog().cancel();
			        }
		        });
		return builder.create();
	}
}
