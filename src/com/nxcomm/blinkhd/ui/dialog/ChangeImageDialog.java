package com.nxcomm.blinkhd.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class ChangeImageDialog extends CommonDialog
{
	public static final int	SELECT_IMAGE_FROM_GALLERY	= 1;
	public static final int	TAKE_A_SNAPSHOT	          = 2;
	private int	            selectImageSource	      = SELECT_IMAGE_FROM_GALLERY;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout

		contentView = inflater.inflate(R.layout.dialog_change_image, null);

		builder.setView(contentView);

		// build type face
		Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		Typeface lightTf = FontManager.fontLight;// Typeface.createFromAsset(getActivity().getAssets(),
		// "fonts/ProximaNova-Light.otf");

		TextView titleView = (TextView) contentView
		        .findViewById(R.id.dialog_change_image_title);
		TextView fromGallery = (TextView) contentView
		        .findViewById(R.id.dialog_change_image_Select_From_Gallery_TextView);
		TextView fromSnapshot = (TextView) contentView
		        .findViewById(R.id.dialog_change_image_Take_A_Snapshot_TextView);

		titleView.setTypeface(sboldTf);
		fromGallery.setTypeface(lightTf);
		fromSnapshot.setTypeface(lightTf);

		final RadioButton fromGalleryRa = (RadioButton) contentView
		        .findViewById(R.id.dialog_change_image_from_gallery_radio);

		fromGalleryRa.setChecked(true);

		final RadioButton fromSnapshotRa = (RadioButton) contentView
		        .findViewById(R.id.dialog_change_image_from_take_snapshot_radio);

		View.OnClickListener radioOnClickListener = new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				RadioButton which = (RadioButton) v;
				if (which == fromGalleryRa)
				{
					fromGalleryRa.setChecked(true);
					selectImageSource = SELECT_IMAGE_FROM_GALLERY;
					fromSnapshotRa.setChecked(false);
				}
				else
				{
					fromGalleryRa.setChecked(false);
					selectImageSource = TAKE_A_SNAPSHOT;
					fromSnapshotRa.setChecked(true);
				}

			}

		};
		fromGalleryRa.setOnClickListener(radioOnClickListener);
		fromSnapshotRa.setOnClickListener(radioOnClickListener);

		builder.setPositiveButton(R.string.OK,
		        new DialogInterface.OnClickListener()
		        {
			        @Override
			        public void onClick(DialogInterface dialog, int id)
			        {
				        if (commonDialogListener != null)
				        {
					        commonDialogListener
					                .onDialogPositiveClick(ChangeImageDialog.this);
				        }

			        }
		        }).setNegativeButton(R.string.Cancel,
		        new DialogInterface.OnClickListener()
		        {
			        public void onClick(DialogInterface dialog, int id)
			        {
				        ChangeImageDialog.this.getDialog().cancel();
			        }
		        });
		return builder.create();
	}

	public int getSelectImageSource()
	{
		return selectImageSource;
	}
}
