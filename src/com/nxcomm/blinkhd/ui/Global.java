package com.nxcomm.blinkhd.ui;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import android.content.Context;
import android.content.SharedPreferences;

import com.msc3.PublicDefine;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CamListResponse;
import com.nxcomm.meapi.device.CameraInfo;

public class Global
{
	
	private String apiKey;
	
	public CameraInfo[] getCameraList() throws SocketTimeoutException,
			MalformedURLException, IOException
	{
		CamListResponse camList = Device.getOwnCamList(apiKey);
		CameraInfo[] cameraList = null;
		if (camList.isSucceed() && camList.getStatus() == 200)
		{
			cameraList = camList.getCamList();
		}
		
		return cameraList;
	}
	public static String getApiKey(Context ctx)
	{
		SharedPreferences prefs = ctx.getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		return prefs.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
	}
	
}
