package com.nxcomm.blinkhd.ui;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.registration.RegularCustomFontButton;
import com.nxcomm.cached.CameraEventOpenHelper;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;

public class CameraDetailSettingActivity2 extends Activity
{
	public static final String	         EXTRA_REGISTRATION_ID	   = "string_registration_id";
	public static final String	         EXTRA_SNAPSHOT_HEIGHT	   = "int_snapshot_height";
	public static final String	         EXTRA_CAMERA_NAME	       = "camera_name";
	public static final String	         EXTRA_FIRMWARE_VERSION	   = "firmware_version";
	public static final String	         EXTRA_CAMERA_IS_ONLINE	   = "is_online";
	public static final String	         EXTRA_CAMERA_IS_UPGRADING	= "is_upgrading";

	private CamProfile[]	             restored_profiles;

	private CameraDetailSettingsFragment	info;
	private CamProfile	                 cameraProfile;
	private String	                     regId, apiKey;
	private int	                         snapshotHeight;
	private static final String	         FRAGMENT_TAG	           = "camera_detail_settings_fragment";

	public static final String	         EXTRA_API_KEY	           = "apiKey";
	public static final String	         EXTRA_DEVICE_ID	       = "deviceID";
	private ProgressDialog	             removeCameraDialog;
	private String	                     cameraName, fwVersion;
	private boolean	                     isOnline, isUpgrading;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings
		        .getString(PublicDefine.PREFS_SAVED_SERVER_URL,
		                "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;

		setContentView(R.layout.camera_info_setting);

		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);
		setTitle(R.string.menu);
		actionBar.setDisplayHomeAsUpEnabled(true);
		FragmentTransaction fragmentTransact = getFragmentManager()
		        .beginTransaction();

		Bundle extra = getIntent().getExtras();
		regId = null;

		// GET - Device ID
		if (extra != null)
		{
			Log.i("mbp", "Extra != null");
			regId = extra.getString(EXTRA_REGISTRATION_ID);
			snapshotHeight = extra.getInt(EXTRA_SNAPSHOT_HEIGHT);

		}

		// GET - Api Key
		SharedPreferences sharePref = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		apiKey = sharePref.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
		        null);

		// Bundle data and send it to fragment

		extra.putString(EXTRA_API_KEY, apiKey);

		if (savedInstanceState == null)
		{
			info = new CameraDetailSettingsFragment();
			info.setArguments(extra);
			fragmentTransact.replace(R.id.cameraDetailPreference, info,
			        FRAGMENT_TAG);
			fragmentTransact.commit();
		}
		else
		{
			try
			{
				info = (CameraDetailSettingsFragment) getFragmentManager()
				        .findFragmentByTag(FRAGMENT_TAG);
				info.setArguments(extra);

			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				Log.i("mbp", "Reload CameraDetailSettingFragment Error.");
				info = new CameraDetailSettingsFragment();
				info.setArguments(extra);

				fragmentTransact.replace(R.id.cameraDetailPreference, info,
				        FRAGMENT_TAG);
				fragmentTransact.commit();
			}
		}

		RegularCustomFontButton removeButton = (RegularCustomFontButton) findViewById(R.id.removeCamera);
		if (removeButton != null)
		{
			removeButton.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					Log.d("mbp", "Remove camera called.");

					AlertDialog.Builder builder = new AlertDialog.Builder(
					        CameraDetailSettingActivity2.this);
					builder.setMessage(R.string.remove_camera_confirm)
					        .setPositiveButton(R.string.remove,
					                new DialogInterface.OnClickListener()
					                {
						                public void onClick(
						                        DialogInterface dialog, int id)
						                {
							                removeCameraDialog = ProgressDialog
							                        .show(CameraDetailSettingActivity2.this,
							                                null,
							                                getResources()
							                                        .getString(
							                                                R.string.remove_camera_notification));
							                removeCameraDialog
							                        .setCancelable(false);

							                RemoveCameraTask try_delCam = new RemoveCameraTask();
							                // if
							                // (!cameraProfile.isInLocal())
							                {
								                try_delCam
								                        .setOnlyRemoveFrServer(true);
							                }
							                try_delCam.execute(apiKey, regId);
						                }
					                })
					        .setNegativeButton(R.string.Cancel,
					                new DialogInterface.OnClickListener()
					                {
						                public void onClick(
						                        DialogInterface dialog, int id)
						                {

						                }
					                });

					AlertDialog removeConfirmDialog = builder.create();
					removeConfirmDialog.setCancelable(false);
					removeConfirmDialog.setTitle(R.string.remove_camera);
					removeConfirmDialog.setIcon(android.R.drawable.ic_delete);
					removeConfirmDialog.show();

				}
			});
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		// Respond to the action bar's Up/Home button
		case android.R.id.home:

			finish();

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		overridePendingTransition(R.anim.ac_slide_in_from_right,
		        R.anim.ac_slide_out_from_right_to_left);
	}

	public void onStart()
	{
		super.onStart();

		/*
		 * Bundle extra = getIntent().getExtras(); regId = null;
		 * 
		 * if (extra != null) { regId = extra.getString(EXTRA_REGISTRATION_ID);
		 * snapshotHeight = extra.getInt(EXTRA_SNAPSHOT_HEIGHT); }
		 */

		cameraProfile = null;
		if (restore_session_data() == true && regId != null)
		{

			for (CamProfile cp1 : restored_profiles)
			{
				if (cp1.getRegistrationId().equalsIgnoreCase(regId))
				{
					cameraProfile = cp1;
					break;
				}
			}

		}

		if (cameraProfile != null)
		{
		}
	}

	private boolean restore_session_data()
	{

		SetupData savedData = new SetupData();
		try
		{
			if (savedData.restore_session_data(getExternalFilesDir(null)))
			{
				restored_profiles = savedData.get_CamProfiles();

				return true;
			}
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}
		return false;

	}

	class RemoveCameraTask extends AsyncTask<String, String, Integer>
	{

		private String		     usrToken;
		private String		     regId;
		private String		     camName;

		private boolean		     only_remove_from_server;

		private static final int	DEL_CAM_SUCCESS		       = 0x1;
		private static final int	DEL_CAM_FAILED_UNKNOWN		= 0x2;
		private static final int	DEL_CAM_FAILED_SERVER_DOWN	= 0x11;

		public RemoveCameraTask()
		{
			only_remove_from_server = false;
		}

		public void setOnlyRemoveFrServer(boolean b)
		{
			only_remove_from_server = b;
		}

		/*
		 * Dont need to support UDT because Remove camera in remote will only
		 * remove camera from server
		 */
		@Override
		protected Integer doInBackground(String... params)
		{

			usrToken = params[0];
			regId = params[1];

			String http_usr, http_pass, http_cmd;

			if (only_remove_from_server == false)
			{
				String device_ip_and_port = params[2];
				/*
				 * 20120911: OBSOLETE: switch_to_uAP mode & reset
				 * 
				 * reset_factory & restart_system
				 */

				http_usr = params[3];
				http_pass = params[4];

				String http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESET_FACTORY);
				Log.d("mbp", "Remove camera, send reset factory");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESTART_DEVICE);
				Log.d("mbp", "Remove camera, send restart system");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				/*
				 * SEND THIS CMD UNTIL WE DON'T GET THE RESPONSE That means the
				 * device is already reset
				 */
				String test_conn = null;

				do
				{
					http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
					        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
					        PublicDefine.GET_VERSION);
					test_conn = HTTPRequestSendRecv
					        .sendRequest_block_for_response(http_addr,
					                http_usr, http_pass);
				}
				while (test_conn != null);

			}

			int ret = -1;
			try
			{
				SimpleJsonResponse response = Device.delete(usrToken, regId);
				if (response != null && response.isSucceed())
				{
					int status_code = response.getStatus();
					Log.d("mbp", "Del cam response code: " + status_code);
					if (status_code == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = DEL_CAM_FAILED_UNKNOWN;
					}
					else if (status_code == HttpURLConnection.HTTP_OK)
					{
						ret = DEL_CAM_SUCCESS;
					}
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (SocketTimeoutException e)
			{
				// Connection Timeout - Server unreachable ???
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (removeCameraDialog != null & removeCameraDialog.isShowing())
			{
				removeCameraDialog.dismiss();
			}

			if (result.intValue() == DEL_CAM_SUCCESS)
			{
				CameraEventOpenHelper.getInstance(getApplicationContext())
				        .removeCacheForCamera(regId);
				finish();
			}
			else
			{
				Log.i("mbp", "Remove camera failed.");

				AlertDialog.Builder builder = new AlertDialog.Builder(
				        CameraDetailSettingActivity2.this);
				builder.setMessage(R.string.remove_camera_failed)
				        .setNegativeButton(R.string.OK,
				                new DialogInterface.OnClickListener()
				                {
					                public void onClick(DialogInterface dialog,
					                        int id)
					                {

					                }
				                });

				builder.create().show();

			}

		}

	}
}
