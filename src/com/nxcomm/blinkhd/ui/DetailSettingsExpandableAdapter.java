package com.nxcomm.blinkhd.ui;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.test.ActivityUnitTestCase;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.msc3.ChangeNameTask;
import com.msc3.IChangeNameCallBack;
import com.msc3.PublicDefine;
import com.msc3.Util;
import com.msc3.comm.HTTPRequestSendRecv;
import com.nxcomm.blinkhd.commonasynctask.IAsyncTaskCommonHandler;
import com.nxcomm.blinkhd.ui.customview.ArtProgressView;
import com.nxcomm.blinkhd.ui.customview.OnStateChangeListener;
import com.nxcomm.blinkhd.ui.customview.SchedularView;
import com.nxcomm.blinkhd.ui.customview.SchedularView.Cell;
import com.nxcomm.blinkhd.ui.customview.SelectionBarView;
import com.nxcomm.blinkhd.ui.customview.SwitchableImageView;
import com.nxcomm.blinkhd.ui.customview.TemperatureMode;
import com.nxcomm.blinkhd.ui.customview.TemperatureUpDownView;
import com.nxcomm.blinkhd.ui.dialog.ChangeCameraNameDialog;
import com.nxcomm.blinkhd.ui.dialog.ChangeImageDialog;
import com.nxcomm.blinkhd.ui.dialog.CommonDialogListener;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;
import com.nxcomm.meapi.device.SendCommandResponse;

public class DetailSettingsExpandableAdapter extends BaseExpandableListAdapter

{

	private static final int	                   ONE_MINUTE	   = 60 * 1000;
	private final SparseArray<GeneralSettingGroup>	groups;
	public LayoutInflater	                       inflater;
	public Activity	                               activity;
	private Fragment	                           fragment;
	private Cell	                               latestCell	   = null;
	private boolean	                               longClickTicked	= false;
	private boolean	                               selectAction	   = true;

	private SchedularView	                       schedulerView;
	private boolean[][]	                           scheduler;
	private MyExpandableListView	               listView;
	private ProgressDialog	                       dialog;
	private ProgressDialog	                       temp_hi_dialog;
	private ProgressDialog	                       temp_lo_dialog;

	private String	                               apiKey, deviceID;

	public static final int	                       SELECT_PICTURE	= 0x00ffff;
	public static final int	                       TAKE_SNAPSHOT	= 0x00fff1;

	private CameraBasicInfo	                       cameraBasicInfo;
	private ProgressDialog	                       removeCameraDialog;

	public CameraBasicInfo getCameraBasicInfo()
	{
		return cameraBasicInfo;
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	public void setCameraBasicInfo(CameraBasicInfo cameraBasicInfo)
	{
		this.cameraBasicInfo = cameraBasicInfo;
	}

	public DetailSettingsExpandableAdapter(Fragment act,
	        SparseArray<GeneralSettingGroup> groups,
	        MyExpandableListView listView, String apiKey, String deviceID)
	{
		this.listView = listView;
		activity = act.getActivity();
		this.groups = groups;
		this.fragment = act;

		inflater = LayoutInflater.from(activity);
		// inflater = LayoutInflater.from(act.getActivity());

		this.apiKey = apiKey;
		this.deviceID = deviceID;

		if (deviceID != null)
		{
			/*
			 * 
			 * listView.getMotionOnOffTask = new SendCommandToCameraViaStun(
			 * motionOnOffHandler); listView.getSoundOnOffTask = new
			 * SendCommandToCameraViaStun( soundOnOffHandler);
			 * listView.getMotionThresholdTask = new SendCommandToCameraViaStun(
			 * motionThresholdHandler); listView.getSoundOnOffTask = new
			 * SendCommandToCameraViaStun( soundThresholdHandler);
			 */

			// listView.getMotionOnOffTask =
		}
		else
		{

		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosition)
	{
		return groups.get(groupPosition).children.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition)
	{
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
	        boolean isLastChild, View convertView, ViewGroup parent)
	{

		final String children = (String) getChild(groupPosition, childPosition);
		TextView text = null;
		if (!groups.get(groupPosition).isEnable())
		{
			return new View(activity.getApplicationContext());
		}

		if (convertView == null)
		{
			if (groupPosition == 0)
			{
				convertView = inflater.inflate(R.layout.camera_detail_fragment,
				        null);
				setupCameraDetails(convertView);
			}
			else if (groupPosition == 1)
			{
				convertView = inflater.inflate(
				        R.layout.settings_notification_sensitity, null);
				setupNotificationSensitity(convertView);
			}
			else if (groupPosition == 2)
			{
				return new View(activity.getApplicationContext());
			}
			// else if (groupPosition == 3)
			// {
			//
			// convertView = inflater.inflate(
			// R.layout.notification_scheduler_fragment, null);
			//
			// setupSchedulerView(convertView);
			//
			// }
			else
			{
				convertView = inflater.inflate(
				        R.layout.listrow_detail_general_setting, null);
				text = (TextView) convertView.findViewById(R.id.txtCamOne);
				text.setText(children);
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Toast.makeText(activity, children, Toast.LENGTH_SHORT)
						        .show();
					}
				});
			}
		}
		else
		{
			if (groupPosition == 0)
			{
				convertView = inflater.inflate(R.layout.camera_detail_fragment,
				        null);
				setupCameraDetails(convertView);
			}
			else if (groupPosition == 1)
			{
				convertView = inflater.inflate(
				        R.layout.settings_notification_sensitity, null);
				setupNotificationSensitity(convertView);
			}
			else if (groupPosition == 2)
			{
				return new View(activity.getApplicationContext());
			}
			// else if (groupPosition == 3)
			// {
			//
			// convertView = inflater.inflate(
			// R.layout.notification_scheduler_fragment, null);
			//
			// setupSchedulerView(convertView);
			//
			// }
			else
			{
				convertView = inflater.inflate(
				        R.layout.listrow_detail_general_setting, null);
				text = (TextView) convertView.findViewById(R.id.txtCamOne);
				text.setText(children);
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Toast.makeText(activity, children, Toast.LENGTH_SHORT)
						        .show();
					}
				});
			}
		}

		return convertView;
	}

	private void setupCameraDetails(View convertView)
	{

		final LinearLayout camera_name_of_location = (LinearLayout) convertView
		        .findViewById(R.id.camera_name_of_location);

		final TextView camreNameTextView = (TextView) convertView
		        .findViewById(R.id.camera_name_of_location_summary);
		camreNameTextView.setText(cameraBasicInfo.getName());

		final TextView firmwareVersionTextView = (TextView) convertView
		        .findViewById(R.id.camera_firmware_version_summary);
		firmwareVersionTextView.setText(cameraBasicInfo.getFwVersion());

		LinearLayout changeImageLinearLayout = (LinearLayout) convertView
		        .findViewById(R.id.camera_change_image);
		changeImageLinearLayout.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				final ChangeImageDialog changeImageDialog = new ChangeImageDialog();

				changeImageDialog
				        .setCommonDialogListener(new CommonDialogListener()
				        {
					        @Override
					        public void onDialogPositiveClick(
					                DialogFragment arg0)
					        {
						        if (changeImageDialog.getSelectImageSource() == ChangeImageDialog.SELECT_IMAGE_FROM_GALLERY)
						        {
							        Intent intent = new Intent(
							                Intent.ACTION_PICK,
							                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							        fragment.startActivityForResult(intent,
							                SELECT_PICTURE);
						        }
						        else
						        {
							        Intent intent = new Intent(
							                MediaStore.ACTION_IMAGE_CAPTURE);
							        String regId = cameraBasicInfo
							                .getRegistratrionID();
							        String snapFileName = Util
							                .getRootRecordingDirectory()
							                + File.separator
							                + getSnapshotNameHashed(regId)
							                + ".jpg";
							        Uri fileUri = Uri.fromFile(new File(
							                snapFileName));
							        intent.putExtra(MediaStore.EXTRA_OUTPUT,
							                fileUri);
							        fragment.startActivityForResult(intent,
							                TAKE_SNAPSHOT);
						        }
						        changeImageDialog.dismiss();

					        }

					        @Override
					        public void onDialogNegativeClick(
					                DialogFragment dialog)
					        {
						        // TODO Auto-generated method stub

					        }

				        });
				changeImageDialog.show(activity.getFragmentManager(),
				        "Dialog_Change_Image");

			}
		});

		camera_name_of_location.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final ChangeCameraNameDialog dialog = new ChangeCameraNameDialog();
				dialog.show(activity.getFragmentManager(),
				        "Dialog_Change_Camera_Name");

				dialog.setCommonDialogListener(new CommonDialogListener()
				{
					@Override
					public void onDialogPositiveClick(DialogFragment arg0)
					{
						final ProgressDialog updateNameDialog = ProgressDialog
						        .show(activity, null, activity.getResources()
						                .getString(R.string.update_camera_info));

						EditText text = (EditText) dialog
						        .findViewById(R.id.dialog_change_camera_name_CameraName_EditText);
						if (text == null)
						{
							return;
						}

						String camera_name = text.getText().toString().trim();
						if (camera_name != null && camera_name.length() >= 4
						        && camera_name.length() <= 31)
						{
							final String device_name = camera_name;

							ChangeNameTask rename = new ChangeNameTask(
							        activity, new IChangeNameCallBack()
							        {

								        @Override
								        public void update_cam_success()
								        {
									        // TODO Auto-generated method stub
									        cameraBasicInfo
									                .setName(device_name);
									        camreNameTextView
									                .setText(device_name);

									        Log.d("mbp",
									                "update camera name success.");
									        updateNameDialog.dismiss();
								        }

								        @Override
								        public void update_cam_failed()
								        {
									        // TODO Auto-generated method stub
									        updateNameDialog.dismiss();
								        }
							        });
							rename.execute(apiKey, device_name, deviceID);
						}
						dialog.dismiss();
					}

					@Override
					public void onDialogNegativeClick(DialogFragment dialog)
					{
					}
				});

			}
		});

		final Button removeCameraBtn = (Button) convertView
		        .findViewById(R.id.removeCamera);
		removeCameraBtn.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				removeCameraDialog = ProgressDialog.show(
				        activity,
				        null,
				        activity.getResources().getString(
				                R.string.remove_camera_notification));
				removeCameraDialog.setCancelable(false);

				RemoveCameraTask try_delCam = new RemoveCameraTask();
				// if (!cameraProfile.isInLocal())
				{
					try_delCam.setOnlyRemoveFrServer(true);
				}
				try_delCam.execute(apiKey, deviceID);

			}
		});

	}

	private void setupNotificationSensitity(View convertView)
	{
		final SwitchableImageView switchMotionOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switcMotionOnOff);
		final SwitchableImageView switchSoundOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchSoundOnOff);
		final SwitchableImageView switchTempOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchTemperatureOnOff);

		final SwitchableImageView switchLowTemp = (SwitchableImageView) convertView
		        .findViewById(R.id.switchableLowTemp);

		final SwitchableImageView switchHighTemp = (SwitchableImageView) convertView
		        .findViewById(R.id.SwitchableHighTemp);

		final SelectionBarView motionSelectionBarView = (SelectionBarView) convertView
		        .findViewById(R.id.selectionBarViewMotion);
		final SelectionBarView soundSelectionBarView = (SelectionBarView) convertView
		        .findViewById(R.id.selectionBarViewSound);

		final TemperatureUpDownView lowTemp = (TemperatureUpDownView) convertView
		        .findViewById(R.id.temperatureUpDownViewLow);
		lowTemp.setTemperatureValue(14);

		final TemperatureUpDownView highTemp = (TemperatureUpDownView) convertView
		        .findViewById(R.id.temperatureUpDownViewHigh);
		highTemp.setTemperatureValue(24);

		final IAsyncTaskCommonHandler motionSensitivityValue = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onPostExecute(Object result)
			{
				if (dialog != null)
					dialog.dismiss();
				String ret = (String) result;

				Log.d("mbp", "Motion sensitivity set result " + result);

				if (ret.contains(PublicDefine.SET_MOTION_SENSITIVITY_CMD
				        + ": 0"))
				{
					Log.d("mbp", "Motion sensitivity setted successfully.");
				}
				else
				{
					Log.d("mbp",
					        "Motion sensitivity setted fail. Reverse state");
					motionSelectionBarView.reverseState();
				}

			}

			@Override
			public void onCancelled()
			{

			}
		};
		motionSelectionBarView
		        .setOnStateChangeListener(new OnStateChangeListener()
		        {

			        @Override
			        public void onStateChanged(View v, int oldValue,
			                int newValue)
			        {
				        if (v == motionSelectionBarView)
				        {
					        listView.task = new SendCommandToCameraViaStun(
					                motionSensitivityValue);

					        String cmd = PublicDefine.SET_MOTION_SENSITIVITY_CMD;
					        String args = PublicDefine.SET_MOTION_SENSITIVITY_PARAM;
					        switch (newValue)
					        {
							case SelectionBarView.STATE_LOW:
								args += SelectionBarView.STATE_LOW_VALUE;
								break;
							case SelectionBarView.STATE_MEDIUM:
								args += SelectionBarView.STATE_MEDIUM_VALUE;
								break;
							case SelectionBarView.STATE_HIGH:
								args += SelectionBarView.STATE_HIGH_VALUE;
								break;

							}

							if (deviceID == null)
							{
								buildDialog("No selected camera").show();
								motionSelectionBarView.setCurrentState(
								        oldValue, false);
							}
							else
							{

								dialog = ProgressDialog
								        .show(activity,
								                null,
								                activity.getResources()
								                        .getString(
								                                R.string.set_motion_sensitivity_value));
								dialog.setCancelable(true);
								dialog.setCanceledOnTouchOutside(false);

								listView.task = new SendCommandToCameraViaStun(
								        motionSensitivityValue);
								// retrieve data

								listView.task.execute(apiKey, deviceID, cmd,
								        args);

							}

						}

					}
		        });

		final IAsyncTaskCommonHandler motionHandler = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{
				// TODO Auto-generated method stub
			}

			@Override
			public void onPostExecute(Object result)
			{
				if (dialog != null)
					dialog.dismiss();
				String ret = (String) result;

				Log.d("mbp", "Motion set result " + result);
				if (ret.contains((PublicDefine.SET_MOTION_AREA_CMD + ": 0")))
				{
					Log.d("mbp", "Motion detected setted on//off successfully.");

				}
				else
				{
					Log.d("mbp",
					        "Motion detected set on//off failed. Reverse state");
					// reserve state
					if (switchMotionOnOff.getBooleanState())
					{
						switchMotionOnOff
						        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						motionSelectionBarView.setViewEnable(false);
					}
					else
					{
						switchMotionOnOff
						        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);
						motionSelectionBarView.setViewEnable(true);
					}

				}

			}

			@Override
			public void onCancelled()
			{

			}
		};

		final IAsyncTaskCommonHandler voxThresholdHandler = new IAsyncTaskCommonHandler()
		{
			@Override
			public void onPreExecute()
			{
			}

			@Override
			public void onPostExecute(Object result)
			{
				if (dialog != null)
					dialog.dismiss();

				String ret = (String) result;

				if (ret.contains(PublicDefine.VOX_GET_THRESHOLD))
				{
					int threshold = Integer
					        .valueOf(ret
					                .substring(PublicDefine.VOX_GET_THRESHOLD
					                        .length() + 2));

					Log.d("mbp", "Vox 1 response : " + threshold);

					// round up to nearest limit
					if (threshold <= 25)
						threshold = 25;
					else if (threshold <= 70)
						threshold = 70;
					else
						threshold = 80;

					Log.d("mbp", "Vox 2 response : " + threshold);

					switch (threshold)
					{
					case 80: // LOW

						soundSelectionBarView.setCurrentState(
						        SelectionBarView.STATE_LOW, false);
						break;
					case 70: // MED
						soundSelectionBarView.setCurrentState(
						        SelectionBarView.STATE_MEDIUM, false);
						break;
					case 25: // HI
						soundSelectionBarView.setCurrentState(
						        SelectionBarView.STATE_HIGH, false);
						break;
					default:
						break;
					}
				}
				else
				{
					Log.d("mbp", "Vox unknown response : " + ret);
				}

			}

			@Override
			public void onCancelled()
			{

			}
		};

		final IAsyncTaskCommonHandler soundHandler = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onPostExecute(Object result)
			{

				String ret = (String) result;

				if (ret.contains(PublicDefine.VOX_DISABLE + ": 0"))
				{
					if (dialog != null)
						dialog.dismiss();

				}
				else if (ret.contains((PublicDefine.VOX_ENABLE + ": 0")))
				{
					String cmd = PublicDefine.VOX_GET_THRESHOLD;
					String args = "";

					listView.task = new SendCommandToCameraViaStun(
					        voxThresholdHandler);
					// retrieve data
					listView.task.execute(apiKey, deviceID, cmd, args);

				}
				else
				{

					Log.d("mbp",
					        "Sound detected setted on//off failed. Reverse state");
					// reserve state
					if (switchSoundOnOff.getBooleanState())
					{
						switchSoundOnOff
						        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						soundSelectionBarView.setViewEnable(false);
					}
					else
					{
						switchSoundOnOff
						        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);
						soundSelectionBarView.setViewEnable(true);
					}

					if (dialog != null)
						dialog.dismiss();

				}

			}

			@Override
			public void onCancelled()
			{

			}
		};

		final IAsyncTaskCommonHandler voxThresholdValue = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onPostExecute(Object result)
			{
				if (dialog != null)
					dialog.dismiss();
				String ret = (String) result;

				Log.d("mbp", "VOX sensitivity set result " + result);

				if (ret.contains(PublicDefine.VOX_SET_THRESHOLD + ": 0"))
				{
					Log.d("mbp", "VOX sensitivity setted successfully.");
				}
				else
				{
					Log.d("mbp", "VOX sensitivity setted fail. Reverse state");
					soundSelectionBarView.reverseState();
				}

			}

			@Override
			public void onCancelled()
			{

			}
		};

		soundSelectionBarView
		        .setOnStateChangeListener(new OnStateChangeListener()
		        {

			        @Override
			        public void onStateChanged(View v, int oldValue,
			                int newValue)
			        {
				        if (v == soundSelectionBarView)
				        {

					        String cmd = PublicDefine.VOX_SET_THRESHOLD;
					        String args = PublicDefine.VOX_SET_THRESHOLD_VALUE;
					        /*
							 * http://nxcomm-office.no-ip.info/bugzilla/show_bug.
							 * cgi?id=320 Please set the threshold value as
							 * follow : LOW 80 MEDIUM 70 HIGH 25
							 */
					        switch (newValue)
					        {
							case SelectionBarView.STATE_LOW:
								args += "80";
								break;
							case SelectionBarView.STATE_MEDIUM:
								args += "70";
								break;
							case SelectionBarView.STATE_HIGH:
								args += "25";
								break;
							}

							if (deviceID == null)
							{
								buildDialog("No selected camera").show();
								soundSelectionBarView.setCurrentState(oldValue,
								        false);
							}
							else
							{

								dialog = ProgressDialog
								        .show(activity,
								                null,
								                activity.getResources()
								                        .getString(
								                                R.string.set_sound_sensitivity_value));
								dialog.setCancelable(true);
								dialog.setCanceledOnTouchOutside(false);

								listView.task = new SendCommandToCameraViaStun(
								        voxThresholdValue);
								// retrieve data

								listView.task.execute(apiKey, deviceID, cmd,
								        args);

							}

						}

					}
		        });

		final IAsyncTaskCommonHandler tempHiStatHandler = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onPostExecute(Object result)
			{

				String ret = (String) result;

				if (ret.contains(PublicDefine.SET_TEMP_HI + ": 0"))
				{

				}
				else
				{

					Log.d("mbp",
					        "Temp detected setted on//off failed. Reverse state");
					// reserve state
					if (switchHighTemp.getBooleanState())
					{
						switchHighTemp
						        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						highTemp.setViewEnable(false);
					}
					else
					{
						switchHighTemp
						        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);
						highTemp.setViewEnable(true);
					}

				}

				if (dialog != null)
					dialog.dismiss();
			}

			@Override
			public void onCancelled()
			{

			}
		};

		final IAsyncTaskCommonHandler tempLoStatHandler = new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{

			}

			@Override
			public void onPostExecute(Object result)
			{

				String ret = (String) result;

				if (ret.contains(PublicDefine.SET_TEMP_LO + ": 0"))
				{

				}
				else
				{

					Log.d("mbp",
					        "Temp detected setted on//off failed. Reverse state");
					// reserve state
					if (switchLowTemp.getBooleanState())
					{
						switchLowTemp
						        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						lowTemp.setViewEnable(false);
					}
					else
					{
						switchLowTemp
						        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);
						lowTemp.setViewEnable(true);
					}

				}

				if (dialog != null)
					dialog.dismiss();
			}

			@Override
			public void onCancelled()
			{

			}
		};

		OnStateChangeListener stateChangeListener = new OnStateChangeListener()
		{

			@Override
			public void onStateChanged(View v, int oldValue, int newValue)
			{

				if (v == switchMotionOnOff)
				{

					if (switchMotionOnOff.getState() == SwitchableImageView.STATE_ONE)
					{

						String cmd = PublicDefine.SET_MOTION_AREA_CMD;
						String args = PublicDefine.MOTION_ON_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchMotionOnOff
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.set_motion_detected_on));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        motionHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							motionSelectionBarView.setViewEnable(true);
						}

					}
					else
					{

						String cmd = PublicDefine.SET_MOTION_AREA_CMD;

						String args = PublicDefine.MOTION_OFF_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchMotionOnOff
							        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);

						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.set_motion_detected_off));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        motionHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							motionSelectionBarView.setViewEnable(false);
						}
					}
				}
				else if (v == switchSoundOnOff)
				{
					if (switchSoundOnOff.getState() == SwitchableImageView.STATE_ONE)
					{
						String cmd = PublicDefine.VOX_ENABLE;
						String args = "";

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchSoundOnOff
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.set_sound_detected_on));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        soundHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							soundSelectionBarView.setViewEnable(true);
						}

					}
					else
					{
						String cmd = PublicDefine.VOX_DISABLE;
						String args = "";

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchSoundOnOff
							        .setStateWithoutTrigger(SwitchableImageView.STATE_ONE);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.set_sound_detected_off));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        soundHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							soundSelectionBarView.setViewEnable(false);
						}

					}
				}
				else if (v == switchTempOnOff)
				{
					if (switchTempOnOff.getState() == SwitchableImageView.STATE_ONE) // celcius

					{
						lowTemp.setTempMode(TemperatureMode.C);
						highTemp.setTempMode(TemperatureMode.C);

					}
					else
					{
						lowTemp.setTempMode(TemperatureMode.F);
						highTemp.setTempMode(TemperatureMode.F);
					}
				}
				else if (v == switchHighTemp)
				{
					if (switchHighTemp.getState() == SwitchableImageView.STATE_ONE) // on
					{
						String cmd = PublicDefine.SET_TEMP_HI;
						String args = PublicDefine.SET_TEMP_HI_ON_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchHighTemp
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.update_camera_setting_));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        tempHiStatHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							highTemp.setViewEnable(true);
						}

					}
					else
					{
						String cmd = PublicDefine.SET_TEMP_HI;
						String args = PublicDefine.SET_TEMP_HI_OFF_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchHighTemp
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.update_camera_setting_));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        tempHiStatHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							highTemp.setViewEnable(false);
						}
					}
				}
				else if (v == switchLowTemp)
				{
					if (switchLowTemp.getState() == SwitchableImageView.STATE_ONE) // on
					{
						String cmd = PublicDefine.SET_TEMP_LO;
						String args = PublicDefine.SET_TEMP_HI_ON_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchLowTemp
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.update_camera_setting_));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        tempLoStatHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							lowTemp.setViewEnable(true);
						}
					}
					else
					{
						String cmd = PublicDefine.SET_TEMP_LO;
						String args = PublicDefine.SET_TEMP_HI_OFF_PARAM;

						if (deviceID == null)
						{
							buildDialog("No selected camera").show();
							switchLowTemp
							        .setStateWithoutTrigger(SwitchableImageView.STATE_TWO);
						}
						else
						{

							dialog = ProgressDialog.show(
							        activity,
							        null,
							        activity.getResources().getString(
							                R.string.update_camera_setting_));
							dialog.setCancelable(true);
							dialog.setCanceledOnTouchOutside(false);

							listView.task = new SendCommandToCameraViaStun(
							        tempLoStatHandler);
							// retrieve data

							listView.task.execute(apiKey, deviceID, cmd, args);
							lowTemp.setViewEnable(false);
						}
					}
				}

			}
		};

		switchMotionOnOff.setOnStateChangeListener(stateChangeListener);
		switchTempOnOff.setOnStateChangeListener(stateChangeListener);
		switchLowTemp.setOnStateChangeListener(stateChangeListener);
		switchHighTemp.setOnStateChangeListener(stateChangeListener);

		switchSoundOnOff.setOnStateChangeListener(stateChangeListener);

		updateSettings(convertView);
	}

	private void updateSettings(View convertView)
	{
		final SwitchableImageView switchMotionOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switcMotionOnOff);
		final SwitchableImageView switchSoundOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchSoundOnOff);
		final SwitchableImageView switchTempOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchTemperatureOnOff);

		final SwitchableImageView switchLowTemp = (SwitchableImageView) convertView
		        .findViewById(R.id.switchableLowTemp);

		final SwitchableImageView switchHighTemp = (SwitchableImageView) convertView
		        .findViewById(R.id.SwitchableHighTemp);

		final SelectionBarView motionSelectionBarView = (SelectionBarView) convertView
		        .findViewById(R.id.selectionBarViewMotion);
		final SelectionBarView soundSelectionBarView = (SelectionBarView) convertView
		        .findViewById(R.id.selectionBarViewSound);

		final TemperatureUpDownView lowTemp = (TemperatureUpDownView) convertView
		        .findViewById(R.id.temperatureUpDownViewLow);
		lowTemp.setBoundaries(18.0f, 10.0f);
		lowTemp.setExec(new TemperatureUpDownView.ITemperatureUpDownViewExecuter()
		{

			@Override
			public void commitChange(TemperatureUpDownView view,
			        final float newValue)
			{

				final IAsyncTaskCommonHandler tempLoThresholdHandler = new IAsyncTaskCommonHandler()
				{

					@Override
					public void onPreExecute()
					{

					}

					@Override
					public void onPostExecute(Object result)
					{

						String ret = (String) result;

						if (ret.contains(PublicDefine.SET_TEMP_LO_THRESHOLD
						        + ": 0"))
						{

						}
						else
						{
							Log.d("mbp", "Temp Lo threhold set failed ");
							// reserve state

						}

						if (temp_lo_dialog != null)
							temp_lo_dialog.dismiss();
					}

					@Override
					public void onCancelled()
					{

					}
				};

				listView.post(new Runnable()
				{

					@Override
					public void run()
					{
						String cmd = PublicDefine.SET_TEMP_LO_THRESHOLD;
						String args = "&value=" + (int) newValue;

						temp_lo_dialog = ProgressDialog.show(
						        activity,
						        null,
						        activity.getString(R.string.update_camera_setting_));
						temp_lo_dialog.setCancelable(true);
						temp_lo_dialog.setCanceledOnTouchOutside(false);

						// Get all settings
						listView.task = new SendCommandToCameraViaStun(
						        tempLoThresholdHandler);
						// retrieve data
						listView.task.execute(apiKey, deviceID, cmd, args);

					}
				});

			}

		});

		final TemperatureUpDownView highTemp = (TemperatureUpDownView) convertView
		        .findViewById(R.id.temperatureUpDownViewHigh);
		highTemp.setBoundaries(33.0f, 25.0f);

		highTemp.setExec(new TemperatureUpDownView.ITemperatureUpDownViewExecuter()
		{

			@Override
			public void commitChange(TemperatureUpDownView view,
			        final float newValue)
			{

				final IAsyncTaskCommonHandler tempHiThresholdHandler = new IAsyncTaskCommonHandler()
				{

					@Override
					public void onPreExecute()
					{

					}

					@Override
					public void onPostExecute(Object result)
					{

						String ret = (String) result;

						if (ret.contains(PublicDefine.SET_TEMP_HI_THRESHOLD
						        + ": 0"))
						{

						}
						else
						{
							Log.d("mbp", "Temp Lo threhold set failed ");
							// reserve state

						}

						if (temp_hi_dialog != null)
							temp_hi_dialog.dismiss();
					}

					@Override
					public void onCancelled()
					{

					}
				};

				listView.post(new Runnable()
				{
					@Override
					public void run()
					{
						String cmd = PublicDefine.SET_TEMP_HI_THRESHOLD;
						String args = "&value=" + (int) newValue;

						temp_hi_dialog = ProgressDialog.show(
						        activity,
						        null,
						        activity.getString(R.string.update_camera_setting_));
						temp_hi_dialog.setCancelable(true);
						temp_hi_dialog.setCanceledOnTouchOutside(false);

						// Get all settings
						listView.task = new SendCommandToCameraViaStun(
						        tempHiThresholdHandler);
						// retrieve data
						listView.task.execute(apiKey, deviceID, cmd, args);
					}
				});

			}

		});

		final IAsyncTaskCommonHandler deviceSettingsHandler = new IAsyncTaskCommonHandler()
		{
			@Override
			public void onPreExecute()
			{
			}

			@Override
			public void onPostExecute(Object result)
			{
				String ret = (String) result;

				if (ret.contains(PublicDefine.DEVICE_SETTING))
				{

					/*
					 * device_setting: ms=[motion status],me=[motion
					 * sensitivity], vs=[vox status],vt=[vox threshold],hs=[high
					 * temp detection status], ls=[high temp detection
					 * status],ht=[high temp threshold],lt=[low temp threshold]
					 */
					String settings = ret.substring(PublicDefine.DEVICE_SETTING
					        .length() + 2);

					String individual_setting[] = settings.split(",");
					if (individual_setting.length == 8)
					{
						String ms = individual_setting[0];
						int state = (ms.equalsIgnoreCase("ms=1")) ? SwitchableImageView.STATE_ONE
						        : SwitchableImageView.STATE_TWO;
						switchMotionOnOff.setStateWithoutTrigger(state);

						int threshold;

						String me = individual_setting[1];
						threshold = Integer.valueOf(me.split("=")[1]);

						// round up to nearest limitxxx
						if (threshold <= SelectionBarView.STATE_LOW_VALUE)
							threshold = SelectionBarView.STATE_LOW_VALUE;
						else if (threshold <= SelectionBarView.STATE_MEDIUM_VALUE)
							threshold = SelectionBarView.STATE_MEDIUM_VALUE;
						else
							threshold = SelectionBarView.STATE_HIGH_VALUE;

						switch (threshold)
						{
						case SelectionBarView.STATE_LOW_VALUE: // LOW
							motionSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_LOW, false);
							break;
						case SelectionBarView.STATE_MEDIUM_VALUE: // MED
							motionSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_MEDIUM, false);
							break;
						case SelectionBarView.STATE_HIGH_VALUE: // HI
							motionSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_HIGH, false);
							break;
						default:
							break;
						}
						if (state == SwitchableImageView.STATE_TWO)
						{
							motionSelectionBarView.setViewEnable(false);
						}

						String vs = individual_setting[2];
						state = (vs.equalsIgnoreCase("vs=1")) ? SwitchableImageView.STATE_ONE
						        : SwitchableImageView.STATE_TWO;
						switchSoundOnOff.setStateWithoutTrigger(state);

						String vt = individual_setting[3];
						threshold = Integer.valueOf(vt.split("=")[1]);
						// round up to nearest limit
						if (threshold <= 25)
							threshold = 25;
						else if (threshold <= 70)
							threshold = 70;
						else
							threshold = 80;

						switch (threshold)
						{
						case 80: // LOW
							soundSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_LOW, false);
							break;
						case 70: // MED
							soundSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_MEDIUM, false);
							break;
						case 25: // HI
							soundSelectionBarView.setCurrentState(
							        SelectionBarView.STATE_HIGH, false);
							break;
						default:
							break;
						}
						if (state == SwitchableImageView.STATE_TWO)
						{
							soundSelectionBarView.setViewEnable(false);
						}

						String hs = individual_setting[4];
						state = (hs.equalsIgnoreCase("hs=1")) ? SwitchableImageView.STATE_ONE
						        : SwitchableImageView.STATE_TWO;
						switchHighTemp.setStateWithoutTrigger(state);

						String ht = individual_setting[6];
						// Get threhold in degree
						threshold = Integer.valueOf(ht.split("=")[1]);
						highTemp.setTemperatureValue((float) threshold);
						if (state == SwitchableImageView.STATE_TWO)
						{
							highTemp.setViewEnable(false);
						}

						String ls = individual_setting[5];
						state = (ls.equalsIgnoreCase("ls=1")) ? SwitchableImageView.STATE_ONE
						        : SwitchableImageView.STATE_TWO;
						switchLowTemp.setStateWithoutTrigger(state);

						String lt = individual_setting[7];
						threshold = Integer.valueOf(lt.split("=")[1]);
						lowTemp.setTemperatureValue((float) threshold);
						if (state == SwitchableImageView.STATE_TWO)
						{
							lowTemp.setViewEnable(false);
						}

						if (switchTempOnOff.getState() == SwitchableImageView.STATE_ONE) // celcius
						{
							lowTemp.setTempMode(TemperatureMode.C);
							highTemp.setTempMode(TemperatureMode.C);

						}
						else
						{
							lowTemp.setTempMode(TemperatureMode.F);
							highTemp.setTempMode(TemperatureMode.F);
						}
					}
				}
				else
				{
					Log.d("mbp", "DevSettings unknown response : " + ret);
				}

				if (dialog != null && dialog.isShowing())
				{
					dialog.dismiss();
				}
			}

			@Override
			public void onCancelled()
			{
				// TODO Auto-generated method stub

			}
		};

		if (deviceID == null)
		{
			return;
		}

		// try to change state of switch on/off

		String cmd = PublicDefine.DEVICE_SETTING;
		String args = "";

		dialog = ProgressDialog.show(activity, null,
		        activity.getString(R.string.querying_camera_settings_));
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);

		// Get all settings
		listView.task = new SendCommandToCameraViaStun(deviceSettingsHandler);
		// retrieve data
		listView.task.execute(apiKey, deviceID, cmd, args);

	}

	private void setupSchedulerView(View convertView)
	{
		final SwitchableImageView switchScheduler = (SwitchableImageView) convertView
		        .findViewById(R.id.swicthSchedulerOnOff);
		final SwitchableImageView schedulerByDayOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchSchedulerByDayOnOff);

		schedulerView = (SchedularView) convertView.findViewById(R.id.gridview);

		switchScheduler.setOnStateChangeListener(new OnStateChangeListener()
		{

			@Override
			public void onStateChanged(View v, int oldValue, int newValue)
			{
				if (switchScheduler.getState() == SwitchableImageView.STATE_ONE) // ON
				{

					schedulerByDayOnOff.setViewEnable(true);
					schedulerView.setViewEnable(true);
				}
				else
				{
					schedulerByDayOnOff.setViewEnable(false);
					schedulerView.setViewEnable(false);
				}

			}
		});

		schedulerView.setOnLongClickListener(new OnLongClickListener()
		{

			@Override
			public boolean onLongClick(View v)
			{
				if (schedulerView.isViewEnable())
				{
					// TODO Auto-generated method stub
					Log.d("mbp", "Long click .................");

					// test code
					/*
					 * scheduler = new boolean[7][24]; scheduler[0][0] = true;
					 * scheduler[0][1] = true; scheduler[6][7] = true;
					 * schedulerView.setScheduler(scheduler);
					 * 
					 * Gson gson = new Gson(); Log.d("mbp", "VALUE is " +
					 * gson.toJson(schedulerView.getScheduler()));
					 */
					longClickTicked = true;
					listView.setScrollable(false);
				}
				return false;
			}
		});

		schedulerView.setOnTouchListener(new OnTouchListener()
		{

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				// TODO Auto-generated method stub

				if (event.getAction() == MotionEvent.ACTION_MOVE
				        && longClickTicked)
				{
					Cell cell = schedulerView.getCellAt(event.getX(),
					        event.getY());
					if (cell != null)
					{
						if (selectAction)
						{
							cell.setState(Cell.State.SELECTED);

						}
						else
						// clear action
						{
							cell.setState(Cell.State.NORMAL);
						}

						schedulerView.invalidate();
					}
				}
				else if (event.getAction() == MotionEvent.ACTION_UP
				        && longClickTicked)
				{
					Log.d("mbp",
					        "Touch up at " + event.getX() + "x" + event.getY());

					longClickTicked = false;

					listView.setScrollable(true);

					selectAction = false;
				}

				else if (event.getAction() == MotionEvent.ACTION_DOWN
				        && longClickTicked)
				{

					selectAction = true;
				}

				else if (event.getAction() == MotionEvent.ACTION_DOWN
				        && !longClickTicked)
				{
					latestCell = schedulerView.getCellAt(event.getX(),
					        event.getY());
					if (latestCell != null)
					{
						if (latestCell.getState() == Cell.State.SELECTED)
						{
							selectAction = false;
						}
						else
						{
							selectAction = true;
						}
					}
				}
				return false;

			}
		});

		schedulerByDayOnOff
		        .setOnStateChangeListener(new OnStateChangeListener()
		        {
			        @Override
			        public void onStateChanged(View v, int oldValue,
			                int newValue)
			        {
				        if (newValue == SwitchableImageView.STATE_ONE) // by day
				                                                       // on
				        {
					        schedulerView
					                .setDrawMode(SchedularView.DrawMode.BYWEEK);
				        }
				        else if (newValue == SwitchableImageView.STATE_TWO) // by
				                                                            // day
				                                                            // off
				        {
					        schedulerView
					                .setDrawMode(SchedularView.DrawMode.BYDAY);
				        }
			        }
		        });
	}

	@Override
	public int getChildrenCount(int groupPosition)
	{

		return groups.get(groupPosition).children.size();
	}

	@Override
	public Object getGroup(int groupPosition)
	{
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount()
	{
		return groups.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition)
	{
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition)
	{
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition)
	{
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
	        View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = inflater.inflate(
			        R.layout.listrow_group_general_setting, null);
		}
		GeneralSettingGroup group = (GeneralSettingGroup) getGroup(groupPosition);

		Typeface regTf = FontManager.fontRegular;// Typeface.createFromAsset(activity.getAssets(),
		// "fonts/ProximaNova-Regular.otf");

		((CheckedTextView) convertView).setText(group.getText());
		((CheckedTextView) convertView).setChecked(isExpanded);
		((CheckedTextView) convertView).setTypeface(regTf);

		if (group.isEnable())
		{
			Drawable img = this.activity.getApplicationContext().getResources()
			        .getDrawable(group.getDrawableID());

			if (img == null)
				Log.d("mbp", "Active image is NULL");
			else
				((CheckedTextView) convertView)
				        .setCompoundDrawablesWithIntrinsicBounds(img, null,
				                null, null);
		}
		else
		{
			Drawable img = this.activity.getApplicationContext().getResources()
			        .getDrawable(group.getDrawableIdDisable());

			if (img == null)
				Log.d("mbp", "Disable image is NULL");
			else
				((CheckedTextView) convertView)
				        .setCompoundDrawablesWithIntrinsicBounds(img, null,
				                null, null);
		}
		return convertView;
	}

	@Override
	protected void finalize() throws Throwable
	{
		Log.d("mbp", "Finalize CustomExpandable Adapter");
		super.finalize();
	}

	@Override
	public boolean hasStableIds()
	{
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}

	private Dialog buildDialog(String msg)
	{
		final AlertDialog dialog = new Builder(activity).create();
		dialog.setCancelable(true);
		dialog.setMessage(msg);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getResources()
		        .getString(R.string.Cancel),
		        new DialogInterface.OnClickListener()
		        {

			        @Override
			        public void onClick(DialogInterface arg0, int arg1)
			        {
				        dialog.dismiss();

			        }
		        });

		return dialog;

	}

	class SendCommandToCameraViaStun extends AsyncTask<String, Void, String>
	{

		private IAsyncTaskCommonHandler	handler;

		public SendCommandToCameraViaStun(IAsyncTaskCommonHandler handler)
		{
			this.handler = handler;
		}

		@Override
		protected String doInBackground(String... params)
		{
			String returnResult = "";
			if (params.length == 4)
			{
				String apiKey = params[0];
				String deviceID = params[1];
				String cmd = params[2];
				String cmdArgs = params[3];

				try
				{
					SendCommandResponse res = Device.sendCommand(apiKey,
					        deviceID, cmd + cmdArgs);
					Log.d("mbp", "SendCommandToCameraViaStun: " + cmd + cmdArgs);

					if (res.getStatus() == 200
					        && res.getSendCommandResponseData() != null)
					{
						if (res.getSendCommandResponseData()
						        .getDevice_response() != null)
						{

							returnResult = res.getSendCommandResponseData()
							        .getDevice_response().getBody();

						}
					}
				}
				catch (SocketTimeoutException e)
				{
					e.printStackTrace();
				}
				catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			return returnResult;
		}

		@Override
		protected void onCancelled()
		{
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result)
		{
			// TODO Auto-generated method stub
			if (handler != null)
			{
				handler.onPostExecute(result);
			}
			super.onPostExecute(result);
		}
	}

	public static String getSnapshotNameHashed(String str)
	{
		String hashed_str = null;
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] bytes = digest.digest(str.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte b : bytes)
			{
				sb.append(String.format("%02X", b));
			}
			hashed_str = sb.toString();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return hashed_str;
	}

	class RemoveCameraTask extends AsyncTask<String, String, Integer>
	{

		private String		     usrToken;
		private String		     regId;
		private String		     camName;

		private boolean		     only_remove_from_server;

		private static final int	DEL_CAM_SUCCESS		       = 0x1;
		private static final int	DEL_CAM_FAILED_UNKNOWN		= 0x2;
		private static final int	DEL_CAM_FAILED_SERVER_DOWN	= 0x11;

		public RemoveCameraTask()
		{
			only_remove_from_server = false;
		}

		public void setOnlyRemoveFrServer(boolean b)
		{
			only_remove_from_server = b;
		}

		/*
		 * Dont need to support UDT because Remove camera in remote will only
		 * remove camera from server
		 */
		@Override
		protected Integer doInBackground(String... params)
		{

			usrToken = params[0];
			regId = params[1];

			String http_usr, http_pass, http_cmd;

			if (only_remove_from_server == false)
			{
				String device_ip_and_port = params[2];
				/*
				 * 20120911: OBSOLETE: switch_to_uAP mode & reset
				 * 
				 * reset_factory & restart_system
				 */

				http_usr = params[3];
				http_pass = params[4];

				String http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESET_FACTORY);
				Log.d("mbp", "Remove camera, send reset factory");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESTART_DEVICE);
				Log.d("mbp", "Remove camera, send restart system");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				/*
				 * SEND THIS CMD UNTIL WE DON'T GET THE RESPONSE That means the
				 * device is already reset
				 */
				String test_conn = null;

				do
				{
					http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
					        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
					        PublicDefine.GET_VERSION);
					test_conn = HTTPRequestSendRecv
					        .sendRequest_block_for_response(http_addr,
					                http_usr, http_pass);
				}
				while (test_conn != null);

			}

			int ret = -1;
			try
			{
				SimpleJsonResponse response = Device.delete(usrToken, regId);
				if (response != null && response.isSucceed())
				{
					int status_code = response.getStatus();
					Log.d("mbp", "Del cam response code: " + status_code);
					if (status_code == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = DEL_CAM_FAILED_UNKNOWN;
					}
					else if (status_code == HttpURLConnection.HTTP_OK)
					{
						ret = DEL_CAM_SUCCESS;
					}
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (SocketTimeoutException e)
			{
				// Connection Timeout - Server unreachable ???
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (removeCameraDialog != null & removeCameraDialog.isShowing())
			{
				removeCameraDialog.dismiss();
			}

			if (result.intValue() == DEL_CAM_SUCCESS)
			{

				if (activity != null)
				{
					Log.i("mbp", "Class of activity "
					        + activity.getClass().getName());
					activity.finish();
				}

			}
			else
			{
				Log.i("mbp", "Remove camera failed.");

				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setMessage(R.string.remove_camera_failed)
				        .setNegativeButton(R.string.OK,
				                new DialogInterface.OnClickListener()
				                {
					                public void onClick(DialogInterface dialog,
					                        int id)
					                {

					                }
				                });

				builder.create().show();
			}

		}

	}

}
