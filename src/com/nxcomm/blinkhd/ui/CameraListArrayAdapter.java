package com.nxcomm.blinkhd.ui;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.media.ExifInterface;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.Util;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nxcomm.blinkhd.ui.customview.CameraStatusView;
import com.nxcomm.blinkhd.ui.customview.CameraStatusView.CameraStatus;

public class CameraListArrayAdapter extends ArrayAdapter<CamProfile>
{
	private Context	        context;
	SparseArray<CamProfile>	cameraps	= new SparseArray<CamProfile>();
	ColorMatrixColorFilter	grayScaleFilter;

	public CameraListArrayAdapter(Context context,
	        SparseArray<CamProfile> cameraps)
	{

		super(context, R.layout.list_row_camera_setting);
		this.context = context;
		this.cameraps = cameraps;

		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		grayScaleFilter = new ColorMatrixColorFilter(cm);
		notifyDataSetChanged();
	}

	// private DisplayImageOptions options;
	// private ImageLoader imageLoader;

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_row_camera_setting,
		        parent, false);
		ImageButton imgButton = (ImageButton) rowView
		        .findViewById(R.id.list_row_camera_setting_camSettingBtn);

		RelativeLayout snapLayout = (RelativeLayout) rowView
		        .findViewById(R.id.relativeLayout1);
		final int row_height = snapLayout.getLayoutParams().height;
		imgButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent camdetail = new Intent(context,
				        CameraDetailSettingActivity2.class);
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_REGISTRATION_ID,
				        cameraps.get(position).getRegistrationId());
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_SNAPSHOT_HEIGHT,
				        row_height);
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_CAMERA_NAME,
				        cameraps.get(position).getName());
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_CAMERA_IS_UPGRADING,
				        cameraps.get(position).isUpgrading());
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_CAMERA_IS_ONLINE,
				        cameraps.get(position).isReachableInRemote());
				camdetail.putExtra(
				        CameraDetailSettingActivity2.EXTRA_FIRMWARE_VERSION,
				        cameraps.get(position).getFirmwareVersion());

				context.startActivity(camdetail);

				// ((Activity) context).startActivityForResult(camdetail,
				// SettingsActivity.CAMERA_REMOVE_STATUS);
			}
		});

		ImageView cameraSnap = (ImageView) rowView
		        .findViewById(R.id.list_row_camera_setting_imageCamera);

		String regId = cameraps.get(position).getRegistrationId();
		String snapFileName = Util.getRootRecordingDirectory() + File.separator
		        + CameraDetailSettingActivity.getSnapshotNameHashed(regId)
		        + ".jpg";

		File snap = new File(snapFileName);
		if (snap.exists())
		{
			checkAndRescaleSnap(snapFileName, row_height);
			Bitmap bm = BitmapFactory.decodeFile(snapFileName);
			cameraSnap.setImageBitmap(bm);
		}

		ImageView img_overlay = (ImageView) rowView
		        .findViewById(R.id.camera_snap_overlay);
		img_overlay.setImageResource(R.drawable.video_overlay_shadow);

		String imageLink = cameraps.get(position).getImageLink();
		if (imageLink != null)
		{

			ImageLoader.getInstance().displayImage(imageLink, cameraSnap,
			        new ImageLoadingListener()
			        {

				        @Override
				        public void onLoadingCancelled(String arg0, View arg1)
				        {
					        // TODO Auto-generated method stub

				        }

				        @Override
				        public void onLoadingComplete(String arg0, View arg1,
				                Bitmap arg2)
				        {
					        // TODO Auto-generated method stub

				        }

				        @Override
				        public void onLoadingFailed(String arg0, View arg1,
				                FailReason arg2)
				        {
					        // TODO Auto-generated method stub

				        }

				        @Override
				        public void onLoadingStarted(String arg0, View arg1)
				        {
					        // TODO Auto-generated method stub

				        }

			        });
		}

		cameraSnap.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if (CamProfile.shouldEnableMic(cameraps.get(position)
				        .getModelId()) == false
				        && cameraps.get(position).isReachableInRemote() == false)
				{
					// donot allow go to video screen
				}
				else
				{
					if (cameraps.get(position).isUpgrading())
					{
						AlertDialog.Builder builder = new AlertDialog.Builder(
						        context);
						builder.setMessage(R.string.upgrading_in_progress)
						        .setNegativeButton(R.string.OK,
						                new DialogInterface.OnClickListener()
						                {
							                public void onClick(DialogInterface dialog, int id)
							                {

							                }
						                });
						       

						AlertDialog upgradingDialog = builder.create();
						upgradingDialog.setCancelable(true);
						upgradingDialog.show();
						
					}
					else
					{
						SharedPreferences m = context.getSharedPreferences(
						        PublicDefine.PREFS_NAME, 0);
						Editor editor = m.edit();
						editor.putString(
						        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
						        cameraps.get(position).getRegistrationId());
						editor.commit();
						
						Intent intent = new Intent(context,
						        HomeScreenActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context.startActivity(intent);
					}
				}

			}
		});

		final Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(context.getAssets(),
		// "fonts/ProximaNova-Semibold.otf");
		TextView textView = (TextView) rowView
		        .findViewById(R.id.textViewCameraName);
		textView.setTypeface(sboldTf);
		textView.setText(cameraps.get(position).getName());

		SharedPreferences m = context.getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);

		// set selected camera name to green
		String regID = m.getString(
		        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
		        null);

		View indicatorView = rowView
		        .findViewById(R.id.list_row_camera_setting_indicatorView);
		CameraStatusView cameraStatusView = (CameraStatusView) rowView
		        .findViewById(R.id.list_row_camera_ImageView_CameraStatus);
		if (regID != null)
		{
			if (cameraps.get(position).getRegistrationId()
			        .equalsIgnoreCase(regID))
			{
				indicatorView.setBackgroundColor(0xFF00ACF7);
			}
		}

		if (!cameraps.get(position).isReachableInRemote())
		{
			cameraSnap.setColorFilter(grayScaleFilter);
			cameraStatusView.setOnline(false);
		}
		else
		{
			cameraStatusView.setOnline(true);
		}

		if (cameraps.get(position).isUpgrading())
		{
			cameraStatusView.setCameraStatus(CameraStatus.UPGRADING);

		}
		else
		{
			cameraStatusView.setCameraStatus(CameraStatus.NORMAL);
		}

		return rowView;
	}

	private void checkAndRescaleSnap(String path, int row_height)
	{
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opts);

		// Calculate inSampleSize
		opts.inSampleSize = calculateInSampleSize(opts, row_height);
		if (opts.inSampleSize != 1)
		{
			// Decode bitmap with inSampleSize set
			opts.inJustDecodeBounds = false;
			Bitmap savedBitmap = BitmapFactory.decodeFile(path, opts);
			ExifInterface exif = null;
			try
			{
				exif = new ExifInterface(path);
			}
			catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (exif != null)
			{
				int orientation = exif.getAttributeInt(
				        ExifInterface.TAG_ORIENTATION,
				        ExifInterface.ORIENTATION_NORMAL);
				int rotate = 0;
				switch (orientation)
				{
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotate = 270;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotate = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotate = 90;
					break;
				}
				Matrix matrix = new Matrix();
				matrix.postRotate(rotate);
				savedBitmap = Bitmap.createBitmap(savedBitmap, 0, 0,
				        savedBitmap.getWidth(), savedBitmap.getHeight(),
				        matrix, true);
			}

			File snap_file = new File(path);
			if (snap_file.exists())
			{
				snap_file.delete();
			}

			BufferedOutputStream bos = null;
			try
			{
				bos = new BufferedOutputStream(new FileOutputStream(path));
				savedBitmap.compress(CompressFormat.JPEG, 75, bos);
				bos.flush();
				bos.close();

			}
			catch (IOException e)
			{
				// todo: error handling
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param options
	 *            options contain width & height of the image which we want to
	 *            scaled
	 * @param reqHeight
	 *            desired height after scaled
	 * @return inSampleSize for scaling
	 */
	private int calculateInSampleSize(BitmapFactory.Options options,
	        int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		// final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight)
		{
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((height / inSampleSize) > reqHeight)
			{
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return this.cameraps.size();
	}
}
