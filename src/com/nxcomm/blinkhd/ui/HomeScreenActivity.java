package com.nxcomm.blinkhd.ui;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.BlinkHDApplication;
import com.blinkhd.DirectionFragment;
import com.blinkhd.FontManager;
import com.blinkhd.MelodyFragment;
import com.blinkhd.R;
import com.blinkhd.RecordingFragment;
import com.blinkhd.TalkbackFragment;
import com.blinkhd.TemperatureFragment;
import com.blinkhd.playback.CameraUnavailableFragment;
import com.blinkhd.playback.LiveFragment;
import com.blinkhd.playback.LiveFragment.IGotoFullScreenHandler;
import com.cvision.eventtimeliner.EventManagerFragment;
import com.discovery.LocalScanForCameras;
import com.discovery.ScanProfile;
import com.media.ffmpeg.FFMpegPlayer;
import com.media.ffmpeg.android.FFMpegMovieViewAndroid;
import com.msc3.BabyMonitorAuthentication;
import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.ConnectToNetwork;
import com.msc3.CountDownTimer;
import com.msc3.ICameraScanner;
import com.msc3.ITimerUpdater;
import com.msc3.IWifiScanUpdater;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.Streamer;
import com.msc3.StreamerFactory;
import com.msc3.VideoStreamer;
import com.msc3.ViewRemoteCamRequestTask;
import com.msc3.ViewRemoteCamViaWowzaRequestTask;
import com.msc3.WifiScan;
import com.nxcomm.blinkhd.commonasynctask.IAsyncTaskCommonHandler;
import com.nxcomm.blinkhd.commonasynctask.SendCommandAsyncTask;
import com.nxcomm.blinkhd.ui.CameraDetailSettingActivity2.RemoveCameraTask;
import com.nxcomm.blinkhd.ui.customview.CameraListSquare;
import com.nxcomm.blinkhd.ui.customview.TabbedMenuItem;
import com.nxcomm.jstun_android.RtspStunBridgeService;
import com.nxcomm.jstun_android.RtspStunBridgeService.RtspStunBridgeServiceBinder;

import cz.havlena.ffmpeg.ui.CustomHorizontalScrollView;
import cz.havlena.ffmpeg.ui.CustomVerticalScrollView;

public class HomeScreenActivity extends Activity implements Callback,
        ICameraScanner, LeftSideMenuItemListener, OnClickListener,
        ILiveFragmentCallback
{
	private static final String	             TAG	                     = "mbp";
	private static final long	             VIDEO_TIMEOUT	             = 5 * 60 * 1000;
	private static final long	             MAX_BUFFERING_TIME	         = 30 * 1000;
	private static final long	             MAX_LONG_CLICK_DURATION	 = 1000;
	private static final int	             MAX_CLICK_DISTANCE	         = 15;
	// min
	/* Connection Constant */
	public static final int	                 CONNECTION_MODE_LOCAL_INFRA	= 1;
	public static final int	                 CONNECTION_MODE_REMOTE	     = 2;

	private DirectionFragment	             directionFragment;
	private TalkbackFragment	             talkbackFragment;
	private RecordingFragment	             recordingFragment;
	private MelodyFragment	                 melodyFragment;
	private TemperatureFragment	             tempFragment;
	private LiveFragment	                 liveFragment;
	private EventManagerFragment	         eventFragment;
	private CameraUnavailableFragment	     unavailablefrag;

	private CamChannel[]	                 restored_channels;
	private CamProfile[]	                 restored_profiles;
	private CamChannel	                     selected_channel;
	private int	                             access_mode;

	private int	                             video_width	             = 0;
	private int	                             video_height	             = 0;

	private static int	                     FRAME_LAYOUT_MELODY_ID	     = 1;

	private int	                             currentMelodyIndx	         = 0;

	private AnimationDrawable	             anim	                     = null;

	private ViewRemoteCamViaWowzaRequestTask	viewUpnpCamTask;
	private LocalScanForCameras	             scan_task;

	private boolean	                         ACTIVITY_HAS_STOPPED;
	private boolean	                         isActivityDestroyed	     = false;
	private TabbedMenuItem	                 textViewNow, textViewEarlier;
	private ActionBarDrawerToggle	         mDrawerToggle;
	private DrawerLayout	                 mDrawerLayout;
	private PowerManager.WakeLock	         wl	                         = null;
	private Timer	                         remoteVideoTimer	         = null;

	private boolean	                         isEalierTabClicked	         = false;

	/* for debug */
	private boolean	                         isDebugEnabled	             = false;
	private boolean	                         isAdaptiveBitrateEnabled	 = false;
	private boolean	                         isVideoTimeoutEnabled	     = true;

	private RetreiveCameraListTask	         task;
	private SparseArray<CamProfile>	         cameras	                 = null;
	private CameraListSquare	             camListSquare	             = null;
	private ImageButton	                     imgButtonShowCamList	     = null;
	private View	                         mainRootView	             = null;
	private FrameLayout	                     liveLayout	                 = null;
	private LinearLayout	                 sw600dpOutContainter	     = null;
	private int	                             leftMargin	                 = 0;
	private EventManagerFragment	         eventManager2	             = null;
	private FrameLayout	                     fragmentHolder	             = null;
	private boolean	                         isTablet	                 = false;
	private RelativeLayout	                 landscapeTimeline	         = null;
	private View	                         dividerOnTablet	         = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

		super.onCreate(savedInstanceState);
		isTablet = getResources().getBoolean(R.bool.isTablet);
		// getActionBar().setBackgroundDrawable(new
		// ColorDrawable(Color.argb(180, 0, 0, 0)));

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings
		        .getString(PublicDefine.PREFS_SAVED_SERVER_URL,
		                "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;

		Log.i("SETUP", "C");
		// set the content view
		setContentView(R.layout.main_activity);
		mDrawerLayout = (DrawerLayout) this.findViewById(R.id.drawer_layout);
		sw600dpOutContainter = (LinearLayout) this
		        .findViewById(R.id.sw600dpoutContainter);

		WifiManager w = (WifiManager) getSystemService(WIFI_SERVICE);
		String curr_ssid = w.getConnectionInfo().getSSID();

		SharedPreferences.Editor editor = settings.edit();
		editor.putString(string_currentSSID, curr_ssid);

		// Commit the edits!
		editor.commit();

		// FragmentTransaction fragmentTransact = getFragmentManager()
		// .beginTransaction();

		directionFragment = new DirectionFragment();
		talkbackFragment = new TalkbackFragment();
		recordingFragment = new RecordingFragment();
		melodyFragment = new MelodyFragment();
		tempFragment = new TemperatureFragment();

		liveLayout = (FrameLayout) findViewById(R.id.fragmentHolder1);
		liveLayout.setBackgroundResource(R.color.black);

		ActionBar actBar = getActionBar();

		actBar.setDisplayHomeAsUpEnabled(true);
		actBar.setHomeButtonEnabled(true);

		actBar.setIcon(R.drawable.hubble_s);

		actBar.setTitle("");

		LayoutInflater inflator = (LayoutInflater) this
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.custom_actionbar_title_view, null);

		// if you need to customize anything else about the text, do it here.
		// I'm using a custom TextView with a custom font in my layout xml so
		// all I need to do is set title
		TextView titleView = (TextView) v.findViewById(R.id.title);

		// Typeface face = Typeface.createFromAsset(getAssets(),
		// "fonts/ProximaNova-Semibold.otf");

		titleView.setTypeface(FontManager.fontSemiBold);
		titleView.setText(this.getTitle());

		// assign the view to the actionbar
		this.getActionBar().setCustomView(v);

		isActivityDestroyed = false;

		camListSquare = (CameraListSquare) this
		        .findViewById(R.id.cameraListSquare1);

		camListSquare.setVisibility(View.INVISIBLE);

		imgButtonShowCamList = (ImageButton) this
		        .findViewById(R.id.imgButtonShowCamList);
		imgButtonShowCamList.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
				{
					if (camListSquare.isShown())
					{
						camListSquare.setVisibility(View.INVISIBLE);
						imgButtonShowCamList.setVisibility(View.INVISIBLE);
					}
					else
					{
						camListSquare.setVisibility(View.VISIBLE);
					}
				}

			}
		});

		mainRootView = this.findViewById(R.id.main_activity_root_layout);

		if (mainRootView != null)
		{
			TypedValue tv = new TypedValue();
			int actionBarHeightInPixel = 0;

			if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
			        true))
			{
				actionBarHeightInPixel = TypedValue
				        .complexToDimensionPixelSize(tv.data, getResources()
				                .getDisplayMetrics());
			}

			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				mainRootView.setPadding(0, 0, 0, 0);

				imgButtonShowCamList
				        .setPadding(0, actionBarHeightInPixel, 0, 0);
			}
			else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				imgButtonShowCamList.setPadding(0, 0, 0, 0);
				mainRootView.setPadding(0, actionBarHeightInPixel, 0, 0);
			}
		}
		if (getResources().getBoolean(R.bool.isTablet))
		{
			if (sw600dpOutContainter != null)
			{
				Object layoutParams = sw600dpOutContainter.getLayoutParams();

				if (layoutParams instanceof DrawerLayout.LayoutParams)
				{
					DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) layoutParams;
					leftMargin = params.leftMargin;
				}
				else if (layoutParams instanceof LinearLayout.LayoutParams)
				{
					LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layoutParams;
					leftMargin = params.leftMargin;
				}
			}
		}

		fragmentHolder = (FrameLayout) findViewById(R.id.fragmentHolder);
		landscapeTimeline = (RelativeLayout) findViewById(R.id.landscapeTimeLine);
		dividerOnTablet = findViewById(R.id.newLine);
		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.GONE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.GONE);
			}
		}

		RelativeLayout video_view = (RelativeLayout) findViewById(R.id.liveFragment);
		if (video_view != null)
		{
			Object layoutParams = video_view.getLayoutParams();
			Log.i("mbp", "LiveLayout param style "
			        + layoutParams.getClass().getName());

			if (layoutParams instanceof RelativeLayout.LayoutParams)
			{
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layoutParams;

				DisplayMetrics displaymetrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(
				        displaymetrics);
				int height = displaymetrics.heightPixels;
				int width = displaymetrics.widthPixels;
				if (width < height)
				{
					params.width = width;
					params.height = (int) (params.width / 1.78f);
					Log.i("mbp", "SIZE " + params.width + " x " + params.height);
					video_view.setLayoutParams(params);
				}
			}

		}

	}

	private void save_session_data()
	{
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, true);
		editor.commit();
		SetupData savedData = new SetupData(access_mode, "NA",
		        restored_channels, restored_profiles);
		savedData.save_session_data(getExternalFilesDir(null));
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
		editor.commit();
	}

	/* restore functions */
	private boolean restore_session_data()
	{

		/*
		 * 20120223: save some session data like melody, vox etc.. because after
		 * restore these data is erased
		 */
		//CamChannel[] temp = restored_channels;

		SetupData savedData = new SetupData();
		try
		{
			if (savedData.restore_session_data(getExternalFilesDir(null)))
			{
				access_mode = savedData.get_AccessMode();
				restored_channels = savedData.get_Channels();
				restored_profiles = savedData.get_CamProfiles();

//				if (temp != null)
//				{
//					for (int i = 0; i < temp.length; i++)
//					{
//						if (temp[i] != null)
//						{
//							for (int j = 0; j < restored_channels.length; j++)
//							{
//								if (restored_channels[j] != null)
//								{
//									CamChannel.copySessionData(temp[i],
//									        restored_channels[j]);
//								}
//							}
//						}
//					}
//				}

				return true;
			}
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}
		return false;

	}

	public static int	SHOULD_EXIT_NOW	    = 0x0000FFFF;
	public static int	SHOULD_EXIT_NOW_YES	= 0x0001FFFF;

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == SHOULD_EXIT_NOW)
		{
			if (resultCode == SHOULD_EXIT_NOW_YES)
			{
				finish();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
			if (scan_task != null
			        && scan_task.getScanStatus() != LocalScanForCameras.SCAN_CAMERA_FINISHED)
			{
				scan_task.stopScan();
			}

			Intent intent = new Intent(this, SettingsActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra(SettingsActivity.EXTRA_SELECTED_CHANNEL,
			        selected_channel);
			startActivityForResult(intent, SHOULD_EXIT_NOW);
			overridePendingTransition(R.anim.ac_slide_in_from_left,
			        R.anim.ac_slide_out_left_to_right);

			return true;
			// DON'T CALL ANY MORE, HANDLE NOW, EARLIER TAB FROM onClick event
		case R.id.btnLive:
		{

			return true;
		}
		case R.id.btnEalier:
		{

			return true;
		}

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_MENU)
		{
			showDebugOptions();
		}
		// else if (keyCode == KeyEvent.KEYCODE_BACK)
		// {
		// onBackPressed();
		// }

		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onBackPressed()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(
		        HomeScreenActivity.this);
		builder.setMessage(R.string.do_you_want_to_exit)
		        .setPositiveButton(R.string.No,
		                new DialogInterface.OnClickListener()
		                {
			                public void onClick(DialogInterface dialog, int id)
			                {

			                }
		                })
		        .setNegativeButton(R.string.Yes,
		                new DialogInterface.OnClickListener()
		                {
			                public void onClick(DialogInterface dialog, int id)
			                {
				                HomeScreenActivity.this.finish();
			                }
		                });

		AlertDialog confirmExitDialog = builder.create();
		confirmExitDialog.setIcon(R.drawable.hubblehome);
		confirmExitDialog.setTitle(R.string.app_name);
		confirmExitDialog.setCancelable(true);
		confirmExitDialog.show();
	}

	private void showDebugOptions()
	{
		final Dialog dialog = new Dialog(this, R.style.myDialogTheme);
		dialog.setContentView(R.layout.dialog_debug_options);
		dialog.setCancelable(true);

		ListView debugList = (ListView) dialog.findViewById(R.id.listDebugOpts);
		ArrayAdapter<String> debugAdapter = new ArrayAdapter<String>(this,
		        R.layout.dialog_debug_option_items);
		String[] items = getResources().getStringArray(R.array.debug_options);
		if (items != null)
		{
			for (int i = 0; i < items.length; i++)
			{
				debugAdapter.add(items[i]);
			}
		}
		debugList.setAdapter(debugAdapter);
		WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
		wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
		dialog.getWindow().setAttributes(wmlp);
		dialog.show();

		SharedPreferences sharedPrefs = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		final String selectedRegID = sharedPrefs.getString(
		        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
		        null);
		final String apiKey = sharedPrefs.getString(
		        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		debugList.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> parent, View v,
			        int position, long id)
			{
				// TODO Auto-generated method stub
				if (position == 0)
				{
					// turn on/off debug info
					if (liveFragment != null && liveFragment.isVisible())
					{
						TextView txtFrameRate = (TextView) findViewById(R.id.textFrameRate);
						TextView txtRes = (TextView) findViewById(R.id.textResolution);
						if (txtFrameRate != null && txtRes != null)
						{
							if (isDebugEnabled == true)
							{
								isDebugEnabled = false;
								txtFrameRate.setVisibility(View.GONE);
								txtRes.setVisibility(View.GONE);
							}
							else
							{
								isDebugEnabled = true;
								txtFrameRate.setVisibility(View.VISIBLE);
								txtRes.setVisibility(View.VISIBLE);
							}
						}

					}

				}
				else if (position == 1)
				{
					// change resolution to 720p
					if (selectedRegID != null && apiKey != null)
					{
						String cmd = PublicDefine.BM_HTTP_CMD_PART
						        + PublicDefine.SET_RESOLUTION_CMD
						        + PublicDefine.SET_RESOLUTION_PARAM_1
						        + PublicDefine.RESOLUTION_720P;
						sendCommand(apiKey, selectedRegID, cmd);
					}
				}
				else if (position == 2)
				{
					// change resolution to 480p
					if (selectedRegID != null && apiKey != null)
					{
						String cmd = PublicDefine.BM_HTTP_CMD_PART
						        + PublicDefine.SET_RESOLUTION_CMD
						        + PublicDefine.SET_RESOLUTION_PARAM_1
						        + PublicDefine.RESOLUTION_480P;
						sendCommand(apiKey, selectedRegID, cmd);
					}
				}
				else if (position == 3)
				{
					// change resolution to 360p
					if (selectedRegID != null && apiKey != null)
					{
						String cmd = PublicDefine.BM_HTTP_CMD_PART
						        + PublicDefine.SET_RESOLUTION_CMD
						        + PublicDefine.SET_RESOLUTION_PARAM_1
						        + PublicDefine.RESOLUTION_360P;
						sendCommand(apiKey, selectedRegID, cmd);
					}
				}
				else if (position == 4)
				{
					if (selectedRegID != null && apiKey != null)
					{
						String mode = "on";
						if (isAdaptiveBitrateEnabled)
						{
							isAdaptiveBitrateEnabled = false;
							mode = "off";
						}
						else
						{
							isAdaptiveBitrateEnabled = true;
							mode = "on";
						}

						String cmd = PublicDefine.BM_HTTP_CMD_PART
						        + PublicDefine.SET_ADAPTIVE_BITRATE
						        + PublicDefine.SET_ADAPTIVE_BITRATE_PARAM
						        + mode;
						sendCommand(apiKey, selectedRegID, cmd);
					}
				}
				else if (position == 5)
				{
					String cmd = PublicDefine.BM_HTTP_CMD_PART
					        + PublicDefine.SET_VIDEO_BITRATE
					        + PublicDefine.SET_VIDEO_BITRATE_PARAM + "800";
					sendCommand(apiKey, selectedRegID, cmd);
				}
				else if (position == 6)
				{
					String cmd = PublicDefine.BM_HTTP_CMD_PART
					        + PublicDefine.SET_VIDEO_BITRATE
					        + PublicDefine.SET_VIDEO_BITRATE_PARAM + "600";
					sendCommand(apiKey, selectedRegID, cmd);
				}
				else if (position == 7)
				{
					String cmd = PublicDefine.BM_HTTP_CMD_PART
					        + PublicDefine.SET_VIDEO_BITRATE
					        + PublicDefine.SET_VIDEO_BITRATE_PARAM + "400";
					sendCommand(apiKey, selectedRegID, cmd);
				}
				else if (position == 8)
				{
					String cmd = PublicDefine.BM_HTTP_CMD_PART
					        + PublicDefine.SET_VIDEO_BITRATE
					        + PublicDefine.SET_VIDEO_BITRATE_PARAM + "200";
					sendCommand(apiKey, selectedRegID, cmd);
				}
				else if (position == 9)
				{
					if (isVideoTimeoutEnabled == true)
					{
						isVideoTimeoutEnabled = false;

						if (remoteVideoTimer != null)
						{
							Log.d("mbp", "cancel current VideoTimeoutTask");
							remoteVideoTimer.cancel();
							remoteVideoTimer = null;
						}
					}
					else
					{
						isVideoTimeoutEnabled = true;

						if (remoteVideoTimer != null)
						{
							Log.d("mbp", "cancel current VideoTimeoutTask");
							remoteVideoTimer.cancel();
							remoteVideoTimer = null;
						}

						remoteVideoTimer = new Timer();
						remoteVideoTimer.schedule(new VideoTimeoutTask(),
						        VIDEO_TIMEOUT);
					}
				}

				dialog.dismiss();
			}
		});
	}

	private void sendCommand(String apiKey, String deviceID, String command)
	{
		SendCommandAsyncTask sendCommandTask = new SendCommandAsyncTask(
		        new IAsyncTaskCommonHandler()
		        {

			        @Override
			        public void onPreExecute()
			        {
				        // TODO Auto-generated method stub

			        }

			        @Override
			        public void onPostExecute(Object result)
			        {
				        // TODO Auto-generated method stub

			        }

			        @Override
			        public void onCancelled()
			        {
				        // TODO Auto-generated method stub

			        }
		        });

		sendCommandTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
				apiKey, deviceID, command);

	}

	private void prepareToViewCameraLocally()
	{
		Log.d("mbp", "prepare to view local camera...");
		viewRelayRtmp = false;
		viewRtspStun = false;
		device_ip = selected_channel.getCamProfile().get_inetAddress()
		        .getHostAddress();
		device_port = selected_channel.getCamProfile().get_port();

		filePath = String.format("rtsp://user:pass@%s:%d/blinkhd", device_ip,
		        6667);

		openFFmpegFragment();
	}

	private boolean	viewRelayRtmp	= false;

	private void prepareToViewCameraViaRelay()
	{
		stun_retries = 0;
		viewRelayRtmp = true;
		Log.d("mbp", "prepare to view relay camera...");

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String saved_token = settings.getString(
		        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

		String clientType = "browser";
		viewUpnpCamTask = new ViewRemoteCamViaWowzaRequestTask(
		        new Handler(this), this);

		selected_channel.getCamProfile().setRemoteCommMode(
		        StreamerFactory.STREAM_MODE_HTTP_REMOTE);
		selected_channel.setCurrentViewSession(CamChannel.REMOTE_RELAY_VIEW);
		selected_channel.setViewReqState(viewUpnpCamTask);
		viewUpnpCamTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
		        selected_channel.getCamProfile().getRegistrationId(),
		        saved_token, clientType);
	}

	protected void onStop()
	{
		super.onStop();

		userWantToCancel = true;
		ACTIVITY_HAS_STOPPED = true;

		stopAllThread();

		if (wl != null && wl.isHeld())
		{
			wl.release();
			wl = null;
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		if (remoteVideoTimer != null)
		{
			Log.d("mbp", "cancel current VideoTimeoutTask");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}

		if (scan_task != null
		        && scan_task.getScanStatus() == LocalScanForCameras.SCAN_CAMERA_PROGRESS)
		{
			/* if it's either PENDING or RUNNING */
			scan_task.stopScan();
		}

		if (viewUpnpCamTask != null)
		{
			viewUpnpCamTask.cancel(true);
		}

	}

	protected void onStart()
	{
		super.onStart();
		ACTIVITY_HAS_STOPPED = false;
		userWantToCancel = false;

		if (isVideoTimeout == false)
		{
			// process in case of landscape
			FrameLayout fragmentHolder = (FrameLayout) findViewById(R.id.fragmentHolder);

			float landscape_height = TypedValue.applyDimension(
			        TypedValue.COMPLEX_UNIT_DIP, 100, getResources()
			                .getDisplayMetrics());

			RelativeLayout.LayoutParams video_params, controls_params;

			removeAllControlFragment();
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{

				video_params = new RelativeLayout.LayoutParams(
				        ViewGroup.LayoutParams.MATCH_PARENT,
				        ViewGroup.LayoutParams.MATCH_PARENT);
				video_params.addRule(RelativeLayout.CENTER_HORIZONTAL);
				// make FULL Screen
				getWindow().setFlags(
				        WindowManager.LayoutParams.FLAG_FULLSCREEN,
				        WindowManager.LayoutParams.FLAG_FULLSCREEN);

				fragmentHolder.removeAllViews();

				if (ACTIVITY_HAS_STOPPED == false)
				{
					if (eventFragment != null)
					{
						FragmentTransaction fragTransaction = getFragmentManager()
						        .beginTransaction();
						fragTransaction.remove(eventFragment);
						fragTransaction.commitAllowingStateLoss();
					}
				}

				controls_params = new RelativeLayout.LayoutParams(
				        (int) landscape_height, // ViewGroup.LayoutParams.WRAP_CONTENT,
				        (int) landscape_height // ViewGroup.LayoutParams.WRAP_CONTENT
				);

				controls_params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				controls_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

				RelativeLayout liveFragment = (RelativeLayout) findViewById(R.id.liveFragment);
				if (liveFragment != null)
				{
					liveFragment.setLayoutParams(video_params);
				}

				if (isTablet && landscapeTimeline != null)
				{
					Log.i("mbp", "Landscape mode.");
					landscapeTimeline.setVisibility(View.GONE);
					if (dividerOnTablet != null)
					{
						dividerOnTablet.setVisibility(View.GONE);
					}
				}

				RelativeLayout twoFragmentContainer = (RelativeLayout) findViewById(R.id.twoFragmentContainer);
				if (isTablet)
				{
					if (twoFragmentContainer != null)
					{
						twoFragmentContainer.setLayoutParams(controls_params);
					}
				}
				else
				{
					if (fragmentHolder != null)
					{
						fragmentHolder.setLayoutParams(controls_params);
					}
				}

			}

			scanAndViewCamera();

			if (getResources().getBoolean(R.bool.isTablet))
			{
				if (selected_channel != null)
				{
					eventManager2 = (EventManagerFragment) getFragmentManager()
					        .findFragmentById(R.id.eventManagerFragment);
					if (eventManager2 == null || !eventManager2.isInLayout())
					{
						// start new Activity
						Log.i("mbp", "Event manager 2 is NULL");
					}
					else
					{
						eventManager2.setChannel(selected_channel);
					}

					if (selected_channel.getCamProfile().isSharedCam())
					{
						if (landscapeTimeline != null)
						{
							landscapeTimeline.setVisibility(View.GONE);
						}
					}
				}

			}

		}
		else
		{
			// dialog 5 min timeout is showing, do nothing
		}

	}

	public void showRotatingIcon()
	{
		ImageView loader = (ImageView) findViewById(R.id.imgLoader);
		if (loader != null)
		{
			// start waiting animation
			loader.setVisibility(View.VISIBLE);
			loader.setBackgroundResource(R.drawable.loader_anim1);
			anim = (AnimationDrawable) loader.getBackground();
			anim.start();
		}
	}

	public void hideRotatingIcon()
	{
		ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
		if (imgLoader != null)
		{
			imgLoader.clearAnimation();
			imgLoader.setVisibility(View.INVISIBLE);
		}
	}

	public void scanAndViewCamera()
	{
		if (wl == null)
		{
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
			        "TURN ON for first time connect");
			wl.setReferenceCounted(false);
			wl.acquire();
		}
		if (getWindow() != null)
		{
			getWindow()
			        .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		FragmentTransaction fragmentTransact;
		fragmentTransact = getFragmentManager().beginTransaction();
		Fragment unavailableFrag = getFragmentManager().findFragmentByTag(
		        "Unavailable");
		if (unavailableFrag != null)
		{
			fragmentTransact.remove(unavailableFrag);
		}

		fragmentTransact.commitAllowingStateLoss();

		ImageView loader = (ImageView) findViewById(R.id.imgLoader);
		if (loader != null)
		{
			// start waiting animation
			loader.setVisibility(View.VISIBLE);
			loader.setBackgroundResource(R.drawable.loader_anim1);
			anim = (AnimationDrawable) loader.getBackground();
			anim.start();
		}

		if (restore_session_data() == true)
		{

			if (restored_profiles != null && restored_profiles.length > 0)
			{
				SharedPreferences m = this.getSharedPreferences(
				        PublicDefine.PREFS_NAME, 0);
				String saved_mac = m
				        .getString(
				                PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
				                null);
				int selected_channel_number = -1;
				for (int i = 0; i < restored_channels.length; i++)
				{

					if (saved_mac != null)
					{
						String mac = null;
						if (restored_channels[i] != null
						        && restored_channels[i].getCamProfile() != null)
						{
							mac = restored_channels[i].getCamProfile()
							        .getRegistrationId();
						}

						if (mac != null && mac.equalsIgnoreCase(saved_mac))
						{
							selected_channel_number = i;
							break;
						}
					}
				}

				if (selected_channel_number == -1)
				{

					Log.d("mbp", "not found :" + saved_mac);
					// not found
					// clear the entry

					// set default first channel
					if (restored_channels != null
					        && restored_channels.length > 0
					        && restored_channels[0].getCamProfile() != null)
					{
						selected_channel_number = 0;
						SharedPreferences.Editor editor = m.edit();
						editor.putString(
						        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
						        restored_channels[0].getCamProfile()
						                .getRegistrationId());
						editor.commit();
					}

				}

				if (selected_channel_number >= 0)
				{
					selected_channel = restored_channels[selected_channel_number];
					if (selected_channel != null)
					{
						Log.d("mbp", "VIEW CAMERA "
						        + selected_channel.getCamProfile().getName());
						selected_channel
						        .setCamProfile(restored_profiles[selected_channel_number]);

						updateOptionsMenu();

						if ((selected_channel.getCamProfile()
						        .isReachableInRemote() || selected_channel
						        .getCamProfile().isInLocal()))
						{
							// camera is online
							ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
							NetworkInfo wifiInfo = connManager
							        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
							NetworkInfo mobileInfo = connManager
							        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
							if (wifiInfo != null && !wifiInfo.isConnected()
							        && mobileInfo != null
							        && mobileInfo.isConnected())
							{
								Log.d("mbp", "mobile network is enabled, dont need to scan...");
								String saved_token = m.getString(
										PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
								String cmd = PublicDefine.BM_HTTP_CMD_PART
								        + PublicDefine.SET_VIDEO_BITRATE
								        + PublicDefine.SET_VIDEO_BITRATE_PARAM + "200";
								sendCommand(saved_token, 
										selected_channel.getCamProfile().getRegistrationId(), cmd);
								
								FragmentTransaction fragmentTransaction = getFragmentManager()
								        .beginTransaction();
								// refresh event fragment
								if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE
								        && CamProfile.shouldEnableMic(selected_channel.getCamProfile().getModelId()))
								{
									if (!isTablet)
									{
										eventFragment = new EventManagerFragment();
										eventFragment
										        .setChannel(selected_channel);
										fragmentTransaction.replace(
										        R.id.fragmentHolder,
										        eventFragment);
									}
								}
								
								liveFragment = new LiveFragment();
								fragmentTransaction.replace(
								        R.id.fragmentHolder1, liveFragment,
								        "FFMpegPlayer");
								fragmentTransaction.commitAllowingStateLoss();

								prepareToViewCameraViaRelay();
							}
							else
							{
								boolean useRemoteOnly = m.getBoolean(
								        PublicDefine.PREFS_USE_REMOTE_ONLY,
								        false);
								if (useRemoteOnly == false)
								{
									// debug option "Use Remote Only" is No
									scan_task = new LocalScanForCameras(this,
									        this);
									// must call startQuickScan on main thread
									scan_task
									        .startQuickScan(new CamProfile[] { selected_channel
									                .getCamProfile() });
								}
								else
								{
									Log.d("mbp",
									        "local mode is disabled, go to remote view...");
									FragmentTransaction fragmentTransaction = getFragmentManager()
									        .beginTransaction();
									// refresh event fragment
									if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE
									        && CamProfile.shouldEnableMic(selected_channel.getCamProfile().getModelId()))
									{
										if (!isTablet)
										{
											eventFragment = new EventManagerFragment();
											eventFragment
											        .setChannel(selected_channel);
											fragmentTransaction.replace(
											        R.id.fragmentHolder,
											        eventFragment);
										}
									}

									liveFragment = new LiveFragment();
									fragmentTransaction.replace(
									        R.id.fragmentHolder1, liveFragment,
									        "FFMpegPlayer");
									fragmentTransaction
									        .commitAllowingStateLoss();

									prepareToViewCameraViaRelay();
								}
							}

						} // if camera is online
						else
						{
							// camera is offline
							if (CamProfile.shouldEnableMic(selected_channel
							        .getCamProfile().getModelId()))
							{
								onEarlierTabClick();
							}
							else
							{
								Intent intent = new Intent(
								        HomeScreenActivity.this,
								        SettingsActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								intent.putExtra(
								        SettingsActivity.EXTRA_SELECTED_CHANNEL,
								        selected_channel);
								startActivityForResult(intent, SHOULD_EXIT_NOW);
								overridePendingTransition(
								        R.anim.ac_slide_in_from_left,
								        R.anim.ac_slide_out_left_to_right);
							}
						}

					} // if (selected_channel != null)

				} // if (selected_channel_number >= 0)
			}
			else
			{
				// no camera in account
				ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
				imgLoader.clearAnimation();
				imgLoader.setVisibility(View.INVISIBLE);

				if (scan_task != null
				        && scan_task.getScanStatus() != LocalScanForCameras.SCAN_CAMERA_FINISHED)
				{
					scan_task.stopScan();
				}

				Intent intent = new Intent(this, SettingsActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(SettingsActivity.EXTRA_SELECTED_CHANNEL,
				        selected_channel);
				startActivity(intent);
				overridePendingTransition(R.anim.ac_slide_in_from_left,
				        R.anim.ac_slide_out_left_to_right);
				finish();
				// Intent intent = new Intent(this, SettingsActivity.class);
				// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// intent.putExtra(SettingsActivity.EXTRA_SELECTED_CHANNEL,
				// selected_channel);
				// startActivityForResult(intent, SHOULD_EXIT_NOW);
				// overridePendingTransition(R.anim.ac_slide_in_from_left,
				// R.anim.ac_slide_out_left_to_right);
			}

		} // if (restore_session_data() == true)
		else
		{
			// no offline data
		}

	}

	@Override
	protected void onResume()
	{
		super.onResume();

		if (isVideoTimeout == false)
		{
			if (wl == null)
			{
				PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
				        "Now Wake Lock");
				wl.acquire();
			}
		}

		if (getWindow() != null)
		{
			getWindow()
			        .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	@Override
	protected void onPause()
	{
		if (wl != null && wl.isHeld())
		{
			wl.release();
			wl = null;
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		super.onPause();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);

		// get action bar height in pixel
		TypedValue tv = new TypedValue();
		int actionBarHeightInPixel = 0;

		if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
			actionBarHeightInPixel = TypedValue.complexToDimensionPixelSize(
			        tv.data, getResources().getDisplayMetrics());
		}

		FrameLayout fragmentHolder = (FrameLayout) findViewById(R.id.fragmentHolder);

		float portrait_height = TypedValue.applyDimension(
		        TypedValue.COMPLEX_UNIT_DIP, 180, getResources()
		                .getDisplayMetrics());

		// detect large screen height > 480dp
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		display.getSize(size);

		if (size.x > 480 * metrics.density)
		{
			portrait_height = TypedValue.applyDimension(
			        TypedValue.COMPLEX_UNIT_DIP, 360, getResources()
			                .getDisplayMetrics());
		}
		// end of detect large screen

		float landscape_height = TypedValue.applyDimension(
		        TypedValue.COMPLEX_UNIT_DIP, 100, getResources()
		                .getDisplayMetrics());

		RelativeLayout.LayoutParams video_params, controls_params;

		removeAllControlFragment();

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			video_params = new RelativeLayout.LayoutParams(
			        ViewGroup.LayoutParams.MATCH_PARENT,
			        ViewGroup.LayoutParams.MATCH_PARENT);
			video_params.addRule(RelativeLayout.CENTER_IN_PARENT);
			// make FULL Screen
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
			fragmentHolder.removeAllViews();

			if (ACTIVITY_HAS_STOPPED == false)
			{
				if (eventFragment != null)
				{
					FragmentTransaction fragTransaction = getFragmentManager()
					        .beginTransaction();
					fragTransaction.remove(eventFragment);
					fragTransaction.commitAllowingStateLoss();
				}
			}

			controls_params = new RelativeLayout.LayoutParams(
			        (int) landscape_height, // ViewGroup.LayoutParams.WRAP_CONTENT,
			        (int) landscape_height // ViewGroup.LayoutParams.WRAP_CONTENT
			);

			controls_params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			controls_params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

			if (mainRootView != null)
			{
				mainRootView.setPadding(0, 0, 0, 0);

			}

			if (isTablet && landscapeTimeline != null)
			{
				Log.i("mbp", "Landscape mode.");
				landscapeTimeline.setVisibility(View.GONE);
			}
		}
		else
		{

			if (mainRootView != null)
			{
				mainRootView.setPadding(0, actionBarHeightInPixel, 0, 0);

			}

			if (imgButtonShowCamList != null)
			{
				imgButtonShowCamList.setVisibility(View.INVISIBLE);
				if (camListSquare != null)
				{
					camListSquare.setVisibility(View.INVISIBLE);
				}
			}
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			video_params = new RelativeLayout.LayoutParams(
			        ViewGroup.LayoutParams.MATCH_PARENT, (int) portrait_height);

			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;
			if (width < height)
			{
				video_params.width = width;
				video_params.height = (int) (video_params.width / 1.777778f);
				Log.i("mbp", "SIZE " + video_params.width + " x "
				        + video_params.height);
				// video_view.setLayoutParams(params);
				// video_params = params;
			}

			fragmentHolder.removeAllViews();
			if (ACTIVITY_HAS_STOPPED == false
			        && eventFragment != null
			        && selected_channel != null
			        && CamProfile.shouldEnableMic(selected_channel
			                .getCamProfile().getModelId()))
			{
				FragmentTransaction fragTransaction = getFragmentManager()
				        .beginTransaction();
				eventFragment.setChannel(selected_channel);
				fragTransaction.replace(R.id.fragmentHolder, eventFragment);
				fragTransaction.commitAllowingStateLoss();
			}

			if (isEalierTabClicked == false)
			{
				controls_params = new RelativeLayout.LayoutParams(
				        ViewGroup.LayoutParams.MATCH_PARENT,
				        ViewGroup.LayoutParams.MATCH_PARENT);
				controls_params
				        .addRule(RelativeLayout.BELOW, R.id.liveFragment);
				controls_params.addRule(RelativeLayout.CENTER_IN_PARENT);
			}
			else
			{
				controls_params = new RelativeLayout.LayoutParams(
				        ViewGroup.LayoutParams.MATCH_PARENT,
				        ViewGroup.LayoutParams.MATCH_PARENT);
				controls_params
				        .addRule(RelativeLayout.BELOW, R.id.ealierBtngrp);
				controls_params.addRule(RelativeLayout.CENTER_IN_PARENT);
			}
			if (isTablet)
			{
				if (fragmentHolder != null)
				{
					fragmentHolder.setVisibility(View.GONE);
				}
				if (landscapeTimeline != null
				        && !selected_channel.getCamProfile().isSharedCam())
				{
					landscapeTimeline.setVisibility(View.VISIBLE);
				}
			}
		}

		RelativeLayout liveFragment = (RelativeLayout) findViewById(R.id.liveFragment);
		if (liveFragment != null)
		{
			liveFragment.setLayoutParams(video_params);
			recalcDefaultScreenSize();
			resizeFFmpegView(1f);
		}

		RelativeLayout twoFragmentContainer = (RelativeLayout) findViewById(R.id.twoFragmentContainer);
		if (isTablet && twoFragmentContainer != null)
		{
			twoFragmentContainer.setLayoutParams(controls_params);
		}
		else
		{
			if (fragmentHolder != null)
			{
				fragmentHolder.setLayoutParams(controls_params);
			}
		}

		/*
		 * if (getResources().getConfiguration().orientation ==
		 * Configuration.ORIENTATION_PORTRAIT) { RelativeLayout video_view =
		 * (RelativeLayout) findViewById(R.id.liveFragment); if (video_view !=
		 * null) { Object layoutParams = video_view.getLayoutParams();
		 * Log.i("mbp", "LiveLayout param style " +
		 * layoutParams.getClass().getName());
		 * 
		 * if (layoutParams instanceof RelativeLayout.LayoutParams) {
		 * RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
		 * layoutParams;
		 * 
		 * DisplayMetrics displaymetrics = new DisplayMetrics();
		 * getWindowManager().getDefaultDisplay().getMetrics( displaymetrics);
		 * int height = displaymetrics.heightPixels; int width =
		 * displaymetrics.widthPixels; if (width < height) { params.width =
		 * width; params.height = (int) (params.width / 1.777778f); Log.i("mbp",
		 * "SIZE " + params.width + " x " + params.height);
		 * video_view.setLayoutParams(params); // video_params = params; } }
		 * 
		 * } }
		 */
	}

	private void removeAllControlFragment()
	{
		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if (directionFragment != null && directionFragment.isVisible())
		{
			fragTransaction.remove(directionFragment);
		}

		if (talkbackFragment != null && talkbackFragment.isVisible())
		{
			fragTransaction.remove(talkbackFragment);
		}

		if (recordingFragment != null && recordingFragment.isVisible())
		{
			fragTransaction.remove(recordingFragment);
		}

		if (melodyFragment != null && melodyFragment.isVisible())
		{
			fragTransaction.remove(melodyFragment);
		}

		if (tempFragment != null && tempFragment.isVisible())
		{
			fragTransaction.remove(tempFragment);
		}

		// fragTransaction.commit();
		// allow state loss
		fragTransaction.commitAllowingStateLoss();
	}

	private void updateOptionsMenu()
	{
		invalidateOptionsMenu();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub
		if (selected_channel != null
		        && selected_channel.getCamProfile() != null)
		{
			if (selected_channel.getCamProfile().isSharedCam())
			{
				menu.clear();
			}
			else if (!selected_channel.getCamProfile().isInLocal() &&
					!selected_channel.getCamProfile().isReachableInRemote())
			{
				//camera is not available
			}
		}
		
		return true; // super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu)
	{
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.home_sreen_action_bar_button, menu);
		textViewNow = (TabbedMenuItem) menu.findItem(R.id.btnLive)
		        .getActionView();
		textViewNow.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textViewNow.setOnClickListener(this);
		textViewNow.setText(R.string.now);
		textViewNow.setBackgroundResource(R.drawable.menu_item_clicked);
		textViewNow.setChecked(true);

		textViewEarlier = (TabbedMenuItem) menu.findItem(R.id.btnEalier)
		        .getActionView();
		textViewEarlier.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textViewEarlier.setText(R.string.earlier);
		this.textViewEarlier.setBackgroundResource(R.drawable.blank_bg);
		textViewEarlier.setOnClickListener(this);
		this.textViewEarlier.setChecked(false);

		/* 20131221:phung : generate a press event to turn on the marker */
		// textViewNow.postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// ((View) textViewNow).dispatchTouchEvent(MotionEvent.obtain(10,
		// 10, MotionEvent.ACTION_DOWN, 0, 0, 0));
		// ((View) textViewNow).dispatchTouchEvent(MotionEvent.obtain(10,
		// 10, MotionEvent.ACTION_UP, 0, 0, 0));
		// }
		// }, 1000);

		return true;
	}

	@Override
	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();

		if (wl != null && wl.isHeld())
		{
			wl.release();
			wl = null;
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		if (remoteVideoTimer != null)
		{
			Log.d("mbp", "cancel current VideoTimeoutTask");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}

		if (viewUpnpCamTask != null)
		{
			viewUpnpCamTask.cancel(true);
		}

		isActivityDestroyed = true;
	}

	private int	                      currentConnectionMode;
	private String	                  device_ip;
	private int	                      device_port;
	private int	                      device_audio_in_port;
	private BabyMonitorAuthentication	bm_session_auth;
	private String	                  filePath;

	private void setupRemoteCamera(CamChannel s_channel,
	        BabyMonitorAuthentication bm_auth)
	{
		currentConnectionMode = CONNECTION_MODE_REMOTE;

		if (bm_auth != null)
		{
			device_ip = bm_auth.getIP();
			device_port = bm_auth.getPort();
			bm_session_auth = bm_auth;// reserved to used later if we need to
			// restart the videostreamer -audio only
			// mode
		}

		device_audio_in_port = s_channel.getCamProfile().get_ptt_port();
		filePath = s_channel.getStreamUrl();

		new Handler().post(new Runnable()
		{
			public void run()
			{
				openFFmpegFragment();
			}
		});

	}

	private String	stream_mode;

	private void openFFmpegFragment()
	{
		if (filePath == null)
		{
			Log.d(TAG, "Not specified video file");
		}
		else
		{

			if (liveFragment != null)
			{
				new Thread(new Runnable()
				{

					@Override
					public void run()
					{
						// TODO Auto-generated method stub

						runOnUiThread(new Runnable()
						{

							@Override
							public void run()
							{
								if (viewRtspStun == true)
								{
									stream_mode = "S";
								}
								else if (viewRelayRtmp == true)
								{
									stream_mode = "R";
								}
								else
								{
									stream_mode = "L";
								}

								// TODO Auto-generated method stub
								liveFragment.setChannel(selected_channel);
								liveFragment.setStreamUrl(filePath);
							}
						});
					}
				}).start();

			}

		}
	}

	private Timer	bufferingTimer	= null;

	@Override
	public boolean handleMessage(Message msg)
	{
		// TODO Auto-generated method stub
		switch (msg.what)
		{
		case FFMpegPlayer.MSG_MEDIA_STREAM_START_BUFFERING:
			if (bufferingTimer != null)
			{
				bufferingTimer.cancel();
			}

			bufferingTimer = new Timer();
			TimerTask timeOutTask = new TimerTask()
			{

				@Override
				public void run()
				{
					// TODO Auto-generated method stub
					if (userWantToCancel == false)
					{
						if (ACTIVITY_HAS_STOPPED == false)
						{
							Log.d("mbp",
							        "Buffering has been timeout, try to reconnect...");

							if (remoteVideoTimer != null)
							{
								Log.d("mbp", "cancel current VideoTimeoutTask");
								remoteVideoTimer.cancel();
								remoteVideoTimer = null;
							}

							if (selected_channel.getCamProfile().isInLocal())
							{
								videoHasStoppedUnexpectedly();
							}
							else if (selected_channel.getCamProfile()
							        .isReachableInRemote())
							{
								remoteVideoHasStopped(Streamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY);
							}

						}
						else
						{
							Log.d(TAG,
							        "Activity has stopped, do nothing here...");
						}
					}
					else
					{
						finish();
					}
				}
			};
			bufferingTimer.schedule(timeOutTask, MAX_BUFFERING_TIME);
			break;
		case FFMpegPlayer.MSG_MEDIA_STREAM_STOP_BUFFERING:
			if (bufferingTimer != null)
			{
				bufferingTimer.cancel();
				bufferingTimer = null;
			}
			break;
		case FFMpegPlayer.MSG_MEDIA_STREAM_RECORDING_TIME:
			final int time_in_sec = msg.arg1;
			if (recordingFragment != null && recordingFragment.isRecording())
			{
				recordingFragment.updateRecordingTime(time_in_sec);
			}
			break;
		case Streamer.MSG_VIDEO_FPS:
			final int fps = msg.arg1;
			if (liveFragment != null)
			{
				runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						TextView txtFps = (TextView) findViewById(R.id.textFrameRate);
						if (txtFps != null)
						{
							String fps_str = String.format("%s %d",
							        stream_mode, fps);
							txtFps.setText(fps_str);
						}
					}
				});

			}
			break;

		case FFMpegPlayer.MSG_MEDIA_STREAM_LOADING_VIDEO_CANCEL:
			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
					if (imgLoader != null)
					{
						imgLoader.clearAnimation();
						imgLoader.setVisibility(View.INVISIBLE);
					}
				}
			});

			break;
		case FFMpegPlayer.MSG_MEDIA_STREAM_LOADING_VIDEO:
			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					ImageView loader = (ImageView) findViewById(R.id.imgLoader);
					if (loader != null)
					{
						// start waiting animation
						loader.setVisibility(View.VISIBLE);
						loader.setBackgroundResource(R.drawable.loader_anim1);
						anim = (AnimationDrawable) loader.getBackground();
						anim.start();
					}
				}
			});
			break;

		case ViewRemoteCamRequestTask.MSG_VIEW_CAM_SUCCESS:

			if (userWantToCancel == false)
			{
				BabyMonitorAuthentication bm_auth = (BabyMonitorAuthentication) msg.obj;
				if (selected_channel != null)
				{
					if (selected_channel.setStreamingState() == true)
					{

						InetAddress remote_addr;
						try
						{

							remote_addr = InetAddress
							        .getByName(bm_auth.getIP());

							selected_channel.getCamProfile().setInetAddr(
							        remote_addr);
							selected_channel.getCamProfile().setPort(
							        bm_auth.getPort());

							if (bm_auth != null)
							{
								String stream_url = bm_auth.getStreamUrl();
								selected_channel.setStreamUrl(stream_url);
							}

							setupRemoteCamera(selected_channel, bm_auth);

						}
						catch (UnknownHostException e)
						{
							e.printStackTrace();
						}
					}

				}
			} // if (userWantToCancel == false)
			else
			// userWantToCancel == true
			{
				// do nothing
			}

			break;

		case ViewRemoteCamRequestTask.MSG_VIEW_CAM_FALIED:

			int status = msg.arg1;
			int code = msg.arg2;

			Log.d(TAG, "Can't get Session Key status: " + status + " code: "
			        + code);
			viewRelayRtmp = false;
			// cancelVideoStoppedReminder();

			if (userWantToCancel == false)
			{
				TextView message = (TextView) findViewById(R.id.txtCamOne);
				if (message != null)
				{
					message.setVisibility(View.VISIBLE);
				}

				// ImageView imgLoader = (ImageView)
				// findViewById(R.id.imgLoader);
				// if (imgLoader != null && imgLoader.isShown())
				// {
				// imgLoader.clearAnimation();
				// imgLoader.setVisibility(View.INVISIBLE);
				// }

				prepareToViewCameraViaRelay();
			}

			switch (status)
			{

			case 500: // BlinkHD
				// try {
				// showDialog(code);
				// } catch (Exception e1) {
				// e1.printStackTrace();
				// }
				break;

			default:
				// try {
				// showDialog(DIALOG_REMOTE_BM_IS_OFFLINE);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				break;
			}

			break;
		case Streamer.MSG_VIDEO_SIZE_CHANGED:
			video_width = msg.arg1;
			video_height = msg.arg2;
			if (video_width != 0 && video_height != 0)
			{
				ratio = (float) video_width / video_height;
			}
			Log.d(TAG, "Video width, height, ratio: " + video_width + ", "
			        + video_height + ", " + ratio);
			recalcDefaultScreenSize();

			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					resizeFFmpegView(1f);

					TextView txtRes = (TextView) findViewById(R.id.textResolution);
					if (txtRes != null)
					{
						String res_str = String.format("%dx%d", video_width,
						        video_height);
						txtRes.setText(res_str);
					}

					TextView txtFps = (TextView) findViewById(R.id.textFrameRate);
					if (txtFps != null)
					{
						String fps_str = String.format("%s %d", stream_mode, 0);
						txtFps.setText(fps_str);
					}
				}
			});
			break;

		case Streamer.MSG_CAMERA_IS_NOT_AVAILABLE:
		{
			// cancelVideoStoppedReminder();
			if (selected_channel != null)
			{
				if (selected_channel.getCamProfile().isReachableInRemote())
				{
					if (viewRtspStun == true)
					{
						viewRtspStun = false;
						// unregister broadcastreceiver
						LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
						        .getInstance(this);
						localBroadcastManager
						        .unregisterReceiver(stunBridgeBCReciever);

						if (mBound == true)
						{
							/*
							 * Don't expect onServiceDisconnected() gets call
							 * because of this
							 */
							unbindService(mConnection);
							mBound = false;
						}
						// STUN RTSP
						if (mService.isBusy() == true)
						{
							mService.closeViewSession();
						}

					} // if (viewRtspStun == true)

					runOnUiThread(new Runnable()
					{

						@Override
						public void run()
						{
							// TODO Auto-generated method stub

							if (remoteVideoTimer != null)
							{
								Log.d("mbp", "cancel current VideoTimeoutTask");
								remoteVideoTimer.cancel();
								remoteVideoTimer = null;
							}

							prepareToViewCameraViaRelay();
						}
					});
				}
				else
				{
					runOnUiThread(new Runnable()
					{

						@Override
						public void run()
						{
							// TODO Auto-generated method stub

							if (remoteVideoTimer != null)
							{
								Log.d("mbp", "cancel current VideoTimeoutTask");
								remoteVideoTimer.cancel();
								remoteVideoTimer = null;
							}

							scanAndViewCamera();
						}
					});
				}
			}

			break;
		}

		case Streamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY:
		{
			Log.d(TAG, "MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY: retry ...");
			if (userWantToCancel == false)
			{
				if (ACTIVITY_HAS_STOPPED == false)
				{
					if (remoteVideoTimer != null)
					{
						Log.d("mbp", "cancel current VideoTimeoutTask");
						remoteVideoTimer.cancel();
						remoteVideoTimer = null;
					}

					if (selected_channel.getCamProfile().isInLocal())
					{
						videoHasStoppedUnexpectedly();
					}
					else if (selected_channel.getCamProfile()
					        .isReachableInRemote())
					{
						remoteVideoHasStopped(Streamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY);
					}

				}
				else
				{
					Log.d(TAG, "Activity has stopped, do nothing here...");
				}
			}
			else
			{
				finish();
			}

			break;
		}
		case Streamer.MSG_VIDEO_STREAM_HAS_STOPPED:

			if (wl != null && wl.isHeld())
			{
				wl.release();
				wl = null;
			}
			if (getWindow() != null)
			{
				getWindow().clearFlags(
				        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			}

			if (remoteVideoTimer != null)
			{
				Log.d("mbp", "cancel current VideoTimeoutTask");
				remoteVideoTimer.cancel();
				remoteVideoTimer = null;
			}

			break;

		case Streamer.MSG_VIDEO_STREAM_HAS_STARTED:

			// reset stun counter
			if (viewRtspStun == true)
			{
				stun_retries = 0;
			}

			if (!selected_channel.getCamProfile().isInLocal())
			{
				if (remoteVideoTimer != null)
				{
					Log.d("mbp", "cancel current VideoTimeoutTask");
					remoteVideoTimer.cancel();
					remoteVideoTimer = null;
				}

				if (isVideoTimeoutEnabled == true)
				{
					remoteVideoTimer = new Timer();
					remoteVideoTimer.schedule(new VideoTimeoutTask(),
					        VIDEO_TIMEOUT);
				}
			}

			cancelVideoStoppedReminder();
			recalcDefaultScreenSize();

			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					resizeFFmpegView(1f);
					ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
					imgLoader.clearAnimation();
					imgLoader.setVisibility(View.INVISIBLE);

					TextView message = (TextView) findViewById(R.id.txtCamOne);
					if (message != null)
					{
						message.setVisibility(View.INVISIBLE);
					}
				}
			});
			break;

		default:
			break;

		}

		return false;
	}

	private int	stun_retries	= 0;

	private void remoteVideoHasStopped(int reason)
	{
		LiveFragment liveFrag = (LiveFragment) getFragmentManager()
		        .findFragmentByTag("FFMpegPlayer");
		if (liveFrag != null)
		{
			liveFrag.stopStreaming();
		}

		if (userWantToCancel)
		{
			return;
		}

		if (outOfRange == null || !_outOfRange.isAlive())
		{
			Log.d("mbp", "start Reminder now!");
			outOfRange = new VideoOutOfRangeReminder();
			_outOfRange = new Thread(outOfRange, "outOfRange");
			_outOfRange.start();
		}
		else
		{
			Log.d("mbp", "reminder is running... dont start another one");
		}

		displayBG(true);

		switch (reason)
		{
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY:
			this.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{

					Log.d("mbp", "Auto - reconnect remote camera ");
					displayBG(true);

					Intent intent = new Intent(HomeScreenActivity.this
					        .getApplicationContext(),
					        RtspStunBridgeService.class);

					SharedPreferences settings = getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					boolean isStunEnabled = settings.getBoolean(
					        PublicDefine.PREFS_IS_STUN_ENABLED, false);
					// if cannot probe, donot need to try connect via stun
					// anymore
					if (isStunEnabled == true && stun_retries < 2)
					{
						stun_retries++;
						Log.d("mbp", "Rebinding to RTSP service >>>>>>>");
						if (bindService(intent, mConnection,
						        Context.BIND_AUTO_CREATE))
						{
							Log.d("mbp", "Rebind to RTSP service succeeded.");
						}
						else
						{
							Log.d("mbp", "Rebind to RTSP service failed.");
						}
					}
					else
					{
						runOnUiThread(new Runnable()
						{

							@Override
							public void run()
							{
								// TODO Auto-generated method stub
								viewRtspStun = false;

								prepareToViewCameraViaRelay();
							}
						});
					}
				}
			});

			break;
		case VideoStreamer.MSG_VIDEO_STREAM_HAS_STOPPED_FROM_SERVER:

			// Not supported
			break;
		}
	}

	private void cancelVideoStoppedReminder()
	{
		boolean retry = true;
		if (_outOfRange != null && _outOfRange.isAlive())
		{
			Log.d("mbp", "stop alarm tone thread now");
			outOfRange.stop();
			/* try to interrupt with this thread is sleeping */
			_outOfRange.interrupt();
			while (retry)
			{
				try
				{
					_outOfRange.join(5000);

					retry = false;
				}
				catch (InterruptedException e)
				{
				}
			}
			_outOfRange = null;
			outOfRange = null;
		}
	}

	private void displayBG(boolean shouldDisplay)
	{

	}

	private String	                string_currentSSID	= "string_currentSSID";
	private VideoOutOfRangeReminder	outOfRange;
	private Thread	                _outOfRange;

	private void videoHasStoppedUnexpectedly()
	{
		this.runOnUiThread(new Runnable()
		{

			@Override
			public void run()
			{

				// Stop players
				LiveFragment liveFrag = (LiveFragment) getFragmentManager()
				        .findFragmentByTag("FFMpegPlayer");
				if (liveFrag != null)
				{
					liveFrag.stopStreaming();
				}

				// Decide whether Router disconnects or Camera disconnect

				if (userWantToCancel == true)
				{
					return;
				}

				WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);

				SharedPreferences settings = getSharedPreferences(
				        PublicDefine.PREFS_NAME, 0);
				String ssid_no_quote = settings.getString(string_currentSSID,
				        null);

				if (wm.getConnectionInfo() != null
				        && wm.getConnectionInfo().getSSID() != null
				        && wm.getConnectionInfo().getSSID()
				                .equalsIgnoreCase(ssid_no_quote))
				{
					// Still on the same network --> camera down
					Log.d("mbp",
					        "Wifi SSID is still the same, camera is probably down ");

					// re-scann
					scan_task = new LocalScanForCameras(
					        HomeScreenActivity.this, new OneCameraScanner());
					scan_task.setShouldGetSnapshot(false);
					// Callback: updateScanResult()
					// setup scanning for just 1 camera -
					Log.d("mbp", "setup scanning for just 1 camera - ");
					scan_task.startScan(new CamProfile[] { selected_channel
					        .getCamProfile() });

				}
				else
				// Router down
				{
					Log.d("mbp",
					        "Wifi SSID is not the same, Router is probably down ");
					MiniWifiScanUpdater iw = new MiniWifiScanUpdater();
					ws = new WifiScan(HomeScreenActivity.this, iw);
					ws.setSilence(true);
					ws.execute("Scan now");
				}

				if (outOfRange == null || !_outOfRange.isAlive())
				{
					Log.d("mbp", "start Reminder now!");
					outOfRange = new VideoOutOfRangeReminder();
					_outOfRange = new Thread(outOfRange, "outOfRange");
					_outOfRange.start();
				}
				else
				{
					Log.d("mbp",
					        "reminder is running... dont start another one");
				}

				displayBG(true);

			}

		});
	}

	@Override
	public void setupScaleDetector()
	{
		OnScaleGestureListener scaleListener = new OnScaleGestureListener()
		{

			@Override
			public void onScaleEnd(ScaleGestureDetector detector)
			{
				// TODO Auto-generated method stub
			}

			@Override
			public boolean onScaleBegin(ScaleGestureDetector detector)
			{
				// TODO Auto-generated method stub
				return true;
			}

			@Override
			public boolean onScale(ScaleGestureDetector detector)
			{
				// TODO Auto-generated method stub
				if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
				{
					float scaleFactor = detector.getScaleFactor();

					scale *= scaleFactor;
					scale = Math.max(1f, Math.min(scale, 5f));
					if (scale == 1f && lastScaleFactor == 1f)
					{
						// donot scale
					}
					else
					{
						resizeFFmpegView(scale);
					}

					if (scale == 1f)
					{
						setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
					}
					else
					{
						int currOrient = getCurrentOrientation();
						setRequestedOrientation(currOrient);
					}
				}
				return true;
			}
		};

		mScaleDetector = new ScaleGestureDetector(this, scaleListener);
	}

	/* use to detect long click */
	private long	pressStartTime;
	private float	pressedX;
	private float	pressedY;

	@Override
	public void setupOnTouchEvent()
	{
		// TODO Auto-generated method stub
		RelativeLayout mMovieView = (RelativeLayout) findViewById(R.id.content_frame);
		if (mMovieView != null)
		{
			mMovieView.setOnTouchListener(new OnTouchListener()
			{

				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					// TODO Auto-generated method stub
					float curX, curY;
					// IF videorecording is going on.. ignore these event
					// RelativeLayout recMenu = (RelativeLayout)
					// findViewById(R.id.rec_menu);
					final CustomVerticalScrollView vScroll = (CustomVerticalScrollView) findViewById(R.id.vscroll);
					final CustomHorizontalScrollView hScroll = (CustomHorizontalScrollView) findViewById(R.id.hscroll);
					if (event.getPointerCount() > 1)
					{
						mScaleDetector.onTouchEvent(event);
					}
					else
					{
						final int action = event.getAction();
						switch (action & MotionEvent.ACTION_MASK)
						{
						case MotionEvent.ACTION_DOWN:
							pressStartTime = System.currentTimeMillis();
							pressedX = event.getX();
							pressedY = event.getY();
							mx = event.getX();
							my = event.getY();
							if (scale == 1f)
							{
								liveFragment
								        .setOnGotoFullScreen(new IGotoFullScreenHandler()
								        {

									        @Override
									        public void onGotoFullScreen()
									        {
										        if (getResources()
										                .getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
										        {
											        imgButtonShowCamList
											                .setVisibility(View.INVISIBLE);
											        camListSquare
											                .setVisibility(View.INVISIBLE);
										        }
									        }
								        });

								if (liveFragment.isFullScreen() == true)
								{
									liveFragment.showSideMenusAndStatus();
									if (imgButtonShowCamList != null)
									{
										if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
										{
											imgButtonShowCamList
											        .setVisibility(View.VISIBLE);
										}
										else
										{
											imgButtonShowCamList
											        .setVisibility(View.INVISIBLE);
										}
									}
									liveFragment.setFullScreen(false);
								}
								else
								{
									liveFragment.goToFullScreen();
								}
							}

							if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
							{
								removeAllControlFragment();
							}

							break;
						case MotionEvent.ACTION_UP:
							liveFragment.tryToGoToFullScreen();
							long pressDuration = System.currentTimeMillis()
							        - pressStartTime;
							if (distance(pressedX, pressedY, event.getX(),
							        event.getY()) < MAX_CLICK_DISTANCE
							        && pressDuration > MAX_LONG_CLICK_DURATION)
							{
								showDebugOptions();
							}
							break;

						case MotionEvent.ACTION_MOVE:
							curX = event.getX();
							curY = event.getY();
							vScroll.smoothScrollBy((int) (mx - curX),
							        (int) (my - curY));
							hScroll.smoothScrollBy((int) (mx - curX),
							        (int) (my - curY));
							mx = curX;
							my = curY;
							break;
						}
					}

					return true;
				}
			});

		}
	}

	private float distance(float x1, float y1, float x2, float y2)
	{
		float dx = x1 - x2;
		float dy = y1 - y2;
		float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
		return pxToDp(distanceInPx);
	}

	private float pxToDp(float px)
	{
		return px / getResources().getDisplayMetrics().density;
	}

	private int getCurrentOrientation()
	{
		int ret = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
		int rotation = getWindowManager().getDefaultDisplay().getRotation();
		int orientation = getResources().getConfiguration().orientation;

		if ((orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT)
		{
			if (rotation == Surface.ROTATION_0
			        || rotation == Surface.ROTATION_270)
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
			}
			else
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
			}
		}
		else if ((orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (rotation == Surface.ROTATION_0
			        || rotation == Surface.ROTATION_90)
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
			}
			else
			{
				ret = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
			}
		}

		return ret;
	}

	private void resizeFFmpegView(float scaleFactor)
	{
		if (ratio != 0)
		{
			if (Math.abs(scaleFactor - lastScaleFactor) >= 0.03
			        || scaleFactor == 1f)
			{
				lastScaleFactor = scaleFactor;
				FFMpegMovieViewAndroid mMovieView = (FFMpegMovieViewAndroid) findViewById(R.id.imageVideo);
				RelativeLayout liveView = (RelativeLayout) findViewById(R.id.liveFragment);
				if (mMovieView != null)
				{
					int new_width, new_height;
					if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
					{
						new_width = (int) (default_width * scaleFactor);
						new_height = (int) (default_height * scaleFactor);
					}
					else
					{
						RelativeLayout.LayoutParams live_params = (RelativeLayout.LayoutParams) liveView
						        .getLayoutParams();
						// note: MATCH_PARENT could return -1 for
						// live_params.width or
						// live_params.height
						if (default_screen_height / ratio < live_params.height)
						{
							// use full height of screen size
							new_width = default_screen_height;
							new_height = (int) (new_width / ratio);
						}
						else
						{
							new_height = live_params.height;
							new_width = (int) (new_height * ratio);
						}
					}

					LayoutParams movie_params = mMovieView.getLayoutParams();

					if (getResources().getBoolean(R.bool.isTablet))
					{
						if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
						{
							movie_params.width = new_width - (40);
							movie_params.height = new_height;

							if (sw600dpOutContainter != null)
							{
								try
								{
									DrawerLayout.LayoutParams lp = new DrawerLayout.LayoutParams(
									        DrawerLayout.LayoutParams.MATCH_PARENT,
									        DrawerLayout.LayoutParams.MATCH_PARENT);
									lp.setMargins(leftMargin, 0, leftMargin, 0);
									sw600dpOutContainter.setLayoutParams(lp);
								}
								catch (Exception ex)
								{
									ex.printStackTrace();
								}
							}

						}
						else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
						{
							movie_params.width = new_width;
							movie_params.height = new_height;

							if (sw600dpOutContainter != null)
							{
								try
								{
									DrawerLayout.LayoutParams lp = new DrawerLayout.LayoutParams(
									        DrawerLayout.LayoutParams.MATCH_PARENT,
									        DrawerLayout.LayoutParams.MATCH_PARENT);
									lp.setMargins(0, 0, 0, 0);

									sw600dpOutContainter.setLayoutParams(lp);
								}
								catch (Exception ex)
								{
									ex.printStackTrace();
								}

							}

						}
					}
					else
					{
						movie_params.width = new_width;
						movie_params.height = new_height;
					}

					mMovieView.setLayoutParams(movie_params);

					Log.d(TAG, "Surface resized: video_screen_width: "
					        + new_width + ", video_screen_height: "
					        + new_height);

				}

			}
		}
	}

	private float	             mx, my;
	private ScaleGestureDetector	mScaleDetector;
	private float	             scale	         = 1f;
	private float	             lastScaleFactor	= 1f;

	private int	                 default_screen_height;
	private int	                 default_screen_width;
	private int	                 default_width;
	private int	                 default_height;
	private float	             ratio	         = 0;

	private void recalcDefaultScreenSize()
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

		if (displaymetrics.widthPixels > displaymetrics.heightPixels)
		{
			default_screen_height = displaymetrics.heightPixels;
			default_screen_width = displaymetrics.widthPixels;
		}
		else
		{
			default_screen_height = displaymetrics.widthPixels;
			default_screen_width = displaymetrics.heightPixels;
		}

		Log.d(TAG, "Default screen size: default width, default height: "
		        + default_screen_width + ", " + default_screen_height);

		if (ratio != 0)
		{
			if (default_screen_height * ratio > default_screen_width)
			{
				default_width = default_screen_width;
				default_height = (int) (default_width / ratio);
			}
			else
			{
				default_height = default_screen_height;
				default_width = (int) (default_height * ratio);
			}
		}
		Log.d(TAG, "Recalculate default size with ratio " + ratio + ": width: "
		        + default_width + ", height: " + default_height);
	}

	@Override
	public void updateScanResult(ScanProfile[] results, int arg1, int arg2)
	{
		if ((results != null) && (restored_profiles != null)
		        && (restored_profiles.length > 0))
		{
			for (int i = 0; i < restored_profiles.length; i++)
			{
				// Default to offline
				if (restored_profiles[i] == null)
				{
					continue;
				}

				// if camera has updated location and in local --> don't update
				// again
				if (restored_profiles[i].hasUpdatedLocation())
				{
					continue;
				}

				restored_profiles[i].setInLocal(false);
				for (int j = 0; j < results.length; j++)
				{
					if (restored_profiles[i].get_MAC().equalsIgnoreCase(
					        results[j].get_MAC()))
					{

						// ONLINE only when we have a match MAC in local
						restored_profiles[i].setInLocal(true);
						// Update the new IP
						restored_profiles[i].setInetAddr(results[j]
						        .get_inetAddress());
						// Update the new port
						restored_profiles[i].setPort(results[j].get_port());
						// checkVoxStatus(restored_profiles[i]);

						break;
					}
				}

			} // end for

			// 20120608: store data to offline storage
			save_session_data();

		}
		else
		// (results == null) || (restore_profiles ==null) ||
		// restore_profile.len =0
		{
			// NO INFRA camera found
			for (int i = 0; i < restored_profiles.length; i++)
			{
				restored_profiles[i].setInLocal(false);
			}

		}

		final SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PublicDefine.PREFS_CAM_BEING_VIEWED, selected_channel
		        .getCamProfile().get_MAC());
		// Commit the edits!
		editor.commit();

		// go to view camera

		runOnUiThread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				if (ACTIVITY_HAS_STOPPED == false)
				{
					// refresh event fragment
					if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE
					        && CamProfile.shouldEnableMic(selected_channel
					                .getCamProfile().getModelId()))
					{
						if (!isTablet)
						{
							FragmentTransaction fragmentTransact = getFragmentManager()
							        .beginTransaction();
							eventFragment = new EventManagerFragment();
							eventFragment.setChannel(selected_channel);
							fragmentTransact.replace(R.id.fragmentHolder,
							        eventFragment);
							fragmentTransact.commitAllowingStateLoss();
						}
					}

					if (selected_channel.getCamProfile().isInLocal())
					{
						FragmentTransaction fragmentTransact = getFragmentManager()
						        .beginTransaction();
						liveFragment = new LiveFragment();
						fragmentTransact.replace(R.id.fragmentHolder1,
						        liveFragment, "FFMpegPlayer");
						fragmentTransact.commitAllowingStateLoss();
						prepareToViewCameraLocally();
					}
					else if (selected_channel.getCamProfile()
					        .isReachableInRemote())
					{
						FragmentTransaction fragmentTransact = getFragmentManager()
						        .beginTransaction();
						liveFragment = new LiveFragment();
						fragmentTransact.replace(R.id.fragmentHolder1,
						        liveFragment, "FFMpegPlayer");
						fragmentTransact.commitAllowingStateLoss();

						Intent intent = new Intent(
						        HomeScreenActivity.this.getApplicationContext(),
						        RtspStunBridgeService.class);
						boolean isStunEnabled = settings.getBoolean(
						        PublicDefine.PREFS_IS_STUN_ENABLED, false);
						if (isStunEnabled == true)
						{
							Log.d("mbp", "Binding  to RTSP service >>>>>>>");
							if (bindService(intent, mConnection,
							        Context.BIND_AUTO_CREATE))
							{
								Log.d("mbp", "Bind to RTSP service succeeded.");
							}
							else
							{
								Log.d("mbp", "Bind to RTSP service failed.");
							}
						}
						else
						{
							// TODO Auto-generated method stub
							viewRtspStun = false;
							prepareToViewCameraViaRelay();
						}

					}
					else
					// Unavailable
					{
						ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
						if (imgLoader != null && imgLoader.isShown())
						{
							imgLoader.clearAnimation();
							imgLoader.setVisibility(View.INVISIBLE);
						}

						if (CamProfile.shouldEnableMic(selected_channel
						        .getCamProfile().getModelId()))
						{
							onEarlierTabClick();
						}
						else
						{
							Intent intent = new Intent(HomeScreenActivity.this,
							        SettingsActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.putExtra(
							        SettingsActivity.EXTRA_SELECTED_CHANNEL,
							        selected_channel);
							startActivityForResult(intent, SHOULD_EXIT_NOW);
							overridePendingTransition(
							        R.anim.ac_slide_in_from_left,
							        R.anim.ac_slide_out_left_to_right);
						}
					}

					removeAllControlFragment();

				} // if (isActivityDestroyed == false)
			}
		});

	}

	@Override
	public void onPan(boolean isEnable)
	{

		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.VISIBLE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.VISIBLE);
			}
		}

		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (melodyFragment != null && melodyFragment.isVisible())
			{
				fragTransaction.remove(melodyFragment);
			}
		}

		if (isEnable)
		{
			directionFragment.setChannel(selected_channel);
			fragTransaction.replace(R.id.fragmentHolder, directionFragment);
		}
		else
		{
			if (CamProfile.shouldEnableMic(selected_channel.getCamProfile()
			        .getModelId())
			        && (getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) != Configuration.ORIENTATION_LANDSCAPE)
			{
				if (!isTablet)
				{
					eventFragment = new EventManagerFragment();
					eventFragment.setChannel(selected_channel);
					fragTransaction.replace(R.id.fragmentHolder, eventFragment);
				}

			}
			else
			{
				fragTransaction.remove(directionFragment);
			}

			if (isTablet && fragmentHolder != null)
			{
				fragmentHolder.setVisibility(View.GONE);
				if (dividerOnTablet != null)
				{
					dividerOnTablet.setVisibility(View.GONE);
				}
			}
		}

		fragTransaction.commitAllowingStateLoss();
	}

	@Override
	public void onMic(boolean isEnable)
	{
		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.VISIBLE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.VISIBLE);
			}
		}

		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (melodyFragment != null && melodyFragment.isVisible())
			{
				fragTransaction.remove(melodyFragment);
			}
		}

		if (CamProfile.shouldEnableMic(selected_channel.getCamProfile()
		        .getModelId()))
		{

			if (isEnable)
			{
				talkbackFragment.setChannel(selected_channel);
				fragTransaction.replace(R.id.fragmentHolder, talkbackFragment);
			}
			else
			{
				if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) != Configuration.ORIENTATION_LANDSCAPE)
				{
					if (!isTablet)
					{
						eventFragment = new EventManagerFragment();
						eventFragment.setChannel(selected_channel);
						fragTransaction.replace(R.id.fragmentHolder,
						        eventFragment);
					}
				}
				else
				{
					fragTransaction.remove(talkbackFragment);
				}

				if (isTablet && fragmentHolder != null)
				{
					fragmentHolder.setVisibility(View.GONE);

					if (dividerOnTablet != null)
					{
						dividerOnTablet.setVisibility(View.GONE);
					}

				}

			}
			fragTransaction.commitAllowingStateLoss();
		}
	}

	@Override
	public void onRecord(boolean isEnable)
	{
		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.VISIBLE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.VISIBLE);
			}

		}

		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (melodyFragment != null && melodyFragment.isVisible())
			{
				fragTransaction.remove(melodyFragment);
			}
		}

		if (isEnable)
		{
			if (liveFragment != null && liveFragment.isVisible())
			{
				FFMpegMovieViewAndroid mMovieView = (FFMpegMovieViewAndroid) liveFragment
				        .getView().findViewById(R.id.imageVideo);
				if (mMovieView.isRecording())
				{
					recordingFragment.setRecording(true);
				}
			}
			recordingFragment.setChannel(selected_channel);
			fragTransaction.replace(R.id.fragmentHolder, recordingFragment);
		}
		else
		{
			if (CamProfile.shouldEnableMic(selected_channel.getCamProfile()
			        .getModelId())
			        && (getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) != Configuration.ORIENTATION_LANDSCAPE)
			{
				if (!isTablet)
				{
					eventFragment = new EventManagerFragment();
					eventFragment.setChannel(selected_channel);
					fragTransaction.replace(R.id.fragmentHolder, eventFragment);
				}
			}
			else
			{
				fragTransaction.remove(recordingFragment);
			}

			if (isTablet && fragmentHolder != null)
			{
				fragmentHolder.setVisibility(View.GONE);
				if (dividerOnTablet != null)
				{
					dividerOnTablet.setVisibility(View.GONE);
				}
			}

		}
		fragTransaction.commitAllowingStateLoss();

	}

	public void setRecord(boolean enable)
	{
		if (liveFragment != null && liveFragment.isVisible())
		{
			FFMpegMovieViewAndroid mMovieView = (FFMpegMovieViewAndroid) liveFragment
			        .getView().findViewById(R.id.imageVideo);

			mMovieView.startRecord(enable);
		}

	}

	@Override
	public void onMelody(boolean isEnable)
	{
		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.VISIBLE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.VISIBLE);
			}

		}

		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (directionFragment != null && directionFragment.isVisible())
			{
				fragTransaction.remove(directionFragment);
			}

			if (talkbackFragment != null && talkbackFragment.isVisible())
			{
				fragTransaction.remove(talkbackFragment);
			}

			if (recordingFragment != null && recordingFragment.isVisible())
			{
				fragTransaction.remove(recordingFragment);
			}

			if (tempFragment != null && tempFragment.isVisible())
			{
				fragTransaction.remove(tempFragment);
			}
		}

		if (isEnable)
		{
			melodyFragment.setChannel(selected_channel);
			if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_PORTRAIT) == Configuration.ORIENTATION_PORTRAIT)
			{
				fragTransaction.replace(R.id.fragmentHolder, melodyFragment);
			}
			else
			{
				final RelativeLayout home_layout = (RelativeLayout) findViewById(R.id.main_activity_root_layout);
				float height = TypedValue.applyDimension(
				        TypedValue.COMPLEX_UNIT_DIP, 226, getResources()
				                .getDisplayMetrics());
				float bottomMargin = TypedValue.applyDimension(
				        TypedValue.COMPLEX_UNIT_DIP, 66, getResources()
				                .getDisplayMetrics());

				FrameLayout melodyLayout = new FrameLayout(this);
				melodyLayout.setId(FRAME_LAYOUT_MELODY_ID);
				android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) new android.widget.RelativeLayout.LayoutParams(
				        (int) height, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
				params.bottomMargin = (int) bottomMargin;
				melodyLayout.setLayoutParams(params);
				melodyLayout.setVisibility(View.VISIBLE);
				home_layout.addView(melodyLayout);

				fragTransaction.replace(FRAME_LAYOUT_MELODY_ID, melodyFragment);
			}
		}
		else
		{
			if (CamProfile.shouldEnableMic(selected_channel.getCamProfile()
			        .getModelId())
			        && (getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) != Configuration.ORIENTATION_LANDSCAPE)
			{
				if (!isTablet)
				{
					eventFragment = new EventManagerFragment();
					eventFragment.setChannel(selected_channel);
					fragTransaction.replace(R.id.fragmentHolder, eventFragment);
				}
			}
			else
			{
				fragTransaction.remove(melodyFragment);
			}
			if (isTablet && fragmentHolder != null)
			{
				fragmentHolder.setVisibility(View.GONE);
				if (dividerOnTablet != null)
				{
					dividerOnTablet.setVisibility(View.GONE);
				}

			}
		}
		fragTransaction.commitAllowingStateLoss();
	}

	@Override
	public void onTemperature(boolean isEnable)
	{
		if (isTablet && fragmentHolder != null)
		{
			fragmentHolder.setVisibility(View.VISIBLE);
			if (dividerOnTablet != null)
			{
				dividerOnTablet.setVisibility(View.VISIBLE);
			}

		}
		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if ((getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE)
		{
			if (melodyFragment != null && melodyFragment.isVisible())
			{
				fragTransaction.remove(melodyFragment);
			}
		}

		if (isEnable)
		{
			tempFragment.setChannel(selected_channel);
			fragTransaction.replace(R.id.fragmentHolder, tempFragment);
		}
		else
		{

			if (CamProfile.shouldEnableMic(selected_channel.getCamProfile()
			        .getModelId())
			        && (getResources().getConfiguration().orientation & Configuration.ORIENTATION_LANDSCAPE) != Configuration.ORIENTATION_LANDSCAPE)
			{
				if (!isTablet)
				{
					eventFragment = new EventManagerFragment();
					eventFragment.setChannel(selected_channel);
					fragTransaction.replace(R.id.fragmentHolder, eventFragment);
				}
			}
			else
			{
				fragTransaction.remove(tempFragment);
			}
			if (isTablet && fragmentHolder != null)
			{
				fragmentHolder.setVisibility(View.GONE);
				if (dividerOnTablet != null)
				{
					dividerOnTablet.setVisibility(View.GONE);
				}

			}
		}

		fragTransaction.commitAllowingStateLoss();

	}

	private void removeMelodyFragment()
	{
		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if (melodyFragment != null && melodyFragment.isVisible())
		{
			fragTransaction.remove(melodyFragment);
		}

		fragTransaction.commitAllowingStateLoss();
	}

	private void removeDirectionFragment()
	{
		FragmentTransaction fragTransaction = getFragmentManager()
		        .beginTransaction();

		if (directionFragment != null && directionFragment.isVisible())
		{
			fragTransaction.remove(directionFragment);
		}

		fragTransaction.commitAllowingStateLoss();
	}

	private void onNowTabClick()
	{
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		this.textViewNow.setBackgroundResource(R.drawable.menu_item_clicked);
		this.textViewEarlier.setBackgroundResource(R.drawable.blank_bg);
		this.textViewNow.setChecked(true);
		this.textViewEarlier.setChecked(false);

		if (wl == null)
		{
			PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
			        "TURN ON for first time connect");
			wl.setReferenceCounted(false);
			wl.acquire();
		}
		if (getWindow() != null)
		{
			getWindow()
			        .addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		LinearLayout btnGrp = (LinearLayout) findViewById(R.id.ealierBtngrp);
		if (btnGrp != null)
		{
			btnGrp.setVisibility(View.GONE);
		}

		RelativeLayout liveFrame = (RelativeLayout) findViewById(R.id.liveFragment);
		if (liveFrame != null)
		{
			liveFrame.setVisibility(View.VISIBLE);
			if (isEalierTabClicked == true)
			{
				isEalierTabClicked = false;
				scanAndViewCamera();
			}
		}

		if (liveFragment != null)
		{
			liveFragment.goToFullScreen();
		}

	}

	private void onEarlierTabClick()
	{
		isEalierTabClicked = true;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		this.textViewNow.setBackgroundResource(R.drawable.blank_bg);
		this.textViewEarlier.setBackgroundResource(R.drawable.menu_item_clicked);
		this.textViewNow.setChecked(false);
		this.textViewEarlier.setChecked(true);
		
		if (wl != null && wl.isHeld())
		{
			wl.release();
			wl = null;
		}
		if (getWindow() != null)
		{
			getWindow().clearFlags(
			        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		if (remoteVideoTimer != null)
		{
			Log.d("mbp", "cancel current VideoTimeoutTask");
			remoteVideoTimer.cancel();
			remoteVideoTimer = null;
		}

		if (scan_task != null
		        && scan_task.getScanStatus() != LocalScanForCameras.SCAN_CAMERA_FINISHED)
		{
			scan_task.stopScan();
		}

		LinearLayout btnGrp = (LinearLayout) findViewById(R.id.ealierBtngrp);
		if (btnGrp != null)
		{
			btnGrp.setVisibility(View.VISIBLE);

			final FrameLayout eventLayout = (FrameLayout) findViewById(R.id.fragmentHolder);
			RelativeLayout.LayoutParams event_params = new RelativeLayout.LayoutParams(
			        RelativeLayout.LayoutParams.MATCH_PARENT,
			        RelativeLayout.LayoutParams.MATCH_PARENT);
			event_params.addRule(RelativeLayout.BELOW, R.id.ealierBtngrp);

			if (!isTablet)
			{
				eventLayout.setLayoutParams(event_params);
			}
			else
			{
				RelativeLayout twoFragmentContainer = (RelativeLayout) findViewById(R.id.twoFragmentContainer);
				if (twoFragmentContainer != null)
				{
					twoFragmentContainer.setLayoutParams(event_params);
				}
				if (eventLayout != null)
				{
					eventLayout.setVisibility(View.GONE);
				}
				if (dividerOnTablet != null)
				{
					dividerOnTablet.setVisibility(View.GONE);
				}
			}

			final Button timeline = (Button) findViewById(R.id.TimelineBtn);
			timeline.setTypeface(FontManager.fontRegular);

			final Button savedDay = (Button) findViewById(R.id.SavedBtn);
			timeline.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					timeline.setActivated(true);
					timeline.setTextColor(Color.WHITE);
					savedDay.setActivated(false);
					savedDay.setTextColor(Color.DKGRAY);

					if (!isTablet)
					{
						FragmentTransaction fragTrans = getFragmentManager()
						        .beginTransaction();
						eventFragment = new EventManagerFragment();
						eventFragment.setChannel(selected_channel);
						fragTrans.replace(R.id.fragmentHolder, eventFragment);
						fragTrans.commitAllowingStateLoss();
					}

				}
			});

			savedDay.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					savedDay.setActivated(true);
					savedDay.setTextColor(Color.WHITE);
					timeline.setActivated(false);
					timeline.setTextColor(Color.DKGRAY);

					FragmentTransaction fragTrans = getFragmentManager()
					        .beginTransaction();
					SaveDayFragment savedDay = new SaveDayFragment();
					savedDay.setChannel(selected_channel);
					fragTrans.replace(R.id.fragmentHolder, savedDay);
					fragTrans.commitAllowingStateLoss();
				}
			});

			// default show timeline
			// timeline.setActivated(true);
			// timeline.setTextColor(Color.WHITE);
			// savedDay.setActivated(false);
			// savedDay.setTextColor(Color.DKGRAY);
			timeline.performClick();

		}

		if (textViewEarlier != null)
		{
			this.textViewEarlier
			        .setBackgroundResource(R.drawable.menu_item_clicked);
			this.textViewEarlier.setChecked(true);
		}

		if (textViewNow != null)
		{
			this.textViewNow.setBackgroundResource(R.drawable.blank_bg);
			this.textViewNow.setChecked(false);
		}

		RelativeLayout liveFrame = (RelativeLayout) findViewById(R.id.liveFragment);
		if (liveFrame != null)
		{
			liveFrame.setVisibility(View.GONE);
			if (liveFragment != null)
			{
				liveFragment.stopStreaming();
			}
		}

	}

	@Override
	public void onClick(View v)
	{
		if (v.getId() == this.textViewEarlier.getId())
		{
			if (selected_channel != null
			        && CamProfile.shouldEnableMic(selected_channel
			                .getCamProfile().getModelId()))
			{
				onEarlierTabClick();
			}
		}
		else if (v.getId() == this.textViewNow.getId())
		{
			onNowTabClick();
		}
	}

	@Override
	public void onSnap()
	{
		if (video_width != 0 && video_height != 0)
		{
			if (liveFragment != null && liveFragment.isVisible())
			{
				FFMpegMovieViewAndroid mMovieView = (FFMpegMovieViewAndroid) liveFragment
				        .getView().findViewById(R.id.imageVideo);
				mMovieView.getSnapShot(video_width, video_height, true);
			}

		}

	}

	/*
	 * should be sent by server side
	 */
	// private void close_relay_session()
	// {
	// SharedPreferences settings = getSharedPreferences(
	// PublicDefine.PREFS_NAME, 0);
	// String saved_token = settings.getString(
	// PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
	// String regId = selected_channel.getCamProfile().getRegistrationId();
	// String command = PublicDefine.BM_HTTP_CMD_PART
	// + PublicDefine.CLOSE_RELAY_RTMP;
	// Log.d("mbp", "Close relay session, cmd: " + command);
	// int retries = 1;
	// String response = null;
	// while (retries > 0)
	// {
	// response = UDTRequestSendRecv.sendRequest_via_stun2(saved_token,
	// regId, command);
	// if (response != null)
	// {
	// break;
	// }
	// }
	// }

	public void stopAllThread()
	{
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(PublicDefine.PREFS_CAM_BEING_VIEWED);
		// Commit the edits!
		editor.commit();

		// stop recording if any
		if (recordingFragment != null && recordingFragment.isRecording())
		{
			setRecord(false);
		}

		Log.d("mbp",
		        "unregisterReceiver  stunBridgeBCReciever <<<<<<<<<<<<<<<<< ");
		// unregister broadcastreceiver
		LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
		        .getInstance(this);
		localBroadcastManager.unregisterReceiver(stunBridgeBCReciever);

		if (mBound == true)
		{
			Log.d("mbp", "unbindService   RTSP service <<<<<<<<<<<<<<<<< ");

			/*
			 * Don't expect onServiceDisconnected() gets call because of this
			 */
			unbindService(mConnection);

			mBound = false;
		}
		// STUN RTSP
		if (this.viewRtspStun)
		{
			if (mService.isBusy() == true)
			{
				mService.closeViewSession();
			}
		}

		// stop WifiScan task
		if (ws != null && ws.getStatus() == AsyncTask.Status.RUNNING)
		{
			ws.cancel(true);
		}

	}

	// --- RTSP STUN

	// Stun client service
	private RtspStunBridgeService	mService;
	private boolean	              mBound	         = false;
	private boolean	              viewRtspStun	     = false;
	private Thread	              initRtspStunThread	= null;
	private boolean	              userWantToCancel;

	// Add Stun Client Service
	private ServiceConnection	  mConnection	     = new ServiceConnection()
	                                                 {

		                                                 @Override
		                                                 public void onServiceConnected(
		                                                         ComponentName className,
		                                                         IBinder service)
		                                                 {
			                                                 // We've bound to
			                                                 // LocalService,
			                                                 // cast the IBinder
			                                                 // and get
			                                                 // LocalService
			                                                 // instance

			                                                 RtspStunBridgeServiceBinder binder = (RtspStunBridgeServiceBinder) service;
			                                                 mService = binder
			                                                         .getService();

			                                                 Log.d("mbp",
			                                                         "RTSP Service connected. busy? "
			                                                                 + mService
			                                                                         .isBusy());

			                                                 int retry = 20;
			                                                 while (mService
			                                                         .isBusy()
			                                                         && retry-- > 0)
			                                                 {
				                                                 try
				                                                 {
					                                                 Thread.sleep(1000);
				                                                 }
				                                                 catch (InterruptedException ie)
				                                                 {
					                                                 ie.printStackTrace();
				                                                 }

			                                                 }

			                                                 if (mService
			                                                         .isBusy())
			                                                 {
				                                                 Log.e("mbp",
				                                                         "mService is still BUSY!!!");
			                                                 }

			                                                 if (userWantToCancel == true)
			                                                 {
				                                                 Log.d("mbp",
				                                                         " catch  userWantToCancel in onServiceConnected..");
				                                                 return;
			                                                 }

			                                                 mBound = true;

			                                                 if (selected_channel
			                                                         .getCamProfile()
			                                                         .isInLocal())
			                                                 { // local

				                                                 // DO nothing
				                                                 // here..
			                                                 }
			                                                 else
			                                                 {
				                                                 SharedPreferences settings = getSharedPreferences(
				                                                         PublicDefine.PREFS_NAME,
				                                                         0);
				                                                 final Editor editor = settings
				                                                         .edit();

				                                                 editor.putBoolean(
				                                                         PublicDefine.PREFS_SHOULD_USE_LOCAL_WOWZA_SERVER,
				                                                         false);
				                                                 editor.commit();

				                                                 viewRtspStun = true;
				                                                 prepareToViewCameraRtspStun();
			                                                 }
		                                                 }

		                                                 // Will only be called
		                                                 // when the service
		                                                 // crash / killed
		                                                 // NOT by calling
		                                                 // unBindService..
		                                                 @Override
		                                                 public void onServiceDisconnected(
		                                                         ComponentName arg0)
		                                                 {
			                                                 Log.d("mbp",
			                                                         "RTSP Service disconnected.");

		                                                 }
	                                                 };

	private void prepareToViewCameraRtspStun()
	{
		if (userWantToCancel == false)
		{
			// now for P2P RTSP STUN
			if (this.mBound)
			{
				Log.d(TAG, "prepareToViewCameraRtspStun");

				Runnable runnable = new Runnable()
				{
					@Override
					public void run()
					{

						int i = 0;
						while (i++ < 20)
						{
							if (mService.isDiscoveryCompleted()
							        && userWantToCancel == false)
							{
								if (mService.isSymmetricNAT() == false)
								{
									Log.d("mbp",
									        "Not Symmetric NAT, use RTSP via STUN");
									SharedPreferences settings = getSharedPreferences(
									        PublicDefine.PREFS_NAME, 0);
									String saved_token = settings
									        .getString(
									                PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
									                null);
									mService.initViewSession(saved_token,
									        selected_channel.getCamProfile()
									                .getRegistrationId());

									LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
									        .getInstance(HomeScreenActivity.this);
									IntentFilter intent = new IntentFilter();
									intent.addAction(RtspStunBridgeService.ACTION_RTSP_BRIDGE_READY);
									intent.addAction(RtspStunBridgeService.ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED);
									localBroadcastManager.registerReceiver(
									        stunBridgeBCReciever, intent);
								}
								else
								{
									Log.d("mbp", "Symmetric NAT, use relay");
									runOnUiThread(new Runnable()
									{

										@Override
										public void run()
										{
											// TODO Auto-generated method stub
											viewRtspStun = false;
											prepareToViewCameraViaRelay();
										}
									});

								}
								break;
							} // if (mService.isDiscoveryCompleted() &&
							  // userWantToCancel == false)
							else if (userWantToCancel == false) // discovery is
							// not completed
							{
								try
								{
									Thread.sleep(5000);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
								Log.d("mbp",
								        "SymmetricNATChecker task is not completed, retry to check in 5 seconds.");
							}
							else
							// userWantToCancel == true
							{
								break;
							}

						}
					}
				};
				initRtspStunThread = new Thread(runnable);
				initRtspStunThread.start();
			}
			else
			{
				Log.d("mbp", "FFMpegPlayer not binding to RTSP service.");
				viewRtspStun = false;
				prepareToViewCameraViaRelay();
			}
		} // if (userWantToCancel == false)
		else
		{
			finish();
		}
	}

	private BroadcastReceiver	stunBridgeBCReciever	= new BroadcastReceiver()
	                                                 {
		                                                 @Override
		                                                 public void onReceive(
		                                                         Context context,
		                                                         Intent intent)
		                                                 {
			                                                 if (ACTIVITY_HAS_STOPPED == false)
			                                                 {
				                                                 if (intent
				                                                         .getAction()
				                                                         .equalsIgnoreCase(
				                                                                 RtspStunBridgeService.ACTION_RTSP_BRIDGE_READY))
				                                                 {
					                                                 // Stun
					                                                 // bridge
					                                                 // started
					                                                 // to
					                                                 // forward
					                                                 // stream..
					                                                 String resolution = intent
					                                                         .getStringExtra(RtspStunBridgeService.RESOLUTION);
					                                                 String url = PublicDefine
					                                                         .getSDPFilePath(
					                                                                 HomeScreenActivity.this,
					                                                                 resolution);
					                                                 Log.d("mbp",
					                                                         " Try to stream  from url : "
					                                                                 + url);
					                                                 filePath = url;

					                                                 // attach
					                                                 // the
					                                                 // fragment
					                                                 // now. We
					                                                 // have
					                                                 // the URL

					                                                 openFFmpegFragment();

				                                                 }
				                                                 else if (intent
				                                                         .getAction()
				                                                         .equalsIgnoreCase(
				                                                                 RtspStunBridgeService.ACTION_RTSP_BRIDGE_CREATE_SESSION_FALIED))
				                                                 {
					                                                 // Stun
					                                                 // bridge
					                                                 // started
					                                                 // to
					                                                 // forward
					                                                 // stream..
					                                                 Log.d("mbp",
					                                                         " Rcved bridge create session failed. Try to use relay server");
					                                                 if (!mService
					                                                         .isStunPortValid())
					                                                 {
						                                                 Intent it = new Intent(
						                                                         HomeScreenActivity.this,
						                                                         RtspStunBridgeService.class);
						                                                 stopService(it);
						                                                 startService(it);
					                                                 }

					                                                 viewRtspStun = false;
					                                                 prepareToViewCameraViaRelay();

				                                                 }
			                                                 } // if
			                                                   // (ACTIVITY_HAS_STOPPED
			                                                   // == false)
		                                                 }

	                                                 };

	@Override
	public void goToFullScreen()
	{
		// TODO Auto-generated method stub
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			FragmentTransaction fragTrans = getFragmentManager()
			        .beginTransaction();
			if (directionFragment != null)
			{
				fragTrans.remove(directionFragment);
			}

			if (talkbackFragment != null)
			{
				fragTrans.remove(recordingFragment);
			}

			if (melodyFragment != null)
			{
				fragTrans.remove(melodyFragment);
			}

			if (tempFragment != null)
			{
				fragTrans.remove(tempFragment);
			}

			if (recordingFragment != null)
			{
				fragTrans.remove(recordingFragment);
			}

			fragTrans.commitAllowingStateLoss();
		}
	}

	/**
	 * @author phung This is to reconnect just 1 camera after scanning If no
	 *         camera found -- rescan again until we found one or -- sbdy press
	 *         stop
	 */
	private class OneCameraScanner implements ICameraScanner
	{

		@Override
		public void updateScanResult(final ScanProfile[] results, int status,
		        int index)
		{

			if (userWantToCancel)
			{
				// do nothing, exit...
				return;
			}

			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					// TODO Auto-generated method stub
					if (results != null && results.length == 1)
					{
						ScanProfile cp = results[0];
						// update the ip address
						CamProfile seletectProfile = selected_channel
						        .getCamProfile();

						if (seletectProfile.get_MAC().equalsIgnoreCase(
						        results[0].get_MAC()))
						{

							// Update the new IP
							seletectProfile.setInetAddr(results[0]
							        .get_inetAddress());
							seletectProfile.setPort(results[0].get_port());
							seletectProfile.setInLocal(true);

							// restart connecting now..
							prepareToViewCameraLocally();

							return;
						}
					}
					else
					{
						Log.d(TAG, "Failed to find camera via Scan");
					}

					// If the camera is not found -- send
					// the error message to trigger scanning
					// again .
					Handler dummy = new Handler(HomeScreenActivity.this);
					dummy.dispatchMessage(Message.obtain(dummy,
					        Streamer.MSG_VIDEO_STREAM_HAS_STOPPED_UNEXPECTEDLY));
				}
			});

		}
	}

	private WifiScan	ws	= null;

	private class MiniWifiScanUpdater implements IWifiScanUpdater
	{

		@Override
		public void scanWasCanceled()
		{
			// Do nothing here for now
		}

		@Override
		public void updateWifiScanResult(List<ScanResult> results)
		{

			SharedPreferences settings = getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String check_SSID = settings.getString(string_currentSSID, null);
			String check_SSID_w_quote = "\"" + check_SSID + "\"";
			boolean found_in_range = false;
			if (results != null)
			{
				for (ScanResult result : results)
				{
					if ((result.SSID != null)
					        && (result.SSID.equalsIgnoreCase(check_SSID)))
					{
						Log.d("mbp", "found " + check_SSID + " .. in range");
						found_in_range = true;
						break;
					}
				}
			}

			if (found_in_range)
			{
				/* try to connect back to this BSSID */
				WifiManager w = (WifiManager) getSystemService(WIFI_SERVICE);
				List<WifiConfiguration> wcs = w.getConfiguredNetworks();

				Handler.Callback h = new Handler.Callback()
				{

					@Override
					public boolean handleMessage(Message msg)
					{
						switch (msg.what)
						{
						case ConnectToNetwork.MSG_CONNECT_TO_NW_DONE:

							runOnUiThread(new Runnable()
							{

								@Override
								public void run()
								{
									prepareToViewCameraLocally();

								}
							});

							break;
						case ConnectToNetwork.MSG_CONNECT_TO_NW_FAILED:
							// /Scan again
							ws = new WifiScan(HomeScreenActivity.this,
							        new MiniWifiScanUpdater());
							ws.setSilence(true);
							ws.execute("Scan now");

							break;
						}
						return false;
					}
				};

				ConnectToNetwork connect_task = new ConnectToNetwork(
				        HomeScreenActivity.this, new Handler(h));
				connect_task.dontRemoveFailedConnection(true);
				connect_task.setSilence(true);
				connect_task.setIgnoreBSSID(true);
				boolean foundExisting = false;
				for (WifiConfiguration wc : wcs)
				{
					if ((wc.SSID != null)
					        && wc.SSID.equalsIgnoreCase(check_SSID_w_quote))
					{
						// This task will make sure the app is connected to the
						// camera.
						// At the end it will send MSG_CONNECT_TO_NW_DONE
						connect_task.execute(wc);
						foundExisting = true;
						break;
					}
				}

			}
			else
			/* not found the SSID */
			{

				// /Scan again
				ws = new WifiScan(HomeScreenActivity.this,
				        new MiniWifiScanUpdater());
				ws.setSilence(true);
				ws.execute("Scan now");
			}

		}
	}

	/**********************************************************************************************
	 ********************************** PRIVATE CLASSES *******************************************
	 **********************************************************************************************/

	private class VideoOutOfRangeReminder implements Runnable
	{

		private boolean	running;

		public VideoOutOfRangeReminder()
		{
			running = true;
		}

		@Override
		public void run()
		{
			/* Play beep: preparing */
			SharedPreferences settings = HomeScreenActivity.this
			        .getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			boolean shouldPlay = settings.getBoolean(
			        PublicDefine.PREFS_MBP_DISCONNECT_ALERT, true);
			MediaPlayer mMediaPlayer = null;
			if (shouldPlay == true)
			{
				mMediaPlayer = new MediaPlayer();
				String uri = "android.resource://" + getPackageName() + "/"
				        + R.raw.beep;
				try
				{
					mMediaPlayer.setDataSource(HomeScreenActivity.this,
					        Uri.parse(uri));
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

				mMediaPlayer
				        .setAudioStreamType(AudioManager.STREAM_NOTIFICATION);

				try
				{
					mMediaPlayer.prepare();
				}
				catch (IllegalStateException e1)
				{
					mMediaPlayer = null;
					e1.printStackTrace();
				}
				catch (IOException e1)
				{
					mMediaPlayer = null;
					e1.printStackTrace();
				}

			}

			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
			        + ":Beeping in local router mode start");

			while (running)
			{

				HomeScreenActivity.this.runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{

						// if (liveFragment != null)
						// {
						// liveFragment.grayOut();
						// }
						//
						ImageView loader = (ImageView) findViewById(R.id.imgLoader);
						if (loader != null)
						{
							// start waiting animation
							loader.setVisibility(View.VISIBLE);
							loader.setBackgroundResource(R.drawable.loader_anim1);
							anim = (AnimationDrawable) loader.getBackground();
							anim.start();
						}
					}
				});

				try
				{
					Thread.sleep(5500);
				}
				catch (InterruptedException e)
				{
				}

				if (ACTIVITY_HAS_STOPPED == true)
				{
					Log.d("mbp",
					        "Stop Beeping in local coz activity is stopped.");
					break;
				}

				if (shouldPlay == true)
				{
					// 20120511_: move the beeping to after 5 sec
					if (mMediaPlayer != null)
					{
						if (mMediaPlayer.isPlaying())
							mMediaPlayer.stop();

						mMediaPlayer.start();
					}
				}

			} // while running
			Log.d("mbp", "Thread:" + Thread.currentThread().getId()
			        + ":Beeping in local router mode stop");

			if ((mMediaPlayer != null) && mMediaPlayer.isPlaying())
			{
				mMediaPlayer.stop();
			}

			runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{

					ImageView imgLoader = (ImageView) findViewById(R.id.imgLoader);
					imgLoader.clearAnimation();
					imgLoader.setVisibility(View.INVISIBLE);

				}
			});

		}

		public void stop()
		{
			running = false;
		}

	}

	private boolean	isVideoTimeout	= false;

	private class VideoTimeoutTask extends TimerTask
	{
		public void run()
		{
			if (isVideoTimeoutEnabled == true)
			{
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d("mbp",
						        "5 min timeout is up ...start counting down ");

						RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
						if (refreshRoot != null)
						{
							refreshRoot.setVisibility(View.VISIBLE);

							TextView txt = (TextView) findViewById(R.id.textStopping);
							if (txt != null)
							{
								String msg = String.format("%s %d",
								        getString(R.string.stopping_in), 30);
								txt.setText(msg);
							}
						}

						final CountDownTimer ctimer = new CountDownTimer(30,
						        new ITimerUpdater()
						        {

							        @Override
							        public void updateCurrentCount(
							                final int count)
							        {
								        runOnUiThread(new Runnable()
								        {
									        public void run()
									        {
										        TextView txt = (TextView) findViewById(R.id.textStopping);
										        if (txt != null)
										        {
											        String msg = String
											                .format("%s %d",
											                        getString(R.string.stopping_in),
											                        count);
											        txt.setText(msg);
										        }
									        }
								        });
							        }

							        @Override
							        public void timeUp()
							        {
								        // Time is realy up --
								        HomeScreenActivity.this
								                .runOnUiThread(new Runnable()
								                {
									                public void run()
									                {
										                if (liveFragment != null)
										                {
											                liveFragment
											                        .stopStreaming();
										                }

										                if (wl != null
										                        && wl.isHeld())
										                {
											                wl.release();
											                wl = null;
										                }
										                if (getWindow() != null)
										                {
											                getWindow()
											                        .clearFlags(
											                                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
										                }

										                if (HomeScreenActivity.this.remoteVideoTimer != null)
										                {
											                HomeScreenActivity.this.remoteVideoTimer
											                        .cancel();
										                }

										                // TODO Auto-generated
										                // method stub
										                RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
										                if (refreshRoot != null)
										                {
											                refreshRoot
											                        .setVisibility(View.INVISIBLE);
										                }

										                AlertDialog.Builder builder = new AlertDialog.Builder(
										                        HomeScreenActivity.this);
										                Spanned msg = Html
										                        .fromHtml("<big>"
										                                + getResources()
										                                        .getString(
										                                                R.string.the_video_view_session_has_been_timeout_would_you_like_to_continue)
										                                + "</big>");
										                builder.setMessage(msg)
										                        .setCancelable(
										                                false)
										                        .setPositiveButton(
										                                getResources()
										                                        .getString(
										                                                R.string.Yes),
										                                new DialogInterface.OnClickListener()
										                                {
											                                @Override
											                                public void onClick(
											                                        DialogInterface dialog,
											                                        int which)
											                                {
												                                dialog.dismiss();
												                                isVideoTimeout = false;
												                                scanAndViewCamera();

											                                }
										                                })
										                        .setNegativeButton(
										                                getResources()
										                                        .getString(
										                                                R.string.view_another_camera),
										                                new DialogInterface.OnClickListener()
										                                {
											                                @Override
											                                public void onClick(
											                                        DialogInterface dialog,
											                                        int which)
											                                {
												                                dialog.dismiss();
												                                isVideoTimeout = false;
												                                Intent intent = new Intent(
												                                        HomeScreenActivity.this,
												                                        SettingsActivity.class);
												                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
												                                intent.putExtra(
												                                        SettingsActivity.EXTRA_SELECTED_CHANNEL,
												                                        selected_channel);
												                                startActivityForResult(
												                                        intent,
												                                        SHOULD_EXIT_NOW);
												                                overridePendingTransition(
												                                        R.anim.ac_slide_in_from_left,
												                                        R.anim.ac_slide_out_left_to_right);
											                                }
										                                });

										                AlertDialog alert = builder
										                        .create();

										                try
										                {
											                alert.show();
											                isVideoTimeout = true;
										                }
										                catch (Exception e)
										                {
										                }
									                }
								                });

							        }

							        @Override
							        public void timerKick()
							        {
								        // Timer is kicked before timeout
								        // create a new remoteVideoTimer ..
								        if (HomeScreenActivity.this.remoteVideoTimer != null)
								        {
									        HomeScreenActivity.this.remoteVideoTimer
									                .cancel();
								        }

								        runOnUiThread(new Runnable()
								        {

									        @Override
									        public void run()
									        {
										        // TODO Auto-generated method
										        // stub
										        RelativeLayout refreshRoot = (RelativeLayout) findViewById(R.id.refreshLayout);
										        if (refreshRoot != null)
										        {
											        refreshRoot
											                .setVisibility(View.INVISIBLE);
										        }
									        }
								        });

								        HomeScreenActivity.this.remoteVideoTimer = new Timer();
								        HomeScreenActivity.this.remoteVideoTimer
								                .schedule(
								                        new VideoTimeoutTask(),
								                        VIDEO_TIMEOUT);
							        }
						        });

						new Thread(ctimer).start();

						if (refreshRoot != null)
						{
							// setup button now..
							Button refresh = (Button) refreshRoot
							        .findViewById(R.id.refreshBtn);
							if (refresh != null)
							{
								refresh.setOnClickListener(new OnClickListener()
								{

									@Override
									public void onClick(View v)
									{
										ctimer.stop();
									}
								});

							}

						}

					}
				});

			} // if (isVideoTimeoutEnabled == true)
		}
	}
}
