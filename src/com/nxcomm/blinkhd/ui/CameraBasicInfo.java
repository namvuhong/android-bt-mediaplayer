package com.nxcomm.blinkhd.ui;

public class CameraBasicInfo
{
	private String	name;
	private boolean	available;
	private boolean upgrading;
	private String	fwVersion;
	private String	registratrionID;

	public CameraBasicInfo()
	{

	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public boolean isAvailable()
	{
		return available;
	}

	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	public String getFwVersion()
	{
		return fwVersion;
	}

	public void setFwVersion(String fwVersion)
	{
		this.fwVersion = fwVersion;
	}

	public String getRegistratrionID()
	{
		return registratrionID;
	}

	public void setRegistratrionID(String registratrionID)
	{
		this.registratrionID = registratrionID;
	}

	public boolean isUpgrading()
    {
	    return upgrading;
    }

	public void setUpgrading(boolean upgrading)
    {
	    this.upgrading = upgrading;
    }
}