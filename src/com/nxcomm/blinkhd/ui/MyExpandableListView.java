package com.nxcomm.blinkhd.ui;

import java.util.ArrayList;
import java.util.List;

import com.nxcomm.blinkhd.commonasynctask.GetCameraInfoAsyncTask;
import com.nxcomm.blinkhd.ui.DetailSettingsExpandableAdapter.SendCommandToCameraViaStun;

import android.content.Context;
import android.os.AsyncTask.Status;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

class MyExpandableListView extends ExpandableListView
{
	private int	                             lastExpandedPosition	= -1;
	private boolean	                         expanded	          = false;

	private boolean	                         scrollable	          = true;

	private List<SendCommandToCameraViaStun>	taskList	      = new ArrayList<SendCommandToCameraViaStun>();

	SendCommandToCameraViaStun	             task;
	SendCommandToCameraViaStun	             getMotionOnOffTask;
	SendCommandToCameraViaStun	             getSoundOnOffTask;
	SendCommandToCameraViaStun	             getMotionThresholdTask;
	SendCommandToCameraViaStun	             getSoundThresholdTask;

	SendCommandToCameraViaStun	             getFirmwareTask;
	GetCameraInfoAsyncTask	                 getCameraInfoTask;

	public void setScrollable(boolean value)
	{
		this.scrollable = value;
	}

	@Override
	protected void onDetachedFromWindow()
	{
		// TODO Auto-generated method stub
		Log.d("mbp", "On detatch on window");
		for (SendCommandToCameraViaStun task : taskList)
		{
			if (task != null && task.getStatus() != Status.FINISHED)
			{
				task.cancel(true);
			}

			if (getCameraInfoTask != null
			        && getCameraInfoTask.getStatus() != Status.FINISHED)
			{
				getCameraInfoTask.cancel(true);
			}

			if (getFirmwareTask != null
			        && getFirmwareTask.getStatus() != Status.FINISHED)
			{
				getFirmwareTask.cancel(true);
			}
		}
		super.onDetachedFromWindow();
	}

	public MyExpandableListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev)
	{ // TODO

		return super.dispatchTouchEvent(ev);

	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{

		if (scrollable)
			onTouchEvent(ev);

		return false;

	}

	@Override
	public boolean onTouchEvent(MotionEvent ev)
	{

		return super.onTouchEvent(ev);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// HACK! TAKE THAT ANDROID!
		if (isExpanded())
		{
			// Calculate entire height by providing a very large height hint.
			// View.MEASURED_SIZE_MASK represents the largest height possible.
			int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
			        MeasureSpec.AT_MOST);
			super.onMeasure(widthMeasureSpec, expandSpec);

			ViewGroup.LayoutParams params = getLayoutParams();
			params.height = getMeasuredHeight();
		}
		else
		{
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}

	}

	public void init()
	{
		setOnGroupExpandListener(new OnGroupExpandListener()
		{

			@Override
			public void onGroupExpand(int groupPosition)
			{
				if (lastExpandedPosition != -1
				        && groupPosition != lastExpandedPosition)
				{
					collapseGroup(lastExpandedPosition);

				}
				lastExpandedPosition = groupPosition;

			}
		});

	}

	public boolean isExpanded()
	{
		return expanded;
	}

	public void setExpanded(boolean expanded)
	{
		this.expanded = expanded;
	}

}