package com.nxcomm.blinkhd.ui;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.widget.ListView;

import com.blinkhd.R;
import com.msc3.PublicDefine;

public class CameraInfoFragment extends PreferenceFragment
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preference_cam_name);
	}
	
	public void onAttach(Activity activity)
	{
		super.onAttach(activity); 
		
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		ListView list = (ListView) getView().findViewById(android.R.id.list);
		list.setDivider(null);
	}
	
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState); 
	}
}