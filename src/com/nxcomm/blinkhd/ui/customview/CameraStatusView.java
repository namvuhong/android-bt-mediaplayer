package com.nxcomm.blinkhd.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class CameraStatusView extends FrameLayout
{
	private TextView	textView;
	private boolean	  online;
	private ImageView	imageViewOnineStatus;

	private CameraStatus cameraStatus;
	public enum CameraStatus
	{
		NORMAL, UPGRADING
	}

	public CameraStatusView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initView();
	}

	public CameraStatusView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView();
	}

	public CameraStatusView(Context context)
	{
		super(context);
		initView();
	}

	private void initView()
	{
		View view = inflate(getContext(), R.layout.camera_status_view, null);
		addView(view);

		if (!this.isInEditMode())
		{
			textView = (TextView) view.findViewById(R.id.textViewCameraStatus);
			imageViewOnineStatus = (ImageView) view
			        .findViewById(R.id.imageViewCameraStatus);
			if (textView != null)
			{
				textView.setTypeface(FontManager.fontSemiBold);
			}
		}
	}

	public boolean isOnline()
	{
		return online;
	}

	public void setOnline(boolean online)
	{
		this.online = online;
		if (online)
		{
			if (imageViewOnineStatus != null)
			{
				imageViewOnineStatus
				        .setImageResource(R.drawable.settings_circle_green);
			}
			if (textView != null)
			{
				textView.setText(R.string.online);
			}
		}
		else
		{
			if (imageViewOnineStatus != null)
			{
				imageViewOnineStatus
				        .setImageResource(R.drawable.settings_circle_disable);
			}
			if (textView != null)
			{
				textView.setText(R.string.offline);
			}
		}
	}

	public CameraStatus getCameraStatus()
    {
	    return cameraStatus;
    }

	public void setCameraStatus(CameraStatus cameraStatus)
    {
	    this.cameraStatus = cameraStatus;
	    if(cameraStatus == CameraStatus.UPGRADING)
	    {
	    	if (textView != null)
			{
				textView.setText(R.string.upgrading);
			}
	    }
	    else
	    {
	    	setOnline(online);
	    }
    }
}
