package com.nxcomm.blinkhd.ui.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.blinkhd.R;

public class SchedularView extends View
{
	private Paint mPaint, mTextPaint, selectRectPaint, normalRectPaint;
	private int width, height;
	private int column, row;
	private float cellWidth, cellHeight;

	private float[] ptsHorizontal;
	private float[] ptsVertical;
	private Cell[][] cells;

	private float densityMultiplier;

	private float textSizeInDP = 13.33f;
	private float maxTextLength = 0.0f;
	private float paddingLeftText = 10;
	private float paddingRightText = 10;
	private float hourColumnWidth = 0;
	private String[] days = { "M", "T", "W", "Th", "F", "S", "Su" };
	private String everyDay;
	private int drawMode;
	private boolean[][] scheduler;

	public class DrawMode
	{
		public static final int BYDAY = 2;
		public static final int BYWEEK = 4;
	}

	public class Cell
	{
		public class State
		{
			public static final int INVALID_STATE = -1;
			public static final int SELECTED = 1;
			public static final int NORMAL = 0;
		}

		private RectF rectF;
		private int state;

		public Cell(RectF rectF, int state)
		{
			this.setRectF(rectF);
			this.setState(state);

		}

		public RectF getRectF()
		{
			return rectF;
		}

		public void setRectF(RectF rectF)
		{
			this.rectF = rectF;
		}

		public int getState()
		{
			return state;
		}

		public void setState(int state)
		{
			this.state = state;
		}
	}
	
	private boolean viewEnable = true;

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// setMeasuredDimension()

		this.width = this.getMeasuredWidth();

		// calculate the height

		// calculate cellWidth

		if (column == 1)
		{
			this.cellWidth = (float) (this.width - this.hourColumnWidth);
			float tmp = ((float) (this.width - this.hourColumnWidth) / 7);
			this.cellHeight = 0.8f * tmp;
		}
		else
		{
			this.cellWidth = ((float) (this.width - this.hourColumnWidth) / column);
			this.cellHeight = 0.8f * this.cellWidth;
		}

		this.height = (int) (this.cellHeight / 3 + this.cellHeight * row);

		calcData();

		setMeasuredDimension(width, height);

	}

	public SchedularView(Context context)
	{
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}

	public SchedularView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public SchedularView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4)
	{
		// TODO Auto-generated method stub

	}

	private float convertFromDpToPixel(float dp)
	{

		float returnVal = dp * densityMultiplier;
		return returnVal;
	}

	private void init()
	{

		densityMultiplier = getContext().getResources().getDisplayMetrics().density;

		selectRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectRectPaint.setStyle(Style.FILL);
		selectRectPaint.setColor(getResources().getColor(
				com.blinkhd.R.color.main_blue));

		normalRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		normalRectPaint.setStyle(Style.FILL);
		normalRectPaint.setColor(getResources().getColor(
				com.blinkhd.R.color.main_gray));

		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		
		mPaint.setColor(getResources().getColor(com.blinkhd.R.color.line_break));
		
		mPaint.setStrokeWidth(1);

		mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setColor(getResources().getColor(R.color.main_text_color));
		mTextPaint.setTextSize(convertFromDpToPixel(textSizeInDP));

		// calculate hours text
		Rect textBounds = new Rect();
		String text = getContext().getString(R.string.mid_night);

		mTextPaint.getTextBounds(text, 0, text.length(), textBounds);
		maxTextLength = mTextPaint.measureText(text);

		hourColumnWidth = convertFromDpToPixel(paddingLeftText) + maxTextLength
				+ convertFromDpToPixel(paddingRightText);

		drawMode = DrawMode.BYDAY;

		this.everyDay = getResources().getString(R.string.everyday);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{

	}

	private void calcData()
	{
		// horizontal line

		if (drawMode == DrawMode.BYDAY)
		{
			column = 7;
		}
		else
		{
			column = 1;
		}
		row = 25;

		if (column == 1)
		{
			this.cellWidth = (float) (this.width - this.hourColumnWidth);
			float tmp = ((float) (this.width - this.hourColumnWidth) / 7);
			this.cellHeight = 0.8f * tmp;
		}
		else
		{
			this.cellWidth = ((float) (this.width - this.hourColumnWidth) / column);
			this.cellHeight = 0.8f * this.cellWidth;
		}

		this.height = (int) (this.cellHeight / 3 + this.cellHeight * row);

		Log.d("mbp", "Size changed, new size is " + width + "x" + height);

		Log.d("mbp", " onMeasure called. Cell size is " + this.cellWidth + " "
				+ this.cellHeight);

		cells = new Cell[column][row - 1];
		scheduler = new boolean[column][row - 1];

		this.ptsHorizontal = new float[(row + 1) * 4];
		this.ptsVertical = new float[(column + 1) * 4];

		for (int i = 0; i < ptsVertical.length; i += 4)
		{

			ptsVertical[i] = (cellWidth * i / 4) + this.hourColumnWidth;
			ptsVertical[i + 1] = 0;

			ptsVertical[i + 2] = (cellWidth * i / 4) + +this.hourColumnWidth;
			ptsVertical[i + 3] = this.height - this.cellHeight / 3;

		}

		for (int i = 0; i < ptsHorizontal.length; i += 4)
		{

			ptsHorizontal[i] = this.maxTextLength + paddingLeftText
					* densityMultiplier;
			ptsHorizontal[i + 1] = (cellHeight * i / 4) + this.cellHeight;

			ptsHorizontal[i + 2] = this.width;
			ptsHorizontal[i + 3] = (cellHeight * i / 4) + this.cellHeight;

		}
		// rect ( left, top, right, bottom)
		float left, top, right, bottom;
		for (int i = 0; i < column; i++)
		{
			for (int j = 0; j < (row - 1); j++)
			{
				left = this.hourColumnWidth + i * this.cellWidth
						+ this.mPaint.getStrokeWidth();
				top = this.cellHeight * (j + 1) + this.mPaint.getStrokeWidth();
				right = left + this.cellWidth - this.mPaint.getStrokeWidth();
				bottom = top + this.cellHeight - this.mPaint.getStrokeWidth();

				RectF rectF = new RectF(left, top, right, bottom);

				cells[i][j] = new Cell(rectF, Cell.State.NORMAL);

			}
		}

	}

	public void setCellState(float x, float y, int state)
	{
		Cell cell = getCellAt(x, y);
		if (cell != null)
		{
			cell.setState(state);
			this.invalidate();
		}
	}

	public Cell getCellAt(float x, float y)
	{
		Cell returnCell = null;
		float newX = x - this.hourColumnWidth;
		float newY = y - this.cellHeight;

		if (newX > 0 && newY > 0)
		{
			int i = (int) (newX / this.cellWidth);
			int j = (int) (newY / this.cellHeight);
			if (i < column && j < (row - 1))
			{
				returnCell = cells[i][j];
			}

		}
		return returnCell;

	}

	public int getCellState(float x, float y)
	{

		int returnState = Cell.State.INVALID_STATE;
		Cell cell = getCellAt(x, y);
		if (cell != null)
		{
			returnState = cell.getState();

		}
		return returnState;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub
		super.onDraw(canvas);

		// Draw the shadow
		// canvas.drawLine(0, 0, width, height, mPaint);
		canvas.drawLines(ptsHorizontal, mPaint);
		canvas.drawLines(ptsVertical, mPaint);

		// draw hours text from Mid Night -> 23 pm
		for (int i = 0; i < 25; i++)
		{
			String text = "";
			if (i == 0 || i == 24)
			{
				text = getContext().getString(R.string.mid_night);
			}
			else if (i < 12)
			{
				text = i + " am";
			}
			else if(i == 12)
			{
				text = "Midday";
			}
			else
			{
				text = (i%12) + " pm";
			}

			float textLengthPx = mTextPaint.measureText(text);

			int xPos = (int) (paddingLeftText * densityMultiplier + (maxTextLength - textLengthPx) / 2);

			int yPos = (int) ((cellHeight * (i + 1)) - ((mTextPaint.descent() + mTextPaint
					.ascent()) / 2));

			canvas.drawText(text, xPos, yPos, mTextPaint);

		}
		// draw Day from M -> S

		if (drawMode == DrawMode.BYDAY)
		{
			for (int i = 0; i < column; i++)
			{
				float textLengthPx = mTextPaint.measureText(days[i]);

				int xPos = (int) (this.cellWidth * i + this.hourColumnWidth + (this.cellWidth - textLengthPx) / 2);

				int yPos = (int) (cellHeight / 2 - ((mTextPaint.descent() + mTextPaint
						.ascent()) / 2));

				canvas.drawText(days[i], xPos, yPos, mTextPaint);
			}
		}
		else
		{
			float textLengthPx = mTextPaint.measureText(this.everyDay);

			int xPos = (int) (this.hourColumnWidth + (this.cellWidth - textLengthPx) / 2);

			int yPos = (int) (cellHeight / 2 - ((mTextPaint.descent() + mTextPaint
					.ascent()) / 2));
			canvas.drawText(everyDay, xPos, yPos, mTextPaint);
		}

		for (int i = 0; i < column; i++)
		{
			for (int j = 0; j < (row - 1); j++)
			{
				if (cells[i][j].getState() == Cell.State.SELECTED)
				{
					canvas.drawRect(cells[i][j].getRectF(), selectRectPaint);
				}

			}
		}

	}

	public int getColumn()
	{
		return column;
	}

	public void setColumn(int column)
	{
		this.column = column;
	}

	public int getRow()
	{
		return row;
	}

	public void setRow(int row)
	{
		this.row = row;
	}

	public int getDrawMode()
	{
		return drawMode;
	}

	public void setDrawMode(int drawMode)
	{
		this.drawMode = drawMode;
		calcData();
		this.invalidate();
	}

	public boolean[][] getScheduler()
	{

		for (int i = 0; i < column; i++)
		{
			for (int j = 0; j < (row - 1); j++)
			{

				scheduler[i][j] = cells[i][j].getState() == Cell.State.SELECTED ? true
						: false;

			}
		}

		return scheduler;

	}

	public void setScheduler(boolean[][] scheduler)
	{

		for (int i = 0; i < column; i++)
		{
			for (int j = 0; j < (row - 1); j++)
			{
				try
				{
					if (scheduler[i][j])
					{
						cells[i][j].setState(Cell.State.SELECTED);
					}
					else
					{
						cells[i][j].setState(Cell.State.NORMAL);
					}
				}
				catch (Exception ex)
				{
					cells[i][j].setState(Cell.State.NORMAL);
				}

			}
		}
		this.invalidate();

	}

	public boolean isViewEnable()
	{
		return viewEnable;
	}

	public void setViewEnable(boolean viewEnable)
	{
		this.viewEnable = viewEnable;
		
		if(this.viewEnable)
		{
			mTextPaint.setColor(getResources().getColor(R.color.main_text_color));
			selectRectPaint.setColor(getResources().getColor(R.color.main_blue));
		}
		else
		{
			mTextPaint.setColor(getResources().getColor(R.color.sub_text_color));
			selectRectPaint.setColor(getResources().getColor(R.color.disable_gray));
		}
		this.invalidate();
	}
}
