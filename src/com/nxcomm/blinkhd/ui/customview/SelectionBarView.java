package com.nxcomm.blinkhd.ui.customview;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.R;

public class SelectionBarView extends FrameLayout implements
        View.OnClickListener, OnTouchListener
{
	public static final int	         STATE_LOW	               = 1;
	public static final int	         STATE_MEDIUM	           = 3;
	public static final int	         STATE_HIGH	               = 5;

	public static final int	         STATE_LOW_VALUE	       = 10;
	public static final int	         STATE_MEDIUM_VALUE	       = 50;
	public static final int	         STATE_HIGH_VALUE	       = 90;

	private int	                     currentState	           = STATE_MEDIUM;
	private int	                     latestState	           = STATE_MEDIUM;
	ArrayList<OnStateChangeListener>	eventHappenedObservers	= new ArrayList<OnStateChangeListener>();

	public void setOnStateChangeListener(OnStateChangeListener onStateChange)
	{
		eventHappenedObservers.add(onStateChange);
	}

	private ImageView	   lowImageView, mediumImageView, highImageView;
	private TextView	   lowTextView, mediumTextView, highTextView;

	private ImageView	   btnLine;
	private RelativeLayout	mainroot;

	private boolean	       viewEnable;

	public SelectionBarView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initView();

	}

	public SelectionBarView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView();
	}

	public SelectionBarView(Context context)
	{
		super(context);
		initView();
	}

	private void initView()
	{
		View view = inflate(getContext(),
		        R.layout.settings_selection_line_view, null);
		addView(view);

		viewEnable = true;

		if (!this.isInEditMode())
		{

			btnLine = (ImageView) view.findViewById(R.id.btnLine);

			lowImageView = (ImageView) view.findViewById(R.id.btnSelectionOne);
			mediumImageView = (ImageView) view
			        .findViewById(R.id.btnSelectionTwo);
			highImageView = (ImageView) view
			        .findViewById(R.id.btnSelectionThree);

			lowTextView = (TextView) view.findViewById(R.id.textViewLow);
			mediumTextView = (TextView) view.findViewById(R.id.textViewMedium);
			highTextView = (TextView) view.findViewById(R.id.textViewHigh);

			mediumTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));

			mainroot = (RelativeLayout) view.findViewById(R.id.main_root);
			mainroot.setOnTouchListener(this);
			/*
			 * Rect ( left , top , right, bottom)
			 */

			lowImageView.setOnClickListener(this);
			mediumImageView.setOnClickListener(this);
			highImageView.setOnClickListener(this);

			lowTextView.setOnClickListener(this);
			mediumTextView.setOnClickListener(this);
			highTextView.setOnClickListener(this);
		}
	}

	public int getCurrentState()
	{
		return currentState;
	}

	public void setCurrentState(int currentState)
	{
		if (this.currentState == currentState)
			return;

		int oldValue = this.currentState;

		latestState = oldValue;

		this.currentState = currentState;

		lowImageView.setImageResource(R.drawable.settings_circle);
		mediumImageView.setImageResource(R.drawable.settings_circle);
		highImageView.setImageResource(R.drawable.settings_circle);

		lowTextView.setTextColor(getResources()
		        .getColor(R.color.sub_text_color));
		mediumTextView.setTextColor(getResources().getColor(
		        R.color.sub_text_color));
		highTextView.setTextColor(getResources().getColor(
		        R.color.sub_text_color));

		switch (currentState)
		{
		case STATE_LOW:
			lowImageView.setImageResource(R.drawable.settings_circle_selected);
			lowTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		case STATE_MEDIUM:
			mediumImageView
			        .setImageResource(R.drawable.settings_circle_selected);
			mediumTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		case STATE_HIGH:
			highImageView.setImageResource(R.drawable.settings_circle_selected);
			highTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		default:
			break;
		}

		for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
		{
			eventHappenedObserver.onStateChanged(this, oldValue, currentState);
		}

	}

	public void reverseState()
	{
		setCurrentState(latestState, false);
	}

	public void setCurrentState(int currentState, boolean triggerHandler)
	{
		if (this.currentState == currentState)
			return;

		int oldValue = this.currentState;
		latestState = oldValue;
		this.currentState = currentState;

		lowImageView.setImageResource(R.drawable.settings_circle);
		mediumImageView.setImageResource(R.drawable.settings_circle);
		highImageView.setImageResource(R.drawable.settings_circle);

		lowTextView.setTextColor(getResources()
		        .getColor(R.color.sub_text_color));
		mediumTextView.setTextColor(getResources().getColor(
		        R.color.sub_text_color));
		highTextView.setTextColor(getResources().getColor(
		        R.color.sub_text_color));

		switch (currentState)
		{
		case STATE_LOW:
			lowImageView.setImageResource(R.drawable.settings_circle_selected);
			lowTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		case STATE_MEDIUM:
			mediumImageView
			        .setImageResource(R.drawable.settings_circle_selected);
			mediumTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		case STATE_HIGH:
			highImageView.setImageResource(R.drawable.settings_circle_selected);
			highTextView.setTextColor(getResources().getColor(
			        R.color.main_text_color));
			break;
		default:
			break;
		}

		if (triggerHandler)
		{
			for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
			{
				eventHappenedObserver.onStateChanged(this, oldValue,
				        currentState);
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		if (isEnabled())
		{
			Log.d("mbp", "Onclick");
			if (v == lowImageView || v == lowTextView)
			{
				setCurrentState(STATE_LOW);
			}
			else if (v == mediumImageView || v == mediumTextView)
			{
				setCurrentState(STATE_MEDIUM);
			}
			else if (v == highImageView || v == highTextView)
			{
				setCurrentState(STATE_HIGH);
			}
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{

		if (isEnabled())
		{
			if (event.getAction() == MotionEvent.ACTION_UP
			        || event.getAction() == MotionEvent.ACTION_DOWN)
			{
				if (v == mainroot)
				{
					float width = mainroot.getMeasuredWidth();
					float click_x = event.getX();
					float part = click_x / width;

					if (part < 0.2f)
					{
						setCurrentState(STATE_LOW);
					}
					else if (part > 0.45f && part < 0.55f)
					{
						setCurrentState(STATE_MEDIUM);
					}
					else if (part > 0.8f)
					{
						setCurrentState(STATE_HIGH);
					}
				}
			}
		}

		return false;
	}

	public boolean isViewEnable()
	{
		return viewEnable;
	}

	public void setViewEnable(boolean enable)
	{
		this.viewEnable = enable;
		this.setEnabled(enable);
		if (this.viewEnable)
		{
			btnLine.setImageResource(R.drawable.settings_line);
			lowImageView.setImageResource(R.drawable.settings_circle);
			mediumImageView.setImageResource(R.drawable.settings_circle);
			highImageView.setImageResource(R.drawable.settings_circle);

			switch (getCurrentState())
			{
			case STATE_HIGH:
				highImageView
				        .setImageResource(R.drawable.settings_circle_selected);
				highTextView.setTextColor(getResources().getColor(
				        R.color.main_text_color));
				break;
			case STATE_LOW:
				lowImageView
				        .setImageResource(R.drawable.settings_circle_selected);
				lowTextView.setTextColor(getResources().getColor(
				        R.color.main_text_color));
				break;
			case STATE_MEDIUM:
				mediumImageView
				        .setImageResource(R.drawable.settings_circle_selected);
				mediumTextView.setTextColor(getResources().getColor(
				        R.color.main_text_color));
				break;
			}

		}
		else
		{
			btnLine.setImageResource(R.drawable.settings_line_disable);
			lowImageView.setImageResource(R.drawable.settings_circle_disable);
			mediumImageView
			        .setImageResource(R.drawable.settings_circle_disable);
			highImageView.setImageResource(R.drawable.settings_circle_disable);

			lowTextView.setTextColor(getResources().getColor(
			        R.color.sub_text_color));
			mediumTextView.setTextColor(getResources().getColor(
			        R.color.sub_text_color));
			highTextView.setTextColor(getResources().getColor(
			        R.color.sub_text_color));

			switch (getCurrentState())
			{
			case STATE_HIGH:
				highImageView
				        .setImageResource(R.drawable.settings_circle_selected_disable);
				break;
			case STATE_LOW:
				lowImageView
				        .setImageResource(R.drawable.settings_circle_selected_disable);
				break;
			case STATE_MEDIUM:
				mediumImageView
				        .setImageResource(R.drawable.settings_circle_selected_disable);
				break;
			}
		}
	}
}
