package com.nxcomm.blinkhd.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class ButtonImageCenterView extends FrameLayout
{
	private TextView	textView;
	private ImageView	imageView;
	private Drawable	icon;
	private Drawable	icon2;
	private int	      colorPressed	= 0xff000;
	private String	  text	       = "Button";

	public ButtonImageCenterView(Context context, AttributeSet attrs,
	        int defStyle)
	{
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs,
		        R.styleable.ButtonImageCenterView);
		icon = a.getDrawable(R.styleable.ButtonImageCenterView_icon);
		icon2 = a.getDrawable(R.styleable.ButtonImageCenterView_icon2);
		text = a.getString(R.styleable.ButtonImageCenterView_text);
		colorPressed = a.getColor(
		        R.styleable.ButtonImageCenterView_textColorPressed, defStyle);

		initView();

	}

	public ButtonImageCenterView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs,
		        R.styleable.ButtonImageCenterView);
		icon = a.getDrawable(R.styleable.SwitchableImageView_src2);
		icon2 = a.getDrawable(R.styleable.ButtonImageCenterView_icon2);
		text = a.getString(R.styleable.ButtonImageCenterView_text);
		colorPressed = a.getColor(
		        R.styleable.ButtonImageCenterView_textColorPressed, 0);
		initView();
	}

	public ButtonImageCenterView(Context context)
	{
		super(context);
		initView();
	}

	private void initView()
	{

		View view = inflate(getContext(), R.layout.button_image_center, null);

		addView(view);

		textView = (TextView) view.findViewById(R.id.textView);

		imageView = (ImageView) view.findViewById(R.id.imageView);

		if (textView != null && text != null)
		{
			if (!this.isInEditMode())
			{
				textView.setTypeface(FontManager.fontRegular);
			}

			textView.setText(this.text);
		}
		if (imageView != null && this.icon != null)
		{
			imageView.setImageDrawable(this.icon);
		}

		this.setOnTouchListener(new OnTouchListener()
		{

			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN)
				{
					textView.setTextColor(colorPressed);
					if (icon2 != null)
					{
						imageView.setImageDrawable(icon2);
					}
				}
				else if (event.getAction() == MotionEvent.ACTION_UP)
				{
					textView.setTextColor(Color.WHITE);
					if (icon != null)
					{
						imageView.setImageDrawable(icon);
					}
				}
				return false;
			}
		});

	}

}
