package com.nxcomm.blinkhd.ui.customview;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.blinkhd.R;
import com.nxcomm.vector.Vector2f;

public class ArtProgressView extends View
{

	private Paint	                           mPaint, mPaint1, mPaint2,
	        mPaint3, mPaint4, mTextPaint;

	private RectF	                           circleIn, circleOut, arc;

	private float	                           width, height;
	private float	                           centerW, centerH;

	private float	                           arcRadius;
	private float	                           circleRadius;

	private final float	                       startAngle	           = 90;	                                            // this
	                                                                                                                        // start
	                                                                                                                        // angle
	                                                                                                                        // must
	                                                                                                                        // match
	                                                                                                                        // with
	                                                                                                                        // vector
	                                                                                                                        // src

	private float	                           sweepAngle	           = 60;

	private float	                           progressWidth	       = 9;

	private float	                           x, y;

	private float	                           circleSelectorInRadius,
	        circleSelectorOutRadius;

	private float	                           selectorX, selectorY;

	private Vector2f	                       src	                   = new Vector2f(
	                                                                           0,
	                                                                           -1);

	private boolean	                           viewEnable	           = true;

	private float	                           density;

	private float	                           hmFontSize	           = 48;
	private float	                           textFontSize	           = 20;
	private String	                           hmText;
	private String	                           minuteText, minutesText;
	private volatile String	                   useMinuteText	       = "";

	private int	                               maxValue	               = 180;	                                            // 3
	                                                                                                                        // hours
	private int	                               currentValue;

	private double	                           currentAngle;

	private boolean	                           motionActionDownTrigger	= false;

	private Queue<Float>	                   historySweepAngle	   = new LinkedList<Float>();
	private float	                           previousSweepAngle	   = -1;
	private boolean	                           reachLimit	           = false;

	ArrayList<OnAdjustValueCompeletedListener>	eventHappenedObservers	= new ArrayList<OnAdjustValueCompeletedListener>();

	public void setOnAdjustValueCompeletedListener(
	        OnAdjustValueCompeletedListener onAdjustValueCompeletedListener)
	{
		eventHappenedObservers.add(onAdjustValueCompeletedListener);
	}

	public ArtProgressView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initialize();
		// TODO Auto-generated constructor stub
	}

	public ArtProgressView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{

		// TODO Auto-generated method stub
		if (viewEnable)
		{
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				motionActionDownTrigger = true;
			}
			else if (event.getAction() == MotionEvent.ACTION_MOVE)
			{
				if (motionActionDownTrigger)
				{

					Vector2f dest = new Vector2f(event.getX() - centerW,
					        centerH - event.getY());

					dest.normalize();

					float angle = 0;
					if (dest.x < 0)
					{
						angle = src.angle(dest);
					}
					else
					{
						angle = (float) (Math.PI + (Math.PI - src.angle(dest)));
					}
					float tmp = (float) (angle * (180 / Math.PI));

					Log.d("mbp", "Temporary sweep angle: " + tmp
					        + " history sweep angle " + previousSweepAngle);

					if (!reachLimit)
					{

						if (previousSweepAngle > 350 && tmp < 10)
						{
							sweepAngle = 360;
							reachLimit = true;
						}
						else if (previousSweepAngle < 10 && tmp > 180)
						{
							sweepAngle = 0;
							// reachLimit = false;
						}
						else
						{
							sweepAngle = tmp;
							previousSweepAngle = tmp;
						}
					}
					else
					{
						if (tmp + 10 > previousSweepAngle)
						{
							sweepAngle = tmp;
							reachLimit = false;
							previousSweepAngle = tmp;
						}
					}

					this.invalidate();

				}

			}
			else if (event.getAction() == MotionEvent.ACTION_UP)
			{
				if (motionActionDownTrigger)
				{
					motionActionDownTrigger = false;

					for (OnAdjustValueCompeletedListener eventHappenedObserver : eventHappenedObservers)
					{
						eventHappenedObserver.onAdjustValueCompeleted(this
						        .getCurrentValue());
					}
				}
			}
		}

		return true;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		this.width = getMeasuredWidth();
		this.height = getMeasuredHeight();

		if (this.width != 0 && height != 0)
		{
			invalidateData();
		}

	}

	public void setSweepAngle(float d)
	{
		this.sweepAngle = d;
		this.invalidate();
	}

	public ArtProgressView(Context context)
	{
		super(context); // TODO Auto-generated constructor stub
		initialize();
	}

	private void invalidateData()
	{

		centerW = this.width / 2;
		centerH = this.height / 2;

		circleSelectorInRadius = (progressWidth + 4) / 2;
		circleSelectorOutRadius = circleSelectorInRadius * 1.45f;

		arcRadius = (this.height / 2)
		        - (circleSelectorOutRadius - progressWidth / 2);
		circleRadius = this.arcRadius - progressWidth;

		selectorX = (arcRadius + circleRadius) / 2;
		selectorY = 0;

		currentAngle = Math.PI * (sweepAngle + startAngle) / 180;

		arc = new RectF(centerW - arcRadius, centerH - arcRadius, centerW
		        + arcRadius, centerH + arcRadius);

		circleOut = new RectF(centerW - arcRadius, centerH - arcRadius, centerW
		        + arcRadius, centerH + arcRadius);
		circleIn = new RectF(centerW - circleRadius, centerH - circleRadius,
		        centerW + circleRadius, centerH + circleRadius);
	}

	private void initialize()
	{

		src.normalize();
		density = getContext().getResources().getDisplayMetrics().density;

		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setColor(getResources().getColor(R.color.main_blue));

		mPaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint1.setColor(getResources().getColor(R.color.main_gray));

		mPaint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint2.setColor(getResources().getColor(R.color.line_break));

		mPaint3 = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint3.setColor(0xFFDAEEF6);

		mPaint4 = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint4.setColor(0x78DAEEF6);

		mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setColor(getResources().getColor(R.color.main_text_color));
		mTextPaint.setTextSize(hmFontSize * density);

		minuteText = getResources().getString(R.string.minute);
		minutesText = getResources().getString(R.string.minutes);
		useMinuteText = minutesText;

		this.progressWidth = density * progressWidth;

	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		// TODO Auto-generated method stub

		currentAngle = Math.PI * (sweepAngle + startAngle) / 180;
		x = (float) (selectorX * Math.cos(currentAngle) - selectorY
		        * Math.sin(currentAngle));
		y = (float) (selectorX * Math.sin(currentAngle) + selectorY
		        * Math.cos(currentAngle));
		x += centerW;

		y += (centerH - circleSelectorOutRadius + progressWidth);

		super.onDraw(canvas);

		// Log.d("mbp"," selector position "+x+" x "+y);
		// Log.d("mbp"," arcRadius x circleRadius "+ arcRadius
		// +" "+circleRadius);

		canvas.drawOval(circleOut, mPaint2);
		canvas.drawArc(arc, startAngle, sweepAngle, true, mPaint);
		canvas.drawOval(circleIn, mPaint1);

		canvas.drawCircle(x, y, circleSelectorOutRadius, mPaint4);

		canvas.drawCircle(x, y, circleSelectorInRadius, mPaint3);

		hmText = formatHours(this.getCurrentValue());

		mTextPaint.setTextSize(hmFontSize * density);

		float w = mTextPaint.measureText(hmText);

		int xPos = (int) ((this.width - w) / 2);
		int yPos = (int) ((this.height / 2) - ((mTextPaint.descent() + mTextPaint
		        .ascent()) / 2));
		// ((textPaint.descent() + textPaint.ascent()) / 2) is the distance from
		// the baseline to the center.

		// Log.d("mbp", "text pos " + x + "x" + y);
		mTextPaint.setFakeBoldText(true);
		canvas.drawText(hmText, xPos, yPos - (this.hmFontSize) / 2, mTextPaint);

		mTextPaint.setTextSize(textFontSize * density);
		mTextPaint.setFakeBoldText(false);

		canvas.drawText(useMinuteText,
		        (this.width - mTextPaint.measureText(minuteText)) / 2, yPos
		                + (this.hmFontSize) / 3, mTextPaint);

	}

	public boolean isViewEnable()
	{
		return viewEnable;
	}

	public void setViewEnable(boolean viewEnable)
	{
		this.viewEnable = viewEnable;

		if (this.viewEnable)
		{
			mTextPaint.setColor(getResources()
			        .getColor(R.color.main_text_color));
			mPaint.setColor(getResources().getColor(R.color.main_blue));
		}
		else
		{
			mTextPaint
			        .setColor(getResources().getColor(R.color.sub_text_color));
			mPaint.setColor(getResources().getColor(R.color.disable_black));
		}
		this.invalidate();
	}

	public int getCurrentValue()
	{

		return (int) (maxValue * ((this.sweepAngle * Math.PI / 180) / (2 * Math.PI)));
	}

	public void setCurrentValue(int value)
	{

		if (value > maxValue)
		{
			value = maxValue;
		}

		setSweepAngle(((float) value / this.maxValue) * 360);
	}

	public interface OnAdjustValueCompeletedListener
	{
		void onAdjustValueCompeleted(float newValue);
	}

	private String formatHours(int value)
	{
		String str = null;

		int hours = value / 60;
		int minutes = value % 60;

		if (hours > 0)
		{

			str = hours + "h " + formatMinutes(minutes);
		}
		else
		{
			str = formatMinutes(minutes);
		}

		return str;
	}

	private String formatMinutes(int value)
	{
		String str = null;
		if (value < 10)
		{
			if (value == 1 || value == 0)
			{
				useMinuteText = minuteText;
			}
			else
			{
				useMinuteText = minutesText;
			}

			str = "" + value;
		}
		else
		{
			str = "" + value;
			useMinuteText = minutesText;
		}
		return str;
	}
}
