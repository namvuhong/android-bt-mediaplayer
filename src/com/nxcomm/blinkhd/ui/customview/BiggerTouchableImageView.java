package com.nxcomm.blinkhd.ui.customview;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

public class BiggerTouchableImageView extends ImageView
{

	private Rect extendRect = new Rect();

	public Rect getExtendRect()
	{
		return extendRect;
	}

	public void setExtendRect(Rect extendRect)
	{
		this.extendRect = extendRect;
	}

	public BiggerTouchableImageView(Context context, AttributeSet attrs)

	{
		super(context, attrs);
		init();
	}

	public BiggerTouchableImageView(Context context)
	{
		super(context);
		init();
	}

	public BiggerTouchableImageView(Context context, AttributeSet attrs,
									int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}
	private void init()
	{
		
	}
}
