package com.nxcomm.blinkhd.ui.customview;

import java.io.File;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.net.wifi.WifiManager;
import android.text.Html;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.Util;
import com.msc3.registration.SingleCamConfigureActivity;
import com.nxcomm.blinkhd.ui.CameraDetailSettingActivity;
import com.nxcomm.blinkhd.ui.HomeScreenActivity;

public class CameraListSquare extends FrameLayout
{
	private SparseArray<CamProfile>	cameras	    = null;

	private ButtonImageCenterView[]	btnAddCam	= new ButtonImageCenterView[4];
	private TextView[]	            txtCamName	= new TextView[4];
	private ImageView[]	            imgCamImage	= new ImageView[4];

	private ColorMatrixColorFilter	grayScaleFilter;

	public CameraListSquare(Context context)
	{
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}

	public CameraListSquare(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public CameraListSquare(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		// TODO Auto-generated constructor stub
		init();
	}

	private void init()
	{
		if (!isInEditMode())
		{

			ColorMatrix cm = new ColorMatrix();
			cm.setSaturation(0);
			grayScaleFilter = new ColorMatrixColorFilter(cm);

			View view = inflate(getContext(), R.layout.camera_list_square, null);
			addView(view);

			/*
			 * collect all control
			 */

			// Button Add Camera
			btnAddCam[0] = (ButtonImageCenterView) view
			        .findViewById(R.id.btnAddCamOne);
			btnAddCam[1] = (ButtonImageCenterView) view
			        .findViewById(R.id.btnAddCamTwo);
			btnAddCam[2] = (ButtonImageCenterView) view
			        .findViewById(R.id.btnAddCamThree);
			btnAddCam[3] = (ButtonImageCenterView) view
			        .findViewById(R.id.btnAddCamFour);

			// TextView Camera Name
			txtCamName[0] = (TextView) view.findViewById(R.id.txtCamOne);
			txtCamName[1] = (TextView) view.findViewById(R.id.txtCamTwo);
			txtCamName[2] = (TextView) view.findViewById(R.id.txtCamThree);
			txtCamName[3] = (TextView) view.findViewById(R.id.txtCamFour);

			// Image view
			imgCamImage[0] = (ImageView) view.findViewById(R.id.imgCamOne);
			imgCamImage[1] = (ImageView) view.findViewById(R.id.imgCamTwo);
			imgCamImage[2] = (ImageView) view.findViewById(R.id.imgCamThree);
			imgCamImage[3] = (ImageView) view.findViewById(R.id.imgCamFour);
		}
	}

	private void updateUI()
	{
		if (cameras != null)
		{
			int i = 0;
			for (i = 0; i < cameras.size(); i++)
			{
				if (i < 4)
				{
					btnAddCam[i].setVisibility(View.INVISIBLE);

					String regId = cameras.get(i).getRegistrationId();
					String snapFileName = Util.getRootRecordingDirectory()
					        + File.separator
					        + CameraDetailSettingActivity
					                .getSnapshotNameHashed(regId) + ".jpg";
					File snap = new File(snapFileName);

					if (snap.exists())
					{
						Bitmap bm = BitmapFactory.decodeFile(snapFileName);
						imgCamImage[i].setImageBitmap(bm);
					}

					if (!cameras.get(i).isReachableInRemote())
					{
						imgCamImage[i].setColorFilter(grayScaleFilter);
					}

					txtCamName[i].setText(cameras.get(i).getName());

					final CamProfile current = cameras.get(i);

					imgCamImage[i].setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{
							if (current.isReachableInRemote() == false)
							{
								// donot allow go to video screen
							}
							else
							{
								SharedPreferences m = getContext()
								        .getSharedPreferences(
								                PublicDefine.PREFS_NAME, 0);
								Editor editor = m.edit();
								editor.putString(
								        PublicDefine.PREFS_SELECTED_CAMERA_MAC_FROM_CAMERA_SETTING,
								        current.getRegistrationId());
								editor.commit();

								// Toast toast = Toast.makeText(context,
								// "You selected "
								// + cameraps.get(position).getName()
								// + ", press Back to view your camera.",
								// Toast.LENGTH_SHORT);
								// toast.setGravity(Gravity.CENTER, 0, 0);
								// toast.show();

								Intent intent = new Intent(getContext(),
								        HomeScreenActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								getContext().startActivity(intent);
							}

						}
					});

				}
			}

			for (; i < 4; i++)
			{
				btnAddCam[i].setVisibility(View.VISIBLE);
				imgCamImage[i].setVisibility(View.INVISIBLE);
				txtCamName[i].setVisibility(View.INVISIBLE);

				btnAddCam[i].setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						launchAddCameraAcitivity();
					}
				});

			}

		}
	}

	private void launchAddCameraAcitivity()
	{

		final WifiManager w = (WifiManager) getContext().getSystemService(
		        Context.WIFI_SERVICE);
		if (w.getWifiState() == WifiManager.WIFI_STATE_ENABLED
		        || w.getWifiState() == WifiManager.WIFI_STATE_ENABLING)
		{
			SharedPreferences settings = getContext().getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

			Intent cam_conf = new Intent(getContext(),
			        SingleCamConfigureActivity.class);
			cam_conf.putExtra(SingleCamConfigureActivity.str_userToken,
			        saved_token);
			getContext().startActivity(cam_conf);

		}
		else
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
			Spanned msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.mobile_data_is_enabled_please_turn_on_wifi_to_add_camera)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setNegativeButton(
			                getResources().getString(R.string.Cancel),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
				                }
			                })
			        .setPositiveButton(
			                getResources().getString(R.string.turn_on_wifi),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                turnOnWifi();

					                SharedPreferences settings = getContext()
					                        .getSharedPreferences(
					                                PublicDefine.PREFS_NAME, 0);
					                String saved_token = settings
					                        .getString(
					                                PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
					                                null);

					                Intent cam_conf = new Intent(getContext(),
					                        SingleCamConfigureActivity.class);
					                cam_conf.putExtra(
					                        SingleCamConfigureActivity.str_userToken,
					                        saved_token);
					                getContext().startActivity(cam_conf);
					                // activity.finish();
				                }
			                });

			AlertDialog alert = builder.create();
			alert.show();
		}

	}

	private void turnOnWifi()
	{
		/* re-use Vox_main, just an empty and transparent activity */
		WifiManager w = (WifiManager) getContext().getSystemService(
		        Context.WIFI_SERVICE);
		w.setWifiEnabled(true);
		// while ((w.getWifiState() != WifiManager.WIFI_STATE_ENABLED))
		// {
		// try
		// {
		// Thread.sleep(1000);
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
		// }

	}

	public SparseArray<CamProfile> getCameras()
	{
		return cameras;
	}

	public void setCameras(SparseArray<CamProfile> cameras)
	{
		this.cameras = cameras;
		updateUI();
	}

}
