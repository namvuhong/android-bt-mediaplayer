package com.nxcomm.blinkhd.ui.customview;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.blinkhd.R;

public class SwitchableImageView extends ImageView
{
	private Drawable	             src2;
	private Drawable	             src1;

	public static final int	         STATE_ONE	               = 1;	                                  // ON
	public static final int	         STATE_TWO	               = 2;	                                  // OFF
	private boolean	                 viewEnable	               = true;

	ArrayList<OnStateChangeListener>	eventHappenedObservers	= new ArrayList<OnStateChangeListener>();

	public void setOnStateChangeListener(OnStateChangeListener onStateChange)
	{
		eventHappenedObservers.add(onStateChange);
	}

	public boolean getBooleanState()
	{		
		if (this.getState() == STATE_ONE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void setBooleanState(boolean value)
	{		
		if (value)
		{
			this.setState(STATE_ONE);
		}
		else
		{
			this.setState(STATE_TWO);
		}

	}

	public int getState()
	{		
		if (this.getDrawable() == src1)
		{
			return STATE_ONE;
		}
		else
		{
			return STATE_TWO;
		}
	}

	public void setState(int state)
	{		
		int oldValue = this.getState();

		if (state == STATE_ONE)
		{
			if (src1 != null)
				this.setImageDrawable(src1);
		}
		else if (state == STATE_TWO)
		{
			if (src2 != null)
				this.setImageDrawable(src2);
		}

		for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
		{
			eventHappenedObserver.onStateChanged(SwitchableImageView.this,
			        state, oldValue);
		}
	}

	public void setStateWithoutTrigger(int state)
	{		
		if (state == STATE_ONE)
		{
			if (src1 != null)
				this.setImageDrawable(src1);
		}
		else if (state == STATE_TWO)
		{
			if (src2 != null)
				this.setImageDrawable(src2);
		}

	}

	public SwitchableImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		src1 = getDrawable();
		TypedArray a = context.obtainStyledAttributes(attrs,
		        R.styleable.SwitchableImageView);
		src2 = a.getDrawable(R.styleable.SwitchableImageView_src2);

		this.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				if (viewEnable)
				{
					if (src2 != null)
					{
						if (getDrawable() == src1)
						{
							setImageDrawable(src2);

							for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
							{
								eventHappenedObserver.onStateChanged(
								        SwitchableImageView.this, STATE_TWO,
								        STATE_ONE);
							}

						}
						else
						{
							setImageDrawable(src1);

							for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
							{
								eventHappenedObserver.onStateChanged(
								        SwitchableImageView.this, STATE_ONE,
								        STATE_TWO);
							}
						}
					}
				}
			}
		});
	}

	public boolean isViewEnable()
	{
		return viewEnable;
	}

	public void setViewEnable(boolean viewEnable)
	{
		this.viewEnable = viewEnable;

	}

}
