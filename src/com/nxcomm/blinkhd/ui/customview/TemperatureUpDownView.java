package com.nxcomm.blinkhd.ui.customview;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.blinkhd.R;


public class TemperatureUpDownView extends FrameLayout implements
View.OnClickListener
{

	public interface ITemperatureUpDownViewExecuter
	{
		public void commitChange(TemperatureUpDownView view, float newValueInCelsuis); 
	}



	private ITemperatureUpDownViewExecuter exec = null; 

	private float tempMax, tempMin; 
	private int tempMode;
	private float temperatureValue;
	private Button btnMinus, btnPlus;
	private TextView temparatureTextView;

	ArrayList<OnStateChangeListener> eventHappenedObservers = new ArrayList<OnStateChangeListener>();

	private boolean viewEnable;

	public void setOnStateChangeListener(OnStateChangeListener onStateChange)
	{
		eventHappenedObservers.add(onStateChange);
	}

	public TemperatureUpDownView(Context context, AttributeSet attrs,
			int defStyle)
	{
		super(context, attrs, defStyle);
		initView();
	}

	public TemperatureUpDownView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initView();
	}

	public TemperatureUpDownView(Context context)
	{
		super(context);
		initView();
	}


	public void setExec(ITemperatureUpDownViewExecuter ex)
	{
		this.exec  = ex; 
	}
	public void setBoundaries(float max, float min)
	{
		this.tempMax = max; 
		this.tempMin = min; 
	}
	private void initView()
	{
		View view = inflate(getContext(), R.layout.temperature_up_down, null);
		addView(view);

		if (!this.isInEditMode())
		{

			viewEnable = true;
			tempMode = TemperatureMode.C;

			btnMinus = (Button) view.findViewById(R.id.btnMinusTemp);
			btnPlus = (Button) view.findViewById(R.id.btnPlusTemp);
			temparatureTextView = (TextView) view
					.findViewById(R.id.temparatureTextView);
			this.setTemperatureValue(25);
			btnMinus.setOnClickListener(this);
			btnPlus.setOnClickListener(this);
		}
	}

	private boolean firstTimeClick = true; 

	private long lastUpdateTime  ; 
	private float oldValue; 
	private Timer sendCommandTimer ;
	// Start timer now
	private  class UpdateTimerTask extends TimerTask
	{
		@Override
		public void run() {

			long now = System.currentTimeMillis(); 

			if ( ( now - lastUpdateTime )>= 2000 ) //no update during the last 5secs
			{

				

				if (exec != null)
				{
					float tempValue = temperatureValue;
					
					if (tempMode == TemperatureMode.F) // convert from f to c
					{
						tempValue = convertFtoC(tempValue); 
					}
					
					//XXX: assuming user does not change the Temperature units 
					if (oldValue == temperatureValue)
					{
						Log.d("mbp", "skip update, since same value"); 
					}
					else
					{
						exec.commitChange(TemperatureUpDownView.this, tempValue); 
					}
				}

				this.cancel(); 
				sendCommandTimer.cancel();
				firstTimeClick = true; 
			}

		}
	};

	@Override
	public void onClick(View v)
	{
		if (this.viewEnable)
		{				
			lastUpdateTime =System.currentTimeMillis(); 

			if (firstTimeClick == true)
			{
				oldValue = temperatureValue;
				firstTimeClick = false; 

				sendCommandTimer = new Timer();
				sendCommandTimer.scheduleAtFixedRate(new UpdateTimerTask(), 0, 1000); 
			}

			if (v == btnMinus)
			{
				decreaseTemp(1);
			}
			else if (v == btnPlus)
			{
				increaseTemp(1);
			}



		}

	}

	public void increaseTemp(float value)
	{
		if ( (this.getTempMode() == TemperatureMode.C && 
				this.getTemperatureValue() < 45) ||
				(this.getTempMode() == TemperatureMode.F && 
				this.getTemperatureValue() < 113) )
		{
			this.setTemperatureValue(this.getTemperatureValue() + value);
		}
	}

	public void decreaseTemp(float value)
	{
		if ( (this.getTempMode() == TemperatureMode.C && 
				this.getTemperatureValue() > 0) ||
				(this.getTempMode() == TemperatureMode.F && 
				this.getTemperatureValue() > 32) )
		{
			this.setTemperatureValue(this.getTemperatureValue() - value);
		}
	}

	public float getTemperatureValue()
	{
		return temperatureValue;
	}

	public void setTemperatureValue(float temperatureValue)
	{
		if (this.getTempMode() == TemperatureMode.C)
		{
			if (temperatureValue < tempMin)
			{
				temperatureValue = tempMin; 
			}
			if (temperatureValue > tempMax)
			{
				temperatureValue = tempMax; 
			}
		}
		else
		{
			if (temperatureValue < convertCtoF(tempMin))
			{
				temperatureValue = convertCtoF(tempMin); 
			}
			if (temperatureValue > convertCtoF(tempMax))
			{
				temperatureValue = convertCtoF(tempMax); 
			}
		}


		int oldValue = (int) this.temperatureValue;
		this.temperatureValue = temperatureValue;
		if (this.getTempMode() == TemperatureMode.C)
		{
			temparatureTextView.setText(String.format("%.1f", temperatureValue)
					+ " \u2103");
		}
		else
		{
			temparatureTextView.setText(String.format("%.1f", temperatureValue)
					+ " \u2109");
		}
		for (OnStateChangeListener eventHappenedObserver : eventHappenedObservers)
		{
			eventHappenedObserver.onStateChanged(this, (int) temperatureValue, oldValue);
		}

	}

	public boolean isViewEnable()
	{
		return viewEnable;
	}

	public void setViewEnable(boolean viewEnable)
	{
		this.viewEnable = viewEnable;
		if (this.viewEnable)
		{
			temparatureTextView
			.setBackgroundResource(R.drawable.settings_temp_bg);

		}
		else
		{
			temparatureTextView
			.setBackgroundResource(R.drawable.settings_temp_bg_disable);
		}
	}

	public int getTempMode()
	{
		return tempMode;
	}

	public void setTempMode(int tempMode)
	{
		if (this.tempMode != tempMode)
		{
			this.tempMode = tempMode;

			if (tempMode == TemperatureMode.C) // convert from f to c
			{
				this.setTemperatureValue(convertFtoC(this.getTemperatureValue()));
			}
			else
			{
				this.setTemperatureValue(convertCtoF(this.getTemperatureValue()));
			}

		}
	}

	public float convertFtoC(float f)
	{
		f = (f - 32) * 5f / 9f;
		return f;

	}

	public float convertCtoF(float c)
	{
		c = 9f * c / 5f + 32;
		return c;
	}

}
