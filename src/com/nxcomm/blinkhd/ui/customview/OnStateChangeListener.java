package com.nxcomm.blinkhd.ui.customview;

import android.view.View;

public interface OnStateChangeListener
{
	public void onStateChanged(View v, int newValue, int oldValue);
}
