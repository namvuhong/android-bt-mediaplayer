package com.nxcomm.blinkhd.ui.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;

public class TabbedMenuItem extends TextView
{
	private Drawable previousBackground;
	private boolean isTouchDownFromMe = false;
	private boolean checked = false;
	
	private Typeface sboldTf = FontManager.fontSemiBold;// Typeface.createFromAsset(getContext().getAssets(), 
			//"fonts/ProximaNova-Semibold.otf");
	private Typeface lightTf = FontManager.fontLight;//Typeface.createFromAsset(getContext().getAssets(), 
			//"fonts/ProximaNova-Light.otf");

	public TabbedMenuItem(Context context)
	{
		super(context);
		setFocusable(true);
		setFocusableInTouchMode(true);
		setClickable(true);
		setGravity(Gravity.CENTER);
		setPadding(15, 0, 15, 0);
		setBackgroundResource(R.drawable.blank_bg);

		this.setBackgroundResource(R.drawable.blank_bg);
		setTypeface(lightTf);
		
		setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.MATCH_PARENT));

	}

	public void setChecked(boolean value)
	{
		this.checked = value;
		if (this.checked)
		{
			Log.d("mbp", "Menu item " + this.getText() + "set checked " + value);
			this.setBackgroundResource(R.drawable.menu_item_clicked);
			setTypeface(sboldTf);
		}
		else
		{
			this.setBackgroundResource(R.drawable.blank_bg);
			setTypeface(lightTf);
		}

		this.invalidate();
	}

	public boolean isChecked()
	{
		return this.checked;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// TODO Auto-generated method stub
		this.requestFocus();

		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			// Log.d("mbp", "TOUCH DOWN FROM TAB "+this.isFocused());
			this.previousBackground = this.getBackground();
			this.setBackgroundResource(R.drawable.overlay_bg);
			isTouchDownFromMe = true;

		}
		else if (event.getAction() == MotionEvent.ACTION_UP
				&& isTouchDownFromMe)
		{
			// Log.d("mbp", "TOUCH UP FROM TAB "+this.isFocused());
			isTouchDownFromMe = false;
			this.setBackgroundDrawable(this.previousBackground);
		}

		return super.onTouchEvent(event);
	}

}
