package com.nxcomm.blinkhd.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.blinkhd.BlinkHDApplication;
import com.blinkhd.R;
import com.blinkhd.ReportErrorActivity;
import com.blinkhd.UpgradeToPremium;
import com.msc3.LogCollectorTask;
import com.msc3.PublicDefine;
import com.msc3.Util;
import com.nxcomm.blinkhd.ui.dialog.ChangePasswordDialog;
import com.nxcomm.blinkhd.ui.dialog.CommonDialogListener;
import com.nxcomm.blinkhd.ui.dialog.EnableDisconnectAlertDialog;
import com.nxcomm.blinkhd.ui.dialog.RemoteConnectionSettingDialog;
import com.nxcomm.blinkhd.ui.dialog.UseRemoteOnlySettingDialg;
import com.nxcomm.meapi.User;
import com.nxcomm.meapi.user.LoginResponse2;
import com.nxcomm.meapi.user.UserInformation;

public class AccountSettingFragment extends PreferenceFragment
{
	private final String	logFileDir	             = Util.getRootRecordingDirectory()
	                                                         + File.separator
	                                                         + "logs";
	private final String	logFileName	             = "log.log";
	private final String	logFilePath	             = logFileDir
	                                                         + File.separator
	                                                         + logFileName;

	public static final int	REQUEST_SEND_APP_LOG	 = 1;

	private boolean	        isStunEnabled	         = false;
	private boolean	        useRemoteOnly	         = false;
	private boolean	        isDisconnectAlertEnabled	= true;
	private Activity	    activity	             = null;
	private ProgressDialog	update_dialog	         = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preference_account);
	}

	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		this.activity = activity;
	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		super.onDetach();
		activity = null;
	}

	@Override
	public void onResume()
	{
		super.onResume();

		ListView list = (ListView) getView().findViewById(android.R.id.list);
		list.setDivider(null);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_SEND_APP_LOG)
		{
			Log.d("mbp", "Send app log successfully.");
		}
	}

	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		SharedPreferences settings = ((ContextWrapper) getActivity())
		        .getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		String user_id = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_ID,
		        null);

		Preference userName, changePwd, logOut;
		PreferenceCategory cat = null;

		cat = (PreferenceCategory) findPreference("MBP_SETTINGS");

		userName = cat.findPreference("string_PortalUsrId");
		userName.setSummary(user_id);

		changePwd = cat.findPreference("change_pwd");
		changePwd.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{

			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				// TODO : goto Change FWD link

				/**
				 * 20140312_sonnguyen_change_four_dialog_UI
				 */
				final ChangePasswordDialog dialog = new ChangePasswordDialog();
				dialog.setCancelable(true);
				dialog.setCommonDialogListener(new CommonDialogListener()
				{

					@Override
					public void onDialogPositiveClick(DialogFragment arg0)
					{
						Log.i("mbp", "ON OK CLICKED");
						EditText pass = (EditText) dialog
						        .findViewById(R.id.txtNewPassword);

						EditText confirmPass = (EditText) dialog
						        .findViewById(R.id.txtConfirmNewPassword);

						if (pass == null || confirmPass == null)
						{
							return;
						}

						if (pass.length() >= 8 && pass.length() <= 31)
						{
							String pass_str = pass.getText().toString();
							String confirmPass_str = confirmPass.getText()
							        .toString();
							if (pass_str.equals(confirmPass_str))
							{
								update_dialog = new ProgressDialog(activity);
								Spanned msg = Html
								        .fromHtml("<big>"
								                + getResources()
								                        .getString(
								                                R.string.LoginOrRegistrationActivity_conn_bms)
								                + "</big>");
								update_dialog.setMessage(msg);
								update_dialog.setIndeterminate(true);
								update_dialog.show();

								SharedPreferences settings = activity
								        .getSharedPreferences(
								                PublicDefine.PREFS_NAME, 0);
								String saved_token = settings.getString(
								        PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
								        null);
								ChangePasswordTask rename = new ChangePasswordTask();
								rename.executeOnExecutor(
								        AsyncTask.THREAD_POOL_EXECUTOR,
								        saved_token, pass_str);
							}
						}

						dialog.dismiss();

					}

					@Override
					public void onDialogNegativeClick(DialogFragment dialog)
					{

					}
				});

				dialog.show(getFragmentManager(), "Change_Camera_Name");

				return true;
			}
		});

		logOut = cat.findPreference("logOut");

		logOut.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{

			@Override
			public boolean onPreferenceClick(Preference preference)
			{

				((SettingsActivity) getActivity()).onUserLogout();
				getActivity().setResult(HomeScreenActivity.SHOULD_EXIT_NOW_YES);
				getActivity().finish();
				return false;
			}
		});

		PreferenceCategory saveLogCat = (PreferenceCategory) findPreference("pref_key_save_log");
		Preference saveLog = saveLogCat.findPreference("save_log");
		saveLog.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{

			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				// TODO Auto-generated method stub
				// LogCollectorTask logC = new LogCollectorTask(getActivity(),
				// getActivity().getExternalFilesDir(null));
				// logC.execute(new ArrayList<String>());
				sendAppLog();
				return false;
			}
		});

		PreferenceCategory aboutCat = (PreferenceCategory) findPreference("pref_key_about");
		Preference aboutPref = aboutCat.findPreference("app_version");
		String ver = null;
		if (activity != null)
		{
			PackageInfo pinfo;
			try
			{
				pinfo = activity.getPackageManager().getPackageInfo(
				        activity.getPackageName(), 0);
				ver = pinfo.versionName;
			}
			catch (NameNotFoundException e)
			{
				e.printStackTrace();
				ver = "Unknown";
			}
		}
		aboutPref.setSummary(ver);

		PreferenceCategory stunSettings = (PreferenceCategory) findPreference("pref_key_remote_settings");
		final Preference remoteSetting = (Preference) stunSettings
		        .findPreference("string_remote_settings");
		final Preference useRemoteOnlySetting = (Preference) stunSettings
		        .findPreference("string_use_remote_only");
		final Preference disconnectAlertSetting = (Preference) stunSettings
		        .findPreference("string_disconnect_alert_setting");

		isStunEnabled = settings.getBoolean(PublicDefine.PREFS_IS_STUN_ENABLED,
		        false);
		String useStun = null;
		if (isStunEnabled == true)
		{
			useStun = getResources().getString(R.string.Yes);
		}
		else
		{
			useStun = getResources().getString(R.string.No);
		}
		remoteSetting.setSummary(useStun);
		remoteSetting
		        .setOnPreferenceClickListener(new OnPreferenceClickListener()
		        {

			        @Override
			        public boolean onPreferenceClick(Preference preference)
			        {

				        final RemoteConnectionSettingDialog dialog = new RemoteConnectionSettingDialog();

				        dialog.setCommonDialogListener(new CommonDialogListener()
				        {

					        @Override
					        public void onDialogPositiveClick(
					                DialogFragment arg0)
					        {

						        // SharedPreferences settings = getActivity()
						        // .getSharedPreferences(
						        // PublicDefine.PREFS_NAME, 0);
						        // Editor editor = settings.edit();
						        // editor.putBoolean(
						        // PublicDefine.PREFS_IS_STUN_ENABLED,
						        // dialog.isStunEnable());
						        // editor.commit();
						        //
						        // isStunEnabled = dialog.isStunEnable();
						        //
						        // if (isStunEnabled == true)
						        // {
						        // remoteSetting.setSummary(R.string.Yes);
						        // }
						        // else
						        // {
						        // remoteSetting.setSummary(R.string.No);
						        // }
						        dialog.dismiss();
					        }

					        @Override
					        public void onDialogNegativeClick(
					                DialogFragment dialog)
					        {
						        // TODO Auto-generated method stub

					        }
				        });

				        dialog.show(getFragmentManager(),
				                "Dialog_Remote_Connection_Setting");
				        dialog.setStunEnable(isStunEnabled);

				        // dialog.setStunEnable(isStunEnabled);

				        return true;
			        }
		        });

		useRemoteOnly = settings.getBoolean(PublicDefine.PREFS_USE_REMOTE_ONLY,
		        false);
		String useRemoteOnlyStr = null;
		if (useRemoteOnly == true)
		{
			useRemoteOnlyStr = getResources().getString(R.string.Yes);
		}
		else
		{
			useRemoteOnlyStr = getResources().getString(R.string.No);
		}
		useRemoteOnlySetting.setSummary(useRemoteOnlyStr);
		useRemoteOnlySetting
		        .setOnPreferenceClickListener(new OnPreferenceClickListener()
		        {

			        @Override
			        public boolean onPreferenceClick(Preference preference)
			        {

				        final UseRemoteOnlySettingDialg dialog = new UseRemoteOnlySettingDialg();

				        dialog.setCommonDialogListener(new CommonDialogListener()
				        {

					        @Override
					        public void onDialogPositiveClick(
					                DialogFragment arg0)
					        {

						        SharedPreferences settings = getActivity()
						                .getSharedPreferences(
						                        PublicDefine.PREFS_NAME, 0);
						        Editor editor = settings.edit();
						        editor.putBoolean(
						                PublicDefine.PREFS_USE_REMOTE_ONLY,
						                dialog.isEnable());
						        editor.commit();

						        useRemoteOnly = dialog.isEnable();

						        if (useRemoteOnly == true)
						        {
							        useRemoteOnlySetting
							                .setSummary(R.string.Yes);
						        }
						        else
						        {
							        useRemoteOnlySetting
							                .setSummary(R.string.No);
						        }
						        dialog.dismiss();
					        }

					        @Override
					        public void onDialogNegativeClick(
					                DialogFragment dialog)
					        {
						        // TODO Auto-generated method stub

					        }
				        });

				        dialog.show(getFragmentManager(),
				                "Dialog_Use_Remote_Only_Setting");
				        dialog.setEnable(useRemoteOnly);

				        return true;
			        }
		        });

		isDisconnectAlertEnabled = settings.getBoolean(
		        PublicDefine.PREFS_MBP_DISCONNECT_ALERT, true);
		String enableDisconnectAlertStr = null;
		if (isDisconnectAlertEnabled == true)
		{
			enableDisconnectAlertStr = getResources().getString(R.string.Yes);
		}
		else
		{
			enableDisconnectAlertStr = getResources().getString(R.string.No);
		}
		disconnectAlertSetting.setSummary(enableDisconnectAlertStr);
		disconnectAlertSetting
		        .setOnPreferenceClickListener(new OnPreferenceClickListener()
		        {

			        @Override
			        public boolean onPreferenceClick(Preference preference)
			        {

				        final EnableDisconnectAlertDialog dialog = new EnableDisconnectAlertDialog();

				        dialog.setCommonDialogListener(new CommonDialogListener()
				        {

					        @Override
					        public void onDialogPositiveClick(
					                DialogFragment arg0)
					        {

						        SharedPreferences settings = getActivity()
						                .getSharedPreferences(
						                        PublicDefine.PREFS_NAME, 0);
						        Editor editor = settings.edit();
						        editor.putBoolean(
						                PublicDefine.PREFS_MBP_DISCONNECT_ALERT,
						                dialog.isEnable());
						        editor.commit();

						        isDisconnectAlertEnabled = dialog.isEnable();

						        if (isDisconnectAlertEnabled == true)
						        {
							        disconnectAlertSetting
							                .setSummary(R.string.Yes);
						        }
						        else
						        {
							        disconnectAlertSetting
							                .setSummary(R.string.No);
						        }
						        dialog.dismiss();
					        }

					        @Override
					        public void onDialogNegativeClick(
					                DialogFragment dialog)
					        {
						        // TODO Auto-generated method stub

					        }
				        });

				        dialog.show(getFragmentManager(),
				                "Dialog_Enable_Disconnect_Setting");
				        dialog.setEnable(isDisconnectAlertEnabled);

				        return true;
			        }
		        });

		final PreferenceCategory storageSettingsCat = (PreferenceCategory) findPreference("pref_key_plan_settings");
		Preference subscription = storageSettingsCat
		        .findPreference("subscription");
		subscription
		        .setOnPreferenceClickListener(new OnPreferenceClickListener()
		        {
			        @Override
			        public boolean onPreferenceClick(Preference preference)
			        {
				        UpgradeToPremium.openUpgradePage(activity);
				        return true;
			        }
		        });
	}

	private void storeOnSD(StringBuilder log)
	{
		File dir = new File(logFileDir);
		if (!dir.exists())
		{
			dir.mkdirs();
		}

		/* create a data file in the external dir */
		File file = new File(logFilePath);
		if (file.exists())
		{
			/* remove the old file */
			file.delete();
		}

		try
		{
			OutputStream os = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(os);

			pw.write(log.toString());

			pw.close();
			os.close();

		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{

			if (file.exists())
			{
				/* remove the incomplete file */
				Log.e("mbp", "Remove imcompleted file.");
				file.delete();
			}
			e.printStackTrace();
		}
	}

	private String getDebugInfo()
	{
		PackageInfo pInfo;
		try
		{

			String fullInfo = "=======================DEBUG INFORMATION=======================\n";

			Runtime rt = Runtime.getRuntime();
			long maxMemory = rt.maxMemory();

			pInfo = getActivity().getPackageManager().getPackageInfo(
			        getActivity().getPackageName(), 0);
			String appInfo = pInfo.packageName + " version code "
			        + pInfo.versionCode + " version name " + pInfo.versionName;
			fullInfo += "Application information: " + appInfo + "\n";
			fullInfo += "Device name: " + BlinkHDApplication.getDeviceName()
			        + "\n";
			fullInfo += "CPU Information: \n" + BlinkHDApplication.getCpuInfo()
			        + "\n";
			fullInfo += "Memory Information: \n"
			        + BlinkHDApplication.getMemoryInfo() + "\n";
			fullInfo += "Max heap size for me: " + (maxMemory / 1024)
			        + " Kbytes.\n";

			fullInfo += "=======================END DEBUG INFORMATION=======================\n";
			return fullInfo;

		}
		catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}

		return "";
	}

	private void writeLogAndroidDeviceInfo()
	{
		try
		{
			Class<?> c = Build.class;
			for (Field f : c.getDeclaredFields())
			{

				Class<?> t = f.getType();
				if (t == String.class)
				{
					Log.i("mbp", f.getName() + ": " + (String) f.get(null));
				}

			}

			Class<?> version = Build.VERSION.class;

			for (Field f : version.getDeclaredFields())
			{

				Class<?> t = f.getType();
				if (t == String.class)
				{
					Log.i("mbp", f.getName() + ": " + (String) f.get(null));
				}
				else if (t == Integer.class)
				{
					Log.i("mbp", f.getName() + ": " + f.getInt(null));
				}
			}
		}
		catch (Exception x)
		{
			x.printStackTrace();
		}

		DisplayMetrics displaymetrics = new DisplayMetrics();

		WindowManager windowManager = getActivity().getWindowManager();

		windowManager.getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;
		Log.i("mbp", "Screen DPI: " + displaymetrics.densityDpi);
		Log.i("mbp", "Screen resolution " + width + " x " + height + " pixels.");
		Log.i("mbp", "Screen resolution " + width / displaymetrics.density
		        + " x " + height / displaymetrics.density + " dpi.");

	}

	private void sendAppLog()
	{
		try
		{
			final StringBuilder log = new StringBuilder();
			String logcat_cmd = "logcat -d -v time *:s mbp:V FFMpegFileExplorer:v FFMpegPlayerActivity:v "
			        + "FFMpegPlaybackActivity:v mbp.update:V System.err:V ActivityManager:I AndroidRuntime:I "
			        + "FFMpegPlayer-JNI:v FFMpegMediaPlayer-native:v FFMpeg:v ffmpeg_onLoad:v "
			        + "FFMpegMovieViewAndroid:v libffmpeg:v FFMpegMediaPlayer-java:v FFMpegAudioDecoder:v "
			        + "FFMpegVideoDecoder:v FFMpegIDecoder:v System.out:v AudioTrack:v native-Pjnath:v "
			        + "stun_client:v DEBUG:v";
			Process process;

			writeLogAndroidDeviceInfo();
			process = Runtime.getRuntime().exec(logcat_cmd);

			BufferedReader bufferedReader = new BufferedReader(
			        new InputStreamReader(process.getInputStream()));

			String line;
			int lineOfLog = 0;

			while ((line = bufferedReader.readLine()) != null)
			{
				log.append(line);
				log.append(BlinkHDApplication.LINE_SEPARATOR);
				lineOfLog++;
			}

			log.append(getDebugInfo());

			storeOnSD(log);

		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL,
		        new String[] { "androidcrashreport@cvisionhk.com" });
		intent.putExtra(Intent.EXTRA_SUBJECT, "Hubble Home Android App Report");
		intent.putExtra(Intent.EXTRA_TEXT, "[Put your description here]");
		Uri uri = Uri.fromFile(new File(logFilePath));

		intent.putExtra(Intent.EXTRA_STREAM, uri);
		startActivityForResult(Intent.createChooser(intent, "Send email..."),
		        REQUEST_SEND_APP_LOG);
	}

	private void change_password_success(String userToken)
	{
		SharedPreferences settings = activity.getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		Editor editor = settings.edit();
		editor.putString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, userToken);
		editor.commit();

		if (update_dialog != null && update_dialog.isShowing())
		{
			update_dialog.dismiss();
		}
	}

	private void change_password_failed(String error_message)
	{
		if (update_dialog != null && update_dialog.isShowing())
		{
			update_dialog.dismiss();
		}

		AlertDialog.Builder builder;
		AlertDialog alert;
		builder = new AlertDialog.Builder(activity);
		builder.setMessage(error_message)
		        .setCancelable(true)
		        .setPositiveButton(getResources().getString(R.string.OK),
		                new DialogInterface.OnClickListener()
		                {
			                @Override
			                public void onClick(DialogInterface diag, int which)
			                {
				                diag.cancel();
			                }
		                });

		alert = builder.create();
		alert.show();
	}

	private class ChangePasswordTask extends AsyncTask<String, String, Integer>
	{
		private static final int	UPDATE_SUCCESS		             = 0x1;
		private static final int	UPDATE_FAILED_SERVER_UNREACHABLE	= 0x11;
		private static final int	UPDATE_FAILED_WITH_DESC		     = 0x12;

		private String		     userToken;
		private String		     newPassword;
		private String		     newToken		                     = null;
		private String		     _error_desc;

		@Override
		protected Integer doInBackground(String... params)
		{
			// TODO Auto-generated method stub
			userToken = params[0];
			newPassword = params[1];

			int ret = -1;
			try
			{
				UserInformation user_info = User.changePassword(userToken,
				        newPassword);
				if (user_info != null)
				{
					if (user_info.getStatus() == HttpURLConnection.HTTP_OK)
					{
						ret = UPDATE_SUCCESS;
						String userName = user_info.getName();
						try_login(userName, newPassword);
					}
					else
					{
						ret = UPDATE_FAILED_WITH_DESC;
						_error_desc = user_info.getMessage();
					}
				}
			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = UPDATE_FAILED_SERVER_UNREACHABLE;
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = UPDATE_FAILED_SERVER_UNREACHABLE;
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				ret = UPDATE_FAILED_SERVER_UNREACHABLE;
			}

			return new Integer(ret);
		}

		private void try_login(String userName, String userPassword)
		{
			int retries = 5;
			do
			{
				try
				{
					LoginResponse2 login_res = User.login2(userName,
					        userPassword);
					if (login_res != null)
					{
						if (login_res.getStatus() == HttpURLConnection.HTTP_OK)
						{
							String usrToken = login_res
							        .getAuthenticationToken();
							if (usrToken != null)
							{
								newToken = usrToken;
								break;
							}
						}
					}

				}
				catch (SocketTimeoutException e1)
				{
					e1.printStackTrace();
				}
				catch (MalformedURLException e1)
				{
					e1.printStackTrace();
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}

				retries--;

			}
			while (retries > 0);

		}

		@Override
		protected void onPostExecute(Integer result)
		{
			// TODO Auto-generated method stub
			if (result.intValue() == UPDATE_SUCCESS)
			{
				change_password_success(newToken);
			}
			else
			{
				change_password_failed(_error_desc);
			}
		}
	}
}
