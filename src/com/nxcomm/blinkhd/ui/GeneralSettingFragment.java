package com.nxcomm.blinkhd.ui;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.blinkhd.R;

public class GeneralSettingFragment extends Fragment
{
	// more efficient than HashMap for mapping integers to objects
	SparseArray<GeneralSettingGroup>	     groups	             = new SparseArray<GeneralSettingGroup>();
	private MyExpandableListView	         listView;
	private GeneralSettingGroup[]	         settings	         = new GeneralSettingGroup[2];
	private GeneralSettingsExpandableAdapter	adapter;
	private boolean	                         sharedCameraSetting	= false;

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		createData();

		listView = (MyExpandableListView) getActivity().findViewById(
		        R.id.generalSettingListView);
		if (listView != null)
		{
			listView.setGroupIndicator(null);

			adapter = new GeneralSettingsExpandableAdapter(this, groups,
			        listView);

			listView.setAdapter(adapter);
			listView.setPadding(0, 0, 0, 0);

			listView.setOnGroupClickListener(new OnGroupClickListener()
			{

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
				        int groupPosition, long id)
				{
					Log.d("mbp", "Ongroup click");
					GeneralSettingGroup group = (GeneralSettingGroup) adapter
					        .getGroup(groupPosition);
					if (group.isEnable())
						return false;
					else
						return true;
				}
			});
		}

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_general_setting, container,
		        false);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	public void createData()
	{
		// General Settings section
		GeneralSettingGroup generalSettings = new GeneralSettingGroup(
		        getResources().getString(R.string.general_setting),
		        R.drawable.settings_general);
		generalSettings.children.add("dummy");

		groups.append(0, generalSettings);

		// Do not disturb section
		GeneralSettingGroup donotDisturb = new GeneralSettingGroup(
		        getResources().getString(R.string.do_not_disturb),
		        R.drawable.settings_donotdisturb);
		donotDisturb.children.add("dummy");
		groups.append(1, donotDisturb);

		settings[0] = generalSettings;
		settings[1] = donotDisturb;

	}

	public boolean isSharedCameraSetting()
	{
		return sharedCameraSetting;
	}

	public void setSharedCameraSetting(boolean sharedCameraSetting)
	{
		this.sharedCameraSetting = sharedCameraSetting;
	}
}
