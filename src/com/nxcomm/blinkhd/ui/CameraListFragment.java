package com.nxcomm.blinkhd.ui;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.ConnectToNetworkActivity;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.WifiScan;
import com.msc3.registration.SingleCamConfigureActivity;
import com.msc3.registration.UserAccount;
import com.nxcomm.blinkhd.commonasynctask.IAsyncTaskCommonHandler;
import com.nxcomm.blinkhd.ui.customview.ButtonImageCenterView;

import eu.erikw.CameraListView;
import eu.erikw.CameraListView.OnRefreshListener;

public class CameraListFragment extends Fragment implements
        IAsyncTaskCommonHandler, OnItemClickListener
{
	private Activity	            activity	= null;
	private SparseArray<CamProfile>	cameras	 = null;
	private AnimationDrawable	    anim	 = null;
	private RetreiveCameraListTask	task, syncTask;

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		this.activity = activity;
	}

	@Override
	public void onDetach()
	{

		super.onDetach();
		activity = null;
	}

	@Override
	public void onStart()
	{
		super.onStart();
		task = new RetreiveCameraListTask(this, this.getActivity());
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
		        Global.getApiKey(getActivity().getApplicationContext()));
		// loadingDialog = ProgressDialog.show(getActivity(), null,
		// "Loading camera list");

		final CameraListView listView = (CameraListView) getActivity()
		        .findViewById(R.id.cameraSettingListView);
		listView.setVisibility(View.INVISIBLE);
		listView.setOnRefreshListener(new OnRefreshListener()
		{

			@Override
			public void onRefresh()
			{
				// TODO Auto-generated method stub
				final LinearLayout topLayout = (LinearLayout) activity
				        .findViewById(R.id.topLayout);
				if (topLayout != null)
				{
					topLayout.setVisibility(View.INVISIBLE);
				}

				listView.setVisibility(View.INVISIBLE);

				task = new RetreiveCameraListTask(new IAsyncTaskCommonHandler()
				{

					@Override
					public void onPreExecute()
					{
						// TODO Auto-generated method stub

					}

					@Override
					public void onPostExecute(Object result)
					{
						updateCameraListUI(result);
					}

					@Override
					public void onCancelled()
					{
						// TODO Auto-generated method stub

					}
				}, CameraListFragment.this.getActivity());
				task.executeOnExecutor(
				        AsyncTask.THREAD_POOL_EXECUTOR,
				        Global.getApiKey(getActivity().getApplicationContext()),
				        "true");

				ImageView loader = (ImageView) CameraListFragment.this
				        .getActivity().findViewById(R.id.settingImgLoader);
				if (loader != null)
				{
					loader.setBackgroundResource(R.drawable.loader_anim1);
					loader.setVisibility(View.VISIBLE);
					anim = (AnimationDrawable) loader.getBackground();
					// start waiting animation
					anim.start();
				}
				final RelativeLayout loading = (RelativeLayout) getView()
				        .findViewById(R.id.settingImgHolder);
				if (loading != null)
				{
					loading.setVisibility(View.VISIBLE);
				}
			}
		});

		ImageView loader = (ImageView) getActivity().findViewById(
		        R.id.settingImgLoader);
		if (loader != null)
		{
			loader.setBackgroundResource(R.drawable.loader_anim1);
			loader.setVisibility(View.VISIBLE);
			anim = (AnimationDrawable) loader.getBackground();
			// start waiting animation
			anim.start();
		}
		final RelativeLayout loading = (RelativeLayout) getView().findViewById(
		        R.id.settingImgHolder);
		if (loading != null)
		{
			loading.setVisibility(View.VISIBLE);
		}

		final LinearLayout topLayout = (LinearLayout) activity
		        .findViewById(R.id.topLayout);
		if (topLayout != null)
		{
			topLayout.setVisibility(View.INVISIBLE);
		}

		final ButtonImageCenterView addCameraButton = (ButtonImageCenterView) activity
		        .findViewById(R.id.fragment_camera_setting_add_camera_button);

		if (addCameraButton != null)
		{

			addCameraButton.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					if (cameras != null && cameras.size() < 4)
					{
						launchAddCameraAcitivity();
					}
				}

			});
		}

		final ButtonImageCenterView buyCameraButton = (ButtonImageCenterView) activity
		        .findViewById(R.id.fragment_camera_setting_buy_camera_button);

		if (buyCameraButton != null)
		{

			buyCameraButton.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					try
					{
						String url = "http://hubble.in/hubble-products/";
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(url));
						startActivity(i);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
				}

			});
		}

	}

	private void launchAddCameraAcitivity()
	{

		final WifiManager w = (WifiManager) activity
		        .getSystemService(Context.WIFI_SERVICE);
		if (w.getWifiState() == WifiManager.WIFI_STATE_ENABLED
		        || w.getWifiState() == WifiManager.WIFI_STATE_ENABLING)
		{
			SharedPreferences settings = activity.getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);

			Intent cam_conf = new Intent(activity,
			        SingleCamConfigureActivity.class);
			cam_conf.putExtra(SingleCamConfigureActivity.str_userToken,
			        saved_token);
			startActivity(cam_conf);
			activity.finish();
		}
		else
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			Spanned msg = Html
			        .fromHtml("<big>"
			                + getResources()
			                        .getString(
			                                R.string.mobile_data_is_enabled_please_turn_on_wifi_to_add_camera)
			                + "</big>");
			builder.setMessage(msg)
			        .setCancelable(true)
			        .setNegativeButton(
			                getResources().getString(R.string.Cancel),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
				                }
			                })
			        .setPositiveButton(
			                getResources().getString(R.string.turn_on_wifi),
			                new DialogInterface.OnClickListener()
			                {
				                @Override
				                public void onClick(DialogInterface dialog,
				                        int which)
				                {
					                dialog.dismiss();
					                turnOnWifi();

					                SharedPreferences settings = activity
					                        .getSharedPreferences(
					                                PublicDefine.PREFS_NAME, 0);
					                String saved_token = settings
					                        .getString(
					                                PublicDefine.PREFS_SAVED_PORTAL_TOKEN,
					                                null);

					                Intent cam_conf = new Intent(activity,
					                        SingleCamConfigureActivity.class);
					                cam_conf.putExtra(
					                        SingleCamConfigureActivity.str_userToken,
					                        saved_token);
					                startActivity(cam_conf);
					                activity.finish();
				                }
			                });

			AlertDialog alert = builder.create();
			alert.show();
		}

	}

	private void turnOnWifi()
	{
		/* re-use Vox_main, just an empty and transparent activity */
		WifiManager w = (WifiManager) activity
		        .getSystemService(Context.WIFI_SERVICE);
		w.setWifiEnabled(true);
		// while ((w.getWifiState() != WifiManager.WIFI_STATE_ENABLED))
		// {
		// try
		// {
		// Thread.sleep(1000);
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
		// }

	}

	private void updateCameraListUI(Object result)
	{
		if (getView() != null)
		{

			final RelativeLayout loading = (RelativeLayout) getView()
			        .findViewById(R.id.settingImgHolder);
			if (loading != null)
			{
				loading.setVisibility(View.INVISIBLE);
			}
			final ImageView loader = (ImageView) getView().findViewById(
			        R.id.settingImgLoader);
			if (loader != null)
			{
				loader.clearAnimation();
			}

			LinearLayout topLayout = (LinearLayout) getView().findViewById(
			        R.id.topLayout);
			if (topLayout != null)
			{
				topLayout.setVisibility(View.VISIBLE);
			}

			cameras = new SparseArray<CamProfile>();
			cameras = (SparseArray<CamProfile>) result;

			CameraListView listView = (CameraListView) getView().findViewById(
			        R.id.cameraSettingListView);
			listView.onRefreshComplete();
			listView.setVisibility(View.VISIBLE);

			if (getActivity() != null)
			{
				CameraListArrayAdapter adapter = new CameraListArrayAdapter(
				        this.getActivity(), cameras);
				if (listView != null)
				{
					listView.setAdapter(adapter);
					listView.setOnItemClickListener(this);
				}
			}
		}
	}

	@Override
	public void onPostExecute(Object result)
	{
		updateCameraListUI(result);

		syncTask = new RetreiveCameraListTask(new IAsyncTaskCommonHandler()
		{

			@Override
			public void onPreExecute()
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onPostExecute(Object result)
			{
				updateCameraListUI(result);
			}

			@Override
			public void onCancelled()
			{
				// TODO Auto-generated method stub

			}
		}, this.getActivity());

		syncTask.execute(
		        Global.getApiKey(getActivity().getApplicationContext()), "true");

		Log.i("mbp", "Sync camera list online...");

	}

	@Override
	public void onPreExecute()
	{

	}

	@Override
	public void onCancelled()
	{

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fragment_camera_setting, container,
		        false);

		if (v != null)
		{

		}

		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
	{
		Log.d("mbp", "On item clicked.");

	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onDestroyView()
	{
		if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
		{
			Log.d("mbp", "Cancel RetrieveCameraListTask task.");
			task.cancel(true);
		}
		if (syncTask != null
		        && syncTask.getStatus() != AsyncTask.Status.FINISHED)
		{
			syncTask.cancel(true);
		}
		super.onDestroyView();
	}

}

class RetreiveCameraListTask extends
        AsyncTask<String, Void, SparseArray<CamProfile>>
{
	private Context	                mContext;
	private Exception	            exception;
	private IAsyncTaskCommonHandler	handler;
	private boolean	                syncedTask	    = false;
	private boolean	                syncedSucceeded	= false;

	public RetreiveCameraListTask(IAsyncTaskCommonHandler handler,
	        Context mContext)
	{
		this.handler = handler;
		this.mContext = mContext;
	}

	private CamProfile[]	restored_profiles;

	/* restore functions */
	private boolean restore_session_data()
	{

		SetupData savedData = new SetupData();
		try
		{
			if (savedData.restore_session_data(mContext
			        .getExternalFilesDir(null)))
			{
				restored_profiles = savedData.get_CamProfiles();
				return true;
			}
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}
		return false;

	}

	protected SparseArray<CamProfile> doInBackground(String... apiKey)
	{

		UserAccount online_user;
		boolean sync_online = false;
		if (apiKey.length >= 2)
		{
			sync_online = Boolean.parseBoolean(apiKey[1]);
		}

		setSyncedTask(sync_online);
		try
		{
			online_user = new UserAccount(apiKey[0],
			        mContext.getExternalFilesDir(null), null, mContext);
			// Blocking call
			//
			if (sync_online)
			{
				online_user.sync_user_data();
			}

		}
		catch (StorageException e1)
		{
			Log.d("mbp", e1.getLocalizedMessage());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		SparseArray<CamProfile> cameras = new SparseArray<CamProfile>();

		if (restore_session_data() == true)
		{

			if (restored_profiles != null && restored_profiles.length > 0)
			{
				for (int i = 0; i < restored_profiles.length; i++)
				{
					cameras.append(i, restored_profiles[i]);
				}

			}
		}

		return cameras;
	}

	@Override
	protected void onPostExecute(SparseArray<CamProfile> result)
	{

		if (this.handler != null)
		{
			setSyncedTask(true);
			if (result instanceof SparseArray<?>)
			{
				setSyncedSucceeded(true);
			}

			this.handler.onPostExecute(result);
		}
		super.onPostExecute(result);
	}

	public boolean isSyncedTask()
	{
		return syncedTask;
	}

	public void setSyncedTask(boolean syncedTask)
	{
		this.syncedTask = syncedTask;
	}

	public boolean isSyncedSucceeded()
	{
		return syncedSucceeded;
	}

	public void setSyncedSucceeded(boolean syncedSucceeded)
	{
		this.syncedSucceeded = syncedSucceeded;
	}

}
