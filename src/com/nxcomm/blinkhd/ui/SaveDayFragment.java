package com.nxcomm.blinkhd.ui;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.blinkhd.R;
import com.cvision.eventtimeliner.EventManagerFragment;
import com.msc3.CamChannel;

public class SaveDayFragment extends Fragment
{
	private CamChannel selected_channel;
	private SparseArray<SavedDay>	savedDays	= new SparseArray<SavedDay>();
	private EventManagerFragment frg = new EventManagerFragment();
	private void initDummyData()
	{
		savedDays.append(0, new SavedDay("Window"));
		savedDays.append(1, new SavedDay("Door"));
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_saved_day, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		initDummyData();
		ListView listView = (ListView) getActivity().findViewById(
				R.id.saved_day_fragment_listview);
		SavedDayArrayAdapter adapter = new SavedDayArrayAdapter(
				this.getActivity(), savedDays);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener()
		
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3)
			{
				FragmentTransaction trans = getActivity().getFragmentManager()
						.beginTransaction();
				frg.setChannel(selected_channel);
				trans.setCustomAnimations(R.anim.card_flip_left_in, R.anim.card_flip_left_out);
				trans.replace(R.id.fragmentHolder, frg);
				//trans.addToBackStack(null);
				trans.commit();
				
			}
		});
	}
	
	public void setChannel(CamChannel s_channel)
	{
		selected_channel = s_channel;
	}
}
