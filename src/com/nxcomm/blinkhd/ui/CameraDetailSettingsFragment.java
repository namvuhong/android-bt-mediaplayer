package com.nxcomm.blinkhd.ui;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.msc3.Util;
import com.msc3.comm.HTTPRequestSendRecv;
import com.nxcomm.blinkhd.commonasynctask.CheckCamIsAvailableAsyncTask;
import com.nxcomm.blinkhd.commonasynctask.GetCameraInfoAsyncTask;
import com.nxcomm.blinkhd.commonasynctask.IAsyncTaskCommonHandler;
import com.nxcomm.blinkhd.commonasynctask.SendCommandAsyncTask;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;
import com.nxcomm.meapi.device.CameraInfo;
import com.nxcomm.meapi.device.SendCommandResponse;

public class CameraDetailSettingsFragment extends Fragment
{
	// more efficient than HashMap for mapping integers to objects
	SparseArray<GeneralSettingGroup>	    groups	            = new SparseArray<GeneralSettingGroup>();
	private MyExpandableListView	        listView;

	private GeneralSettingGroup[]	        settings;
	private String	                        apiKey, deviceID;
	private DetailSettingsExpandableAdapter	adapter;

	private GetCameraInfoAsyncTask	        getCameraInfoAsyncTask;
	private SendCommandAsyncTask	        getFwVersionTask;
	private CameraBasicInfo	                cameraBasicInfo;
	private boolean	                        sharedCameraSetting	= false;
	private ProgressDialog	                removeCameraDialog;
	private static final String	            SHARED_CAM_PREFIX	= "02";

	private String	                        cameraName, fwVersion;
	private boolean	                        isOnline, isUpgrading;

	private void stopAsyncTask(AsyncTask task)
	{
		if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
		{
			task.cancel(true);
		}
	}

	private void stopAllAsyncTask()
	{
		stopAsyncTask(getCameraInfoAsyncTask);
		stopAsyncTask(getFwVersionTask);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("mbp", "On activity result on fragment " + requestCode
		        + " result code " + resultCode);

		if (resultCode == Activity.RESULT_OK)
		{

			if (requestCode == DetailSettingsExpandableAdapter.SELECT_PICTURE)
			{

				Uri selectedImageUri = data.getData();
				String selectedImagePath = getPath(selectedImageUri);
				Log.d("mbp", "selected image: " + selectedImagePath);
				if (selectedImagePath != null)
				{
					save_camera_snap(selectedImagePath);
				}
			}
			else if (requestCode == DetailSettingsExpandableAdapter.TAKE_SNAPSHOT)
			{

				// Uri snapUri = data.getData();
				// String snapPath = getPath(snapUri);
				// Log.d("mbp", "take snapshot path: " + snapPath);
				String snapFileName = Util.getRootRecordingDirectory()
				        + File.separator + getSnapshotNameHashed(deviceID)
				        + ".jpg";
				save_camera_snap(snapFileName);

			}

		}

	}

	public String getPath(Uri uri)
	{
		// just some safety built in
		if (uri == null)
		{
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(this.getActivity(), uri,
		        projection, null, null, null);
		Cursor cursor = loader.loadInBackground();
		if (cursor != null)
		{
			int column_index = cursor
			        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	private void save_camera_snap(String path)
	{
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opts);

		// Calculate inSampleSize
		opts.inSampleSize = calculateInSampleSize(opts, 200);
		// Decode bitmap with inSampleSize set
		opts.inJustDecodeBounds = false;
		Bitmap savedBitmap = BitmapFactory.decodeFile(path, opts);
		ExifInterface exif = null;
		try
		{
			exif = new ExifInterface(path);
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (exif != null)
		{
			int orientation = exif.getAttributeInt(
			        ExifInterface.TAG_ORIENTATION,
			        ExifInterface.ORIENTATION_NORMAL);
			int rotate = 0;
			switch (orientation)
			{
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}
			Matrix matrix = new Matrix();
			matrix.postRotate(rotate);
			savedBitmap = Bitmap.createBitmap(savedBitmap, 0, 0,
			        savedBitmap.getWidth(), savedBitmap.getHeight(), matrix,
			        true);
		}

		String snapFileName = Util.getRootRecordingDirectory() + File.separator
		        + getSnapshotNameHashed(deviceID) + ".jpg";
		File snap_file = new File(snapFileName);
		if (snap_file.exists())
		{
			snap_file.delete();
		}

		BufferedOutputStream bos = null;
		try
		{
			bos = new BufferedOutputStream(new FileOutputStream(snapFileName));
			savedBitmap.compress(CompressFormat.JPEG, 75, bos);
			bos.flush();
			bos.close();

		}
		catch (IOException e)
		{
			// todo: error handling
		}
	}

	/**
	 * @param options
	 *            options contain width & height of the image which we want to
	 *            scaled
	 * @param reqHeight
	 *            desired height after scaled
	 * @return inSampleSize for scaling
	 */
	private int calculateInSampleSize(BitmapFactory.Options options,
	        int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		// final int width = options.outWidth;
		Log.i("mbp", "Width x Height = " + options.outHeight);
		int inSampleSize = 1;
		if (height > reqHeight)
		{

			final int halfHeight = height / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight)
			{
				inSampleSize *= 2;
			}
		}

		Log.i("mbp", "Insample size " + inSampleSize);
		return inSampleSize;
	}

	public static String getSnapshotNameHashed(String str)
	{
		String hashed_str = null;
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] bytes = digest.digest(str.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte b : bytes)
			{
				sb.append(String.format("%02X", b));
			}
			hashed_str = sb.toString();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return hashed_str;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		createData();

		listView = (MyExpandableListView) getActivity().findViewById(
		        R.id.generalSettingListView);
		if (listView != null)
		{
			listView.setGroupIndicator(null);

			adapter = new DetailSettingsExpandableAdapter(this, groups,
			        listView, apiKey, deviceID);

			listView.setAdapter(adapter);
			listView.setPadding(0, 0, 0, 0);

			listView.setOnGroupClickListener(new OnGroupClickListener()
			{

				@Override
				public boolean onGroupClick(ExpandableListView parent, View v,
				        int groupPosition, long id)
				{
					Log.d("mbp", "Ongroup click");
					GeneralSettingGroup group = (GeneralSettingGroup) adapter
					        .getGroup(groupPosition);
					if (groupPosition == 2)
					{
						Log.d("mbp", "Remove camera called.");

						AlertDialog.Builder builder = new AlertDialog.Builder(
						        getActivity());
						builder.setMessage(R.string.remove_camera_confirm)
						        .setPositiveButton(R.string.remove,
						                new DialogInterface.OnClickListener()
						                {
							                public void onClick(
							                        DialogInterface dialog,
							                        int id)
							                {
								                removeCameraDialog = ProgressDialog
								                        .show(getActivity(),
								                                null,
								                                getResources()
								                                        .getString(
								                                                R.string.remove_camera_notification));
								                removeCameraDialog
								                        .setCancelable(false);

								                RemoveCameraTask try_delCam = new RemoveCameraTask();
								                // if
								                // (!cameraProfile.isInLocal())
								                {
									                try_delCam
									                        .setOnlyRemoveFrServer(true);
								                }
								                try_delCam.execute(apiKey,
								                        deviceID);
							                }
						                })
						        .setNegativeButton(R.string.Cancel,
						                new DialogInterface.OnClickListener()
						                {
							                public void onClick(
							                        DialogInterface dialog,
							                        int id)
							                {

							                }
						                });

						AlertDialog removeConfirmDialog = builder.create();
						removeConfirmDialog.setCancelable(false);
						removeConfirmDialog.setTitle(R.string.remove_camera);
						removeConfirmDialog
						        .setIcon(android.R.drawable.ic_delete);
						removeConfirmDialog.show();

						return true;
					}

					if (group.isEnable())
						return false;
					else
						return true;
				}
			});
		}

		cameraBasicInfo = new CameraBasicInfo();

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_general_setting, container,
		        false);

	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub
		super.onStart();
		// SharedPreferences sharePrefs = getActivity().getSharedPreferences(
		// PublicDefine.PREFS_NAME, 0);

		/*
		 * if (deviceID != null) { this.checkCamIsAvailableAsyncTask = new
		 * CheckCamIsAvailableAsyncTask( this);
		 * this.checkCamIsAvailableAsyncTask.execute(apiKey, deviceID); }
		 */

		if (cameraBasicInfo != null)
		{

			// Camera name
			if (cameraName != null)
			{
				cameraBasicInfo.setName(cameraName);
			}
			else
			{
				cameraBasicInfo.setName(getResources().getString(
				        R.string.Not_Available));
			}

			// Camera firmware version
			if (fwVersion != null)
			{
				cameraBasicInfo.setFwVersion(fwVersion);
			}
			else
			{
				cameraBasicInfo.setName(getResources().getString(
				        R.string.Not_Available));
			}

			// Camera status & upgrading
			cameraBasicInfo.setAvailable(isOnline);
			cameraBasicInfo.setUpgrading(isUpgrading);
			cameraBasicInfo.setRegistratrionID(deviceID);
			
			if(isOnline)
			{
				if(settings[1]!= null)
				{
					settings[1].setEnable(true);
				}
			}
			else
			{
				if(settings[1]!= null)
				{
					settings[1].setEnable(false);
				}
			}

			if (adapter != null)
			{
				adapter.setCameraBasicInfo(cameraBasicInfo);
				adapter.notifyDataSetChanged();
			}

		}
		else
		{
			Log.i("mbp",
			        "Camera basic information is null. Plz create one first.");
		}

		// don't check online any more
		/*
		 * if (deviceID != null && apiKey != null) { getFwVersionTask = new
		 * SendCommandAsyncTask( new IAsyncTaskCommonHandler() {
		 * 
		 * @Override public void onPreExecute() { // TODO Auto-generated method
		 * stub
		 * 
		 * }
		 * 
		 * @Override public void onPostExecute(Object result) {
		 * 
		 * if (result != null && result instanceof SendCommandResponse) {
		 * SendCommandResponse res = (SendCommandResponse) result; String ret =
		 * res.getSendCommandResponseData() .getDevice_response().getBody();
		 * 
		 * if (ret.contains(PublicDefine.GET_VERSION)) {
		 * cameraBasicInfo.setFwVersion(ret.replace( PublicDefine.GET_VERSION +
		 * ":", "") .trim()); } else { Log.d("mbp",
		 * "Get firmware version failed. Result is " + result);
		 * cameraBasicInfo.setFwVersion(getResources()
		 * .getString(R.string.Not_Available)); } } else { Log.d("mbp",
		 * "Get firmware version failed.");
		 * cameraBasicInfo.setFwVersion(getResources()
		 * .getString(R.string.Not_Available)); }
		 * 
		 * }
		 * 
		 * @Override public void onCancelled() { // TODO Auto-generated method
		 * stub
		 * 
		 * } });
		 * 
		 * getFwVersionTask.execute(apiKey, deviceID,
		 * PublicDefine.BM_HTTP_CMD_PART + PublicDefine.GET_VERSION, "");
		 * 
		 * getCameraInfoAsyncTask = new GetCameraInfoAsyncTask( new
		 * IAsyncTaskCommonHandler() {
		 * 
		 * @Override public void onPreExecute() { // TODO Auto-generated method
		 * stub
		 * 
		 * }
		 * 
		 * @Override public void onPostExecute(Object result) { if (result !=
		 * null && result instanceof CameraInfo) { CameraInfo cameraInfo =
		 * (CameraInfo) result; cameraBasicInfo.setName(cameraInfo.getName());
		 * cameraBasicInfo.setAvailable(cameraInfo .isIs_available());
		 * cameraBasicInfo.setRegistratrionID(cameraInfo .getRegistration_id());
		 * 
		 * boolean ret = cameraBasicInfo.isAvailable();
		 * 
		 * settings[0].setEnable(true);
		 * 
		 * if (!ret) { // camera is not available Log.d("mbp", "Camera " +
		 * deviceID + " is not available.");
		 * 
		 * } else { if (!isSharedCam(deviceID) && settings.length > 1) {
		 * settings[1].setEnable(true); } Log.d("mbp", "Camera " + deviceID +
		 * " is available."); }
		 * 
		 * if (adapter != null) { adapter.setCameraBasicInfo(cameraBasicInfo);
		 * adapter.notifyDataSetChanged(); }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * @Override public void onCancelled() { // TODO Auto-generated method
		 * stub
		 * 
		 * } });
		 * 
		 * getCameraInfoAsyncTask.execute(apiKey, deviceID);
		 * 
		 * }
		 */

	}

	@Override
	public void onAttach(Activity activity)
	{
		apiKey = getArguments().getString(
		        CameraDetailSettingActivity2.EXTRA_API_KEY);
		deviceID = getArguments().getString(
		        CameraDetailSettingActivity2.EXTRA_REGISTRATION_ID);

		cameraName = getArguments().getString(
		        CameraDetailSettingActivity2.EXTRA_CAMERA_NAME, null);
		fwVersion = getArguments().getString(
		        CameraDetailSettingActivity2.EXTRA_FIRMWARE_VERSION, null);

		isOnline = getArguments().getBoolean(
		        CameraDetailSettingActivity2.EXTRA_CAMERA_IS_ONLINE, false);
		isUpgrading = getArguments().getBoolean(
		        CameraDetailSettingActivity2.EXTRA_CAMERA_IS_UPGRADING, false);

		super.onAttach(activity);

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onDestroy()
	{
		stopAllAsyncTask();
		super.onDestroy();
	}

	private boolean isSharedCam(String deviceID)
	{
		boolean result = false;
		if (deviceID.startsWith(SHARED_CAM_PREFIX))
		{
			result = true;
		}
		return result;
	}

	public void createData()
	{
		if (deviceID != null)
		{
			// General Settings section
			if (!isSharedCam(deviceID))
			{
				settings = new GeneralSettingGroup[2];
				GeneralSettingGroup cameraGeneralInformation = new GeneralSettingGroup(
				        getResources().getString(R.string.camera_details),
				        R.drawable.settings_general,
				        R.drawable.ic_action_settings_general_disable);
				cameraGeneralInformation.children.add("dummy");

				groups.append(0, cameraGeneralInformation);

				// Notification Sensitity section
				GeneralSettingGroup notificationSensitity = new GeneralSettingGroup(
				        getResources().getString(
				                R.string.notification_sensitivity),
				        R.drawable.settings_notification,
				        R.drawable.settings_notification_disable);
				notificationSensitity.children.add("dummy");
				groups.append(1, notificationSensitity);

				settings[0] = cameraGeneralInformation;
				settings[1] = notificationSensitity;

			}
			else
			{
				settings = new GeneralSettingGroup[2];
				GeneralSettingGroup cameraGeneralInformation = new GeneralSettingGroup(
				        getResources().getString(R.string.camera_details),
				        R.drawable.settings_general,
				        R.drawable.ic_action_settings_general_disable);
				cameraGeneralInformation.children.add("dummy");

				groups.append(0, cameraGeneralInformation);

				settings[0] = cameraGeneralInformation;
				settings[0].setEnable(false);

			}

			// Notification Scheduler section
			// GeneralSettingGroup notificationSheduler = new
			// GeneralSettingGroup(
			// getResources().getString(R.string.notification_sheduler),
			// R.drawable.settings_scheduler,
			// R.drawable.settings_scheduler_disable);
			// notificationSheduler.children.add("dummy");
			// groups.append(3, notificationSheduler);

			/*
			 * GeneralSettingGroup removeCamera = new GeneralSettingGroup(
			 * getResources().getString(R.string.remove_camera),
			 * android.R.drawable.ic_delete);
			 * 
			 * removeCamera.children.add("dummy");
			 * 
			 * groups.append(2, removeCamera);
			 */

		}

	}

	public boolean isSharedCameraSetting()
	{
		return sharedCameraSetting;
	}

	public void setSharedCameraSetting(boolean sharedCameraSetting)
	{
		this.sharedCameraSetting = sharedCameraSetting;
	}

	class RemoveCameraTask extends AsyncTask<String, String, Integer>
	{

		private String		     usrToken;
		private String		     regId;
		private String		     camName;

		private boolean		     only_remove_from_server;

		private static final int	DEL_CAM_SUCCESS		       = 0x1;
		private static final int	DEL_CAM_FAILED_UNKNOWN		= 0x2;
		private static final int	DEL_CAM_FAILED_SERVER_DOWN	= 0x11;

		public RemoveCameraTask()
		{
			only_remove_from_server = false;
		}

		public void setOnlyRemoveFrServer(boolean b)
		{
			only_remove_from_server = b;
		}

		/*
		 * Dont need to support UDT because Remove camera in remote will only
		 * remove camera from server
		 */
		@Override
		protected Integer doInBackground(String... params)
		{

			usrToken = params[0];
			regId = params[1];

			String http_usr, http_pass, http_cmd;

			if (only_remove_from_server == false)
			{
				String device_ip_and_port = params[2];
				/*
				 * 20120911: OBSOLETE: switch_to_uAP mode & reset
				 * 
				 * reset_factory & restart_system
				 */

				http_usr = params[3];
				http_pass = params[4];

				String http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESET_FACTORY);
				Log.d("mbp", "Remove camera, send reset factory");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESTART_DEVICE);
				Log.d("mbp", "Remove camera, send restart system");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				/*
				 * SEND THIS CMD UNTIL WE DON'T GET THE RESPONSE That means the
				 * device is already reset
				 */
				String test_conn = null;

				do
				{
					http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
					        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
					        PublicDefine.GET_VERSION);
					test_conn = HTTPRequestSendRecv
					        .sendRequest_block_for_response(http_addr,
					                http_usr, http_pass);
				}
				while (test_conn != null);

			}

			int ret = -1;
			try
			{
				SimpleJsonResponse response = Device.delete(usrToken, regId);
				if (response != null && response.isSucceed())
				{
					int status_code = response.getStatus();
					Log.d("mbp", "Del cam response code: " + status_code);
					if (status_code == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = DEL_CAM_FAILED_UNKNOWN;
					}
					else if (status_code == HttpURLConnection.HTTP_OK)
					{
						ret = DEL_CAM_SUCCESS;
					}
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (SocketTimeoutException e)
			{
				// Connection Timeout - Server unreachable ???
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (removeCameraDialog != null & removeCameraDialog.isShowing())
			{
				removeCameraDialog.dismiss();
			}

			if (result.intValue() == DEL_CAM_SUCCESS)
			{
				if (getActivity() != null)
				{
					getActivity().finish();
				}

			}
			else
			{
				Log.i("mbp", "Remove camera failed.");

				AlertDialog.Builder builder = new AlertDialog.Builder(
				        getActivity());
				builder.setMessage(R.string.remove_camera_failed)
				        .setNegativeButton(R.string.OK,
				                new DialogInterface.OnClickListener()
				                {
					                public void onClick(DialogInterface dialog,
					                        int id)
					                {

					                }
				                });

				builder.create().show();

			}

		}

	}

}
