package com.nxcomm.blinkhd.ui;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.blinkhd.R;
import com.msc3.CamProfile;
import com.msc3.ChangeNameTask;
import com.msc3.IChangeNameCallBack;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.StorageException;
import com.msc3.Util;
import com.msc3.comm.HTTPRequestSendRecv;
import com.msc3.comm.UDTRequestSendRecv;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.msc3.registration.UserAccount;
import com.nxcomm.blinkhd.ui.dialog.ChangeCameraNameDialog;
import com.nxcomm.blinkhd.ui.dialog.ChangeImageDialog;
import com.nxcomm.blinkhd.ui.dialog.CommonDialog;
import com.nxcomm.blinkhd.ui.dialog.CommonDialogListener;
import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.SimpleJsonResponse;

public class CameraDetailSettingActivity extends Activity implements
        IChangeNameCallBack
{
	public static final String	EXTRA_REGISTRATION_ID	= "string_registration_id";
	public static final String	EXTRA_SNAPSHOT_HEIGHT	= "int_snapshot_height";

	private CamProfile[]	   restored_profiles;

	private CameraInfoFragment	info;

	private static final int	SELECT_PICTURE	      = 1;
	private static final int	TAKE_SNAPSHOT	      = 2;

	private CamProfile	       cameraProfile;
	private String	           device_name;
	private String	           regId;
	private int	               snapshotHeight;
	private ProgressDialog	   progressDialog, changeNameDialog;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings
		        .getString(PublicDefine.PREFS_SAVED_SERVER_URL,
		                "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;

		setContentView(R.layout.camera_info_setting);

		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);
		setTitle(R.string.menu);
		actionBar.setDisplayHomeAsUpEnabled(true);
		FragmentTransaction fragmentTransact = getFragmentManager()
		        .beginTransaction();

		info = new CameraInfoFragment();

		fragmentTransact.replace(R.id.cameraDetailPreference, info);
		fragmentTransact.commit();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		// Respond to the action bar's Up/Home button
		case android.R.id.home:

			finish();
			overridePendingTransition(R.anim.ac_slide_in_from_right,
			        R.anim.ac_slide_out_from_right_to_left);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void camera_name_length_is_out_of_range()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String msg = getResources().getString(R.string.camera_name_length_is_out_of_boundary);
		builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener()
			{
				
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
		builder.create().show();
	}
	
	private void camera_name_is_invalid()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String msg = getResources().getString(R.string.camera_name_is_invalid);
		builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener()
			{
				
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
		builder.create().show();
	}
	
	public void onStart()
	{
		super.onStart();

		Bundle extra = getIntent().getExtras();
		regId = null;

		if (extra != null)
		{
			regId =  extra.getString(EXTRA_REGISTRATION_ID);
			snapshotHeight = extra.getInt(EXTRA_SNAPSHOT_HEIGHT);
		}

		cameraProfile = null;
		if (restore_session_data() == true && regId != null)
		{

			for (CamProfile cp1 : restored_profiles)
			{
				if (cp1.getRegistrationId().equalsIgnoreCase(regId))
				{
					cameraProfile = cp1;
					break;
				}
			}

		}

		if (cameraProfile != null)
		{
			queryFwVersion();

			Preference name = (Preference) info
			        .findPreference("string_cam_name");
			name.setSummary(cameraProfile.getName());
			name.setOnPreferenceClickListener(new OnPreferenceClickListener()
			{

				@Override
				public boolean onPreferenceClick(Preference preference)
				{

					final ChangeCameraNameDialog dialog = new ChangeCameraNameDialog();
					dialog.show(getFragmentManager(),
					        "Dialog_Change_Camera_Name");
					dialog.setCommonDialogListener(new CommonDialogListener()
					{

						@Override
						public void onDialogPositiveClick(DialogFragment arg0)
						{
							dialog.dismiss();
							EditText text = (EditText) dialog
							        .findViewById(R.id.dialog_change_camera_name_CameraName_EditText);
							if (text == null)
							{
								return;
							}

							String camera_name = text.getText().toString()
							        .trim();
							if (camera_name != null
							        && camera_name.length() >= 5
							        && camera_name.length() <= 30)
							{
								if (LoginOrRegistrationActivity.validate(
										LoginOrRegistrationActivity.CAMERA_NAME, camera_name))
								{
									changeNameDialog = ProgressDialog.show(
											CameraDetailSettingActivity.this, null, 
											getApplicationContext().getResources().getString(
													R.string.changing_camera_name), true, false);
									
									device_name = camera_name;
									SharedPreferences settings = getSharedPreferences(
											PublicDefine.PREFS_NAME, 0);
									String saved_token = settings.getString(
											PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
									ChangeNameTask rename = new ChangeNameTask(
											CameraDetailSettingActivity.this,
											CameraDetailSettingActivity.this);
									rename.execute(saved_token, device_name, regId);
								}
								else
								{
									camera_name_is_invalid();
								}
							}
							else
							{
								camera_name_length_is_out_of_range();
							}
						}

						@Override
						public void onDialogNegativeClick(DialogFragment dialog)
						{
						}
					});

					return true;
				}
			});

			Log.d("mbp", " image link: " + cameraProfile.getImageLink());
			Preference changeImage = (Preference) info
			        .findPreference("string_change_image");
			changeImage
			        .setOnPreferenceClickListener(new OnPreferenceClickListener()
			        {

				        @Override
				        public boolean onPreferenceClick(Preference preference)
				        {

					        // Change the camera image
					        // in onCreate or any event where your want the user
					        // to
					        // select a file
					        // Intent intent = new Intent(Intent.ACTION_PICK,
					        // MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					        // startActivityForResult(intent, SELECT_PICTURE);

					        final ChangeImageDialog dialog = new ChangeImageDialog();

					        dialog.setCommonDialogListener(new CommonDialogListener()
					        {

						        @Override
						        public void onDialogPositiveClick(
						                DialogFragment arg0)
						        {
							        if (dialog.getSelectImageSource() == ChangeImageDialog.SELECT_IMAGE_FROM_GALLERY)
							        {
								        Intent intent = new Intent(
								                Intent.ACTION_PICK,
								                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
								        startActivityForResult(intent,
								                SELECT_PICTURE);
							        }
							        else
							        {
								        Intent intent = new Intent(
								                MediaStore.ACTION_IMAGE_CAPTURE);
								        String regId = cameraProfile
								                .getRegistrationId();
								        String snapFileName = Util
								                .getRootRecordingDirectory()
								                + File.separator
								                + getSnapshotNameHashed(regId)
								                + ".jpg";
								        Uri fileUri = Uri.fromFile(new File(
								                snapFileName));
								        intent.putExtra(
								                MediaStore.EXTRA_OUTPUT,
								                fileUri);
								        startActivityForResult(intent,
								                TAKE_SNAPSHOT);
							        }
							        dialog.dismiss();

						        }

						        @Override
						        public void onDialogNegativeClick(
						                DialogFragment dialog)
						        {
							        // TODO Auto-generated method stub

						        }

					        });
					        dialog.show(getFragmentManager(),
					                "Dialog_Change_Image");

					        return true;
				        }
			        });

			// Preference fw_version = (Preference)
			// info.findPreference("string_fw_version");
			// fw_version.setSummary(cameraProfile.getFirmwareVersion());

			Button removeCamera = (Button) findViewById(R.id.removeCamera);
			removeCamera.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0)
				{
					progressDialog = ProgressDialog.show(CameraDetailSettingActivity.this, null, getApplicationContext().getResources().getString(R.string.remove_camera_notification), true, false);
					
					String portal_usrToken = Global
					        .getApiKey(getApplicationContext());

					RemoveCameraTask try_delCam = new RemoveCameraTask();
					//if (!cameraProfile.isInLocal())
					{
						try_delCam.setOnlyRemoveFrServer(true);
					}
					try_delCam.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 
							portal_usrToken, cameraProfile.getRegistrationId());

				}
			});

		}

	}

	private void queryFwVersion()
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				String ver = null;
				if (cameraProfile.isInLocal())
				{
					try
					{
						ver = HTTPRequestSendRecv.getFirmwareVersion(
						        cameraProfile.get_inetAddress()
						                .getHostAddress(), String
						                .valueOf(cameraProfile.get_port()),
						        PublicDefine.DEFAULT_BASIC_AUTH_USR,
						        PublicDefine.DEFAULT_CAM_PWD);
					}
					catch (SocketException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					String cmd = PublicDefine.BM_HTTP_CMD_PART
					        + PublicDefine.GET_VERSION;
					SharedPreferences settings = getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					String saved_token = settings.getString(
					        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
					ver = UDTRequestSendRecv.sendRequest_via_stun2(saved_token,
					        cameraProfile.getRegistrationId(), cmd);
				}

				if (ver != null && ver.startsWith(PublicDefine.GET_VERSION))
				{
					ver = ver.substring(PublicDefine.GET_VERSION.length() + 2);
				}
				else
				{
					ver = "NA";
				}
				cameraProfile.setFirmwareVersion(ver);

				runOnUiThread(new Runnable()
				{

					@Override
					public void run()
					{
						// TODO Auto-generated method stub
						Preference fw_version = (Preference) info
						        .findPreference("string_fw_version");
						fw_version.setSummary(cameraProfile
						        .getFirmwareVersion());
					}
				});

			}
		}).start();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode == RESULT_OK)
		{
			if (requestCode == SELECT_PICTURE)
			{
				Uri selectedImageUri = data.getData();
				String selectedImagePath = getPath(selectedImageUri);
				Log.d("mbp", "selected image: " + selectedImagePath);
				if (selectedImagePath != null)
				{
					save_camera_snap(selectedImagePath);
				}
			}
			else if (requestCode == TAKE_SNAPSHOT)
			{
				String snapFileName = Util.getRootRecordingDirectory()
						+ File.separator + getSnapshotNameHashed(regId) + ".jpg";
				save_camera_snap(snapFileName);
			}
		}
	}

	private void save_camera_snap(String path)
	{
		final BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opts);

		// Calculate inSampleSize
		opts.inSampleSize = calculateInSampleSize(opts, snapshotHeight);
		// Decode bitmap with inSampleSize set
		opts.inJustDecodeBounds = false;
		Bitmap savedBitmap = BitmapFactory.decodeFile(path, opts);
		ExifInterface exif = null;
        try
        {
	        exif = new ExifInterface(path);
        }
        catch (IOException e1)
        {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
        }
        
        if (exif != null)
        {
        	int orientation = exif.getAttributeInt(
        			ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        	int rotate = 0;
        	switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            }
        	Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            savedBitmap = Bitmap.createBitmap(
            		savedBitmap , 0, 0, savedBitmap.getWidth(), 
            		savedBitmap.getHeight(), matrix, true);
        }
        
		String snapFileName = Util.getRootRecordingDirectory() + File.separator
		        + getSnapshotNameHashed(regId) + ".jpg";
		File snap_file = new File(snapFileName);
		if (snap_file.exists())
		{
			snap_file.delete();
		}
		
		BufferedOutputStream bos = null;
		try
		{
			bos = new BufferedOutputStream(new FileOutputStream(snapFileName));
			savedBitmap.compress(CompressFormat.JPEG, 75, bos);
			bos.flush();
			bos.close();

		}
		catch (IOException e)
		{
			// todo: error handling
		}
	}

	/**
	 * @param options
	 *            options contain width & height of the image which we want to
	 *            scaled
	 * @param reqHeight
	 *            desired height after scaled
	 * @return inSampleSize for scaling
	 */
	private int calculateInSampleSize(BitmapFactory.Options options,
	        int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		// final int width = options.outWidth;
		int inSampleSize = 1;
		if (height > reqHeight)
		{
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((height / inSampleSize) > reqHeight)
			{
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static String getSnapshotNameHashed(String str)
	{
		String hashed_str = null;
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] bytes = digest.digest(str.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte b : bytes)
			{
				sb.append(String.format("%02X", b));
			}
			hashed_str = sb.toString();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return hashed_str;
	}

	/* mainly to save the ip addresses */
	private synchronized void save_session_data(CamProfile cp)
	{

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, true);
		editor.commit();

		SetupData savedData = new SetupData();

		try
		{
			savedData.restore_session_data(getExternalFilesDir(null));
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}

		CamProfile[] restored_profile = savedData.get_CamProfiles();

		for (int i = 0; i < restored_profile.length; i++)
		{
			if ((restored_profile[i] != null)
			        && cp.get_MAC().equalsIgnoreCase(
			                restored_profile[i].get_MAC()))
			{
				// update profile...
				restored_profile[i].setImageLink(cp.getImageLink());
			}
		}

		savedData.set_CamProfiles(restored_profile);
		savedData.save_session_data(getExternalFilesDir(null));

		editor.putBoolean(PublicDefine.PREFS_READ_WRITE_OFFLINE_DATA, false);
		editor.commit();
	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri)
	{
		// just some safety built in
		if (uri == null)
		{
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(this, uri, projection, null,
		        null, null);
		Cursor cursor = loader.loadInBackground();
		if (cursor != null)
		{
			int column_index = cursor
			        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	public void onDestroy()
	{
		super.onDestroy();
	}

	/* restore functions */
	private boolean restore_session_data()
	{

		SetupData savedData = new SetupData();
		try
		{
			if (savedData.restore_session_data(getExternalFilesDir(null)))
			{
				restored_profiles = savedData.get_CamProfiles();

				return true;
			}
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}
		return false;

	}

	private void del_cam_failed()
	{
		setResult(SettingsActivity.CAMERA_NOT_REMOVED);
		this.finish();
	}

	private void del_cam_success()
	{
		setResult(SettingsActivity.CAMERA_REMOVED_OK);
		String snapFileName = Util.getRootRecordingDirectory() + File.separator
		        + CameraDetailSettingActivity.getSnapshotNameHashed(regId)
		        + ".jpg";
		File snap = new File(snapFileName);
		if (snap.exists())
		{
			snap.delete();
		}

		this.finish();
	}

	class RemoveCameraTask extends AsyncTask<String, String, Integer>
	{

		private String		     usrToken;
		private String		     regId;
		private String		     camName;

		private boolean		     only_remove_from_server;

		private static final int	DEL_CAM_SUCCESS		       = 0x1;
		private static final int	DEL_CAM_FAILED_UNKNOWN		= 0x2;
		private static final int	DEL_CAM_FAILED_SERVER_DOWN	= 0x11;

		public RemoveCameraTask()
		{
			only_remove_from_server = false;
		}

		public void setOnlyRemoveFrServer(boolean b)
		{
			only_remove_from_server = b;
		}

		/*
		 * Dont need to support UDT because Remove camera in remote will only
		 * remove camera from server
		 */
		@Override
		protected Integer doInBackground(String... params)
		{

			usrToken = params[0];
			regId = params[1];

			String http_usr, http_pass, http_cmd;

			if (only_remove_from_server == false)
			{
				String device_ip_and_port = params[2];
				/*
				 * 20120911: OBSOLETE: switch_to_uAP mode & reset
				 * 
				 * reset_factory & restart_system
				 */

				http_usr = params[3];
				http_pass = params[4];

				String http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESET_FACTORY);
				Log.d("mbp", "Remove camera, send reset factory");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
				        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
				        PublicDefine.RESTART_DEVICE);
				Log.d("mbp", "Remove camera, send restart system");
				HTTPRequestSendRecv.sendRequest_block_for_response(http_addr,
				        http_usr, http_pass);

				/*
				 * SEND THIS CMD UNTIL WE DON'T GET THE RESPONSE That means the
				 * device is already reset
				 */
				String test_conn = null;

				do
				{
					http_addr = String.format("%1$s%2$s%3$s%4$s", "http://",
					        device_ip_and_port, PublicDefine.HTTP_CMD_PART,
					        PublicDefine.GET_VERSION);
					test_conn = HTTPRequestSendRecv
					        .sendRequest_block_for_response(http_addr,
					                http_usr, http_pass);
				}
				while (test_conn != null);

			}

			int ret = -1;
			try
			{
				SimpleJsonResponse response = Device.delete(usrToken, regId);
				if (response != null && response.isSucceed())
				{
					int status_code = response.getStatus();
					Log.d("mbp", "Del cam response code: " + status_code);
					if (status_code == HttpURLConnection.HTTP_ACCEPTED)
					{
						ret = DEL_CAM_FAILED_UNKNOWN;
					}
					else if (status_code == HttpURLConnection.HTTP_OK)
					{
						ret = DEL_CAM_SUCCESS;
					}
				}
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (SocketTimeoutException e)
			{
				// Connection Timeout - Server unreachable ???
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				ret = DEL_CAM_FAILED_SERVER_DOWN;
			}

			return new Integer(ret);
		}

		/* UI thread */
		protected void onPostExecute(Integer result)
		{
			if (result.intValue() == DEL_CAM_SUCCESS)
			{
				del_cam_success();
			}
			else
			{
				del_cam_failed();
			}
			
			if(progressDialog != null & progressDialog.isShowing())
			{
				progressDialog.dismiss();
			}
		}

	}

	@Override
	public void update_cam_success()
	{
		if (changeNameDialog != null && changeNameDialog.isShowing())
		{
			changeNameDialog.dismiss();
		}
		
		if (info != null)
		{
			Preference pref = info
					.findPreference("string_cam_name");
			pref.setSummary(device_name);
		}
		
	}

	@Override
	public void update_cam_failed()
	{
		if (changeNameDialog != null && changeNameDialog.isShowing())
		{
			changeNameDialog.dismiss();
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String msg = getResources().getString(R.string.change_camera_name_failed);
		builder.setMessage(msg)
			.setCancelable(true)
			.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener()
			{
				
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
		builder.create().show();
	}
}
