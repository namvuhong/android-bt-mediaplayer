package com.nxcomm.blinkhd.ui;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;

import com.blinkhd.R;
import com.blinkhd.SupportModel;
import com.blinkhd.gcm.AlertData;
import com.blinkhd.gcm.GcmIntentService;
import com.msc3.CamChannel;
import com.msc3.CamProfile;
import com.msc3.PublicDefine;
import com.msc3.SetupData;
import com.msc3.Util;
import com.msc3.VoiceActivationService;
import com.msc3.registration.LoginOrRegistrationActivity;
import com.nxcomm.blinkhd.ui.customview.TabbedMenuItem;
import com.nxcomm.meapi.App;

public class SettingsActivity extends Activity implements OnClickListener
{
	public static final int	       CAMERA_REMOVE_STATUS	  = 100;
	public static final int	       CAMERA_REMOVED_OK	  = 1;
	public static final int	       CAMERA_NOT_REMOVED	  = 2;

	private CamChannel	           selected_channel	      = null;

	public static final String	   EXTRA_SELECTED_CHANNEL	= "extra_selected_channel";

	private CameraListFragment	cameraSettingFragment;
	private GeneralSettingFragment	generalSettingFragment;
	private AccountSettingFragment	accountSettingFragment;
	private TabbedMenuItem	       cameraSettingMenuItem,
	        generalSettingMenuItem, accountSettingMenuItem;

	private boolean	               isActivityDestroyed;

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

		// Force portrait on Phone
		if (!getResources().getBoolean(R.bool.isTablet))
		{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		/* added to force url */
		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		String serverUrl = settings.getString(
		        PublicDefine.PREFS_SAVED_SERVER_URL,
		        "https://api.hubble.in/v1");
		com.nxcomm.meapi.PublicDefines.SERVER_URL = serverUrl;

		setContentView(R.layout.activity_settings);
		// overridePendingTransition(R.anim.activity_slide_in_from_left,
		// R.anim.activity_slide_in_to_left);

		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// setup action bar for tabs
		Bundle extras = getIntent().getExtras();
		if (extras != null)
		{
			selected_channel = (CamChannel) extras
			        .getSerializable(EXTRA_SELECTED_CHANNEL);
		}

		isActivityDestroyed = false;

		ActionBar actionBar = getActionBar();

		cameraSettingFragment = new CameraListFragment();
		generalSettingFragment = new GeneralSettingFragment();
		accountSettingFragment = new AccountSettingFragment();

		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			// NavUtils.navigateUpFromSameTask(this);

			finish();
			overridePendingTransition(R.anim.ac_slide_in_from_right,
			        R.anim.ac_slide_out_from_right_to_left);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void unregisterWithGCM(final String api_token)
	{

		Log.d("mbp", "unregister app via Me-Api Json.");
		// unregisterWithBMS
		SharedPreferences set = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		final int appID = (int) set.getLong(
		        PublicDefine.PREFS_PUSH_NOTIFICATION_APP_ID, 0);

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub
				try
				{
					App.unregisterNotification(api_token, appID);
					App.unregisterApp(api_token, appID);
				}
				catch (SocketTimeoutException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (MalformedURLException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();

		String phoneModel = android.os.Build.MODEL;
		// register if it's not iHome Phone
		if (SupportModel.doesPhoneModelSupportGCM(phoneModel))
		{

			Intent unregIntent = new Intent(
			        "com.google.android.c2dm.intent.UNREGISTER");

			// Put it into bundle, because we will remove it from PREFERENCES
			// after
			// this call..
			Intent pendingIntent = new Intent();
			pendingIntent.putExtra("api_token", api_token);

			unregIntent.putExtra("app",
			        PendingIntent.getBroadcast(this, 0, pendingIntent, 0));

			startService(unregIntent);
		}
	}

	private void stopVoxService()
	{
		if (LoginOrRegistrationActivity.isVOXServiceRunning(this))
		{
			Intent i = new Intent(this, VoiceActivationService.class);
			stopService(i);
		}
	}

	public void onUserLogout()
	{
		stopVoxService();

		SharedPreferences settings = getSharedPreferences(
		        PublicDefine.PREFS_NAME, 0);
		boolean offlineMode = settings.getBoolean(
		        PublicDefine.PREFS_USER_ACCES_INFRA_OFFLINE, false);
		if (offlineMode == false)
		{

			// vox service should not take wakelock
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean(PublicDefine.PREFS_VOX_SHOULD_TAKE_WAKELOCK,
			        false);
			// remove password when user logout
			editor.remove(PublicDefine.PREFS_TEMP_PORTAL_PWD);

			String api_token = settings.getString(
			        PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			unregisterWithGCM(api_token);

			Intent new_login = new Intent(this,
			        LoginOrRegistrationActivity.class);
			new_login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(new_login);

			// Remove offline data.
			new SetupData().clear_session_data(getExternalFilesDir(null));

			// Remove all unread alerts
			Log.d("mbp", "Delete all unread alerts & clear notifications");
			AlertData
			        .purgeAlertsNotFromCameras(null, getExternalFilesDir(null));

			// Remove all pending notification on Status bar
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			int i = 0;
			int current_id;
			while (i < GcmIntentService.PUSH_IDs.length)
			{
				current_id = GcmIntentService.PUSH_IDs[i];
				notificationManager.cancel(current_id);
				i++;
			}

			editor.remove(PublicDefine.PREFS_SAVED_PORTAL_TOKEN);
			editor.commit();
		}
	}

	@Override
	protected void onPause()
	{
		// TODO Auto-generated method stub
		super.onPause();
		// overridePendingTransition(R.anim.activity_open_translate,
		// R.anim.activity_close_translate);

	}

	protected void onStart()
	{
		super.onStart();
		// RelativeLayout topLayout = (RelativeLayout)
		// findViewById(R.id.topLayout);
		// if (topLayout != null)
		// {
		// topLayout.removeAllViews();
		// }
	}

	@Override
	protected void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu items for use in the action bar MenuInflater
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.setting_activity_action_bar_button, menu);

		/*
		 * Camera Settings TabbedMenuItem 'Camera'
		 */
		cameraSettingMenuItem = (TabbedMenuItem) menu.findItem(
		        R.id.btnCameraSetting).getActionView();
		cameraSettingMenuItem.setOnClickListener(this);
		cameraSettingMenuItem.setText(R.string.camera);
		cameraSettingMenuItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		/*
		 * General Settings TabbedMenuItem 'Settings'
		 */
		generalSettingMenuItem = (TabbedMenuItem) menu.findItem(
		        R.id.btnGeneralSetting).getActionView();
		generalSettingMenuItem.setOnClickListener(this);
		generalSettingMenuItem.setText(R.string.settings);
		generalSettingMenuItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

		/*
		 * Account Settings TabbedMenuItem 'Account'
		 */
		accountSettingMenuItem = (TabbedMenuItem) menu.findItem(
		        R.id.btnAccountSetting).getActionView();
		accountSettingMenuItem.setOnClickListener(this);
		accountSettingMenuItem.setText(R.string.account);
		accountSettingMenuItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

		cameraSettingMenuItem.setChecked(true);
		if (selected_channel == null
		        || selected_channel.getCamProfile() == null
		        || selected_channel.getCamProfile().isSharedCam())
		{
			//menu.removeItem(R.id.btnGeneralSetting);
			
			accountSettingMenuItem.setChecked(false);
		}
		else
		{
			generalSettingMenuItem.setChecked(false);
			accountSettingMenuItem.setChecked(false);
		}
		
		
		this.transitionFragmentFromLeft(cameraSettingFragment);

		/* 20131221:phung : generate a press event to turn on the marker */
		// cameraSettingMenuItem.postDelayed( new Runnable() {
		//
		// @Override
		// public void run()
		// {
		// ((View) cameraSettingMenuItem).dispatchTouchEvent(
		// MotionEvent.obtain(10, 10, MotionEvent.ACTION_DOWN, 0,0 , 0));
		// ((View) cameraSettingMenuItem).dispatchTouchEvent(
		// MotionEvent.obtain(10, 10, MotionEvent.ACTION_UP, 0,0 , 0));
		// }
		// }, 500);

		return super.onCreateOptionsMenu(menu);

	}

	public void goCameraTab()
	{
		if(cameraSettingMenuItem != null)
		{
			cameraSettingMenuItem.performClick();
		}
	}
	@Override
	protected void onDestroy()
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		isActivityDestroyed = true;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		/*
		Log.d("mbp", "On activity result on fragment. Request code "
		        + requestCode + " result code " + resultCode);
		if (requestCode == DetailSettingsExpandableAdapter.SELECT_PICTURE)
		{
			if (resultCode == RESULT_OK)
			{
				Uri selectedImageUri = data.getData();
				String selectedImagePath = getPath(selectedImageUri);
				Log.d("mbp", "selected image: " + selectedImagePath);
				if (selectedImagePath != null)
				{
					save_camera_snap(selectedImagePath);
				}
			}

		}
		else if (requestCode == DetailSettingsExpandableAdapter.TAKE_SNAPSHOT)
		{
			if (resultCode == RESULT_OK)
			{

//				Uri snapUri = data.getData();
//				String snapPath = getPath(snapUri);
//				Log.d("mbp", "take snapshot path: " + snapPath);
				String snapFileName = Util.getRootRecordingDirectory()
						+ File.separator + getSnapshotNameHashed(regId) + ".jpg";
				save_camera_snap(snapFileName);
			}
		}
		else*/ if (requestCode == CAMERA_REMOVE_STATUS)
		{
			if (resultCode == CAMERA_REMOVED_OK)
			{
				// Camera remove - Refresh the camera list
				// ActionBar actionBar = getActionBar();
				// actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
				// actionBar.removeAllTabs();
				//
				// Tab tab = actionBar
				// .newTab()
				// .setText(R.string.camera)
				// .setTabListener(
				// new TabListener<CameraListFragment>(this, "camera",
				// CameraListFragment.class));
				// actionBar.addTab(tab);
				//
				// tab = actionBar
				// .newTab()
				// .setText(R.string.settings)
				// .setTabListener(
				// new TabListener<GeneralSettingFragment>(this,
				// "settings", GeneralSettingFragment.class));
				// actionBar.addTab(tab);
				//
				// tab = actionBar
				// .newTab()
				// .setText(R.string.account)
				// .setTabListener(
				// new TabListener<AccountSettingFragment>(this,
				// "account", AccountSettingFragment.class));
				//
				// actionBar.addTab(tab);

			}
		}
	}

	public static class TabListener<T extends Fragment> implements
	        ActionBar.TabListener
	{
		private Fragment		mFragment;
		private final Activity	mActivity;
		private final String	mTag;
		private final Class<T>	mClass;

		/**
		 * Constructor used each time a new tab is created.
		 * 
		 * @param activity
		 *            The host Activity, used to instantiate the fragment
		 * @param tag
		 *            The identifier tag for the fragment
		 * @param clz
		 *            The fragment's Class, used to instantiate the fragment
		 */
		public TabListener(Activity activity, String tag, Class<T> clz)
		{
			mActivity = activity;
			mTag = tag;
			mClass = clz;
		}

		/* The following are each of the ActionBar.TabListener callbacks */

		public void onTabSelected(Tab tab, FragmentTransaction ft)
		{
			// Check if the fragment is already initialized
			if (mFragment == null)
			{
				// If not, instantiate and add it to the activity
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				ft.replace(android.R.id.content, mFragment, mTag);
			}
			else
			{
				// If it exists, simply attach it in order to show it
				ft.attach(mFragment);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft)
		{
			if (mFragment != null)
			{
				// Detach the fragment, because another one is being attached
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft)
		{
			// User selected the already selected tab. Usually do nothing.
		}
	}

	private void transitionFragmentFromLeft(Fragment frag)
	{
		if (isActivityDestroyed == false)
		{
			FragmentManager fragmentManager = getFragmentManager();

			FragmentTransaction transaction = fragmentManager
			        .beginTransaction();

			transaction.setCustomAnimations(R.anim.frag_slide_in_from_left,
			        R.anim.frag_slide_out_from_left);

			transaction.replace(R.id.settings_fragment_holder, frag);

			// transaction.commit();
			transaction.commitAllowingStateLoss();
		}
	}

	private void transitionFragmentFromRight(Fragment frag)
	{
		if (isActivityDestroyed == false)
		{
			FragmentManager fragmentManager = getFragmentManager();

			FragmentTransaction transaction = fragmentManager
			        .beginTransaction();

			transaction.setCustomAnimations(R.anim.frag_slide_in_from_right,
			        R.anim.frag_slide_out_from_right);

			transaction.replace(R.id.settings_fragment_holder, frag);

			// transaction.commit();
			transaction.commitAllowingStateLoss();
		}
	}

	@Override
	public void onClick(View v)
	{
		if (v == cameraSettingMenuItem)
		{
			accountSettingMenuItem.setChecked(false);
			generalSettingMenuItem.setChecked(false);
			((TabbedMenuItem) v).setChecked(true);

			this.transitionFragmentFromLeft(cameraSettingFragment);
		}
		else if (v == accountSettingMenuItem)
		{
			cameraSettingMenuItem.setChecked(false);
			generalSettingMenuItem.setChecked(false);
			((TabbedMenuItem) v).setChecked(true);

			this.transitionFragmentFromRight(accountSettingFragment);
		}
		else if (v == generalSettingMenuItem)
		{
			cameraSettingMenuItem.setChecked(false);
			accountSettingMenuItem.setChecked(false);
			((TabbedMenuItem) v).setChecked(true);

			TabbedMenuItem tabs[] = { cameraSettingMenuItem,
			        accountSettingMenuItem, generalSettingMenuItem };
			
			TabbedMenuItem selectedTab = null;
			for (TabbedMenuItem tab : tabs)
			{
				selectedTab = tab;
				if (tab.isChecked())
				{
					break;
				}
			}
			
			if (selectedTab == cameraSettingMenuItem)
			{
				this.transitionFragmentFromRight(generalSettingFragment);
			}
			else if (selectedTab == accountSettingMenuItem)
			{
				this.transitionFragmentFromLeft(generalSettingFragment);
			}
			else
			{
				this.transitionFragmentFromLeft(generalSettingFragment);
			}
			
			/*
			if (selected_channel != null
			        && CamProfile.shouldEnableMic(selected_channel
			                .getCamProfile().getModelId()))
			{
				TabbedMenuItem tabs[] = { cameraSettingMenuItem,
				        accountSettingMenuItem, generalSettingMenuItem };

				TabbedMenuItem selectedTab = null;
				for (TabbedMenuItem tab : tabs)
				{
					selectedTab = tab;
					if (tab.isChecked())
					{
						break;
					}
				}

				cameraSettingMenuItem.setChecked(false);
				accountSettingMenuItem.setChecked(false);
				((TabbedMenuItem) v).setChecked(true);

				if (selectedTab == cameraSettingMenuItem)
				{
					this.transitionFragmentFromRight(generalSettingFragment);
				}
				else if (selectedTab == accountSettingMenuItem)
				{
					this.transitionFragmentFromLeft(generalSettingFragment);
				}
				else
				{
					this.transitionFragmentFromLeft(generalSettingFragment);
				}
			}
			*/
			
		}
	}
}
