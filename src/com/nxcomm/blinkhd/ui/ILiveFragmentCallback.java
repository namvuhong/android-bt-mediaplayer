package com.nxcomm.blinkhd.ui;

public interface ILiveFragmentCallback {
	public void goToFullScreen();
	public void setupScaleDetector();
	public void setupOnTouchEvent();
}
