package com.nxcomm.blinkhd.ui;

import java.util.ArrayList;
import java.util.List;

public class GeneralSettingGroup
{

	private String	          string;
	public final List<String>	children	    = new ArrayList<String>();

	private int	              drawableID	    = 0;
	private int	              drawableIdDisable	= 0;
	private boolean	          enable	        = true;

	public GeneralSettingGroup(String string, int drawableID)
	{
		this.string = string;
		this.drawableID = drawableID;
	}

	public GeneralSettingGroup(String string, int enableDrawableID,
	        int disableDrawableID)
	{
		this.string = string;
		this.drawableID = enableDrawableID;
		this.drawableIdDisable = disableDrawableID;
	}

	public String getText()
	{
		return string;
	}

	public int getDrawableID()
	{
		if (enable)
		{
			return drawableID;
		}
		
		return drawableIdDisable;
	}

	public void setDrawableID(int drawableID)
	{
		this.drawableID = drawableID;
	}

	public boolean isEnable()
	{
		return enable;
	}

	public void setEnable(boolean enable)
	{
		this.enable = enable;
	}

	public int getDrawableIdDisable()
	{
		return drawableIdDisable;
	}

	public void setDrawableIdDisable(int drawableIdDisable)
	{
		this.drawableIdDisable = drawableIdDisable;
	}

}
