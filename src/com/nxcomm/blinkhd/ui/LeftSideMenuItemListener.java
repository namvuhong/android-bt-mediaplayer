package com.nxcomm.blinkhd.ui;

public interface LeftSideMenuItemListener {
	public void onPan(boolean isEnable);
	public void onMic(boolean isEnable);
	public void onRecord(boolean isEnable);
	public void onMelody(boolean isEnable);
	public void onTemperature(boolean isEnable);
	public void onSnap();
}
