package com.nxcomm.blinkhd.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.blinkhd.FontManager;
import com.blinkhd.R;
import com.msc3.PublicDefine;
import com.nxcomm.blinkhd.ui.customview.ArtProgressView;
import com.nxcomm.blinkhd.ui.customview.OnStateChangeListener;
import com.nxcomm.blinkhd.ui.customview.SwitchableImageView;

public class GeneralSettingsExpandableAdapter extends BaseExpandableListAdapter

{

	private final SparseArray<GeneralSettingGroup>	groups;
	public LayoutInflater	                       inflater;
	public Activity	                               activity;
	private Fragment	                           fragment;

	private MyExpandableListView	               listView;

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	public GeneralSettingsExpandableAdapter(Fragment act,
	        SparseArray<GeneralSettingGroup> groups,
	        MyExpandableListView listView)
	{
		this.listView = listView;
		activity = act.getActivity();
		this.groups = groups;
		this.fragment = act;

		inflater = LayoutInflater.from(activity);
		// inflater = LayoutInflater.from(act.getActivity());

	}

	@Override
	public Object getChild(int groupPosition, int childPosition)
	{
		return groups.get(groupPosition).children.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition)
	{
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
	        boolean isLastChild, View convertView, ViewGroup parent)
	{

		final String children = (String) getChild(groupPosition, childPosition);
		TextView text = null;
		if (!groups.get(groupPosition).isEnable())
		{
			return new View(activity.getApplicationContext());
		}

		if (convertView == null)
		{
			if (groupPosition == 0)
			{
				convertView = inflater.inflate(
				        R.layout.general_setting_clock_temperature, null);
				setupGeneral(convertView);
			}

			else if (groupPosition == 1)
			{
				convertView = inflater.inflate(R.layout.do_not_disturb, null);
				// setupProgressWheel(convertView);
				setupDonotDisturb(convertView);

			}
			else
			{
				convertView = inflater.inflate(
				        R.layout.listrow_detail_general_setting, null);
				text = (TextView) convertView.findViewById(R.id.txtCamOne);
				text.setText(children);
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Toast.makeText(activity, children, Toast.LENGTH_SHORT)
						        .show();
					}
				});
			}
		}
		else
		{
			if (groupPosition == 0)
			{
				convertView = inflater.inflate(
				        R.layout.general_setting_clock_temperature, null);
				setupGeneral(convertView);
			}

			else if (groupPosition == 1)
			{
				convertView = inflater.inflate(R.layout.do_not_disturb, null);
				setupDonotDisturb(convertView);
			}
			else
			{
				convertView = inflater.inflate(
				        R.layout.listrow_detail_general_setting, null);
				text = (TextView) convertView.findViewById(R.id.txtCamOne);
				text.setText(children);
				convertView.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Toast.makeText(activity, children, Toast.LENGTH_SHORT)
						        .show();
					}
				});
			}
		}

		return convertView;
	}

	private void setupDonotDisturb(View convertView)
	{

		final SwitchableImageView switchDonotDisturbOnOff = (SwitchableImageView) convertView
		        .findViewById(R.id.switchDonotDisturbOnOff);
		final ArtProgressView artProgressView = (ArtProgressView) convertView
		        .findViewById(R.id.artProgressViewDonotDisturb);

		final SharedPreferences sharePrefs = this.activity
		        .getSharedPreferences(PublicDefine.PREFS_NAME, 0);
		final SharedPreferences.Editor sharePrefsEditor = sharePrefs.edit();

		boolean isDonotDisturbEnable = sharePrefs.getBoolean(
		        PublicDefine.PREFS_IS_DO_NOT_DISTURB_ENABLE, false);

		long timeDonotDisturbExpired = sharePrefs.getLong(
		        PublicDefine.PREFS_TIME_DO_NOT_DISTURB_EXPIRED, 0);
		long currentTime = System.currentTimeMillis();
		long remainTime = 0;

		if (isDonotDisturbEnable)
		{
			remainTime = timeDonotDisturbExpired - currentTime;

			if (remainTime < 0)
			{
				remainTime = 0;
			}
			else
			{
				remainTime = (long) (remainTime / (60 * 1000.0f));
			}
		}
		else
		{
			remainTime = sharePrefs.getInt(
			        PublicDefine.PREFS_DO_NOT_DISTURB_REMAINING_TIME, 0);
		}
		Log.d("mbp", "Remaning time of do not disturb " + remainTime);

		artProgressView.setCurrentValue((int) remainTime);

		switchDonotDisturbOnOff
		        .setOnStateChangeListener(new OnStateChangeListener()
		        {

			        @Override
			        public void onStateChanged(View v, int newValue,
			                int oldValue)
			        {
				        if (newValue == SwitchableImageView.STATE_ONE) // ON
				        {
					        artProgressView.setViewEnable(true);
				        }
				        else
				        {
					        artProgressView.setViewEnable(false);
					        sharePrefsEditor
					                .putInt(PublicDefine.PREFS_DO_NOT_DISTURB_REMAINING_TIME,
					                        artProgressView.getCurrentValue());
				        }

				        sharePrefsEditor.putBoolean(
				                PublicDefine.PREFS_IS_DO_NOT_DISTURB_ENABLE,
				                switchDonotDisturbOnOff.getBooleanState());
				        sharePrefsEditor.commit();
			        }
		        });
		switchDonotDisturbOnOff.setBooleanState(isDonotDisturbEnable);
		artProgressView
		        .setOnAdjustValueCompeletedListener(new ArtProgressView.OnAdjustValueCompeletedListener()
		        {

			        @Override
			        public void onAdjustValueCompeleted(float newValue)
			        {
				        // TODO Auto-generated method stub
				        long timeDonotDisturbExpired = (long) (System
				                .currentTimeMillis() + newValue * 1000 * 60);
				        sharePrefsEditor.putLong(
				                PublicDefine.PREFS_TIME_DO_NOT_DISTURB_EXPIRED,
				                timeDonotDisturbExpired);
				        sharePrefsEditor.commit();
				        Log.d("mbp", "Time do not disturb expired change to "
				                + timeDonotDisturbExpired);
			        }
		        });
	}

	private void setupGeneral(View convertView)
	{
		final SwitchableImageView switchClockMode = (SwitchableImageView) convertView
		        .findViewById(R.id.imgCamThree);
		final SwitchableImageView switchTempUnit = (SwitchableImageView) convertView
		        .findViewById(R.id.imageView2);

		if (activity != null)
		{
			SharedPreferences sharePrefs = activity.getSharedPreferences(
			        PublicDefine.PREFS_NAME, 0);
			int tempUnit = sharePrefs.getInt(
			        PublicDefine.PREFS_TEMPERATURE_UNIT,
			        PublicDefine.TEMPERATURE_UNIT_DEG_C);
			if (tempUnit == PublicDefine.TEMPERATURE_UNIT_DEG_C)
			{
				switchTempUnit.setState(SwitchableImageView.STATE_ONE);
			}
			else
			{
				switchTempUnit.setState(SwitchableImageView.STATE_TWO);
			}

			int clockMode = sharePrefs.getInt(PublicDefine.PREFS_CLOCK_MODE,
			        PublicDefine.CLOCK_MODE_12H);
			if (clockMode == PublicDefine.CLOCK_MODE_12H)
			{
				switchClockMode.setState(SwitchableImageView.STATE_ONE);
			}
			else
			{
				switchClockMode.setState(SwitchableImageView.STATE_TWO);
			}
		}

		switchClockMode.setOnStateChangeListener(new OnStateChangeListener()
		{

			@Override
			public void onStateChanged(View v, int newValue, int oldValue)
			{
				// TODO Auto-generated method stub
				int clockMode;
				switch (switchClockMode.getState())
				{
				case SwitchableImageView.STATE_ONE:
					clockMode = PublicDefine.CLOCK_MODE_12H;
					break;

				case SwitchableImageView.STATE_TWO:
					clockMode = PublicDefine.CLOCK_MODE_24H;
					break;

				default:
					clockMode = PublicDefine.CLOCK_MODE_12H;
					break;
				}

				if (activity != null)
				{
					SharedPreferences settings = activity.getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					Editor editor = settings.edit();
					editor.putInt(PublicDefine.PREFS_CLOCK_MODE, clockMode);
					editor.commit();
				}
			}
		});

		switchTempUnit.setOnStateChangeListener(new OnStateChangeListener()
		{

			@Override
			public void onStateChanged(View v, int newValue, int oldValue)
			{
				// TODO Auto-generated method stub
				int tempUnit;
				switch (switchTempUnit.getState())
				{
				case SwitchableImageView.STATE_ONE:
					tempUnit = PublicDefine.TEMPERATURE_UNIT_DEG_C;
					break;

				case SwitchableImageView.STATE_TWO:
					tempUnit = PublicDefine.TEMPERATURE_UNIT_DEG_F;
					break;
				default:
					tempUnit = PublicDefine.TEMPERATURE_UNIT_DEG_C;
					break;
				}

				if (activity != null)
				{
					SharedPreferences settings = activity.getSharedPreferences(
					        PublicDefine.PREFS_NAME, 0);
					Editor editor = settings.edit();
					editor.putInt(PublicDefine.PREFS_TEMPERATURE_UNIT, tempUnit);
					editor.commit();
				}
			}
		});
	}

	@Override
	public int getChildrenCount(int groupPosition)
	{

		return groups.get(groupPosition).children.size();
	}

	@Override
	public Object getGroup(int groupPosition)
	{
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount()
	{
		return groups.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition)
	{
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition)
	{
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition)
	{
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
	        View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = inflater.inflate(
			        R.layout.listrow_group_general_setting, null);
		}
		GeneralSettingGroup group = (GeneralSettingGroup) getGroup(groupPosition);

		Typeface regTf = FontManager.fontRegular;// Typeface.createFromAsset(activity.getAssets(),
		// "fonts/ProximaNova-Regular.otf");

		((CheckedTextView) convertView).setText(group.getText());
		((CheckedTextView) convertView).setChecked(isExpanded);
		((CheckedTextView) convertView).setTypeface(regTf);

		if (group.isEnable())
		{
			Drawable img = this.activity.getApplicationContext().getResources()
			        .getDrawable(group.getDrawableID());

			if (img == null)
				Log.d("mbp", "Active image is NULL");
			else
				((CheckedTextView) convertView)
				        .setCompoundDrawablesWithIntrinsicBounds(img, null,
				                null, null);
		}
		else
		{
			Drawable img = this.activity.getApplicationContext().getResources()
			        .getDrawable(group.getDrawableIdDisable());

			if (img == null)
				Log.d("mbp", "Disable image is NULL");
			else
				((CheckedTextView) convertView)
				        .setCompoundDrawablesWithIntrinsicBounds(img, null,
				                null, null);
		}
		return convertView;
	}

	@Override
	protected void finalize() throws Throwable
	{
		Log.d("mbp", "Finalize CustomExpandable Adapter");
		super.finalize();
	}

	@Override
	public boolean hasStableIds()
	{
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}
}
