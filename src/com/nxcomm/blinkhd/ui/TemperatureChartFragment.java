package com.nxcomm.blinkhd.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import com.blinkhd.R;
import com.blinkhd.TemperatureData;

public class TemperatureChartFragment extends Fragment
{
	private List<TemperatureData>	tempData	= new ArrayList<TemperatureData>();
	
	public TemperatureChartFragment()
	{
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		setRetainInstance(true);
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_temperature_chart, container,
				false);
	}
	
	public List<TemperatureData> getTempData()
	{
		return tempData;
	}
	
	public void setTempData(List<TemperatureData> tempData)
	{
		this.tempData = tempData;
	}
	
	public void openChart()
	{
		if (!this.tempData.isEmpty())
		{
			
			DateFormat df = new SimpleDateFormat("MMM dd");
			
			List<Float> morning = new ArrayList<Float>();
			List<Float> afternoon = new ArrayList<Float>();
			List<Float> evening = new ArrayList<Float>();
			
			for (TemperatureData data : tempData)
			{
				morning.add(data.getMorningAvgTemp());
				afternoon.add(data.getAfternoonAvgTemp());
				evening.add(data.getEveningAvgTemp());
				
			}
			// Creating an XYSeries for Morning
			// CategorySeries incomeSeries = new CategorySeries("Income");
			XYSeries morningSeries = new XYSeries("Morning");
			// Creating an XYSeries for Afternoon
			XYSeries afternoonSeries = new XYSeries("Afternoon");
			// Creating an XYSeries for Evening
			XYSeries eveningSeries = new XYSeries("Evening");
			// Adding data to Income and Expense Series
			
			for (int i = 0; i < tempData.size(); i++)
			{
				morningSeries.add(i, morning.get(i));
				afternoonSeries.add(i, afternoon.get(i));
				eveningSeries.add(i, evening.get(i));
			}
			
			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			
			// Adding Income Series to the dataset
			dataset.addSeries(morningSeries);
			// Adding Expense Series to dataset
			dataset.addSeries(afternoonSeries);
			dataset.addSeries(eveningSeries);
			
			// Creating XYSeriesRenderer to customize incomeSeries
			XYSeriesRenderer morningRender = new XYSeriesRenderer();
			morningRender.setColor(Color.CYAN);
			morningRender.setFillPoints(true);
			morningRender.setLineWidth(2);
			morningRender.setDisplayChartValues(true);
			
			// Creating XYSeriesRenderer to customize expenseSeries
			XYSeriesRenderer afternoonRender = new XYSeriesRenderer();
			afternoonRender.setColor(Color.MAGENTA);
			afternoonRender.setFillPoints(true);
			afternoonRender.setLineWidth(2);
			afternoonRender.setDisplayChartValues(true);
			
			// Creating XYSeriesRenderer to customize expenseSeries
			XYSeriesRenderer eveningRender = new XYSeriesRenderer();
			eveningRender.setColor(Color.YELLOW);
			eveningRender.setFillPoints(true);
			eveningRender.setLineWidth(2);
			eveningRender.setDisplayChartValues(true);
			
			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			multiRenderer.setChartTitle("Temperature Chart");
			multiRenderer.setXTitle("Week xx");
			multiRenderer.setYTitle("Temperature in Celcius");
			multiRenderer.setZoomButtonsVisible(false);
			multiRenderer.setBarSpacing(0.5);
			for (int i = 0; i < tempData.size(); i++)
			{
				
				String reportDate = df.format(tempData.get(i).getDate());
				
				multiRenderer.addXTextLabel(i, reportDate);
			}
			
			// Adding incomeRenderer and expenseRenderer to multipleRenderer
			// Note: The order of adding dataseries to dataset and renderers to
			// multipleRenderer
			// should be same
			multiRenderer.addSeriesRenderer(morningRender);
			multiRenderer.addSeriesRenderer(afternoonRender);
			multiRenderer.addSeriesRenderer(eveningRender);
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.BLACK);
			multiRenderer.setMarginsColor(Color.BLACK);
			// Creating an intent to plot bar chart using dataset and
			// multipleRenderer
			GraphicalView chartView = ChartFactory.getBarChartView(
					getActivity().getBaseContext(), dataset, multiRenderer,
					Type.DEFAULT);
			
			// Start Activity
			LinearLayout layout = (LinearLayout) getActivity().findViewById(
					R.id.temperature_barchart_layout);
			// layout.removeAllViews();
			layout.addView(chartView, new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	}
}
