package com.nxcomm.blinkhd.ui;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.blinkhd.R;
import com.nxcomm.meapi.device.CameraInfo;

public class SavedDayArrayAdapter extends ArrayAdapter<SavedDay>
{
	private Context			context;
	SparseArray<SavedDay>	savedDays	= new SparseArray<SavedDay>();
	@Override
	public boolean isEnabled(int position)
	{
		// TODO Auto-generated method stub
		return true;
	}
	public SavedDayArrayAdapter(Context context, SparseArray<SavedDay> savedDays)
	{
		
		super(context, R.layout.list_row_saved_day);
		this.context = context;
		this.savedDays = savedDays;
		
		notifyDataSetChanged();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_row_saved_day, parent,
				false);
		TextView textView = (TextView) rowView
				.findViewById(R.id.textViewCameraName);
		
		textView.setText(savedDays.get(position).getName());
		Log.d("mbp", savedDays.get(position).getName());
		return rowView;
	}
	
	@Override
	public int getCount()
	{
		// TODO Auto-generated method stub
		return this.savedDays.size();
	}
}

class SavedDay
{
	private String	name;
	private int dateStamp;
	private String dateString;
	
	
	public SavedDay(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
}
