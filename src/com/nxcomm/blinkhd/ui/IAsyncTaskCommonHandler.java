package com.nxcomm.blinkhd.ui;

public interface IAsyncTaskCommonHandler
{
	public void onPostExecute(Object result);
	public void onPreExecute();
	public void onCancelled();
	
}
