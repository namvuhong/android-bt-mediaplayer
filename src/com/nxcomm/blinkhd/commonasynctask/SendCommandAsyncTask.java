package com.nxcomm.blinkhd.commonasynctask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import android.os.AsyncTask;

import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.SendCommandResponse;

public class SendCommandAsyncTask extends
        AsyncTask<String, Void, SendCommandResponse>
{
	private IAsyncTaskCommonHandler	handler;

	public SendCommandAsyncTask(IAsyncTaskCommonHandler handler)
	{
		this.handler = handler;
	}

	@Override
	protected SendCommandResponse doInBackground(String... params)
	{

		SendCommandResponse res = null;
		if (params.length >= 3)
		{
			try
			{
				res = Device.sendCommand(params[0], params[1], params[2]);
			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}

	@Override
	protected void onPostExecute(SendCommandResponse result)
	{
		// TODO Auto-generated method stub
		if (handler != null)
		{
			handler.onPostExecute(result);
		}
		super.onPostExecute(result);
	}

}
