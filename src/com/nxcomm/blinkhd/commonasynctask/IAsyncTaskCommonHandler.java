package com.nxcomm.blinkhd.commonasynctask;

public interface IAsyncTaskCommonHandler
{
	public void onPostExecute(Object result);
	public void onPreExecute();
	public void onCancelled();
	
}
