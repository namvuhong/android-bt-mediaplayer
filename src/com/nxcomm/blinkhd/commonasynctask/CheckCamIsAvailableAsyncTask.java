package com.nxcomm.blinkhd.commonasynctask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.IsAvailableResponse;

import android.os.AsyncTask;

public class CheckCamIsAvailableAsyncTask extends
        AsyncTask<String, Void, Boolean>
{
	private IAsyncTaskCommonHandler	handler;

	public CheckCamIsAvailableAsyncTask(IAsyncTaskCommonHandler handler)
	{
		this.handler = handler;
	}

	@Override
	protected Boolean doInBackground(String... params)
	{
		boolean result = false;
		if(params.length == 2)
		{
			String apiKey = params[0];
			String deviceID = params[1];
			try
            {
	           IsAvailableResponse res = Device.isAvailable(apiKey, deviceID);
	           if(res != null && res.getStatus() == 200 && res.getData() != null)
	           {
	        	   result = res.getData().isAvailable();
	           }
	   
            }
            catch (SocketTimeoutException e)
            {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
            catch (MalformedURLException e)
            {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
            catch (IOException e)
            {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
            }
			
		}
		return result;
	}
	@Override
	protected void onPostExecute(Boolean result)
	{
	    // TODO Auto-generated method stub
		if(handler != null)
		{
			handler.onPostExecute(result);
		}
	    super.onPostExecute(result);
	}

}
