package com.nxcomm.blinkhd.commonasynctask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

import com.nxcomm.meapi.Device;
import com.nxcomm.meapi.device.CameraInfo;
import com.nxcomm.meapi.device.GetCameraInfoResponse;
import com.nxcomm.meapi.device.IsAvailableResponse;

import android.os.AsyncTask;

public class GetCameraInfoAsyncTask extends AsyncTask<String, Void, CameraInfo>
{
	private IAsyncTaskCommonHandler	handler;

	public GetCameraInfoAsyncTask(IAsyncTaskCommonHandler handler)
	{
		this.handler = handler;
	}

	@Override
	protected CameraInfo doInBackground(String... params)
	{
		CameraInfo result = null;
		if (params.length == 2)
		{
			String apiKey = params[0];
			String deviceID = params[1];
			try
			{
				GetCameraInfoResponse res = Device.getCameraInfo(apiKey,
				        deviceID);
				if (res != null && res.getStatus() == 200
				        && res.getCameraInfo() != null)
				{
					result = res.getCameraInfo();
				}

			}
			catch (SocketTimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (MalformedURLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return result;
	}

	@Override
	protected void onPostExecute(CameraInfo result)
	{
		// TODO Auto-generated method stub
		if (handler != null)
		{
			handler.onPostExecute(result);
		}
		super.onPostExecute(result);
	}

}
