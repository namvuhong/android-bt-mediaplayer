package cz.havlena.ffmpeg.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Handler.Callback;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blinkhd.R;
import com.blinkhd.playback.LoadPlaylistListener;
import com.blinkhd.playback.LoadPlaylistTask;
import com.media.ffmpeg.FFMpeg;
import com.media.ffmpeg.FFMpegException;
import com.media.ffmpeg.android.FFMpegMovieViewAndroid;
import com.msc3.PublicDefine;
import com.msc3.Streamer;
import com.nxcomm.meapi.device.Event;
import com.nxcomm.meapi.device.GeneralData;
import com.nxcomm.meapi.device.Playlist;
import com.nxcomm.meapi.device.TimelineEvent;

public class FFMpegPlaybackActivity extends Activity implements Callback, LoadPlaylistListener
{
	private static final String 	TAG = "FFMpegPlaybackActivity";

	public static final String ACTION_FFMPEG_PLAYER_STOPPED = 
			"cz.havlena.ffmpeg.ui.ACTION_FFMPEG_PLAYER_STOPPED";

	public static final String EVENT_NOTIFICATION = "FFMpegPlaybackActivity_event_notification";
	public static final String STR_STREAM_URL = "FFMpegPlaybackActivity_str_stream_url";
	public static final String STR_DEVICE_MAC = "FFMpegPlaybackActivity_str_device_mac";
	public static final String STR_EVENT_CODE = "FFMpegPlaybackActivity_str_event_code";

	//private static final String 	LICENSE = "This software uses libraries from the FFmpeg project under the LGPLv2.1";
	private static final int QUERY_PLAYLIST_INTERVAL = 10000;

	private FFMpegMovieViewAndroid 	mMovieView;
	private String device_mac;
	//private WakeLock				mWakeLock;

	private int default_width, default_height;
	private int default_screen_width, default_screen_height;
	private float ratio;

	private IPlaylistUpdater playlistUpdater;
	private Timer queryPlaylistTask;
	
	private TimelineEvent mEvent;
	private String eventCode;
	private String firstClipPath;
	private boolean shouldStopLoading;
	private ArrayList<String> mPlaylist;
	
	private PowerManager.WakeLock wl;

	private boolean user_cancelled ; 
	private static final int DIALOG_CONNECTION_IN_PROGRESS = 1; 
	private static final int DIALOG_CONNECTION_CANCELLING = 2;
	private static final int DIALOG_CAMERA_IS_NOT_AVAILABLE = 3;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, TAG + " onCreate...");

		user_cancelled = false; 

		firstClipPath = null;
		mEvent = null;
		mPlaylist = new ArrayList<String>();

		setContentView(R.layout.ffmpeg_main);

		Intent intent = getIntent();
		if (intent != null)
		{

			String filePath = null; 
			if (intent.getData() != null)
			{

				filePath = intent.getData().getPath();
				Log.d(TAG, "Play offline file: "+ filePath);
				mPlaylist.add(filePath);
			}



			Bundle extra = intent.getExtras();
			if (extra != null)
			{
				device_mac = extra.getString(STR_DEVICE_MAC);
				eventCode = extra.getString(STR_EVENT_CODE);
				Log.d(TAG, "event code: " + eventCode);
			}
		}

		try
		{
			showDialog(DIALOG_CONNECTION_IN_PROGRESS);
		}
		catch (Exception e)
		{

		}

		if (device_mac == null || eventCode == null) 
		{
			Log.d(TAG, "Not specified device mac or time code");

			if (mPlaylist.size() == 0 )
			{
				Log.d(TAG, "Also no URL passed . Exiting.. ");

				finish();
				return; 

			}


			//Wait to onStart to start playing 

		}
		else
		{

			try {

				FFMpeg ffmpeg = new FFMpeg();
				queryPlaylistTask = new Timer();	
				//run query playlist immediately
				queryPlaylistTask.schedule(new QueryPlaylistTask(), 0);


			} catch (FFMpegException e) {
				Log.d(TAG, "Error when initializing ffmpeg: " + e.getMessage());
				FFMpegMessageBox.show(this, e);
				finish();
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		if (mMovieView != null)
		{
			recalcDefaultScreenSize();
			resizeFFMpegView();
		}
	}

	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder;
		AlertDialog alert;
		ProgressDialog dialog;
		Spanned msg;
		switch (id) {
		case DIALOG_CAMERA_IS_NOT_AVAILABLE:
			builder = new AlertDialog.Builder(this);
			msg = Html
					.fromHtml("<big>"
							+ getResources()
							.getString(
									R.string.cant_open_file)
									+ "</big>");
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.OK),
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int which) {
					dialog.cancel();

					finish(); 


				}
			});

			alert = builder.create();
			return alert;
		case DIALOG_CONNECTION_IN_PROGRESS:
			dialog = new ProgressDialog(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.buffering_this_could_take_a_few_second_please_wait_)
							+ "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);

			dialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {

					user_cancelled = true; 
					dialog.dismiss();

					showDialog(DIALOG_CONNECTION_CANCELLING);

					new Thread(new Runnable()
					{
						
						@Override
						public void run()
						{
							// TODO Auto-generated method stub
							stopPlayback();
							finish();
						}
					}).start();
					
				}
			});

			dialog.setButton(getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			return dialog;

		case DIALOG_CONNECTION_CANCELLING:
			dialog = new ProgressDialog(this);
			msg = Html
					.fromHtml("<big>"
							+ getString(R.string.cancelling_)
							+ "</big>");
			dialog.setMessage(msg);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);



			return dialog;
		default:
			return null;
		}
	}


	private void setupFFMpegPlayback()
	{

		if (user_cancelled == false)
		{

			if (firstClipPath == null)
			{
				Log.d(TAG, "First clip path is null");
				finish();
			}
			else
			{
				PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
				wl = pm.newWakeLock(
						PowerManager.FULL_WAKE_LOCK
						| PowerManager.ACQUIRE_CAUSES_WAKEUP
						| PowerManager.ON_AFTER_RELEASE,
						"TURN ON for first time connect");
				wl.setReferenceCounted(false);
				wl.acquire();
				
				setContentView(R.layout.ffmpeg_playback_activity);
				
				mMovieView = (FFMpegMovieViewAndroid) findViewById(R.id.imageVideo);

				mMovieView.initVideoView(new Handler(FFMpegPlaybackActivity.this),true,false);

				mMovieView.setVideoPath(firstClipPath);

				//set playlist updater
				playlistUpdater = mMovieView.getFFMpegPlayer();
				playlistUpdater.finishLoadingPlaylist(shouldStopLoading);
				playlistUpdater.updatePlaylist(mPlaylist);
				
				final ImageView imgFullClose = (ImageView) findViewById(R.id.imgCloseFull);
				if (imgFullClose != null)
				{
					imgFullClose.setOnClickListener(new OnClickListener()
					{
						
						@Override
						public void onClick(View v)
						{
							// TODO Auto-generated method stub
							stopPlayback();
							finish();
						}
					});
				}
				
				final TextView txtDone = (TextView) findViewById(R.id.txtPlaybackDone);
				if (txtDone != null)
				{
					txtDone.setOnClickListener(new OnClickListener()
					{
						
						@Override
						public void onClick(View v)
						{
							// TODO Auto-generated method stub
							stopPlayback();
							finish();
						}
					});
				}
			}
		}
		else
		{
			stopPlayback();
			finish();
		}
	}


	private void recalcDefaultScreenSize()
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		if (displaymetrics.widthPixels > displaymetrics.heightPixels)
		{
			default_screen_height = displaymetrics.heightPixels;
			default_screen_width = displaymetrics.widthPixels;
		}
		else
		{
			default_screen_height = displaymetrics.widthPixels;
			default_screen_width = displaymetrics.heightPixels;
		}
		Log.d(TAG, "Default screen size: default width, default height: " +
				default_screen_width +	", "+default_screen_height);

		if (mMovieView != null)
		{
			if (default_screen_height * ratio > default_screen_width) {
				default_width = default_screen_width;
				default_height = (int) (default_width / ratio);
			} else {
				default_height = default_screen_height;
				default_width = (int) (default_height * ratio);
			}
		}

		Log.d(TAG, "Recalculate default size: width: " + default_width +
				", height: " + default_height);
	}

	private void resizeFFMpegView()
	{
		if (mMovieView != null)
		{
			int new_width, new_height;

			if ( (getResources().getConfiguration().orientation & 
					Configuration.ORIENTATION_LANDSCAPE) == Configuration.ORIENTATION_LANDSCAPE) 
			{
				new_width = default_width;
				new_height = default_height;
			}
			else
			{
				new_width = default_screen_height;
				new_height = (int) (new_width / ratio);
			}

			LayoutParams params = mMovieView.getLayoutParams();
			params.width = new_width;
			params.height = new_height;
			mMovieView.setLayoutParams(params);
			Log.d(TAG, "Surface resized: width: "+new_width+", height: "+new_height);
		}
	}


	private void stopPlayback()
	{
		if (wl != null && wl.isHeld()) {
			wl.release();
			wl = null;
		}
		
		if (mPlaylist != null)
		{
			Iterator<String> iter = mPlaylist.iterator();
			while (iter.hasNext())
			{
				iter.next();
				iter.remove();
			}

			mPlaylist = null;
		}

		if (queryPlaylistTask != null)
		{
			queryPlaylistTask.cancel();
		}

		if (mMovieView != null && !mMovieView.isReleasingPlayer())
		{
			mMovieView.release();
		}


	}

	protected void onStop()
	{
		super.onStop();

		stopPlayback();
		finish(); 
	}


	@Override
	protected void onStart() {
		super.onStart();

		if (device_mac == null || eventCode == null)
		{
			//this is local playback

			try
			{
				FFMpeg ffmpeg = new FFMpeg();
				firstClipPath =  mPlaylist.get(0); 
				shouldStopLoading = true; 

				setupFFMpegPlayback();
			}
			catch (FFMpegException e) {
				Log.d(TAG, "Error when initializing ffmpeg: " + e.getMessage());
				FFMpegMessageBox.show(this, e);
				finish();
			}

		}

	}



	protected void onDestroy()
	{
		//Close the player properly here. 
		super.onDestroy();
		
		if (wl != null && wl.isHeld()) {
			wl.release();
			wl = null;
		}
	}

	/* (non-Javadoc)
	 * @see android.os.Handler.Callback#handleMessage(android.os.Message)
	 */
	@Override
	public boolean handleMessage(Message msg) 
	{

		if (user_cancelled == true)
		{
			stopPlayback();
			finish();
			return false; 
		}


		switch(msg.what)
		{

		case Streamer.MSG_CAMERA_IS_NOT_AVAILABLE:

			if (wl != null && wl.isHeld()) {
				wl.release();
				wl = null;
			}
			
			final Runnable showDialog = new Runnable() {
				@Override
				public void run() {

					try {
						dismissDialog(DIALOG_CONNECTION_IN_PROGRESS);
					} catch (Exception e) {

					}


					try {
						showDialog(DIALOG_CAMERA_IS_NOT_AVAILABLE);
					} catch (Exception ie) {
					}



				}
			};
			runOnUiThread(showDialog);
			break;

		case Streamer.MSG_VIDEO_SIZE_CHANGED:
			int video_width = msg.arg1;
			int video_height = msg.arg2;
			ratio = (float) video_width / video_height;
			Log.d(TAG, "Video width, height, ratio: "+video_width+", "+video_height+", "+ratio);
			recalcDefaultScreenSize();

			break;
		case Streamer.MSG_VIDEO_STREAM_HAS_STOPPED:
			Log.d(TAG, "handle mesg MSG_VIDEO_STREAM_HAS_STOPPED : finishing.. () ");
			finish(); 
			break;

		case Streamer.MSG_VIDEO_STREAM_HAS_STARTED:

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					resizeFFMpegView();
				}
			});

			
			try
			{
				dismissDialog(DIALOG_CONNECTION_IN_PROGRESS);
			}
			catch (Exception e)
			{

			}
			
		
			break;
		}



		return false;
	}

	
	/* (non-Javadoc)
	 * @see com.blinkhd.playback.LoadPlaylistListener#onRemoteCallComplete(com.nxcomm.meapi.device.Event, com.nxcomm.meapi.device.Event[])
	 */
	@Override
	public void onRemoteCallSucceeded(TimelineEvent latest, TimelineEvent[] allEvents) 
	{

		if (user_cancelled == true)
		{
			stopPlayback();
			finish();
			return ; 
		}


		if (latest != null)
		{
			mEvent = latest;
			GeneralData[] playlist = latest.getData();
			if (playlist != null && playlist.length > 0)
			{
				for (int i=0; i<playlist.length; i++)
				{
					if (playlist[i] != null && playlist[i].getFile() != null)
					{
						String filePath = playlist[i].getFile();
						if (!filePath.isEmpty() && isSupportedFormat(filePath))
						{
							if (i < mPlaylist.size())
							{
								mPlaylist.set(i, playlist[i].getFile());
							}
							else
							{
								mPlaylist.add(playlist[i].getFile());
							}
						}
					}
				}
			}


			if (mPlaylist.size() > 0)
			{				
				//check whether the playlist is complete
				String last_file = mPlaylist.get(mPlaylist.size()-1);
				if (isLastClip(last_file))
				{
					//the playlist is complete --> stop loading
					shouldStopLoading = true;
				}

				if (firstClipPath != null)
				{
					//update playlist for FFMpegPlayer
					playlistUpdater.updatePlaylist(mPlaylist);
					playlistUpdater.finishLoadingPlaylist(shouldStopLoading);

					if (!shouldStopLoading)
					{
						queryPlaylistTask.schedule(new QueryPlaylistTask(), QUERY_PLAYLIST_INTERVAL);
					}
				}
				else
				{
					firstClipPath = mPlaylist.get(0);
					Log.d(TAG, "First clip path: "+firstClipPath);
					setupFFMpegPlayback();
				}
			}
			else
			{
				//playlist is empty --> finish
				Log.d(TAG, "Playlist is empty");
				FFMpegPlaybackActivity.this.finish();
			}

		}
		else
		{
			//event is null --> finish
			Log.d(TAG, "Event is null");
			FFMpegPlaybackActivity.this.finish();
		}
	}


	private boolean isSupportedFormat(String fileUrl)
	{
		boolean isSupported = false;
		String fileName = null;
		int startIdx = fileUrl.lastIndexOf("/") + 1;
		int endIdx = fileUrl.lastIndexOf("?");

		if (startIdx > 0 && endIdx > 0 && startIdx < endIdx)
		{
			fileName = fileUrl.substring(startIdx, endIdx);
		}

		if (fileName != null && fileName.contains(".flv") && 
				fileName.lastIndexOf(".flv") == fileName.length()-4)
		{
			isSupported = true;
		}

		return isSupported;
	}


	private boolean isLastClip(String fileUrl)
	{
		boolean isLast = false;
		String fileName = null;
		int startIdx = fileUrl.lastIndexOf("/") + 1;
		int endIdx = fileUrl.lastIndexOf("?");

		if (startIdx > 0 && endIdx > 0 && startIdx < endIdx)
		{
			fileName = fileUrl.substring(startIdx, endIdx);
		}

		if (fileName != null && fileName.contains("last.flv") && 
				fileName.lastIndexOf("last.flv") == fileName.length()-8)
		{
			isLast = true;
		}

		return isLast;
	}


	private class QueryPlaylistTask extends TimerTask
	{

		/* (non-Javadoc)
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {

			SharedPreferences settings = getSharedPreferences(PublicDefine.PREFS_NAME, 0);
			String saved_token = settings.getString(PublicDefine.PREFS_SAVED_PORTAL_TOKEN, null);
			
			LoadPlaylistTask load_list_task = new LoadPlaylistTask(
					FFMpegPlaybackActivity.this, false);
			
			load_list_task.execute(saved_token, device_mac, eventCode);
		}

	}


	/* (non-Javadoc)
	 * @see com.blinkhd.playback.LoadPlaylistListener#onRemoteCallFailed(int)
	 */
	@Override
	public void onRemoteCallFailed(int errorCode) 
	{

		if (user_cancelled == true)
		{
			stopPlayback();
			finish();
			return ; 
		}


		if (queryPlaylistTask != null)
		{
			Log.d(TAG, "Load playlist timeout...try to reload...");
			queryPlaylistTask.schedule(new QueryPlaylistTask(), QUERY_PLAYLIST_INTERVAL);
		}
	}

}
