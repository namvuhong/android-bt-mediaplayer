package cz.havlena.ffmpeg.ui;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import com.blinkhd.R;
import com.media.ffmpeg.FFMpeg;
import com.nxcomm.jstun_android.RtspStunBridgeService;

import android.app.ActivityManager;
import android.app.ListActivity;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class FFMpegFileExplorer extends ListActivity {

	private static final String TAG = "FFMpegFileExplorer";

	private String 			mRoot = "/";
	private TextView 		mTextViewLocation;
	private File[]			mFiles;
	private String http_pass = null;
	private String device_ip = null;
	private int device_port = -1;
	private boolean shouldUnregStun;


	private static final String RTSP_BRIDGE_SERVICE = "com.nxcomm.jstun_android.RtspStunBridgeService";
	

	protected void onDestroy()
	{
		super.onDestroy();
		if (shouldUnregStun)
		{
			unregisterReceiver(stunBridgeBCReciever);
		}
		
		Intent  i = new Intent(RTSP_BRIDGE_SERVICE);

		if (isServiceRunning(FFMpegFileExplorer.this))
		{
			stopService(i);
		}
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		shouldUnregStun = false;
		Bundle extra = getIntent().getExtras();

		if (extra != null)
		{
			String url = extra.getString("StreamUrl");

			if (url != null)
			{
				Log.d(TAG, " Get URL from extra : " + url);
				http_pass = extra.getString("http_pass");
				device_ip = this.get_ip_from_url(url);
				device_port = this.get_port_from_url(url);
				Log.d(TAG, "ip for direction pad cmd: " + device_ip);
				Log.d(TAG, "port for direction pad cmd: " + device_port);
				startPlayer(url);

				extra.remove("StreamUrl");
				extra.remove("http_pass");

				finish();
				return; 

			}
		}


		setContentView(R.layout.ffmpeg_file_explorer);
		mTextViewLocation = (TextView) findViewById(R.id.textview_path);
		getDirectory(mRoot);


		Button rtmpBtn = (Button) findViewById(R.id.testRtmp) ;
		rtmpBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				EditText te = (EditText)findViewById(R.id.editText1);

				String url  =     te.getText().toString();
				//String url = "http://192.168.5.121:8090/test-sdp.sdp";


				//String url = "http://nxcomm-office.no-ip.info:8090/aaa";


				//String url = "http://192.168.5.119:8090/no_audio";
				//String url = "http://192.168.5.119:8090/no_video";

				//String url = "rtsp://118.140.96.190:1935/live/cam1.stream"; 
				//String url =  "http://115.77.220.87:49383/?action=appletvastream";
				//String url = "rtsp://192.168.5.102:554/amrtest";


				//String url = "udp://@:5000";

				Log.d(TAG, " Try to stream  from url : " + url);

				if ( url  != null /*&& url.startsWith("http://")*/ )
				{
					startPlayer(url);

					SharedPreferences settings = getSharedPreferences("Preferences", 0);
					SharedPreferences.Editor edit = settings.edit();
					edit.putString("url1", url);
					edit.commit();
				}
				else
				{
					Log.e(TAG," URL is NULL"); 
				}

			}
		});


		Button rtmp2Btn = (Button) findViewById(R.id.testRtmp1) ;
		rtmp2Btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				EditText te = (EditText)findViewById(R.id.EditText02);

				String url  = te.getText().toString();

				//String url = "rtsp://192.168.5.121:6666/test";
				//String url = "rtsp://192.168.5.121:554/amrtest";

				//Aztech camera
				//String url = "rtsp://admin:123456@192.168.5.123:554/iphone/11";

				//gst rtsp
				//String video_url = "rtsp://192.168.3.105:6667/test";
				//String audio_url = "rtsp://192.168.5.121:8554/amrAudioTest";
				shouldUnregStun = true;
				registerReceiver(stunBridgeBCReciever, 
						new IntentFilter(RtspStunBridgeService.ACTION_RTSP_BRIDGE_READY));

				if ( url  != null && url.startsWith("rtsp://") )
				{
					Log.d(TAG, " Try to stream  from url : " + url);


					SharedPreferences settings = getSharedPreferences("Preferences", 0);
					SharedPreferences.Editor edit = settings.edit();
					edit.putString("url2", url);
					edit.commit();

					startPlayer(url);
				}
				else
				{
					Log.e(TAG," URL is NULL"); 
				}


			}
		});


		Button rtspStunBtn = (Button) findViewById(R.id.testSDP) ;
		rtspStunBtn.setOnClickListener( new OnClickListener() {

			
			@Override
			public void onClick(View v) {

				Intent  i = new Intent(RTSP_BRIDGE_SERVICE);
				
				
				if (isServiceRunning(FFMpegFileExplorer.this))
				{
					stopService(i);
				}

				shouldUnregStun = true;
				registerReceiver(stunBridgeBCReciever, 
						new IntentFilter(RtspStunBridgeService.ACTION_RTSP_BRIDGE_READY));
				
				int sessionId = -1; 
				EditText te = (EditText)findViewById(R.id.EditText03);
				String url  = te.getText().toString();
				
				sessionId = Integer.parseInt(url);
				
				
				i.putExtra(RtspStunBridgeService.SESSION_ID_EXTRA, sessionId);
				startService(i);



			}
		});



		SharedPreferences settings = getSharedPreferences("Preferences", 0);
		String url1 = settings.getString("url1", null); 

		EditText te = (EditText)findViewById(R.id.editText1);
		if (url1 != null)
		{
			te.setText(url1);
		}

		url1 = settings.getString("url2",null);

		te = (EditText)findViewById(R.id.EditText02);
		if(url1 != null)
		{
			te.setText(url1);
		}



	}


	private BroadcastReceiver stunBridgeBCReciever = new  BroadcastReceiver() 
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equalsIgnoreCase(RtspStunBridgeService.ACTION_RTSP_BRIDGE_READY))
			{
				//Stun bridge started  to forward stream..
				Log.d(TAG, " Rcved bridge ready bc -- start stream now.. ");
				String url = Environment.getExternalStorageDirectory().getPath() + File.separator +
						"camera-stream.sdp";
				Log.d(TAG, " Try to stream  from url : " + url);
				startPlayer(url);
			}
		}

	};


	private static boolean isServiceRunning(Context c) {
		ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) 
		{
			if (RTSP_BRIDGE_SERVICE.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	protected void onStart()
	{
		super.onStart();


		Log.d(TAG, "onStart enter.. ");
		
		Intent  i = new Intent(RTSP_BRIDGE_SERVICE);
		if (isServiceRunning(FFMpegFileExplorer.this))
		{
			stopService(i);
		}

		
		Log.d(TAG, "onStart exit.. ");
	}


	protected static boolean checkExtension(File file) {
		String[] exts = FFMpeg.EXTENSIONS;
		for(int i=0;i<exts.length;i++) {
			if(file.getName().indexOf(exts[i]) > 0) {
				return true;
			}
		}
		return false;
	}

	private void sortFilesByDirectory(File[] files) {
		Arrays.sort(files, new Comparator<File>() {

			public int compare(File f1, File f2) {
				return Long.valueOf(f1.length()).compareTo(f2.length());
			}

		});
	}

	private void getDirectory(String dirPath) {
		try {
			mTextViewLocation.setText("Location: " + dirPath);

			File f = new File(dirPath);
			File[] temp = f.listFiles();

			sortFilesByDirectory(temp);

			File[] files = null;
			if(!dirPath.equals(mRoot)) {
				files = new File[temp.length + 1];
				System.arraycopy(temp, 0, files, 1, temp.length);
				files[0] = new File(f.getParent());
			} else {
				files = temp;
			}

			mFiles = files;
			setListAdapter(new FileExplorerAdapter(this, files, temp.length == files.length));
		} catch(Exception ex) {
			FFMpegMessageBox.show(this, "Error", ex.getMessage());
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		File file = mFiles[position];

		if (file.isDirectory()) {
			if (file.canRead())
				getDirectory(file.getAbsolutePath());
			else {
				FFMpegMessageBox.show(this, "Error", "[" + file.getName() + "] folder can't be read!");
			}
		} else {
			if(!checkExtension(file)) {
				StringBuilder strBuilder = new StringBuilder();
				for(int i=0;i<FFMpeg.EXTENSIONS.length;i++)
					strBuilder.append(FFMpeg.EXTENSIONS[i] + " ");
				FFMpegMessageBox.show(this, "Error", "File must have this extensions: " + strBuilder.toString());
				return;
			}



			//String url = "rtmp://112.213.86.13:1935/live/cam1.stream";
			String url = file.getAbsolutePath();
			Log.d(TAG, "set to view from " + url );
			startPlayer(url);
		}
	}

	private void startPlayer(String filePath) {
//		Intent i = new Intent(this, FFMpegPlayerActivity.class);
//		i.putExtra(getResources().getString(R.string.input_file), filePath);
//
//		if (device_ip != null)
//		{
//			i.putExtra("device_ip", device_ip);
//		}
//
//		if (device_port != -1)
//		{
//			i.putExtra("device_port", device_port);
//		}
//
//		i.putExtra("http_pass", http_pass);
//
//		startActivity(i);
	}

	private void startPlayer(String filePath, String filePath2) {
//		Intent i = new Intent(this, FFMpegPlayerActivity.class);
//		i.putExtra(getResources().getString(R.string.input_file), filePath);
//		i.putExtra(getResources().getString(R.string.input_file2), filePath2);
//
//		if (device_ip != null)
//		{
//			i.putExtra("device_ip", device_ip);
//		}
//
//		if (device_port != -1)
//		{
//			i.putExtra("device_port", device_port);
//		}
//
//		i.putExtra("http_pass", http_pass);
//
//		startActivity(i);
	}

	private String get_ip_from_url(String url)
	{
		String result = null;
		int startIdx = 7; // "http://..."
		int endIdx = url.indexOf(":", 7);
		result = url.substring(startIdx, endIdx);

		return result;
	}

	private int get_port_from_url(String url)
	{
		int result = -1;
		String result_str = null;
		int startIdx = url.indexOf(":", 7) + 1;
		int endIdx = url.indexOf("/", startIdx);
		result_str = url.substring(startIdx, endIdx);
		result = Integer.parseInt(result_str);
		if (result == 8090)
		{
			result = 80;
		}

		return result;
	}

}
