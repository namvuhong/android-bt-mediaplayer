/**
 * 
 */
package cz.havlena.ffmpeg.ui;

import java.util.ArrayList;

/**
 * @author Hoang
 *
 */
public interface IPlaylistUpdater {
	
	public void updatePlaylist(ArrayList<String> list);
	
	public void finishLoadingPlaylist(boolean isFinishLoading);

}
